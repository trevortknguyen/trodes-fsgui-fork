#ifndef TRODES_USB_H
#define TRODES_USB_H
#ifdef __cplusplus
extern "C" {
#endif

#include "trodes_settings.h"

//################################
//  Communication with MCU via USB
//################################

typedef struct SpkgUsbConn SpkgUsbConn;

SpkgUsbConn* SpkgUsbConn_new();
void SpkgUsbConn_delete(SpkgUsbConn* h);

/*
Inits and closes ftdi handle
*/
int usb_initialize(SpkgUsbConn* h);
void usb_close(SpkgUsbConn* h);

/*
Sends start/stop acquisition commands
*/
int usb_startacquisition(SpkgUsbConn* h, int ecu_connected);
int usb_startsimulation(SpkgUsbConn *h, int auxbytes, int intancards);
int usb_stopacquisition(SpkgUsbConn* h);

/*
Receiving data over usb
*/
// //Checks for pending packets with EXPECTED LENGTH. Returns size of pending dgram
// int usb_pendingpacket(SpkgUsbConn* h, unsigned int packetsize);

//Reads in packet of expected size, returns bytes read in. Default timeout of 1000
int usb_recvpacket(SpkgUsbConn* h, void* buffer, unsigned int expectedlen);

/*
Misc. commands
*/
int usb_sendsettlecommand(SpkgUsbConn* h);
int usb_sendsettlechannel(SpkgUsbConn* h, uint16_t byte, uint8_t bit, uint16_t delay, uint8_t triggerstate);
int usb_getMCUSettings(SpkgUsbConn* h, MCUSettings* settings);
int usb_getHSSettings(SpkgUsbConn* h, HSSettings* settings);


#ifdef __cplusplus
}
#endif
#endif // TRODES_USB_H
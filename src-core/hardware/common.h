#ifndef SPKGCODES_H
#define SPKGCODES_H


enum CommandMessageCodes {
    command_startNoECU = 0x61,
    command_stop = 0x62,
    command_startWithECU = 0x64,
    command_sdCardUnlock = 0x65,
    command_settle = 0x66,
    command_writeHSSettingsToFlash = 0x67,
    command_trigger = 0x69,
    command_setSettleTriggerChannel = 0x6A,
    command_startSimulationNoECU = 0x6B,
    command_startSimulationWithECU = 0x6C,
    command_configureSD = 0x6D,
    command_getHeadstageSettings = 0x81,
    command_setHeadstageSettings = 0x82,
    command_getControllerSettings = 0x83,
    command_setControllerSettings = 0x84,
    command_setStimulateParams = 0x85, //Set stim pattern in one slot
    command_clearStimulateParams = 0x86, //Clear a stim pattern in one memory slot
    command_stimulateStart = 0x87,  //Start a single defined stim pattern
    command_stimulateStartGroup = 0x88,//Start all defined stim patterns
    command_stimulateStop = 0x89,  //Stop a single running stim pattern
    command_stimulateStopGroup = 0x8A,//Stop all running stim patterns
    command_getStimulationParams = 0x8B, //Get stimulation parameters in one memory slot
    command_setGlobalStimSettings = 0x8C, //Send global stimulation settings
    command_setGlobalStimAction = 0x8D, //Send one-time global stimulation command
    command_setNeuroPixelsSettings = 0xA0, //Send neuropixels-specific settings (channels on, etc)
    command_startImpedanceTest = 0xB0, //command to initiate the measure of impedance on a specific channel

};

enum ReceivedMessageCodes {
    received_headstageSettings = 0x81,
    received_controllerSettings = 0x83,
    received_stimulationParams = 0x74,
    received_impedanceValues = 0xB0
};

#define SPKG_MCU_CTRL_PORT 8100
#define SPKG_MCU_DATA_PORT 8200

#define MCUSETTINGSSIZE 17
#define HSSETTINGSSIZE 27

#endif// SPKGCODES_H

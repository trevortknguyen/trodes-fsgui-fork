#include "datexporthandler.h"
#include <algorithm>
//#include <iostream>

DATExportHandler::DATExportHandler(QStringList arguments):
    AbstractExportHandler(arguments)
{

    setDefaultMenuEnabled(false); //the default options are turned off
    trodesRFInterleavedFlagByte = -1; //where the interleaved flag byte is in the packet
    trodesRFInterleavedFlagBit = -1; //the bit in the flag byte that indicates that a sync came in
    for (int i = 0; i < 8; i++) {
        sdInterleavedData[i] = 0;
    }
    recInterleavedFlag = 0;
    sdInterleavedFlag = 0;
    proceedToProcessing = true;


    parseArguments();

    /*
    Do custom argument checks here like this:
    if (ARGVAL != REQUIREMENT) {
        qDebug() << "Error: ....";
        argumentReadOk = false;
    }
    */

    //Parse custom arguments
    //parseCustomArguments(argumentsProcessed);

    if (argumentsProcessed != argumentList.length()-1) {
        _argumentsSupported = false;
        return;
    }
}

DATExportHandler::~DATExportHandler()
{

}

void DATExportHandler::printHelpMenu() {
    //printf("-outputrate <integer>  -- The sampling rate of the output file. \n"
    //       );


    printf("\nUsed to merge data from an SD card recording with simultaneuous Trodes recording. Output file is saved is same directory unless another directory is specified.\n");
    printf("Usage:  mergesdrecording -rec TRODESINPUTFILENAME -sd SDINPUTFILENAME -numchan NUMBEROFCHANNELS -mergeconf MERGEFILEWORKSPACE\n");
    printf("Optional arguments: \n"
           "-output OUTPUTFILENAME \n"
           "-outputdirectory OUTPUTDIRECTORYNAME\n"
           "-v (prints version info and quits)\n");


    //AbstractExportHandler::printHelpMenu();
}

void DATExportHandler::displayVersionInfo() {
    printf("\n****MergeSDRecording Version information****\n");
    printf("%s", qPrintable(GlobalConfiguration::getVersionInfo(false)));
    printf("\n");
}

void DATExportHandler::parseArguments() {
    //Parse extra arguments not handled by the base class

    int optionInd = 1;

    QString sd_string = "";
    QString merge_workspace_string = "";
    QString numchan_string = "";

    while (optionInd < argumentList.length()) {

        if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0)) {
            //printCustomMenu();
            //return;
            printHelpMenu();
            argumentsProcessed = argumentList.length()-1; //We don't need to process the rest of the aruments becuase we are going to end the program.
            proceedToProcessing = false;
            return;
        }  else if ((argumentList.at(optionInd).compare("-v",Qt::CaseInsensitive)==0)) {
            //printCustomMenu();
            //return;
            displayVersionInfo();
            argumentsProcessed = argumentList.length()-1; //We don't need to process the rest of the aruments becuase we are going to end the program.
            proceedToProcessing = false;
            return;
        }else if ((argumentList.at(optionInd).compare("-sd",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            sd_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-mergeconf",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            merge_workspace_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-numchan",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            numchan_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }
        optionInd++;
    }

    if (sd_string.isEmpty()) {
        printf("Error. No SD filename given.  Must include -sd <filename> in arguments.\n");
        argumentReadOk = false;
        return;
    } else {
        //Make sure the first file exists
        sdFileName = sd_string;

        QFileInfo fi(sdFileName);

        if (!fi.exists()) {
            printf("Error. File could not be found: %s\n", sdFileName.toLocal8Bit().data());
            argumentReadOk = false;
        }

    }

    if (merge_workspace_string.isEmpty()) {
        printf("Error. No merge workspace filename given.  Must include -mergeconf <filename> in arguments.\n");
        argumentReadOk = false;
        return;
    } else {
        //Make sure the first file exists
        mergedConfFileName = merge_workspace_string;

        QFileInfo fi(mergedConfFileName);

        if (!fi.exists()) {
            printf("Error. File could not be found: %s\n", mergedConfFileName.toLocal8Bit().data());
            argumentReadOk = false;
        }

    }

    if (numchan_string.isEmpty()) {
        //qDebug() << "Error. Must include -numchan NUMBEROFCHANNELS in arguments.";
        //argumentReadOk = false;
        //return;
        SDRecNumChan = -1;
    } else {
        bool ok;

        SDRecNumChan = numchan_string.toInt(&ok);
        if (!ok) {
            printf("Error. Must include -numchan NUMBEROFCHANNELS in arguments. NUMBEROFCHANNELS must be an integer multiple of 32.\n");
            argumentReadOk = false;
            return;
        }

    }

    AbstractExportHandler::parseArguments();
}


int DATExportHandler::processData() {
    qDebug() << "\nMerging files...";

    //Calculate the packet positions for each channel that we are extracting, plus
    //other critical info (number of saved channels, reference info, etc).
    calculateChannelInfo();

    if (readMergeConfig() < 0) {
        return -1;
    }


    //Open the REC and SD files for reading. The SD file is searched for all sync stamps here. No searching
    //is done on the REC file at this point.

    if (!openInputFile()) {
        printf("Error. Rec file could not be opened and read.\n");
        return -1;
    }


    //Compile a list of the devices from which to write data from
    deviceListToUse.clear();
    for (int i=0;i < sortedDeviceList.length();i++) {
        //if the environmental rec file has a dedicated RF device (not interleaved), then we will not include it in the final merged file.
        if (!(hardwareConf->devices[i].name == "RF") ) {
            deviceListToUse.push_back(i);
        }
    }

    //Find the location of the RF sync in the rec packet
    for (int deviceInd = 0; deviceInd < hardwareConf->devices.length(); deviceInd++) {
        for (int devChInd = 0; devChInd < hardwareConf->devices[deviceInd].channels.length(); devChInd++) {

            if (hardwareConf->devices[deviceInd].channels[devChInd].idString == "RFsync")  {
                trodesRFPacketByte = hardwareConf->devices[deviceInd].channels[devChInd].startByte;
                trodesRFInterleavedFlagByte = hardwareConf->devices[deviceInd].channels[devChInd].interleavedDataIDByte; //where the interleaved flag byte is in the packet
                trodesRFInterleavedFlagBit = hardwareConf->devices[deviceInd].channels[devChInd].interleavedDataIDBit; //the bit in the flag byte that indicates that a sync came in

                break;

            }

        }
    }

    int finalNumHeaderBytes = mergeConfigData.hardwareConf.headerSize*2;
    if (mergeConfigData.hardwareConf.sysTimeIncluded) {
        finalNumHeaderBytes+=8;
    }

    int expectedNumHeaderBytes;
    if (trodesRFInterleavedFlagByte == -1) {

        expectedNumHeaderBytes = (hardwareConf->headerSize*2) - 4 + 8;  //take away the four from RF that won't get transferred and add 8 for the sensor data
    } else {

        expectedNumHeaderBytes = (hardwareConf->headerSize*2) + 8;  //add 8 for the sensor data

    }

    if (hardwareConf->sysTimeIncluded) {

        expectedNumHeaderBytes+=8;
    }

    if (mergeConfigData.spikeConf.deviceType == "neuropixels1") {

        //If we are using neuropixels recording, then there are some modifications to the default behavior
        int numberOfProbes = mergeConfigData.hardwareConf.NCHAN/384;

        expectedNumHeaderBytes += (numberOfProbes*64)+2; //This is the number of bytes that the LFP section adds.

    }
    if (trodesRFInterleavedFlagByte > -1) {
        //Both files have interleaved data. We will combine them, so the merge workspace should not have
        //both.

        expectedNumHeaderBytes -= 8;
    }


    if (expectedNumHeaderBytes != finalNumHeaderBytes) {
        qDebug() << "Error. The device list in the workspace that will be appended to the merged data does not add up to the expected size. Expected:" << expectedNumHeaderBytes << "bytes. Size in the chosen file:" << finalNumHeaderBytes << "bytes.";
        if (hardwareConf->sysTimeIncluded != mergeConfigData.hardwareConf.sysTimeIncluded) {
            printf("Possible reason: Detected a mismatch in SysClock between the rec file and the final workspace.\n");
        }
        bool foundSensorDevice = false;
        for (int i=0;i<mergeConfigData.hardwareConf.devices.length();i++) {
            if (mergeConfigData.hardwareConf.devices[i].name.contains("multiplexed",Qt::CaseInsensitive)) {
                foundSensorDevice = true;
            }
        }
        if (!foundSensorDevice) {
            printf("Possible reason: No 'multiplexed' device found in final workspace.  It is required.\n");
        }

        if (trodesRFInterleavedFlagByte == -1) {
            printf("Possible reason: No 'RFSync' location found in environmental workspace.  It is required.\n");
        }

        //For debugging
        /*qDebug() << "Environmental header size" << hardwareConf->headerSize*2 << "Merge header size:" << mergeConfigData.hardwareConf.headerSize*2;
        for (int i=0;i<mergeConfigData.hardwareConf.devices.length();i++) {
            qDebug() << mergeConfigData.hardwareConf.devices[i].name << mergeConfigData.hardwareConf.devices[i].numBytes;
        }
        for (int i=0;i<hardwareConf->devices.length();i++) {
            qDebug() << hardwareConf->devices[i].name << hardwareConf->devices[i].numBytes;
        }*/


        //qDebug() << "Make sure the workspace has included motion sensors and system clock (if it was used in the environmental recording)";
        return -1;
    }




    if (!openSDInputFile()) {
        return -1;
    }


    if (!scanRecForSyncs()) {
        return -1;
    }

    progressMarker = sdFilePtr->size()/100;
    QFileInfo fi(sdFileName);
    QString fileBaseName;

    if (outputFileName.isEmpty()) {
        fileBaseName = fi.completeBaseName()+"_merged";
    } else {
        fileBaseName = outputFileName.endsWith(".rec") ? outputFileName.replace(".rec", "") : outputFileName;
    }

    QString saveLocation;
    if (outputDirectory.isEmpty()) {
        saveLocation = fi.absolutePath()+QString(QDir::separator());
    } else {
        saveLocation = outputDirectory;
    }

    QList<QFile*> neuroFilePtrs;






    //Create an output file for the merged data
    //*****************************************

    //Write the final configuration to the top of the output file
    if (!mergeConfigData.writeRecConfig(saveLocation+fileBaseName+QString(".rec"),-1)) {
        printf("Error creating output file.\n");
        return -1;
    }


    neuroFilePtrs.push_back(new QFile);
    neuroFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".rec"));
    if (!neuroFilePtrs.last()->open(QIODevice::Append)) {
        printf("Error opening output file for appending data.\n");
        return -1;
    }
    neuroStreamPtrs.push_back(new QDataStream(neuroFilePtrs.last()));
    neuroStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

    neuroFilePtrs.last()->flush();

    //************************************************

    int inputFileInd = 0;

    while (inputFileInd < recFileNameList.length()) {
        if (inputFileInd > 0) {
            //There are multiple files that need to be stiched together. It is assumed that they all have
            //the exact same header section.
            recFileName = recFileNameList.at(inputFileInd);
            uint32_t lastFileTStamp = currentTimeStamp;


            printf("\nAppending from file: %s\n", recFileName.toLocal8Bit().data());
            fflush(stdout);
            QFileInfo fi(recFileName);

            if (!fi.exists()) {
                printf("Error. File could not be found: %s", recFileName.toLocal8Bit().data());
                break;
            }
            if (!openInputFile()) {
                printf("Error: it appears that the file does not have an identical header to the last file. Cannot append to file.\n");
                return -1;
            }
            for (int i=0; i < channelFilters.length(); i++) {
                channelFilters[i]->resetHistory();
            }
            if (currentTimeStamp < lastFileTStamp) {
                printf("Error: timestamps do not begin with greater value than the end of the last file. Aborting.\n");
                return -1;
            }
        }

        if (!(sdSyncValues.length() > 1)) {
            printf("Error: could not find at least two sync stamps in the SD recording file.\n");
            return -1;
        }



        currentSyncInd = 0; //used to keep track of which sync stamp we are on
        uint32_t packetsSinceLastRecSync = 0;      
        packLengthAdjustmentofSD = 0;
        currentRecPacket = -1;
        currentSDPacket = -1;
        int numSyncsFound = 0;


        //*******************
        //Register the two files. This is done by identifying the packet location
        //in each file when the first common sync stamp occurs in both files. From there,
        //the files are both rewound and much as possible.
        //********************

        if (!registerFiles()) {
            printf("Error. File registration failed. No common sync values found in the two files.\n");
            return -1;
        }

        firstSample = true;

        //**************************
        //Main processing loop begins here amd runs until one of the two files stops.
        //Goal: continuously align the data and stream merged data to output files
        //**************************
        while(!filePtr->atEnd()) {


            //Read the next packet from both files.
            if (!readNextRecPacket()) {
                //If we have reached the end of the rec file, we are done
                printf("Rec file ended first (merge ending before end of logger data).\n");
                break;
            }
            if (filePtr->atEnd()) {
                printf("Rec file ended first (merge ending before end of logger data).\n");
            }
            if (!readNextSDPacket()) {
                //If we have reached the end of the SD file, we are done
                printf("SD file ended first.\n");
                break;
            }

            //**********************
            //First we check for dropped packets in either file. If SD packets are
            //dropped, we drop an equal number of REC packets. If REC packets are dropped,
            //we filled in the dropped values with the last measured value.
            //************************

            //There should be no check for dropped packets on the first sample
            if (firstSample) {
                lastTimeStampInFile = currentTimeStamp-1;
                lastSDTimeStamp = currentSDTimeStamp-1;
                firstSample = false;
            }

            //A timestamp jump of 1 means nothing was dropped.
            int jumpInRecTimeStamps = (currentTimeStamp-lastTimeStampInFile); // (1 if no data missing)
            int jumpInSDTimeStamps = (currentSDTimeStamp-lastSDTimeStamp);

            //If a dropped packet in the REC file occured...
            if (jumpInRecTimeStamps > 1) {
                printf("REC recording has %d packets dropped at timestamp %d (%s)\n",jumpInRecTimeStamps-1, currentTimeStamp, getHumanReadableTime(currentTimeStamp,conf_ptrs.hardwareConf->sourceSamplingRate).toLocal8Bit().data());
                fflush(stdout);
                currentTimeStamp = lastTimeStampInFile+1; //rewind time to the first missing rec packet
            }

            //If a dropped packet in the SD file occured...
            if (jumpInSDTimeStamps > 1) {
                //There are missing packets in the SD recording. We create an equal packet drop in the rec file.
                //Note: Dealing with it here can cause weird oscillations.
                printf("SD recording has %d packets dropped at timestamp %d (%s). SD Timestamp: %d. Last SD timestamp was: %d.\n",jumpInSDTimeStamps-1,currentTimeStamp,getHumanReadableTime(currentTimeStamp,conf_ptrs.hardwareConf->sourceSamplingRate).toLocal8Bit().data(),currentSDTimeStamp,lastSDTimeStamp);
                fflush(stdout);
                //If more (or equal) REC packets were dropped than SD packets...
                if (jumpInRecTimeStamps >= jumpInSDTimeStamps) {
                    printf("Rec file had simulaneous drops of %d packets.\n",jumpInRecTimeStamps-1);
                    fflush(stdout);
                    //Fast-forward over the common dropped packets. The remaining missing packets will be filled in below.
                    while (jumpInSDTimeStamps > 1) {
                        jumpInRecTimeStamps--;
                        jumpInSDTimeStamps--;
                        currentTimeStamp++;
                    }
                } else {
                    //First we fast-forward over common dropped packets
                    while (jumpInRecTimeStamps > 1) {
                        jumpInRecTimeStamps--;
                        jumpInSDTimeStamps--;
                        currentTimeStamp++;
                    }
                    //Then we drop the remaining rec packets to match the full drop size of the sd file
                    while (jumpInSDTimeStamps > 1) {
                        readNextRecPacket();
                        //If a sync stamp is found in the REC file while skipping over packets...
                        if (currentRecSyncStamp > 0) {
                            uint32_t sVal = currentRecSyncStamp & 0xFFFFFF; //The sync stamp is a 24 bit value
                            for (int i=0; i<recSyncValues.length();i++) {
                                if (recSyncValues.at(i) == sVal) {
                                    printf("Ignoring sync %d from rec file.\n",sVal);
                                    fflush(stdout);
                                    recSyncValues.removeAt(i);
                                    break;
                                }
                            }
                        }
                        jumpInSDTimeStamps--;
                        packLengthAdjustmentofSD--;
                    }

                }
            }

            //Take care of any remaining drops in the rec file by filling in the rec packets with the last read packet from the rec file
            while (jumpInRecTimeStamps > 1) {

                writeMergedPacket();
                readNextSDPacket();

                //currentRecPacket++; //CHECK: Not sure if this is right
                packLengthAdjustmentofSD++;
                packetsSinceLastRecSync++;
                currentTimeStamp++;
                jumpInRecTimeStamps--;
            }

            //*******************************
            //Next we check for temporal drift in synchronization becuase of small differences
            //in sampling rate between the two systems. Again, we give preference to the data
            //in the SD file by deleting or filling in REC packets to adjust for the drift.
            //*******************************

            //If a sync stamp is found in the REC file...
            if (currentRecSyncStamp > 0) {

                uint32_t sVal = currentRecSyncStamp & 0xFFFFFF; //The sync stamp is a 24 bit value

                //Now we need to check if there is a sync in the sd file at the same relative location
                //Search the sync values on the SD recording (already compiled) for a match
                for (int s = 0; s < sdSyncValues.length(); s++) {

                    if (sdSyncValues[s] == sVal) {
                        //A match sync was found in the sd recording                       
                        currentSyncInd = s;
                        numSyncsFound++;
                        packetsSinceLastRecSync = 0;

                        //Based on the most recent registration, are the packet indeces where the sync is detected still aligned
                        //between the two files?
                        if (!(sdPacketDuringSync[s] == (currentRecPacket-syncOffsetPackets)+packLengthAdjustmentofSD)) {
                            //The sync in the SD card file is not in the expected place.  Either drift or dropped packets would cause this.

                            int numberOfPacketsOff;
                            numberOfPacketsOff = sdPacketDuringSync[s] - ((currentRecPacket-syncOffsetPackets)+packLengthAdjustmentofSD);
                            if (numberOfPacketsOff > 0) {
                                //Rec file is coming in slower than sd data, so we copy this rec file packet while
                                //reading in the drifted sd card packets until the drift is corrected.                               
                                printf("Sync. SD file behind by %d samples. This can occur if the sampling speed of the headstage is faster than the environemental controller. Adjusting by filling in the enivironmental record with duplicate data.\n",numberOfPacketsOff);
                                fflush(stdout);
                                while (numberOfPacketsOff > 0) {
                                    if (!readNextSDPacket()) break;
                                    //currentSDPacket++;
                                    //currentTimeStamp++;
                                    numberOfPacketsOff--;
                                    writeMergedPacket();
                                    packLengthAdjustmentofSD++;
                                }
                            } else {

                                //We deal with the reverse condition below, so we should never get here.
                                //qDebug() << "Error: Can't figure out sync order.";
                            }
                        } else {

                            if (sdPacketDuringSync[currentSyncInd] == currentSDPacket) {
                                printf("Sync. Files are in register and no adjustment needed.\n");
                                fflush(stdout);
                            } else {
                                printf("Sync. Files in register but secondary test failed.\n");
                                fflush(stdout);
                            }
                        }
                        break;
                    }
                }

            } else {
                //No sync signal found in rec file for this packet. Check if a sync signal was found in the SD file.
                if (((currentSyncInd+1) <  sdPacketDuringSync.length()) && (sdPacketDuringSync[currentSyncInd+1] == currentSDPacket)) {
                    //The next SD sync signal is encountered before the next rec file sync signal,
                    //so the SD data is coming in slower than the rec data, and we skip over rec file packets until the drift is corrected.

                    currentSyncInd++;
                    numSyncsFound++;
                    bool foundNextRecSync = false;
                    int numStepsJumped = 0;

                    bool sameRecSyncExists = false;
                    for (int recSyncInd = 0; recSyncInd < recSyncValues.length(); recSyncInd++) {
                        if (recSyncValues.at(recSyncInd) == sdSyncValues[currentSyncInd]) {
                            sameRecSyncExists = true;
                            break;
                        }
                    }

                    if (sameRecSyncExists) {
                        //Skip over the rec (dio, etc) data samples until we reach the same sync signal.
                        while (!foundNextRecSync) {
                            //Read in a packet of data to make sure everything looks good

                            if (!readNextRecPacket()) {
                                printf("Reached end of rec file before finding corresponding sync value.\n");
                                break;
                            }
                            numStepsJumped++;
                            packLengthAdjustmentofSD--;
                            uint32_t sVal = currentRecSyncStamp & 0xFFFFFF; //The sync stamp is a 24 bit value

                            if (sVal == sdSyncValues[currentSyncInd]) {
                                foundNextRecSync = true;
                            } else if (sVal > sdSyncValues[currentSyncInd]) {
                                //This is an error condition
                                printf("WARNING: Next rec sync is %d, which is more than the current SD sync of %d. THIS IS AN ERROR CONDITION RESULTING IN SOME DATA LOSS.\n",sVal,sdSyncValues[currentSyncInd]);
                                fflush(stdout);
                                break;
                            }
                        }
                        if (foundNextRecSync) {
                            printf("Sync. Rec file behind by %d samples. This can occur if the sampling speed of the headstage is slower than the environemental controller. Adjusting by removing samples from the environemtal record.\n",numStepsJumped);
                            fflush(stdout);
                        }
                    }
                }
            }

            //***************************
            //Now that all corrections in alignment are done, we write the next merged packet in the output file
            //(which also potentially occurs a bit in the alignment procedure above).
            //***************************
            writeMergedPacket();
            packetsSinceLastRecSync++;

            //Print progress (based on location in input file)
            if ((sdFilePtr->pos()%progressMarker) < lastProgressMod) {
                //printf("\r%d%%",currentProgressMarker);
                //fflush(stdout);

                std::cerr  << "\r" << currentProgressMarker << "%";

                currentProgressMarker = 100.0*((double)sdFilePtr->pos()/(double)sdFilePtr->size());
                //currentProgressMarker+=10;
            }
            lastProgressMod = (sdFilePtr->pos()%progressMarker);

            pointsSinceLastLog = (pointsSinceLastLog+1)%decimation;
        }
        printf("Final timestamp: %d\n",currentTimeStamp);

        printf("\nNumber of sync points found in both files: %d\n",numSyncsFound);
        printf("\rDone\n");
        filePtr->close();
        inputFileInd++;
    }

    for (int i=0; i < neuroFilePtrs.length(); i++) {
        neuroFilePtrs[i]->flush();
        neuroFilePtrs[i]->close();
    }

    for (int i=0; i < timeFilePtrs.length(); i++) {
        timeFilePtrs[i]->flush();
        timeFilePtrs[i]->close();
    }

    return 0;
}

QString DATExportHandler::getHumanReadableTime(uint32_t timestamp, uint32_t samplingrate) {
    QString ss = QString(":%1").arg((timestamp/samplingrate)%60,2,10,QLatin1Char('0'));
    QString mm = QString(":%1").arg((timestamp/(samplingrate*60))%60,2,10,QLatin1Char('0'));
    QString hh = QString("%1").arg(timestamp/(samplingrate*60*60),2,10,QLatin1Char('0'));


    return hh+mm+ss;
}

bool DATExportHandler::readNextRecPacket() {


    //Read in a packet of data from the REC file
    if (!(filePtr->read(buffer.data(),filePacketSize) == filePacketSize)) {
        //We have reached the end of the rec file
        return false;
    }


    uint32_t tmpTimeStamp = currentTimeStamp;
    bool goodPacketFound = false;
    //Check if the packet looks good.
    if ((*buffer.data() == 0x55)) {
        if (firstSample) {
            goodPacketFound = true;
        } else {

            char *bufferPtr = buffer.data()+packetTimeLocation;
            uint32_t *timePtr = (uint32_t *)(bufferPtr);
            if ((*timePtr > tmpTimeStamp) && (*timePtr < tmpTimeStamp+2000)) {
                goodPacketFound = true;
            }
        }
    }


    while (!goodPacketFound) {
        //skip bad packets
        if (!(filePtr->read(buffer.data(),filePacketSize) == filePacketSize)) {
            //We have reached the end of the rec file
            return false;
        }
        if ((*buffer.data() == 0x55)) {
            if (firstSample) {
                goodPacketFound = true;
            } else {
                char *bufferPtr = buffer.data()+packetTimeLocation;
                uint32_t *timePtr = (uint32_t *)(bufferPtr);
                if ((*timePtr > tmpTimeStamp) && (*timePtr < tmpTimeStamp+2000)) {
                    goodPacketFound = true;
                } else {
                    tmpTimeStamp = *timePtr;
                }

            }
        }

    }



    lastTimeStampInFile = currentTimeStamp;

    //Find the time stamp from rec file
    bufferPtr = buffer.data()+packetTimeLocation;
    tPtr = (uint32_t *)(bufferPtr);
    currentTimeStamp = *tPtr+startOffsetTime;

    //Find the sync info
    bufferPtr = buffer.data() + trodesRFPacketByte;
    tPtr = (uint32_t *)(bufferPtr);

    if (trodesRFInterleavedFlagByte > -1) {
        //the RF info is multiplexed with other info, so we first check if RF stamp is in the packet
        char* flagPtr = buffer.data()+trodesRFInterleavedFlagByte;
        if (*flagPtr & (1 << trodesRFInterleavedFlagBit)) {
            currentRecSyncStamp = *tPtr;
        } else {
            currentRecSyncStamp = 0;
        }

        recInterleavedFlag = *flagPtr;
        if (recInterleavedFlag > 0) {
            //copy the interleaved data
            QByteArray b(flagPtr,8);
            recInterleavedData.push_front(b);
            //b.resize(8);

            //recInterleavedData
            //memcpy(recInterleavedData, flagPtr, 8);
        }
    } else {
        //no multiplexed RF, it has a dedicated spot
        currentRecSyncStamp = *tPtr;
    }

    currentRecPacket++;

    return true;

}

bool DATExportHandler::registerFiles() {
    //*********************
    //This function is used to identify the location in both files (rec and sd files) where the first common sync stamp occurs.
    //This is used to register the two files.
    //It is assumed that both files are rewound to the beginning of the data section
    //and that all sync stamps (and their locations) have already been identified in the SD file.
    //Therefore, only the rec file needs to be searched for sync stamps.
    //**********************
    printf("Registering the files...\n");
    int numberOfCommonSyncs = 0;
    for (int i=0; i<recSyncValues.length();i++) {
        for (int j=0; j<sdSyncValues.length();j++) {
            if (recSyncValues.at(i) == sdSyncValues.at(j)) {
                numberOfCommonSyncs++;
                break;
            }
        }
    }
    printf("Number of common syncs: %d\n",numberOfCommonSyncs);

    bool firstSyncFound = false;
    while ( (!filePtr->atEnd()) && !firstSyncFound) {

        //read in the next packet from the REC file
        if (!readNextRecPacket()) {
            //If we have reached the end of the rec file and no registration has occurred, there is a problem!
            printf("Error: rec file ended before first common sync stamp found.\n");
            return false;
        }

        //If the packet contains a sync stamp...
        if (currentRecSyncStamp > 0) {
            //A sync stamp is found in the rec file

            uint32_t sVal = currentRecSyncStamp & 0xFFFFFF; //The sync stamp is a 24 bit value
            //qDebug() << "Rec Sync:" << sVal;


            //search the known sync stamps in the SD file for a match
            for (int s = 0; s < sdSyncValues.length(); s++) {

                if (sVal == sdSyncValues[s]) {
                    //a match was found

                    syncOffsetTime = (int64_t)currentTimeStamp - sdTimeDuringSync[s];
                    printf("Time lag between files: %d samples.\n",syncOffsetTime);
                    fflush(stdout);

                    if (sdPacketDuringSync[s] >= currentRecPacket) {
                        //The SD recording started before the rec recording (usually the case)
                        //Seek to the beginning of the rec file and to the corresponding position in the SD file

                        syncOffsetPackets = currentRecPacket-sdPacketDuringSync[s];

                        sdFilePtr->seek((sdPacketDuringSync[s]-currentRecPacket)*sdFilePacketSize); //rewind sd file by as much as the rec file has advanced
                        currentSDPacket = sdPacketDuringSync[s]-currentRecPacket-1;

                        filePtr->seek(filePtr->pos()-((currentRecPacket+1)*filePacketSize)); //rewind rec file to beginning
                        currentRecPacket = -1;


                    } else if (sdPacketDuringSync[s] < currentRecPacket) {
                        //The rec recording started before the SD recording

                        syncOffsetPackets = currentRecPacket-sdPacketDuringSync[s];
                        sdFilePtr->seek(0); //rewind sd file to beginning
                        filePtr->seek(dataStartLocBytes + (currentRecPacket-sdPacketDuringSync[s])*filePacketSize); //rewind rec file by as much as the sd file was rewound

                        currentRecPacket = currentRecPacket-sdPacketDuringSync[s]-1;
                        currentSDPacket = -1;
                    }

                    printf("First common sync is index %d in the SD file. Initial packet offset between the files is %d.\n",s,syncOffsetPackets);
                    printf("Starting REC packet is %d and starting SD packet is %d\n", currentRecPacket+1, currentSDPacket+1);
                    fflush(stdout);
                    firstSyncFound = true;
                    break; //no need to search anymore since we found our match

                }
            }
        }
    }
    return firstSyncFound;
}

bool DATExportHandler::readNextSDPacket() {


    if (!(sdFilePtr->read(sdFilePacketBuffer.data(),sdFilePacketSize) == sdFilePacketSize)) {
        //We have reached the end of the sd file
        return false;
    }
    currentSDPacket++;

    uint32_t tmpTimeStamp = currentSDTimeStamp;
    bool goodPacketFound = false;
    //Check if the packet looks good.
    if ((*sdFilePacketBuffer.data() == 0x55)) {
        if (firstSample) {
            goodPacketFound = true;
        } else {

            char *sdBufferPtr = sdFilePacketBuffer.data()+sdFilePacketTimeByteLoc;
            uint32_t *sdTimePtr = (uint32_t *)(sdBufferPtr);
            if ((*sdTimePtr > tmpTimeStamp) && (*sdTimePtr < tmpTimeStamp+2000)) {
                goodPacketFound = true;
            }
        }
    }


    while (!goodPacketFound) {
        //skip bad packets
        if (!(sdFilePtr->read(sdFilePacketBuffer.data(),sdFilePacketSize) == sdFilePacketSize)) {
            //We have reached the end of the sd file
            return false;
        }
        if ((*sdFilePacketBuffer.data() == 0x55)) {
            if (firstSample) {
                goodPacketFound = true;
            } else {

                char *sdBufferPtr = sdFilePacketBuffer.data()+sdFilePacketTimeByteLoc;
                uint32_t *sdTimePtr = (uint32_t *)(sdBufferPtr);
                if ((*sdTimePtr > tmpTimeStamp) && (*sdTimePtr < tmpTimeStamp+2000)) {
                    goodPacketFound = true;
                } else {
                    tmpTimeStamp = *sdTimePtr;
                }
            }
        }
        currentSDPacket++;

        //This variable is used to adjust the expected packet where sync occur when packets are dropped on the sd card. In this case, the packets were not dropped, they were corrupted.
        //However, the jump in timestamps will be interpreted as a drop. We therefore add to the variable the same amount that will be removed to balance things out.
        packLengthAdjustmentofSD++;

    }
    lastSDTimeStamp = currentSDTimeStamp;
    //Find the time stamp from sd file
    char *sdBufferPtr = sdFilePacketBuffer.data()+sdFilePacketTimeByteLoc;
    uint32_t *sdTimePtr = (uint32_t *)(sdBufferPtr);
    currentSDTimeStamp = *sdTimePtr;
    sdBufferPtr = sdFilePacketBuffer.data()+sdFilePacketFlagByteLoc;
    sdInterleavedFlag = *sdBufferPtr;
    if (sdInterleavedFlag > 0) {
        //copy the interleaved data
        memcpy(sdInterleavedData, sdBufferPtr, 8);
    }
    return true;



}

bool DATExportHandler::writeMergedPacket() {
    //*******************
    //This function combines data from the currently loaded packets
    //from the REC and SD files into one merged packet, then writes
    //it to the output file.
    //*******************
    int numBytesWrittenInPacket = 0;
    if (currentTimeStamp <= lastTimeStampInFile) {
        printf("Skipping non-increasing timestmap packet at time %d. Last rec timestamp was %d. This is ok if at end of the merge.\n",currentTimeStamp, lastTimeStampInFile);
        return false;
    }

    neuroStreamPtrs.at(0)->writeRawData(buffer.data(),1); //We always have the the sync byte
    numBytesWrittenInPacket += 1;

    //Write data from approved devices in environmental record
    for (int i=0; i < deviceListToUse.length(); i++) {        
        if ((hardwareConf->devices[deviceListToUse[i]].name != "SysClock")&& (!hardwareConf->devices[deviceListToUse[i]].name.contains("multiplexed",Qt::CaseInsensitive)) ){

            neuroStreamPtrs.at(0)->writeRawData(buffer.data()+hardwareConf->devices[deviceListToUse[i]].byteOffset,hardwareConf->devices[deviceListToUse[i]].numBytes);
            numBytesWrittenInPacket += hardwareConf->devices[deviceListToUse[i]].numBytes;
        }
    }

    //Write sensor data from headstage or rec file
    if (recInterleavedData.length()==0) {
        //No queued rec interleaved packets. Write the sd interleaved data by default.
        neuroStreamPtrs.at(0)->writeRawData(sdFilePacketBuffer.data()+2,8);
        numBytesWrittenInPacket += 8;
    } else if (sdInterleavedFlag > 0) {
        //Even if we have rec interleaved packets queued, we write the sd data if it is nonzero
        neuroStreamPtrs.at(0)->writeRawData(sdFilePacketBuffer.data()+2,8);
        numBytesWrittenInPacket += 8;
    } else {
        neuroStreamPtrs.at(0)->writeRawData(recInterleavedData.takeLast().constData(),8);
        numBytesWrittenInPacket += 8;
    }

    //Write Neuropixels LFP data (if we need to)
    if (mergeConfigData.spikeConf.deviceType == "neuropixels1") {


        int numberOfProbes = mergeConfigData.hardwareConf.NCHAN/384;
        int LFPPacketPayload = (numberOfProbes*64)+2; //This is the number of bytes that the LFP section adds.
        neuroStreamPtrs.at(0)->writeRawData(sdFilePacketBuffer.data()+10,LFPPacketPayload);
        numBytesWrittenInPacket += LFPPacketPayload;
    }


    //Write system time from environmental record (if included)
    if (hardwareConf->sysTimeIncluded) {
        //System time (if included) is always the 8 bytes before the 4-byte hardware timestamp
        neuroStreamPtrs.at(0)->writeRawData(buffer.data()+(packetHeaderSize-12),8);
        numBytesWrittenInPacket += 8;

    }

    //Write the timestamp from the trodes file (environmental record)
    *neuroStreamPtrs.at(0) << currentTimeStamp;
    numBytesWrittenInPacket += 4;

    //Write the neural data from the sd card
    char *dataStartLoc = sdFilePacketBuffer.data()+sdFilePacketStartDataLoc;
    for (int i=0; i < writeSDChannel.length();i++) {
        if (writeSDChannel.at(i)) {
            neuroStreamPtrs.at(0)->writeRawData(dataStartLoc+(2*i),2);
            numBytesWrittenInPacket += 2;
        }
    }

    //qDebug() << numBytesWrittenInPacket;
     return true;
}

int  DATExportHandler::scanSDFileForChannelCount()
{
    //This function is used to calculate the number of neural channels in the SD recording
    //if the user does not specify it.

    QFile sdFile;
    sdFile.setFileName(sdFileName);

    //open the raw data file
    if (!sdFile.open(QIODevice::ReadOnly)) {
        printf("Error: could not open sd recording file for reading.\n");
        return -1;
    }

    char syncByteCheck;
    int byteScan = 0;
    int syncIntervals[200]; //this is the number of sync bytes we are going to scan through
    int syncIntervalIndex = 0;


    //Read one byte at a time, check if it is a sync byte (0x55). If so, assume that
    //a new packet is starting (not always true). Record the number of bytes
    //in the last packet. Do this multiple times to see if the same number comes up a lot.
    while ((!sdFile.atEnd()) && (syncIntervalIndex < 200)) {
        bool doubleFound = false;
        bool atSync = false;
        qint64 currentpos = sdFile.pos();
        sdFile.read(&syncByteCheck,1);
        if ((syncByteCheck == 0x55) && (byteScan>0)) { //78 is the smallest possible packet size with 32 channels
            //qDebug() << "Sync at" << byteScan;
            atSync = true;
            if (sdFile.seek(currentpos+byteScan)) {
                sdFile.read(&syncByteCheck,1);
                if (syncByteCheck == 0x55) {
                    //qDebug() << "Packet size double" << byteScan;
                    doubleFound = true;
                }
                sdFile.seek(currentpos+1);
            }
        }
        if (doubleFound) {
            sdFile.seek(currentpos+byteScan+1);
            //qDebug() << "Packet size" << byteScan;
            syncIntervals[syncIntervalIndex] = byteScan;
            syncIntervalIndex++;
            byteScan=1;
        } else if (atSync) {
            byteScan = 1;
        } else {
            byteScan++;
        }

            /*sdFile.seek(currentpos+1);
            qDebug() << "Packet size" << byteScan;
            syncIntervals[syncIntervalIndex] = byteScan;
            syncIntervalIndex++;
            byteScan=1;
        } else {
            byteScan++;
        }*/
    }

    //Find all the unique packet sizes, and create a tally for each
    int uniqueSizesFound = 0;
    int packetSizes[200];
    int tallies[200];
    int maxValue = 0;
    int maxValueTally = 0;
    for (int i=0; i<200; i++) {
        packetSizes[i]=0;
        tallies[i]=0;
    }
    for (int i=0; i<200; i++) {
      int tempSize = syncIntervals[i];
      int found = -1;
      for (int j=0; j<uniqueSizesFound; j++) {
           if (packetSizes[j]==tempSize) {
                found = j;
                tallies[j]++;
                break;
           }
      }
      if (found == -1) {
            packetSizes[uniqueSizesFound] = tempSize;
            tallies[uniqueSizesFound] = 1;
            uniqueSizesFound++;
      }

      if (tempSize > maxValue) {
          maxValue = tempSize;
          maxValueTally = 1;
      } else if (tempSize == maxValue) {
          maxValueTally++;
      }
    }

    //find the packet size that has the highest tally
    int maxTally = 0;
    int maxIndex = 0;



    for (int i=0; i<200;i++) {
        //qDebug() << tallies[i];
        if ((tallies[i] > 10) && (tallies[i] > maxTally) ) {
            //We have a hit.
            maxTally = tallies[i];
            maxIndex = i;
        }

    }

    int packetSize_calc = -1;

    //We need to subtract the size of the head section that does
    //not contain neural channels.
    int headerSectionSize = 14; //The default header size
    //If the data came from neuropixels, the header section also contains LFP data
    if (mergeConfigData.spikeConf.deviceType == "neuropixels1") {
        int numberOfProbes = mergeConfigData.hardwareConf.NCHAN/384;
        headerSectionSize += ((numberOfProbes*64)+2); //This is the number of bytes that the LFP section adds.
    }


    if (maxValueTally > 1) {
        //The largest packet size found happended more than once. We go with that
        packetSize_calc = maxValue;
        return (packetSize_calc-headerSectionSize)/2; //2 bytes per sample
    } else if (maxTally > 0) {
        //The max value was not repeated, so we can't trust it. So we use whatever value happened a lot (more than 10)

        packetSize_calc = packetSizes[maxIndex];
        return (packetSize_calc-headerSectionSize)/2; //2 bytes per sample
    } else {

        //If we get here, no tally reached the required threshold.
        return -1;
    }

}

bool DATExportHandler::scanRecForSyncs() {
    printf("Finding sync packets in rec file...\n");
    //There needs to be a device entry called 'RF'. We will use this data to synchronize the two files
    bool trodesRFChannelIndFound = false;

    if (trodesRFInterleavedFlagByte > -1) {
        trodesRFChannelIndFound  = true;
    }
    //Make sure the RF sync channel is defined in the .rec file workspace
    if (!trodesRFChannelIndFound) {
        printf("Error: could not find the RF channel in the .rec file.  Workspace must contain a device named RF with a channel named RFsync.\n");
        return false;
    }

    //Seek back to begining of data
    filePtr->seek(dataStartLocBytes);
    uint32_t packetCount = 0;
    recSyncValues.clear();
    recPacketDuringSync.clear();


    while (!filePtr->atEnd()) {
        //Read in a packet of data from the REC file
        if (!(filePtr->read(buffer.data(),filePacketSize) == filePacketSize)) {
            //We have reached the end of the rec file
            break;
        }



        //Find the time stamp from rec file
        bufferPtr = buffer.data()+packetTimeLocation;
        tPtr = (uint32_t *)(bufferPtr);
        uint32_t timeStamp = *tPtr;

        //Find the sync info
        bufferPtr = buffer.data() + trodesRFPacketByte;
        tPtr = (uint32_t *)(bufferPtr);

        uint32_t syncInfo;
        if (trodesRFInterleavedFlagByte > -1) {
            //the RF info is multiplexed with other info, so we first check if RF stamp is in the packet
            char* flagPtr = buffer.data()+trodesRFInterleavedFlagByte;
            if (*flagPtr & (1 << trodesRFInterleavedFlagBit)) {
                syncInfo = *tPtr;
            } else {
                syncInfo = 0;
            }


        } else {
            //no multiplexed RF, it has a dedicated spot
            syncInfo = *tPtr;
        }

        if (syncInfo > 0) {

            syncInfo = syncInfo & 0xFFFFFF; //The sync stamp is a 24 bit value
            //qDebug() << "Rec sync:" << syncInfo << "packet timestamp:" << timeStamp;
            recSyncValues.push_back(syncInfo);
            recPacketDuringSync.push_back(packetCount);
        }


        packetCount++;
    }

    printf("%d sync values found on Rec file.\n",recSyncValues.length());
    filePtr->seek(dataStartLocBytes);
    return true;

}


bool DATExportHandler::openSDInputFile() {
    //This function will open the SD file, and scan for number of channels and all RF sync stamps.

    /*
    Below is the default packet format for SD card recording.
    The total packet size is 14 bytes + 2*SDRecNumChan

    x55 (Sync byte)
    x68 (Data format byte - Note: MCU will replace this with Digital inputs)
    1 byte with valid flags (bit3 = acc_valid, bit2 = gyro_valid, bit1 = mag_valid, bit0 = rf_valid) (Note: at most one bit set)
    x00 (filler to make the packet byte count even)
    i2c_data_x[7:0]   (or rf_timestamp[7:0] if rf_valid)
    i2c_data_x[15:8]  (or rf_timestamp[15:8] if rf_valid)
    i2c_data_y[7:0]   (or rf_timestamp[23:16] if rf_valid)
    i2c_data_y[15:8]  (or rf_timestamp[31:24] if rf_valid)
    i2c_data_z[7:0]   (or x00 if rf_valid)
    i2c_data_z[15:8]  (or x00 if rf_valid)
    timestamp[7:0]
    timestamp[15:8]
    timestamp[23:16]
    timestamp[31:24]
    for (ch = 0, ch < N, ch++) {
      sample0[7:0]
      sample0[15:8]
      sample1[7:0]
      sample1[15:8]
      sample2[7:0]
      sample2[15:8]
      sample3[7:0]
      sample3[15:8]
    }

    Note: i2c_data[x,y,z] = 0 if no valid flag set.
    */

    if (SDRecNumChan == -1) {
        //Number of channels in SD recording was not specified, so we need to scan the file to calculate it.
        SDRecNumChan = scanSDFileForChannelCount();
        if (SDRecNumChan < 0) {
            printf("Error: Could not calculate number of channels in SD recording. Try providing the value with the -numchan input.\n");
            return false;
        } else {
            printf("Scanned number of channels in SD recording: %d\n",SDRecNumChan);
        }
    }

    if (mergeConfigData.globalConf.saveDisplayedChanOnly) {
        //numChannelsInFile = sortedChannelList.length();
        if (mergeConfigData.streamConf.nChanConfigured != SDRecNumChan) {
            printf("Error. Number of channels configured in merge workspace (%d) is different than what is in the SD recording (%d)\n",mergeConfigData.streamConf.nChanConfigured,SDRecNumChan);
            return false;
        }
    }

    sdFilePacketSize = 2*SDRecNumChan + 14;
    sdFilePacketStartDataLoc = 14;
    sdFilePacketFlagByteLoc = 2;
    sdFilePacketSyncByteLoc = 4;
    sdFilePacketTimeByteLoc = 10;




    hardwareConf->NCHAN = SDRecNumChan;



    bool partialSDChannelSet = false;
    if (mergeConfigData.hardwareConf.NCHAN != SDRecNumChan) {

        //In this condition, the headstage logger may have been configured to only save a partial set of channels.
        //To be compatible with tethered recordings, the NCHAN value should be the total number of possible channels before some were discarded, for either logging or tethered recordings.
        //
        //This should only be allowed if the saveDisplayChanOnly field is true

        if (!mergeConfigData.globalConf.saveDisplayedChanOnly) {
            printf("Error: The number of channels in the final workspace is not %d. It is configured for %d channels.\n",SDRecNumChan,mergeConfigData.hardwareConf.NCHAN);
            return false;
        } else {
            partialSDChannelSet = true;
        }
    }

    if (mergeConfigData.spikeConf.deviceType == "neuropixels1") {

        //If we are using neuropixels recording, then there are some modifications to the default packet structure
        //becuase the header section now contains LFP data.
        int numberOfProbes = mergeConfigData.hardwareConf.NCHAN/384;
        int LFPPacketPayload = (numberOfProbes*64)+2; //This is the number of bytes that the LFP section adds.
        sdFilePacketSize = sdFilePacketSize+LFPPacketPayload;
        sdFilePacketStartDataLoc += LFPPacketPayload;
        sdFilePacketTimeByteLoc += LFPPacketPayload;
    }



    //If saveDisplayedChanOnly is set, we should only write the channels from the SD card that are configured in the merge workspace file.
    writeSDChannel.resize(SDRecNumChan);
    if (mergeConfigData.globalConf.saveDisplayedChanOnly && (!partialSDChannelSet)) {
        //We recorded all possible channels on the logger, but only some of them sould be saved in the merged file.
        //We need to identify which of the channels from the SD recording should be discarded before the merge.
        writeSDChannel.fill(false);
        QList<int> sortedChannelList;


        for (int n = 0; n < mergeConfigData.spikeConf.ntrodes.length(); n++) {

            for (int c = 0; c < mergeConfigData.spikeConf.ntrodes[n]->hw_chan.length(); c++) {
                bool channelalreadyused = false;

                int tempHWRead = mergeConfigData.spikeConf.ntrodes[n]->hw_chan[c];
                if (tempHWRead > SDRecNumChan) {
                    return -1;
                }
                //make sure there is not a repeat channel
                for (int ch=0;ch<sortedChannelList.length();ch++) {
                    if (sortedChannelList[ch]==tempHWRead) {
                        channelalreadyused = true;
                    }
                }
                //qDebug() << "Read" << spikeConf->ntrodes[n]->unconverted_hw_chan[c] << tempHWRead;
                if (!channelalreadyused) {
                    sortedChannelList.push_back(tempHWRead);
                    writeSDChannel[tempHWRead] = true;

                }

            }
        }

    } else {
        //Other allowed possibilities:
        //1) discard nothing AND all channels exist in the logger file
        //2) discard nothing AND a partial set of channels exist in the logger file
        //
        //NOT ALLOWED: save a further reduced set from a partially saved channel set in logger file

        writeSDChannel.fill(true);
    }


    sdFilePtr = new QFile;
    sdFilePtr->setFileName(sdFileName);

    //open the raw data file
    if (!sdFilePtr->open(QIODevice::ReadOnly)) {
        delete sdFilePtr;
        printf("Error: could not open sd recording file for reading.\n");
        return false;
    }

    sdFilePacketBuffer.resize(sdFilePacketSize*2); //we make the input buffer the size of two packets
    //dataLastTimePoint.resize(channelPacketLocations.size()); //Stores the last data point.
    //dataLastTimePoint.fill(0);
    //pointsSinceLastLog=-1;

    //----------------------------------

    //skip past the config header
    /*
    if (!filePtr->seek(dataStartLocBytes)) {
        delete filePtr;
        qDebug() << "Error seeking in file";
        return false;
    }*/


    char *sdBufferPtr;
    uint32_t *sdTimePtr;


    //Read in a packet of data to make sure everything looks good
    if (!(sdFilePtr->read(sdFilePacketBuffer.data(),sdFilePacketSize) == sdFilePacketSize)) {
        delete sdFilePtr;
        printf("Error: could not read from SD card recording file.\n");
        return false;
    }

    printf("Finding sync packets in SD file...\n");
    fflush(stdout);

    //Seek back to begining of data
    sdFilePtr->seek(0);

    qint64 currentSDPacket = 0;
    //Find all of the sync stamps


    uint32_t ltime=0;
    uint32_t thistime=0;

    uint32_t lastSyncVal;
    uint32_t lastBadSyncVal = 0;
    bool recovered = false;


    while (!sdFilePtr->atEnd()) {
        //Seek to flag location of data packet

        if (!sdFilePtr->seek((currentSDPacket*sdFilePacketSize))) {
            break;
        }


        if (sdFilePtr->read(sdFilePacketBuffer.data(),sdFilePacketStartDataLoc) == sdFilePacketStartDataLoc) {

            //Report dropped packets

            sdTimePtr = (uint32_t *)(sdFilePacketBuffer.data()+sdFilePacketTimeByteLoc);


            thistime = *sdTimePtr;

            if (recovered) {
                qDebug() << "Recovery packet timestamp:" << thistime << "Continuing sync packet scan...";
                recovered = false;
            }

            /*if (thistime-ltime > 1) {
                qDebug() << thistime-ltime << "gap in data at time. Last time" << ltime << "Current time" << thistime << "Packet" << currentSDPacket;
            }*/



            sdBufferPtr = sdFilePacketBuffer.data()+sdFilePacketFlagByteLoc;

            if (*sdFilePacketBuffer.data() != 0x55) {
                printf("Bad SD file packet found. Expected packet size: %d. Last timestamp: %d\n",sdFilePacketSize,ltime);
                fflush(stdout);

                uint32_t scanSDpackets = currentSDPacket;

                bool foundNextSync = false;
                char syncByteCheck;
                int packetsSinceLastSync = 0;

                //If scanning full packets did not work, we try scanning byte-by-byte.
                if (!recovered) {
                    if (currentSDPacket > 0) {
                        int byteScan = 0;
                        packetsSinceLastSync = 0;

                        sdFilePtr->seek(((currentSDPacket-1)*sdFilePacketSize));



                        //for (int byteScan=0; byteScan < sdFilePacketSize*100; byteScan++) {
                        int numTries = 0;

                        while (!sdFilePtr->atEnd()&&(numTries < 1000)) {
                            sdFilePtr->read(&syncByteCheck,1);
                            //qDebug() << (uint8_t)syncByteCheck;
                            if ((syncByteCheck == 0x55)) {

                                numTries++;
                                if (packetsSinceLastSync > 0) {
                                    printf("Next packet size scanned: %d\n",packetsSinceLastSync);
                                }
                                packetsSinceLastSync = 0;


                                //Sync found.  Check for two consecutive syncs to be sure.
                                sdFilePtr->seek(((currentSDPacket-1)*sdFilePacketSize) + byteScan + sdFilePacketSize);
                                sdFilePtr->read(&syncByteCheck,1);

                                if (syncByteCheck == 0x55) {

                                    if ( ((byteScan-sdFilePacketSize)%sdFilePacketSize) == 0) {
                                        printf("Number of packets to skip in file: %d\n",(byteScan-sdFilePacketSize)/sdFilePacketSize);
                                        //qDebug() << "Number of bytes to skip:" << (byteScan-sdFilePacketSize);
                                        currentSDPacket = currentSDPacket + (byteScan-sdFilePacketSize)/sdFilePacketSize;                                
                                        recovered = true;
                                        break;
                                    }

                                } else {
                                    sdFilePtr->seek(((currentSDPacket-1)*sdFilePacketSize) + byteScan + 1);
                                }


                            }
                            if (sdFilePtr->atEnd()) {
                                printf("Reached end of file while searching for next sync byte.\n");
                                return false;
                            }
                            byteScan++;
                            packetsSinceLastSync++;
                        }

                    }
                }

                if (!recovered) {
                    //We never found the next sync byte.
                    printf("Error: not able to recover from bad packet.\n");
                    return false;
                }

            }

            if (recovered) {
                continue;
            }

            ltime = thistime;

            if (*sdBufferPtr == 1) {
                //The 0th bit is set in the flag byte, we have a sync value
                sdTimePtr = (uint32_t *)(sdFilePacketBuffer.data()+sdFilePacketSyncByteLoc);
                uint32_t sVal = *sdTimePtr;
                sVal = sVal & 0xFFFFFF;
                printf("SD sync: %d. Packet timestamp: %d.\n", sVal, ltime);
                fflush(stdout);

                if (sdSyncValues.length() > 1) {
                    if ( (!(sVal > lastSyncVal)) && (sVal < 30) && (sVal > 0)) {
                        printf("Warning: the SD syncs were reset. Starting merge from reset location.\n");
                        sdSyncValues.clear();
                    }
                    else if ( (!(sVal > lastSyncVal)) || ((sVal-lastSyncVal)>3000)) {
                        if ((!(sVal > lastSyncVal)) || (sVal-lastBadSyncVal) > 3000) { //This is to rescue syncs if there was a large drop out of syncs.
                            printf("Warning: bad sync value in SD file found. Ignoring value. Current value: %d. Last value: %d\n",sVal, lastSyncVal);
                            //lastSyncVal = sVal;
                            currentSDPacket++;
                            lastBadSyncVal = sVal;
                            continue;
                        }
                    }

                }

                sdSyncValues.push_back(sVal);
                lastSyncVal = sVal;

                sdTimePtr = (uint32_t *)(sdFilePacketBuffer.data()+sdFilePacketTimeByteLoc);
                sdTimeDuringSync.push_back(*sdTimePtr);
                sdPacketDuringSync.push_back(currentSDPacket);
            }

        } else {
            //qDebug() << "Could not read from file.";

        }


        currentSDPacket++;

        //sdFilePtr->

    }
    printf("%d sync values found on SD file.\n",sdSyncValues.length());

    currentSDTimeStamp = 0;
    lastSDTimeStamp = 0;

    //Seek back to begining of data
    sdFilePtr->seek(0);


    return true;

}

int DATExportHandler::readMergeConfig() {
    //Read the config .xml file



    QString configFileName = mergedConfFileName;
    //int filePos = 0;
    //startOffsetTime = 0;
    QString errorString = mergeConfigData.readTrodesConfig(mergedConfFileName);

    if (!errorString.isEmpty()) {
        printf("Error when parsing the final merge workspace. %s\n", errorString.toLocal8Bit().data());
        return -1;
    }



    //Transfer over session info that is in the environmental rec file
    mergeConfigData.globalConf.commitHeadStr = globalConf->commitHeadStr;
    mergeConfigData.globalConf.compileDate = globalConf->compileDate;
    mergeConfigData.globalConf.compileTime = globalConf->compileTime;
    mergeConfigData.globalConf.controllerFirmwareVersion = globalConf->controllerFirmwareVersion;
    mergeConfigData.globalConf.controllerSerialNumber = globalConf->controllerSerialNumber;

    //TODO: we will need to get info about the headstage from the SD file somehow.
    //mergeConfigData.globalConf.headstageFirmwareVersion =
    //mergeConfigData.globalConf.headstageSerialNumber =
    mergeConfigData.globalConf.qtVersion = globalConf->qtVersion;
    mergeConfigData.globalConf.systemTimeAtCreation = globalConf->systemTimeAtCreation;
    mergeConfigData.globalConf.timestampAtCreation = globalConf->timestampAtCreation;
    mergeConfigData.globalConf.trodesVersion = globalConf->trodesVersion;
    //mergeConfigData.globalConf.saveDisplayedChanOnly = false; //The headstage records all channels;


    if (hardwareConf->sourceSamplingRate != mergeConfigData.hardwareConf.sourceSamplingRate) {
        printf("Error: mismatch in sampling rates. Environmental sampling rate: %d. Logger sampling rate: %d\n",hardwareConf->sourceSamplingRate, mergeConfigData.hardwareConf.sourceSamplingRate);
        return -10;
    }




    //Sort the channels
    //std::sort(sortedChannelList.begin(),sortedChannelList.end());

    return 0; //return the position of the file where data begins

}

#include "datexporthandler.h"

#include "datexporthandler.h"
#include <algorithm>

//#include <iostream>

DATExportHandler::DATExportHandler(QStringList arguments):
    supportedDataCategories(0)
    ,embeddedWorkspace()
    ,loadedWorkspacePtr(nullptr)
{
    enableAVX = true;
    firstPacketRead = false;
    neuralDataNeedsProcessing = false;

    globalConf = &embeddedWorkspace.globalConf;
    hardwareConf = &embeddedWorkspace.hardwareConf;
    headerConf = &embeddedWorkspace.headerConf;
    streamConf = &embeddedWorkspace.streamConf;
    spikeConf = &embeddedWorkspace.spikeConf;
    moduleConf = &embeddedWorkspace.moduleConf;
    benchConfig = &embeddedWorkspace.benchConfig;
    conf_ptrs.globalConf = globalConf;
    conf_ptrs.hardwareConf = hardwareConf;
    conf_ptrs.headerConf = headerConf;
    conf_ptrs.streamConf = streamConf;
    conf_ptrs.spikeConf = spikeConf;
    conf_ptrs.moduleConf = moduleConf;
    conf_ptrs.benchConfig = benchConfig;

    numChannelsInFile = 0;
    packetTimeLocation = -1;
    packetHeaderSize = -1;
    filePacketSize = -1;
    defaultMenuEnabled = true;

    argumentList = arguments;

    _fileNameFound = false;
    helpMenuPrinted = false;
    argumentReadOk = true;
    _argumentsSupported = true;
    _configReadOk = true;

    filterLowPass_SpikeBand = -1;
    filterHighPass_SpikeBand = -1;
    filterLowPass_LFPBand = -1;
    override_Spike_Refs = false;
    override_LFPRefs = false;
    override_LFPRate = false;
    override_RAWRefs = false;
    override_useSpikeFilters = false;
    override_useLFPFilters = false;
    override_useNotchFilters = false;
    sortingMode = SpikeSortingGroupMode::BySortingGroup;
    //sortingMode = SpikeSortingGroupMode::ByNtrodes;
    //sortingMode = SpikeSortingGroupMode::OneGroup;
    useRefs = true;
    useLFPRefs = true;
    useRAWRefs = true;
    useSpikeFilters = true;
    useLFPFilters = true;
    useNotchFilters = true;
    maxGapSizeInterpolation = -1; //default number of points to interpolate over (-1 = inf)
    backupInterpCounter = 0;
    outputSamplingRate = 1500; //default of -1 means to use the same as the input
    invertSpikes = true;
    threshOverride = false;
    //recFileName = "";
    externalWorkspaceFile = "";
    outputFileName = "";
    outputDirectory = "";

    argumentsProcessed = 0;
    abortOnBackwardsTimestamp = false;

    paddingBytes = 0;

    requireRecFile = true;

    currentTimeStamp = 0;
    currentSysTimestamp = 0;
    nextSysTimeStampInFile = 0;
    lastSysTimeStampInFile = 0;
    lastTimeStampInFile = 0;
    firstRecProcessed = false;






    //supportedDataCategories = DataCategories::SpikeBand | DataCategories::SpikeEvents;



    parseArguments();

    /*
    Do custom argument checks here like this:
    if (ARGVAL != REQUIREMENT) {
        qDebug() << "Error: ....";
        argumentReadOk = false;
    }
    */


    /*if (outputSamplingRate != -1) {
        qDebug() << "Error: outputrate must stay unchanged for spike processing.";
        qDebug() << outputSamplingRate;
        argumentReadOk = false;
    }*/

    //Parse custom arguments
    //parseCustomArguments(argumentsProcessed);

    if (argumentsProcessed != argumentList.length()-1) {
        _argumentsSupported = false;
        return;
    }
}

DATExportHandler::~DATExportHandler()
{

}

bool DATExportHandler::fileNameFound() {
    return _fileNameFound;
}

bool DATExportHandler::argumentsOK() {
    return argumentReadOk;
}

bool DATExportHandler::argumentsSupported() {
    return _argumentsSupported;
}

bool DATExportHandler::wasHelpMenuDisplayed() {
    return helpMenuPrinted;
}

void DATExportHandler::writeHeaderInfo(QFile *filePtr, QString description) {
    QFileInfo fi(recFileName);
    QString infoLine;

    filePtr->write("<Start settings>\r\n");
    infoLine = QString("Description: ") + description;
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Byte_order: little endian\r\n");
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Original_file: ") + fi.fileName() + "\r\n";
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Clockrate: %1\r\n").arg(hardwareConf->sourceSamplingRate);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Trodes_version: %1\r\n").arg(globalConf->trodesVersion);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Compile_date: %1\r\n").arg(globalConf->compileDate);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Compile_time: %1\r\n").arg(globalConf->compileTime);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("QT_version: %1\r\n").arg(globalConf->qtVersion);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Commit_tag: %1\r\n").arg(globalConf->commitHeadStr);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Controller_firmware: %1\r\n").arg(globalConf->controllerFirmwareVersion);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Headstage_firmware: %1\r\n").arg(globalConf->headstageFirmwareVersion);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Controller_serialnum: %1\r\n").arg(globalConf->controllerSerialNumber);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Headstage_serialnum: %1\r\n").arg(globalConf->headstageSerialNumber);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("AutoSettle: %1\r\n").arg(globalConf->autoSettleOn);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("SmartRef: %1\r\n").arg(globalConf->smartRefOn);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Gyro: %1\r\n").arg(globalConf->gyroSensorOn);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Accelerometer: %1\r\n").arg(globalConf->accelSensorOn);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Magnetometer: %1\r\n").arg(globalConf->magSensorOn);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Time_offset: %1\r\n").arg(startOffsetTime);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("System_time_at_creation: %1\r\n").arg(globalConf->systemTimeAtCreation);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Timestamp_at_creation: %1\r\n").arg(globalConf->timestampAtCreation);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("First_timestamp: %1\r\n").arg(currentTimeStamp);
    filePtr->write(infoLine.toLocal8Bit());

}

bool DATExportHandler::fileConfigOK() {
    return _configReadOk;
}

void DATExportHandler::printHelpMenu() {
    if (defaultMenuEnabled) {

        printf("\n"
               "trodesexport <-h -v> <-spikes -lfp -spikeband -raw -stim -mountainsort -kilosort -time> -rec FILENAME <-optionflag1 optionvalue1> <optionflag2 optionvalue2> ... "
               "\n"
               "--Program information--\n"
               "-v or -version                  -- Prints the current executable's version information to the terminal. \n"
               "-h                              -- Prints the help menu. \n"
               "\n"
               "--Processing modes (multiples allowed)--\n"
               "-spikes                         -- Spike waveform export (spike snippets only). \n"
               "-lfp                            -- Continuous LFP band export. \n"
               "-spikeband                      -- Continuous spike band export. \n"
               "-raw                            -- Continuous raw band export. \n"
               "-stim                           -- Continuous stimulation band export for stimulation capable recording channels. \n"
               "-dio                            -- Digital IO channel state change export. \n"
               "-analogio                       -- Continuous analog IO export. \n"
               "-mountainsort                   -- One mountainsort file per ntrode (using raw band data) \n"
               "-kilosort                       -- One kilosort file per ntrode (using raw band data). \n"
               "-time                           -- Sequence of timestamps (raw sample numbers) in the file \n"
               "                                   using the set interpolation setting. File will also include \n"
               "                                   system timestamps if the recording has them. \n"
               "\n"
               "--Option flags--\n"
               "-rec <filename>                 -- Recording filename. Required. Muliple -rec <filename> entries can be used to append data in output.\n"
               "-output <basename>              -- The base name for the output files. If not specified, the base name of the first .rec file is used. \n"
               "-outputdirectory <directory>    -- A root directory to extract output files to (default is directory of .rec file). \n"
               "-reconfig <filename>            -- Use a different workspace than the one embedded in the recording file. \n"
               "-interp <integer>               -- Default is -1 (infinite). Maximum number of dropped packets to interpolate over (linear) Choose -1 for infinite.\n"
               "                                   A value of -1 is infinite. 0 is no interpolation. Note: for Neuropixels1.0 probes, LFP data is not interpolated.\n"
               "                                   Instead, the most recent values are used throughout the interp window. \n"
               "-sortingmode <0,1,2 or 3>       -- Used by mountainsort, kilosort, and raw modes. Can be a value of 0, 1, 2 (default), or 3. 0 combines channels into one file. \n"
               "                                   Channels in the file will be in the same order as that in the workspace, but won't be divided\n"
               "                                   into separate ntrodes. A value of 1 separates files by nTrodes. A value of 2 separates files by their designated sorting group. \n"
               "                                   A value of 3 separates files into single channels. \n"
               "-abortbaddata <1 or 0>          -- Whether or not to abort export if data appears corrupted. \n"
               "-paddingbytes <integer>         -- Used to add extra bytes to the expected packet size if an override is required\n\n\n"
               "\n"
               "--LFP data band--\n"
               "-lfplowpass <integer>           -- Low pass filter value for the LFP band. Overrides settings in workspace for all nTrodes.\n"
               "-lfpoutputrate <integer>        -- Override what is in the workspace. The LFP band output can have a sampling rate less than the full sampling rate.\n"
               "-uselfprefs <1 or 0>            -- Override what is in the workspace for lfp band reference on/off settings for all nTrodes.\n"
               "-uselfpfilters <1 or 0>         -- Override what is in the workspace for lfp filter on/off settings for all nTrodes. \n"
               "\n"
               "--Spike data band--\n"
               "-spikehighpass <integer>        -- High pass filter value for the spike band. Overrides settings in workspace for all nTrodes. \n"
               "-spikelowpass <integer>         -- Low pass filter value for the spike band. Overrides settings in workspace for all nTrodes.\n"
               "-invert <1 or 0>                -- (Default 1) Sets whether or not to invert spikes to go upward.\n"
               "-usespikerefs <1 or 0>          -- Override what is in the workspace for spike band reference on/off settings for all nTrodes.\n"
               "-usespikefilters <1 or 0>       -- Override what is in the workspace for spike filter on/off settings for all nTrodes. \n"
               "\n"
               "--Spike event processing--\n"
               "-thresh <integer>               -- Overrides the thresholds for each nTrode stored in the workspace and applies the\n"
               "                                   new threshold to all nTrodes.\n"
               "\n"
               "--Raw data band--\n"
               "-userawrefs <1 or 0>            -- Override what is in the workspace for raw band reference on/off settings for all nTrodes.\n"
               "\n");

    }
}

void DATExportHandler::displayVersionInfo() {
    printf("%s", qPrintable(GlobalConfiguration::getVersionInfo(false)));
    printf("\n");
}

void DATExportHandler::parseArguments() {



    QString filterLowPass_string = "";
    QString filterHighPass_string = "";
    QString one_file_string = "";
    QString maxGapSizeInterpolation_string = "";
    QString ref_string = "";
    QString use_lfp_ref_string = "";
    QString use_raw_ref_string = "";
    QString notch_string = "";
    QString outputrate_string = "";
    QString spikefilter_string = "";
    QString reconfig_string = "";
    QString outputname_string = "";
    QString outputdirectory_string = "";
    QString abort_string = "";
    QString padding_string = "";
    QString lfpfilter_string = "";
    QString lfpfilter_lowpass_string = "";

    //For spikes
    QString invertSpikes_string = "";
    QString threshOverride_string = "";


    int optionInd = 1;
    while (optionInd < argumentList.length()) {
        if ((argumentList.at(optionInd).compare("-rec",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            recFileNameList.push_back(argumentList.at(optionInd+1));
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-spikes",Qt::CaseInsensitive)==0) ) {
            supportedDataCategories |= DataCategories::SpikeEvents;
            argumentsProcessed = argumentsProcessed+1;
        } else if ((argumentList.at(optionInd).compare("-time",Qt::CaseInsensitive)==0) ) {
            supportedDataCategories |= DataCategories::Time;
            argumentsProcessed = argumentsProcessed+1;
        } else if ((argumentList.at(optionInd).compare("-lfp",Qt::CaseInsensitive)==0) ) {
            supportedDataCategories |= DataCategories::LfpBand;
            argumentsProcessed = argumentsProcessed+1;
        } else if ((argumentList.at(optionInd).compare("-raw",Qt::CaseInsensitive)==0) ) {
            supportedDataCategories |= DataCategories::RawBand;
            argumentsProcessed = argumentsProcessed+1;
        } else if ((argumentList.at(optionInd).compare("-spikeband",Qt::CaseInsensitive)==0) ) {
            supportedDataCategories |= DataCategories::SpikeBand;
            argumentsProcessed = argumentsProcessed+1;
        } else if ((argumentList.at(optionInd).compare("-dio",Qt::CaseInsensitive)==0) ) {
            supportedDataCategories |= DataCategories::DigitalIO;
            argumentsProcessed = argumentsProcessed+1;
        } else if ((argumentList.at(optionInd).compare("-analogio",Qt::CaseInsensitive)==0) ) {
            supportedDataCategories |= DataCategories::AnalogIO;
            argumentsProcessed = argumentsProcessed+1;
        } else if ((argumentList.at(optionInd).compare("-mountainsort",Qt::CaseInsensitive)==0) ) {
            supportedDataCategories |= DataCategories::MountainSort;
            argumentsProcessed = argumentsProcessed+1;
        }  else if ((argumentList.at(optionInd).compare("-kilosort",Qt::CaseInsensitive)==0) ) {
            supportedDataCategories |= DataCategories::KiloSort;
            argumentsProcessed = argumentsProcessed+1;
        }  else if ((argumentList.at(optionInd).compare("-stim",Qt::CaseInsensitive)==0) ) {
            supportedDataCategories |= DataCategories::Stimulation;
            argumentsProcessed = argumentsProcessed+1;
        }  else if ((argumentList.at(optionInd).compare("-sortingmode",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {

            one_file_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;

        }  else if ((argumentList.at(optionInd).compare("-spikehighpass",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            filterHighPass_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-spikelowpass",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            filterLowPass_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-lfplowpass",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            lfpfilter_lowpass_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-interp",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            maxGapSizeInterpolation_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-lfpoutputrate",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            outputrate_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-usespikerefs",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            ref_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-uselfprefs",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            use_lfp_ref_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-userawrefs",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            use_raw_ref_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-usespikefilters",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            spikefilter_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-uselfpfilters",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            lfpfilter_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        /*} else if ((argumentList.at(optionInd).compare("-usenotchfilters",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            notch_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;*/
        } else if ((argumentList.at(optionInd).compare("-reconfig",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            reconfig_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }  else if ((argumentList.at(optionInd).compare("-output",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            outputname_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }  else if ((argumentList.at(optionInd).compare("-outputdirectory",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            outputdirectory_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-abortbaddata",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            abort_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-paddingbytes",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            padding_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-version",Qt::CaseInsensitive)==0) || (argumentList.at(optionInd).compare("-v",Qt::CaseInsensitive)==0)) {
            displayVersionInfo();
            helpMenuPrinted = true; //we're going to consider version info the same as the help menu to terminate the main loop
            argumentsProcessed = argumentsProcessed+1;
            return;
        } else if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0)) {
            printHelpMenu();
            helpMenuPrinted = true;
            argumentsProcessed = argumentsProcessed+1;
            return;
        } else if ((argumentList.at(optionInd).compare("-invert",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            invertSpikes_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-thresh",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            threshOverride_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }
        optionInd++;
    }


    //Read the low pass filter argument
    if (!filterLowPass_string.isEmpty()) {
        bool ok1;

        filterLowPass_SpikeBand = filterLowPass_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Low pass filter could not be resolved into an integer.";
            //filterLowPass_SpikeBand = -1;
            argumentReadOk = false;
            return;
        } else {

            //setting the filter automatically overrides what is in the file and turns on all filters
            override_useSpikeFilters = true;
            useSpikeFilters = true;
            //useSpikeFilters = false; // This setting overrides the file's settings
            //useLFPFilters = false;
            //if (filterHighPass_SpikeBand == -1) {
                //Set the high pass filter to 0
            //    filterHighPass_SpikeBand = 0;
            //}
        }
    }

    //Read the high pass filter argument
    if (!filterHighPass_string.isEmpty()) {
        bool ok1;

        filterHighPass_SpikeBand = filterHighPass_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "High pass filter could not be resolved into an integer.";
            //filterHighPass_SpikeBand = -1;
            argumentReadOk = false;
            return;
        } else {

            //setting the filter automatically overrides what is in the file and turns on all filters
            override_useSpikeFilters = true;
            useSpikeFilters = true;

            //useSpikeFilters = false; // This setting overrides the file's settings
            //if (filterLowPass_SpikeBand == -1) {
                //Set the low pass filter to highest supported value
            //    filterLowPass_SpikeBand = 6000;
            //}
        }
    }

    //Read the low pass filter argument
    if (!lfpfilter_lowpass_string.isEmpty()) {
        bool ok1;

        filterLowPass_LFPBand = lfpfilter_lowpass_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Low pass filter for LFP could not be resolved into an integer.";
            argumentReadOk = false;
            return;
        } else {
            //setting the filter automatically overrides what is in the file and turns on all filters
            override_useLFPFilters = true;
            useLFPFilters = true;

        }
    }

    //TODO: notch filter settings

    if (!outputrate_string.isEmpty()) {
        bool ok1;

        outputSamplingRate = outputrate_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Output rate could not be resolved into an integer.";
            outputSamplingRate = -1;
            argumentReadOk = false;
            return;
        }

        if (((outputSamplingRate < 1)&&(outputSamplingRate != -1)) || (outputSamplingRate > 30000))  {
            qDebug() << "Outputrate must be between 1 and 30000. Or, it can be -1 to match the original rate.";
            outputSamplingRate = -1;
            argumentReadOk = false;
            return;
        } else {
            override_LFPRate = true;
        }
    }

    if (!one_file_string.isEmpty()) {
        bool ok1;
        int value;

        value = one_file_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "sortingmode number could not be resolved into a supported integer (0, 1, 2, or 3).";
            argumentReadOk = false;
            return;
        } else {
            if (value == 0) {
                sortingMode = SpikeSortingGroupMode::OneGroup;
            } else if (value == 1) {
                sortingMode = SpikeSortingGroupMode::ByNtrodes;
            } else if (value == 2) {
                sortingMode = SpikeSortingGroupMode::BySortingGroup;
            } else if (value == 3) {
                sortingMode = SpikeSortingGroupMode::SingleChannels;
            } else {
                qDebug() << "sortingmode number could not be resolved into a supported integer (0, 1, 2, or 3).";
                argumentReadOk = false;
                return;
            }
            override_Spike_Refs = true;
        }
    }

    if (!ref_string.isEmpty()) {
        bool ok1;

        useRefs = ref_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Reference mode could not be resolved into an integer (1 or 0).";
            useRefs = true;
            argumentReadOk = false;
            return;
        } else {
            override_Spike_Refs = true;
        }
    }

    if (!use_lfp_ref_string.isEmpty()) {
        bool ok1;

        useLFPRefs = use_lfp_ref_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "LFP reference mode could not be resolved into an integer (1 or 0).";

            argumentReadOk = false;
            return;
        } else {
            override_LFPRefs = true;
        }
    }

    if (!use_raw_ref_string.isEmpty()) {
        bool ok1;

        useRAWRefs = use_raw_ref_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "RAW reference mode could not be resolved into an integer (1 or 0).";

            argumentReadOk = false;
            return;
        } else {
            override_RAWRefs = true;
        }
    }

    if (!abort_string.isEmpty()) {
        bool ok1;

        abortOnBackwardsTimestamp = abort_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Abort mode could not be resolved into an integer (1 or 0).";
            abortOnBackwardsTimestamp = false;
            argumentReadOk = false;
            return;
        }
    }

    if (!spikefilter_string.isEmpty()) {
        bool ok1;

        useSpikeFilters = spikefilter_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Spikefilter mode could not be resolved into an integer (1 or 0).";
            useSpikeFilters = true;
            filterHighPass_SpikeBand = -1;
            filterLowPass_SpikeBand = -1;
            argumentReadOk = false;
            return;
        } else {
            override_useSpikeFilters = true;
        }
    }

    if (!lfpfilter_string.isEmpty()) {
        bool ok1;

        useLFPFilters = lfpfilter_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "LFP filter mode could not be resolved into an integer (1 or 0).";
            useLFPFilters = true;
            argumentReadOk = false;
            return;
        } else {
            override_useLFPFilters = true;
        }
    }

    if (!notch_string.isEmpty()) {
        bool ok1;

        useNotchFilters = notch_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Notch filter mode could not be resolved into an integer (1 or 0).";

            argumentReadOk = false;
            return;
        } else {
            override_useNotchFilters = true;
        }
    }

    //Read the interpolation argument
    if (!maxGapSizeInterpolation_string.isEmpty()) {
        bool ok1;

        maxGapSizeInterpolation = maxGapSizeInterpolation_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Max interpolation gap value could not be resolved into an integer.";
            maxGapSizeInterpolation = -1;
            argumentReadOk = false;
            return;
        }
    }

    //Read the high pass filter argument
    if (!padding_string.isEmpty()) {
        bool ok1;

        paddingBytes = padding_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Padding could not be resolved into an integer.";
            paddingBytes = 0;
            argumentReadOk = false;
            return;
        } else {
            if (paddingBytes < 0) {
                qDebug() << "Padding must be an integer greater than or equal to 0";
                paddingBytes = 0;
                argumentReadOk = false;
                return;
            }

        }
    }

    if (!reconfig_string.isEmpty()) {

        externalWorkspaceFile = reconfig_string;
        QFileInfo fi(externalWorkspaceFile);
        if (!fi.exists()) {
            qDebug() << "File could not be found: " << externalWorkspaceFile;
            argumentReadOk = false;
        }

    }

    //Read the invert argument
    if (!invertSpikes_string.isEmpty()) {
        bool ok1;

        invertSpikes = invertSpikes_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Invert spikes argument could not be resolved into an integer. Use 0 (false) or 1 (true)";
            argumentReadOk = false;
            return;
        }
    }

    //Read the thresh argument
    if (!threshOverride_string.isEmpty()) {
        bool ok1;

        threshOverrideVal = threshOverride_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Thresh argument could not be resolved into an integer.";
            argumentReadOk = false;
            return;
        } else if ((threshOverrideVal < 10) || (threshOverrideVal > 2000)) {
            qDebug() << "A threshold override value of" << threshOverride << "is not allowed. Value must be >= 10 uV and <= 2000 uV.";
            argumentReadOk = false;
            return;
        } else {
            threshOverride = true; //when set to true, the threshOverrideVal is used for all nTrodes
            //threshOverrideVal_rangeConvert = (threshOverrideVal * 65536) / AD_CONVERSION_FACTOR; //convert the threshold value to the full-range value
        }
    }


    if (!outputname_string.isEmpty()) {

        outputFileName = outputname_string;
    }

    if (!outputdirectory_string.isEmpty()) {
        if(outputdirectory_string.endsWith('/'))
            outputDirectory = outputdirectory_string;
        else
            outputDirectory = outputdirectory_string.append('/');
    }

    if (recFileNameList.isEmpty()) {
        if (requireRecFile) {
            qDebug() << "No filename(s) given.  Must include -rec <filename> in arguments.";
            argumentReadOk = false;
            return;
        }
    } else {
        //Make sure the first file exists
        recFileName = recFileNameList.at(0);

        QFileInfo fi(recFileName);

        if (fi.exists()) {
            _fileNameFound = true;
            if (readConfig() < 0) {
                _configReadOk = false;
            }
        } else {
            qDebug() << "File could not be found: " << recFileName;
            argumentReadOk = false;
        }

    }

    if (conf_ptrs.spikeConf->deviceType == "neuropixels1") {

        if (maxGapSizeInterpolation != 0) {
            qDebug() << "Error: Workspaces using neuropixels1 does not support interpolation during export. Setting -interp flag to 0.";
            maxGapSizeInterpolation = 0;
            //argumentReadOk = false;
        }

    }

    if (supportedDataCategories == 0) {
        qDebug() << "No processing modes were provided. Include at least one in the arguments.";
        argumentReadOk = false;
    }


}

bool DATExportHandler::getNextPacket() {
    bool debugOutput = false;

    if (neuralDataNeedsProcessing && neuralDataHandler->getSamplesLeftToInterpolate() > 0) {
        if (debugOutput) std::cerr  << "Interpolating neural sample\n";
        neuralDataHandler->stepPreviousSample();

        currentTimeStamp++;
        return true;

    } else if (backupInterpCounter > 0) { //Interpolation counter if no neural data is being processed

        backupInterpCounter--;
        currentTimeStamp++;

        return true;

    } else {
        if (debugOutput) std::cerr  << "Reading from file\n";
        //We have no gaps that need to be interpolated over, so we now read the next packet in the file
        //Read in a packet of data to make sure everything looks good
        if (!(filePtr->read(buffer.data(),filePacketSize) == filePacketSize)) {
            //If we can't read, we have reached the end of the file
            return false;
        }
        bool packetOk = true;
        bufferPtr = buffer.data();
        if (*bufferPtr != 0x55) {

            packetOk = false;
        }

        if (debugOutput) std::cerr  << "Processing packet time\n";
        //Find the time stamp
        bufferPtr = buffer.data()+packetTimeLocation;
        tPtr = (uint32_t *)(bufferPtr);
        lastTimeStampInFile = nextTimeStampInFile;
        nextTimeStampInFile = *tPtr+startOffsetTime;
        if (!packetOk && ((nextTimeStampInFile-lastTimeStampInFile) != 1) )  {
            qDebug() << "WARNING: Sync byte not in expected location and timestamp read is not incremented by one. Timestamp read from file:" << nextTimeStampInFile;
        }
        if (hardwareConf->sysTimeIncluded) {
            qint64 *sysTimePtr = (qint64 *)(bufferPtr-8);
            lastSysTimeStampInFile = nextSysTimeStampInFile;
            nextSysTimeStampInFile = *sysTimePtr;
        }
        if (!firstPacketRead) {
            lastTimeStampInFile = nextTimeStampInFile;
            firstPacketRead = true;
        }

        if (debugOutput) std::cerr  << "Processing packet neural data\n";
        if (neuralDataNeedsProcessing) {
            if (channelPacketLocations.length() > 0) {

                bufferPtr = buffer.data()+channelPacketLocations[0];

                tempDataPtr = (int16_t*)(bufferPtr);

                calculateCARDataInPacket(tempDataPtr);

                neuralDataHandler->processNextSamples(nextTimeStampInFile,tempDataPtr,CARData.data(),buffer.data());

            }
        } else {
            //The number of expected data points between this time stamp and the last (1 if no data is missing)
            int64_t numberOfPointsToProcess = (nextTimeStampInFile-lastTimeStampInFile);

            if (numberOfPointsToProcess > 1) {
                //At least one packet is missing.
                if ((maxGapSizeInterpolation > -1) && (numberOfPointsToProcess > maxGapSizeInterpolation)) {
                    //The gap in data is too large to interpolate over, so we do not interpolate over the gap
                    /*QString ss = QString(":%1").arg((lastTimeStampInFile/(conf_ptrs.hardwareConf->sourceSamplingRate))%60,2,10,QLatin1Char('0'));
                    QString mm = QString(":%1").arg((lastTimeStampInFile/(conf_ptrs.hardwareConf->sourceSamplingRate*60))%60,2,10,QLatin1Char('0'));
                    QString hh = QString("%1").arg(lastTimeStampInFile/(conf_ptrs.hardwareConf->sourceSamplingRate*60*60),2,10,QLatin1Char('0'));
                    QString dateString = hh+mm+ss;
                    qDebug() << "A gap of" << nextTimeStampInFile-lastTimeStampInFile-1 << "packets occured in the file. Last timestamp: " << lastTimeStampInFile << "(Clock is: " << dateString << ")" ;*/

                    backupInterpCounter = 0;
                } else {
                    //We are going to interpolate over the gap

                    backupInterpCounter = numberOfPointsToProcess;
                }
            }

        }


        if (debugOutput) std::cerr  << "Analyzing timestamp jumps\n";

        //The number of expected data points between this time stamp and the last (1 if no data is missing)
        int64_t timestampJump = ((int64_t)nextTimeStampInFile-(int64_t)lastTimeStampInFile)-1;
        if (timestampJump < -1) {
            qDebug() << "Error with file: backwards timestamp at time " << nextTimeStampInFile << "and the last timestamp was" << lastTimeStampInFile;
            if (abortOnBackwardsTimestamp) {
                qDebug() << "Aborting";
                return false;
            }
        }

        if (backupInterpCounter > 0) {
            //Interpolation needed, so we recursively call this function

            qDebug() << "Interpolating data during gap of" << backupInterpCounter << "points after timestamp" << lastTimeStampInFile;
            //numberOfPacketsToInterpolate_done = 0;
            return getNextPacket();
        } else if (neuralDataHandler->getSamplesLeftToInterpolate() > 0) {
            //Interpolation needed, so we recursively call this function

            qDebug() << "Interpolating data during gap of" << neuralDataHandler->getSamplesLeftToInterpolate()-1 << "points after timestamp" << lastTimeStampInFile;
            //numberOfPacketsToInterpolate_done = 0;
            return getNextPacket();
        //} else if (neuralDataHandler->gapOccured()) {
        } else if (timestampJump > 1) {

            QString ss = QString(":%1").arg((lastTimeStampInFile/(conf_ptrs.hardwareConf->sourceSamplingRate))%60,2,10,QLatin1Char('0'));
            QString mm = QString(":%1").arg((lastTimeStampInFile/(conf_ptrs.hardwareConf->sourceSamplingRate*60))%60,2,10,QLatin1Char('0'));
            QString hh = QString("%1").arg(lastTimeStampInFile/(conf_ptrs.hardwareConf->sourceSamplingRate*60*60),2,10,QLatin1Char('0'));


            QString dateString = hh+mm+ss;

            qDebug() << "A gap of" << nextTimeStampInFile-lastTimeStampInFile-1 << "packets occured in the file. Last timestamp: " << lastTimeStampInFile << "(Clock is: " << dateString << ")" ;
        }

        currentTimeStamp = nextTimeStampInFile; //No interpolation needed, so we update the timestamp using that value in the file
        currentSysTimestamp = nextSysTimeStampInFile;
    }



    return true;



}


void DATExportHandler::createFilters() {

    //Default filter creation-- use the filters that are in the config for each nTrode
    //Redefine in inheriting class if different behavior is required
    for (int i=0; i < channelPacketLocations.length(); i++) {
        channelFilters.push_back(new BesselFilter);
        channelFilters.last()->setSamplingRate(hardwareConf->sourceSamplingRate);

        if (useSpikeFilters) {
            //The user has given new filters to override the file's settings
            filterOn[i] = true;
            channelFilters.last()->setFilterRange(filterHighPass_SpikeBand,filterLowPass_SpikeBand);
        } //else if (useSpikeFilters && highPassFilters[i] != -1 && lowPassFilters[i] != -1) {
            //Use the spike filter settings in the file
          //  channelFilters.last()->setFilterRange(highPassFilters[i],lowPassFilters[i]);}
        if (useLFPFilters) {
            //Use the lfp filter settings in the file
            lfpFilterOn[i] = true;

            //Should be lfp filter
            channelFilters.last()->setFilterRange(0,lfpLowPassFilters[i]);
        }else {
            //Turn off filters
            filterOn[i] = false;
        }
    }
}

void DATExportHandler::setDefaultMenuEnabled(bool enabled) {
    defaultMenuEnabled = enabled;
}

bool DATExportHandler::createTimeOffsetFile() {

    QString fileTypeExtension = "timestampoffset";

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+"."+fileTypeExtension+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating" << fileTypeExtension <<  "directory" << saveLocation;
            return false;
        }
    }


    QString infoLine;


    //Create an output file
    //*****************************************
    QFileInfo fi(recFileName);
    QString recName = fi.completeBaseName();


    QFile file;
    file.setFileName(saveLocation+recName+QString(".timestampoffset.txt"));
    if (!file.open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating timestamp offset file.";
        return false;
    }

    infoLine = QString("%1\r\n").arg(startOffsetTime);
    file.write(infoLine.toLocal8Bit());

    file.close();
    return true;

}

bool DATExportHandler::openInputFile() {
    filePtr = new QFile;
    filePtr->setFileName(recFileName);
    dataStartLocBytes = findEndOfConfigSection(recFileName);
    packetsRead = 0;

    //open the raw data file
    if (!filePtr->open(QIODevice::ReadOnly)) {
        delete filePtr;
        qDebug() << "Error: could not open file for reading.";
        return false;
    }

    buffer.resize(filePacketSize*2); //we make the input buffer the size of two packets
    currentPacket.resize(filePacketSize*2);
    lastReadPacket.resize(filePacketSize*2);
    dataLastTimePoint.resize(channelPacketLocations.size()); //Stores the last data point.
    dataLastTimePoint.fill(0);
    pointsSinceLastLog=-1;

    //----------------------------------

    //skip past the config header
    if (!filePtr->seek(dataStartLocBytes)) {
        delete filePtr;
        qDebug() << "Error seeking in file";
        return false;
    }

    //Read in a packet of data to make sure everything looks good
    if (!(filePtr->read(buffer.data(),filePacketSize) == filePacketSize)) {
        delete filePtr;
        qDebug() << "Error: could not read from file";
        return false;
    }

    if (firstRecProcessed) {

        int readCode = readNextConfig(recFileName);
        if (readCode < 0) {
            return false;
        }
    }


    //Look in the trodesComments file to see if time was reset.  If so, create a time offset.
    QFileInfo fi(recFileName);
    QString commentsCheckName = fi.absolutePath() + "/"+ fi.completeBaseName() + ".trodesComments";
    QFileInfo commentsFileInfo(commentsCheckName);
    bool offsetDefined = false;
    startOffsetTime = 0;
    if (commentsFileInfo.exists()) {
        QFile commentsFile;
        commentsFile.setFileName(commentsFileInfo.absoluteFilePath());
        if (commentsFile.open(QIODevice::ReadOnly)) {
            QString line;

            while (!commentsFile.atEnd()) {
                line += commentsFile.readLine();

                int resetKeywordPos = line.indexOf("time reset");
                if (resetKeywordPos > -1) {

                    qDebug() << "Creating offset for file.";
                    startOffsetTime = currentTimeStamp+1;
                    offsetDefined = true;

                    break;

                }
                line = "";
            }
        }

    }


    //Find first time stamp
    bufferPtr = buffer.data()+packetTimeLocation;
    tPtr = (uint32_t *)(bufferPtr);

    if (firstRecProcessed && !offsetDefined) {
        qDebug() << "Calculating time offset for appended file...";
        if  (globalConf->systemTimeAtCreation == systemTimeAtCreation) {
            //Same file-- this happens when a single environmental file was merged with multiple sequential logger files
            qDebug() << "According to the date and time when the file was created, it was created at the same time as the previous file. No offset will be applied to the timestamps.";
           startOffsetTime = 0;
        } else if ((globalConf->systemTimeAtCreation > 0) && (globalConf->systemTimeAtCreation < systemTimeAtCreation)) {
            qDebug() << "WARNING: this file was created BEFORE the first file. Stitching files anyway with no time offset.";
            startOffsetTime = 0;
        } else if (globalConf->systemTimeAtCreation < 1) {
            qDebug() << "WARNING: this file does not contain information about the date and time when the file was created, so if the system clock was reset there is no way of knowing how much time elapsed between the two files. No offset will be applied.";
            startOffsetTime = 0;
        } else if ((globalConf->systemTimeAtCreation > 0) && (globalConf->systemTimeAtCreation > systemTimeAtCreation)) {
            //The user is stitching together two or more rec files, and the next rec file has a time stamp reset.
            //We need to use system time to calculate the offset.
            //qDebug() << "Appending files using system time to calculate offset.";
            //Make sure the creation time of the next file is after the creation time of the first file.


            //Estimated hardware timestamp based on the computer clock difference
            int64_t tempOffset = timestampAtCreation+(int64_t)((globalConf->systemTimeAtCreation-systemTimeAtCreation) * ((double)hardwareConf->sourceSamplingRate / 1000.0));

            //if the estimated time is within 1 sec of the hardware timestamp, we use the hardware timestamp
            int timeTolerance = hardwareConf->sourceSamplingRate;
            if ((globalConf->timestampAtCreation > lastTimeStampInFile) &&
                (((globalConf->timestampAtCreation > tempOffset) && ((globalConf->timestampAtCreation-tempOffset) < timeTolerance)) ||
                ((globalConf->timestampAtCreation < tempOffset) && ((tempOffset-globalConf->timestampAtCreation) < timeTolerance))) ) {
                //The hardware clock in the file is later than the estimated time. The stream probably never ended, so we use the hardware clock.
                //qDebug() << "The hardware timestamp is very close to the esimated timestamp based on computer clock. Using actual hardware timestamp in file";
                qDebug() << "There was a" << (double)(globalConf->timestampAtCreation-lastTimeStampInFile)/(double)hardwareConf->sourceSamplingRate << "second period from the end of the last file to when this file was created.";
                //qDebug() << globalConf->timestampAtCreation << timestampAtCreation << globalConf->systemTimeAtCreation-systemTimeAtCreation << tempOffset << lastTimeStampInFile;
                startOffsetTime = 0;
            } else {
                tempOffset = tempOffset - globalConf->timestampAtCreation;

                if ((tempOffset + globalConf->timestampAtCreation) > lastTimeStampInFile) {
                    qDebug() << "Created new timestamps based on computer's time when file was created. There was a" << (double)(tempOffset+ globalConf->timestampAtCreation-lastTimeStampInFile)/(double)hardwareConf->sourceSamplingRate << "second period from the end of the last file to when this file was created.";
                    startOffsetTime = tempOffset;
                    qDebug() << "Offset:" << startOffsetTime;
                } else {
                    qDebug() << "ERROR: system clock when file created is before the end of the last file. Stitching files anyway, ignoring system time.";

                    startOffsetTime = 0;
                }

            }

        }

    }

    currentTimeStamp = *tPtr + startOffsetTime;
    lastTimeStampInFile = currentTimeStamp-1;

    //Seek back to begining of data
    filePtr->seek(dataStartLocBytes);

    //Initialize info about progress
    //progressMarker = filePtr->size()/100;
    progressMarkerTrigger = 30000*60; //One status print per minute of data (at 30 kHz sampling)
    progressMarkerCounter = 0;
    currentProgressMarker = 0.0;
    //lastProgressMod = 0;
    /*if (progressMarker == 0) {
        progressMarker = 1;
    }*/

    //Calculate the number of points to skip to achieve the output rate
    if (override_LFPRate) {
        if (outputSamplingRate == -1) {
            decimation = 1;
        } else {
            decimation = hardwareConf->sourceSamplingRate/outputSamplingRate;
            if (supportedDataCategories & DataCategories::LfpBand) {
                //qDebug() << "LFP output rate downsampled to" << outputSamplingRate << "samples per sec.";
            }
        }
    } else {
        //use the decimation parameter in the workspace
        decimation = hardwareConf->lfpSubsamplingInterval;
    }

    firstRecProcessed = true; //if multiple files are being stitched, this flag will be true for the next file.


    return true;

}

void DATExportHandler::printProgress() {
    //Print progress (based on location in input file)
    progressMarkerCounter++;

    if (progressMarkerCounter > progressMarkerTrigger) {
        progressMarkerCounter = 0;
        //printf("\r%d%%",currentProgressMarker);
        //fflush(stdout);



        currentProgressMarker = 100.0*((double)filePtr->pos()/(double)filePtr->size());
        //std::cerr.precision(dbl::m
        std::cerr  << "\r" << std::setprecision(4) << currentProgressMarker << "%";
        //currentProgressMarker+=10;
    }
    //lastProgressMod = (filePtr->pos()%progressMarkerTrigger);
}

int DATExportHandler::findEndOfConfigSection(QString configFileName) {


    QFile file;
    int filePos = -1;

    if (!configFileName.isEmpty()) {
        file.setFileName(configFileName);
        if (!file.open(QIODevice::ReadOnly)) {
            return -1;
        }

        QFileInfo fi(configFileName);
        QString ext = fi.suffix();
        if (ext.compare("rec") == 0) {
            //this is a rec file with a configuration in the header

            QString configContent;
            QString configLine;
            bool foundEndOfConfig = false;

            while (file.pos() < 5000000) {
                configLine += file.readLine();
                configContent += configLine;
                if (configLine.indexOf("</Configuration>") > -1) {
                    foundEndOfConfig = true;
                    break;
                }
                configLine = "";
            }

            if (foundEndOfConfig) {
                filePos = file.pos();
            }
        }
        file.close();
        return filePos;
    }
    return -1;
}

int DATExportHandler::readConfig() {
    //Read the config .xml file

    bool debugStatementsOn = false;
    QString configFileName = recFileName;
    startOffsetTime = 0;

    QFileInfo fI(configFileName);
    QString baseName = fI.completeBaseName();

    QString workspaceCheckName = fI.absolutePath() + "/"+ baseName + ".trodesconf";
    QFileInfo workspaceFile(workspaceCheckName);

    bool externalChannelSettings = false;

    QDomDocument doc("TrodesConf"); //Channel settings. Can be reconfigured with external workspce
    QDomDocument sessionAttributes("TrodesConf"); //Settings that were logged at the time of the recording. Can only be retrived from the rec file

    if (!externalWorkspaceFile.isEmpty()) {
        //An external workspace file should be used
        //this is a normal xml-based config file

        QString errString = externalWorkspace.readTrodesConfig(externalWorkspaceFile);
        if (!errString.isEmpty()) {
            qDebug() << "There was an error parsing" << externalWorkspaceFile << "." << errString;
            return -1;
        }
        //filePos = findEndOfConfigSection(recFileName);
        externalChannelSettings = true;

    } else if (workspaceFile.exists()) {
        //Check if there is a .trodeconf file with the same name in the same directory.
        //If so, use that.

        qDebug() << "Using the following workspace file: " << workspaceFile.fileName();


        externalWorkspaceFile = workspaceFile.absoluteFilePath();
        QString errString = externalWorkspace.readTrodesConfig(workspaceFile.absoluteFilePath());
        if (!errString.isEmpty()) {
            qDebug() << "There was an error parsing" << externalWorkspaceFile << "." << errString;
            return -1;
        }

        externalChannelSettings = true;
    }


    //Load in workspace embedded in the rec file

    QString errorString = embeddedWorkspace.readTrodesConfig(recFileName);
    if (!errorString.isEmpty()) {
        qDebug() << "There is an issue reading in the workspace embedded in the rec file: " << errorString;
        return -1;
    }


    if (!externalChannelSettings) {
        //No external workspace, so load all settings from those embedded in the rec file
        globalConf = &embeddedWorkspace.globalConf;
        hardwareConf = &embeddedWorkspace.hardwareConf;
        headerConf = &embeddedWorkspace.headerConf;
        streamConf = &embeddedWorkspace.streamConf;
        spikeConf = &embeddedWorkspace.spikeConf;
        moduleConf = &embeddedWorkspace.moduleConf;
        benchConfig = &embeddedWorkspace.benchConfig;
        loadedWorkspacePtr = &embeddedWorkspace;
        conf_ptrs.globalConf = globalConf;
        conf_ptrs.hardwareConf = hardwareConf;
        conf_ptrs.headerConf = headerConf;
        conf_ptrs.streamConf = streamConf;
        conf_ptrs.spikeConf = spikeConf;
        conf_ptrs.moduleConf = moduleConf;
        conf_ptrs.benchConfig = benchConfig;
    } else {
        //The global settings are still grabbed from the rec file, but everything else comes from the external file.
        globalConf = &embeddedWorkspace.globalConf;
        hardwareConf = &externalWorkspace.hardwareConf;
        headerConf = &externalWorkspace.headerConf;
        streamConf = &externalWorkspace.streamConf;
        spikeConf = &externalWorkspace.spikeConf;
        moduleConf = &externalWorkspace.moduleConf;
        benchConfig = &externalWorkspace.benchConfig;
        loadedWorkspacePtr = &externalWorkspace;
        conf_ptrs.globalConf = globalConf;
        conf_ptrs.hardwareConf = hardwareConf;
        conf_ptrs.headerConf = headerConf;
        conf_ptrs.streamConf = streamConf;
        conf_ptrs.spikeConf = spikeConf;
        conf_ptrs.moduleConf = moduleConf;
        conf_ptrs.benchConfig = benchConfig;

    }

    systemTimeAtCreation = globalConf->systemTimeAtCreation;
    timestampAtCreation = globalConf->timestampAtCreation;

    dataStartLocBytes = findEndOfConfigSection(recFileName);

    if (debugStatementsOn) {
        qDebug() << "[readConfig] Got to the end with start byte" << dataStartLocBytes;
    }
    return dataStartLocBytes; //return the position of the file where data begins

}


int DATExportHandler::readNextConfig(QString configFileName) {
    //Read the next config .xml file.  Since critical info had already been calulated from the first file, we only need to
    //get info unique to this file.

    int filePos = 0;
    //startOffsetTime = 0;



    QFileInfo fI(configFileName);
    QString baseName = fI.completeBaseName();

    QString workspaceCheckName = fI.absolutePath() + "/"+ baseName + ".trodesconf";
    QFileInfo workspaceFile(workspaceCheckName);



    QDomDocument doc("TrodesConf");


    if (!externalWorkspaceFile.isEmpty()) {
        //An external workspace file should be used
        //this is a normal xml-based config file

        QFile file;
        file.setFileName(externalWorkspaceFile);
        if (!file.open(QIODevice::ReadOnly)) {
            qDebug() << QString("File %1 not found").arg(externalWorkspaceFile);
            return -1;
        }

        if (!doc.setContent(&file)) {
            file.close();
            qDebug("XML didn't read properly in external workspace file.");
            return -1;
        }
        filePos = findEndOfConfigSection(recFileName);

    } else if (workspaceFile.exists()) {
        //Check if there is a .trodeconf file with the same name in the same directory.
        //If so, use that.

        qDebug() << "Using the following workspace file: " << workspaceFile.fileName();
        QFile file;
        file.setFileName(workspaceFile.absoluteFilePath());
        if (!file.open(QIODevice::ReadOnly)) {
            qDebug() << QString("File %1 not found").arg(workspaceFile.absoluteFilePath());
            return -1;
        }

        if (!doc.setContent(&file)) {
            file.close();
            qDebug("XML didn't read properly in external workspace file.");
            return -1;
        }
        filePos = findEndOfConfigSection(recFileName);
    } else if (!configFileName.isEmpty()) {
        //If the other conditions are not met, use the settings embedded in the recording file.
        QFile file;
//        bool isRecFile = false;
        file.setFileName(configFileName);
        if (!file.open(QIODevice::ReadOnly)) {
            qDebug() << QString("File %1 not found").arg(configFileName);
            return -1;
        }

        QFileInfo fi(configFileName);
        QString ext = fi.suffix();

        if (ext.compare("rec") == 0) {
            //this is a rec file with a configuration in the header
//            isRecFile = true;
            QString configContent;
            QString configLine;
            bool foundEndOfConfig = false;

            while (!file.atEnd()) {
                configLine += file.readLine();
                configContent += configLine;
                if (configLine.indexOf("</Configuration>") > -1) {
                    //qDebug() << "End of config header found at " << file.pos();
                    foundEndOfConfig = true;
                    break;
                }
                configLine = "";
            }

            if (foundEndOfConfig) {
                filePos = file.pos();
                if ((externalWorkspaceFile.isEmpty()) && (!doc.setContent(configContent))) {
                    file.close();
                    qDebug("Config header in didn't read properly.");
                    return -1;
                }
            }
        }
        file.close();
    }

    //Load in workspace
    QString errorString = embeddedWorkspace.readTrodesConfig(doc);
    if (!errorString.isEmpty()) {
        qDebug() << "There is an issue reading in the workspace: " << errorString;
        return -1;
    }

    globalConf = &embeddedWorkspace.globalConf; //We only care about the global info, since the rest was already loaded before


    dataStartLocBytes = filePos;
    return filePos; //return the position of the file where data begins

}



void DATExportHandler::calculateChannelInfo() {

    QList<int> devicePrefOrder;

    for (int i=0;i<hardwareConf->devices.length();i++) {

        int insertInd = 0;
        for (int sortInd= 0; sortInd < sortedDeviceList.length(); sortInd++) {
            if (hardwareConf->devices[i].packetOrderPreference < sortedDeviceList[sortInd]) {
                break;
            } else {
                insertInd++;
            }
        }
        devicePrefOrder.insert(insertInd,hardwareConf->devices[i].packetOrderPreference);
        sortedDeviceList.insert(insertInd,i);
    }

    if(hardwareConf->sysTimeIncluded) {
        packetHeaderSize = (2*hardwareConf->headerSize)+12; //Aux info plus 4-byte timestamp and 8-byte system clock
    } else {
        packetHeaderSize = (2*hardwareConf->headerSize)+4; //Aux info plus 4-byte timestamp
    }

    packetTimeLocation = packetHeaderSize-4; //timestamp are always the last 4 bytes of the packet header

    QList<int> sortedChannelList;
    //Gather all HW channels

    for (int n = 0; n < spikeConf->ntrodes.length(); n++) {

        nTrodeIndexList.push_back(n);

        //spikeDetectors.push_back(new ThresholdSpikeDetector(this,n,loadedWorkspacePtr));

        if (override_Spike_Refs) {
            if (useRefs) {
                //User wants to turn on all refs for the spike band, overriding settings in the file
                spikeConf->ntrodes[n]->refOn = true;
            } else {
                //User wants to to turn off all referencing
                spikeConf->ntrodes[n]->refOn = false;
            }
        }

        if (!override_LFPRate) {

        }

        if (override_LFPRefs) {
            if (useLFPRefs) {
                //User wants to turn on all refs for the lfp band, overriding settings in the file
                spikeConf->ntrodes[n]->lfpRefOn = true;
            } else {
                //User wants to to turn off all referencing  for lfp
                spikeConf->ntrodes[n]->lfpRefOn = false;
            }
        }

        if (override_RAWRefs) {
            if (useRAWRefs) {
                //User wants to turn on all refs for the lfp band, overriding settings in the file
                spikeConf->ntrodes[n]->rawRefOn = true;
            } else {
                //User wants to to turn off all referencing  for lfp
                spikeConf->ntrodes[n]->rawRefOn = false;
            }
        }

        //TODO: override_rawRefs...

        if (override_useSpikeFilters) {

            if (useSpikeFilters) {
                //User wants to turn on all filters for the spike band, overriding settings in the file
                spikeConf->ntrodes[n]->filterOn = true;
            } else {
                //User wants to to turn off all spike filters
                spikeConf->ntrodes[n]->filterOn = false;
            }

            if (filterLowPass_SpikeBand != -1) {
                spikeConf->ntrodes[n]->highFilter = filterLowPass_SpikeBand;
            }
            if (filterHighPass_SpikeBand != -1) {
                spikeConf->ntrodes[n]->lowFilter = filterHighPass_SpikeBand;
            }
        }

        if (override_useLFPFilters) {
            if (useLFPFilters) {
                //User wants to turn on all filters for the lfp band, overriding settings in the file
                spikeConf->ntrodes[n]->lfpFilterOn = true;
            } else {
                //User wants to to turn off all lfp filters
                spikeConf->ntrodes[n]->lfpFilterOn = false;
            }
            if (filterLowPass_LFPBand != -1) {
                spikeConf->ntrodes[n]->lfpHighFilter = filterLowPass_LFPBand;
            }
        }

        if (override_useNotchFilters) {
            if (useNotchFilters) {
                //User wants to turn on all notch filters, overriding settings in the file
                spikeConf->ntrodes[n]->notchFilterOn = true;
            } else {
                //User wants to to turn off all notch filters
                spikeConf->ntrodes[n]->notchFilterOn = false;
            }
        }

        //If a common threshhold override was provided, change all ntrodes in workspace
        if (threshOverride) {

                for (int chInd = 0; chInd < conf_ptrs.spikeConf->ntrodes[n]->hw_chan.length();chInd++) {
                    conf_ptrs.spikeConf->ntrodes[n]->thresh[chInd] = threshOverrideVal;
                    conf_ptrs.spikeConf->ntrodes[n]->thresh_rangeconvert[chInd] = ((double)threshOverrideVal/conf_ptrs.spikeConf->ntrodes[n]->spike_scaling_to_uV);
                }

        }

        //nTrodeSettings.push_back(nInf);
        for (int c = 0; c < spikeConf->ntrodes[n]->hw_chan.length(); c++) {
            bool channelalreadyused = false;

            int tempHWRead = spikeConf->ntrodes[n]->hw_chan[c];
            //make sure there is not a repeat channel
            for (int ch=0;ch<sortedChannelList.length();ch++) {
                if (sortedChannelList[ch]==tempHWRead) {
                    channelalreadyused = true;
                }
            }
            //qDebug() << "Read" << spikeConf->ntrodes[n]->unconverted_hw_chan[c] << tempHWRead;
            if (!channelalreadyused) {
                sortedChannelList.push_back(tempHWRead);
            }

        }
    }

    //Sort the channels
    std::sort(sortedChannelList.begin(),sortedChannelList.end());

    //Remember how many channels are actually saved in the file
    //(may be different from how many channels came from recording hardware)

    if (globalConf->saveDisplayedChanOnly) {
        //numChannelsInFile = sortedChannelList.length();
        numChannelsInFile = streamConf->nChanConfigured;
    } else {
        numChannelsInFile = hardwareConf->NCHAN;
    }

    int lastChannel = -1;
    int unusedChannelsSoFar = 0;
    for (int i=0;i<sortedChannelList.length();i++) {
        if ((sortedChannelList[i]-lastChannel > 1) && (globalConf->saveDisplayedChanOnly)){
            unusedChannelsSoFar = unusedChannelsSoFar + (sortedChannelList[i]-lastChannel-1);

        }
        channelPacketLocations.push_back(packetHeaderSize+(2*(sortedChannelList[i]-unusedChannelsSoFar)));
        lastChannel = sortedChannelList[i];
    }

    if (globalConf->saveDisplayedChanOnly) {
        filePacketSize = 2*(numChannelsInFile) + packetHeaderSize+paddingBytes;
    } else {
        filePacketSize = 2*(hardwareConf->NCHAN) + packetHeaderSize+paddingBytes;
    }


    //Now we go back and gather some more info about each channel...
    for (int i=0;i<sortedChannelList.length();i++) {
        bool foundthischannel = false;
        for (int n = 0; n < spikeConf->ntrodes.length(); n++) {
            if (!foundthischannel) {
                for (int c = 0; c < spikeConf->ntrodes[n]->hw_chan.length(); c++) {
                    int tempHWRead = spikeConf->ntrodes[n]->hw_chan[c];
                    if (tempHWRead == sortedChannelList[i]) {
                        if (globalConf->saveDisplayedChanOnly) {
                            spikeConf->ntrodes[n]->hw_chan[c] = i; //Change the actual value in the workspace
                        }
                        //qDebug() << "Original HW" << sortedChannelList[i] << "Packet ch location" << i << "nTrode ID" << spikeConf->ntrodes[n]->nTrodeId << "channel" << c;


                        //Keep a record of the channel's nTrode ID
                        nTrodeForChannels.push_back(spikeConf->ntrodes[n]->nTrodeId);
                        //qDebug() << tempHWRead << spikeConf->ntrodes[n]->unconverted_hw_chan[c] << "ntrode" << spikeConf->ntrodes[n]->nTrodeId << c;
                        nTrodeIndForChannels.push_back(n);
                        //Keep a record of the channel within the nTrode
                        channelInNTrode.push_back(c);

                        foundthischannel = true;
                        break;
                    }
                }
            }
        }
    }

    //Create a lookup table, where we have a list of channels that go into each sorting group
    int maxSortingGroupNum = 0;
    for (int nt=0; nt<spikeConf->ntrodes.length(); nt++) {
        for (int ch=0; ch<spikeConf->ntrodes.at(nt)->spikeSortingGroup.length();ch++) {
            if (spikeConf->ntrodes.at(nt)->spikeSortingGroup.at(ch) > maxSortingGroupNum) {
                maxSortingGroupNum = spikeConf->ntrodes.at(nt)->spikeSortingGroup.at(ch);
            }
        }
    }

    //Check to make sure the sorting groups actually make sense.
    if (maxSortingGroupNum > hardwareConf->NCHAN) {
        std::cerr  << "Warning: sorting groups are not properly set in the workspace. Ignoring sorting groups. \n";
        maxSortingGroupNum = 0;
        for (int nt=0; nt<spikeConf->ntrodes.length(); nt++) {
            for (int ch=0; ch<spikeConf->ntrodes.at(nt)->spikeSortingGroup.length();ch++) {
                spikeConf->ntrodes[nt]->spikeSortingGroup[ch] = 0;
            }
        }
    }

    for (int sg=0; sg <= maxSortingGroupNum;sg++) {

        QList<int> chInGroup;
        int count = 0;
        for (int nt=0; nt<spikeConf->ntrodes.length(); nt++) {
            for (int ch=0; ch<spikeConf->ntrodes.at(nt)->spikeSortingGroup.length();ch++) {
                if (spikeConf->ntrodes.at(nt)->spikeSortingGroup.at(ch) == sg) {
                    chInGroup.push_back(count);
                }
                count++;
            }
        }
        spikeSortingGroupLookup.push_back(chInGroup);
    }


    for (int i=0; i < MAXCARGROUPS; i++) {
        CARData.push_back(0); //Where the computed CAR data will be stored for each packet
    }
    for(int i = 0; i < spikeConf->carGroups.length(); ++i){

        for (int j = 0; j < spikeConf->carGroups[i].chans.length(); j++) {

            int tmpCARHWChan = sortedChannelList.indexOf(spikeConf->carGroups[i].chans[j].hw_chan);
            if (tmpCARHWChan == -1) {
                qDebug() << "There is a problem with the Common Average Referencing (CAR) channel list. One of the designated CAR channels was not recorded.  CAR group" << i << "Channel" << j;
                //TODO: remove all use of this CAR group from the workspace
                return;
            } else {
                if (tmpCARHWChan != spikeConf->carGroups[i].chans[j].hw_chan) {
                    //The channel HW channel needs to be modified becuase not all channels were recorded.
                    spikeConf->carGroups[i].chans[j].hw_chan = tmpCARHWChan;
                }

            }
        }
        //CARData.push_back(0); //Where the computed CAR data will be stored for each packet
    }


    //avxsupported = false;
    avxsupported = enableAVX && detectAVXCapabilities();
    if (!avxsupported) {
        qDebug() << "No AVX support detected. It is required for this program to work.";
        exit(1);
    }
    if(avxsupported){
        uint32_t extraSteps = 0;

        if (conf_ptrs.spikeConf->deviceType == "neuropixels1") {
            extraSteps |= AbstractNeuralDataHandler::ExtraProcessorType::NeuroPixels1;
            //invertSpikes = !invertSpikes; //NP probe already invert the spikes
        }

        extraSteps |= AbstractNeuralDataHandler::ExtraProcessorType::Export;

        neuralDataHandler = new VectorizedNeuralDataHandler(conf_ptrs, nTrodeIndexList,extraSteps);
    }
    else {

        if (conf_ptrs.spikeConf->deviceType == "neuropixels1") {
            qDebug() << "Error: Workspaces using neuropixels1 hardware MUST use the -avx flag set to 1.";
            exit(1);
        }

        neuralDataHandler = new NeuralDataHandler(conf_ptrs,nTrodeIndexList);
    }


    if ((maxGapSizeInterpolation < 0) || (maxGapSizeInterpolation > 4294967295)) {
        maxGapSizeInterpolation = 4294967295; //infinite interpolation
    } else {
        maxGapSizeInterpolation = maxGapSizeInterpolation+1; //This way, a value of 0 changes to 1, which is the diff between two consecutive time points with no gap
    }
    if (maxGapSizeInterpolation > 0) {
        neuralDataHandler->setInterpMode(true,(uint32_t)maxGapSizeInterpolation); //With this, the handler will interpolate across data gaps
    }
    neuralDataHandler->setSpikeInvert(invertSpikes);


    for (int auxChInd = 0; auxChInd < headerConf->headerChannels.length(); auxChInd++) {

            if (headerConf->headerChannels[auxChInd].dataType == DeviceChannel::DIGITALTYPE) {           
                DIOChannelInds.push_back(auxChInd);
                DIOLastValue.push_back(2); //state will be either 1 or 0, 2 means that it's the first time point
                DIOinterleavedDataBytes.push_back(headerConf->headerChannels[auxChInd].interleavedDataIDByte);
            } else if (headerConf->headerChannels[auxChInd].dataType == DeviceChannel::INT16TYPE) {
                //This is an analog channel
                analogChannelInds.push_back(auxChInd);
                analogLastValues.push_back(0);
                analogInterleavedDataBytes.push_back(headerConf->headerChannels[auxChInd].interleavedDataIDByte);
            }
    }

}

void DATExportHandler::calculateCARDataInPacket(int16_t *startOfNeuralData) {

    //Calculate the common average reference values to be subtracted from samples that use them
    for (int i=0; i < spikeConf->carGroups.length(); ++i){
    if(spikeConf->carGroups[i].chans.length() == 0){
        //if this car group is empty, skip it
        continue;
    }
    int32_t accum = 0;
    for(auto const &chan : spikeConf->carGroups[i].chans){
        accum += startOfNeuralData[chan.hw_chan];
    }

    //divide by 0 exception not possible since if chans is empty, skip calculations
    CARData[i] = accum/spikeConf->carGroups[i].chans.length();

    }

}



int DATExportHandler::processData() {

    bool debugOutput = false;
    std::cerr  << "Exporting data...\n";

    //Calculate the packet positions for each channel that we are extracting, plus
    //other critical info (number of saved channels, reference info, etc).    


    if (debugOutput) std::cerr  << "Calculating channel info...";
    calculateChannelInfo();
    if (debugOutput) std::cerr  << "done\n";






    //Check to make sure at least one neural channel has been defined in the workspace
    if (channelPacketLocations.length() == 0) {

        //If there are no neural channels in the file, we only support dio and aio export.
        uint32_t mask = 0; //0x90; // 10010000
        mask |= (supportedDataCategories & DataCategories::DigitalIO);
        mask |= (supportedDataCategories & DataCategories::AnalogIO);
        mask |= (supportedDataCategories & DataCategories::Time);
        supportedDataCategories = mask;


        if (supportedDataCategories == 0) {
            std::cerr  << "No neural information described in the workspace. Aborting.";
            return -1;
        }
    }

    if (debugOutput) std::cerr  << "Opening file...";




    if (!openInputFile()) {
        return -1;
    }
    if (debugOutput) std::cerr  << "done\n";



    if (debugOutput) std::cerr  << "Creating output files...";
    if (!createAllOutputFiles()) {
        qDebug() << "Aborting program.";
        exit(1);
    }

    if (debugOutput) std::cerr  << "done\n";

    if (debugOutput) std::cerr  << "Creating time offset file...";
    if (!createTimeOffsetFile()) {
        std::cerr  << "Error creating time offset file.";
        return -1;
    }
    if (debugOutput) std::cerr  << "done\n";

    //************************************************
    int inputFileInd = 0;
    qint32 packetsWritten = 0;
    while (inputFileInd < recFileNameList.length()) {
        if (inputFileInd > 0) {
            //There are multiple files that need to be stitched together. It is assumed that they all have
            //the exact same header section.
            recFileName = recFileNameList.at(inputFileInd);
            uint32_t lastFileTStamp = currentTimeStamp;

            qDebug() << "\nAppending from file: " << recFileName;
            QFileInfo fi(recFileName);

            if (!fi.exists()) {
                qDebug() << "File could not be found: " << recFileName;
                break;
            }
            if (!openInputFile()) {
                qDebug() << "Error: it appears that the file does not have an identical header to the last file. Cannot append to file.";
                return -1;
            }
            for (int i=0; i < channelFilters.length(); i++) {
                channelFilters[i]->resetHistory();
            }
            if (currentTimeStamp < lastFileTStamp) {
                qDebug() << "Error: timestamps do not begin with greater value than the end of the last file. Aborting.";
                return -1;
            }
            createTimeOffsetFile();

        }
        int pointsSinceLastSpikeCheck = 0;
        int spikeCheckInterval = 1000;
        //Process the data and stream results to output files

        if (debugOutput) std::cerr  << "Starting processing\n";
        while(!filePtr->atEnd()) {

            if (debugOutput && packetsWritten == 0) std::cerr  << "Processing first packet\n";
            if (!getNextPacket()) {
                //We have reached the end of the file             
                break;
            }

            /*if (neuralDataHandler->gapOccured()) {


                for (int i=0; i<spikeDetectors.length();i++){
                    spikeDetectors[i]->clearHistory();
                }
            }*/


            if (debugOutput && packetsWritten == 0) std::cerr  << "Writing first packet...";
            if (supportedDataCategories & DataCategories::SpikeEvents) {
                //Write spikes to file
                writeSpikeData();
            } else {
                neuralDataHandler->clearAvailableWaveforms();
            }

            if (supportedDataCategories & DataCategories::LfpBand) {
                writeLfpBandData();
            }

            if (supportedDataCategories & DataCategories::Time) {
                writeTimeData();
            }

            if (supportedDataCategories & DataCategories::SpikeBand) {
                writeSpikeBandData();
            }

            if (supportedDataCategories & DataCategories::RawBand) {
                writeRawBandData();
            }

            if (supportedDataCategories & DataCategories::DigitalIO) {
                writeDIOData();
            }

            if (supportedDataCategories & DataCategories::AnalogIO) {
                writeAnalogData();
            }

            if (supportedDataCategories & DataCategories::Stimulation) {
                writeStimBandData();
            }

            if (supportedDataCategories & DataCategories::MountainSort) {
                writeMountainSortData();
            }

            if (supportedDataCategories & DataCategories::KiloSort) {
                writeKiloSortData();
            }
            if (debugOutput && packetsWritten == 0) std::cerr  << "done\n";

            packetsWritten++;


            //Print the progress to stdout
            printProgress();
            pointsSinceLastLog++;
            pointsSinceLastSpikeCheck++;



        }
        filePtr->close();
        printf("\rDone\n");
        inputFileInd++;
    }

    if (supportedDataCategories & DataCategories::MountainSort) {
        QByteArray numSamplesData;

        timeFilePtrs[fMountainSort][0]->flush();
        timeFilePtrs[fMountainSort][0]->seek(12);
        *timeStreamPtrs[fMountainSort][0] << packetsWritten;

        for (int i=0; i < outputFilePtrs[fMountainSort].length(); i++) {
            outputFilePtrs[fMountainSort][i]->flush();
            outputFilePtrs[fMountainSort][i]->seek(16);
            *outputStreamPtrs[fMountainSort][i] << packetsWritten;


            //Check if the info was written
            /*
            outputFilePtrs[fMountainSort][i]->seek(0);
            for (int r=0;r<10;r++)  {
                qint32 readVal;
                *outputStreamPtrs[fMountainSort][i] >> readVal;
                qDebug() << i << readVal;
            }*/
            //outputFilePtrs[fMountainSort][i]->write(numSamplesData);
        }


    }

    for (int i=0; i < outputFilePtrs.length(); i++) {
        for (int j=0; j < outputFilePtrs[i].length(); j++) {
            outputFilePtrs[i][j]->flush();
            outputFilePtrs[i][j]->close();
        }

    }

    for (int i=0; i < timeFilePtrs.length(); i++) {
        for (int j=0; j < timeFilePtrs[i].length(); j++) {
            timeFilePtrs[i][j]->flush();
            timeFilePtrs[i][j]->close();
        }

    }


    return 0; //success
}

/*void DATExportHandler::writeSpikeData(int nTrodeNum, const QVector<int2d> *waveForm, const int *, uint32_t time){
    //A spike event was detected.
    //Write the spike waveforms plus the timestamps to the correct file


    //First, write the timestamp
    *neuroStreamPtrs[nTrodeNum] << time;

    //Then write the 40 waveform points for channel 0, then 40 points for channel 1, etc.
    for (int chInd=0; chInd < spikeConf->ntrodes[nTrodeNum]->hw_chan.length(); chInd++) {
        for (int sampleIndex = 0; sampleIndex < 40; sampleIndex++) {
            *neuroStreamPtrs[nTrodeNum] << waveForm[chInd][sampleIndex].y;
        }
    }

}*/

void DATExportHandler::writeSpikeData()
{

    QVector<SpikeWaveform> newSpks;
    neuralDataHandler->getAvailableWaveforms(newSpks);
    if (!newSpks.isEmpty()) {
        //A spike event was detected.
        //Write the spike waveforms plus the timestamps to the correct file
        for (int spInd=0; spInd<newSpks.length();spInd++) {
            //First, write the timestamp
            *outputStreamPtrs[fSpikeEvents][newSpks[spInd].ntrodeIndex] << newSpks[spInd].peakTime;



            //Then write the 40 waveform points for channel 0, then 40 points for channel 1, etc.
            for (int chInd=0; chInd < newSpks[spInd].numChannels; chInd++) {
                for (int sampleIndex = 0; sampleIndex < newSpks[spInd].numSamples; sampleIndex++) {
                    *outputStreamPtrs[fSpikeEvents][newSpks[spInd].ntrodeIndex] <<  newSpks[spInd].waveData[chInd][sampleIndex];
                }
            }

            newSpks[spInd].clearMemory();
        }
    }
}

void DATExportHandler::writeSpikeBandData()
{

    *timeStreamPtrs[fSpikeBand].at(0) << currentTimeStamp; //Write the data to disk
    int fileInd = 0;
    for (int ntInd=0; ntInd < spikeConf->ntrodes.length(); ntInd++) {
        const int16_t* tmpNtrodeData;

        tmpNtrodeData = neuralDataHandler->getNTrodeSamples(ntInd,AbstractNeuralDataHandler::SPIKE);
        for (int chInd = 0; chInd < spikeConf->ntrodes[ntInd]->hw_chan.length(); chInd++) {
            *outputStreamPtrs[fSpikeBand].at(fileInd) << tmpNtrodeData[chInd]; //Write the data to disk
            fileInd++; //This works because the original files were created with the exact same nested loops with the same check
        }

    }

}

void DATExportHandler::writeTimeData()
{


    *timeStreamPtrs[fTime].at(0) << currentTimeStamp; //Write the data to disk
    if (hardwareConf->sysTimeIncluded) {
        *timeStreamPtrs[fTime].at(0) << currentSysTimestamp; //Write the data to disk
    }


}

void DATExportHandler::writeLfpBandData()
{
    if ((currentTimeStamp%decimation) == 0) { //if we are subsampling the data, make sure this is a point we want to save in the first place
        //save data in time file

        *timeStreamPtrs[fLfpBand].at(0) << currentTimeStamp; //Write the data to disk
        int fileInd = 0;
        for (int ntInd=0; ntInd < spikeConf->ntrodes.length(); ntInd++) {
            const int16_t* tmpNtrodeData = neuralDataHandler->getNTrodeSamples(ntInd,AbstractNeuralDataHandler::LFP);
            *outputStreamPtrs[fLfpBand].at(fileInd) << tmpNtrodeData[0]; //Write the data to disk
            fileInd++; //This works because the original files were created with the exact same nested loops with the same check

        }
    }
}
void DATExportHandler::writeRawBandData()
{
    *timeStreamPtrs[fRawBand].at(0) << currentTimeStamp; //Write the data to disk

    if (sortingMode == SpikeSortingGroupMode::OneGroup) {
        //const int16_t* tmpNtrodeData = neuralDataHandler->getNTrodeSamples(ntInd,AbstractNeuralDataHandler::RAW_REFERENCED);
        int numSamplesToWrite;

        const int16_t* tmpNtrodeData = neuralDataHandler->getEntirePacket(AbstractNeuralDataHandler::RAW_REFERENCED, numSamplesToWrite);
        for (int wr = 0; wr < numSamplesToWrite; wr++) {
            *outputStreamPtrs[fRawBand].at(0) << tmpNtrodeData[wr]; //Write the data from one ntrode to disk
        }

    }  else if (sortingMode == SpikeSortingGroupMode::ByNtrodes){
        int fileInd = 0;
        for (int ntInd=0; ntInd < spikeConf->ntrodes.length(); ntInd++) {
            const int16_t* tmpNtrodeData = neuralDataHandler->getNTrodeSamples(ntInd,AbstractNeuralDataHandler::RAW_REFERENCED);
            for (int chInd = 0; chInd < spikeConf->ntrodes[ntInd]->hw_chan.length(); chInd++) {
                *outputStreamPtrs[fRawBand].at(fileInd) << tmpNtrodeData[chInd]; //Write the data from one nTrode to disk
            }
            fileInd++;
        }
    } else if (sortingMode == SpikeSortingGroupMode::BySortingGroup){
        int numSamplesToWrite;
        const int16_t* tmpNtrodeData = neuralDataHandler->getEntirePacket(AbstractNeuralDataHandler::RAW_REFERENCED, numSamplesToWrite);
        for (int group = 0; group < spikeSortingGroupLookup.length(); group++) {
            for (int chInd=0; chInd < spikeSortingGroupLookup[group].length(); chInd++) {
                *outputStreamPtrs[fRawBand].at(group) << tmpNtrodeData[spikeSortingGroupLookup[group][chInd]]; //Write the data from one ntrode to disk
            }
        }
    } else if (sortingMode == SpikeSortingGroupMode::SingleChannels){

        int fileInd = 0;
        for (int ntInd=0; ntInd < spikeConf->ntrodes.length(); ntInd++) {
            const int16_t* tmpNtrodeData;

            tmpNtrodeData = neuralDataHandler->getNTrodeSamples(ntInd,AbstractNeuralDataHandler::RAW_REFERENCED);
            for (int chInd = 0; chInd < spikeConf->ntrodes[ntInd]->hw_chan.length(); chInd++) {
                *outputStreamPtrs[fRawBand].at(fileInd) << tmpNtrodeData[chInd]; //Write the data to disk
                fileInd++; //This works because the original files were created with the exact same nested loops with the same check
            }

        }
    }
}
void DATExportHandler::writeStimBandData()
{
    float stimScale = neuralDataHandler->getStimScale_nA();

    *timeStreamPtrs[fStimulation].at(0) << currentTimeStamp; //Write the data to disk
    int fileInd = 0;
    float stimCurrent;
    for (int ntInd=0; ntInd < spikeConf->ntrodes.length(); ntInd++) {
        const int16_t* tmpNtrodeData;

        tmpNtrodeData = neuralDataHandler->getNTrodeSamples(ntInd,AbstractNeuralDataHandler::STIMULATION);
        int16_t rawStimInfo;

        for (int chInd = 0; chInd < spikeConf->ntrodes[ntInd]->hw_chan.length(); chInd++) {
            if (spikeConf->ntrodes[ntInd]->stimCapable[chInd]) {
                rawStimInfo = tmpNtrodeData[chInd];
                stimCurrent = stimScale*((rawStimInfo & 0xFF00) >> 2); //calculate the current in nA
                *outputStreamPtrs[fStimulation].at(fileInd) << stimCurrent; //Write the data to disk
                fileInd++; //This works because the original files were created with the exact same nested loops with the same check
            }
        }

    }
}

void DATExportHandler::writeMountainSortData()
{

    *timeStreamPtrs[fMountainSort].at(0) << currentTimeStamp; //Write the time data to disk

    if (sortingMode == SpikeSortingGroupMode::OneGroup) {
        //const int16_t* tmpNtrodeData = neuralDataHandler->getNTrodeSamples(ntInd,AbstractNeuralDataHandler::RAW_REFERENCED);
        int numSamplesToWrite;

        const int16_t* tmpNtrodeData = neuralDataHandler->getEntirePacket(AbstractNeuralDataHandler::RAW_REFERENCED, numSamplesToWrite);
        for (int wr = 0; wr < numSamplesToWrite; wr++) {
            *outputStreamPtrs[fMountainSort].at(0) << tmpNtrodeData[wr]; //Write the data from one ntrode to disk
        }

    }  else if (sortingMode == SpikeSortingGroupMode::ByNtrodes){
        int fileInd = 0;
        for (int ntInd=0; ntInd < spikeConf->ntrodes.length(); ntInd++) {
            const int16_t* tmpNtrodeData = neuralDataHandler->getNTrodeSamples(ntInd,AbstractNeuralDataHandler::RAW_REFERENCED);
            for (int chInd = 0; chInd < spikeConf->ntrodes[ntInd]->hw_chan.length(); chInd++) {
                *outputStreamPtrs[fMountainSort].at(fileInd) << tmpNtrodeData[chInd]; //Write the data from one nTrode to disk
            }
            fileInd++;
        }
    } else if (sortingMode == SpikeSortingGroupMode::BySortingGroup){
        int numSamplesToWrite;
        const int16_t* tmpNtrodeData = neuralDataHandler->getEntirePacket(AbstractNeuralDataHandler::RAW_REFERENCED, numSamplesToWrite);
        for (int group = 0; group < spikeSortingGroupLookup.length(); group++) {
            for (int chInd=0; chInd < spikeSortingGroupLookup[group].length(); chInd++) {
                *outputStreamPtrs[fMountainSort].at(group) << tmpNtrodeData[spikeSortingGroupLookup[group][chInd]]; //Write the data from one ntrode to disk
            }
        }
    }


    /*int fileInd = 0;
    for (int ntInd=0; ntInd < spikeConf->ntrodes.length(); ntInd++) {
        const int16_t* tmpNtrodeData = neuralDataHandler->getNTrodeSamples(ntInd,AbstractNeuralDataHandler::RAW_REFERENCED);
        for (int chInd = 0; chInd < spikeConf->ntrodes[ntInd]->hw_chan.length(); chInd++) {
            if (sortingMode == SpikeSortingGroupMode::OneGroup) {
                *outputStreamPtrs[fMountainSort].at(0) << tmpNtrodeData[chInd]; //Combine data to one file
            }  else if (sortingMode == SpikeSortingGroupMode::ByNtrodes){
                *outputStreamPtrs[fMountainSort].at(fileInd) << tmpNtrodeData[chInd]; //Write the data from one nTrode to disk
            } else if (sortingMode == SpikeSortingGroupMode::BySortingGroup){
                *outputStreamPtrs[fMountainSort].at(fileInd) << tmpNtrodeData[chInd]; //Write the data from one nTrode to disk
            }
        }
        fileInd++;
    }*/
}

void DATExportHandler::writeKiloSortData()
{

    *timeStreamPtrs[fKiloSort].at(0) << currentTimeStamp; //Write the time data to disk

    if (spikeConf->deviceType == "neuropixels1") {
        //For neuropixels probes, we write one file per probe. We use the page breaks entry to determine where each probe begins.

        int numSamplesToWrite;
        const int16_t* tmpNtrodeData = neuralDataHandler->getEntirePacket(AbstractNeuralDataHandler::RAW_REFERENCED, numSamplesToWrite);

        int samplesWritten = 0;
        for (int i=0; i<streamConf->pageBreaks.length(); i++) {

            if (!invertSpikes) {
                for (int wr = samplesWritten; wr < streamConf->pageBreaks.at(i); wr++) {
                    *outputStreamPtrs[fKiloSort].at(i) << tmpNtrodeData[wr]; //Write the data from one ntrode to disk
                    samplesWritten++;
                }
            } else {
                //if the invert spikes flag is set, we need to flip the spikes again for kilosort (spikes sould not be inverted for kilosort)
                //const int16_t inv = -1;
                for (int wr = samplesWritten; wr < streamConf->pageBreaks.at(i); wr++) {
                    *outputStreamPtrs[fKiloSort].at(i) << (int16_t)(tmpNtrodeData[wr]*-1); //Write the data from one ntrode to disk
                    samplesWritten++;
                }
            }

        }


    } else {
        if (sortingMode == SpikeSortingGroupMode::OneGroup) {
            //const int16_t* tmpNtrodeData = neuralDataHandler->getNTrodeSamples(ntInd,AbstractNeuralDataHandler::RAW_REFERENCED);
            int numSamplesToWrite;

            const int16_t* tmpNtrodeData = neuralDataHandler->getEntirePacket(AbstractNeuralDataHandler::RAW_REFERENCED, numSamplesToWrite);
            for (int wr = 0; wr < numSamplesToWrite; wr++) {
                *outputStreamPtrs[fKiloSort].at(0) << tmpNtrodeData[wr]; //Write the data from one ntrode to disk
            }

        }  else if (sortingMode == SpikeSortingGroupMode::ByNtrodes){
            int fileInd = 0;
            for (int ntInd=0; ntInd < spikeConf->ntrodes.length(); ntInd++) {
                const int16_t* tmpNtrodeData = neuralDataHandler->getNTrodeSamples(ntInd,AbstractNeuralDataHandler::RAW_REFERENCED);
                for (int chInd = 0; chInd < spikeConf->ntrodes[ntInd]->hw_chan.length(); chInd++) {
                    *outputStreamPtrs[fKiloSort].at(fileInd) << tmpNtrodeData[chInd]; //Write the data from one nTrode to disk
                }
                fileInd++;
            }
        } else if (sortingMode == SpikeSortingGroupMode::BySortingGroup){

            int numSamplesToWrite;
            const int16_t* tmpNtrodeData = neuralDataHandler->getEntirePacket(AbstractNeuralDataHandler::RAW_REFERENCED, numSamplesToWrite);
            for (int group = 0; group < spikeSortingGroupLookup.length(); group++) {
                for (int chInd=0; chInd < spikeSortingGroupLookup[group].length(); chInd++) {
                    *outputStreamPtrs[fKiloSort].at(group) << tmpNtrodeData[spikeSortingGroupLookup[group][chInd]]; //Write the data from one ntrode to disk
                }
            }

        }
    }
}

void DATExportHandler::writeDIOData()
{

    //Write one packet of DIO data
    uint8_t *interleavedDataIDBytePtr;
    uint8_t tmpVal = 0;

    for (int chInd=0; chInd < DIOChannelInds.length(); chInd++) {

        bufferPtr = buffer.data()+headerConf->headerChannels[DIOChannelInds[chInd]].startByte;

        if (DIOinterleavedDataBytes[chInd] != -1) {
            //This location in the packet has interleaved data from multiple sources that are sampled at a lower rate

            interleavedDataIDBytePtr = (uint8_t*)(buffer.data()) + DIOinterleavedDataBytes[chInd];

            if (*interleavedDataIDBytePtr & (1 << headerConf->headerChannels[DIOChannelInds[chInd]].interleavedDataIDBit)) {
                //This interleaved data point belongs to this channel, so update the channel. Otherwise, no update occurs.

                tmpVal = (uint8_t)((*bufferPtr & (1 << headerConf->headerChannels[DIOChannelInds[chInd]].digitalBit)) >> headerConf->headerChannels[DIOChannelInds[chInd]].digitalBit);

            } else {
                //Use the last data point received
                tmpVal = DIOLastValue[chInd];

            }
        } else {
            //No interleaving
            tmpVal = (uint8_t)((*bufferPtr & (1 << headerConf->headerChannels[DIOChannelInds[chInd]].digitalBit)) >> headerConf->headerChannels[DIOChannelInds[chInd]].digitalBit);

        }

        if ((tmpVal != DIOLastValue[chInd]) && (tmpVal < 2)) {

            *outputStreamPtrs[fDigitalIO].at(chInd) << currentTimeStamp << tmpVal;
            DIOLastValue[chInd] = tmpVal;
        }


    }

}

void DATExportHandler::writeAnalogData()
{
    //Write one packet of analog IO data
    uint8_t *interleavedDataIDBytePtr;
    int16_t tmpVal;
    int16_t *valuePtr;

    *timeStreamPtrs[fAnalogIO].at(0) << currentTimeStamp;

    for (int chInd=0; chInd < analogChannelInds.length(); chInd++) {

        if (analogInterleavedDataBytes[chInd] != -1) {
            //This location in the packet has interleaved data from multiple sources that are sampled at a lower rate

            interleavedDataIDBytePtr = (uint8_t*)(buffer.data()) + analogInterleavedDataBytes[chInd];

            if (*interleavedDataIDBytePtr & (1 << headerConf->headerChannels[analogChannelInds[chInd]].interleavedDataIDBit)) {
                //This interleaved data point belongs to this channel, so update the channel. Otherwise, no update occurs.
                bufferPtr = buffer.data()+headerConf->headerChannels[analogChannelInds[chInd]].startByte;
                valuePtr = (int16_t*)(bufferPtr);
                tmpVal = (int16_t)(*valuePtr);
                *outputStreamPtrs[fAnalogIO].at(chInd) << tmpVal;
                analogLastValues[chInd] = tmpVal;

            } else {
                //Use the last data point received
                *outputStreamPtrs[fAnalogIO].at(chInd) << analogLastValues[chInd];

            }
        } else {
            //No interleaving
            bufferPtr = buffer.data()+headerConf->headerChannels[analogChannelInds[chInd]].startByte;
            valuePtr = (int16_t*)(bufferPtr);
            tmpVal = (int16_t)(*valuePtr);

            *outputStreamPtrs[fAnalogIO].at(chInd) << tmpVal;
        }
    }
}


bool DATExportHandler::createAllOutputFiles()
{
    //Create a directory for the output files located in the same place as the source file

    QFileInfo fi(recFileName);

    if (outputFileName.isEmpty()) {
        fileBaseName = fi.completeBaseName();
    } else {
        fileBaseName = outputFileName;
    }


    if(outputDirectory.isEmpty()) {
        directory = fi.absolutePath();
    } else {
        directory = outputDirectory;
    }



    for (int i=0; i < numberOfDataCategories; i++) {

        outputFilePtrs.push_back(QList<QFile*>());
        outputStreamPtrs.push_back(QList<QDataStream*>());
        timeFilePtrs.push_back(QList<QFile*>());
        timeStreamPtrs.push_back(QList<QDataStream*>());
    }



    /*fSpikeBand         = 0,
    fLfpBand           = 1,
    fRawBand           = 2,
    fDigitalIO         = 3,
    fAnalogIO          = 4,
    fSpikeEvents       = 5,
    fStimulation       = 6,
    fEvents            = 7,
    fMountainSort      = 8,
    fKiloSort          = 9*/

    if ( (supportedDataCategories & LfpBand) ||
         (supportedDataCategories & RawBand) ||
         (supportedDataCategories & SpikeBand) ||
         (supportedDataCategories & SpikeEvents) ||
         (supportedDataCategories & Stimulation) ||
         (supportedDataCategories & MountainSort) ||
         (supportedDataCategories & KiloSort) ) {
        neuralDataNeedsProcessing = true;
    }


    if (supportedDataCategories & LfpBand) {
        if (!createLFPOutputFiles()) return false;
    }

    if (supportedDataCategories & Time) {
        if (!createTimeOutputFiles()) return false;
    }



    if (supportedDataCategories & RawBand) {
        if (!createRawOutputFiles()) return false;
    }

    if (supportedDataCategories & SpikeBand) {
        if (!createSpikeBandOutputFiles()) return false;
    }



    if (supportedDataCategories & DigitalIO) {
        if (!createDIOOutputFiles()) return false;
    }

    if (supportedDataCategories & AnalogIO) {
        if (!createAnalogIOOutputFiles()) return false;
    }

    if (supportedDataCategories & SpikeEvents) {
        if (!createSpikeEventOutputFiles()) return false;
    }

    if (supportedDataCategories & Stimulation) {
        if (!createStimulationOutputFiles()) return false;
    }

    if (supportedDataCategories & Events) {
        if (!createEventsOutputFiles()) return false;
    }

    if (supportedDataCategories & MountainSort) {
        if (!createMountainSortOutputFiles()) return false;
    }

    if (supportedDataCategories & KiloSort) {
        if (!createKiloSortOutputFiles()) return false;
    }



    return true;

}


bool DATExportHandler::createSpikeBandOutputFiles()
{
    int fileDataType = fSpikeBand;
    QString fileTypeExtension = fileTypeNames.at(fileDataType);

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+"."+fileTypeExtension+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating" << fileTypeExtension <<  "directory.";
            return false;
        }
    }


    QString infoLine;
    QString fieldLine;

    //Create an output file for the timestamps
    //*****************************************
    timeFilePtrs[fileDataType].push_back(new QFile);
    timeFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".timestamps.dat"));
    if (!timeFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating time file for continuous spike band data.";
        return false;
    }
    timeStreamPtrs[fileDataType].push_back(new QDataStream(timeFilePtrs[fileDataType].last()));
    timeStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);

    //Write the current settings to file
    writeHeaderInfo(timeFilePtrs[fileDataType].last(),QString("Spike band timestamps\r\n"));

    fieldLine.clear();
    fieldLine += "Fields: ";
    fieldLine += "<time uint32>";
    fieldLine += "\n";
    timeFilePtrs[fileDataType].last()->write(fieldLine.toLocal8Bit());
    timeFilePtrs[fileDataType].last()->write("<End settings>\n");
    timeFilePtrs[fileDataType].last()->flush();


    //Create an output file for the neural data
    //*****************************************

    //Create the output files (one for each channel) and write header sections
    for (int nt=0; nt < spikeConf->ntrodes.length(); nt++) {
        for (int chInd = 0; chInd < spikeConf->ntrodes[nt]->hw_chan.length(); chInd++) {
            bool saveThisChannel = true;

            if (saveThisChannel) {
                outputFilePtrs[fileDataType].push_back(new QFile);

                outputFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".spikeband_nt%1ch%2.dat").arg(spikeConf->ntrodes[nt]->nTrodeId).arg(chInd+1));
                if (!outputFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
                    qDebug() << "Error creating output file for" << fileTypeExtension;
                    if (outputFilePtrs[fileDataType].last()->error() == 4) {
                        qDebug() << "Too many files open simultaneously.";
                    }
                    return false;
                }
                outputStreamPtrs[fileDataType].push_back(new QDataStream(outputFilePtrs[fileDataType].last()));
                outputStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);

                //Write the current settings to file
                writeHeaderInfo(outputFilePtrs[fileDataType].last(),QString("Spike band continuous data for one channel\r\n"));

                infoLine = QString("nTrode_ID: %1\n").arg(spikeConf->ntrodes[nt]->nTrodeId);
                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());
                infoLine = QString("num_channels: %1\n").arg(spikeConf->ntrodes[nt]->hw_chan.length());
                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

                infoLine = QString("Voltage_scaling: %1\n").arg(spikeConf->ntrodes[nt]->spike_scaling_to_uV);
                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

                if (spikeConf->ntrodes[nt]->refOn) {
                    if (spikeConf->ntrodes[nt]->groupRefOn) {
                        infoLine = QString("Reference: Common average reference group %1\n").arg(spikeConf->ntrodes[nt]->refGroup);
                    } else {
                        infoLine = QString("Reference: on \nReferenceNTrode: %1\nReferenceChannel: %2\n").arg(spikeConf->ntrodes[nt]->refNTrodeID).arg(spikeConf->ntrodes[nt]->refChan+1);
                    }
                } else {
                    infoLine = QString("Reference: off\n");
                }

                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

                if (spikeConf->ntrodes[nt]->filterOn) {
                    infoLine = QString("Filter: on \nlowPassFilter: %1\nhighPassFilter: %2\n").arg(spikeConf->ntrodes[nt]->highFilter).arg(spikeConf->ntrodes[nt]->lowFilter);

                } else {
                    infoLine = QString("Filter: off \n");
                }

                if (invertSpikes) {
                    infoLine = QString("Spike_invert: yes\n");
                } else {
                    infoLine = QString("Spike_invert: no\n");
                }


                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());


                fieldLine.clear();
                fieldLine += "Fields: ";
                fieldLine += "<voltage int16>";
                fieldLine += "\n";
                outputFilePtrs[fileDataType].last()->write(fieldLine.toLocal8Bit());
                outputFilePtrs[fileDataType].last()->write("<End settings>\n");
                outputFilePtrs[fileDataType].last()->flush();
            }
        }
    }
    return true;
}

bool DATExportHandler::createLFPOutputFiles()
{
    int fileDataType = fLfpBand;
    QString fileTypeExtension = fileTypeNames.at(fileDataType);

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+"."+fileTypeExtension+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating" << fileTypeExtension <<  "directory.";
            return false;
        }
    }


    QString infoLine;
    QString fieldLine;

    //Create an output file for the timestamps
    //*****************************************
    timeFilePtrs[fileDataType].push_back(new QFile);
    timeFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".timestamps.dat"));
    if (!timeFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating time file for LFP.";
        return false;
    }
    timeStreamPtrs[fileDataType].push_back(new QDataStream(timeFilePtrs[fileDataType].last()));
    timeStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);


    //Write the current settings to file
    writeHeaderInfo(timeFilePtrs[fileDataType].last(),QString("LFP timestamps\r\n"));

    infoLine = QString("Decimation: %1\n").arg(decimation);
    timeFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

    fieldLine.clear();
    fieldLine += "Fields: ";
    fieldLine += "<time uint32>";
    fieldLine += "\r\n";
    timeFilePtrs[fileDataType].last()->write(fieldLine.toLocal8Bit());
    timeFilePtrs[fileDataType].last()->write("<End settings>\n");
    timeFilePtrs[fileDataType].last()->flush();


    //Create an output file for the neural data
    //*****************************************

    //Create the output files (one for each channel) and write header sections
    for (int nt=0; nt < spikeConf->ntrodes.length(); nt++) {
        for (int chInd = 0; chInd < spikeConf->ntrodes[nt]->hw_chan.length(); chInd++) {
            bool saveThisChannel = false;
            if (spikeConf->ntrodes[nt]->lfpDataChan == chInd) {
                saveThisChannel = true; //We are only saving the designated LFP channel in the ntrode
            }


            if (saveThisChannel) {
                outputFilePtrs[fileDataType].push_back(new QFile);


                outputFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".LFP_nt%1ch%2.dat").arg(spikeConf->ntrodes[nt]->nTrodeId).arg(chInd+1));
                if (!outputFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
                    qDebug() << "Error creating output file for" << fileTypeExtension << "NTrode" << spikeConf->ntrodes[nt]->nTrodeId << "Channel" << chInd+1;
                    if (outputFilePtrs[fileDataType].last()->error() == 4) {
                        qDebug() << "Too many files open simultaneously.";
                    }

                    return false;
                }
                outputStreamPtrs[fileDataType].push_back(new QDataStream(outputFilePtrs[fileDataType].last()));
                outputStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);

                //Write the current settings to file
                writeHeaderInfo(outputFilePtrs[fileDataType].last(),QString("LFP data for one channel\r\n"));

                infoLine = QString("nTrode_ID: %1\r\n").arg(spikeConf->ntrodes[nt]->nTrodeId);
                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());
                infoLine = QString("nTrode_channel_number: %1\r\n").arg(spikeConf->ntrodes[nt]->lfpDataChan + 1);
                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());
                infoLine = QString("num_channels: %1\r\n").arg(spikeConf->ntrodes[nt]->hw_chan.length());
                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

                infoLine = QString("Voltage_scaling: %1\r\n").arg(spikeConf->ntrodes[nt]->spike_scaling_to_uV);
                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());



                if (spikeConf->ntrodes[nt]->lfpRefOn) {
                    if (spikeConf->ntrodes[nt]->groupRefOn) {
                        infoLine = QString("Reference: Common average reference group %1\r\n").arg(spikeConf->ntrodes[nt]->refGroup);
                    } else {
                        infoLine = QString("Reference: on \nReferenceNTrode: %1\nReferenceChannel: %2\r\n").arg(spikeConf->ntrodes[nt]->refNTrodeID).arg(spikeConf->ntrodes[nt]->refChan+1);
                    }
                } else {
                    infoLine = QString("Reference: off\r\n");
                }

                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

                if (spikeConf->ntrodes[nt]->lfpFilterOn) {
                    infoLine = QString("Low_pass_filter: %1\r\n").arg(spikeConf->ntrodes[nt]->lfpHighFilter);

                } else {
                    infoLine = QString("Low_pass_filter: none\r\n");
                }
                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());


                fieldLine.clear();
                fieldLine += "Fields: ";
                fieldLine += "<voltage int16>";
                fieldLine += "\r\n";
                outputFilePtrs[fileDataType].last()->write(fieldLine.toLocal8Bit());
                outputFilePtrs[fileDataType].last()->write("<End settings>\n");
                outputFilePtrs[fileDataType].last()->flush();
            }
        }
    }
    return true;
}


bool DATExportHandler::createTimeOutputFiles()
{

    int fileDataType = fTime;
    QString fileTypeExtension = fileTypeNames.at(fileDataType);

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+"."+fileTypeExtension+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating" << fileTypeExtension <<  "directory.";
            return false;
        }
    }


    QString infoLine;
    QString fieldLine;

    //Create an output file for the timestamps
    //*****************************************
    timeFilePtrs[fileDataType].push_back(new QFile);
    timeFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".timestamps.dat"));
    if (!timeFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating time file.";
        return false;
    }
    timeStreamPtrs[fileDataType].push_back(new QDataStream(timeFilePtrs[fileDataType].last()));
    timeStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);


    //Write the current settings to file
    writeHeaderInfo(timeFilePtrs[fileDataType].last(),QString("Timestamps\r\n"));

    infoLine = QString("Decimation: %1\n").arg(decimation);
    timeFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

    fieldLine.clear();
    fieldLine += "Fields: ";
    fieldLine += "<time uint32>";
    if (hardwareConf->sysTimeIncluded) {
        fieldLine += "<systime int64>";
    }
    fieldLine += "\r\n";
    timeFilePtrs[fileDataType].last()->write(fieldLine.toLocal8Bit());
    timeFilePtrs[fileDataType].last()->write("<End settings>\n");
    timeFilePtrs[fileDataType].last()->flush();



    return true;
}


bool DATExportHandler::createRawOutputFiles()
{

    int fileDataType = fRawBand;
    QString fileTypeExtension = fileTypeNames.at(fileDataType);

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+"."+fileTypeExtension+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating" << fileTypeExtension <<  "directory.";
            return false;
        }
    }


    QString infoLine;
    QString fieldLine;

    //Create an output file for the timestamps
    //*****************************************
    timeFilePtrs[fileDataType].push_back(new QFile);
    timeFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".timestamps.dat"));
    if (!timeFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating time file for continuous raw band data.";
        return false;
    }
    timeStreamPtrs[fileDataType].push_back(new QDataStream(timeFilePtrs[fileDataType].last()));
    timeStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);

    //Write the current settings to file
    //Write the current settings to file
    writeHeaderInfo(timeFilePtrs[fileDataType].last(),QString("Raw timestamps\r\n"));



    fieldLine.clear();
    fieldLine += "Fields: ";
    fieldLine += "<time uint32>";
    fieldLine += "\r\n";
    timeFilePtrs[fileDataType].last()->write(fieldLine.toLocal8Bit());
    timeFilePtrs[fileDataType].last()->write("<End settings>\n");
    timeFilePtrs[fileDataType].last()->flush();


    if (spikeConf->deviceType == "neuropixels1") {
        int channelsProcessed = 0;

        for (int i=0; i<streamConf->pageBreaks.length(); i++) {

            outputFilePtrs[fileDataType].push_back(new QFile);
            outputFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".raw_probe%1.dat").arg(i+1));

            if (!outputFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
                qDebug() << "Error creating output file.";
                if (outputFilePtrs[fileDataType].last()->error() == 4) {
                    qDebug() << "Too many files open simultaneously.";
                }
                return false;
            }
            outputStreamPtrs[fileDataType].push_back(new QDataStream(outputFilePtrs[fileDataType].last()));
            outputStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);
            //Write the current settings to file
            writeHeaderInfo(outputFilePtrs[fileDataType].last(),QString("Raw (unfiltered) data for one probe\r\n"));

            QFile tmpChannelMapFile;

            tmpChannelMapFile.setFileName(saveLocation+fileBaseName+QString(".channelmap_probe%1.dat").arg(i+1));
            if (!tmpChannelMapFile.open(QIODevice::WriteOnly)) {
                qDebug() << "Error creating output file.";
                if (tmpChannelMapFile.error() == 4) {
                    qDebug() << "Too many files open simultaneously.";
                }
                return false;
            }
            QDataStream tmpChannelMapStream(&tmpChannelMapFile);
            tmpChannelMapStream.setByteOrder(QDataStream::LittleEndian);
            //Write the current settings to file
            writeHeaderInfo(&tmpChannelMapFile,QString("Pad locations in microns\r\n"));

            fieldLine.clear();
            fieldLine += "Fields: ";
            fieldLine += "<ml int32><dv int32><ap int32>";
            fieldLine += "\r\n";
            tmpChannelMapFile.write(fieldLine.toLocal8Bit());
            tmpChannelMapFile.write("<End settings>\n");


            int entriesToWrite;
            if (i < (streamConf->pageBreaks.length()-1)) {
                entriesToWrite = streamConf->pageBreaks.at(i)-channelsProcessed;
            } else {
                entriesToWrite = spikeConf->ntrodes.length() - channelsProcessed;
            }

            for (int ntInd = channelsProcessed; ntInd < channelsProcessed+entriesToWrite; ntInd++) {
                //qDebug() << (int32_t)spikeConf->ntrodes.at(ntInd)->coord_ml.at(0) << (int32_t)spikeConf->ntrodes.at(ntInd)->coord_dv.at(0);
                tmpChannelMapStream << (int32_t)spikeConf->ntrodes.at(ntInd)->coord_ml.at(0); //ml coord
                tmpChannelMapStream << (int32_t)spikeConf->ntrodes.at(ntInd)->coord_dv.at(0); //dv coord
                tmpChannelMapStream << (int32_t)spikeConf->ntrodes.at(ntInd)->coord_ap.at(0); //ap coord
            }
            channelsProcessed+=entriesToWrite;
            tmpChannelMapFile.flush();
            tmpChannelMapFile.close();

        }
    } else if (sortingMode == SpikeSortingGroupMode::OneGroup) {
        //Create a single output file and write header sections

        outputFilePtrs[fileDataType].push_back(new QFile);
        outputFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".raw_combined.dat"));
        if (!outputFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
            qDebug() << "Error creating output file.";
            if (outputFilePtrs[fileDataType].last()->error() == 4) {
                qDebug() << "Too many files open simultaneously.";
            }
            return false;
        }
        outputStreamPtrs[fileDataType].push_back(new QDataStream(outputFilePtrs[fileDataType].last()));
        outputStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);
        //Write the current settings to file
        writeHeaderInfo(outputFilePtrs[fileDataType].last(),QString("Raw (unfiltered) data with all channels combinedl\r\n"));
        infoLine = QString("num_channels: %1\r\n").arg(numChannelsInFile);
        outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

        if (spikeConf->ntrodes.length() > 0) {
            infoLine = QString("Voltage_scaling: %1\r\n").arg(spikeConf->ntrodes[0]->spike_scaling_to_uV);
            outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());
        }
        fieldLine.clear();
        fieldLine += "Fields: ";

        fieldLine += "<voltage ";
        fieldLine += QString("%1").arg(numChannelsInFile);
        fieldLine += "*int16>";

        //fieldLine += "<voltage int16>";
        fieldLine += "\r\n";
        outputFilePtrs[fileDataType].last()->write(fieldLine.toLocal8Bit());
        outputFilePtrs[fileDataType].last()->write("<End settings>\n");
        outputFilePtrs[fileDataType].last()->flush();

        QVector<int> ntIndices;
        QVector<int> chIndices;
        for (int nt=0; nt<spikeConf->ntrodes.length(); nt++) {
            for (int ch=0; ch<spikeConf->ntrodes.at(nt)->hw_chan.length(); ch++) {
                ntIndices.push_back(nt);
                chIndices.push_back(ch);
            }
        }
        if (!createChannelLocationFile(saveLocation+fileBaseName+QString(".coordinates.dat"), ntIndices, chIndices)) {
            return false;
        }

    } else if (sortingMode == SpikeSortingGroupMode::ByNtrodes) {

        //Create the output files (one for each nTrode) and write header sections
        for (int i=0; i<spikeConf->ntrodes.length(); i++) {

            int nt = i;
            outputFilePtrs[fileDataType].push_back(new QFile);
            outputFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".raw_nt%1.dat").arg(spikeConf->ntrodes[i]->nTrodeId));

            if (!outputFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
                qDebug() << "Error creating output file.";
                if (outputFilePtrs[fileDataType].last()->error() == 4) {
                    qDebug() << "Too many files open simultaneously.";
                }
                return false;
            }
            outputStreamPtrs[fileDataType].push_back(new QDataStream(outputFilePtrs[fileDataType].last()));
            outputStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);
            //Write the current settings to file
            writeHeaderInfo(outputFilePtrs[fileDataType].last(),QString("Raw (unfiltered) data for one nTrode\r\n"));
            infoLine = QString("nTrode_ID: %1\r\n").arg(spikeConf->ntrodes[nt]->nTrodeId);
            outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());
            infoLine = QString("num_channels: %1\r\n").arg(spikeConf->ntrodes[nt]->hw_chan.length());
            outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

            infoLine = QString("Voltage_scaling: %1\r\n").arg(spikeConf->ntrodes[nt]->spike_scaling_to_uV);
            outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());



            if (spikeConf->ntrodes[nt]->rawRefOn) {
                if (spikeConf->ntrodes[nt]->groupRefOn) {
                    infoLine = QString("Reference: Common average reference group %1\r\n").arg(spikeConf->ntrodes[nt]->refGroup);
                } else {
                    infoLine = QString("Reference: on \nReferenceNTrode: %1\nReferenceChannel: %2\r\n").arg(spikeConf->ntrodes[nt]->refNTrodeID).arg(spikeConf->ntrodes[nt]->refChan+1);
                }
            } else {
                infoLine = QString("Reference: off\r\n");
            }

            outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

            fieldLine.clear();
            fieldLine += "Fields: ";
            fieldLine += "<voltage ";
            fieldLine += QString("%1").arg(spikeConf->ntrodes[nt]->hw_chan.length());
            fieldLine += "*int16>";
            fieldLine += "\r\n";
            outputFilePtrs[fileDataType].last()->write(fieldLine.toLocal8Bit());
            outputFilePtrs[fileDataType].last()->write("<End settings>\n");
            outputFilePtrs[fileDataType].last()->flush();

            QVector<int> ntIndices;
            QVector<int> chIndices;
            for (int ch=0; ch<spikeConf->ntrodes.at(i)->hw_chan.length(); ch++) {
                ntIndices.push_back(i);
                chIndices.push_back(ch);
            }
            if (!createChannelLocationFile(saveLocation+fileBaseName+QString(".nt%1.coordinates.dat").arg(spikeConf->ntrodes[i]->nTrodeId), ntIndices, chIndices)) {
                return false;
            }

        }


    } else if (sortingMode == SpikeSortingGroupMode::BySortingGroup) {
        //Create the output files (one for each sorting group) and write header sections
        int maxSortingGroupNum = 0;
        for (int nt=0; nt<spikeConf->ntrodes.length(); nt++) {
            for (int ch=0; ch<spikeConf->ntrodes.at(nt)->spikeSortingGroup.length();ch++) {
                if (spikeConf->ntrodes.at(nt)->spikeSortingGroup.at(ch) > maxSortingGroupNum) {
                    maxSortingGroupNum = spikeConf->ntrodes.at(nt)->spikeSortingGroup.at(ch);
                }
            }
        }


        for (int sg=0; sg <= maxSortingGroupNum; sg++) {

            QVector<int> ntIndices;
            QVector<int> chIndices;

            for (int nt=0; nt<spikeConf->ntrodes.length(); nt++) {
                for (int ch=0; ch<spikeConf->ntrodes.at(nt)->spikeSortingGroup.length(); ch++) {
                    if (spikeConf->ntrodes.at(nt)->spikeSortingGroup.at(ch) == sg) {
                        ntIndices.push_back(nt);
                        chIndices.push_back(ch);
                    }
                }
            }

            outputFilePtrs[fileDataType].push_back(new QFile);
            outputFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".raw_group%1.dat").arg(sg));

            if (!outputFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
                qDebug() << "Error creating output file.";
                if (outputFilePtrs[fileDataType].last()->error() == 4) {
                    qDebug() << "Too many files open simultaneously.";
                }
                return false;
            }
            outputStreamPtrs[fileDataType].push_back(new QDataStream(outputFilePtrs[fileDataType].last()));
            outputStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);
            //Write the current settings to file
            writeHeaderInfo(outputFilePtrs[fileDataType].last(),QString("Raw (unfiltered) data for one sorting group\r\n"));
            infoLine = QString("sorting_group: %1\r\n").arg(sg);
            outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());
            infoLine = QString("num_channels: %1\r\n").arg(ntIndices.length());
            outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

            if (spikeConf->ntrodes.length() > 0) {
                infoLine = QString("Voltage_scaling: %1\r\n").arg(spikeConf->ntrodes[0]->spike_scaling_to_uV);
                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());
            }

            fieldLine.clear();
            fieldLine += "Fields: ";
            fieldLine += "<voltage ";
            fieldLine += QString("%1").arg(ntIndices.length());
            fieldLine += "*int16>";
            fieldLine += "\r\n";
            outputFilePtrs[fileDataType].last()->write(fieldLine.toLocal8Bit());
            outputFilePtrs[fileDataType].last()->write("<End settings>\n");
            outputFilePtrs[fileDataType].last()->flush();


            if (!createChannelLocationFile(saveLocation+fileBaseName+QString(".group%1.coordinates.dat").arg(sg), ntIndices, chIndices)) {
                return false;
            }

        }

    } else if (sortingMode == SpikeSortingGroupMode::SingleChannels) {
        //Create the output files (one for each channel) and write header sections
        for (int nt=0; nt < spikeConf->ntrodes.length(); nt++) {
            for (int chInd = 0; chInd < spikeConf->ntrodes[nt]->hw_chan.length(); chInd++) {
                bool saveThisChannel = true;


                if (saveThisChannel) {
                    outputFilePtrs[fileDataType].push_back(new QFile);


                    outputFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".raw_nt%1ch%2.dat").arg(spikeConf->ntrodes[nt]->nTrodeId).arg(chInd+1));
                    if (!outputFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
                        qDebug() << "Error creating output file for" << fileTypeExtension;
                        if (outputFilePtrs[fileDataType].last()->error() == 4) {
                            qDebug() << "Too many files open simultaneously.";
                        }

                        return false;
                    }
                    outputStreamPtrs[fileDataType].push_back(new QDataStream(outputFilePtrs[fileDataType].last()));
                    outputStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);

                    //Write the current settings to file
                    writeHeaderInfo(outputFilePtrs[fileDataType].last(),QString("Raw (unfiltered) data for one channel\r\n"));

                    infoLine = QString("nTrode_ID: %1\r\n").arg(spikeConf->ntrodes[nt]->nTrodeId);
                    outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());
                    infoLine = QString("num_channels: %1\r\n").arg(spikeConf->ntrodes[nt]->hw_chan.length());
                    outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

                    infoLine = QString("Voltage_scaling: %1\r\n").arg(spikeConf->ntrodes[nt]->spike_scaling_to_uV);
                    outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());



                    if (spikeConf->ntrodes[nt]->rawRefOn) {
                        if (spikeConf->ntrodes[nt]->groupRefOn) {
                            infoLine = QString("Reference: Common average reference group %1\r\n").arg(spikeConf->ntrodes[nt]->refGroup);
                        } else {
                            infoLine = QString("Reference: on \nReferenceNTrode: %1\nReferenceChannel: %2\r\n").arg(spikeConf->ntrodes[nt]->refNTrodeID).arg(spikeConf->ntrodes[nt]->refChan+1);
                        }
                    } else {
                        infoLine = QString("Reference: off\r\n");
                    }

                    outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());




                    if (spikeConf->ntrodes[nt]->lfpDataChan == chInd) {
                        infoLine = QString("Selected_LFP_channel: 1\r\n");
                    } else {
                        infoLine = QString("Selected_LFP_channel: 0\r\n");
                    }
                    outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

                    fieldLine.clear();
                    fieldLine += "Fields: ";
                    fieldLine += "<voltage int16>";
                    fieldLine += "\r\n";
                    outputFilePtrs[fileDataType].last()->write(fieldLine.toLocal8Bit());
                    outputFilePtrs[fileDataType].last()->write("<End settings>\n");
                    outputFilePtrs[fileDataType].last()->flush();
                }
            }
        }
    }


    return true;
}
bool DATExportHandler::createDIOOutputFiles()
{

    int fileDataType = fDigitalIO;
    QString fileTypeExtension = fileTypeNames.at(fileDataType);

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+"."+fileTypeExtension+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating" << fileTypeExtension <<  "directory.";
            return false;
        }
    }


    QString infoLine;
    QString fieldLine;

    for (int auxChInd = 0; auxChInd < headerConf->headerChannels.length(); auxChInd++) {

            if (headerConf->headerChannels[auxChInd].dataType == DeviceChannel::DIGITALTYPE) {

                outputFilePtrs[fileDataType].push_back(new QFile);
                outputFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".dio_%1.dat").arg(headerConf->headerChannels[auxChInd].idString));
                if (!outputFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
                    qDebug() << "Error creating output file for" << fileTypeExtension;
                    if (outputFilePtrs[fileDataType].last()->error() == 4) {
                        qDebug() << "Too many files open simultaneously.";
                    }
                    return false;
                }
                outputStreamPtrs[fileDataType].push_back(new QDataStream(outputFilePtrs[fileDataType].last()));
                outputStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);

                //Write the current settings to file
                writeHeaderInfo(outputFilePtrs[fileDataType].last(),QString("State change data for one digital channel. Display_order is 1-based\r\n"));

                if (headerConf->headerChannels[auxChInd].input) {
                    infoLine = QString("Direction: input\r\n");
                } else {
                    infoLine = QString("Direction: output\r\n");
                }
                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());
                infoLine = QString("ID: %1\r\n").arg(headerConf->headerChannels[auxChInd].idString);
                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());
                infoLine = QString("Display_order: %1\r\n").arg(auxChInd+1);
                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

                fieldLine.clear();
                fieldLine += "Fields: ";
                fieldLine += "<time uint32>";
                fieldLine += "<state uint8>";
                fieldLine += "\r\n";
                outputFilePtrs[fileDataType].last()->write(fieldLine.toLocal8Bit());


                outputFilePtrs[fileDataType].last()->write("<End settings>\n");
                outputFilePtrs[fileDataType].last()->flush();

            }
    }
    return true;

}
bool DATExportHandler::createAnalogIOOutputFiles()
{
    int fileDataType = fAnalogIO;
    QString fileTypeExtension = fileTypeNames.at(fileDataType);

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+"."+fileTypeExtension+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating" << fileTypeExtension <<  "directory.";
            return false;
        }
    }


    QString infoLine;
    QString fieldLine;

    //Create an output file for the timestamps
    //*****************************************
    timeFilePtrs[fileDataType].push_back(new QFile);
    timeFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".timestamps.dat"));
    if (!timeFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating time file.";
        return false;
    }
    timeStreamPtrs[fileDataType].push_back(new QDataStream(timeFilePtrs[fileDataType].last()));
    timeStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);

    //Write the current settings to file
    writeHeaderInfo(timeFilePtrs[fileDataType].last(),QString("Analog IO timestamps\r\n"));

    fieldLine.clear();
    fieldLine += "Fields: ";
    fieldLine += "<time uint32>";
    fieldLine += "\r\n";
    timeFilePtrs[fileDataType].last()->write(fieldLine.toLocal8Bit());
    timeFilePtrs[fileDataType].last()->write("<End settings>\n");
    timeFilePtrs[fileDataType].last()->flush();


    for (int auxChInd = 0; auxChInd < headerConf->headerChannels.length(); auxChInd++) {

            if (headerConf->headerChannels[auxChInd].dataType == DeviceChannel::INT16TYPE) {
                //This is an analog channel
                outputFilePtrs[fileDataType].push_back(new QFile);
                outputFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".analog_%1.dat").arg(headerConf->headerChannels[auxChInd].idString));
                if (!outputFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
                    qDebug() << "Error creating output file for" << fileTypeExtension;
                    if (outputFilePtrs[fileDataType].last()->error() == 4) {
                        qDebug() << "Too many files open simultaneously.";
                    }
                    return false;
                }
                outputStreamPtrs[fileDataType].push_back(new QDataStream(outputFilePtrs[fileDataType].last()));
                outputStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);

                //Write the current settings to file
                writeHeaderInfo(outputFilePtrs[fileDataType].last(),QString("One analog IO channel. Continuous sampling.\r\n"));

                if (headerConf->headerChannels[auxChInd].input) {
                    infoLine = QString("Direction: input\r\n");
                } else {
                    infoLine = QString("Direction: output\r\n");
                }
                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());
                infoLine = QString("ID: %1\r\n").arg(headerConf->headerChannels[auxChInd].idString);
                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());
                infoLine = QString("Display_order: %1\r\n").arg(auxChInd+1);
                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

                fieldLine.clear();
                fieldLine += "Fields: ";
                //fieldLine += "<time uint32>";
                fieldLine += "<voltage int16>";
                fieldLine += "\r\n";
                outputFilePtrs[fileDataType].last()->write(fieldLine.toLocal8Bit());

                outputFilePtrs[fileDataType].last()->write("<End settings>\n");
                outputFilePtrs[fileDataType].last()->flush();

            }
    }
    return true;
}
bool DATExportHandler::createSpikeEventOutputFiles()
{
    int fileDataType = fSpikeEvents;
    QString fileTypeExtension = fileTypeNames.at(fileDataType);

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+"."+fileTypeExtension+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating" << fileTypeExtension <<  "directory.";
            return false;
        }
    }

    QString infoLine;
    QString fieldLine;

    //Create an output files
    //*****************************************


    //Create the output files (one for each nTrode) and write header sections
    for (int i=0; i<spikeConf->ntrodes.length(); i++) {
        outputFilePtrs[fileDataType].push_back(new QFile);

        //neuroFilePtrs.last()->setFileName(saveLocation+QString("spikes_nt%1.dat").arg(spikeConf->ntrodes[i]->nTrodeId));
        outputFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".spikes_nt%1.dat").arg(spikeConf->ntrodes[i]->nTrodeId));
        if (!outputFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
            qDebug() << "Error creating output file for" << fileTypeExtension;
            if (outputFilePtrs[fileDataType].last()->error() == 4) {
                qDebug() << "Too many files open simultaneously.";
            }
            return false;
        }
        outputStreamPtrs[fileDataType].push_back(new QDataStream(outputFilePtrs[fileDataType].last()));
        outputStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);

        //Write the current settings to file
        writeHeaderInfo(outputFilePtrs[fileDataType].last(),QString("Spike waveforms for one nTrode\r\n"));

        infoLine = QString("nTrode_ID: %1\r\n").arg(spikeConf->ntrodes[i]->nTrodeId);
        outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());
        infoLine = QString("num_channels: %1\r\n").arg(spikeConf->ntrodes[i]->hw_chan.length());
        outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

        infoLine = QString("Voltage_scaling: %1\r\n").arg(spikeConf->ntrodes[i]->spike_scaling_to_uV);
        outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

        if (threshOverride) {
            infoLine = QString("Threshold: %1\r\n").arg(threshOverrideVal);
        } else {
            infoLine = QString("Threshold: %1\r\n").arg(spikeConf->ntrodes[i]->thresh[0]);
        }
        outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

        if (invertSpikes) {
            infoLine = QString("Spike_invert: yes\r\n");
        } else {
            infoLine = QString("Spike_invert: no\r\n");
        }
        outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

        if (spikeConf->ntrodes[i]->refOn) {
            if (spikeConf->ntrodes[i]->groupRefOn) {
                infoLine = QString("Reference: Common average reference group %1\r\n").arg(spikeConf->ntrodes[i]->refGroup);
            } else {
                infoLine = QString("Reference: on \nReferenceNTrode: %1\nReferenceChannel: %2\r\n").arg(spikeConf->ntrodes[i]->refNTrodeID).arg(spikeConf->ntrodes[i]->refChan+1);
            }
        } else {
            infoLine = QString("Reference: off\r\n");
        }
        outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

        if (spikeConf->ntrodes[i]->filterOn) {
            infoLine = QString("Filter: on \r\nlowPassFilter: %1\r\nhighPassFilter: %2\r\n").arg(spikeConf->ntrodes[i]->highFilter).arg(spikeConf->ntrodes[i]->lowFilter);

        }else if ((filterLowPass_SpikeBand != -1) || (filterHighPass_SpikeBand != -1) ) {
            infoLine = QString("Filter: on \r\nlowPassFilter: %1\r\nhighPassFilter: %2\r\n").arg(filterLowPass_SpikeBand).arg(filterHighPass_SpikeBand);

        } else {
            infoLine = QString("Filter: off \r\n");
        }

        outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());
        for (int ch=0;ch<spikeConf->ntrodes[i]->hw_chan.length();ch++) {
            infoLine = QString("Spike_trigger_on_%1: %2\r\n").arg(ch+1).arg(spikeConf->ntrodes[i]->triggerOn[ch]);
            outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());
        }


        fieldLine.clear();
        fieldLine += "Fields: ";
        fieldLine += "<time uint32>";

        for (int ch=0;ch<spikeConf->ntrodes[i]->hw_chan.length();ch++) {
            fieldLine += "<waveformCh";
            fieldLine += QString("%1").arg(ch+1);
            fieldLine += " 40*int16>";
        }
        fieldLine += "\r\n";
        outputFilePtrs[fileDataType].last()->write(fieldLine.toLocal8Bit());


        outputFilePtrs[fileDataType].last()->write("<End settings>\n");
        outputFilePtrs[fileDataType].last()->flush();
    }
    return true;
}
bool DATExportHandler::createStimulationOutputFiles()
{
    int fileDataType = fStimulation;
    QString fileTypeExtension = fileTypeNames.at(fileDataType);

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+"."+fileTypeExtension+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating" << fileTypeExtension <<  "directory.";
            return false;
        }
    }


    QString infoLine;
    QString fieldLine;

    //Create an output file for the timestamps
    //*****************************************
    timeFilePtrs[fileDataType].push_back(new QFile);
    timeFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".timestamps.dat"));
    if (!timeFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating time file.";
        return false;
    }
    timeStreamPtrs[fileDataType].push_back(new QDataStream(timeFilePtrs[fileDataType].last()));
    timeStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);

    //Write the current settings to file
    //Write the current settings to file
    writeHeaderInfo(timeFilePtrs[fileDataType].last(),QString("Stimulation timestamps\r\n"));

    fieldLine.clear();
    fieldLine += "Fields: ";
    fieldLine += "<time uint32>";
    fieldLine += "\r\n";
    timeFilePtrs[fileDataType].last()->write(fieldLine.toLocal8Bit());
    timeFilePtrs[fileDataType].last()->write("<End settings>\n");
    timeFilePtrs[fileDataType].last()->flush();


    //Create an output file for the neural data
    //*****************************************

    //Create the output files (one for each channel) and write header sections
    for (int nt=0; nt < spikeConf->ntrodes.length(); nt++) {
        for (int chInd = 0; chInd < spikeConf->ntrodes[nt]->hw_chan.length(); chInd++) {
            bool saveThisChannel = true;
            //we only create a file for the stim capable channels
            if (!spikeConf->ntrodes[nt]->stimCapable[chInd]) {
                saveThisChannel = false;
            }



            if (saveThisChannel) {
                outputFilePtrs[fileDataType].push_back(new QFile);


                outputFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".stimulation_nt%1ch%2.dat").arg(spikeConf->ntrodes[nt]->nTrodeId).arg(chInd+1));
                if (!outputFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
                    qDebug() << "Error creating output file for" << fileTypeExtension;
                    if (outputFilePtrs[fileDataType].last()->error() == 4) {
                        qDebug() << "Too many files open simultaneously.";
                    }
                    return false;
                }
                outputStreamPtrs[fileDataType].push_back(new QDataStream(outputFilePtrs[fileDataType].last()));
                outputStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);
                outputStreamPtrs[fileDataType].last()->setFloatingPointPrecision(QDataStream::SinglePrecision);

                //Write the current settings to file
                writeHeaderInfo(outputFilePtrs[fileDataType].last(),QString("Stimulation current for one channel in nA\r\n"));

                infoLine = QString("nTrode_ID: %1\r\n").arg(spikeConf->ntrodes[nt]->nTrodeId);
                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());
                infoLine = QString("num_channels: %1\r\n").arg(spikeConf->ntrodes[nt]->hw_chan.length());
                outputFilePtrs[fileDataType].last()->write(infoLine.toLocal8Bit());

                fieldLine.clear();
                fieldLine += "Fields: ";
                fieldLine += "<stim_current single>";
                fieldLine += "\r\n";
                outputFilePtrs[fileDataType].last()->write(fieldLine.toLocal8Bit());
                outputFilePtrs[fileDataType].last()->write("<End settings>\n");
                outputFilePtrs[fileDataType].last()->flush();
            }
        }
    }
    return true;
}
bool DATExportHandler::createEventsOutputFiles()
{
    return false;
}
bool DATExportHandler::createMountainSortOutputFiles()
{
    int fileDataType = fMountainSort;
    QString fileTypeExtension = fileTypeNames.at(fileDataType);

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+"."+fileTypeExtension+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating" << fileTypeExtension <<  "directory.";
            return false;
        }
    }


    QString infoLine;
    QString fieldLine;

    //Create an output file for the timestamps
    //*****************************************
    timeFilePtrs[fileDataType].push_back(new QFile);
    timeFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".timestamps.mda"));
    if (!timeFilePtrs[fileDataType].last()->open(QIODevice::ReadWrite)) {
        qDebug() << "Error creating time file.";
        return false;
    }
    timeStreamPtrs[fileDataType].push_back(new QDataStream(timeFilePtrs[fileDataType].last()));
    timeStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);



    //Write the current settings to file
    qint32 tDataType = -8; //-8 now means uint32 datatype
    qint32 tByteSize = 4; //Not used in mda script, but just in case.
    qint32 tArrayDim = 1;  //The dataset will be M by 1, which is 1-dim
    qint32 tDim1 = 0; //This is the M component, which we don't know yet.

    *timeStreamPtrs[fileDataType].last() << tDataType << tByteSize << tArrayDim << tDim1;
    timeFilePtrs[fileDataType].last()->flush();

    //Create an output file for the neural data
    //*****************************************

    if (sortingMode == SpikeSortingGroupMode::OneGroup) {
        //Create a single output file and write header sections

        outputFilePtrs[fileDataType].push_back(new QFile);
        outputFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".combined.mda"));

        if (!outputFilePtrs[fileDataType].last()->open(QIODevice::ReadWrite)) {
            qDebug() << "Error creating output file.";
            if (outputFilePtrs[fileDataType].last()->error() == 4) {
                qDebug() << "Too many files open simultaneously.";
            }
            return false;
        }
        outputStreamPtrs[fileDataType].push_back(new QDataStream(outputFilePtrs[fileDataType].last()));
        outputStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);

        //Write the current settings to file
        qint32 dataType = -4; //-4 means int16 datatype
        qint32 byteSize = 2; //Redundant
        qint32 arrayDim = 2;  //The dataset will be M by N, which is 2-dim
        qint32 dim1;
        dim1 = numChannelsInFile;

        qint32 dim2 = 0;  //This is the N component, which we don't know yet.

        *outputStreamPtrs[fileDataType].last() << dataType << byteSize << arrayDim << dim1 << dim2;

        //Create the coordinate file
        QVector<int> ntIndices;
        QVector<int> chIndices;
        for (int nt=0; nt<spikeConf->ntrodes.length(); nt++) {
            for (int ch=0; ch<spikeConf->ntrodes.at(nt)->hw_chan.length(); ch++) {
                ntIndices.push_back(nt);
                chIndices.push_back(ch);
            }
        }
        if (!createChannelLocationFile(saveLocation+fileBaseName+QString(".coordinates.dat"), ntIndices, chIndices)) {
            return false;
        }


    } else if (sortingMode == SpikeSortingGroupMode::ByNtrodes) {
        //Create the output files (one for each nTrode) and write header sections
        for (int i=0; i<spikeConf->ntrodes.length(); i++) {


            outputFilePtrs[fileDataType].push_back(new QFile);
            outputFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".nt%1.mda").arg(spikeConf->ntrodes[i]->nTrodeId));

            if (!outputFilePtrs[fileDataType].last()->open(QIODevice::ReadWrite)) {
                qDebug() << "Error creating output file.";
                if (outputFilePtrs[fileDataType].last()->error() == 4) {
                    qDebug() << "Too many files open simultaneously.";
                }
                return false;
            }
            outputStreamPtrs[fileDataType].push_back(new QDataStream(outputFilePtrs[fileDataType].last()));
            outputStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);

            //Write the current settings to file
            qint32 dataType = -4; //-4 means int16 datatype
            qint32 byteSize = 2; //Redundant
            qint32 arrayDim = 2;  //The dataset will be M by N, which is 2-dim
            qint32 dim1;
            dim1 = spikeConf->ntrodes[i]->hw_chan.length(); // num channels in nTrode
            qint32 dim2 = 0;  //This is the N component, which we don't know yet.

            *outputStreamPtrs[fileDataType].last() << dataType << byteSize << arrayDim << dim1 << dim2;

            //Create the coordinate file
            QVector<int> ntIndices;
            QVector<int> chIndices;
            for (int ch=0; ch<spikeConf->ntrodes.at(i)->hw_chan.length(); ch++) {
                ntIndices.push_back(i);
                chIndices.push_back(ch);
            }
            if (!createChannelLocationFile(saveLocation+fileBaseName+QString(".nt%1.coordinates.dat").arg(spikeConf->ntrodes[i]->nTrodeId), ntIndices, chIndices)) {
                return false;
            }

        }
    } else if (sortingMode == SpikeSortingGroupMode::BySortingGroup) {
        //Create the output files (one for each sorting group) and write header sections
        int maxSortingGroupNum = 0;
        for (int nt=0; nt<spikeConf->ntrodes.length(); nt++) {
            for (int ch=0; ch<spikeConf->ntrodes.at(nt)->spikeSortingGroup.length();ch++) {
                if (spikeConf->ntrodes.at(nt)->spikeSortingGroup.at(ch) > maxSortingGroupNum) {
                    maxSortingGroupNum = spikeConf->ntrodes.at(nt)->spikeSortingGroup.at(ch);
                }
            }
        }


        for (int sg=0; sg <= maxSortingGroupNum; sg++) {


            outputFilePtrs[fileDataType].push_back(new QFile);
            outputFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".group%1.mda").arg(sg));

            if (!outputFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
                qDebug() << "Error creating output file.";
                if (outputFilePtrs[fileDataType].last()->error() == 4) {
                    qDebug() << "Too many files open simultaneously.";
                }
                return false;
            }
            outputStreamPtrs[fileDataType].push_back(new QDataStream(outputFilePtrs[fileDataType].last()));
            outputStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);


            QVector<int> ntIndices;
            QVector<int> chIndices;

            for (int nt=0; nt<spikeConf->ntrodes.length(); nt++) {
                for (int ch=0; ch<spikeConf->ntrodes.at(nt)->spikeSortingGroup.length(); ch++) {
                    if (spikeConf->ntrodes.at(nt)->spikeSortingGroup.at(ch) == sg) {
                        ntIndices.push_back(nt);
                        chIndices.push_back(ch);
                    }
                }
            }

            //Write the current settings to file
            qint32 dataType = -4; //-4 means int16 datatype
            qint32 byteSize = 2; //Redundant
            qint32 arrayDim = 2;  //The dataset will be M by N, which is 2-dim
            qint32 dim1;
            dim1 = chIndices.length(); // num channels in the group
            qint32 dim2 = 0;  //This is the N component, which we don't know yet.

            *outputStreamPtrs[fileDataType].last() << dataType << byteSize << arrayDim << dim1 << dim2;


            if (!createChannelLocationFile(saveLocation+fileBaseName+QString(".group%1.coordinates.dat").arg(sg), ntIndices, chIndices)) {
                return false;
            }

        }
    } else if (sortingMode == SpikeSortingGroupMode::SingleChannels) {
        qDebug() << "Single channel sort mode not yet supported with MountainSort export.";
        return false;
    }


    return true;
}

bool DATExportHandler::createChannelLocationFile(QString fileName, QVector<int> ntIndices,QVector<int> chIndices)
{
    //This function is used to write a file that contains the coordinates in the brain
    //for the chosen ntrodes and channels within those nTrodes. The two index vectors should be the same length.


    QFile tmpChannelMapFile;

    tmpChannelMapFile.setFileName(fileName);
    if (!tmpChannelMapFile.open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating output file.";
        if (tmpChannelMapFile.error() == 4) {
            qDebug() << "Too many files open simultaneously.";
        }
        return false;
    }
    QDataStream tmpChannelMapStream(&tmpChannelMapFile);
    tmpChannelMapStream.setByteOrder(QDataStream::LittleEndian);
    //Write the current settings to file
    writeHeaderInfo(&tmpChannelMapFile,QString("Pad locations in microns\r\n"));

    QString infoLine;
    QString fieldLine;

    fieldLine.clear();
    fieldLine += "Fields: ";
    fieldLine += "<ml int32><dv int32><ap int32>";
    fieldLine += "\r\n";
    tmpChannelMapFile.write(fieldLine.toLocal8Bit());
    tmpChannelMapFile.write("<End settings>\n");

    for (int ind = 0; ind < ntIndices.length(); ind++) {
        //qDebug() << (int32_t)spikeConf->ntrodes.at(ntInd)->coord_ml.at(0) << (int32_t)spikeConf->ntrodes.at(ntInd)->coord_dv.at(0);
        tmpChannelMapStream << (int32_t)spikeConf->ntrodes.at(ntIndices[ind])->coord_ml.at(chIndices[ind]); //ml coord
        tmpChannelMapStream << (int32_t)spikeConf->ntrodes.at(ntIndices[ind])->coord_dv.at(chIndices[ind]); //dv coord
        tmpChannelMapStream << (int32_t)spikeConf->ntrodes.at(ntIndices[ind])->coord_ap.at(chIndices[ind]); //ap coord
    }

    tmpChannelMapFile.flush();
    tmpChannelMapFile.close();

    return true;

}

bool DATExportHandler::createKiloSortOutputFiles()
{
    int fileDataType = fKiloSort;
    QString fileTypeExtension = fileTypeNames.at(fileDataType);

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+"."+fileTypeExtension+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating" << fileTypeExtension <<  "directory.";
            return false;
        }
    }


    QString infoLine;
    QString fieldLine;

    //Create an output file for the timestamps
    //*****************************************
    timeFilePtrs[fileDataType].push_back(new QFile);
    timeFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".timestamps.dat"));
    if (!timeFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating time file.";
        return false;
    }
    timeStreamPtrs[fileDataType].push_back(new QDataStream(timeFilePtrs[fileDataType].last()));
    timeStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);


    //Write the current settings to file
    writeHeaderInfo(timeFilePtrs[fileDataType].last(),QString("Sample timestamps\r\n"));

    fieldLine.clear();
    fieldLine += "Fields: ";
    fieldLine += "<time uint32>";
    fieldLine += "\r\n";
    timeFilePtrs[fileDataType].last()->write(fieldLine.toLocal8Bit());
    timeFilePtrs[fileDataType].last()->write("<End settings>\n");
    timeFilePtrs[fileDataType].last()->flush();


    if (spikeConf->deviceType == "neuropixels1") {
        int channelsProcessed = 0;

        for (int i=0; i<streamConf->pageBreaks.length(); i++) {

            outputFilePtrs[fileDataType].push_back(new QFile);
            outputFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".probe%1.dat").arg(i+1));

            if (!outputFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
                qDebug() << "Error creating output file.";
                if (outputFilePtrs[fileDataType].last()->error() == 4) {
                    qDebug() << "Too many files open simultaneously.";
                }
                return false;
            }
            outputStreamPtrs[fileDataType].push_back(new QDataStream(outputFilePtrs[fileDataType].last()));
            outputStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);

            QFile tmpChannelMapFile;

            tmpChannelMapFile.setFileName(saveLocation+fileBaseName+QString(".channelmap_probe%1.dat").arg(i+1));
            if (!tmpChannelMapFile.open(QIODevice::WriteOnly)) {
                qDebug() << "Error creating output file.";
                if (tmpChannelMapFile.error() == 4) {
                    qDebug() << "Too many files open simultaneously.";
                }
                return false;
            }
            QDataStream tmpChannelMapStream(&tmpChannelMapFile);
            tmpChannelMapStream.setByteOrder(QDataStream::LittleEndian);
            //Write the current settings to file
            writeHeaderInfo(&tmpChannelMapFile,QString("Pad locations in microns\r\n"));

            fieldLine.clear();
            fieldLine += "Fields: ";
            fieldLine += "<ml int32><dv int32><ap int32>";
            fieldLine += "\r\n";
            tmpChannelMapFile.write(fieldLine.toLocal8Bit());
            tmpChannelMapFile.write("<End settings>\n");


            int entriesToWrite;
            if (i < (streamConf->pageBreaks.length()-1)) {
                entriesToWrite = streamConf->pageBreaks.at(i)-channelsProcessed;
            } else {
                entriesToWrite = spikeConf->ntrodes.length() - channelsProcessed;
            }

            for (int ntInd = channelsProcessed; ntInd < channelsProcessed+entriesToWrite; ntInd++) {
                //qDebug() << (int32_t)spikeConf->ntrodes.at(ntInd)->coord_ml.at(0) << (int32_t)spikeConf->ntrodes.at(ntInd)->coord_dv.at(0);
                tmpChannelMapStream << (int32_t)spikeConf->ntrodes.at(ntInd)->coord_ml.at(0); //ml coord
                tmpChannelMapStream << (int32_t)spikeConf->ntrodes.at(ntInd)->coord_dv.at(0); //dv coord
                tmpChannelMapStream << (int32_t)spikeConf->ntrodes.at(ntInd)->coord_ap.at(0); //ap coord
            }
            channelsProcessed+=entriesToWrite;
            tmpChannelMapFile.flush();
            tmpChannelMapFile.close();

        }
    } else if (sortingMode == SpikeSortingGroupMode::OneGroup) {
        //Create a single output file and write header sections

        outputFilePtrs[fileDataType].push_back(new QFile);
        outputFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".combined.dat"));
        if (!outputFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
            qDebug() << "Error creating output file.";
            if (outputFilePtrs[fileDataType].last()->error() == 4) {
                qDebug() << "Too many files open simultaneously.";
            }
            return false;
        }
        outputStreamPtrs[fileDataType].push_back(new QDataStream(outputFilePtrs[fileDataType].last()));
        outputStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);

        QVector<int> ntIndices;
        QVector<int> chIndices;
        for (int nt=0; nt<spikeConf->ntrodes.length(); nt++) {
            for (int ch=0; ch<spikeConf->ntrodes.at(nt)->hw_chan.length(); ch++) {
                ntIndices.push_back(nt);
                chIndices.push_back(ch);
            }
        }
        if (!createChannelLocationFile(saveLocation+fileBaseName+QString(".coordinates.dat"), ntIndices, chIndices)) {
            return false;
        }

    } else if (sortingMode == SpikeSortingGroupMode::ByNtrodes) {

        //Create the output files (one for each nTrode) and write header sections
        for (int i=0; i<spikeConf->ntrodes.length(); i++) {


            outputFilePtrs[fileDataType].push_back(new QFile);
            outputFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".nt%1.dat").arg(spikeConf->ntrodes[i]->nTrodeId));

            if (!outputFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
                qDebug() << "Error creating output file.";
                if (outputFilePtrs[fileDataType].last()->error() == 4) {
                    qDebug() << "Too many files open simultaneously.";
                }
                return false;
            }
            outputStreamPtrs[fileDataType].push_back(new QDataStream(outputFilePtrs[fileDataType].last()));
            outputStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);

            QVector<int> ntIndices;
            QVector<int> chIndices;
            for (int ch=0; ch<spikeConf->ntrodes.at(i)->hw_chan.length(); ch++) {
                ntIndices.push_back(i);
                chIndices.push_back(ch);
            }
            if (!createChannelLocationFile(saveLocation+fileBaseName+QString(".nt%1.coordinates.dat").arg(spikeConf->ntrodes[i]->nTrodeId), ntIndices, chIndices)) {
                return false;
            }

        }


    } else if (sortingMode == SpikeSortingGroupMode::BySortingGroup) {
        //Create the output files (one for each sorting group) and write header sections
        int maxSortingGroupNum = 0;
        for (int nt=0; nt<spikeConf->ntrodes.length(); nt++) {
            for (int ch=0; ch<spikeConf->ntrodes.at(nt)->spikeSortingGroup.length();ch++) {
                if (spikeConf->ntrodes.at(nt)->spikeSortingGroup.at(ch) > maxSortingGroupNum) {
                    maxSortingGroupNum = spikeConf->ntrodes.at(nt)->spikeSortingGroup.at(ch);
                }
            }
        }


        for (int sg=0; sg <= maxSortingGroupNum; sg++) {


            outputFilePtrs[fileDataType].push_back(new QFile);
            outputFilePtrs[fileDataType].last()->setFileName(saveLocation+fileBaseName+QString(".group%1.dat").arg(sg));

            if (!outputFilePtrs[fileDataType].last()->open(QIODevice::WriteOnly)) {
                qDebug() << "Error creating output file.";
                if (outputFilePtrs[fileDataType].last()->error() == 4) {
                    qDebug() << "Too many files open simultaneously.";
                }
                return false;
            }
            outputStreamPtrs[fileDataType].push_back(new QDataStream(outputFilePtrs[fileDataType].last()));
            outputStreamPtrs[fileDataType].last()->setByteOrder(QDataStream::LittleEndian);

            QVector<int> ntIndices;
            QVector<int> chIndices;

            for (int nt=0; nt<spikeConf->ntrodes.length(); nt++) {
                for (int ch=0; ch<spikeConf->ntrodes.at(nt)->spikeSortingGroup.length(); ch++) {
                    if (spikeConf->ntrodes.at(nt)->spikeSortingGroup.at(ch) == sg) {
                        ntIndices.push_back(nt);
                        chIndices.push_back(ch);
                    }
                }
            }
            if (!createChannelLocationFile(saveLocation+fileBaseName+QString(".group%1.coordinates.dat").arg(sg), ntIndices, chIndices)) {
                return false;
            }

        }

    } else if (sortingMode == SpikeSortingGroupMode::SingleChannels) {
        qDebug() << "Single channel sort mode not yet supported with Kilosort export.";
        return false;
    }

    return true;

}


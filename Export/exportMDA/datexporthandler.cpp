#include "datexporthandler.h"
#include <algorithm>
//#include <iostream>

DATExportHandler::DATExportHandler(QStringList arguments):
    AbstractExportHandler(arguments)
{


    supportedDataCategories = DataCategories::SpikeBand | DataCategories::ContinuousOutput;

    parseArguments();

    /*
    Do custom argument checks here like this:
    if (ARGVAL != REQUIREMENT) {
        qDebug() << "Error: ....";
        argumentReadOk = false;
    }   
    */

    override_useSpikeFilters = true;
    useSpikeFilters = false; //turn off all spike band filtering by default


    //Parse custom arguments
    //parseCustomArguments(argumentsProcessed);

    if (argumentsProcessed != argumentList.length()-1) {
        _argumentsSupported = false;
        return;
    }
}

DATExportHandler::~DATExportHandler()
{

}

void DATExportHandler::printHelpMenu() {
    //printf("-outputrate <integer>  -- The sampling rate of the output file. \n"
    //       );

    printf("\nUsed to extract continuous spike band data from a raw rec file and save to a set of files in MDA format (used by MountainSort). \n It will apply whatever notch filter \n and spike band reference settings you have in the workspace by default unless a flag is provided to change the default behavior.\n No spike band filter is applied to the output data.\n");
    printf("NOTE: SPIKE BAND FILTERING IS TURNED OFF BY DEFAULT. \n");
    printf("Usage:  exportmda -rec INPUTFILENAME OPTION1 VALUE1 OPTION2 VALUE2 ...  \n\n"
           "Input arguments \n");
    printf("Defaults:\n"
           "    -outputrate -1 (full) \n"
           "    -usespikefilters 0"
           "    -interp -1 (inf) \n");


    //AbstractExportHandler::printHelpMenu();
}

void DATExportHandler::parseArguments() {
    //Parse extra arguments not handled by the base class

    int optionInd = 1;
    while (optionInd < argumentList.length()) {

        if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0)) {
            //printCustomMenu();
            //return;
            printHelpMenu();
        }
        optionInd++;
    }

    AbstractExportHandler::parseArguments();
}


int DATExportHandler::processData() {
    qDebug() << "Exporting to MDA format file...";

    //Calculate the packet positions for each channel that we are extracting, plus
    //other critical info (number of saved channels, reference info, etc).
    calculateChannelInfo();
    //createFilters();

    qint32 numPointsWrittenToFile = 0;

    if (!openInputFile()) {
        return -1;
    }


    //Create a directory for the output files located in the same place as the source file
    QFileInfo fi(recFileName);
    QString fileBaseName;
    if (outputFileName.isEmpty()) {
        fileBaseName = fi.completeBaseName();
    } else {
        fileBaseName = outputFileName;
    }

    QString directory;
    if(outputDirectory.isEmpty()) {
        directory = fi.absolutePath();
    } else {
        directory = outputDirectory;
    }

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+".mda"+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating MDA directory.";
            return -1;
        }
    }

    QList<QFile*> neuroFilePtrs;
    QList<QDataStream*> neuroStreamPtrs;

    QList<QFile*> timeFilePtrs;
    QList<QDataStream*> timeStreamPtrs;



    //Create an output file for the timestamps
    //*****************************************
    timeFilePtrs.push_back(new QFile);
    timeFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".timestamps.mda"));
    if (!timeFilePtrs.last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating output file.";
        return -1;
    }
    timeStreamPtrs.push_back(new QDataStream(timeFilePtrs.last()));
    timeStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

    //Write the current settings to file
    qint32 tDataType = -8; //-8 now means uint32 datatype
    qint32 tByteSize = 4; //Not used in mda script, but just in case.
    qint32 tArrayDim = 1;  //The dataset will be M by 1, which is 1-dim
    qint32 tDim1 = 0; //This is the M component, which we don't know yet.

    *timeStreamPtrs.last() << tDataType << tByteSize << tArrayDim << tDim1;


    //Create an output file for the neural data
    //*****************************************
    QVector<QVector<int16_t>* > tempDataHolder;  //Holds the data for each packet, organized in the correct order before writing to disk

    //Create the output files (one for each nTrode) and write header sections
    for (int i=0; i<spikeConf->ntrodes.length(); i++) {

        tempDataHolder.push_back(new QVector<int16_t>);
        tempDataHolder.last()->resize(spikeConf->ntrodes[i]->hw_chan.length());

        neuroFilePtrs.push_back(new QFile);

        neuroFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".nt%1.mda").arg(spikeConf->ntrodes[i]->nTrodeId));
        if (!neuroFilePtrs.last()->open(QIODevice::WriteOnly)) {
            qDebug() << "Error creating output file.";
            return -1;
        }
        neuroStreamPtrs.push_back(new QDataStream(neuroFilePtrs.last()));
        neuroStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

        //Write the current settings to file
        qint32 dataType = -4; //-4 means int16 datatype
        qint32 byteSize = 2; //Redundant
        qint32 arrayDim = 2;  //The dataset will be M by N, which is 2-dim
        qint32 dim1 = spikeConf->ntrodes[i]->hw_chan.length(); // num channels in nTrode
        qint32 dim2 = 0;  //This is the N component, which we don't know yet.


        *neuroStreamPtrs.last() << dataType << byteSize << arrayDim << dim1 << dim2;

        //neuroFilePtrs.last()->flush();
    }


    //************************************************

    int inputFileInd = 0;

    while (inputFileInd < recFileNameList.length()) {
        if (inputFileInd > 0) {
            //There are multiple files that need to be stiched together. It is assumed that they all have
            //the exact same header section.
            recFileName = recFileNameList.at(inputFileInd);
            uint32_t lastFileTStamp = currentTimeStamp;

            qDebug() << "\nAppending from file: " << recFileName;
            QFileInfo fi(recFileName);

            if (!fi.exists()) {
                qDebug() << "File could not be found: " << recFileName;
                break;
            }
            if (!openInputFile()) {
                qDebug() << "Error: it appears that the file does not have an identical header to the last file. Cannot append to file.";
                return -1;
            }
            for (int i=0; i < channelFilters.length(); i++) {
                channelFilters[i]->resetHistory();
            }
            if (currentTimeStamp < lastFileTStamp) {
                qDebug() << "Error: timestamps do not begin with greater value than the end of the last file. Aborting.";
                return -1;
            }

        }



        //Process the data and stream results to output files
        while(!filePtr->atEnd()) {

            if (!getNextPacket()) {
                //We have reached the end of the file
                break;
            }


            if ((currentTimeStamp%decimation) == 0) { //if we are subsampling the data, make sure this is a point we want to save in the first place
                //save data in time file
                *timeStreamPtrs.at(0) << currentTimeStamp; //Write the data to disk

                int fileInd = 0;
                for (int ntInd=0; ntInd < spikeConf->ntrodes.length(); ntInd++) {
                    const int16_t* tmpNtrodeData = neuralDataHandler->getNTrodeSamples(ntInd,AbstractNeuralDataHandler::SPIKE);
                    for (int chInd = 0; chInd < spikeConf->ntrodes[ntInd]->hw_chan.length(); chInd++) {
                          *neuroStreamPtrs.at(fileInd) << tmpNtrodeData[chInd]; //Write the data to disk
                    }
                    fileInd++;
                }
            }

            //Print the progress to stdout
            printProgress();

            lastTimeStampInFile = currentTimeStamp;


        }

        printf("\rDone\n");
        filePtr->close();
        inputFileInd++;
    }

    for (int i=0; i < neuroFilePtrs.length(); i++) {
        neuroFilePtrs[i]->flush();
        neuroFilePtrs[i]->seek(16);

        *neuroStreamPtrs.at(i) << numPointsWrittenToFile;

        neuroFilePtrs[i]->close();
    }

    for (int i=0; i < timeFilePtrs.length(); i++) {
        timeFilePtrs[i]->flush();
        timeFilePtrs[i]->seek(12);
        *timeStreamPtrs.at(i) << numPointsWrittenToFile;

        timeFilePtrs[i]->close();
    }



    return 0; //success
}

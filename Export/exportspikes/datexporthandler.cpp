#include "datexporthandler.h"
#include <algorithm>
//#include <iostream>

DATExportHandler::DATExportHandler(QStringList arguments):
    AbstractExportHandler(arguments)
{

    supportedDataCategories = DataCategories::SpikeBand | DataCategories::SpikeEvents;

    maxGapSizeInterpolation = 0;

    parseArguments();

    /*
    Do custom argument checks here like this:
    if (ARGVAL != REQUIREMENT) {
        qDebug() << "Error: ....";
        argumentReadOk = false;
    }   
    */

    if ((maxGapSizeInterpolation > 0) || (maxGapSizeInterpolation < 0)) {
         qDebug() << "Error: interp must be 0.";
         argumentReadOk = false;
    }

    if (outputSamplingRate != -1) {
        qDebug() << "Error: outputrate must stay unchanged for spike processing.";
        qDebug() << outputSamplingRate;
        argumentReadOk = false;
    }

    //Parse custom arguments
    //parseCustomArguments(argumentsProcessed);

    if (argumentsProcessed != argumentList.length()-1) {
        _argumentsSupported = false;
        return;
    }
}

DATExportHandler::~DATExportHandler()
{

}

void DATExportHandler::printHelpMenu() {

    printf("\nUsed to extract spike waveforms from a raw rec file and save to individual files for each nTrode. \n");
    printf("Usage:  exportspikes -rec INPUTFILENAME OPTION1 VALUE1 OPTION2 VALUE2 ...  \n\n"
           "Input arguments \n");
    printf("Defaults:\n "
           "    -invert 1 \n"
           "    -interp 0 \n");



    //AbstractExportHandler::printHelpMenu();
}

void DATExportHandler::parseArguments() {
    //Parse extra arguments not handled by the base class
    QString opn_string;
    int optionInd = 1;
    while (optionInd < argumentList.length()) {

        if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0)) {
            //printCustomMenu();
            //return;
            printHelpMenu();
        }
        optionInd++;
    }



    AbstractExportHandler::parseArguments();
}


int DATExportHandler::processData() {


    qDebug() << "Exporting to spike waveform data...";

    //Calculate the packet positions for each channel that we are extracting, plus
    //other critical info (number of saved channels, reference info, etc).


    calculateChannelInfo();

    //Check to make sure at least one neural channel has been defined in the workspace
    if (channelPacketLocations.length() == 0) {
        qDebug() << "No neural information described in the workspace. Aborting.";
        return -1;
    }

    if (!openInputFile()) {
        return -1;
    }

    //Create a directory for the output files located in the same place as the source file
    QFileInfo fi(recFileName);
    QString fileBaseName;
    if (outputFileName.isEmpty()) {
        fileBaseName = fi.completeBaseName();
    } else {
        fileBaseName = outputFileName;
    }

    QString directory;
    if(outputDirectory.isEmpty()) {
        directory = fi.absolutePath();
    } else {
        directory = outputDirectory;
    }

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+".spikes"+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating spikes directory.";
            return -1;
        }
    }

    QString infoLine;
    QString fieldLine;

    //Create an output file for the spike data
    //*****************************************


    //Create the output files (one for each nTrode) and write header sections
    for (int i=0; i<spikeConf->ntrodes.length(); i++) {
        neuroFilePtrs.push_back(new QFile);

        //Connect the spike detection event to a function that writes data to file
        if (threshOverride) {
            //spikeDetectors[i]->newSpikeThreshold(threshOverrideVal_rangeConvert); //set the spike detection threshold to the overrride value (if one was given)
            spikeDetectors[i]->newSpikeThreshold(threshOverrideVal*(1/spikeConf->ntrodes[i]->spike_scaling_to_uV));
        }
        connect(spikeDetectors[i],SIGNAL(spikeDetectionEvent(int,const QVector<int2d>*,const int*,uint32_t)),this,SLOT(writeSpikeData(int,const QVector<int2d>*,const int*,uint32_t)));

        //neuroFilePtrs.last()->setFileName(saveLocation+QString("spikes_nt%1.dat").arg(spikeConf->ntrodes[i]->nTrodeId));
        neuroFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".spikes_nt%1.dat").arg(spikeConf->ntrodes[i]->nTrodeId));
        if (!neuroFilePtrs.last()->open(QIODevice::WriteOnly)) {
            qDebug() << "Error creating output file.";
            return -1;
        }
        neuroStreamPtrs.push_back(new QDataStream(neuroFilePtrs.last()));
        neuroStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

        //Write the current settings to file

        neuroFilePtrs.last()->write("<Start settings>\n");
        infoLine = QString("Description: Spike waveforms for one nTrode\n");
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());

        infoLine = QString("nTrode_ID: %1\n").arg(spikeConf->ntrodes[i]->nTrodeId);
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
        infoLine = QString("num_channels: %1\n").arg(spikeConf->ntrodes[i]->hw_chan.length());
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());

        infoLine = QString("Voltage_scaling: %1\n").arg(spikeConf->ntrodes[i]->spike_scaling_to_uV);
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());

        if (threshOverride) {
            infoLine = QString("Threshold: %1\n").arg(threshOverrideVal);
        } else {
            infoLine = QString("Threshold: %1\n").arg(spikeConf->ntrodes[i]->thresh[0]);
        }
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());

        if (invertSpikes) {
            infoLine = QString("Spike_invert: yes\n");
        } else {
            infoLine = QString("Spike_invert: no\n");
        }
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());

        if (spikeConf->ntrodes[i]->refOn) {
            if (spikeConf->ntrodes[i]->groupRefOn) {
                infoLine = QString("Reference: Common average reference group %1\n").arg(spikeConf->ntrodes[i]->refGroup);
            } else {
                infoLine = QString("Reference: on \nReferenceNTrode: %1\nReferenceChannel: %2\n").arg(spikeConf->ntrodes[i]->refNTrodeID).arg(spikeConf->ntrodes[i]->refChan+1);
            }
        } else {
            infoLine = QString("Reference: off\n");
        }
        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());

        if (spikeConf->ntrodes[i]->filterOn) {
            infoLine = QString("Filter: on \nlowPassFilter: %1\nhighPassFilter: %2\n").arg(spikeConf->ntrodes[i]->highFilter).arg(spikeConf->ntrodes[i]->lowFilter);

        }else if ((filterLowPass_SpikeBand != -1) || (filterHighPass_SpikeBand != -1) ) {
            infoLine = QString("Filter: on \nlowPassFilter: %1\nhighPassFilter: %2\n").arg(filterLowPass_SpikeBand).arg(filterHighPass_SpikeBand);

        } else {
            infoLine = QString("Filter: off \n");
        }

        neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
        for (int ch=0;ch<spikeConf->ntrodes[i]->hw_chan.length();ch++) {
            infoLine = QString("Spike_trigger_on_%1: %2\n").arg(ch+1).arg(spikeConf->ntrodes[i]->triggerOn[ch]);
            neuroFilePtrs.last()->write(infoLine.toLocal8Bit());
        }

        writeDefaultHeaderInfo(neuroFilePtrs.last());

        fieldLine.clear();
        fieldLine += "Fields: ";
        fieldLine += "<time uint32>";

        for (int ch=0;ch<spikeConf->ntrodes[i]->hw_chan.length();ch++) {
            fieldLine += "<waveformCh";
            fieldLine += QString("%1").arg(ch+1);
            fieldLine += " 40*int16>";
        }
        fieldLine += "\n";
        neuroFilePtrs.last()->write(fieldLine.toLocal8Bit());


        neuroFilePtrs.last()->write("<End settings>\n");
        neuroFilePtrs.last()->flush();
    }


    //************************************************
    int inputFileInd = 0;

    while (inputFileInd < recFileNameList.length()) {
        if (inputFileInd > 0) {
            //There are multiple files that need to be stitched together. It is assumed that they all have
            //the exact same header section.
            recFileName = recFileNameList.at(inputFileInd);
            uint32_t lastFileTStamp = currentTimeStamp;

            qDebug() << "\nAppending from file: " << recFileName;
            QFileInfo fi(recFileName);

            if (!fi.exists()) {
                qDebug() << "File could not be found: " << recFileName;
                break;
            }
            if (!openInputFile()) {
                qDebug() << "Error: it appears that the file does not have an identical header to the last file. Cannot append to file.";
                return -1;
            }
            for (int i=0; i < channelFilters.length(); i++) {
                channelFilters[i]->resetHistory();
            }
            if (currentTimeStamp < lastFileTStamp) {
                qDebug() << "Error: timestamps do not begin with greater value than the end of the last file. Aborting.";
                return -1;
            }

        }
        int pointsSinceLastSpikeCheck = 0;
        int spikeCheckInterval = 1000;
        //Process the data and stream results to output files

        while(!filePtr->atEnd()) {

            if (!getNextPacket()) {
                //We have reached the end of the file
                break;
            }

            if (neuralDataHandler->gapOccured()) {
                for (int i=0; i<spikeDetectors.length();i++){
                    spikeDetectors[i]->clearHistory();
                }
            }


            if (!avxsupported) {
                for (int nt = 0; nt < spikeConf->ntrodes.length();nt++) {

                    const int16_t* spikeBandSamples = neuralDataHandler->getNTrodeSamples(nt,AbstractNeuralDataHandler::SPIKE);
                    spikeDetectors[nt]->newData(spikeBandSamples, currentTimeStamp, 0);
                    /*for (int chInd=0; chInd < spikeConf->ntrodes[nt]->hw_chan.length(); chInd++) {
                    spikeDetectors[nt]->newChannelData(chInd,spikeBandSamples[chInd],currentTimeStamp);

                }*/
                }
                //When enough points have been processed, check if spikes occured.
                if (pointsSinceLastSpikeCheck > spikeCheckInterval) {
                    for (int nTrodeInd = 0;nTrodeInd < spikeDetectors.length(); nTrodeInd++) {
                        spikeDetectors[nTrodeInd]->processNewData();
                    }
                    pointsSinceLastSpikeCheck = 0;
                }
            } else {
                QVector<SpikeWaveform> newSpks;
                neuralDataHandler->getAvailableWaveforms(newSpks);
                if (!newSpks.isEmpty()) {
                    //Write spikes to file
                    writeSpikeData(newSpks);
                }

            }

            //Print the progress to stdout
            printProgress();

            pointsSinceLastLog++;

            pointsSinceLastSpikeCheck++;



        }
        filePtr->close();
        printf("\rDone\n");
        inputFileInd++;
    }

    for (int i=0; i < neuroFilePtrs.length(); i++) {
        neuroFilePtrs[i]->flush();
        neuroFilePtrs[i]->close();
    }

    for (int i=0; i < timeFilePtrs.length(); i++) {
        timeFilePtrs[i]->flush();
        timeFilePtrs[i]->close();
    }

    return 0; //success
}

void DATExportHandler::writeSpikeData(int nTrodeNum, const QVector<int2d> *waveForm, const int *, uint32_t time){
    //A spike event was detected.
    //Write the spike waveforms plus the timestamps to the correct file


    //First, write the timestamp
    *neuroStreamPtrs[nTrodeNum] << time;

    //Then write the 40 waveform points for channel 0, then 40 points for channel 1, etc.
    for (int chInd=0; chInd < spikeConf->ntrodes[nTrodeNum]->hw_chan.length(); chInd++) {
        for (int sampleIndex = 0; sampleIndex < 40; sampleIndex++) {
            *neuroStreamPtrs[nTrodeNum] << waveForm[chInd][sampleIndex].y;
        }
    }

    /*
    for (int sampleIndex = 0; sampleIndex < 40; sampleIndex++) {
        for (int chInd=0; chInd < nTrodeSettings[nTrodeNum].numChannels; chInd++) {
            *neuroStreamPtrs[nTrodeNum] << waveForm[chInd][sampleIndex].y;
        }
    }*/

}

void DATExportHandler::writeSpikeData(QVector<SpikeWaveform> newSpks){

    //A spike event was detected.
    //Write the spike waveforms plus the timestamps to the correct file
    for (int spInd=0; spInd<newSpks.length();spInd++) {
        //First, write the timestamp
        *neuroStreamPtrs[newSpks[spInd].ntrodeIndex] << newSpks[spInd].peakTime;


        //Then write the 40 waveform points for channel 0, then 40 points for channel 1, etc.
        for (int chInd=0; chInd < newSpks[spInd].numChannels; chInd++) {
            for (int sampleIndex = 0; sampleIndex < newSpks[spInd].numSamples; sampleIndex++) {
                *neuroStreamPtrs[newSpks[spInd].ntrodeIndex] <<  newSpks[spInd].waveData[chInd][sampleIndex];
            }
        }
    }


}

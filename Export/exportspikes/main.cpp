#include <QCoreApplication>
#include <QtCore>
#include "datexporthandler.h"

int main(int argc, char *argv[])
{

    QCoreApplication a(argc, argv);
    DATExportHandler h(a.arguments());

    if (h.wasHelpMenuDisplayed()) {
        //The user requested the help menu
        return 0;
    }

    if (!h.argumentsSupported()) {
        qDebug() << "Error: one or more argument flags is not supported. Use -h flag to print usage.";
        return -1;
    }

    if (!h.argumentsOK()) {
        qDebug() << "Error in the argument list. Use -h flag to print usage.";
        return -1;
    }

    if (!h.fileConfigOK()) {
        qDebug() << "Error reading recording configuration. Aborting.";
        return -1;
    }

    return h.processData();
    //return a.exec();
}

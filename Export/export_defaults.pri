#-------------------------------------------------
#
# Project include file for all Trodes export utilities
#
#-------------------------------------------------
#macx:CONFIG   -= app_bundle
include (../build_defaults.pri)

QT       += core xml gui network widgets

CONFIG   += console
# Do not build an app bundle on Mac


TEMPLATE = app


INCLUDEPATH  += $$TRODES_REPO_DIR/Trodes/src-main
INCLUDEPATH  += $$TRODES_REPO_DIR/Trodes/src-config
INCLUDEPATH  += $$TRODES_REPO_DIR/Trodes/src-threads
INCLUDEPATH  += $$TRODES_REPO_DIR/Trodes/src-display
INCLUDEPATH  += $$TRODES_REPO_DIR/Trodes/src-network
INCLUDEPATH  += $$TRODES_REPO_DIR
INCLUDEPATH  += $$TRODES_REPO_DIR/Export
INCLUDEPATH  += $$REPO_LIBRARY_DIR
INCLUDEPATH  += $$REPO_LIBRARY_DIR/Libraries/Utility

# trodesnetwork: includes, libs
include(../trodesnetwork_defaults.pri)

unix:!macx{
    INCLUDEPATH  += $$TRODES_REPO_DIR/Trodes/Libraries/Linux
    DEPENDPATH +=  ../../Libraries/Linux/TrodesNetwork/include
    QMAKE_CXXFLAGS += -mavx2
}
win32{
INCLUDEPATH  += $$TRODES_REPO_DIR/Trodes/Libraries/Windows
INCLUDEPATH  += $$TRODES_REPO_DIR/Libraries/Windows64/include/
LIBS += -L$$REPO_LIBRARY_DIR/Windows64/lib -lTrodesNetwork
}
macx{
INCLUDEPATH += $$TRODES_REPO_DIR/Trodes/Libraries/Mac
INCLUDEPATH += $$TRODES_REPO_DIR/Libraries/MacOS/include
LIBS += -L$$REPO_LIBRARY_DIR/MacOS/lib -lTrodesNetwork
QMAKE_POST_LINK += "cp $$TRODES_REPO_DIR/Libraries/MacOS/lib/libTrodesNetwork.*.dylib $$DESTDIR/$${TARGET}.app/Contents/MacOS/"

#    libraries.files += $$PWD/../Libraries/MacOS/lib/libTrodesNetwork.*.dylib
#    trodesnetwork.files += $$PWD/../Libraries/MacOS/lib/libTrodesNetwork.*.dylib
#    trodesnetwork.path = $$DESTDIR/$${TARGET}.app/Contents/MacOS/
#    QtDeploy.commands += "install_name_tool -change libTrodesNetwork.0.dylib @executable_path/../Frameworks/libTrodesNetwork.0.dylib $$shell_path($$INSTALL_DIR/$${TARGET}.app/Contents/MacOS/$${TARGET})" ;
#    libraries.files +=  $$PWD/Libraries/Mac/libftd2xx.dylib \
#                        $$PWD/Libraries/Mac/libftd2xx.1.4.4.dylib
#QMAKE_POST_LINK += "cp $$TRODES_REPO_DIR/Libraries/MacOS/lib/libTrodesNetwork.*.dylib $$DESTDIR/"
QMAKE_CXXFLAGS += -mavx -mavx2
}

#unix: QMAKE_CXXFLAGS += -D__STDC_CONSTANT_MACROS
#QMAKE_CFLAGS += -g -O3
# Requied for some C99 defines
#DEFINES += __STDC_CONSTANT_MACROS



SOURCES += ../../Trodes/src-config/configuration.cpp \
           $$PWD/../Trodes/src-threads/streamprocesshandlers.cpp \
           ../../Trodes/src-main/iirFilter.cpp \
           ../abstractexporthandler.cpp \
           ../../Trodes/src-threads/spikeDetectorThread.cpp \
#           ../../Trodes/src-main/trodesSocket.cpp \
           ../../Trodes/src-main/trodesdatastructures.cpp \
           ../../Trodes/src-main/eventHandler.cpp \
           $$REPO_LIBRARY_DIR/qtmoduleclient.cpp \
           $$REPO_LIBRARY_DIR/qtmoduledebug.cpp \
           ../../Trodes/src-display/cargrouppanel.cpp \
        ../../Trodes/src-threads/avxfloattoshort.cpp \
        ../../Trodes/src-threads/filtercoefcalculator.cpp \
        ../../Trodes/src-threads/auxchanprocessor.cpp \
        ../../Trodes/src-threads/displayprocessor.cpp \
        ../../Trodes/src-threads/referenceprocessor.cpp \
        ../../Trodes/src-threads/lowpassprocessor.cpp \
        ../../Trodes/src-threads/bandpassprocessor.cpp \
        ../../Trodes/src-threads/thresholdprocessor.cpp \
        ../../Trodes/src-threads/vectorizedneuraldatahandler.cpp

HEADERS  += ../../Trodes/src-config/configuration.h \
    $$PWD/../Trodes/src-threads/streamprocesshandlers.h \
            ../../Trodes/src-main/iirFilter.h \
            ../abstractexporthandler.h \
            ../../Trodes/src-threads/spikeDetectorThread.h \
            ../../Trodes/src-threads/avxutils.h \
#            ../../Trodes/src-main/trodesSocket.h \
           ../../Trodes/src-main/trodesdatastructures.h \
           ../../Trodes/src-main/eventHandler.h \
           $$REPO_LIBRARY_DIR/qtmoduleclient.h \
           $$REPO_LIBRARY_DIR/qtmoduledebug.h \
           ../../Trodes/src-display/cargrouppanel.h \
        ../../Trodes/src-threads/avxfloattoshort.h \
        ../../Trodes/src-threads/filtercoefcalculator.h \
        ../../Trodes/src-threads/auxchanprocessor.h \
        ../../Trodes/src-threads/displayprocessor.h \
        ../../Trodes/src-threads/referenceprocessor.h \
        ../../Trodes/src-threads/lowpassprocessor.h \
        ../../Trodes/src-threads/bandpassprocessor.h \
        ../../Trodes/src-threads/thresholdprocessor.h \
        ../../Trodes/src-threads/vectorizedneuraldatahandler.h


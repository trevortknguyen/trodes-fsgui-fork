#ifndef HARDWARESETTINGS_H
#define HARDWARESETTINGS_H

#include <stdint.h>
#include <algorithm>
#include <iostream>
#include "src-core/hardware/common.h"
#include <QtCore>
//#include "configuration.h"

#define SOURCE_STATE_NOT_CONNECTED 0
#define SOURCE_STATE_INITIALIZED   1
#define SOURCE_STATE_RUNNING       2
#define SOURCE_STATE_CONNECTERROR  3
#define SOURCE_STATE_PAUSED        4

#define SENDSTARTCOMMAND    97
#define SENDSTOPCOMMAND     98
#define SENDCHANNELCONFIGCOMMAND 99 //followed by 4 bytes designating which cards are active (up to 32)
#define SENDDIGITALOUTSTATE 10 // followed by one byte with the channel and another byte with the state (0 or 1)
#define SENDANALOGOUTSTATE  20 // followed by one byte with the channel and then two bytes with the value (16 bits)
#define SENDSTATESCRIPTCHARACTER 30 // follwed by one byte with the character to send to stateScript
#define SENDSTATESCRIPTFUNCTIONTRIGGER 31 // followed by one byte with the function number to trigger

#define EEG_BUFFER_SIZE 60000
#define MAXCARGROUPS 32
#define MAXCHANPERGROUP 16


class HeadstageSettings {

public:
    static const int READSETTINGSBYTES = 27;
    static const int WRITESETTINGSBYTES = 16;
    int valid;
    uint8_t minorVersion;
    uint8_t majorVersion;
    uint16_t numberOfChannels;
    uint16_t hsTypeCode;
    uint16_t hsSerialNumber;
    uint16_t packetSize;
    uint8_t rfChannel;
//    uint8_t rfMode;
    uint16_t samplingRate;
    uint8_t auxbytes;
    uint8_t sensorboard_version;
//    uint8_t statusCode8;

    uint16_t percentChannelsForSettle;
    uint16_t threshForSettle;

    int rfAvailable;

    bool smartRefAvailable;
    bool autosettleAvailable;
    bool accelSensorAvailable;
    bool gyroSensorAvailable;
    bool magSensorAvailable;
    bool sample12bitAvailable;
    bool sample20khzAvailable;
    bool rfSessionIDModeAvailable;

    bool smartRefOn;
    bool autoSettleOn;
    bool accelSensorOn;
    bool gyroSensorOn;
    bool magSensorOn;
    bool sample12bitOn;
    bool sample20khzOn;
    bool rfSessionIDModeOn;

    bool waitForStartOverride;      // I think this should be removed
    bool cardEnableCheckOverride;   // I think this should be removed

    HeadstageSettings() {
        valid = false; //Indicates if values have been filled

        minorVersion=0;
        majorVersion=0;

        numberOfChannels=0;
        hsTypeCode=0;
        hsSerialNumber=0;
        packetSize=0;


        autoSettleOn=false;
        percentChannelsForSettle=0;
        threshForSettle=0;

        accelSensorOn=false;
        gyroSensorOn=false;
        magSensorOn=false;

        smartRefOn=false;

        smartRefAvailable=false;
        autosettleAvailable=false;
        accelSensorAvailable=false;
        gyroSensorAvailable=false;
        magSensorAvailable=false;
        rfAvailable=false;

        waitForStartOverride=false;
        cardEnableCheckOverride=false;

        rfChannel=0;

        samplingRate = 0;
        sample12bitOn = false;
        sample12bitAvailable = false;
        sample20khzOn = false;
        sample20khzAvailable = false;
        rfSessionIDModeOn = false;
        rfSessionIDModeAvailable = false;

        auxbytes = 0;
    }

    HeadstageSettings(const char* buffer){
        this->valid             = ((uint8_t)buffer[0] == command_getHeadstageSettings);
        this->hsTypeCode        = *(uint16_t*)(buffer+1); //2 bytes
        this->hsSerialNumber    = *(uint16_t*)(buffer+3); //2 bytes
        this->minorVersion      = *(uint8_t* )(buffer+5); //1 byte
        this->majorVersion      = *(uint8_t* )(buffer+6); //1 byte

        this->numberOfChannels      = *(uint16_t*)(buffer+7); //2 bytes
        this->packetSize            = *(uint16_t*)(buffer+9); //2 bytes
        this->autosettleAvailable   = (buffer[11] & 2) != 0;
        this->autoSettleOn          = (buffer[11] & 1) != 0;

        this->percentChannelsForSettle  = *(uint16_t*)(buffer+12); //2 bytes
        this->threshForSettle           = *(uint16_t*)(buffer+14);//2 bytes

        this->smartRefAvailable     = (buffer[16] & 2) != 0;
        this->smartRefOn            = (buffer[16] & 1) != 0;

        this->accelSensorAvailable  = (buffer[17] & (1 << 0)) != 0;
        this->gyroSensorAvailable   = (buffer[17] & (1 << 1)) != 0;
        this->magSensorAvailable    = (buffer[17] & (1 << 2)) != 0;
        this->rfAvailable           = (buffer[17] & (1 << 3));

        this->accelSensorOn = (buffer[18] & (1 << 0)) != 0;
        this->gyroSensorOn  = (buffer[18] & (1 << 1)) != 0;
        this->magSensorOn   = (buffer[18] & (1 << 2)) != 0;

        this->rfChannel     = *(uint8_t*)(buffer+19); //1 byte
        this->samplingRate  = *(uint16_t*)(buffer+20);//2 bytes

        this->sample12bitOn   = (buffer[22] & (1 << 0)) != 0;
        this->sample12bitAvailable = (buffer[22] & (1 << 1)) != 0;

        this->sample20khzOn   = (buffer[23] & (1 << 0)) != 0;
        this->sample20khzAvailable = (buffer[23] & (1 << 1)) != 0;

        this->rfSessionIDModeOn        = (buffer[24] & (1 << 0)) != 0;
        this->rfSessionIDModeAvailable = (buffer[24] & (1 << 1)) != 0;

        this->auxbytes        = *(uint8_t*)(buffer+25); //1 byte

        this->sensorboard_version   = *(uint8_t*)(buffer+26); //1 byte

        if (numberOfChannels > 0) {
            percentChannelsForSettle = uint16_t(round(float(percentChannelsForSettle)*100.0 / float(numberOfChannels)));
        }
    }

    void writeSettings(char* buffer){
        *(uint8_t*)buffer = command_setHeadstageSettings;
        *(uint8_t*)(buffer+1) = (uint8_t)autoSettleOn;
        if (autoSettleOn) {
            *(uint16_t*)(buffer+2) = uint16_t(round(float(percentChannelsForSettle)/100.0 * numberOfChannels));
        } else {
            *(uint16_t*)(buffer+2) = (uint16_t)numberOfChannels;
        }
        *(uint16_t*)(buffer+4) =  (uint16_t)threshForSettle;

        *(uint8_t*)(buffer+6) = (uint8_t)smartRefOn;


        uint8_t sensorsOn = 0;
        sensorsOn |= ((accelSensorOn & 1) << 0);
        sensorsOn |= ((gyroSensorOn & 1) << 1);
        sensorsOn |= ((magSensorOn & 1) << 2);
        *(uint8_t*)(buffer+7) = sensorsOn;

        *(uint8_t*)(buffer+8) = rfChannel; //1 byte
        *(uint8_t*)(buffer+9) = ((sample12bitOn & 1) << 0);
        *(uint8_t*)(buffer+10) = ((sample20khzOn & 1) << 0);
        *(uint8_t*)(buffer+11) = ((rfSessionIDModeOn & 1) << 0);
        *(uint8_t*)(buffer+12) = (char)0x0;
        *(uint8_t*)(buffer+13) = (char)0x0;
        *(uint8_t*)(buffer+14) = (char)0x0;
        *(uint8_t*)(buffer+15) = (char)0x0;
    }

    //Overload == to comapare the following fields:
    bool operator==(const HeadstageSettings& b) {
        return ( (this->autoSettleOn==b.autoSettleOn) &&
                 (this->percentChannelsForSettle==b.percentChannelsForSettle) &&
                 (this->threshForSettle==b.threshForSettle) &&
                 (this->accelSensorOn==b.accelSensorOn) &&
                 (this->gyroSensorOn==b.gyroSensorOn) &&
                 (this->magSensorOn==b.magSensorOn) &&
                 (this->smartRefOn==b.smartRefOn) &&
/* I think this shoulde be here too
                 (this->sample12bitOn==b.sample12bitOn) &&
                 (this->sample20khzOn==b.sample20khzOn) &&
                 (this->rfModeOn==b.rfModeOn) &&
                 (this->sample12bitAvailable==b.sample12bitAvailable) &&
                 (this->sample20khzAvailable==b.sample20khzAvailable) &&
                 (this->rfModeAvailable==b.rfModeAvailable) &&
*/
                 (this->smartRefAvailable==b.smartRefAvailable) &&
                 (this->autosettleAvailable==b.autosettleAvailable) &&
                 (this->accelSensorAvailable==b.accelSensorAvailable) &&
                 (this->gyroSensorAvailable==b.gyroSensorAvailable) &&
                 (this->magSensorAvailable==b.magSensorAvailable) &&
                 (this->rfAvailable==b.rfAvailable) &&
                 (this->waitForStartOverride==b.waitForStartOverride) &&
                 (this->cardEnableCheckOverride==b.cardEnableCheckOverride));

    }
};

//Settings that are specific to neuropixels probes
class NeuroPixelsSettings {
    public:

    static const int READSETTINGSBYTES = 27;
    static const int WRITESETTINGSBYTES = 125;

    NeuroPixelsSettings() {
        _numberOfProbes = 0;
        /*for (int i=0;i<120;i++) {
            padState[i]=0;
        }*/
    }

    /*void loadFromWorkspace(const TrodesConfiguration &c)
    {




        QList<int> APGainForEachProbe;
        QList<int> LFPGainForEachProbe;
        QList<int> refModeForEachProbe;
        QList<QVector<bool> > channelsTurnedOn;

        QVector<int> gainModes;
        gainModes << 50 << 125 << 250 << 500 << 1000 << 1500 << 2000 << 3000;

        for (int i=0; i<c.hardwareConf.sourceControls.length();i++) { //Probes
            if (c.hardwareConf.sourceControls.at(i).name == "NeuroPixels1") {
                //We have found a section for the Neuropixels 1.0 probe

                //set default mode for each probe here
                int tmpAPGainMode = 0;
                int tmpLFPGainMode = 0;
                int tmpRefMode = 0;
                QVector<bool> tmpChannelsTurnedOn;

                for (int j=0;j<c.hardwareConf.sourceControls.at(i).customOptions.length();j++) {


                    if (c.hardwareConf.sourceControls.at(i).customOptions.at(j).name == "APGainMode") {
                            int APGainVal = c.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0);

                            for (int s=0;s<gainModes.length();s++) {
                                if (gainModes.at(s) == APGainVal) {
                                    tmpAPGainMode = s;
                                    break;
                                }
                            }
                    }
                    if (c.hardwareConf.sourceControls.at(i).customOptions.at(j).name == "LFPGainMode") {
                            int LFPGainVal = c.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0);

                            for (int s=0;s<gainModes.length();s++) {
                                if (gainModes.at(s) == LFPGainVal) {
                                    tmpLFPGainMode = s;
                                    break;
                                }
                            }
                    }
                    if (c.hardwareConf.sourceControls.at(i).customOptions.at(j).name == "RefMode") {
                        if ( (c.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0) >= 0) && (c.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0) <= 2) ) {
                            tmpRefMode = c.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0);
                        }
                    }

                    if (c.hardwareConf.sourceControls.at(i).customOptions.at(j).name == "channelsOn") {

                        if (c.hardwareConf.sourceControls.at(i).customOptions.at(j).data.length() == 960) {
                            for (int k=0; k<960;k++) {
                                tmpChannelsTurnedOn.push_back(c.hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(k));
                            }
                        }


                    }


                }


                //selectionModeForEachProbe.push_back(tmpSelectionMode);
                APGainForEachProbe.push_back(tmpAPGainMode);
                LFPGainForEachProbe.push_back(tmpLFPGainMode);
                refModeForEachProbe.push_back(tmpRefMode);
                channelsTurnedOn.push_back(tmpChannelsTurnedOn);
                //allSelectionModeArgs.push_back(probeData);
            }
        }

        setNumProbes(APGainForEachProbe.length());
        for (int i=0;i<APGainForEachProbe.length();i++) {
            APGainCode[i] = APGainForEachProbe.at(i);
            LFPGainCode[i] = LFPGainForEachProbe.at(i);
            refCode[i] = refModeForEachProbe.at(i);

            QVector<uint8_t> newPadState;
            newPadState.resize(120);
            newPadState.fill(0);
            padState[i] = newPadState;

            for (int j=0;j<960;j++) {
                setPadState(j,channelsTurnedOn.at(i).at(j),i);
            }
        }

    }*/



    void setNumProbes(int num)
    {
        APGainCode.clear();
        LFPGainCode.clear();
        refCode.clear();
        padState.clear();

        for (int i=0;i<num;i++) {
            APGainCode.push_back(0);
            LFPGainCode.push_back(0);
            refCode.push_back(0);
            padState.push_back(QVector<uint8_t>());
            padState.last().fill(0,120);
        }
        _numberOfProbes = num;

    }

    int numProbes() {
        return _numberOfProbes;
    }

    void setRefCode(uint8_t code, int probeIndex)
    {
        refCode[probeIndex] = code;
    }

    void setAPGainCode(uint8_t code, int probeIndex)
    {
        APGainCode[probeIndex] = code;
    }

    void setLFPGainCode(uint8_t code, int probeIndex)
    {
        LFPGainCode[probeIndex] = code;
    }

    /*NeuroPixelsSettings(const char* buffer) {

    }*/

    void setPadState(int padIndex, bool state, int probeIndex)
    {
        int targetByte = padIndex/8;
        int targetBit = padIndex % 8;

        if (state) {
            uint8_t replaceByteMask = 1 << targetBit;
            padState[probeIndex][targetByte] |= replaceByteMask;
        } else {
            uint8_t replaceByteMask = ~(1 << targetBit);
            padState[probeIndex][targetByte] &= replaceByteMask;
        }

    }

    int valid;
    int _numberOfProbes;
    //uint8_t probeIndex; //0,1,2,...
    QList<uint8_t> refCode;
    QList<uint8_t> APGainCode;
    QList<uint8_t> LFPGainCode;
    QList<QVector<uint8_t> > padState;

    void writeSettings(char* buffer, int probeIndex){
        *(uint8_t*)buffer = command_setNeuroPixelsSettings;
        *(uint8_t*)(buffer+1) = (uint8_t)probeIndex;
        *(uint8_t*)(buffer+2) = refCode[probeIndex];

        uint8_t gainMode = APGainCode[probeIndex]; //The first three bits are for AP gain
        gainMode |= (LFPGainCode[probeIndex] << 3); // The next three bits are for LFP gain
        *(uint8_t*)(buffer+3) = gainMode;
        std::copy_n(padState[probeIndex].data(),120,buffer+4);

        //Calculate a checksum and append to the end of buffer
        int checksum = 0;
        for (int chInd = 0; chInd < (WRITESETTINGSBYTES-1); chInd++) {
                checksum = (checksum + buffer[chInd]);
        }

        *(uint8_t*)(buffer+124) = checksum & 0xff;
    }


};


class HardwareControllerSettings {

public:
    static const int READSETTINGSBYTES = 17;
    static const int WRITESETTINGSBYTES = 9;
    int valid;
    uint8_t minorVersion;
    uint8_t majorVersion;
    uint16_t modelNumber;
    uint16_t serialNumber;
    uint16_t packetSize;
    uint8_t rfChannel;
    bool rfSessionIDMode;
    uint8_t samplingRateKhz;
    uint8_t hardwaredetect;
    uint8_t unused;
    uint8_t IPAddress0;
    uint8_t IPAddress1;
    uint8_t IPAddress2;
    uint8_t IPAddress3;

    bool ECUDetected;
    bool RFDetected;


    HardwareControllerSettings() {
      valid = false;
      minorVersion=0;
      majorVersion=0;
      modelNumber=0;
      serialNumber=0;
      packetSize=0;
      rfChannel=0;
      rfSessionIDMode = false;
      samplingRateKhz=0;
//      statusCode3=0;
      hardwaredetect = 0;
      ECUDetected = false;
      RFDetected = false;
      unused=0;
      IPAddress0=0;
      IPAddress1=0;
      IPAddress2=0;
      IPAddress3=0;
    }

    HardwareControllerSettings(const char* buffer){
        readSettings(buffer);
    }

    void readSettings(const char* buffer){
        this->valid             = ((uint8_t)buffer[0] == command_getControllerSettings);
        this->modelNumber       = *(uint16_t*)(buffer+1);
        this->serialNumber      = *(uint16_t*)(buffer+3);
        this->minorVersion      = *(uint8_t* )(buffer+5);
        this->majorVersion      = *(uint8_t* )(buffer+6);
        this->packetSize        = *(uint16_t*)(buffer+7);
        this->rfChannel         = *(uint8_t* )(buffer+9);
        this->samplingRateKhz   = *(uint8_t* )(buffer+10);
//        this->statusCode3       = *(uint8_t* )(buffer+11);
        this->hardwaredetect    = *(uint8_t* )(buffer+11);

        if (this->samplingRateKhz == 0) {
            //Backward compatibility (it was always 30 kHz)
            this->samplingRateKhz = 30;
        }


        //qDebug() << "Devices" << this->hardwaredetect;
        if (this->hardwaredetect & 0x01) {
          this->ECUDetected = true;
          this->packetSize += 32;
        } else {
          this->ECUDetected = false;
        }

        if (this->hardwaredetect & 0x02) {
          this->RFDetected = true;
        } else {
          this->RFDetected = false;
        }

        if (this->hardwaredetect & 0x04) {
          this->rfSessionIDMode = true;
        } else {
          this->rfSessionIDMode = false;
        }

        /*if(majorVersion==3 && minorVersion>=11){
            this->ecuDetect         = (buffer[11] & (1 << 0)) != 0;
            this->rfDetect          = (buffer[11] & (1 << 1)) != 0;
        }*/
        this->unused       = *(uint8_t* )(buffer+12);


        this->IPAddress0       = *(uint8_t* )(buffer+13);
        this->IPAddress1       = *(uint8_t* )(buffer+14);
        this->IPAddress2       = *(uint8_t* )(buffer+15);
        this->IPAddress3       = *(uint8_t* )(buffer+16);
    }

    void writeSettings(char* buffer){
        //9 bytes
        *(uint8_t*)buffer = command_setControllerSettings;
        *(uint8_t*)(buffer+1) = rfChannel; //1 byte
        *(uint8_t*)(buffer+2) = samplingRateKhz; //1 byte: MCU sampling rate
        *(uint8_t*)(buffer+3) = hardwaredetect; //1 byte: RF mode, etc
        *(uint8_t*)(buffer+4) = unused; //1 byte: unused
        *(uint8_t*)(buffer+5) = IPAddress0; //1 byte IP Address x.x.x.x
        *(uint8_t*)(buffer+6) = IPAddress1; //1 byte
        *(uint8_t*)(buffer+7) = IPAddress2; //1 byte
        *(uint8_t*)(buffer+8) = IPAddress3; //1 byte

    }

};

class HardwareImpedanceMeasureCommand {

public:

    static const int WRITESETTINGSBYTES = 5;
    uint16_t frequency;
    uint16_t notchHz;
    uint16_t numberOfCycles;
    uint16_t channel;


    HardwareImpedanceMeasureCommand() {
        frequency = 1000; //Measuring signal frequency (Hz)
        notchHz = 0; //Current output mode of signal
        numberOfCycles = 5;
        channel = 0;
    }


    void writeSettings(char* buffer){
        //5 bytes
        *(uint8_t*)buffer = command_startImpedanceTest; //0xb0
        *(uint8_t*)(buffer+1) = (uint8_t)(numberOfCycles); //1 byte. Should be in power of 2, so 7 = 256 cycles.
        *(uint8_t*)(buffer+2) = (uint8_t)notchHz; //1 byte. Valid values are 0, 1 or 2
        //int16_t chanRemap = (channel % 4)*32 + floor(channel/4);
        //int16_t chipSelect = channel & 0xFFC0;
        //chanRemap = channel & 0x3F; //The 6 LSB's are the channel. The rest is the chip.
        *(uint16_t*)(buffer+3) = channel; //2 bytes (0-based HW channel)


    }

};

#endif // HARDWARESETTINGS_H

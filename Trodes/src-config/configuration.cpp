/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "configuration.h"
#include "trodesSocketDefines.h"
#include <math.h>

#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QDateTime>
//#include <QMessageBox>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <fstream>
#include <QMutex>
//#include <qtmoduledebug.h>

/* --------------------------------------------------------------------- */
/* --------------------------------------------------------------------- */
// Global Configuration Structures - these are extern defined in globalObjects.h
NetworkConfiguration *networkConf;
ModuleConfiguration *moduleConf;
//NTrodeTable *nTrodeTable;
streamConfiguration *streamConf;
SpikeConfiguration *spikeConf;
headerDisplayConfiguration *headerConf;
HardwareConfiguration *hardwareConf;
GlobalConfiguration *globalConf;
#ifdef PHOTOMETRY_CODE
PhotometryConfiguration *photometryConf;
#endif
BenchmarkConfig *benchConfig;
bool unitTestMode;


//info about the structure of the data stream
//these can be changed by the source

//int NCHAN; //Number of channels
//int hourceSamplingRate = 25000; //sampling rate (Hz)
//int headerSize; //for each sample cycle, a multi-purpose header is collected (digital events)
//QString filePrefix;
//QString filePath;

//Global access to link settings
extern bool linkChangesBool;


XMLContainer::XMLContainer(const QString &name):
    tagName(name)
{

}

XMLContainer::~XMLContainer()
{
    clear();
}

void XMLContainer::clear()
{

    while (childElements.length() > 0) {
        //qDebug() << "Deleting" << childElements.last()->tagName;
        delete childElements.takeLast();
    }

    attributes.clear();
}

QHash<QString,QVariant> XMLContainer::getAttributes()
{
    return attributes;
}

QList<XMLContainer*> XMLContainer::getChildElements()
{
    return childElements;
}

void XMLContainer::setName(const QString &name)
{
    tagName = name;
}

QString XMLContainer::name()
{
    return tagName;
}

void XMLContainer::appendField(const QString &fName, const QVariant &fdata)
{
    if (fName.isEmpty()) {
        //Needs a non-empty tagName
        return;
    }
    attributes.insert(fName,fdata);
}

void XMLContainer::appendVersionFields()
{
    int version = TRODES_VERSION;
    int places = 0;
    int firstMod = 10;
    while (version > 0 ) {
        version = version/10;
        places++;
        if (places > 3) {
            firstMod = firstMod*10;
        }
    }
    version = TRODES_VERSION;
    int bugVersion = version%(firstMod);
    version = version/(firstMod);
    int featureVersion = version%10;
    version = (version/10)%10;

    appendField("Trodes_Version_Major",version);
    appendField("Trodes_Version_Minor",featureVersion);
    appendField("Trodes_Version_Build",bugVersion);

    if(BETA_VERSION){
        appendField("Trodes_Version_Revision",BETA_VERSION);

    }
}

void XMLContainer::appendChild(XMLContainer *container)
{
    childElements.append(container);
}

QDomElement XMLContainer::toXML(QDomDocument &doc) const
{

    //Create the element in the document
    QDomElement outElement = doc.createElement(tagName);
    outElement.setTagName(tagName);

    //Create the attributes
    QHash<QString, QVariant>::const_iterator i = attributes.constBegin();
    while (i != attributes.constEnd()) {
        QString key = i.key();
        QVariant value = i.value();
        bool ok;
        //Try to save as a double
        outElement.setAttribute(key,value.toDouble(&ok));
        //If that doesn't work, we save as a string
        if (!ok) {
            outElement.setAttribute(key,value.toString());
        }

        ++i;
    }

    //Recursively do the above for all children
    for (int childInd = 0; childInd < childElements.length(); childInd++) {
        outElement.appendChild(childElements.at(childInd)->toXML(doc));
    }

    return outElement;

}

bool XMLContainer::fromXML(const QDomElement &XMLElem)
{


    clear();

    //Get the tagname of the element
    setName(XMLElem.tagName());

    //get the attributes of the element
    QDomNamedNodeMap nMap = XMLElem.attributes();
    for (int i=0; i < nMap.length(); i++) {
        QDomNode node = nMap.item(i);
        if (node.isAttr()) {
            QDomAttr attr = node.toAttr();
            QString attrName = attr.name();
            if (attrName.isEmpty()) {
                break;
            }
            QString attrVal = attr.value();
            bool ok;
            double attrDoubleVal = attrVal.toDouble(&ok);
            //Try to insert value as a double. If that does not work, use a string instead
            if (ok) {
                attributes.insert(attrName,attrDoubleVal);
            } else {
                attributes.insert(attrName,attrVal);
            }
        }
    }

     //get the children of the element
    for(QDomNode n = XMLElem.firstChild(); !n.isNull(); n = n.nextSibling()) {

        XMLContainer *tmpChild = new XMLContainer;
        tmpChild->fromXML(n.toElement());
        appendChild(tmpChild);

    }

    return true;

}

//------------------------------------

void Helper::setWidgetTextPaletteColor(QWidget *widget, QColor newColor) {
    QPalette p = widget->palette();
    p.setColor(QPalette::WindowText, newColor);
    widget->setPalette(p);
}

void Helper::setWidgetTextPaletteColor(QCheckBox *widget, QColor newColor){
    widget->setStyleSheet("QCheckBox {border: none;color: " + newColor.name() + ";}");
}

QList<QList<QString> > Helper::getSortedTagList(SingleSpikeTrodeConf *nTrode) {
    QHashIterator<GroupingTag, int> i(nTrode->gTags);
    QList<QList<QString> > tagList;

    while (i.hasNext()) {
        i.next();
        QString curCata = i.key().category;
        QString curTag = i.key().tag;
//        qDebug() << "[" << curCata << "] " << curTag;
        bool createNewCata = true;
        for (int j = 0; j < tagList.length(); j++) {
            if (tagList.at(j).at(0) == curCata) { //first item of each sub list will be catagory
                createNewCata = false;
                QList<QString> curTagList = tagList.at(j);
                curTagList.push_back(curTag);
                tagList.replace(j, curTagList);
                break;
            }
        }

        if (createNewCata) {
            QList<QString> newCataList;
            newCataList.push_back(curCata);
            newCataList.push_back(curTag);
            tagList.push_back(newCataList);
        }
    }
    return(tagList);
}

/* --------------------------------------------------------------------- */
GlobalConfiguration::GlobalConfiguration(QObject *):
    saveDisplayedChanOnly(false),
    realTimeMode(false),
    MBPerFileChunk(-1)
{
    suppressModuleAbsPathWarning = 0;
    timestampAtCreation = -1;
    systemTimeAtCreation = -1;
    headstageSerialNumber = "-1";
    controllerSerialNumber = "-1";
    autoSettleOn = -1;
    smartRefOn = -1;
    gyroSensorOn = -1;
    accelSensorOn = -1;
    magSensorOn = -1;
    controllerFirmwareVersion = "-1";
    headstageFirmwareVersion = "-1";
    filePath = "";
    filePrefix = "";

    //may return 0 when not able to detect
    processorCount = std::thread::hardware_concurrency();

    loadCurrentVersionInfo();

}

int GlobalConfiguration::loadFromXML(QDomNode &globalConfNode) {
    filePath = globalConfNode.toElement().attribute("filePath", "/");
    filePrefix = globalConfNode.toElement().attribute("filePrefix", "");
    saveDisplayedChanOnly = globalConfNode.toElement().attribute("saveDisplayedChanOnly", "0").toInt();
    realTimeMode = globalConfNode.toElement().attribute("realtimeMode", "0").toInt();
    MBPerFileChunk = globalConfNode.toElement().attribute("fileChunkSize", "-1").toInt();
    suppressModuleAbsPathWarning = globalConfNode.toElement().attribute("suppressModuleAbsPathWarning", 0).toInt();

    timestampAtCreation = globalConfNode.toElement().attribute("timestampAtCreation", "-1").toLongLong();
    systemTimeAtCreation = globalConfNode.toElement().attribute("systemTimeAtCreation", "-1").toLongLong();
    trodesVersion = globalConfNode.toElement().attribute("trodesVersion","-1");
    compileDate = globalConfNode.toElement().attribute("compileDate","-1");
    compileTime = globalConfNode.toElement().attribute("compileTime","-1");
    qtVersion = globalConfNode.toElement().attribute("qtVersion","-1");
    //exPathStr = globalConfNode.toElement().attribute("executablePath","0");
    commitHeadStr = globalConfNode.toElement().attribute("commitHead","-1");

    headstageSerialNumber = globalConfNode.toElement().attribute("headstageSerial", "-1");
    controllerSerialNumber = globalConfNode.toElement().attribute("controllerSerial", "-1");
    autoSettleOn = globalConfNode.toElement().attribute("headstageAutoSettleOn", "-1").toInt();
    smartRefOn = globalConfNode.toElement().attribute("headstageSmartRefOn", "-1").toInt();
    gyroSensorOn = globalConfNode.toElement().attribute("headstageGyroSensorOn", "-1").toInt();
    accelSensorOn = globalConfNode.toElement().attribute("headstageAccelSensorOn", "-1").toInt();
    magSensorOn = globalConfNode.toElement().attribute("headstageMagSensorOn", "-1").toInt();
    controllerFirmwareVersion = globalConfNode.toElement().attribute("controllerFirmwareVersion","-1");
    headstageFirmwareVersion = globalConfNode.toElement().attribute("headstageFirmwareVersion","-1");

    //qDebug() << "Realtime mode: " << realTimeMode;
    return 0;
}

void GlobalConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode) {
    QDomElement gconf = doc.createElement("GlobalConfiguration");

    rootNode.appendChild(gconf);
    //loadCurrentVersionInfo(); //Version info should simply pass what was in the loaded config (not the current Trodes info)

    gconf.setAttribute("filePath", filePath);
    gconf.setAttribute("filePrefix", filePrefix);
    gconf.setAttribute("saveDisplayedChanOnly", saveDisplayedChanOnly);
    gconf.setAttribute("realtimeMode", realTimeMode);
    gconf.setAttribute("fileChunkSize", MBPerFileChunk);
    gconf.setAttribute("suppressModuleAbsPathWarning", suppressModuleAbsPathWarning);
    gconf.setAttribute("timestampAtCreation", QString("%1").arg(timestampAtCreation));
    gconf.setAttribute("systemTimeAtCreation", QString("%1").arg(systemTimeAtCreation, 20));
    gconf.setAttribute("trodesVersion", trodesVersion);
    gconf.setAttribute("compileDate", compileDate);
    gconf.setAttribute("compileTime", compileTime);
    gconf.setAttribute("qtVersion", qtVersion);
    gconf.setAttribute("headstageSerial", headstageSerialNumber);
    gconf.setAttribute("controllerSerial", controllerSerialNumber);
    gconf.setAttribute("headstageAutoSettleOn", autoSettleOn);
    gconf.setAttribute("headstageSmartRefOn", smartRefOn);
    gconf.setAttribute("headstageGyroSensorOn", gyroSensorOn);
    gconf.setAttribute("headstageAccelSensorOn", accelSensorOn);
    gconf.setAttribute("headstageMagSensorOn", magSensorOn);
    gconf.setAttribute("headstageFirmwareVersion",headstageFirmwareVersion);
    gconf.setAttribute("controllerFirmwareVersion",controllerFirmwareVersion);

    //gconf.setAttribute("executablePath", exPathStr);
    gconf.setAttribute("commitHead", commitHeadStr);
}
void GlobalConfiguration::saveToXMLNoSessionData(QDomDocument &doc, QDomElement &rootNode) {

    //Does not write session-specific data
    QDomElement gconf = doc.createElement("GlobalConfiguration");

    rootNode.appendChild(gconf);
    //loadCurrentVersionInfo(); //Version info should simply pass what was in the loaded config (not the current Trodes info)

    gconf.setAttribute("filePath", filePath);
    gconf.setAttribute("filePrefix", filePrefix);
    gconf.setAttribute("saveDisplayedChanOnly", saveDisplayedChanOnly);
    gconf.setAttribute("realtimeMode", realTimeMode);
    gconf.setAttribute("fileChunkSize", MBPerFileChunk);


    //gconf.setAttribute("executablePath", exPathStr);
    //gconf.setAttribute("commitHead", commitHeadStr);
}

void GlobalConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode, qint64 currentTimeStamp) {
    QDomElement gconf = doc.createElement("GlobalConfiguration");

    rootNode.appendChild(gconf);
    loadCurrentVersionInfo(); //make sure version info is current before saving the workspace

    gconf.setAttribute("filePath", filePath);
    gconf.setAttribute("filePrefix", filePrefix);
    gconf.setAttribute("saveDisplayedChanOnly", saveDisplayedChanOnly);
    gconf.setAttribute("realtimeMode", realTimeMode);
    if (currentTimeStamp < 0) {
        //Use what is already in the workspace
        gconf.setAttribute("timestampAtCreation", timestampAtCreation);
        gconf.setAttribute("systemTimeAtCreation", systemTimeAtCreation);

    } else {
        gconf.setAttribute("timestampAtCreation", currentTimeStamp);
        gconf.setAttribute("systemTimeAtCreation", QString("%1").arg(QDateTime::currentMSecsSinceEpoch(), 20));
    }
    gconf.setAttribute("trodesVersion", trodesVersion);
    gconf.setAttribute("compileDate", compileDate);
    gconf.setAttribute("compileTime", compileTime);
    gconf.setAttribute("qtVersion", qtVersion);
    //gconf.setAttribute("executablePath", exPathStr);
    gconf.setAttribute("commitHead", commitHeadStr);
    gconf.setAttribute("headstageSerial", headstageSerialNumber);
    gconf.setAttribute("controllerSerial", controllerSerialNumber);
    gconf.setAttribute("headstageAutoSettleOn", autoSettleOn);
    gconf.setAttribute("headstageSmartRefOn", smartRefOn);
    gconf.setAttribute("headstageGyroSensorOn", gyroSensorOn);
    gconf.setAttribute("headstageAccelSensorOn", accelSensorOn);
    gconf.setAttribute("headstageMagSensorOn", magSensorOn);
    gconf.setAttribute("headstageFirmwareVersion",headstageFirmwareVersion);
    gconf.setAttribute("controllerFirmwareVersion",controllerFirmwareVersion);


}

QString GlobalConfiguration::getVersionInfo(bool withSpaces, bool richtext) {
    int version = TRODES_VERSION;
    int places = 0;
    int firstMod = 10;
    while (version > 0 ) {
        version = version/10;
        places++;
        if (places > 3) {
            firstMod = firstMod*10;
        }
    }
    version = TRODES_VERSION;
    int bugVersion = version%(firstMod);
    version = version/(firstMod);
    int featureVersion = version%10;
    version = (version/10)%10;
    QString versionStr = QString("Trodes version %1.%2.%3").arg(version).arg(featureVersion).arg(bugVersion);
    if(BETA_VERSION){
        versionStr = QString("%1 BETAv%2").arg(versionStr).arg(BETA_VERSION);
    }
    QString compileInfoStr = QString("Compiled on %1 at %2 with Qt v%3").arg(__DATE__).arg(__TIME__).arg(QT_VERSION_STR);
    QString pathStr = QString("Executable Location: '%1'").arg(QCoreApplication::applicationFilePath());
    QString commitStr = QString("Git Commit: %1").arg(GIT_COMMIT);
    QString aboutStr;

    if (withSpaces)
        aboutStr = QString("%1\n\n%2\n\n%3\n\n%4").arg(versionStr).arg(compileInfoStr).arg(pathStr).arg(commitStr);
    else if(richtext)
        aboutStr = QString("<p>%1</p><p>%2</p><p>%3</p><p>%4</p>").arg(versionStr).arg(compileInfoStr).arg(pathStr).arg(commitStr);
    else
        aboutStr = QString("%1\n%2\n%3\n%4").arg(versionStr).arg(compileInfoStr).arg(pathStr).arg(commitStr);

    return(aboutStr);
}

void GlobalConfiguration::applyHeadstageSettings(HeadstageSettings headstageSettings){
    headstageSerialNumber = QString("%1 %2").arg(headstageSettings.hsTypeCode,5,10,QChar('0')).arg(headstageSettings.hsSerialNumber,5,10,QChar('0'));
    autoSettleOn = (int)headstageSettings.autoSettleOn;
    smartRefOn = (int)headstageSettings.smartRefOn;
    gyroSensorOn =(int)(headstageSettings.gyroSensorAvailable && headstageSettings.gyroSensorOn);
    accelSensorOn = (int)(headstageSettings.accelSensorAvailable && headstageSettings.accelSensorOn);
    magSensorOn = (int)(headstageSettings.magSensorAvailable && headstageSettings.magSensorOn);
    headstageFirmwareVersion = QString("%1.%2").arg(headstageSettings.majorVersion).arg(headstageSettings.minorVersion);
}

void GlobalConfiguration::applyControllerSettings(HardwareControllerSettings controllerSettings){
    controllerSerialNumber =  QString("%1 %2").arg(controllerSettings.modelNumber,5,10,QChar('0')).arg(controllerSettings.serialNumber,5,10,QChar('0'));
    controllerFirmwareVersion = QString("%1.%2").arg(controllerSettings.majorVersion).arg(controllerSettings.minorVersion);
}

void GlobalConfiguration::loadCurrentVersionInfo(void) {
    int version = TRODES_VERSION;
    int places = 0;
    int firstMod = 10;
    while (version > 0 ) {
        version = version/10;
        places++;
        if (places > 3) {
            firstMod = firstMod*10;
        }
    }
    version = TRODES_VERSION;
    int bugVersion = version%(firstMod);
    version = version/(firstMod);
    int featureVersion = version%10;
    version = (version/10)%10;

    trodesVersion = QString("%1.%2.%3").arg(version).arg(featureVersion).arg(bugVersion);
    if(BETA_VERSION){
        trodesVersion = QString("%1 BETAv%2").arg(trodesVersion).arg(BETA_VERSION);
    }
    compileDate = __DATE__;
    compileTime = __TIME__;
    qtVersion = QT_VERSION_STR;
    //exPathStr = QCoreApplication::applicationFilePath();
    commitHeadStr = GIT_COMMIT;
}


HardwareConfiguration::HardwareConfiguration(QObject *):
    sourceSamplingRate(30000),
    lfpSubsamplingInterval(20),
    headerSize(0),
    headerSizeManuallyDefined(false),
    ECUConnected(false),
    sysTimeIncluded(false)

{

}

int HardwareConfiguration::loadFromXML(QDomNode &hardwareConfNode) {

    //Some of these items can be defined in a backwards compatible section, so
    //we need to make sure thay have not already been defines (still equal to 0).

    if (NCHAN == 0) {
        NCHAN = hardwareConfNode.toElement().attribute("numChannels", "32").toInt();
        if ((NCHAN % 32) != 0) {
            qDebug() << "[ParseTrodesConfig] Error: numChannels must be a multiple of 32";
            return -6;
        }

        //qDebug() << "[ParseTrodesConfig] Number of channels: " << NCHAN;
    }

    if (sourceSamplingRate == 0) {
        sourceSamplingRate = hardwareConfNode.toElement().attribute("samplingRate", "30000").toInt();
        //qDebug() << "[ParseTrodesConfig] Sampling rate: " << sourceSamplingRate << " Hz";
    }


    if (headerSize == 0) {
        headerSize = hardwareConfNode.toElement().attribute("headerSize", "0").toInt();
        if (headerSize > 0) {
            //qDebug() << "[ParseTrodesConfig] Header size: " << hardwareConf->headerSize;
            headerSizeManuallyDefined = true;
        }
    }

    useIntrinsics = hardwareConfNode.toElement().attribute("useIntrinsics", "1").toInt();

    //hardwareConf->numStimChan = hardwareConfNode.toElement().attribute("numStimChannels", "0").toInt();

#ifdef PHOTOMETRY_CODE
    if (NFIBER == 0) {
        NFIBER = hardwareConfNode.toElement().attribute("numFibers", "0").toInt();
    }

    if (APDsamplingRate == 0) {
        APDsamplingRate = hardwareConfNode.toElement().attribute("APDsamplingRate", "5000").toInt();
    }
#endif

    ECUConnected = hardwareConfNode.toElement().attribute("ECU", "0").toInt();
    //qDebug() << "[ParseTrodesConfig] ECU: " << ECUConnected;


    QDomNode n = hardwareConfNode.firstChild();
    QDomElement hardwareElement;


    while (!n.isNull()) {

        hardwareElement = n.toElement();

        if (!hardwareElement.isNull()) {

            if (hardwareElement.tagName() == "Device") {
                //This section is describing a part of the packet structure containing data from a hardware device
                if (headerSizeManuallyDefined) {
                    qDebug() << "[ParseTrodesConfig] Error: if device info is listed in harwareConfig, then 'headerSize' should not be defined.";
                    return -1;
                }
                //qDebug() << "[ParseTrodesConfig] Device: " << deviceElement.attribute("name","");

                DeviceInfo newDevice;
                bool ok;
                newDevice.name = hardwareElement.attribute("name","");



                if (newDevice.name == "ECU") {
                    ECUConnected = true;
                    //qDebug() << "[ParseTrodesConfig] ECU mode on";

                }
                newDevice.packetOrderPreference = hardwareElement.attribute("packetOrderPreference","").toInt(&ok);
                if (!ok) {
                    qDebug() << "[ParseTrodesConfig] Error: Packet order preference conversion to number failed.";
                    return -1;
                }

                newDevice.numBytes = hardwareElement.attribute("numBytes","").toInt(&ok);
                if (!ok) {
                    qDebug() << "[ParseTrodesConfig] Error: Device numBytes field conversion to number failed.";
                    return -1;
                }

                newDevice.available = hardwareElement.attribute("available","").toInt(&ok);
                if (!ok) {
                    qDebug() << "[ParseTrodesConfig] Error: Device available field conversion to number failed.";
                    return -1;
                }

                if (newDevice.name == "SysClock" && newDevice.available) {

                    sysTimeIncluded = true;
                    newDevice.packetOrderPreference = 10000;  //Must be the last (just before the hardware clock);
                    if (newDevice.numBytes != 8) {
                        qDebug() << "[ParseTrodesConfig] Error: SysClock device must be 8 bytes.";
                        return -1;
                    }
                }

                QDomNode devChanN = n.firstChild();
                QDomElement deviceChannelElement;
                while (!devChanN.isNull()) {

                    deviceChannelElement = devChanN.toElement();
                    if (!deviceChannelElement.isNull()) {
                        DeviceChannel newDeviceChannel;
                        newDeviceChannel.idString = deviceChannelElement.attribute("id","");

                        QString dataTypeString = deviceChannelElement.attribute("dataType", "");

                        if (dataTypeString == "digital") {
                            newDeviceChannel.dataType = DeviceChannel::DIGITALTYPE;
                        }
                        else if (dataTypeString == "analog") {
                            newDeviceChannel.dataType = DeviceChannel::INT16TYPE;
                        }
                        else if (dataTypeString == "uint32") {
                            newDeviceChannel.dataType = DeviceChannel::UINT32TYPE;
                        }

                        else {
                            //QMessageBox::information(0, "error", QString("Config file error: Error in parsing data type: ") + dataTypeString);
                            qDebug() << "[ParseTrodesConfig] Error: Error in parsing data type:" << dataTypeString;
                            return -2;
                        }


                        //tmpHeaderChan.port = nt.attribute("port", "1").toInt(&ok);
                        //newDeviceChannel.input = deviceChannelElement.attribute("input", "1").toInt(&ok);
                        newDeviceChannel.startByte = deviceChannelElement.attribute("startByte", "1").toInt(&ok); //add the device offset to the value
                        if (!ok) {
                            //QMessageBox::information(0, "error", QString("Config file error: Error in converting startByte to number in device configuration. Read value is '") + deviceChannelElement.attribute("startByte", "") + QString("'."));
                            return -2;
                        }
                        else if ((newDeviceChannel.startByte < 0) || (newDeviceChannel.startByte > (newDevice.numBytes-1))) {
                            //QMessageBox::information(0, "error", QString("Config file error: startByte must be between 0 and ") + QString("%1").arg(newDevice.numBytes-1) + QString(". Read value is '") + deviceChannelElement.attribute("startByte", "") + QString("'."));
                            qDebug() << "[ParseTrodesConfig] Error: startByte must be between 0 and numbytes-1 for the device";
                            return -2;
                        }

                        newDeviceChannel.digitalBit = deviceChannelElement.attribute("bit", "0").toInt(&ok);
                        if ((newDeviceChannel.dataType == DeviceChannel::DIGITALTYPE) && (!ok)) {
                            //QMessageBox::information(0, "error", QString("Config file error: Error in converting bit value to number in device configuration. Read value is '") + deviceChannelElement.attribute("bit", "") + QString("'."));
                            return -2;
                        }
                        else if ((newDeviceChannel.dataType == DeviceChannel::DIGITALTYPE) && ((newDeviceChannel.digitalBit < 0) || (newDeviceChannel.digitalBit > 7))) {
                            //QMessageBox::information(0, "error", QString("Config file error: bit value must be between 0 and 7. Read value is '") + deviceChannelElement.attribute("bit", "") + QString("'."));
                            return -2;
                        }

                        newDeviceChannel.interleavedDataIDByte = deviceChannelElement.attribute("interleavedDataIDByte", "-1").toInt();
                        if (newDeviceChannel.interleavedDataIDByte != -1) {
                            newDeviceChannel.interleavedDataIDByte = newDeviceChannel.interleavedDataIDByte;
                            newDeviceChannel.interleavedDataIDBit = deviceChannelElement.attribute("interleavedDataIDBit","-1").toInt(&ok);
                            if (!ok) {
                                qDebug() << "[ParseTrodesConfig] Error: An interleavedDataIDByte entry must be accompanied by an interleavedDataIDBit entry.";
                                return -1;
                            }
                        }


                        //This next section assigns a port number to each digital line. However, it assumes that there is only an ECU connected
                        //which is often not right.  MUST BE FIXED!!!
                        if (newDeviceChannel.startByte >= 0 && newDeviceChannel.startByte < 4 && newDeviceChannel.dataType == DeviceChannel::DIGITALTYPE) {
                            //Hardcoded digital input port
                            // port number determined by its relative bit position in the header (one indexed)
                            newDeviceChannel.port = (newDeviceChannel.startByte * 8 + newDeviceChannel.digitalBit) + 1;
                            newDeviceChannel.input = true;
                        }
                        else if (newDeviceChannel.startByte >= 4 && newDeviceChannel.startByte < 8 && newDeviceChannel.dataType == DeviceChannel::DIGITALTYPE) {
                            //Hardcoded digital output port
                            newDeviceChannel.port = (newDeviceChannel.startByte * 8 + newDeviceChannel.digitalBit) + 1 - 32;
                            newDeviceChannel.input = false;
                        }
                        else if (newDeviceChannel.idString.contains("Ain") && newDeviceChannel.dataType  == DeviceChannel::INT16TYPE) { //make sure analog Ains have their input set to TRUE
                            newDeviceChannel.input = true;
                        }
                        else if (newDeviceChannel.dataType == DeviceChannel::DIGITALTYPE) {
                            //QMessageBox::information(0, "error", QString("Config file error: Channel startByte 0 to 8 must be digital type."));
                            return -2;
                        }


                        // append this to the appropriate list
                        /*
                    if (newDeviceChannel.input) {
                        digInIDList.append(newDeviceChannel.idString);
                        digInPortList.append(newDeviceChannel.port);
                    }
                    else {
                        digOutIDList.append(newDeviceChannel.idString);
                        digOutPortList.append(newDeviceChannel.port);

                    }*/


                        newDevice.channels.append(newDeviceChannel);
                    }

                    devChanN = devChanN.nextSibling();

                }

                int insertIndex = 0;
                for (int d=0; d<devices.length();d++) {
                    if (devices.at(d).packetOrderPreference < newDevice.packetOrderPreference) {
                        insertIndex++;
                    } else if (devices.at(d).packetOrderPreference == newDevice.packetOrderPreference) {
                        //Two devices have the same preference number. Error out.
                        qDebug() << "[ParseTrodesConfig] Error: Two hardware devices share the same packetOrderPreference.";
                        return -1;
                    } else {
                        //This device should go after the device that needs to be inserted.
                        break;
                    }
                }
                devices.insert(insertIndex,newDevice);
                //devices.append(newDevice);


            } else if (hardwareElement.tagName() == "SourceOptions") {
                //This section is describing the custom control settings associated with a data source

                SourceControl newSourceControl;
                bool ok;
                newSourceControl.name = hardwareElement.attribute("name","");
                newSourceControl.unitNumber = hardwareElement.attribute("unit","0").toInt(&ok);
                if (!ok) {
                    return -2;
                }

                QDomNode sourceControlN = n.firstChild();
                QDomElement customSourceElement;
                while (!sourceControlN.isNull()) {

                    customSourceElement = sourceControlN.toElement();
                    if (!customSourceElement.isNull()) {
                        CustomSourceOption newCustomSourceOption;
                        newCustomSourceOption.name = customSourceElement.attribute("name","");


                        QString tmpDataString = customSourceElement.attribute("data","");
                        QStringList stringList= tmpDataString.split(" ",Qt::SkipEmptyParts);
                        for (int i=0;i<stringList.length();i++) {
                            double tmpnum = stringList.at(i).toFloat(&ok);
                            if (!ok) break;                            
                            newCustomSourceOption.data.append(tmpnum);
                        }


                        newSourceControl.customOptions.append(newCustomSourceOption);

                    }

                    sourceControlN = sourceControlN.nextSibling();

                }
                sourceControls.append(newSourceControl);
            }
            n = n.nextSibling();
        }
    }

    //Calculate the total length of the section of the packet that contains aux data
    int tempAuxPacketSize = 1; //in bytes. //The fist byte is reserved for sync, so we start with 1
    QVector<int> preferenceList;
    for (int dNum = 0; dNum<devices.length();dNum++) {
        if (devices[dNum].available && devices[dNum].name != "SysClock") {
//        if (devices[dNum].available) {
            tempAuxPacketSize = tempAuxPacketSize+devices[dNum].numBytes;
            preferenceList.append(devices[dNum].packetOrderPreference);
        }
    }



    //qSort(preferenceList.data(),preferenceList.data()+preferenceList.length()-1);
    std::sort(preferenceList.data(),preferenceList.data()+preferenceList.length()-1);


    //If the header size has not been manually defined, calculate it here
    if (headerSize == 0) {
        if ((tempAuxPacketSize % 2)!=0) {
            qDebug() << "[ParseTrodesConfig] Error: Total header size must be an even number of bytes";
            return -1;
        }

        //Downstream code ussumes haederSize is in int16 steps. This will probably change.
        headerSize = tempAuxPacketSize/2;
        //qDebug() << "[ParseTrodesConfig] Header size: " << headerSize;

    }

    //Calculate the byte offset within each packet where each device's data begins
    int currentOffset= 1; //The first byte is reserved for sync, so we start with 1
    for (int availInd = 0; availInd<preferenceList.length();availInd++) {

        for (int dNum = 0; dNum<devices.length();dNum++) {
            if (devices[dNum].name == "SysClock") {
                devices[dNum].byteOffset = headerSize*2;
            } else {
                if (devices[dNum].packetOrderPreference == preferenceList[availInd]) {
                    devices[dNum].byteOffset = currentOffset;
                    //qDebug() << devices[dNum].name << currentOffset;
                    for (int devCh=0; devCh<devices[dNum].channels.length();devCh++) {
                        devices[dNum].channels[devCh].startByte += currentOffset;
                        if (devices[dNum].channels[devCh].interleavedDataIDByte > -1) {
                            devices[dNum].channels[devCh].interleavedDataIDByte += currentOffset;
                        }


                    }
                    currentOffset += devices[dNum].numBytes;
                    break;
                }
            }
        }
    }

    return 0;
}

void HardwareConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode) {

    QDomElement hconf = doc.createElement("HardwareConfiguration");

    rootNode.appendChild(hconf);

    hconf.setAttribute("numChannels", NCHAN);
    hconf.setAttribute("lfpSubsamplingInterval",lfpSubsamplingInterval);
    hconf.setAttribute("samplingRate", sourceSamplingRate);
    hconf.setAttribute("useIntrinsics", useIntrinsics);
    //hconf.setAttribute("numStimChannels", numStimChan);
#ifdef PHOTOMETRY_CODE
    hconf.setAttribute("numFibers", NFIBER);
    hconf.setAttribute("APDsamplingRate", APDsamplingRate);
#endif
    if (headerSizeManuallyDefined) {
        hconf.setAttribute("headerSize", headerSize);
    }

    if (!headerSizeManuallyDefined) {
        for (int j = 0; j < devices.length(); j++) {
            QDomElement devElem = doc.createElement("Device");
            devElem.setAttribute("name", devices[j].name);
            devElem.setAttribute("packetOrderPreference", devices[j].packetOrderPreference);
            devElem.setAttribute("numBytes", devices[j].numBytes);
            devElem.setAttribute("available", (int)devices[j].available);

            for (int ch = 0; ch < devices[j].channels.length(); ch++) {
                QDomElement chanElem = doc.createElement("Channel");
                chanElem.setAttribute("id",devices[j].channels[ch].idString);

                if (devices[j].channels[ch].dataType == DeviceChannel::DIGITALTYPE) {
                    chanElem.setAttribute("dataType", "digital");
                }
                else if (devices[j].channels[ch].dataType == DeviceChannel::INT16TYPE) {
                    chanElem.setAttribute("dataType", "analog");
                }
                else if (devices[j].channels[ch].dataType == DeviceChannel::UINT32TYPE) {
                    chanElem.setAttribute("dataType", "uint32");
                }
                chanElem.setAttribute("startByte",devices[j].channels[ch].startByte-devices[j].byteOffset);

                chanElem.setAttribute("bit",devices[j].channels[ch].digitalBit);
                chanElem.setAttribute("input",(int)devices[j].channels[ch].input);
                //chanElem.setAttribute("port",(int)devices[j].channels[ch].port);


                //If the channel is interleaved, write the interleave info
                if (devices[j].channels[ch].interleavedDataIDByte > -1) {
                    chanElem.setAttribute("interleavedDataIDByte", devices[j].channels[ch].interleavedDataIDByte-devices[j].byteOffset);

                    chanElem.setAttribute("interleavedDataIDBit", devices[j].channels[ch].interleavedDataIDBit);
                }
                devElem.appendChild(chanElem);
            }

            hconf.appendChild(devElem);
        }

        for (int j = 0; j < sourceControls.length(); j++) {
            QDomElement devElem = doc.createElement("SourceOptions");
            devElem.setAttribute("name", sourceControls[j].name);



            for (int ch = 0; ch < sourceControls[j].customOptions.length(); ch++) {
                QDomElement chanElem = doc.createElement("CustomOption");
                chanElem.setAttribute("name",sourceControls[j].customOptions[ch].name);
                QString dataOutString;
                for (int dInd=0;dInd<sourceControls[j].customOptions[ch].data.length();dInd++) {
                    dataOutString += QString("%1 ").arg(sourceControls[j].customOptions[ch].data.at(dInd));
                }
                chanElem.setAttribute("data",dataOutString);
                devElem.appendChild(chanElem);
            }

            hconf.appendChild(devElem);
        }
    }

}


/* Network Configuration */


NetworkConfiguration::NetworkConfiguration()
{
    dataSocketType = 0;
    trodesHost = "";
    networkConfigFound = false;
    networkTypeConfiguredinWorkspace = false;
    networkAddressConfiguredinWorkspace = false;
    //lfpRate = 1500;

    //defaultNetworkType = zmq_based;
    defaultNetworkType = qsocket_based;

#ifdef __APPLE__
    defaultNetworkType = qsocket_based;
#endif

/*#ifdef _WIN32
    defaultNetworkType = qsocket_based;
#endif*/




    networkType = defaultNetworkType;
    //networkType = zmq_based;
}

NetworkConfiguration::~NetworkConfiguration()
{
}

int NetworkConfiguration::loadFromXML(QDomNode &networkConfNode)
{
    trodesHost = networkConfNode.toElement().attribute("trodesHost", "");
    trodesPort = networkConfNode.toElement().attribute("trodesPort", "").toUInt();

    //lfpRate = networkConfNode.toElement().attribute("lfpRate", "1500").toInt();

    // trodesPort will be zero if nothing is specified
    if (!trodesHost.isEmpty()) {
        networkAddressConfiguredinWorkspace = true;
    }

    if ((trodesPort != 0) && ((trodesPort < 1025)/* || (trodesPort > 65535)*/)) { //trodesPort>65535 literally impossible. quint16 cannot hold values > 65535
        //QMessageBox::information(0, "error", QString("Config file error: trodes port %1 must be between between 1025 and 65535.").arg(trodesPort));
        return -2;
    }
    QString networkTypeString = networkConfNode.toElement().attribute("networkType", "default");
    if (networkTypeString == "default") {
        //qDebug() << "Workspace does not request a specific network type. Using default qsocket based network";
        networkType = defaultNetworkType;
        networkTypeConfiguredinWorkspace = false;
    } else if (networkTypeString == "qsocket") {
        //qDebug() << "Workspace requests qsocket based network.";
        networkTypeConfiguredinWorkspace = true;
        networkType = qsocket_based;
    } else if (networkTypeString == "zmq") {
        //qDebug() << "Workspace requests ZMQ based network.";
        networkTypeConfiguredinWorkspace = true;
        networkType = zmq_based;
    } else {
        //qDebug() << "Workspace does not request a specific network type. Using default qsocket based network";
        networkType = defaultNetworkType;
        networkTypeConfiguredinWorkspace = false;
    }

    hardwareAddress = networkConfNode.toElement().attribute("hardwareAddress", TRODESHARDWARE_DEFAULTIP);
    // Note that the ports are set in the main read configuration function

    QString tmpSocketType = networkConfNode.toElement().attribute("dataSocketType", "");
    if (tmpSocketType.toUpper() == "TCPIP") {
        dataSocketType = TRODESSOCKETTYPE_TCPIP;
    }
    else if (tmpSocketType.toUpper() == "UDP") {
        dataSocketType = TRODESSOCKETTYPE_UDP;
    }
    else if (tmpSocketType.toUpper() == "LOCAL") {
        //NOTE: not yet implemented in the rest of the code
        dataSocketType = TRODESSOCKETTYPE_LOCAL;
    }
    else if (tmpSocketType != "") {
        //QMessageBox::information(0, "error", QString("Config file error: dataSocketType must be TCPIP, UDP or LOCAL."));
        return -2;
    }

    return 0;
}

void NetworkConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode)
{
    QDomElement nconf = doc.createElement("NetworkConfiguration");

    rootNode.appendChild(nconf);

    //if (networkTypeConfiguredinWorkspace) {
        if (networkType == qsocket_based) {
            nconf.setAttribute("networkType", "qsocket");
        } else if (networkType == zmq_based) {
            nconf.setAttribute("networkType", "zmq");
        }
    //}


    if (networkAddressConfiguredinWorkspace) {
        nconf.setAttribute("trodesHost", trodesHost);
        nconf.setAttribute("trodesPort", QString("%1").arg(trodesPort));
    }

    //These items should probably not have user control
    /*
    nconf.setAttribute("hardwareAddress", hardwareAddress);
    if (dataSocketType == TRODESSOCKETTYPE_TCPIP) {
        nconf.setAttribute("dataSocketType", "TCPIP");
    }
    else if (dataSocketType == TRODESSOCKETTYPE_UDP) {
        nconf.setAttribute("dataSocketType", "UDP");
    }
    else if (dataSocketType == TRODESSOCKETTYPE_LOCAL) {
        nconf.setAttribute("dataSocketType", "UDP");
    }*/
}




/* Module Configuration */

ModuleConfiguration::ModuleConfiguration(QObject *)
{
}

ModuleConfiguration::~ModuleConfiguration()
{
    while (!singleModuleConf.isEmpty()) {
        singleModuleConf.takeLast();
    }
}

int ModuleConfiguration::loadFromXML(QDomNode &moduleConfNode)
{
    QDomNode n = moduleConfNode.firstChild();

    QString dataToList;
    QString portString;
    bool ok;

    while (!n.isNull()) {
        QDomElement nt = n.toElement();
        if (!nt.isNull()) {
            SingleModuleConf tmpModuleConf;
            // initialize the hostName in case it is not set

            tmpModuleConf.moduleName = nt.attribute("moduleName", "");
            //qDebug() << "[ParseTrodesConfig] Reading in configuration for module: " << tmpModuleConf.moduleName;
            tmpModuleConf.sendTrodesConfig = nt.attribute("sendTrodesConfig", "0").toInt(&ok);

            if (!ok) {
                /*QMessageBox::information(
                            0, "error", QString(
                                "Config file error: Error in converting "
                                "sendTrodesConfig in Module Configuration. "
                                "Read values are '") +
                            nt.attribute("sendTrodesConfig", "") + QString("'"));*/
                return -2;
            }
            tmpModuleConf.sendNetworkInfo = nt.attribute("sendNetworkInfo", "0").toInt(&ok);
            if (!ok) {
                /*QMessageBox::information(
                            0, "error", QString(
                                "Config file error: Error in converting "
                                "sendNetworkInfo to number in Module Configuration. "
                                "Read values are '") +
                            nt.attribute("sendNetorkInfo", "") + QString("'"));*/
                return -2;
            }
            tmpModuleConf.hostName = nt.attribute("hostName", "localhost");

            QDomNode argNode = n.firstChild();
            QDomElement argElement;
            while (!argNode.isNull()) {
                argElement = argNode.toElement();
                if (!argElement.isNull()) {
                    //qDebug() << "[ParseTrodesConfig]" << argElement.attribute("flag","") << " " << argElement.attribute("value", "");
                    tmpModuleConf.moduleArguments.push_back(argElement.attribute("flag", ""));
                    tmpModuleConf.moduleArguments.push_back(argElement.attribute("value", ""));
                }
                argNode = argNode.nextSibling();
            }
            singleModuleConf.append(tmpModuleConf);
        }
        n = n.nextSibling();
    }

    if (singleModuleConf.length() > 0) {
        modulesDefined = 1;
    }

    return 0;
}

void ModuleConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode)
{
    QDomElement mconf = doc.createElement("ModuleConfiguration");

    rootNode.appendChild(mconf);

    for (int i = 0; i < singleModuleConf.length(); i++) {
        QDomElement nt = doc.createElement("SingleModuleConfiguration");

        nt.setAttribute("moduleName", singleModuleConf[i].moduleName);
        //nt.setAttribute("modulePath",singleModuleConf[i].modulePath);
        //nt.setAttribute("moduleConfigFile",singleModuleConf[i].moduleConfigFile);
        nt.setAttribute("sendTrodesConfig", singleModuleConf[i].sendTrodesConfig);
        nt.setAttribute("sendNetworkInfo", singleModuleConf[i].sendNetworkInfo);
        for (int j = 0; j < singleModuleConf[i].moduleArguments.length(); j=j+2) {
            if (singleModuleConf[i].moduleArguments.length() > j+1) {
                QDomElement arg = doc.createElement("Argument");
                arg.setAttribute("flag", singleModuleConf[i].moduleArguments[j]);
                arg.setAttribute("value", singleModuleConf[i].moduleArguments[j+1]);
                nt.appendChild(arg);
            }
        }


        mconf.appendChild(nt);
    }

}

bool ModuleConfiguration::modulePresent(QString modName) {
    // returns true if the requested module is listed in the config file
    for (int i = 0; i < singleModuleConf.length(); i++) {
        if (singleModuleConf[i].moduleName.contains(modName, Qt::CaseInsensitive)) {
            return true;
        }
    }
    return false;
}

int ModuleConfiguration::findModule(QString modName) {
    // returns the index of the requested module if listed in the config file
    for (int i = 0; i < singleModuleConf.length(); i++) {
        if (singleModuleConf[i].moduleName.contains(modName, Qt::CaseInsensitive)) {
            return i;
        }
    }
    return -1;
}

/* --------------------------------------------------------------------- */
/* --------------------------------------------------------------------- */
//Stream configuration

streamConfiguration::streamConfiguration(bool dontCreateFilter)
{


    //this if statement is for the workspaceGui to specifi that it doesn't need to create the data filters on initialization
    //which prevents an occassional bad_alloc crash
    //if (!dontCreateFilter) {
//        spikeFilters.reserve(hardwareConf->NCHAN);

        /*for (int i=0; i<hardwareConf->NCHAN;i++) {
            spikeFilters.push_back(new BesselFilter());
            spikeFilters.last()->setSamplingRate(hardwareConf->sourceSamplingRate);
            notchFilters.push_back(new NotchFilter());
            notchFilters.last()->setSamplingRate(hardwareConf->sourceSamplingRate);
        }*/
//        lfpFilters.reserve(spikeConf->ntrodes.length());
        /*for(int trode = 0; trode < 28; trode++){
            //lfpFilters.push_back(new BesselFilter());
            //lfpFilters.last()->setSamplingRate(hardwareConf->sourceSamplingRate);
            //lfpFilters.last()->setFilterRange(0,spikeConf->ntrodes[trode]->moduleDataHighFilter);
            spikeModeOn.push_back(true);
            lfpModeOn.push_back(false);
        }*/
    //}


#ifdef PHOTOMETRY_CODE
    photometryFilters = new ButterworthFilter[hardwareConf->NFIBER*4];
    for (int i=0; i<hardwareConf->NFIBER;i++) {
        for (int j=0 ; j < 4; j++) {
            if(photometryConf->nfibers[i]->filterOn[j]){
                photometryFilters[i*4+j].setSamplingRate(hardwareConf->sourceSamplingRate);
                photometryFilters[i*4+j].setFilterRange(photometryConf->nfibers[i]->lowFilter[j],
                                                        photometryConf->nfibers[i]->highFilter[j]);
                qDebug() << "add filter " << i*4+j;
            }
        }
    }
#endif
    nColumns = 1;
    nTabs = 1;
    tLength = 1.0;
    FS = 25000;
    horizontalLayout = false;

    //saveHWChan = nullptr;

}

streamConfiguration::~streamConfiguration()
{
    /*if (saveHWChan != nullptr) {
        delete [] saveHWChan;
        saveHWChan = nullptr;
    }*/
}

/*void streamConfiguration::setSpikeModeOn(int ntrode, bool on, bool emitchange){
    spikeModeOn[ntrode] = on;
    if(emitchange) emit updatedSpikeMode();
}

void streamConfiguration::setLFPModeOn(int ntrode, bool on, bool emitchange){
    lfpModeOn[ntrode] = on;
    if(emitchange) emit updatedSpikeMode();
}

void streamConfiguration::setAllModesOn(bool spike, bool lfp){
    //spikeModeOn.fill(spike);
    //lfpModeOn.fill(lfp);

    memset(spikeModeOn.data(), spike, spikeModeOn.length());
    memset(lfpModeOn.data(), lfp, lfpModeOn.length());

    emit updatedAllModes();
}*/
void streamConfiguration::setTLength(double newTLength)
{
    tLength = newTLength;

    emit updatedTLength(tLength);
}

/*int streamConfiguration::loadFromXML(QDomNode &eegDispConfNode) {
    //Overloaded method that still uses global variable

    loadFromXML(eegDispConfNode,hardwareConf,spikeConf);
}*/

bool streamConfiguration::createIndexData(HardwareConfiguration *hconf, SpikeConfiguration *spconf) {

    bool okFlag = true;

    trodeIndexLookupByHWChan.clear();
    trodeChannelLookupByHWChan.clear();


    for (int n = 0; n < hconf->NCHAN; n++) {
        trodeIndexLookupByHWChan.push_back(-1);
        trodeChannelLookupByHWChan.push_back(-1);
    }
    for (int tmpTrodeIndex = 0; tmpTrodeIndex < spconf->ntrodes.length(); tmpTrodeIndex++) {
        for (int tmpChanNum = 0; tmpChanNum < spconf->ntrodes[tmpTrodeIndex]->maxDisp.length();
               tmpChanNum++) {

            trodeIndexLookupByHWChan[spconf->ntrodes[tmpTrodeIndex]->hw_chan[tmpChanNum]] = tmpTrodeIndex;
            if (trodeChannelLookupByHWChan[spconf->ntrodes[tmpTrodeIndex]->hw_chan[tmpChanNum]] != -1) {
                qDebug() << "Warning - HW channel" << spconf->ntrodes[tmpTrodeIndex]->unconverted_hw_chan[tmpChanNum] << "is assigned more than once. NOT SUPPORTED!";

                okFlag = false;
            }
            trodeChannelLookupByHWChan[spconf->ntrodes[tmpTrodeIndex]->hw_chan[tmpChanNum]] = tmpChanNum;
        }
    }

    return okFlag;

}

int streamConfiguration::loadFromXML(QDomNode &eegDispConfNode)
{
    //bool warnFlag = false;
    bool ok;


    nColumns = eegDispConfNode.toElement().attribute("columns", "2").toInt();
    nTabs = eegDispConfNode.toElement().attribute("pages", "2").toInt();
    horizontalLayout = eegDispConfNode.toElement().attribute("horizontal", "0").toInt();
    pageBreaks.clear();
    QString tmpDataString = eegDispConfNode.toElement().attribute("pageBreaks","");
    QStringList stringList= tmpDataString.split(" ",Qt::SkipEmptyParts);
    for (int i=0;i<stringList.length();i++) {
        int tmpnum = stringList.at(i).toInt(&ok);
        if (!ok) {
            pageBreaks.clear();
            break;
        }
        pageBreaks.append(tmpnum);
    }

    if (nColumns < 1) {
        qDebug() << "Error - allowed range for number of stream display columns per page is 1 through 4";
        nColumns = 1;
    }
    if (nColumns > 4) {
        qDebug() << "Error - allowed range for number of stream display columns per page is 1 through 4";
        nColumns = 4;
    }
    tLength = 1.0;

    nChanConfigured = 0;

    backgroundColor.setNamedColor(eegDispConfNode.toElement().attribute("backgroundColor", "#808080")); //default color is gray (RGB in hex)

    /*trodeIndexLookupByHWChan.clear();
    trodeChannelLookupByHWChan.clear();

    for (int n = 0; n < hconf->NCHAN; n++) {
        trodeIndexLookupByHWChan.push_back(-1);
        trodeChannelLookupByHWChan.push_back(-1);
    }
    for (int tmpTrodeIndex = 0; tmpTrodeIndex < spconf->ntrodes.length(); tmpTrodeIndex++) {
        for (int tmpChanNum = 0; tmpChanNum < spconf->ntrodes[tmpTrodeIndex]->maxDisp.length();
               tmpChanNum++) {

            trodeIndexLookupByHWChan[spconf->ntrodes[tmpTrodeIndex]->hw_chan[tmpChanNum]] = tmpTrodeIndex;
            if (trodeChannelLookupByHWChan[spconf->ntrodes[tmpTrodeIndex]->hw_chan[tmpChanNum]] != -1) {
                qDebug() << "Warning - HW channel" << spconf->ntrodes[tmpTrodeIndex]->unconverted_hw_chan[tmpChanNum] << "is assigned more than once. NOT SUPPORTED!";

                warnFlag = true;
            }
            trodeChannelLookupByHWChan[spconf->ntrodes[tmpTrodeIndex]->hw_chan[tmpChanNum]] = tmpChanNum;
        }
    }*/
#ifdef PHOTOMETRY_CODE
    if(hardwareConf->NFIBER>0){
        for (int n = 0; n < hardwareConf->NFIBER*4; n++) {
            fiberIndexLookupByHWChan.push_back(-1);
            fiberChannelLookupByHWChan.push_back(-1);
        }
        for (int tmpFiberIndex = 0; tmpFiberIndex < photometryConf->nfibers.length(); tmpFiberIndex++) {
            for (int tmpChanNum = 0; tmpChanNum < photometryConf->nfibers[tmpFiberIndex]->maxDisp.length();
                   tmpChanNum++, nChanConfigured++) {
                fiberIndexLookupByHWChan[photometryConf->nfibers[tmpFiberIndex]->hw_chan[tmpChanNum]] = tmpFiberIndex;
                fiberChannelLookupByHWChan[photometryConf->nfibers[tmpFiberIndex]->hw_chan[tmpChanNum]] = tmpChanNum;
            }
        }
    }
#endif
    //setChanToSave(hconf->NCHAN);



    //if (warnFlag) return 100;  //100 creates a warning dialog for the last debug statement

    return 0;
}

void streamConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode)
{
    QDomElement streamdisp = doc.createElement("StreamDisplay");

    rootNode.appendChild(streamdisp);
    streamdisp.setAttribute("columns", nColumns);
    streamdisp.setAttribute("pages", nTabs);
    streamdisp.setAttribute("backgroundColor", backgroundColor.name());
    streamdisp.setAttribute("horizontal", horizontalLayout);

    QString dataOutString;
    for (int dInd=0;dInd<pageBreaks.length();dInd++) {
        dataOutString += QString("%1 ").arg(pageBreaks.at(dInd));
    }
    streamdisp.setAttribute("pageBreaks",dataOutString);
    //streamdisp.setAttribute("tLength",tLength);
}

void streamConfiguration::setChanToSave(int numHWChan) {

    saveHWChan.clear();
    for (int i=0;i<numHWChan; i++) {
        saveHWChan.push_back(false);
    }

    // go through the channel list and find the hardware channels to be saved.  This is only
    // used if saveDisplayedChanOnly is set to true
    for (int i = 0; i < numHWChan; i++) {
        if (trodeChannelLookupByHWChan[i] != -1) {
            saveHWChan[i] = true;
            nChanConfigured++;
            //qDebug() << "saveHWChan" << i << saveHWChan[i] << streamConf->trodeChannelLookupByHWChan[i];
        }
        else {
            saveHWChan[i] = false;
        }
    }

}
void streamConfiguration::listChanToSave(int numHWChan)
{
    for (int i = 0; i < numHWChan; i++) {
        qDebug() << "[ParseTrodesConfig] saveHWChan" << i << saveHWChan[i] << trodeChannelLookupByHWChan[i];
    }
}

void streamConfiguration::setBackgroundColor(QColor c) {
    backgroundColor = c;
    emit updatedBackgroundColor(c);
}

/* --------------------------------------------------------------------- */




headerDisplayConfiguration::headerDisplayConfiguration(QObject *)
{
}

headerDisplayConfiguration::~headerDisplayConfiguration()
{
    while (!headerChannels.isEmpty()) {
        headerChannels.takeLast();
    }
}

int headerDisplayConfiguration::maxDigitalPort(bool input) {
    int maxPort = 0;

    QList<int> portList;

    portList = (input == 1) ? digInPortList: digOutPortList;

    for (int i = 0; i < portList.length(); i++) {
        if (portList[i] > maxPort) {
            maxPort = portList[i];
        }
    }
    //qDebug() << "In maxDigitalPort, portlist" << portList << "maxPort" << maxPort;

    return maxPort;
}

int headerDisplayConfiguration::minDigitalPort(bool input) {
    int minPort = 10000000;

    QList<int> portList;

    portList = (input == 1) ? digInPortList: digOutPortList;

    for (int i = 0; i < portList.length(); i++) {
        if (portList[i] < minPort) {
            minPort = portList[i];
        }
    }
    //qDebug() << "In minDigitalPort, portlist" << portList << "minPort" << minPort;
    return minPort;
}

bool headerDisplayConfiguration::digitalPortValid(int port, bool input) {
    QList<int> portList;

    portList = (input == 1) ? digInPortList: digOutPortList;

    if (portList.indexOf(port) == -1) {
        return false;
    }
    return true;
}

bool headerDisplayConfiguration::digitalIDValid(QString ID, bool input) {

    QStringList IDList;

    IDList = (input == 1) ? digInIDList: digOutIDList;

    if (!IDList.contains(ID,Qt::CaseInsensitive)) {
        return false;
    }
    return true;
}

int headerDisplayConfiguration::loadFromXML(QDomNode &headerConfNode) {
    //Overloaded method that still uses global variable
    return loadFromXML(headerConfNode, hardwareConf);
}

int headerDisplayConfiguration::loadFromXML(QDomNode &headerConfNode, HardwareConfiguration *HWConf)
{
    QDomNode n = headerConfNode.firstChild();

    bool ok;

    //int maxDigitalInputPort = 0;
    //int maxDigitalOutputPort = 0;
    //int maxAnalogInputPort = 0;
    //int maxAnalogOutputPort = 0;

    while (!n.isNull()) {
        QDomElement nt = n.toElement();
        if (!nt.isNull()) {
            //int offset = 0;
            headerChannel tmpHeaderChan;
            bool skipChan = false;

            tmpHeaderChan.idString = nt.attribute("id", "");
            tmpHeaderChan.color.setNamedColor(nt.attribute("color", "#808080"));  //default color is gray (RGB in hex)
            tmpHeaderChan.maxDisp = nt.attribute("maxDisp", "1").toInt(&ok);


            tmpHeaderChan.storeStateChanges = nt.attribute("analyze", "0").toInt(&ok) && ok; //if true, then Trodes will store the times of every state change
            if (!HWConf->headerSizeManuallyDefined) {

                tmpHeaderChan.deviceName = nt.attribute("device", "");
                if (!tmpHeaderChan.deviceName.isEmpty()) {

                    if (HWConf != NULL) {
                        bool deviceFound = false;
                        for (int i=0; i<HWConf->devices.length();i++) {
                            if (HWConf->devices[i].name.compare(tmpHeaderChan.deviceName)==0) {
                                //offset = hardwareConf->devices[i].packetOffset;
                                deviceFound = true;
                                if (!HWConf->devices[i].available) {
                                    //This device has been turned off
                                    skipChan = true;
                                    break;
                                }

                                //Look up the hardware info for this channel
                                bool channelFound = false;
                                for (int devCh = 0; devCh < HWConf->devices[i].channels.length();devCh++) {
                                    if (HWConf->devices[i].channels[devCh].idString.compare(tmpHeaderChan.idString)==0) {
                                        channelFound = true;
                                        tmpHeaderChan.port = HWConf->devices[i].channels[devCh].port;
                                        tmpHeaderChan.dataType = HWConf->devices[i].channels[devCh].dataType;
                                        tmpHeaderChan.startByte = HWConf->devices[i].channels[devCh].startByte;
                                        tmpHeaderChan.digitalBit = HWConf->devices[i].channels[devCh].digitalBit;
                                        tmpHeaderChan.interleavedDataIDByte = HWConf->devices[i].channels[devCh].interleavedDataIDByte;
                                        tmpHeaderChan.interleavedDataIDBit = HWConf->devices[i].channels[devCh].interleavedDataIDBit;
                                        tmpHeaderChan.input = HWConf->devices[i].channels[devCh].input;
                                        break;
                                    }
                                }
                                if (!channelFound) {
                                    qDebug() << "[ParseTrodesConfig] Error: Channel name in headerdisplay not found in hardware config:" << tmpHeaderChan.idString;
                                    return -1;
                                }

                                break;
                            }
                        }
                        if (!deviceFound) {
                            qDebug() << "[ParseTrodesConfig] Error: Device name in headerdisplay not found in hardware config:" << tmpHeaderChan.deviceName;
                            return -1;
                        }

                    } else {
                        qDebug() << "[ParseTrodesConfig] Error: Hardware config must be defined before headerdisplay config.";
                        return -1;

                    }
                } else {
                    qDebug() << "[ParseTrodesConfig] Error: Each aux display entry must reference a device.";
                    return -1;
                }
            } else {

                //If no devices are defined, the hardware info for the channel can be defined in the display section
                QString dataTypeString = nt.attribute("dataType", "");

                if (dataTypeString == "digital") {
                    tmpHeaderChan.dataType = DeviceChannel::DIGITALTYPE;
                }
                else if (dataTypeString == "analog") {
                    tmpHeaderChan.dataType = DeviceChannel::INT16TYPE;
                }
                else if (dataTypeString == "uint32") {
                    tmpHeaderChan.dataType = DeviceChannel::UINT32TYPE;
                }
                else {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in parsing data type: ") + dataTypeString);
                    return -2;
                }
                tmpHeaderChan.port = nt.attribute("port", "1").toInt(&ok);
                tmpHeaderChan.input = nt.attribute("input", "1").toInt(&ok);
                tmpHeaderChan.startByte = nt.attribute("startByte", "1").toInt(&ok);
                //qDebug() << "CONFIGURATION: " << tmpHeaderChan.port << " " << tmpHeaderChan.input << " " << tmpHeaderChan.startByte;
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting startByte to number in header display configuration. Read value is '") + nt.attribute("startByte", "") + QString("'."));
                    return -2;
                }
                else if ((tmpHeaderChan.startByte < 1) || (tmpHeaderChan.startByte >= HWConf->headerSize * 2)) {
                    //QMessageBox::information(0, "error", QString("Config file error: startByte must be between 1 and ") + QString("%1").arg((hardwareConf->headerSize * 2) - 1) + QString(". Read value is '") + nt.attribute("startByte", "") + QString("'."));
                    return -2;
                }

                tmpHeaderChan.digitalBit = nt.attribute("bit", "0").toInt(&ok);
                if ((tmpHeaderChan.dataType == DeviceChannel::DIGITALTYPE) && (!ok)) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting bit value to number in header display configuration. Read value is '") + nt.attribute("bit", "") + QString("'."));
                    return -2;
                }
                else if ((tmpHeaderChan.dataType == DeviceChannel::DIGITALTYPE) && ((tmpHeaderChan.digitalBit < 0) || (tmpHeaderChan.digitalBit > 7))) {
                    //QMessageBox::information(0, "error", QString("Config file error: bit value must be between 0 and 7. Read value is '") + nt.attribute("bit", "") + QString("'."));
                    return -2;
                }

                tmpHeaderChan.interleavedDataIDByte = nt.attribute("interleavedDataIDByte", "-1").toInt();
                if (tmpHeaderChan.interleavedDataIDByte != -1) {
                    tmpHeaderChan.interleavedDataIDByte = tmpHeaderChan.interleavedDataIDByte;
                    tmpHeaderChan.interleavedDataIDBit = nt.attribute("interleavedDataIDBit","-1").toInt(&ok);
                    if (!ok) {
                        qDebug() << "[ParseTrodesConfig] Error: An interleavedDataIDByte entry must be accompanied by an interleavedDataIDBit entry.";
                        return -1;
                    } else {
                        //qDebug() << "Interleaved channel: " << tmpHeaderChan.idString;
                    }
                }
            }

            if (!skipChan) {
                // append this to the appropriate list
                if (tmpHeaderChan.input) {
                    digInIDList.append(tmpHeaderChan.idString);
                    digInPortList.append(tmpHeaderChan.port);
                }
                else {
                    digOutIDList.append(tmpHeaderChan.idString);
                    digOutPortList.append(tmpHeaderChan.port);
                }

                headerChannels.append(tmpHeaderChan);
            }
        }
        n = n.nextSibling();
    }

    return 0;
}

void headerDisplayConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode)
{
    //Overloaded method that still uses global variable
    saveToXML(doc,rootNode,hardwareConf);
}

void headerDisplayConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode,HardwareConfiguration *HWconf)
{
    QDomElement hconf = doc.createElement("AuxDisplayConfiguration");

    rootNode.appendChild(hconf);

    for (int i = 0; i < headerChannels.length(); i++) {

        QDomElement nt = doc.createElement("DispChannel");

        nt.setAttribute("id", headerChannels[i].idString);

        nt.setAttribute("maxDisp", headerChannels[i].maxDisp);
        nt.setAttribute("color", headerChannels[i].color.name());
        nt.setAttribute("analyze", (int)headerChannels[i].storeStateChanges);

        if (!HWconf->headerSizeManuallyDefined) {
            nt.setAttribute("device", headerChannels[i].deviceName);
        } else {

            //Include the hardware info in each display entry (no hardware devices defined)
            if (headerChannels[i].dataType == DeviceChannel::DIGITALTYPE) {
                nt.setAttribute("dataType", "digital");
            } else if (headerChannels[i].dataType == DeviceChannel::INT16TYPE) {
                nt.setAttribute("dataType", "analog");
            } else if (headerChannels[i].dataType == DeviceChannel::UINT32TYPE) {
                nt.setAttribute("dataType", "uint32");
            }
            nt.setAttribute("port", headerChannels[i].port);
            nt.setAttribute("input", headerChannels[i].input);
            nt.setAttribute("bit", headerChannels[i].digitalBit);
            nt.setAttribute("startByte", headerChannels[i].startByte);

            if (headerChannels[i].interleavedDataIDByte > -1) {
                nt.setAttribute("interleavedDataIDByte", headerChannels[i].interleavedDataIDByte);
                nt.setAttribute("interleavedDataIDBit", headerChannels[i].interleavedDataIDBit);
            }
        }
        hconf.appendChild(nt);
    }
}



/* --------------------------------------------------------------------- */
/* --------------------------------------------------------------------- */
// Spike Configuration

CategoryDictionary::CategoryDictionary(void) {
//    addCategory("All"); //add the default all category
}

bool CategoryDictionary::categoryExists(QString category) {
    return(categories.contains(category));
}

bool CategoryDictionary::tagExists(QString tag) {
    QHashIterator<QString, QHash<QString,QString> > iter(categories);
    while(iter.hasNext()) {
        iter.next();
        if (iter.value().contains(tag)) {
            return(true);
        }
    }
    return(false);
}

bool CategoryDictionary::gTagExists(GroupingTag gt) {
    return(isTagInCategory(gt.category,gt.tag));
}

bool CategoryDictionary::isTagInCategory(QString category, QString tag) {
    if (!categoryExists(category)) //if the category doesn't exist, the tag certainly isn't in it
        return(false);
    return(categories.value(category).contains(tag));
}

QString CategoryDictionary::getTagsCategory(QString tag) {
    QHashIterator<QString, QHash<QString,QString> > iter(categories);
    while(iter.hasNext()) {
        iter.next();
        if (iter.value().contains(tag) && iter.key() != "All") {
            return(iter.key());
        }
    }
    return("All"); //'All' is the system reserved category that will include all tags
}

QList<GroupingTag> CategoryDictionary::getSortedAllTagList() {
    QList<GroupingTag> retList;

    QList<QList<QString> > complexList;
    QHashIterator<GroupingTag,int> i(all);
    while(i.hasNext()) {
        i.next();
        QString curCata = i.key().category;
        QString curTag = i.key().tag;
        bool createNewCata = true;
        for (int j = 0; j < complexList.length(); j++) {
            if (complexList.at(j).at(0) == curCata) { //first item of each sub list will be catagory
                createNewCata = false;
                QList<QString> curTagList = complexList.at(j);
                bool tagAlreadyInList = false;
                for (int y = 1; y < curTagList.length(); y++) { //check the list to see if the tag is already in it
                    if (curTagList.at(y) == curTag) {
                        tagAlreadyInList = true;
                        break;
                    }

                }
                if (!tagAlreadyInList) { //only add the tag if it's not already in the list
                    curTagList.push_back(curTag);
                    complexList.replace(j, curTagList);
                }
                break;
            }
        }
        if (createNewCata) {
            QList<QString> newCataList;
            newCataList.push_back(curCata);
            newCataList.push_back(curTag);
            complexList.push_back(newCataList);
        }
    }

    //now turn the complex list into a sorted list

    for (int i = 0; i < complexList.length(); i++) {
        QList<QString> curTagList = complexList.at(i);
        QString curCata = curTagList.at(0);
        for (int j = 1; j < curTagList.length(); j++) {
            QString curTag = curTagList.at(j);
            GroupingTag curGTag;
            curGTag.category = curCata;
            curGTag.tag = curTag;
            retList.append(curGTag);
        }

    }

    return(retList);
}

QHash<GroupingTag, int> CategoryDictionary::getAllTags() {
    return(all);
}

QHash<QString,QString> CategoryDictionary::getCategorysTags(QString category) {
    return(categories.value(category));
}

QHash<QString, QHash<QString,QString> > CategoryDictionary::getCategories() {
    return(categories);
}

bool CategoryDictionary::addCategory(QString newCategory) {
    if (categoryExists(newCategory)) {
//        qDebug() << "category already exists";
        return(false);
    }
    if (newCategory == "All") {
        return(false);
    }

    QHash<QString,QString> emptyTagHash;
    categories.insert(newCategory, emptyTagHash);
    return(true);
}

//Add's the specified tag the to the specified category, supports multiple tags across different categories
bool CategoryDictionary::addTagToCategory(QString category, QString tag) {
    if (!categoryExists(category)) { //if the category doesn't exist
        //qDebug() << "Error: Specified category does not exist. (CategoryDictionary::addTagToCategory)";
        return(false);
    }
    QHash<QString,QString> tagHash = categories.value(category);
    if (tagHash.contains(tag)) { //don't add duplicate tags to the same category
//        qDebug() << "Tag already added.";
        return(false);
    }

    tagHash.insert(tag,tag);
    categories.insert(category,tagHash); //should replace the previous value, if not do remove(cateogry) and insert(category, hash)
    if (category != "All") { //add all tags to the 'All' category, this will automatically ignore duplicates
//        addTagToCategory("All",tag); //todo: remove the "All" category from the categories list.  It is now stored in a separate object

        GroupingTag gt;
        gt.tag = tag;
        gt.category = category;
        addTagToALL(gt);
    }
    return(true);
}

bool CategoryDictionary::addTag(GroupingTag gt) {
    if (this->gTagExists(gt)) //don't add if it already is in the dictionary
        return(false);
    this->addCategory(gt.category); //add the category, will automatically skip if the category already has been added
    this->addTagToCategory(gt.category,gt.tag); //add the tag, will automatically skip if the tag has already been added
    //this will also automatically append the new tag to the 'All' category, skipping duplicates by default
    return true;
}

bool CategoryDictionary::addTagToALL(GroupingTag gt) {
//    qDebug() << "Adding tag to all";
    if (all.contains(gt))
        return(false);
//    qDebug() << "   Done";
    all.insert(gt, 1);
    return(true);
}

void CategoryDictionary::clear() {
    categories.clear();
    all.clear();
}

void CategoryDictionary::printAll() {
    qDebug() << "Printing Category Dictionary";

    qDebug() << "-- All";
    QHashIterator<GroupingTag, int> i(all);
    while (i.hasNext()) {
        i.next();
        qDebug() << "   -- [" << i.key().category << "] " << i.key().tag;
    }

    QHashIterator<QString, QHash<QString,QString> > iter(categories);
    while(iter.hasNext()) {
        iter.next();
        qDebug() << "-- " << iter.key();
        QHashIterator<QString,QString> subIter(iter.value());
        while(subIter.hasNext()) {
            subIter.next();
            qDebug() << "   -- " << subIter.key();
        }
    }
}


SpikeConfiguration::~SpikeConfiguration()
{
    /*while (!ntrodes.isEmpty()) {
        ntrodes.takeLast();
    }*/
}

#ifdef TRODES_CODE
void SpikeConfiguration::setModuleDataSwitch(int nTrode, bool on, bool emitchange)
{
    ntrodes[nTrode]->moduleDataOn = on;
    if(emitchange) emit updatedModuleData();
}

void SpikeConfiguration::setRefSwitch(int nTrode, bool on, bool emitchange)
{

    ntrodes[nTrode]->refOn = on;
    if(emitchange) emit updatedRef();
}

void SpikeConfiguration::setNotchFilterSwitch(int nTrode, bool on, bool emitchange) {
    ntrodes[nTrode]->notchFilterOn = on;
    //qDebug() << "setNotchFilterSwitch" << ntrodes[nTrode]->notchFilterOn;
    if(emitchange) emit updatedNotchFilter();
}

void SpikeConfiguration::setLFPRefSwitch(int nTrode, bool on, bool emitchange){
    ntrodes[nTrode]->lfpRefOn = on;
    if(emitchange) emit updatedRef();
}

void SpikeConfiguration::setLFPFilterSwitch(int nTrode, bool on, bool emitchange) {
    ntrodes[nTrode]->lfpFilterOn = on;
    if(emitchange) emit updatedLFPFilter();
}

void SpikeConfiguration::setFilterSwitch(int nTrode, bool on, bool emitchange)
{
    ntrodes[nTrode]->filterOn = on;
    if(emitchange) emit updatedFilterOn();
}

void SpikeConfiguration::setModuleDataChan(int nTrode, int newChan, bool emitchange)
{
    ntrodes[nTrode]->lfpDataChan = newChan;
    if(emitchange) emit updatedModuleData();
}

void SpikeConfiguration::setMaxDisp(int nTrode, int newMaxDisp, bool emitchange)
{
    if (linkChangesBool) {
        emit changeAllMaxDisp(newMaxDisp);
    }
    else if (ntrodes[nTrode]->maxDisp[0] != newMaxDisp ) {

        for (int i=0; i < ntrodes[nTrode]->maxDisp.length(); i++) {
            ntrodes[nTrode]->maxDisp[i] = newMaxDisp;
        }

        emit newMaxDisplay(nTrode, newMaxDisp);
        if(emitchange) emit updatedMaxDisplay();
    }
}

void SpikeConfiguration::setThresh(int nTrode, int newThresh, bool emitchange)
{
    if (linkChangesBool) {
        emit changeAllThresh(newThresh);
    }
    else if (ntrodes[nTrode]->thresh[0] != newThresh) {
        for (int i=0; i < ntrodes[nTrode]->thresh.length(); i++) {
            ntrodes[nTrode]->thresh[i] = newThresh;
            //ntrodes[nTrode]->thresh_rangeconvert[i] = (newThresh * 65536) / AD_CONVERSION_FACTOR;
            ntrodes[nTrode]->thresh_rangeconvert[i] = (newThresh * (1/ntrodes[nTrode]->spike_scaling_to_uV));

        }

        emit newThreshold(nTrode, newThresh);
        //emit newThreshold(nTrode, chan, newThresh);
        if(emitchange) emit updatedThresh();
    }
}


void SpikeConfiguration::setMaxDisp(int nTrode, int chan, int newMaxDisp, bool emitchange)
{
    if (linkChangesBool) {
        emit changeAllMaxDisp(newMaxDisp);
    }
    else {
        ntrodes[nTrode]->maxDisp[chan] = newMaxDisp;
        emit newMaxDisplay(ntrodes[nTrode]->hw_chan[chan], newMaxDisp);
        if(emitchange) emit updatedMaxDisplay();
    }
}

void SpikeConfiguration::setThresh(int nTrode, int chan, int newThresh, bool emitchange)
{
    if (linkChangesBool) {
        emit changeAllThresh(newThresh);
    }
    else {
        ntrodes[nTrode]->thresh[chan] = newThresh;
        //ntrodes[nTrode]->thresh_rangeconvert[chan] = (newThresh * 65536) / AD_CONVERSION_FACTOR;
        ntrodes[nTrode]->thresh_rangeconvert[chan] = (newThresh * (1/ntrodes[nTrode]->spike_scaling_to_uV));
        emit newThreshold(ntrodes[nTrode]->hw_chan[chan], newThresh);
        emit newThreshold(nTrode, chan, newThresh);
        if(emitchange) emit updatedThresh();
    }
}

void SpikeConfiguration::setTriggerMode(int nTrode, bool triggerOn)
{
    for (int i = 0; i < ntrodes[nTrode]->triggerOn.length(); i++) {
        setTriggerMode(nTrode, i, triggerOn);
    }

}

void SpikeConfiguration::setTriggerMode(int nTrode, int chan, bool triggerOn)
{
    ntrodes[nTrode]->triggerOn[chan] = triggerOn;
    emit newTriggerMode(ntrodes[nTrode]->hw_chan[chan], triggerOn);
    emit newTriggerMode(nTrode, chan, triggerOn);
    emit updatedTriggerMode();
}

void SpikeConfiguration::setReference(int nTrode, int newRefNTrode, int newRefNTrodeChan, bool emitchange)
{

    ntrodes[nTrode]->refNTrode = newRefNTrode;
    ntrodes[nTrode]->refNTrodeID = ntrodes[newRefNTrode]->nTrodeId;
    ntrodes[nTrode]->refChan = newRefNTrodeChan;

    if(emitchange) emit updatedRef();
}

void SpikeConfiguration::setColor(int nTrode, QColor newColor, bool emitchange)
{
    ntrodes[nTrode]->color = newColor;
    if(emitchange) emit updatedTraceColor();
}


void SpikeConfiguration::setLowFilter(int nTrode, int cutoff, bool emitchange)
{

//    int hw_chan;
    ntrodes[nTrode]->lowFilter = cutoff;
//    for (int i = 0; i < ntrodes[nTrode]->maxDisp.length(); i++) {
//        hw_chan = ntrodes[nTrode]->hw_chan[i];
//        //streamConf->dataFilters[ntrodes[nTrode]->streamingChannelLookup[i]].setFilterRange(ntrodes[nTrode]->lowFilter, ntrodes[nTrode]->highFilter);
//        streamConf->spikeFilters[hw_chan]->setFilterRange(ntrodes[nTrode]->lowFilter, ntrodes[nTrode]->highFilter);

//    }

    if(emitchange) emit updatedFilter({nTrode});
}

void SpikeConfiguration::setHighFilter(int nTrode, int cutoff, bool emitchange)
{
//    int hw_chan;
    ntrodes[nTrode]->highFilter = cutoff;
//    for (int i = 0; i < ntrodes[nTrode]->maxDisp.length(); i++) {
//        hw_chan = ntrodes[nTrode]->hw_chan[i];
//        //streamConf->dataFilters[ntrodes[nTrode]->streamingChannelLookup[i]].setFilterRange(ntrodes[nTrode]->lowFilter, ntrodes[nTrode]->highFilter);
//        streamConf->spikeFilters[hw_chan]->setFilterRange(ntrodes[nTrode]->lowFilter, ntrodes[nTrode]->highFilter);

//    }
    if(emitchange) emit updatedFilter({nTrode});
}

void SpikeConfiguration::setModuleDataHighFilter(int nTrode, int cutoff, bool emitchange)
{
    ntrodes[nTrode]->lfpHighFilter = cutoff;
    //streamConf->lfpFilters[nTrode]->setFilterRange(0, cutoff);
    if(emitchange) emit updatedModuleDataFilter({nTrode});
}

void SpikeConfiguration::setNotchFilterFreq(int nTrode, int notchFreq, bool emitchange)
{

    int hw_chan;
    ntrodes[nTrode]->notchFreq = notchFreq;
//    for (int i = 0; i < ntrodes[nTrode]->maxDisp.length(); i++) {
//        hw_chan = ntrodes[nTrode]->hw_chan[i];
//        streamConf->notchFilters[hw_chan]->setNotchFreq(ntrodes[nTrode]->notchFreq);
//    }

    if(emitchange) emit updatedNotchFilter();
}

void SpikeConfiguration::setNotchFilterBW(int nTrode, int notchBW, bool emitchange)
{

    int hw_chan;
    ntrodes[nTrode]->notchBW = notchBW;
//    for (int i = 0; i < ntrodes[nTrode]->maxDisp.length(); i++) {
//        hw_chan = ntrodes[nTrode]->hw_chan[i];
//        streamConf->notchFilters[hw_chan]->setBandwidth(ntrodes[nTrode]->notchBW);
//    }

    if(emitchange) emit updatedNotchFilter();
}

void SpikeConfiguration::setGroupRef(int nTrode, bool toggle, int group, bool emitchange){
    if(!toggle && ntrodes[nTrode]->refGroup){
        //if turning off groupref, first decrement usecount of old group
        --carGroups[ntrodes[nTrode]->refGroup-1].useCount;
    }
    else{
        if(group && !ntrodes[nTrode]->groupRefOn){
            //if previously off
            ++carGroups[group-1].useCount;
        }
        else{
            //group reference staying on, moving from one group to another
            if(group) ++carGroups[group-1].useCount;
            if(ntrodes[nTrode]->refGroup) --carGroups[ntrodes[nTrode]->refGroup-1].useCount;
        }
    }
//    for(int i = 0; i < carGroups.length(); ++i){
//        qDebug() << i+1 << "used: " << carGroups[i].useCount;
//    }
    ntrodes[nTrode]->groupRefOn = toggle;
    ntrodes[nTrode]->refGroup = group;
    if(emitchange) emit updatedRef();
}

void SpikeConfiguration::setRawRefOn(int nTrode, bool on, bool emitchange){
    ntrodes[nTrode]->rawRefOn = on;
    if(emitchange) emit updatedRef();
}

void SpikeConfiguration::setSpikeModeOn(int ntrode, bool on, bool emitchange){
    ntrodes[ntrode]->spikeViewMode = on;
    if(emitchange) emit updatedSpikeMode();
}

void SpikeConfiguration::setStimViewModeOn(int ntrode, bool on, bool emitchange){
    ntrodes[ntrode]->stimViewMode = on;
    if(emitchange) emit updatedStimViewMode();
}

void SpikeConfiguration::setLFPModeOn(int ntrode, bool on, bool emitchange){
    ntrodes[ntrode]->lfpViewMode = on;
    if(emitchange) emit updatedSpikeMode();
}

void SpikeConfiguration::setAllModesOn(bool spike, bool lfp, bool stim){
    for (int i=0; i<ntrodes.length();i++) {
        //qDebug() << "ntrode" << i << spike << lfp << stim;
        ntrodes[i]->spikeViewMode = spike;
        ntrodes[i]->lfpViewMode = lfp;
        ntrodes[i]->stimViewMode = stim;
    }

    emit updatedAllModes();
}

#endif

int SpikeConfiguration::loadFromXML(QDomNode &spikeConfNode) {
    //Overloaded method that still relies on a global value
    return loadFromXML(spikeConfNode,hardwareConf->NCHAN);
}

int SpikeConfiguration::loadFromXML(QDomNode &spikeConfNode, int totalNumChannels)
{


    QDomNode tetNode;
    QDomElement tetElement;
    QDomNode n = spikeConfNode.firstChild();
    int eidx = 0;
    int tetidx = 0;
    int totalChannelCount = 0;
    bool ok;

    int autoNtrodeType = 0;
    autoNtrodeType = spikeConfNode.toElement().attribute("autoPopulate", "0").toInt(); //used as a debug tool to automatically distribute the channels into even nTrode groups. Not useful as an end-user tool.

    deviceType = spikeConfNode.toElement().attribute("device", "intan");

    //The device tag is used to designate what type of hardware is used to collect the neural signals, and automatically populate the SpikeConfig section based on source parameters in the hardware section if there are no entries
    //options:
    //1) "intan" (the default)
    //2) "neuropixels1"

    int autoConfigMode;
    autoConfigMode = spikeConfNode.toElement().attribute("auto", "0").toInt(&ok);

    if (ok && (autoConfigMode > 0)) {
        if (deviceType == "neuropixels1") {
            qDebug() << "Workspace requesting automatic spike (ntrode) configuration using neuropixels1.0 probes.";
            return (int)ReturnCode::autoConfigNeeded_neuropixel1; //request auto population using neuropixels settings from Hardware section
        } else {
            autoNtrodeType = 1;  //Unknown device type.  Just create single-channel ntrodes with hardware channels in order
        }
    }



    //If all chips in the headstage have the same number of channels, we can use a single number to convert the HW channel numbers. Default is 32.
    numChanPerChip = spikeConfNode.toElement().attribute("chanPerChip", "32").toInt(&ok);
    if (!ok) {
        qDebug() << "[ParseTrodesConfig] Error: The chanPerChip could not be converted to a number.";
        return -1;
    }



    QString cata = spikeConfNode.toElement().attribute("categories", "");
    if (!cata.isEmpty()) {
        QStringList categoryList = cata.split(";");
        for (int i = 0; i < categoryList.length(); i++) {
            QString categoryName = categoryList.at(i).split("=").first();
            groupingDict.addCategory(categoryName);

            QString tags = categoryList.at(i).split("=").last();
            tags = tags.left(tags.length()-1);
            tags = tags.right(tags.length()-1);
            QStringList tagList = tags.split(",");
            for (int j = 0; j < tagList.length(); j++) {
                groupingDict.addTagToCategory(categoryName,tagList.at(j));
            }
        }
    }
    else {
        //default ALL category intialization here
    }

    //Does the config designate that channels should be distrubuted automatically?
    if (autoNtrodeType > 0) {
        qDebug() << "Auto configuration with ntrodes of " << autoNtrodeType;
        int currentHardwareChan = 0;
        QVector<int> autoHWChanList;

        QString autoChannelOrder= spikeConfNode.toElement().attribute("channelOrder", "");
        if (!autoChannelOrder.isEmpty()) {

            //The user can give a channel list in the form of a string,
            //where the numbers are separated by spaces
            QStringList slist;
            //slist = autoChannelOrder.split(" ");
            slist = autoChannelOrder.split(QRegularExpression("\\s+"));
            bool allOk(true);
            bool ok;
            for (int x = 0; x < slist.count(); x++) {

                if (!slist.at(x).isEmpty()) {
                    qDebug() << slist.at(x);

                    autoHWChanList.append(slist.at(x).toInt(&ok));
                    allOk &= ok;
                }
            }
            if (!allOk) {
                qDebug() << "[ParseTrodesConfig] Error: The Automatic HW list could not be converted to numbers.";
                return -1;
            }
            if (autoHWChanList.length() != totalNumChannels) {
                qDebug() << "[ParseTrodesConfig] Error: Automatic HW channel list is not the correct length!!";
                return -1;
            }
        } else {
            for (int i=0; i < totalNumChannels; i++) {
                autoHWChanList.push_back(i);
            }
        }

        //This is the list of colors to cycle through
        QList<QColor> colorOptions;
        colorOptions.append(QColor(250,250,250));
        colorOptions.append(QColor(250,50,50));
        colorOptions.append(QColor(20,250,20));
        colorOptions.append(QColor(20,20,250));
        int currentColor = 0;
        int ntrNum = -1;



        for (int autoNTrodeChan=0;autoNTrodeChan<autoHWChanList.length();autoNTrodeChan++) {
            qDebug() << autoNTrodeChan;
            currentHardwareChan = autoHWChanList.at(autoNTrodeChan);
            if ((autoNTrodeChan % autoNtrodeType)==0) {
                ntrNum++;
                currentColor = (currentColor+1)%colorOptions.length();

                //ntrodes.append(ntrNum+1,new SingleSpikeTrodeConf);
                //ntrodes.append(ntrNum+1, new SingleSpikeTrodeConf);
                ntrodes.append(ntrNum+1);
                //default view mode
                ntrodes[ntrNum]->spikeViewMode = true;
                ntrodes[ntrNum]->lfpViewMode = false;
                ntrodes[ntrNum]->stimViewMode = false;
                ntrodes[ntrNum]->nTrodeId = ntrNum+1;
                ntrodes[ntrNum]->spike_scaling_to_uV = 0.195;
                ntrodes[ntrNum]->raw_scaling_to_uV = 0.195;
                ntrodes[ntrNum]->lfp_scaling_to_uV = 0.195;
                ntrodes[ntrNum]->lowFilter = 600;
                ntrodes[ntrNum]->highFilter = 6000;
                ntrodes[ntrNum]->color = colorOptions[currentColor];
                ntrodes[ntrNum]->refNTrode = 0;
                ntrodes[ntrNum]->refNTrodeID = 1;
                ntrodes[ntrNum]->refOn = false;
                ntrodes[ntrNum]->refChan = 0;
                ntrodes[ntrNum]->lfpDataChan = 0;
                ntrodes[ntrNum]->lfpHighFilter = 200;
                ntrodes[ntrNum]->lfpLowFilter = 0;
                ntrodes[ntrNum]->filterOn = true;
                ntrodes[ntrNum]->moduleDataOn = false;
                ntrodes[ntrNum]->lfpRefOn = false;
                ntrodes[ntrNum]->lfpFilterOn = true;
                ntrodes[ntrNum]->rawRefOn = false;
                ntrodes[ntrNum]->groupRefOn = false;
                ntrodes[ntrNum]->refGroup = 0;
                ntrodes[ntrNum]->notchFreq = 60.0;
                ntrodes[ntrNum]->notchBW = 10.0;
            }



            ntrodes[ntrNum]->unconverted_hw_chan.push_back(currentHardwareChan);
            ntrodes[ntrNum]->hw_chan.append(convertHWchan(currentHardwareChan,totalNumChannels));
            ntrodes[ntrNum]->stimCapable.append(false);
            ntrodes[ntrNum]->maxDisp.append(400);
            ntrodes[ntrNum]->thresh.append(60);
            ntrodes[ntrNum]->thresh_rangeconvert.push_back(60 * (1/ntrodes[ntrNum]->spike_scaling_to_uV));
            ntrodes[ntrNum]->streamingChannelLookup.append(400);
            ntrodes[ntrNum]->triggerOn.append(true);
            ntrodes[ntrNum]->coord_ap.append(0);
            ntrodes[ntrNum]->coord_ml.append(0);
            ntrodes[ntrNum]->coord_dv.append(0);
            ntrodes[ntrNum]->spikeSortingGroup.append(0);




        }


    } else {

        //No automatic channel distribution
        int nTrodeCount = 0;

        while (!n.isNull()) {

            QDomElement nt = n.toElement();
            if (!nt.isNull()) {
                int ntidx = nt.attribute("id", "").toInt(&ok);
                //ntrodes.append(ntidx, new SingleSpikeTrodeConf);
                ntrodes.append(ntidx);

                if (!ok) {
                    qDebug() << "[ParseTrodesConfig] Error converting id to number in Spike Configuration. Read value is" << nt.attribute("id", "");
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting id to number in Spike Configuration. Read value is '") + nt.attribute("id", "") + QString("'."));
                    return -2;
                }
                nTrodeCount++;

                //default view mode. TODO: read and save these values from/to file
                ntrodes[eidx]->spikeViewMode = nt.attribute("viewSpikeBand", "1").toInt(&ok);
                ntrodes[eidx]->lfpViewMode = nt.attribute("viewLFPBand", "0").toInt(&ok);
                ntrodes[eidx]->stimViewMode = nt.attribute("viewStimBand", "0").toInt(&ok);

                //Only one should be on, if not the case, go to default mode.
                int viewSum = ((int)ntrodes[eidx]->spikeViewMode + (int)ntrodes[eidx]->lfpViewMode + (int)ntrodes[eidx]->stimViewMode);
                if (viewSum != 1) {
                    ntrodes[eidx]->spikeViewMode = true;
                    ntrodes[eidx]->lfpViewMode = false;
                    ntrodes[eidx]->stimViewMode = false;
                }

                ntrodes[eidx]->color.setNamedColor(nt.attribute("color", "#808080")); //default color is gray (RGB in hex)
                ntrodes[eidx]->lowFilter = nt.attribute("lowFilter", "300").toInt(&ok);
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting lowFilter to number in Spike Configuration. Read value is '") + nt.attribute("lowFilter", "") + QString("'."));
                    return -2;
                }
                ntrodes[eidx]->highFilter = nt.attribute("highFilter", "6000").toInt(&ok);
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting highFilter to number in Spike Configuration. Read value is '") + nt.attribute("highFilter", "") + QString("'."));
                    return -2;
                } else
                ntrodes[eidx]->refNTrode = nt.attribute("refNTrode", "1").toInt(&ok) - 1;
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting refNTrode to number in Spike Configuration. Read value is '") + nt.attribute("refNTrode", "") + QString("'."));
                    return -2;
                }
                ntrodes[eidx]->refNTrodeID = nt.attribute("refNTrodeID", "-1").toInt(&ok);
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting refNTrode to number in Spike Configuration. Read value is '") + nt.attribute("refNTrode", "") + QString("'."));
                    return -2;
                }
                ntrodes[eidx]->refChan = nt.attribute("refChan", "1").toInt(&ok) - 1;
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting refChan to number in Spike Configuration. Read value is '") + nt.attribute("refChan", "") + QString("'."));
                    return -2;
                }


                ntrodes[eidx]->groupRefOn = nt.attribute("groupRefOn", "0").toInt(&ok);
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting refChan to number in Spike Configuration. Read value is '") + nt.attribute("refChan", "") + QString("'."));
                    return -2;
                }
                ntrodes[eidx]->refGroup = nt.attribute("refGroup", "0").toInt(&ok);
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting refChan to number in Spike Configuration. Read value is '") + nt.attribute("refChan", "") + QString("'."));
                    return -2;
                }
                if(ntrodes[eidx]->groupRefOn && ntrodes[eidx]->refGroup){
                    //If ntrode is using a group as a reference, add one to used count
                    if(ntrodes[eidx]->refGroup > carGroups.length()){
                        //not valid ref group, disable group ref
                        ntrodes[eidx]->groupRefOn = false;
                    }
                    else{
                        carGroups[ntrodes[eidx]->refGroup-1].useCount++;
                    }
                }


                //Users can either use LFPChan/LFPHighFilter filenames or moduleDataChan/moduleDataHighFilter
                ntrodes[eidx]->lfpDataChan = nt.attribute("moduleDataChan", "0").toInt(&ok) - 1;
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting moduleDataChan to number in Spike Configuration. Read value is '") + nt.attribute("moduleDataChan", "") + QString("'."));
                    return -2;
                }
                ntrodes[eidx]->lfpHighFilter = nt.attribute("moduleDataHighFilter", "-1").toInt(&ok);
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting moduleDataHighFilter to number in Spike Configuration. Read value is '") + nt.attribute("moduleDataHighFilter", "") + QString("'."));
                    return -2;
                }

                if (ntrodes[eidx]->lfpDataChan == -1) {
                    ntrodes[eidx]->lfpDataChan = nt.attribute("LFPChan", "1").toInt(&ok) - 1;
                    if (!ok) {
                        //QMessageBox::information(0, "error", QString("Config file error: Error in converting moduleDataChan to number in Spike Configuration. Read value is '") + nt.attribute("moduleDataChan", "") + QString("'."));
                        return -2;
                    }
                }

                if (ntrodes[eidx]->lfpHighFilter == -1) {
                    ntrodes[eidx]->lfpHighFilter = nt.attribute("LFPHighFilter", "200").toInt(&ok);
                    if (!ok) {
                        //QMessageBox::information(0, "error", QString("Config file error: Error in converting moduleDataHighFilter to number in Spike Configuration. Read value is '") + nt.attribute("moduleDataHighFilter", "") + QString("'."));
                        return -2;
                    }
                    //qDebug() <<"[ParseTrodesConfig] Setting LFPHighFilter to" << ntrodes[eidx]->moduleDataHighFilter;
                }

                ntrodes[eidx]->lfpLowFilter = nt.attribute("LFPLowFilter", "0").toInt(&ok);
                if (!ok) {
                    return -2;
                }
                if (ntrodes[eidx]->lfpLowFilter < 0) {
                    ntrodes[eidx]->lfpLowFilter = 0;
                }

                ntrodes[eidx]->refOn = nt.attribute("refOn", "1").toInt(&ok);
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting refOn to number in Spike Configuration. Read value is '") + nt.attribute("refOn", "") + QString("'."));
                    return -2;
                }
                ntrodes[eidx]->filterOn = nt.attribute("filterOn", "1").toInt(&ok);
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting filterOn to number in Spike Configuration. Read value is '") + nt.attribute("filterOn", "") + QString("'."));
                    return -2;
                }
                ntrodes[eidx]->moduleDataOn = nt.attribute("moduleDataOn", "1").toInt(&ok);
                if (!ok) {
                    //QMessageBox::information(0, "error", QString("Config file error: Error in converting moduleDataOn to number in Spike Configuration. Read value is '") + nt.attribute("moduleDataOn", "") + QString("'."));
                    return -2;
                }
                ntrodes[eidx]->lfpRefOn = nt.attribute("lfpRefOn", "0").toInt(&ok);
                if (!ok) {
                    return -2;
                }
                ntrodes[eidx]->lfpFilterOn = nt.attribute("lfpFilterOn", "1").toInt(&ok);
                if (!ok) {
                    return -2;
                }
                ntrodes[eidx]->rawRefOn = nt.attribute("rawRefOn", "0").toInt(&ok);
                if (!ok) {
                    return -2;
                }
                ntrodes[eidx]->notchFreq = nt.attribute("notchFreq", "60").toInt(&ok);
                if (!ok) {
                    return -2;
                }
                ntrodes[eidx]->notchBW = nt.attribute("notchBW", "10").toInt(&ok);
                if (!ok) {
                    return -2;
                }
                ntrodes[eidx]->notchFilterOn = nt.attribute("notchFilterOn", "0").toInt(&ok);
                if (!ok) {
                    return -2;
                }
                ntrodes[eidx]->spike_scaling_to_uV = nt.attribute("spikeScalingToUv", ".195").toDouble(&ok);
                if (!ok) {
                    return -2;
                }
                ntrodes[eidx]->raw_scaling_to_uV = nt.attribute("rawScalingToUv", ".195").toDouble(&ok);
                if (!ok) {
                    return -2;
                }
                ntrodes[eidx]->lfp_scaling_to_uV = nt.attribute("lfpScalingToUv", ".195").toDouble(&ok);
                if (!ok) {
                    return -2;
                }
                //mark: grouping tags
                QString rawGroupingTagStr = nt.attribute("groupingTags", "");
                if (rawGroupingTagStr != "") {
//                    qDebug() << "*** Reading grouping tags: ";
                    QStringList gTagL = rawGroupingTagStr.split(";");
                    for (int i = 0; i < gTagL.length(); i++) {
                        QString curCata = gTagL.at(i).split("/").first();
                        QString curTag = gTagL.at(i).split("/").last();
                        if (curCata.isEmpty() || curTag.isEmpty())
                            continue;

//                        qDebug() << "   [" << curCata << "] " << curTag;
                        GroupingTag curGTag;
                        curGTag.category = curCata;
                        curGTag.tag = curTag;

                        ntrodes[eidx]->gTags.insert(curGTag, 1);
                        groupingDict.addTag(curGTag); //adds curGTag to the dictionary if it isn't already in it

                    }
//                    groupingDict.printAll();
                }
                else {
                    //LEGACY, depreciated now, only activate if rawGroupingTagStr was empty.  This ensures backwards compatability
//                    qDebug() << "LEGACY reading 1";
                    QString rawTagStr = nt.attribute("tags","");
                    if (rawTagStr != "") {
                        QStringList tagStrL = rawTagStr.split(";"); //tags are split by semi colon
                        for (int i = 0; i < tagStrL.length(); i++) {
                            //mark: to do - make the VALUE of the tag HASH be the CATEGORY it belongs to
                            if (groupingDict.tagExists(tagStrL.at(i))) {
                                //tag exists, don't do anything
                            }
                            else {
                                //tag doesn't exist, add to All
                                qDebug() << "IM not sure this is ever called, if it is, revist and rework";
//                                groupingDict.addTagToCategory("All",tagStrL.at(i));

                            }

                            GroupingTag newTag;
                            newTag.tag = tagStrL.at(i);
                            newTag.category = groupingDict.getTagsCategory(newTag.tag);
                            ntrodes[eidx]->gTags.insert(newTag, 1);
//                            ntrodes[eidx]->tags.insert(tagStrL.at(i),1);
                        }
                    }
                }
//                groupingDict.printAll();

//                nTrodeTable->ntrodes.append(new NTrode);
                ntrodes[eidx]->nTrodeId = ntidx;
                tetNode = n.firstChild();
                tetidx = 0;

                //Read in info about each channel in the nTrode
                while (!tetNode.isNull()) {
                    tetElement = tetNode.toElement();
                    if (!tetElement.isNull()) {
                        ntrodes[eidx]->maxDisp.push_back(tetElement.attribute("maxDisp", "400").toInt(&ok));
                        if (!ok) {
                            //QMessageBox::information(0, "error", QString("Config file error: Error in converting maxDisp to number in SpikeChannel define. Read value is '") + tetElement.attribute("maxDisp", "") + QString("'."));
                            return -2;
                        }
                        if (ntrodes[eidx]->maxDisp.last() < 25) {
                            //QMessageBox::information(0, "error", QString("Config file error: All max display values (maxDisp) must be 25 or more. Error in SpikeNTrode %1.").arg(ntidx));
                            return -2;
                        }
                        ntrodes[eidx]->thresh.push_back(tetElement.attribute("thresh", "60").toInt(&ok));
                        if (!ok) {
                            //QMessageBox::information(0, "error", QString("Config file error: Error in converting thresh to number in SpikeChannel define. Read value is '") + tetElement.attribute("thresh", "") + QString("'."));
                            return -2;
                        }
                        if (ntrodes[eidx]->thresh.last() < 10) {
                            //QMessageBox::information(0, "error", QString("Config file error: All threshold values (thresh) must be 10 or more. Error in SpikeNTrode %1.").arg(ntidx));
                            return -2;
                        }
                        ntrodes[eidx]->triggerOn.push_back(tetElement.attribute("triggerOn", "1").toInt(&ok));
                        if (!ok) {
                            //QMessageBox::information(0, "error", QString("Config file error: Error in converting triggerOn to number in SpikeChannel define. Read value is '") + tetElement.attribute("thresh", "") + QString("'."));
                            return -2;
                        }
                        ntrodes[eidx]->coord_ap.push_back(tetElement.attribute("coord_ap", "-1").toInt(&ok));
                        if (!ok) {
                            //QMessageBox::information(0, "error", QString("Config file error: Error in converting triggerOn to number in SpikeChannel define. Read value is '") + tetElement.attribute("thresh", "") + QString("'."));
                            return -2;
                        }
                        ntrodes[eidx]->coord_ml.push_back(tetElement.attribute("coord_ml", "-1").toInt(&ok));
                        if (!ok) {
                            //QMessageBox::information(0, "error", QString("Config file error: Error in converting triggerOn to number in SpikeChannel define. Read value is '") + tetElement.attribute("thresh", "") + QString("'."));
                            return -2;
                        }
                        ntrodes[eidx]->coord_dv.push_back(tetElement.attribute("coord_dv", "-1").toInt(&ok));
                        if (!ok) {
                            //QMessageBox::information(0, "error", QString("Config file error: Error in converting triggerOn to number in SpikeChannel define. Read value is '") + tetElement.attribute("thresh", "") + QString("'."));
                            return -2;
                        }
                        ntrodes[eidx]->spikeSortingGroup.push_back(tetElement.attribute("spikeSortingGroup", "0").toInt(&ok));
                        if (!ok) {
                            //QMessageBox::information(0, "error", QString("Config file error: Error in converting triggerOn to number in SpikeChannel define. Read value is '") + tetElement.attribute("thresh", "") + QString("'."));
                            return -2;
                        }

                        //ntrodes[eidx]->hw_chan.push_back(tetElement.attribute("hwChan", "0").toInt(&ok));
                        //ntrodes[eidx]->thresh_rangeconvert.push_back((ntrodes[eidx]->thresh[tetidx] * 65536) / AD_CONVERSION_FACTOR);
                        ntrodes[eidx]->thresh_rangeconvert.push_back((ntrodes[eidx]->thresh[tetidx]) * (1/ntrodes[eidx]->spike_scaling_to_uV));




                        int tempHWChanStorage = tetElement.attribute("hwChan", "0").toInt(&ok);
                        if (!ok) {
                            //QMessageBox::information(0, "error", QString("Config file error: Error in converting hwChan to number in SpikeChannel define. Read value is '") + tetElement.attribute("hwChan", "") + QString("'."));
                            return -2;
                        }

                        ntrodes[eidx]->unconverted_hw_chan.push_back(tempHWChanStorage);

                        if (deviceType == "neuropixels1") {
                            ntrodes[eidx]->hw_chan.append(tempHWChanStorage);
                        } else {
                            //Assume Intan-based device

                            if ((ntrodes[eidx]->unconverted_hw_chan.last() > totalNumChannels) || (ntrodes[eidx]->unconverted_hw_chan.last() < 0)) {
                                qDebug() << "[ParseTrodesConfig] Error: All hardware channels (hwChan) must be between 0 and numChannels. Error in NTrode " << tetidx + 1 << "."
                                         << "\n   Value" << ntrodes[eidx]->unconverted_hw_chan.last();
                                //QMessageBox::information(0, "error", QString("Config file error: All hardware channels (hwChan) must be between 0 and numChannels. Error in SpikeNTrode %1.").arg(ntidx));
                                return -2;
                            }
                            ntrodes[eidx]->hw_chan.append(convertHWchan(ntrodes[eidx]->unconverted_hw_chan.last(),totalNumChannels));  //Need to convert to interleaved form
                        }

                        bool tmpStimCapable = tetElement.attribute("stimCapable", "0").toInt(&ok);
                        if (!ok) {
                            qDebug() << "stimCapable value not a number.";
                            return -2;
                        }

                        ntrodes[eidx]->stimCapable.push_back(tmpStimCapable);
                        if (tmpStimCapable) {
                             //qDebug() << "Stim capable hardware channel:" << ntrodes[eidx]->hw_chan.last();
                        }

                        //ntrodes[eidx]->stimBitLookup.push_back(-1); //For stim capable channels, we modify these values below

                        ntrodes[eidx]->streamingChannelLookup.push_back(totalChannelCount);
                        tetidx++;
                        totalChannelCount++;
                    }
                    tetNode = tetNode.nextSibling();
                }

                eidx++;
            }
            n = n.nextSibling();
        }
//        groupingDict.printAll();
        //convert the refNTrode field to RefNTrodeIDs      
        convertNTrodeRefToIDs(); //this function only assigns refNTrodeIDs that don't already exist
        //populate the refNTrode field       
        convertNTrodeIDsToRef();


        if (nTrodeCount != ntrodes.length()) {
            qDebug() << "[ParseTrodesConfig] Error: there is at least one extra nTrode node with bad formating";
            return -2;
        }

        //Now we check to make sure that no stimulation capable channels are used as references.

        for (int nt = 0; nt<ntrodes.length();nt++) {
            int refTrodeIndex = ntrodes[nt]->refNTrode;
            int refTrodeChannelIndex = ntrodes[nt]->refChan;

            //qDebug() << refTrodeIndex << refTrodeChannelIndex << ntrodes[refTrodeIndex]->stimCapable[refTrodeChannelIndex];

            if ((ntrodes.length() > refTrodeIndex) &&
                (ntrodes[refTrodeIndex]->stimCapable.length() > refTrodeChannelIndex) &&
                (ntrodes[refTrodeIndex]->stimCapable[refTrodeChannelIndex] == true) &&
                ((ntrodes[nt]->refOn) || (ntrodes[nt]->lfpRefOn) || (ntrodes[nt]->rawRefOn))    ){

                qDebug() << "[ParseTrodesConfig] ALERT: a stimulation capable channel was used as reference for nTrode with index" << nt << ". This reference has been turned off.";
                ntrodes[nt]->refOn = false;
                ntrodes[nt]->lfpRefOn = false;
                ntrodes[nt]->rawRefOn = false;
            }
        }

    }
    return 0;
}

void SpikeConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode)
{
    //Adding CAR groups

    for(int i = 0; i < carGroups.length(); ++i){
        QDomElement CARElement = doc.createElement("CARGroup");
        CARElement.setAttribute("description", carGroups[i].description);
        CARElement.setAttribute("id", i+1);
        rootNode.appendChild(CARElement);
        for(int j = 0; j < carGroups[i].chans.length(); ++j){
            QDomElement CARChan = doc.createElement("CARChan");
            CARChan.setAttribute("ntrodeid", carGroups[i].chans[j].ntrodeid);
            CARChan.setAttribute("chan", carGroups[i].chans[j].chan);
            CARElement.appendChild(CARChan);
        }
    }

    QDomElement spikeconf = doc.createElement("SpikeConfiguration");
    //If all chips in the headstage have the same number of channels, we can use a single number to convert the HW channel numbers. Default is 32.
    spikeconf.setAttribute("chanPerChip", numChanPerChip);

    rootNode.appendChild(spikeconf);

    for (int i = 0; i < ntrodes.length(); i++) {
        QDomElement nt = doc.createElement("SpikeNTrode");

        nt.setAttribute("moduleDataOn", ntrodes[i]->moduleDataOn);
        nt.setAttribute("filterOn", ntrodes[i]->filterOn);
        nt.setAttribute("refOn", ntrodes[i]->refOn);
        nt.setAttribute("LFPHighFilter", ntrodes[i]->lfpHighFilter);
        //nt.setAttribute("LFPLowFilter", ntrodes[i]->lfpLowFilter);
        nt.setAttribute("LFPChan", ntrodes[i]->lfpDataChan + 1);
        nt.setAttribute("refChan", ntrodes[i]->refChan + 1);
//        nt.setAttribute("refChanID", ntrodes[i]->refChanID);
//        nt.setAttribute("refNTrode", ntrodes[i]->refNTrode + 1);
#ifdef PHOTOMETRY_CODE
        nt.setAttribute("refNTrode", ntrodes[i]->refNTrode + 1);
#endif
        nt.setAttribute("refNTrodeID", ntrodes[i]->refNTrodeID);
        nt.setAttribute("spikeScalingToUv", ntrodes[i]->spike_scaling_to_uV);
        nt.setAttribute("rawScalingToUv", ntrodes[i]->raw_scaling_to_uV);
        nt.setAttribute("lfpScalingToUv", ntrodes[i]->lfp_scaling_to_uV);
        nt.setAttribute("highFilter", ntrodes[i]->highFilter);
        nt.setAttribute("lowFilter", ntrodes[i]->lowFilter);
        nt.setAttribute("color", ntrodes[i]->color.name());
        nt.setAttribute("id", ntrodes[i]->nTrodeId);
        nt.setAttribute("lfpRefOn", ntrodes[i]->lfpRefOn);
        nt.setAttribute("rawRefOn", ntrodes[i]->rawRefOn);
        nt.setAttribute("lfpFilterOn", ntrodes[i]->lfpFilterOn);
        nt.setAttribute("groupRefOn", ntrodes[i]->groupRefOn);
        nt.setAttribute("refGroup", ntrodes[i]->refGroup);
        nt.setAttribute("notchFreq", ntrodes[i]->notchFreq);
        nt.setAttribute("notchBW", ntrodes[i]->notchBW);
        nt.setAttribute("notchFilterOn", ntrodes[i]->notchFilterOn);

        nt.setAttribute("viewSpikeBand", ntrodes[i]->spikeViewMode);
        nt.setAttribute("viewLFPBand", ntrodes[i]->lfpViewMode);
        nt.setAttribute("viewStimBand", ntrodes[i]->stimViewMode);
        //mark: grouping tags


        if (!ntrodes[i]->gTags.empty()) {
//            qDebug() << "Saving only GROUPING TAGS field";
            QHashIterator<GroupingTag, int> gIter(ntrodes[i]->gTags);
            QString groupingTagStr = "";
            while (gIter.hasNext()) {
                gIter.next();
                GroupingTag curGTag = gIter.key();
                groupingTagStr = QString("%1;%2/%3").arg(groupingTagStr).arg(curGTag.category).arg(curGTag.tag);
            }
            if (!groupingTagStr.isEmpty()) {
//                groupingTagStr = groupingTagStr.left(groupingTagStr.length()-1);
                nt.setAttribute("groupingTags",groupingTagStr);
            }
        }
        else { //DEPRCIATED, old tag saving code
            QHashIterator<QString, int> iter(ntrodes[i]->tags);
            QString tagStr = ""; //the old 'tag' string
            QString groupingTagStr = ""; //the new 'groupingTags' string, this includes tag/categor
            while(iter.hasNext()) {
                iter.next();
                QString nextCata;
                nextCata = groupingDict.getTagsCategory(iter.key());

                if (!groupingDict.tagExists(iter.key())) //add the key to the all category if it has not already been assigned to a category
                    groupingDict.addTagToCategory("All",iter.key());

                QString nextTag = QString("%1;").arg(iter.key());
                tagStr = QString("%1%2").arg(tagStr).arg(nextTag);
                groupingTagStr = QString("%1%2/%3").arg(groupingTagStr).arg(nextCata).arg(nextTag);
            }
            if (!tagStr.isEmpty()) {
                tagStr = tagStr.left(tagStr.length()-1);
                nt.setAttribute("tags",tagStr);
            }
            if (!groupingTagStr.isEmpty()) {
                groupingTagStr = groupingTagStr.left(groupingTagStr.length()-1);
                nt.setAttribute("groupingTags",groupingTagStr);
            }
        }

        for (int j = 0; j < ntrodes[i]->hw_chan.length(); j++) {
            QDomElement sCh = doc.createElement("SpikeChannel");
            //sCh.setAttribute("hwChan", ntrodes[i]->hw_chan[j]);

            if (deviceType == "neuropixels1") {
                sCh.setAttribute("hwChan", ntrodes[i]->hw_chan[j]);
            } else {
                //Assume Intan-based device
                sCh.setAttribute("hwChan", ntrodes[i]->unconverted_hw_chan[j]);
            }
            sCh.setAttribute("maxDisp", ntrodes[i]->maxDisp[j]);
            sCh.setAttribute("thresh", ntrodes[i]->thresh[j]);
            sCh.setAttribute("triggerOn", ntrodes[i]->triggerOn[j]);
            sCh.setAttribute("stimCapable", ntrodes[i]->stimCapable[j]);
            sCh.setAttribute("coord_ap", ntrodes[i]->coord_ap[j]);
            sCh.setAttribute("coord_ml", ntrodes[i]->coord_ml[j]);
            sCh.setAttribute("coord_dv", ntrodes[i]->coord_dv[j]);
            sCh.setAttribute("spikeSortingGroup", ntrodes[i]->spikeSortingGroup[j]);
            nt.appendChild(sCh);
        }
        spikeconf.appendChild(nt);
    }


    QString categoryString = "";
    QHash<QString, QHash<QString,QString> > categories = groupingDict.getCategories();
    QHashIterator<QString, QHash<QString,QString> > iter(categories);
    while(iter.hasNext()) {
        iter.next();
//        qDebug() << "-- " << iter.key();
        categoryString = QString("%1%2={").arg(categoryString).arg(iter.key());
        QHashIterator<QString,QString> subIter(iter.value());
        int tags = 0;
        while(subIter.hasNext()) {
            subIter.next();
//            qDebug() << "   -- " << subIter.key();
            QString nextTag = QString("%1,").arg(subIter.key());
            categoryString = QString("%1%2").arg(categoryString).arg(nextTag);
            tags++;
        }
        if (tags > 0) {
            categoryString = categoryString.left(categoryString.length()-1);
        }
        categoryString = QString("%1};").arg(categoryString);
    }
    if (!categoryString.isEmpty())
        categoryString = categoryString.left(categoryString.length()-1);

//    qDebug() << "SAVING " << categoryString;
    spikeconf.setAttribute("categories", categoryString);
    spikeconf.setAttribute("device", deviceType);




}



bool SpikeConfiguration::convertNTrodeRefToIDs() {
    bool retval = false;

    for (int i = 0; i < ntrodes.length(); i++) {
        if (ntrodes[i]->refNTrodeID == -1) {
            retval = true;
            ntrodes[i]->refNTrodeID = ntrodes[ntrodes[i]->refNTrode]->nTrodeId;
        }

    }
    return(retval);
}

void SpikeConfiguration::convertNTrodeIDsToRef() {
    for (int i = 0; i < ntrodes.length(); i++) {
        SingleSpikeTrodeConf *refNTrode = ntrodes.ID(ntrodes[i]->refNTrodeID);
        if (refNTrode != NULL) {
            ntrodes[i]->refNTrode = refNTrode->nTrodeIndex;
//            qDebug() << "Populating refNtrode field w/ " << refNTrode->nTrodeIndex;
        }
    }
}

int SpikeConfiguration::convertHWchan(int hw_chan, int totalchan)
{

    //When samples are collected on the hardware, the order of collection is
    //Card 0 Channel 0, Card 1 Channel 0, ..., Card N Channel 0, Card 0 Channel 1, Card 1, Channel 1, etc.
    //To minimize confusion, the config file uses 0-31 for card 0, 32-63 for card 1, etc.
    //This function converts the config file number to the actual hardware number.
    int numCards = totalchan / 32;
    int new_hw_chan = ((hw_chan % 32) * numCards) + floor(hw_chan / 32);
    return new_hw_chan;
}

#ifdef PHOTOMETRY_CODE
PhotometryConfiguration::~PhotometryConfiguration()
{
    while (!nfibers.isEmpty()) {
        delete nfibers.takeLast();
    }
}


void PhotometryConfiguration::setMaxDisp(int nTrode, int newMaxDisp)
{
    if (linkChangesBool) {
        emit changeAllMaxDisp(newMaxDisp);
    }
    else if (nfibers[nTrode]->maxDisp[0] != newMaxDisp ) {

        for (int i=0; i < nfibers[nTrode]->maxDisp.length(); i++) {
            nfibers[nTrode]->maxDisp[i] = newMaxDisp;
        }
        emit newMaxDisplay(nTrode, newMaxDisp);
        emit updatedMaxDisplay();
    }
}

void PhotometryConfiguration::setColor(int nTrode, QColor newColor)
{
    nfibers[nTrode]->color = newColor;
    emit updatedTraceColor();
}


int PhotometryConfiguration::loadFromXML(QDomNode &spikeConfNode)
{
    QDomNode tetNode;
    QDomElement tetElement;
    QDomNode n = spikeConfNode.firstChild();
    int eidx = 0;
    int tetidx = 0;
    int totalChannelCount = 0;
    bool ok;

    while (!n.isNull()) {
        QDomElement nt = n.toElement();
        if (!nt.isNull()) {

            int ntidx = nt.attribute("id", "").toInt(&ok);
            if (!ok) {
                qDebug() << "Error in converting id to number in Spike Configuration. Read value is" << nt.attribute("id", "");
                return -2;
            }

            nfibers[eidx]->nFiberId = ntidx;
            nfibers[eidx]->color.setNamedColor(nt.attribute("color", "#808080")); //default color is gray (RGB in hex)

            nfibers[eidx]->carrierFreq[0] = nt.attribute("carrierFreq1", "211").toInt(&ok);
            if (!ok) {
                return -2;
            }

            nfibers[eidx]->carrierFreq[1] = nt.attribute("carrierFreq2", "700").toInt(&ok);
            if (!ok) {
                return -2;
            }

            tetNode = n.firstChild();
            tetidx = 0;
            while (!tetNode.isNull()) {
                tetElement = tetNode.toElement();
                if (!tetElement.isNull()) {
                    nfibers[eidx]->hw_chan.push_back(tetElement.attribute("hwChan", "0").toInt(&ok));
                    if ((nfibers[eidx]->hw_chan.last() > hardwareConf->NFIBER*4) || (nfibers[eidx]->hw_chan.last() < 0)) {
                        qDebug() << "Config file error: All hardware channels (hwChan) must be between 0 and numChannels. Error in NTrode "
                                 << tetidx + 1 << ".";
                        return -2;
                    }

                    nfibers[eidx]->maxDisp.push_back(tetElement.attribute("maxDisp", "200").toInt(&ok));
                    if (!ok) {
                        return -2;
                    }
                    nfibers[eidx]->filterOn.push_back(tetElement.attribute("filterOn", "1").toInt(&ok));
                    if (!ok) {
                        return -2;
                    }
                    nfibers[eidx]->cidx.push_back(tetElement.attribute("cidx", "0").toInt(&ok));
                    if (!ok) {
                        return -2;
                    }
                    nfibers[eidx]->lowFilter.push_back(tetElement.attribute("lowFilter", "600").toInt(&ok));
                    if (!ok) {
                        return -2;
                    }
                    nfibers[eidx]->highFilter.push_back(tetElement.attribute("highFilter", "6000").toInt(&ok));
                    if (!ok) {
                        return -2;
                    }
                    tetidx++;
                    totalChannelCount++;
                }
                tetNode = tetNode.nextSibling();
            }

            eidx++;

        }
        n = n.nextSibling();
    }

    return 0;
}

void PhotometryConfiguration::saveToXML(QDomDocument &doc, QDomElement &rootNode)
{
    QDomElement photometryConf = doc.createElement("PhotometryConfiguration");

    rootNode.appendChild(photometryConf);

    for (int i = 0; i < nfibers.length(); i++) {
        QDomElement nt = doc.createElement("PhotometryNFiber");

        nt.setAttribute("carrierFreq1", nfibers[i]->carrierFreq[0]);
        nt.setAttribute("carrierFreq2", nfibers[i]->carrierFreq[1]);
        nt.setAttribute("color",        nfibers[i]->color.name());
        nt.setAttribute("id",           nfibers[i]->nFiberId);

        for (int j = 0; j < nfibers[i]->hw_chan.length(); j++) {
            QDomElement sCh = doc.createElement("PhotometryChannel");
            sCh.setAttribute("maxDisp", nfibers[i]->maxDisp[j]);
            sCh.setAttribute("hwChan", nfibers[i]->hw_chan[j]);
            sCh.setAttribute("filterOn",  nfibers[i]->filterOn[j]);
            sCh.setAttribute("cidx",  nfibers[i]->cidx[j]);
            sCh.setAttribute("lowFilter", nfibers[i]->lowFilter[j]);
            sCh.setAttribute("highFilter", nfibers[i]->highFilter[j]);
            nt.appendChild(sCh);
        }
        photometryConf.appendChild(nt);
    }
}
#endif

BenchmarkConfig::BenchmarkConfig() {
    BenchmarkConfig(false, false, false, false, false, false);
}

BenchmarkConfig::BenchmarkConfig(bool recSysTime, bool pSpikeDetect, bool pSpikeSent, bool pSpikeReceived, bool pPositionStreaming, bool pEventSys) {
    userRuntimeEdited = false;
    iniFromCmdLine = false;
    recordSysTime = recSysTime;
    spikeDetect = pSpikeDetect;
    spikeSent = pSpikeSent;
    spikeReceived = pSpikeReceived;
    positionStreaming = pPositionStreaming;
    eventSys = pEventSys;

    resetDefaultFreq();
    //qDebug() << "ini freq vals to: " << qPrintable(getFreqStr());
}

void BenchmarkConfig::setAllBenchmarking(bool on) {
    userRuntimeEdited = false;
    iniFromCmdLine = false;
    recordSysTime = on;
    spikeDetect = on;
    spikeSent = on;
    spikeReceived = on;
    positionStreaming = on;
    eventSys = on;
}

int BenchmarkConfig::loadFromXML(QDomNode &globalConfNode) {
    //qDebug() << "loading Benchmarking stuff from XML";
    //qDebug() << "@@@@@ Before loading xml: " << qPrintable(this->getBoolStr());
    //qDebug() << "@@@@@ Before loading xml f: " << qPrintable(this->getFreqStr());

    resetDefaultFreq();
    int readVal = -1;
    readVal = globalConfNode.toElement().attribute("recordSysTime", "-1").toInt();
    if (readVal >= 0) {
        recordSysTime = true;
    }
    else
        recordSysTime = false;

    readVal = globalConfNode.toElement().attribute("spikeDetect", "-1").toInt();
    if (readVal >= 0) {
        spikeDetect = true;
        if (readVal != 0) {
            freqSpikeDetect = readVal;
        }
    }
    else
        spikeDetect = false;

    readVal = globalConfNode.toElement().attribute("spikeSent", "-1").toInt();
    if (readVal >= 0) {
        spikeSent = true;
        if (readVal != 0) {
            freqSpikeSent = readVal;
        }
    }
    else
        spikeSent = false;

    readVal = globalConfNode.toElement().attribute("spikeReceived", "-1").toInt();
    if (readVal >= 0) {
        spikeReceived = true;
        if (readVal != 0) {
            freqSpikeReceived = readVal;
        }
    }
    else
        spikeReceived = false;

    readVal = globalConfNode.toElement().attribute("positionStreaming", "-1").toInt();
    if (readVal >= 0) {
        positionStreaming = true;
        if (readVal != 0) {
            freqPositionStreaming = readVal;
        }
    }
    else
        positionStreaming = false;

    readVal = globalConfNode.toElement().attribute("eventSys", "-1").toInt();

    if (readVal >= 0) {
        eventSys = true;
        if (readVal != 0) {
            freqEventSys = readVal;
        }
    }
    else
        eventSys = false;

    if (spikeDetect || spikeSent || spikeReceived || positionStreaming || eventSys)
        recordSysTime = true;
    //qDebug() << "@@@@@ After loading xml: " << qPrintable(this->getBoolStr());
    //qDebug() << "@@@@@ After loading xml f: " << qPrintable(this->getFreqStr());
    return(0);
}

void BenchmarkConfig::saveToXML(QDomDocument &doc, QDomElement &rootNode) {
    QDomElement bconf = doc.createElement("BenchmarkConfiguration");

    rootNode.appendChild(bconf);

    if (spikeDetect)
        bconf.setAttribute("spikeDetect", freqSpikeDetect);
    if (spikeSent)
        bconf.setAttribute("spikeSent", freqSpikeSent);
    if (spikeReceived)
        bconf.setAttribute("spikeReceived", freqSpikeReceived);
    if (positionStreaming)
        bconf.setAttribute("positionStreaming", freqPositionStreaming);
    if(eventSys)
        bconf.setAttribute("eventSys", freqEventSys);


}

void BenchmarkConfig::resetDefaultFreq() {
    freqSpikeDetect = BENCH_FREQ_SPIKE_DETECT_DEFAULT;
    freqSpikeSent = BENCH_FREQ_SPIKE_SENT_DEFAULT;
    freqSpikeReceived = BENCH_FREQ_SPIKE_RECEIVE_DEFAULT;
    freqPositionStreaming = BENCH_FREQ_POS_STREAM_DEFAULT;
    freqEventSys = BENCH_FREQ_EVENTSYS_DEFAULT;
}

void BenchmarkConfig::resetDefaultBools(){
    userRuntimeEdited = false;
    iniFromCmdLine = false;
    recordSysTime = false;
    spikeDetect = false;
    spikeSent = false;
    spikeReceived = false;
    positionStreaming = false;
    eventSys = false;
}

//-------------------------------------------
TrodesConfiguration::TrodesConfiguration():
    globalConf(NULL)
    ,streamConf(false) //True means do not create filters
    ,spikeConf(NULL)
    ,headerConf(NULL)
    ,moduleConf(NULL)
    ,networkConf()
    ,hardwareConf(NULL)
    ,benchConfig()
{

}


void TrodesConfiguration::clear() {



    globalConf.saveDisplayedChanOnly = false;
    globalConf.realTimeMode = false;
    globalConf.MBPerFileChunk = -1;

    globalConf.suppressModuleAbsPathWarning = 0;
    globalConf.timestampAtCreation = -1;
    globalConf.systemTimeAtCreation = -1;
    globalConf.headstageSerialNumber = "-1";
    globalConf.controllerSerialNumber = "-1";
    globalConf.autoSettleOn = -1;
    globalConf.smartRefOn = -1;
    globalConf.gyroSensorOn = -1;
    globalConf.accelSensorOn = -1;
    globalConf.magSensorOn = -1;
    globalConf.controllerFirmwareVersion = "-1";
    globalConf.headstageFirmwareVersion = "-1";

    streamConf.trodeChannelLookupByHWChan.clear();
    streamConf.pageBreaks.clear();
    streamConf.trodeIndexLookupByHWChan.clear();


    spikeConf.carGroups.clear();
    spikeConf.groupingDict.clear();
    spikeConf.deviceType = "";
    spikeConf.ntrodes.clear();  //Causes problems.  Need to fix.

    headerConf.digInIDList.clear();
    headerConf.digInPortList.clear();
    headerConf.digOutIDList.clear();
    headerConf.digOutPortList.clear();
    headerConf.headerChannels.clear();

    moduleConf.modulesDefined = false;
    moduleConf.trodesConfigFileName = "";
    moduleConf.singleModuleConf.clear();

    networkConf.networkConfigFound = false;
    networkConf.networkTypeConfiguredinWorkspace = false;
    networkConf.networkAddressConfiguredinWorkspace = false;

    hardwareConf.NCHAN=0;
    hardwareConf.headerSizeManuallyDefined = false;
    hardwareConf.sourceSamplingRate = 30000;
    hardwareConf.lfpSubsamplingInterval = 20;
    hardwareConf.ECUConnected = false;
    hardwareConf.sysTimeIncluded = false;
    hardwareConf.devices.clear();
    hardwareConf.sourceControls.clear();

    benchConfig.setAllBenchmarking(false);



}

bool TrodesConfiguration::isValid(){
    //temporary soln. ideal fix is to have a valid variable that is set if parsing went through with no errors
    // for now, hardwareconf devices shouldn't ever be empty unless it is invalid
    return !hardwareConf.devices.isEmpty();
}

QString TrodesConfiguration::readTrodesConfig(QString configFileName) {
    //Overloaded function that first opens up the workspace file before passing to the parser

    QDomDocument doc("TrodesConf");
    QFile file;
    bool isRecFile = false;
    int filePos = 0;
    QString errorString = "";

    if (!configFileName.isEmpty()) {
        file.setFileName(configFileName);
        if (!file.open(QIODevice::ReadOnly)) {
            if (!file.exists()) {
                errorString = "Error:" + QString("File %1 not found or does not exist").arg(configFileName);
            } else {
                errorString = "Error:" + QString("File %1 exists but can't be opened").arg(configFileName);
            }
            return errorString;
        }

        QFileInfo fi(configFileName);
        QString ext = fi.suffix(); // ext = "gz"
        if (ext.compare("rec") == 0) {
            //this is a rec file with a configuration in the header
            isRecFile = true;
            QString configContent;
            QString configLine;
            bool foundEndOfConfig = false;

            while (!file.atEnd()) {
                configLine += file.readLine();
                configContent += configLine;
                if (configLine.indexOf("</Configuration>") > -1) {
                    //qDebug() << "End of config header found at " << file.pos();
                    foundEndOfConfig = true;
                    break;
                }
                configLine = "";
            }

            if (foundEndOfConfig) {
                filePos = file.pos();
                if (!doc.setContent(configContent)) {
                    file.close();
                    errorString =  "Error: Config header didn't read properly.";
                    return errorString;
                }
            }
        }
        else {
            //this is a normal xml config file
            if (!doc.setContent(&file)) {
                file.close();
                errorString = "Error: XML didn't read properly. XML formatting issues likely.";
                return errorString;
            }
        }
        file.close();
        if (isRecFile) {
            dataStartLoc = filePos; //the position of the file where data begins
        }
    } else {
        errorString = "Error: empty filename.";
        return errorString;
    }


    return readTrodesConfig(doc, configFileName);
}

QString TrodesConfiguration::readTrodesConfig(QDomDocument doc, QString configFileName) {


    //Workspace parser function that takes a QDomDocument (XML) as input
    clear();

    QString errorString = "";
    QDomElement root = doc.documentElement();
    if (root.tagName() != "Configuration") {
        errorString = "Configuration not root node. Found " + root.tagName();
        return errorString;
    }


    hardwareConf.sourceSamplingRate = 0;
    hardwareConf.headerSize = 0;
    hardwareConf.NCHAN = 0;

    //Load global options.  Note that this is only for backwards compatibility; new files have a GlobalConfiguration section
    QDomNodeList globalOptions = root.elementsByTagName("GlobalOptions");
    if (globalOptions.length() == 1) {
        // load up the globalConf and hardwareConf options from this section
        QDomNode optionsNode = globalOptions.item(0);
        QDomElement optionElements = optionsNode.toElement();

        int tempNCHAN = optionElements.attribute("numChannels", "0").toInt();
        if (tempNCHAN > 0) {
            hardwareConf.NCHAN = tempNCHAN;
            if ((hardwareConf.NCHAN % 32) != 0) {
                errorString =  "Error: numChannels must be a multiple of 32";
                return errorString;
            }

        }

        int tempSampRate = optionElements.attribute("samplingRate", "0").toInt();
        if (tempSampRate > 0) {
            hardwareConf.sourceSamplingRate = tempSampRate;
        }

        //Default LFP subsampling is to get close to 1500 Hz rate
        QString defaultLfpSubSamplingString;
        if (hardwareConf.sourceSamplingRate == 30000) {
            QString defaultLfpSubSamplingString = "20";
        } else if (hardwareConf.sourceSamplingRate == 25000) {
            QString defaultLfpSubSamplingString = "16";
        } else if (hardwareConf.sourceSamplingRate == 20000) {
            QString defaultLfpSubSamplingString = "13";
        } else {
            QString defaultLfpSubSamplingString = "20";
        }

        hardwareConf.lfpSubsamplingInterval = optionElements.attribute("lfpSubsamplingInterval", defaultLfpSubSamplingString).toUInt();
        if (hardwareConf.lfpSubsamplingInterval < 1) {
            hardwareConf.lfpSubsamplingInterval = 1;
        }

        int tempHeaderSize = optionElements.attribute("headerSize", "0").toInt();
        if (tempHeaderSize > 0) {
            hardwareConf.headerSize = tempHeaderSize;

            hardwareConf.headerSizeManuallyDefined = true;
        }

        globalConf.filePrefix = optionElements.attribute("filePrefix", "");
        globalConf.filePath = optionElements.attribute("filePath", "");

    }
    else {
        QDomNodeList globalConfigList = root.elementsByTagName("GlobalConfiguration");
        if (globalConfigList.length() > 1) {
            errorString = "Error: multiple GlobalConfiguration sections found in configuration file.";
            return errorString;
        }
        QDomNode globalnode = globalConfigList.item(0);
        int globalErrorCode = globalConf.loadFromXML(globalnode);
        if (globalErrorCode != 0) {
            errorString = "Error: Failed to load global configuration";
            return errorString;
        }

    }
    globalConf.configfilepath = configFileName;


    QDomNodeList benchmarkConfigList = root.elementsByTagName("BenchmarkConfiguration");

    if (benchmarkConfigList.length() > 0) {
        QDomNode benchNode = benchmarkConfigList.item(0);

        //only load the benchmarking settings from the workspace if benchmarking was not initiated from the command line
        if (!benchConfig.wasInitiatedFromCommandLine() && !benchConfig.wasEditedByUser()) {
            benchConfig.resetDefaultFreq();
            benchConfig.loadFromXML(benchNode);
        }

        //TODO: add in error catch/throw message for incorrect benchmark config settings / failure to load
    }


    QDomNodeList hardwareConfigList = root.elementsByTagName("HardwareConfiguration");
    if (hardwareConfigList.length() > 1) {
        errorString = "Error: Multiple HardwareConfiguration sections found in configuration file.";
        return errorString;
    }
    if (hardwareConfigList.length() > 0) {
        QDomNode hardwarenode = hardwareConfigList.item(0);
        int hardwareErrorCode = hardwareConf.loadFromXML(hardwarenode);
        if (hardwareErrorCode != 0) {
            errorString =  "Error: Failed to load hardware section.";
            return errorString;
        }

    }


    // PARSE NETWORK CONFIGURATION
    QDomNodeList networkConfigList = root.elementsByTagName("NetworkConfiguration");
    if (networkConfigList.length() > 1) {
        errorString = "Error: multiple NetworkConfiguration sections found in configuration file.";
        return errorString;
    }

    if (networkConfigList.length() > 0) {

        QDomNode networknode = networkConfigList.item(0);
        int networkErrorCode = networkConf.loadFromXML(networknode);
        if (networkErrorCode != 0) {
            errorString =  "Error: Failed to load network section.";
            return errorString;
        }
        networkConf.networkConfigFound = true;

    }
    else {

        networkConf.hardwareAddress = TRODESHARDWARE_DEFAULTIP;
        networkConf.networkType = NetworkConfiguration::qsocket_based;

        // TO DO: find the trodes hardware if it's address wasn't specified
    }
    /* set the fixed port values */
//    networkConf.trodesPort = TRODESHARDWARE_CONTROLPORT;
    networkConf.ecuDirectPort = TRODESHARDWARE_ECUDIRECTPORT;
    networkConf.hardwarePort = TRODESHARDWARE_CONTROLPORT;

    if (networkConf.trodesHost == "") {
        // assume localhost
        networkConf.trodesHost = "127.0.0.1";
    }

    if ((networkConf.networkConfigFound) == false || (networkConf.dataSocketType == 0)) {
        // set to TCPIP if this was not specified
        networkConf.dataSocketType = TRODESSOCKETTYPE_TCPIP;
    }

    // PARSE MODULE CONFIGURATION
    QDomNodeList moduleConfigList = root.elementsByTagName("ModuleConfiguration");
    if (moduleConfigList.length() > 1) {
        errorString =  "Error: multiple ModuleConfig sections found in configuration file.";
        return errorString;
    }

    moduleConf.trodesConfigFileName = configFileName;
    if (moduleConfigList.length() > 0) {
        //moduleConf.trodesConfigFileName = configFileName;
        QDomNode modulenode = moduleConfigList.item(0);
        int moduleErrorCode = moduleConf.loadFromXML(modulenode);
        if (moduleErrorCode != 0) {
            errorString =  "Error: Failed to load module section.";
            return errorString;
        }
    }



    // PARSE HEADER CONFIGURATION
    //QDomNodeList headerList = root.elementsByTagName("HeaderDisplay");
    QDomNodeList headerList = root.elementsByTagName("AuxDisplayConfiguration");
    if (headerList.length() > 1) {
        errorString =  "Error: multiple AuxDisplayConfiguration sections found in configuration file.";
        return errorString;
    }

    if (headerList.length() > 0) {
        QDomNode headernode = headerList.item(0);

        int headerErrorCode = headerConf.loadFromXML(headernode,&hardwareConf);
        if (headerErrorCode != 0) {
            errorString =  "Error: Failed to load Aux Display section.";
            return errorString;
        }
    }

    // PARSE STREAMING CONFIGURATION
    QDomNodeList strlist = root.elementsByTagName("StreamDisplay");
    if (strlist.isEmpty() || strlist.length() > 1) {
        errorString = "Error: either no or multiple streamDisplay sections found in configuration file.";
        return errorString;
    }
    QDomNode eegvisnode = strlist.item(0);

    int streamErrorCode = streamConf.loadFromXML(eegvisnode);
    if (streamErrorCode != 0) {
        errorString =  "Error: Failed to load StreamDisplay section.";
        return errorString;
    }




    /* --------------------------------------------------------------------- */
    /* --------------------------------------------------------------------- */
    //      PARSE CAR group CONFIGURATION
    //      MUST BE BEFORE SPIKE CONF PARSING, SINCE SPIKE PARSING WILL MARK GROUPS AS USED OR NOT
    QDomNodeList carlist = root.elementsByTagName("CARGroup");
    for(int i = 0; i < carlist.length() && i < MAXCARGROUPS; ++i){
        QDomNodeList chans = carlist.item(i).childNodes();
        CARGroup group;
        group.useCount = 0;
        group.description = carlist.item(i).toElement().attribute("description");
        for(int j = 0; j < chans.length(); ++j){
            int ntrode = chans.item(j).toElement().attribute("ntrodeid").toInt();
            int chan = chans.item(j).toElement().attribute("chan").toInt();
            group.chans.append({ntrode, chan, -1});
       }
        spikeConf.carGroups.append(group);
    }



    // PARSE SPIKE CONFIGURATION
    QDomNodeList list = root.elementsByTagName("SpikeConfiguration");

    QDomNode spikenode = list.item(0);
    int autoNtrodeType = 0;
    autoNtrodeType = spikenode.toElement().attribute("autoPopulate", "0").toInt();
    if (autoNtrodeType > 0) {

        int numTrodes = hardwareConf.NCHAN/autoNtrodeType;
        qDebug() << "Auto populating" << numTrodes << "ntrodes";
        spikeConf.ntrodes.setSize(numTrodes);
    } else {
        spikeConf.ntrodes.setSize(spikenode.childNodes().length());
    }

    int spikeErrorCode = spikeConf.loadFromXML(spikenode,hardwareConf.NCHAN);

    if (spikeErrorCode < 0) {
        errorString =  "Error: Failed to load SpikeConfiguration section.";
        return errorString;
    } else if (spikeErrorCode > 0) {
        spikeConf.carGroups.clear();  //remove any CAR groups if they exist. Doesn't make sense if we are automatically generaing the section.

        if (spikeErrorCode == (int)SpikeConfiguration::ReturnCode::autoConfigNeeded_neuropixel1) {
            //neuropixel probes are being used, and the SpikeConfiguration section needs to be automatically generated from the info in the Hardware section
            bool autoConfSuccess = autoPopulate_neuropixels1();
        }
    }

    //Fill in hwchans in CAR groups
    for(int i = 0; i < spikeConf.carGroups.length(); ++i){
        bool anotherCheckNeeded = true;

        while (anotherCheckNeeded) {
            anotherCheckNeeded = false;
            for(int j = 0; j < spikeConf.carGroups[i].chans.length(); ++j){
                int ntrode = spikeConf.carGroups[i].chans[j].ntrodeid;
                int chan = spikeConf.carGroups[i].chans[j].chan;


                if (spikeConf.ntrodes.ID(ntrode)->stimCapable[chan-1]) {
                    //We do not allow stim capable channels to be included in CAR groups. But we still
                    //need the config to open. So we rescue the config by removing the stim-capable channels in the group.

                    qDebug() << "ALERT: a stimulation-capable channel was included in a CAR group. It has been removed from the group.";
                    spikeConf.carGroups[i].chans.removeAt(j);
                    anotherCheckNeeded = true;

                    //Turn off CAR reference for this group
                    if (spikeConf.carGroups[i].useCount > 0) {
                        for (int ntInd=0;ntInd<spikeConf.ntrodes.length();ntInd++) {
                            if (spikeConf.ntrodes[ntInd]->refGroup-1 == i) { //The ref group is 1-based
                                spikeConf.ntrodes[ntInd]->groupRefOn = false;
                                //spikeConf.ntrodes[ntInd]->refGroup
                                spikeConf.ntrodes[ntInd]->refGroup = 0;
                            }
                        }
                        spikeConf.carGroups[i].useCount = 0;
                    }

                    break;



                }

                spikeConf.carGroups[i].chans[j].hw_chan = spikeConf.ntrodes.ID(ntrode)->hw_chan[chan-1];
            }

        }
    }

#ifdef TRODES_CODE
    BesselFilter tmpFilt;
    for (int i=0; i<spikeConf.ntrodes.length(); i++) {
        if (!tmpFilt.isFilterSupported(spikeConf.ntrodes[i]->lowFilter,spikeConf.ntrodes[i]->highFilter,hardwareConf.sourceSamplingRate)) {
            spikeConf.ntrodes[i]->lowFilter = 600;
            spikeConf.ntrodes[i]->highFilter = 6000;
        }

    }
#endif



    bool indexCreationOk = streamConf.createIndexData(&hardwareConf,&spikeConf);
    if (!indexCreationOk) {
        errorString = "Error: there was an issue creating the channel index vectors.";
    }
    streamConf.setChanToSave(hardwareConf.NCHAN);


    return errorString;

}


bool TrodesConfiguration::writeTrodesConfig(QString configFileName)
{

    bool debugTest = false;

    QDomDocument doc;
    QDomElement root = doc.createElement("Configuration");

    doc.appendChild(root);

    if (debugTest) qDebug() << "writeTrodesConfig 1";

    globalConf.saveToXMLNoSessionData(doc, root);
    if (debugTest) qDebug() << "writeTrodesConfig 2";
    benchConfig.saveToXML(doc, root);
    if (debugTest) qDebug() << "writeTrodesConfig 3";
    networkConf.saveToXML(doc, root);
    /*if (networkConf.networkConfigFound) {

        networkConf.saveToXML(doc, root);
    }*/
    if (debugTest) qDebug() << "writeTrodesConfig 4";
    hardwareConf.saveToXML(doc, root);
    if (debugTest) qDebug() << "writeTrodesConfig 5";
    moduleConf.saveToXML(doc, root);
    if (debugTest) qDebug() << "writeTrodesConfig 6";
    streamConf.saveToXML(doc, root);
    if (debugTest) qDebug() << "writeTrodesConfig 7";
    headerConf.saveToXML(doc, root,&hardwareConf);
    if (debugTest) qDebug() << "writeTrodesConfig 8";
    spikeConf.saveToXML(doc, root);
    if (debugTest) qDebug() << "writeTrodesConfig 9";
    QFile file(configFileName);
    //use QIODevice::Truncate to overwrite any previously existing file and QIODevice::Text to write newline characters using the local8bit encoding (cross platform diz)
    if (file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)) {
        QTextStream TextStream(&file);
        QString xmlString = doc.toString();
        //doc.save(TextStream, 0);
        QString vers = "<?xml version=\"1.0\"?>";
        TextStream << vers << Qt::endl << xmlString;
        file.close();
        return true;
    }
    else {
        return false;
    }
}

bool TrodesConfiguration::writeRecConfig(QString configFileName, qint64 currentTimestamp)
{

    /*if (currentTimestamp < 0) {
        currentTimestamp = globalConf.timestampAtCreation;
    }*/

    QDomDocument doc;
    QDomElement root = doc.createElement("Configuration");

    doc.appendChild(root);

    globalConf.saveToXML(doc, root,currentTimestamp);
    hardwareConf.saveToXML(doc, root);
    moduleConf.saveToXML(doc, root);
    streamConf.saveToXML(doc, root);
    headerConf.saveToXML(doc, root, &hardwareConf);
    networkConf.saveToXML(doc, root);
    /*if (networkConf.networkConfigFound) {
        networkConf.saveToXML(doc, root);
    }*/
    spikeConf.saveToXML(doc, root);

    QFile file(configFileName);

    if (file.open(QIODevice::WriteOnly)) {
        QTextStream TextStream(&file);
        QString xmlString = doc.toString();
        //doc.save(TextStream, 0);
        QString vers = "<?xml version=\"1.0\"?>";
        TextStream << vers << Qt::endl << xmlString;
        file.close();
        return true;
    }
    else {
        return false;
    }
}

TrodesConfiguration& TrodesConfiguration::operator=(const TrodesConfiguration &tc)
{
    globalConf.filePath = tc.globalConf.filePath;
    globalConf.qtVersion = tc.globalConf.qtVersion;
    globalConf.filePrefix = tc.globalConf.filePrefix;
    globalConf.smartRefOn = tc.globalConf.smartRefOn;
    globalConf.compileDate = tc.globalConf.compileDate;
    globalConf.compileTime = tc.globalConf.compileTime;
    globalConf.magSensorOn = tc.globalConf.magSensorOn;
    globalConf.autoSettleOn = tc.globalConf.autoSettleOn;
    globalConf.gyroSensorOn = tc.globalConf.gyroSensorOn;
    globalConf.realTimeMode = tc.globalConf.realTimeMode;
    globalConf.accelSensorOn = tc.globalConf.accelSensorOn;
    globalConf.commitHeadStr = tc.globalConf.commitHeadStr;
    globalConf.trodesVersion = tc.globalConf.trodesVersion;
    globalConf.MBPerFileChunk = tc.globalConf.MBPerFileChunk;
    globalConf.configfilepath = tc.globalConf.configfilepath;
    globalConf.timestampAtCreation = tc.globalConf.timestampAtCreation;
    globalConf.systemTimeAtCreation = tc.globalConf.systemTimeAtCreation;
    globalConf.headstageSerialNumber = tc.globalConf.headstageSerialNumber;
    globalConf.saveDisplayedChanOnly = tc.globalConf.saveDisplayedChanOnly;
    globalConf.controllerSerialNumber = tc.globalConf.controllerSerialNumber;
    globalConf.headstageFirmwareVersion = tc.globalConf.headstageFirmwareVersion;
    globalConf.controllerFirmwareVersion = tc.globalConf.controllerFirmwareVersion;
    globalConf.suppressModuleAbsPathWarning = tc.globalConf.suppressModuleAbsPathWarning;

    hardwareConf.devices = tc.hardwareConf.devices;
    hardwareConf.NCHAN = tc.hardwareConf.NCHAN;
    hardwareConf.headerSize = tc.hardwareConf.headerSize;
    hardwareConf.ECUConnected = tc.hardwareConf.ECUConnected;
    hardwareConf.sourceControls = tc.hardwareConf.sourceControls;
    hardwareConf.sysTimeIncluded = tc.hardwareConf.sysTimeIncluded;
    hardwareConf.sourceSamplingRate = tc.hardwareConf.sourceSamplingRate;
    hardwareConf.lfpSubsamplingInterval = tc.hardwareConf.lfpSubsamplingInterval;
    hardwareConf.headerSizeManuallyDefined = tc.hardwareConf.headerSizeManuallyDefined;
    hardwareConf.useIntrinsics = tc.hardwareConf.useIntrinsics;

    moduleConf.myID = tc.moduleConf.myID;
    moduleConf.modulesDefined = tc.moduleConf.modulesDefined;
    moduleConf.singleModuleConf = tc.moduleConf.singleModuleConf;
    moduleConf.trodesConfigFileName = tc.moduleConf.trodesConfigFileName;

    streamConf.FS = tc.streamConf.FS;
    streamConf.horizontalLayout = tc.streamConf.horizontalLayout;
    streamConf.pageBreaks = tc.streamConf.pageBreaks;
    streamConf.nTabs = tc.streamConf.nTabs;
    streamConf.tLength = tc.streamConf.tLength;
    streamConf.nColumns = tc.streamConf.nColumns;
    streamConf.saveHWChan = tc.streamConf.saveHWChan;
    streamConf.backgroundColor = tc.streamConf.backgroundColor;
    streamConf.nChanConfigured = tc.streamConf.nChanConfigured; //*
    streamConf.trodeIndexLookupByHWChan = tc.streamConf.trodeIndexLookupByHWChan; //*
    streamConf.trodeChannelLookupByHWChan = tc.streamConf.trodeChannelLookupByHWChan; //*

    headerConf.headerChannels = tc.headerConf.headerChannels;
    headerConf.digInIDList = tc.headerConf.digInIDList;
    headerConf.digOutIDList = tc.headerConf.digOutIDList;
    headerConf.digInPortList = tc.headerConf.digInPortList;
    headerConf.digOutPortList = tc.headerConf.digOutPortList;

    spikeConf.deviceType = tc.spikeConf.deviceType;
    spikeConf.groupingDict = tc.spikeConf.groupingDict;
    spikeConf.carGroups = tc.spikeConf.carGroups;
    //spikeConf.ntrodes = tc.spikeConf.ntrodes;
    spikeConf.ntrodes.copyFrom(tc.spikeConf.ntrodes);

    networkConf.trodesHost = tc.networkConf.trodesHost;
    networkConf.trodesPort = tc.networkConf.trodesPort;
    networkConf.hardwarePort = tc.networkConf.hardwarePort;
    networkConf.ecuDirectPort = tc.networkConf.ecuDirectPort;
    networkConf.dataSocketType = tc.networkConf.dataSocketType;
    networkConf.hardwareAddress = tc.networkConf.hardwareAddress;
    networkConf.networkConfigFound = tc.networkConf.networkConfigFound;
    networkConf.networkTypeConfiguredinWorkspace = tc.networkConf.networkTypeConfiguredinWorkspace;
    networkConf.networkAddressConfiguredinWorkspace = tc.networkConf.networkAddressConfiguredinWorkspace;
    networkConf.networkType = tc.networkConf.networkType;

    //This should maybe be commented out
    //networkConf.myModuleID = tc.networkConf.myModuleID;
    //networkConf.numModulesConnected = tc.networkConf.numModulesConnected;


    dataStartLoc = tc.dataStartLoc;


    return *this;

}

bool TrodesConfiguration::autoPopulate_neuropixels1()
{

    int totalNumProbesDefined = 0;
    int numChannelsConfigured = 0;



    //Create LFP section in the hardare config
    DeviceInfo lfpDevice;
    lfpDevice.name = "neuropixels_LFP";
    lfpDevice.packetOrderPreference = 1000; //should always come last (high number), unless SysTime is included (see below)
    lfpDevice.available = true;
    lfpDevice.byteOffset = (hardwareConf.headerSize*2);

    QList<QVector<bool> > channelsOn_allProbes;
    QList<QList<QColor> > colors;
    QList<int> spikeGains;
    QList<int> lfpGains;

    for (int i=0; i<hardwareConf.sourceControls.length();i++) { //Probes
        if (hardwareConf.sourceControls.at(i).name == "NeuroPixels1") {
            //We have found a section for the Neuropixels 1.0 probe
            //qDebug() << "Found np section:" << workspace.hardwareConf->sourceControls.at(i).unitNumber;



            //set default mode for each probe here

            int tmpAPGainMode = 1000;
            int tmpLFPGainMode = 1000;
            int tmpRefMode = 0;

            QVector<int> gainModes;
            gainModes << 50 << 125 << 250 << 500 << 1000 << 1500 << 2000 << 3000;


            QVector<bool> channelsOn;

            bool channelsOnFound = false;



            for (int j=0;j<hardwareConf.sourceControls.at(i).customOptions.length();j++) {

                if (hardwareConf.sourceControls.at(i).customOptions.at(j).name == "channelsOn") {

                    for (int l=0;l<hardwareConf.sourceControls.at(i).customOptions.at(j).data.length();l++) {
                        channelsOn.push_back((bool)hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(l));
                    }
                    //channelsOn += hardwareConf.sourceControls.at(i).customOptions.at(j).data; //import all the numbers (should be 384 0s and 1s)
                    if (channelsOn.length() != 960) {
                        qDebug() << "Error: each neuropixels section must have eith no channelsOn section, or a section with 960 entries.";
                        return false;
                    }
                    channelsOnFound = true;

                }



                if (hardwareConf.sourceControls.at(i).customOptions.at(j).name == "APGainMode") {
                    int APGainVal = hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0);

                    for (int s=0;s<gainModes.length();s++) {
                        if (gainModes.at(s) == APGainVal) {
                            tmpAPGainMode = APGainVal;
                            break;
                        }
                    }


                }

                if (hardwareConf.sourceControls.at(i).customOptions.at(j).name == "LFPGainMode") {
                    int lfpGainVal = hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0);

                    for (int s=0;s<gainModes.length();s++) {
                        if (gainModes.at(s) == lfpGainVal) {
                            tmpLFPGainMode = lfpGainVal;
                            break;
                        }
                    }

                }

                if (hardwareConf.sourceControls.at(i).customOptions.at(j).name == "RefMode") {
                    if ( (hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0) >= 0) && (hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0) <= 2) ) {
                        tmpRefMode = hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0);
                    }
                }





            }

            spikeGains.push_back(tmpAPGainMode);
            lfpGains.push_back(tmpLFPGainMode);
            //TODO: strore ref modes??

            if (!channelsOnFound) {
                //no channelsOn section found, so we create a default entry
                for (int n=0;n<384;n++) {
                    channelsOn.push_back(true);
                }
                for (int n=0;n<576;n++) {
                    channelsOn.push_back(false);
                }
            }

            channelsOn_allProbes.push_back(channelsOn);
            QList<QColor> colorsForThisProbe;
            for (int c=0;c<960;c++) {
                colorsForThisProbe.push_back(QColor(255,0,0));
            }
            colors.push_back(colorsForThisProbe);

        }
    }

    QList<QPointF> channelLocations;
    QList<QVector<int> > probeLocations;

    return addNeuropixelsNtrodes(channelsOn_allProbes,colors,spikeGains,lfpGains, channelLocations, probeLocations);

}



void  TrodesConfiguration::getNeuropixelsHardwareCommandData(NeuroPixelsSettings &s)
{


    QList<int> APGainForEachProbe;
    QList<int> LFPGainForEachProbe;
    QList<int> refModeForEachProbe;
    QList<QVector<bool> > channelsTurnedOn;

    QVector<int> gainModes;
    gainModes << 50 << 125 << 250 << 500 << 1000 << 1500 << 2000 << 3000;

    for (int i=0; i<hardwareConf.sourceControls.length();i++) { //Probes
        if (hardwareConf.sourceControls.at(i).name == "NeuroPixels1") {

            //We have found a section for the Neuropixels 1.0 probe

            //set default mode for each probe here
            int tmpAPGainMode = 0;
            int tmpLFPGainMode = 0;
            int tmpRefMode = 0;
            QVector<bool> tmpChannelsTurnedOn;

            for (int j=0;j<hardwareConf.sourceControls.at(i).customOptions.length();j++) {


                if (hardwareConf.sourceControls.at(i).customOptions.at(j).name == "APGainMode") {
                        int APGainVal = hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0);

                        for (int s=0;s<gainModes.length();s++) {
                            if (gainModes.at(s) == APGainVal) {
                                tmpAPGainMode = s;
                                break;
                            }
                        }
                }

                if (hardwareConf.sourceControls.at(i).customOptions.at(j).name == "LFPGainMode") {
                        int LFPGainVal = hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0);

                        for (int s=0;s<gainModes.length();s++) {
                            if (gainModes.at(s) == LFPGainVal) {
                                tmpLFPGainMode = s;
                                break;
                            }
                        }
                }

                if (hardwareConf.sourceControls.at(i).customOptions.at(j).name == "RefMode") {
                    if ( (hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0) >= 0) && (hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0) <= 2) ) {
                        tmpRefMode = hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0);
                    }
                }

                if (hardwareConf.sourceControls.at(i).customOptions.at(j).name == "channelsOn") {

                    if (hardwareConf.sourceControls.at(i).customOptions.at(j).data.length() == 960) {
                        for (int k=0; k<960;k++) {
                            tmpChannelsTurnedOn.push_back(hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(k));
                        }
                    }


                }


            }


            //selectionModeForEachProbe.push_back(tmpSelectionMode);
            APGainForEachProbe.push_back(tmpAPGainMode);
            LFPGainForEachProbe.push_back(tmpLFPGainMode);
            refModeForEachProbe.push_back(tmpRefMode);
            channelsTurnedOn.push_back(tmpChannelsTurnedOn);
            //allSelectionModeArgs.push_back(probeData);
        }
    }

    s.setNumProbes(APGainForEachProbe.length());
    for (int i=0;i<APGainForEachProbe.length();i++) {

        s.setAPGainCode(APGainForEachProbe.at(i),i);
        s.setLFPGainCode(LFPGainForEachProbe.at(i),i);
        s.setRefCode(refModeForEachProbe.at(i),i);

        if (channelsTurnedOn.at(i).length() < 1) {
            qDebug() << "Error: Neuropixels pad on/off state missing from workspace.";
            return;
        }
        for (int j=0;j<960;j++) {


            s.setPadState(j,channelsTurnedOn.at(i).at(j),i);

        }
    }

}

void TrodesConfiguration::generateNeuropixelsWorkspace(int numProbes)
{

    QList<QVector<bool> > channelsOn_allProbes;
    QList<QList<QColor> > colors;
    QList<int> spikeGains;
    QList<int> lfpGains;
    QList<int> refModes;

    //Remove extra neuropixel entries if we have too many
    bool doneTrimming = false;
    int existingProbesInWorkspace=0;
    while (!doneTrimming) {
        existingProbesInWorkspace = 0;
        for (int i=0; i<hardwareConf.sourceControls.length();i++) { //Probes
            if (hardwareConf.sourceControls.at(i).name == "NeuroPixels1") {
                existingProbesInWorkspace++;
                if (existingProbesInWorkspace > numProbes) {
                    hardwareConf.sourceControls.removeAt(i);
                    break;
                }
            }
        }
        if (existingProbesInWorkspace <= numProbes) {
            //we did not have to remove any sourceControl entries, so no extra pass-through required
            doneTrimming = true;
        }
    }

    //Add neuropixel entries if we don't have enough
    while (existingProbesInWorkspace < numProbes) {
        SourceControl s;
        s.name = "NeuroPixels1";
        hardwareConf.sourceControls.push_back(s);
        existingProbesInWorkspace++;
    }

    for (int i=0; i<hardwareConf.sourceControls.length();i++) { //Probes
        if (hardwareConf.sourceControls.at(i).name == "NeuroPixels1") {
            //We have found a section for the Neuropixels 1.0 probe
            //qDebug() << "Found np section:" << workspace.hardwareConf->sourceControls.at(i).unitNumber;



            //set default mode for each probe here

            int tmpAPGainMode = 1000;
            int tmpLFPGainMode = 1000;
            int tmpRefMode = 0;

            QVector<int> gainModes;
            gainModes << 50 << 125 << 250 << 500 << 1000 << 1500 << 2000 << 3000;


            QVector<bool> channelsOn;

            bool channelsOnFound = false;



            for (int j=0;j<hardwareConf.sourceControls.at(i).customOptions.length();j++) {

                if (hardwareConf.sourceControls.at(i).customOptions.at(j).name == "channelsOn") {

                    for (int l=0;l<hardwareConf.sourceControls.at(i).customOptions.at(j).data.length();l++) {
                        channelsOn.push_back((bool)hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(l));
                    }
                    //channelsOn += hardwareConf.sourceControls.at(i).customOptions.at(j).data; //import all the numbers (should be 384 0s and 1s)
                    if (channelsOn.length() != 960) {
                        channelsOnFound = false;
                        channelsOn.clear();
                    } else {
                        channelsOnFound = true;
                    }

                }



                if (hardwareConf.sourceControls.at(i).customOptions.at(j).name == "APGainMode") {
                    int APGainVal = hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0);

                    for (int s=0;s<gainModes.length();s++) {
                        if (gainModes.at(s) == APGainVal) {
                            tmpAPGainMode = APGainVal;
                            break;
                        }
                    }


                }
                if (hardwareConf.sourceControls.at(i).customOptions.at(j).name == "LFPGainMode") {
                    int lfpGainVal = hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0);

                    for (int s=0;s<gainModes.length();s++) {
                        if (gainModes.at(s) == lfpGainVal) {
                            tmpLFPGainMode = lfpGainVal;
                            break;
                        }
                    }

                }
                if (hardwareConf.sourceControls.at(i).customOptions.at(j).name == "RefMode") {
                    if ( (hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0) >= 0) && (hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0) <= 2) ) {
                        tmpRefMode = hardwareConf.sourceControls.at(i).customOptions.at(j).data.at(0);
                    }
                }





            }
            spikeGains.push_back(tmpAPGainMode);
            lfpGains.push_back(tmpLFPGainMode);
            refModes.push_back(tmpRefMode);


            if (!channelsOnFound) {
                //no channelsOn section found, so we create a default entry
                for (int n=0;n<384;n++) {
                    channelsOn.push_back(true);
                }
                for (int n=0;n<576;n++) {
                    channelsOn.push_back(false);
                }
            }

            channelsOn_allProbes.push_back(channelsOn);


            //We should eventually find the colors if they have been set, but for now
            //they get changed to red
            QList<QColor> colorsForThisProbe;
            for (int c=0;c<960;c++) {
                colorsForThisProbe.push_back(QColor(255,0,0));
            }
            colors.push_back(colorsForThisProbe);

        }
    }

    QList<QPointF> channelLocations;
    QList<QVector<int> > probeLocations;
    addNeuropixelsNtrodes(channelsOn_allProbes,colors,spikeGains,lfpGains,channelLocations, probeLocations);
}

void TrodesConfiguration::removeNeuropixelsConfig()
{
    //Check if there exists an LFP section for neuropixels probes. If so, remove it.
    for (int dCheck = 0; dCheck < hardwareConf.devices.length(); dCheck++) {
        if (hardwareConf.devices.at(dCheck).name == "neuropixels_LFP") {
            hardwareConf.headerSize = hardwareConf.headerSize - (hardwareConf.devices.at(dCheck).numBytes/2);
            hardwareConf.devices.removeAt(dCheck);
            break;
        }
    }

    //Clear the ntrode entries
    if (!spikeConf.ntrodes.isEmpty()) {
        spikeConf.ntrodes.clear();
    }

    streamConf.pageBreaks.clear();

}

void TrodesConfiguration::printChannelOrderInPacket()
{

    int minHWallowed = -1; //The smallest allowed in each search
    QList<int> order;


    bool keepGoing = true;
    while (keepGoing) {
        int tempMinHW = 1000000; //more than the highest possible HW channel
        int minHWTrodeIndex = -1;
        int minHWChannelIndex = -1;

        for (int i=0; i < spikeConf.ntrodes.length(); i++) {
            for (int j = 0; j < spikeConf.ntrodes.at(i)->hw_chan.length(); j++) {
                if ((spikeConf.ntrodes.at(i)->hw_chan.at(j) > minHWallowed) && ((spikeConf.ntrodes.at(i)->hw_chan.at(j) < tempMinHW))) {
                    minHWTrodeIndex = i;
                    minHWChannelIndex = j;
                    tempMinHW = spikeConf.ntrodes.at(i)->hw_chan.at(j);

                }

            }
        }
        if (minHWTrodeIndex > -1) {
            order.append(spikeConf.ntrodes.at(minHWTrodeIndex)->nTrodeId);
            minHWallowed = spikeConf.ntrodes.at(minHWTrodeIndex)->hw_chan.at(minHWChannelIndex);
        } else {
            keepGoing = false;
        }
    }

    for (int i=0; i < order.length(); i++) {
        qDebug() << order.at(i);
    }

}

bool TrodesConfiguration::addNeuropixelsNtrodes(QList<QVector<bool> > channelsOn_allProbes, QList<QList<QColor> > colors, QList<int> spikeGains, QList<int> lfpGains, QList<QPointF> channelLocations, QList<QVector<int> > probeLocations)
{

    if (channelsOn_allProbes.length() != colors.length()) {
        return false;
    }

    //QList<QVector<int> > probeLocations;


    //Check if there alrady exists an LFP section for neuropixels probes. If so, remove it.
    for (int dCheck = 0; dCheck < hardwareConf.devices.length(); dCheck++) {
        if (hardwareConf.devices.at(dCheck).name == "neuropixels_LFP") {
            hardwareConf.headerSize = hardwareConf.headerSize - (hardwareConf.devices.at(dCheck).numBytes/2);
            hardwareConf.devices.removeAt(dCheck);
            break;
        }
    }

    //Clear the ntrode entries
    if (!spikeConf.ntrodes.isEmpty()) {
        spikeConf.ntrodes.clear();
    }

    //spikeConf.groupingDict.addCategory("X");
    //spikeConf.groupingDict.addCategory("Y");



    //Create LFP section in the hardare config
    DeviceInfo lfpDevice;
    lfpDevice.name = "neuropixels_LFP";
    lfpDevice.packetOrderPreference = 1000; //should always come last (high number), unless SysTime is included (see below)
    lfpDevice.available = true;
    lfpDevice.byteOffset = (hardwareConf.headerSize*2);


    int totalNumProbesDefined = 0;
    int numChannelsConfigured = 0;

    //We need to define the order that the 384 channels come in.
    //One channel from each of the 32 ADC groups (32 channels total) is presented first. If there are multiple
    //probes, then it does this for all probes. Then it goes back to the first probe and presents
    //the second channel from each of the 32 ADC's
    //Since there are 12 channels per ADC group, it takes 12 cycles to make it through all of the channels.

    //First we define the channel order for a single probe (based on NP documentation). We will deal with multiple
    //probes below
    QList<int> startChannelPerADC;
    startChannelPerADC << 0 << 1 << 24 << 25 << 48 << 49 << 72 << 73 << 96 << 97 << 120 << 121 << 144 << 145 << 168 << 169 << 192 << 193 << 216 << 217 << 240 << 241 << 264 << 265 << 288 << 289 << 312 << 313 << 336 << 337 << 360 << 361;
    QList<int> packetOrder;

    //12 channels per ADC
    for (int ADCch = 0; ADCch < 12; ADCch++) {
        //32 ADCs
        for (int ADCindex = 0; ADCindex < 32; ADCindex++) {
            packetOrder << startChannelPerADC.at(ADCindex)+(2*ADCch);
        }
    }


    streamConf.pageBreaks.clear();
    for (int probeIndex = 0; probeIndex < channelsOn_allProbes.length(); probeIndex++) {

        if (channelsOn_allProbes.at(probeIndex).length() != colors.at(probeIndex).length()) {
            return false;
        }

        totalNumProbesDefined = probeIndex;
        QVector<bool> channelsOn = channelsOn_allProbes.at(probeIndex);

        //channelsOn is a 960-length vector of boolean values, which defines which of the 960 pads are on
        for (int chInd = channelsOn.length()-1; chInd >= 0; chInd--) {

            if (channelsOn.at(chInd)) {
                int switchNum = chInd%384; //each probe switch can connect to one of multiple pads. This converts the pad index to it's switch
                int tmpHardwareNum = 0;

                //Search the packet order for where the switch's data is
                for (int s = 0; s < packetOrder.length(); s++) {
                    if (packetOrder.at(s) == switchNum) {
                        tmpHardwareNum = s; //This is the order for a single probe
                        //if we
                        break;
                    }
                }

                //qDebug() << "Probe:" << probeIndex << "Channel:" << chInd << "pHarwarenum:" << tmpHardwareNum << "Global HW:" << tmpHardwareNum+(384*totalNumProbesDefined);


                int ADCgroup = tmpHardwareNum/32;
                //int ADCchan = tmpHardwareNum % 12;

                //If there are multiple probes connected, then the channel order intermingles the channels like this:
                //PROBE 0 <first channel from all 32 ADC's> PROBE 1 <first channel from all 32 ADCs> PROBE 0 <second channel from all 32 ADCs> ...
                //We have already found the order for one probe, but now we need to deal with the multi-probe ordering
                tmpHardwareNum = tmpHardwareNum+(ADCgroup*(channelsOn_allProbes.length()-1)*32)+(probeIndex*32);

                int currentNtrodeID = chInd+((totalNumProbesDefined+1)*1000)+1; //The first probe is in the 1000's, the second in the 2000's, etc

                /*DeviceChannel newDeviceChannel;
                newDeviceChannel.idString = QString("LFP_%1").arg(currentNtrodeID);
                newDeviceChannel.dataType = DeviceChannel::INT16TYPE;
                newDeviceChannel.startByte = 2+(ADCgroup)+(totalNumProbesDefined*32)+lfpDevice.byteOffset;
                newDeviceChannel.digitalBit = 0; //not used since it is an int16 value
                newDeviceChannel.input = true;

                if (ADCchan < 8) {
                    newDeviceChannel.interleavedDataIDByte = 1+lfpDevice.byteOffset;
                } else {
                    newDeviceChannel.interleavedDataIDByte = 0+lfpDevice.byteOffset;
                }

                newDeviceChannel.interleavedDataIDBit = ADCchan % 8;
                lfpDevice.channels.append(newDeviceChannel);*/



                //Add a 1-channel nTrode to the SpikeConfiguration section.
                //spikeConf.ntrodes.append(currentNtrodeID, new SingleSpikeTrodeConf);
                spikeConf.ntrodes.append(currentNtrodeID);                           


                //default view mode
                spikeConf.ntrodes[numChannelsConfigured]->spikeViewMode = true;
                spikeConf.ntrodes[numChannelsConfigured]->lfpViewMode = false;
                spikeConf.ntrodes[numChannelsConfigured]->stimViewMode = false;

                //User-facing hardware channel (for convenience, since the actual order may be scrambled)
                spikeConf.ntrodes[numChannelsConfigured]->unconverted_hw_chan.push_back(tmpHardwareNum);


                //spikeConf.ntrodes[numChannelsConfigured]->hw_chan.append(tmpHardwareNum+(384*totalNumProbesDefined));
                spikeConf.ntrodes[numChannelsConfigured]->hw_chan.append(tmpHardwareNum);
                spikeConf.ntrodes[numChannelsConfigured]->stimCapable.append(false);
                spikeConf.ntrodes[numChannelsConfigured]->spikeSortingGroup.append(probeIndex);

                if (channelLocations.length() == channelsOn.length()) {
                    spikeConf.ntrodes[numChannelsConfigured]->coord_dv.append(probeLocations.at(probeIndex).at(2) + channelLocations.at(chInd).y());
                    spikeConf.ntrodes[numChannelsConfigured]->coord_ml.append(probeLocations.at(probeIndex).at(1) + channelLocations.at(chInd).x());
                    spikeConf.ntrodes[numChannelsConfigured]->coord_ap.append(probeLocations.at(probeIndex).at(0) + 0);
                } else {
                    spikeConf.ntrodes[numChannelsConfigured]->coord_dv.append(0);
                    spikeConf.ntrodes[numChannelsConfigured]->coord_ml.append(0);
                    spikeConf.ntrodes[numChannelsConfigured]->coord_ap.append(0);
                }

                //User facing ID (displayed in the GUI)
                //spikeConf.ntrodes[numChannelsConfigured]->nTrodeId = chInd+(totalNumProbesDefined*960)+1;
                spikeConf.ntrodes[numChannelsConfigured]->nTrodeId = currentNtrodeID; //The first probe is in the 1000's, the second in the 2000's, etc


                spikeConf.ntrodes[numChannelsConfigured]->maxDisp.append(400);
                spikeConf.ntrodes[numChannelsConfigured]->thresh.append(60);

                spikeConf.ntrodes[numChannelsConfigured]->spike_scaling_to_uV = (1000.0/(double)spikeGains.at(probeIndex))*(600/32767.0);
                spikeConf.ntrodes[numChannelsConfigured]->lfp_scaling_to_uV = (1000.0/(double)lfpGains.at(probeIndex))*(600/32767.0);
                spikeConf.ntrodes[numChannelsConfigured]->raw_scaling_to_uV = (1000.0/(double)spikeGains.at(probeIndex))*(600/32767.0);


                //The threshold is converted to uV
                //spikeConf.ntrodes[numChannelsConfigured]->thresh_rangeconvert.push_back((60 * 65536) / AD_CONVERSION_FACTOR);
                spikeConf.ntrodes[numChannelsConfigured]->thresh_rangeconvert.push_back(60 * (1/spikeConf.ntrodes[numChannelsConfigured]->spike_scaling_to_uV));

                //spikeConf.ntrodes[numChannelsConfigured]->streamingChannelLookup.append(400); //what is this for?

                spikeConf.ntrodes[numChannelsConfigured]->triggerOn.append(true);
                spikeConf.ntrodes[numChannelsConfigured]->lowFilter = 600;
                spikeConf.ntrodes[numChannelsConfigured]->highFilter = 6000;
                spikeConf.ntrodes[numChannelsConfigured]->color = colors.at(probeIndex).at(chInd);
                //spikeConf.ntrodes[numChannelsConfigured]->color = QColor(255,0,0);
                spikeConf.ntrodes[numChannelsConfigured]->refNTrode = 0;
                spikeConf.ntrodes[numChannelsConfigured]->refNTrodeID = 1;
                spikeConf.ntrodes[numChannelsConfigured]->refOn = false;
                spikeConf.ntrodes[numChannelsConfigured]->refChan = 0;
                spikeConf.ntrodes[numChannelsConfigured]->lfpDataChan = 0;
                spikeConf.ntrodes[numChannelsConfigured]->lfpHighFilter = 200;
                spikeConf.ntrodes[numChannelsConfigured]->lfpLowFilter = 0;
                spikeConf.ntrodes[numChannelsConfigured]->filterOn = true;
                spikeConf.ntrodes[numChannelsConfigured]->moduleDataOn = false;
                spikeConf.ntrodes[numChannelsConfigured]->lfpRefOn = false;
                spikeConf.ntrodes[numChannelsConfigured]->lfpFilterOn = false;
                spikeConf.ntrodes[numChannelsConfigured]->rawRefOn = false;
                spikeConf.ntrodes[numChannelsConfigured]->groupRefOn = false;
                spikeConf.ntrodes[numChannelsConfigured]->refGroup = 0;
                spikeConf.ntrodes[numChannelsConfigured]->notchFreq = 60.0;
                spikeConf.ntrodes[numChannelsConfigured]->notchBW = 10.0;
                spikeConf.ntrodes[numChannelsConfigured]->notchFilterOn = false;


                numChannelsConfigured++;
            }



        }
        streamConf.pageBreaks.push_back(numChannelsConfigured);
    }

    //qDebug() << "Number chan configured:" << numChannelsConfigured;

    hardwareConf.NCHAN = 384*channelsOn_allProbes.length();
    //Set the number of bytes for the LFP section
    lfpDevice.numBytes = (64*channelsOn_allProbes.length()) + 2;

    if (hardwareConf.sysTimeIncluded) {
        //The system clock is always last, if defined to be included
        hardwareConf.devices.insert(hardwareConf.devices.length()-1,lfpDevice);
    } else {
        hardwareConf.devices.append(lfpDevice);
    }

    //Calculate the total length of the section of the packet that contains aux data
    int tempAuxPacketSize = 1; //in bytes. //The fist byte is reserved for sync, so we start with 1

    for (int dNum = 0; dNum<hardwareConf.devices.length();dNum++) {
        if (hardwareConf.devices[dNum].available && hardwareConf.devices[dNum].name != "SysClock") {
            //
            tempAuxPacketSize = tempAuxPacketSize+hardwareConf.devices[dNum].numBytes;

        }
    }

    if ((tempAuxPacketSize % 2)!=0) {
        qDebug() << "Neuropixels workspace error: Total header size must be an even number of bytes";
        return false;
    }

    //Downstream code ussumes haederSize is in int16 steps. This will probably change.
    hardwareConf.headerSize = tempAuxPacketSize/2;

    streamConf.nColumns = 4;
    streamConf.nTabs = channelsOn_allProbes.length(); //one tab per probe
    streamConf.horizontalLayout = true; //Display channels like a book to match the selection interface




    //tLength = 1.0;

    streamConf.nChanConfigured = 0;

    //streamConf.backgroundColor.setNamedColor(eegDispConfNode.toElement().attribute("backgroundColor", "#808080")); //default color is gray (RGB in hex)

    streamConf.trodeIndexLookupByHWChan.clear();
    streamConf.trodeChannelLookupByHWChan.clear();

    for (int n = 0; n < hardwareConf.NCHAN; n++) {
        streamConf.trodeIndexLookupByHWChan.push_back(-1);
        streamConf.trodeChannelLookupByHWChan.push_back(-1);
    }
    for (int tmpTrodeIndex = 0; tmpTrodeIndex < spikeConf.ntrodes.length(); tmpTrodeIndex++) {
        for (int tmpChanNum = 0; tmpChanNum < spikeConf.ntrodes[tmpTrodeIndex]->maxDisp.length();
               tmpChanNum++) {

            //qDebug() << "NumNtrodes:" << spikeConf.ntrodes.length() << "HW" << spikeConf.ntrodes[tmpTrodeIndex]->hw_chan[tmpChanNum];
            streamConf.trodeIndexLookupByHWChan[spikeConf.ntrodes[tmpTrodeIndex]->hw_chan[tmpChanNum]] = tmpTrodeIndex;
            if (streamConf.trodeChannelLookupByHWChan[spikeConf.ntrodes[tmpTrodeIndex]->hw_chan[tmpChanNum]] != -1) {
                qDebug() << "Warning - Neuropixels HW channel" << spikeConf.ntrodes[tmpTrodeIndex]->hw_chan[tmpChanNum] << "is assigned more than once. NOT SUPPORTED!";


            }
            streamConf.trodeChannelLookupByHWChan[spikeConf.ntrodes[tmpTrodeIndex]->hw_chan[tmpChanNum]] = tmpChanNum;
        }
    }

    streamConf.setChanToSave(hardwareConf.NCHAN);

    return true;
}






/* --------------------------------------------------------------------- */

bool writeTrodesConfig(QString configFileName)
{
    QDomDocument doc;
    QDomElement root = doc.createElement("Configuration");

    doc.appendChild(root);

    /*QDomElement globalOptions = doc.createElement("GlobalOptions");
    root.appendChild(globalOptions);
    globalOptions.setAttribute("numChannels", hardwareConf->NCHAN);
    globalOptions.setAttribute("samplingRate", hardwareConf->sourceSamplingRate);
    globalOptions.setAttribute("hardwareConf->headerSize", hardwareConf->headerSize);
    globalOptions.setAttribute("globalConf->filePath", globalConf->filePath);
    globalOptions.setAttribute("globalConf->filePrefix", globalConf->filePrefix);*/
    globalConf->saveToXMLNoSessionData(doc, root);
    benchConfig->saveToXML(doc, root);
    hardwareConf->saveToXML(doc, root);
    moduleConf->saveToXML(doc, root);
    streamConf->saveToXML(doc, root);
    headerConf->saveToXML(doc, root);
    spikeConf->saveToXML(doc, root);

    QFile file(configFileName);
    //QFile file(QString("outConfig.xml"));
    //use QIODevice::Truncate to overwrite any previously existing file and QIODevice::Text to write newline characters using the local8bit encoding (cross platform diz)
    if (file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)) {
        QTextStream TextStream(&file);
        QString xmlString = doc.toString();
        //doc.save(TextStream, 0);
        QString vers = "<?xml version=\"1.0\"?>";
        TextStream << vers << Qt::endl << xmlString;
        file.close();
        return true;
    }
    else {
        return false;
    }
}

bool writeRecConfig(QString configFileName, quint32 currentTimestamp)
{
    QDomDocument doc;
    QDomElement root = doc.createElement("Configuration");

    doc.appendChild(root);

    /*QDomElement globalOptions = doc.createElement("GlobalOptions");
    root.appendChild(globalOptions);
    globalOptions.setAttribute("numChannels", hardwareConf->NCHAN);
    globalOptions.setAttribute("samplingRate", hardwareConf->sourceSamplingRate);
    globalOptions.setAttribute("hardwareConf->headerSize", hardwareConf->headerSize);
    globalOptions.setAttribute("globalConf->filePath", globalConf->filePath);
    globalOptions.setAttribute("globalConf->filePrefix", globalConf->filePrefix);*/
    globalConf->saveToXML(doc, root,currentTimestamp);
    hardwareConf->saveToXML(doc, root);
    moduleConf->saveToXML(doc, root);
    streamConf->saveToXML(doc, root);
    headerConf->saveToXML(doc, root);
    spikeConf->saveToXML(doc, root);

    QFile file(configFileName);
    //QFile file(QString("outConfig.xml"));

    if (file.open(QIODevice::WriteOnly)) {
        QTextStream TextStream(&file);
        QString xmlString = doc.toString();
        //doc.save(TextStream, 0);
        QString vers = "<?xml version=\"1.0\"?>";
        TextStream << vers << Qt::endl << xmlString;
        file.close();
        return true;
    }
    else {
        return false;
    }
}


//Main config file parser
/*int nsParseTrodesConfig(QString configFileName)
{
    QDomDocument doc("TrodesConf");
    QFile file;
    bool isRecFile = false;
    int filePos = 0;

    if (!configFileName.isEmpty()) {
        file.setFileName(configFileName);
        if (!file.open(QIODevice::ReadOnly)) {
            qDebug() << "[ParseTrodesConfig] Error:" << QString("File %1 not found").arg(configFileName);
            return -1;
        }
        qDebug() << "[ParseTrodesConfig] Loading from configuration file " << configFileName;
        QFileInfo fi(configFileName);
        QString ext = fi.suffix(); // ext = "gz"
        if (ext.compare("rec") == 0) {
            //this is a rec file with a configuration in the header
            isRecFile = true;
            QString configContent;
            QString configLine;
            bool foundEndOfConfig = false;

            while (!file.atEnd()) {
                configLine += file.readLine();
                configContent += configLine;
                if (configLine.indexOf("</Configuration>") > -1) {
                    //qDebug() << "End of config header found at " << file.pos();
                    foundEndOfConfig = true;
                    break;
                }
                configLine = "";
            }

            if (foundEndOfConfig) {
                filePos = file.pos();
                if (!doc.setContent(configContent)) {
                    file.close();
                    qDebug("[ParseTrodesConfig] Error: Config header didn't read properly.");
                    return -1;
                }
            }
        }
        else {
            //this is a normal xml config file
            if (!doc.setContent(&file)) {
                file.close();
                qDebug("[ParseTrodesConfig] Error: XML didn't read properly.");
                return -1;
            }
        }
    }
    file.close();
    QDomElement root = doc.documentElement();
    if (root.tagName() != "Configuration") {
        qDebug("[ParseTrodesConfig] Configuration not root node.");
        return -1;
    }
//    nTrodeTable = new NTrodeTable();

    // create the global configuration object and recreate the hardwareConf if it exists.
    globalConf = new GlobalConfiguration(NULL);
    globalConf->configfilepath = configFileName;

    //If there is a hardware conf around, we delete the old one after we make a new one.
    HardwareConfiguration* oldHardwareConf;
    if (hardwareConf != NULL) {
        oldHardwareConf = hardwareConf;
        hardwareConf = new HardwareConfiguration(NULL);
        delete oldHardwareConf;
    } else {
        hardwareConf = new HardwareConfiguration(NULL);
    }


    hardwareConf->sourceSamplingRate = 0;
    hardwareConf->headerSize = 0;
    hardwareConf->NCHAN = 0;
    //hardwareConf->numStimChan = 0;
#ifdef PHOTOMETRY_CODE
    hardwareConf->NFIBER = 0;
    hardwareConf->APDsamplingRate = 0;
#endif


    //Load global options.  Note that this is only for backwards compatibility; new files have a GlobalConfiguration section
    QDomNodeList globalOptions = root.elementsByTagName("GlobalOptions");
    if (globalOptions.length() == 1) {
        // load up the globalConf and hardwareConf options from this section
        QDomNode optionsNode = globalOptions.item(0);
        QDomElement optionElements = optionsNode.toElement();

        int tempNCHAN = optionElements.attribute("numChannels", "0").toInt();
        if (tempNCHAN > 0) {
            hardwareConf->NCHAN = tempNCHAN;
            if ((hardwareConf->NCHAN % 32) != 0) {
                qDebug() << "[ParseTrodesConfig] Error: numChannels must be a multiple of 32";
                return -6;
            }
//            qDebug() << "[ParseTrodesConfig] Number of channels: " << hardwareConf->NCHAN;
        }



        int tempSampRate = optionElements.attribute("samplingRate", "0").toInt();
        if (tempSampRate > 0) {
            hardwareConf->sourceSamplingRate = tempSampRate;
//            qDebug() << "[ParseTrodesConfig] Sampling rate: " << hardwareConf->sourceSamplingRate << " Hz";
        }

        int tempHeaderSize = optionElements.attribute("headerSize", "0").toInt();
        if (tempHeaderSize > 0) {
            hardwareConf->headerSize = tempHeaderSize;
//            qDebug() << "[ParseTrodesConfig] Header size: " << hardwareConf->headerSize;
            hardwareConf->headerSizeManuallyDefined = true;
        }

        globalConf->filePrefix = optionElements.attribute("filePrefix", "");
        globalConf->filePath = optionElements.attribute("filePath", "");
    }
    else {
        QDomNodeList globalConfigList = root.elementsByTagName("GlobalConfiguration");
        if (globalConfigList.length() > 1) {
            qDebug() << "[ParseTrodesConfig] Config file error: multiple GlobalConfiguration sections found in configuration file.";
            return -7;
        }
        QDomNode globalnode = globalConfigList.item(0);
        int globalErrorCode = globalConf->loadFromXML(globalnode);
        if (globalErrorCode != 0) {
            qDebug() << "[ParseTrodesConfig] Error: Failed to load global configuration err: " << globalErrorCode;
            return globalErrorCode;
        }
    }


    QDomNodeList benchmarkConfigList = root.elementsByTagName("BenchmarkConfiguration");
    //qDebug() << "@@@@ " << benchmarkConfigList.lenght() << " - " << benchmarkConfigList.count() << " - " << benchmarkConfigList.item(0)
    if (benchmarkConfigList.length()> 1) {
        //qDebug() << "@@@@@@@YEEE HAWWW, benchmarking enabled, hit em with some options!";
    }
    else {
        QDomNode benchNode = benchmarkConfigList.item(0);
        if (benchConfig == NULL) {
            //qDebug() << "@@@@@@making new bench config...";
            benchConfig = new BenchmarkConfig();
            benchConfig->resetDefaultFreq();
            benchConfig->resetDefaultBools();
           // qDebug() << "2 freq vals: " << benchConfig->getFreqStr();
        }
        //only load the benchmarking settings from the workspace if benchmarking was not initiated from the command line
        if (!benchConfig->wasInitiatedFromCommandLine() && !benchConfig->wasEditedByUser()) {
            //qDebug() << "@@@@@@bench config not ini from cmd line or by user, loading from workspace...";
            //qDebug() << " freq vals: " << benchConfig->getFreqStr();
            benchConfig->loadFromXML(benchNode);
        }
    }
    //TODO: add in error catch/throw message for incorrect benchmark config settings / failure to load


    //------------------------------------------------------------------

    QDomNodeList hardwareConfigList = root.elementsByTagName("HardwareConfiguration");
    if (hardwareConfigList.length() > 1) {
        qDebug() << "[ParseTrodesConfig] Error: Multiple HardwareConfiguration sections found in configuration file.";
        return -5;
    }
    if (hardwareConfigList.length() > 0) {
        QDomNode hardwarenode = hardwareConfigList.item(0);
        int hardwareErrorCode = hardwareConf->loadFromXML(hardwarenode);
        if (hardwareErrorCode != 0) {
            qDebug() << "[ParseTrodesConfig] Error: Failed to load hardware err: " << hardwareErrorCode;
            return hardwareErrorCode;
        }

    }
    else {
//        qDebug() << "[ParseTrodesConfig] No hardware configuration found.";
    }



    // PARSE NETWORK CONFIGURATION
    QDomNodeList networkConfigList = root.elementsByTagName("NetworkConfiguration");
    if (networkConfigList.length() > 1) {
        qDebug() << "[ParseTrodesConfig] Error: multiple NetworkConfiguration sections found in configuration file.";
        return -5;
    }

    networkConf = new NetworkConfiguration();
    if (networkConfigList.length() > 0) {
        QDomNode networknode = networkConfigList.item(0);
        int networkErrorCode = networkConf->loadFromXML(networknode);
        if (networkErrorCode != 0) {
            return networkErrorCode;
        }
        networkConf->networkConfigFound = true;

        if (networkConf->trodesHost != "") {
            qDebug() << "[ParseTrodesConfig] Network configured, trodesHost = " << networkConf->trodesHost;
        }
        else {
            qDebug() << "[ParseTrodesConfig] Network configured, trodesHost not specified";
        }
    }
    else {
//        qDebug() << "[ParseTrodesConfig] No network configuration found; using default hardware address" << TRODESHARDWARE_DEFAULTIP;
        networkConf->hardwareAddress = TRODESHARDWARE_DEFAULTIP;

        // TO DO: find the trodes hardware if it's address wasn't specified
    }

    // set the fixed port values
    networkConf->trodesPort = TRODESHARDWARE_CONTROLPORT;
    networkConf->ecuDirectPort = TRODESHARDWARE_ECUDIRECTPORT;
    networkConf->hardwarePort = TRODESHARDWARE_CONTROLPORT;

    if (networkConf->trodesHost == "") {
        // assume localhost
        networkConf->trodesHost = "127.0.0.1";
    }

    if ((networkConf->networkConfigFound) == false || (networkConf->dataSocketType == 0)) {
        // set to TCPIP if this was not specified
        networkConf->dataSocketType = TRODESSOCKETTYPE_TCPIP;
//        qDebug() << "[ParseTrodesConfig] TCPIP data sockets selected";
    }

    // PARSE MODULE CONFIGURATION
    QDomNodeList moduleConfigList = root.elementsByTagName("ModuleConfiguration");
    if (moduleConfigList.length() > 1) {
        qDebug() << "[ParseTrodesConfig] Error: multiple ModuleConfig sections found in configuration file.";
        return -5;
    }

    moduleConf = new ModuleConfiguration(NULL);
    if (moduleConfigList.length() > 0) {
        moduleConf->trodesConfigFileName = configFileName;
        QDomNode modulenode = moduleConfigList.item(0);
        int moduleErrorCode = moduleConf->loadFromXML(modulenode);
        if (moduleErrorCode != 0) {
            return moduleErrorCode;
        }
        qDebug() << "[ParseTrodesConfig]" << moduleConf->singleModuleConf.length() << "module(s) configured.";
    }
    else {
//        qDebug() << "[ParseTrodesConfig] No module configuration found.";
    }


#ifdef PHOTOMETRY_CODE
    // PARSE PHOTOMETRY CONFIGURATION
    QDomNodeList fiberList = root.elementsByTagName("PhotometryConfiguration");
    if (fiberList.length() > 1) {
        qDebug() << "Config file error: multiple PhotometryConfiguration sections found in configuration file.";
        return -5;
    } else if (fiberList.length() == 0) {
        qDebug() << "No photometry configuration found.";
    }

    if (fiberList.length() > 0) {
        QDomNode photometrynode = fiberList.item(0);
         photometryConf = new PhotometryConfiguration(NULL, photometrynode.childNodes().length());
        int photometryErrorCode = photometryConf->loadFromXML(photometrynode);
        if (photometryErrorCode != 0) {
            return photometryErrorCode;
        }
        qDebug() << photometryConf->nfibers.length() << " fibers configured.";
    }
#endif

    // PARSE HEADER CONFIGURATION
    //QDomNodeList headerList = root.elementsByTagName("HeaderDisplay");
    QDomNodeList headerList = root.elementsByTagName("AuxDisplayConfiguration");
    if (headerList.length() > 1) {
        qDebug() << "[ParseTrodesConfig] Error: multiple AuxDisplayConfiguration sections found in configuration file.";
        return -5;
    } else if (headerList.length() == 0) {
        //Backwards compatibility-- section used to be called 'HeaderDisplay'
        headerList = root.elementsByTagName("HeaderDisplay");
        if (headerList.length() > 1) {
            qDebug() << "[ParseTrodesConfig] Error: multiple HeaderDisplay sections found in configuration file.";
            return -5;
        }
    }
    headerConf = new headerDisplayConfiguration(NULL);
    if (headerList.length() > 0) {
        QDomNode headernode = headerList.item(0);
        //headerConf = new headerDisplayConfiguration(NULL);
        int headerErrorCode = headerConf->loadFromXML(headernode);
        if (headerErrorCode != 0) {
            return headerErrorCode;
        }
//        qDebug() << "[ParseTrodesConfig]" << headerConf->headerChannels.length() << " header display channels configured.";
    }
    else {
//        qDebug() << "[ParseTrodesConfig] No header display configuration found.";
    }



    // PARSE SPIKE CONFIGURATION
    QDomNodeList list = root.elementsByTagName("SpikeConfiguration");

    QDomNode spikenode = list.item(0);
    int autoNtrodeType = 0;
    autoNtrodeType = spikenode.toElement().attribute("autoPopulate", "0").toInt();
    if (autoNtrodeType > 0) {
        qDebug() << "[ParseTrodesConfig] Using auto population of nTrodes";
        int numTrodes = hardwareConf->NCHAN/autoNtrodeType;
        spikeConf = new SpikeConfiguration(NULL, numTrodes);
    } else {
        spikeConf = new SpikeConfiguration(NULL, spikenode.childNodes().length());
    }

    //      PARSE CAR group CONFIGURATION
    //      MUST BE BEFORE SPIKE CONF PARSING, SINCE SPIKE PARSING WILL MARK GROUPS AS USED OR NOT
    QDomNodeList carlist = root.elementsByTagName("CARGroup");
    for(int i = 0; i < carlist.length() && i < MAXCARGROUPS; ++i){
        QDomNodeList chans = carlist.item(i).childNodes();
        CARGroup group;
        group.useCount = 0;
        group.description = carlist.item(i).toElement().attribute("description");
        for(int j = 0; j < chans.length(); ++j){
            int ntrode = chans.item(j).toElement().attribute("ntrodeid").toInt();
            int chan = chans.item(j).toElement().attribute("chan").toInt();
            group.chans.append({ntrode, chan, -1});
        }
        spikeConf->carGroups.append(group);
    }

    // PARSE SPIKE CONF
    int spikeErrorCode = spikeConf->loadFromXML(spikenode);

    if (spikeErrorCode != 0) {
        return spikeErrorCode;
    }

    //Fill in hwchans in CAR groups
    for(int i = 0; i < spikeConf->carGroups.length(); ++i){
        for(int j = 0; j < spikeConf->carGroups[i].chans.length(); ++j){
            int ntrode = spikeConf->carGroups[i].chans[j].ntrodeid;
            int chan = spikeConf->carGroups[i].chans[j].chan;
            spikeConf->carGroups[i].chans[j].hw_chan = spikeConf->ntrodes.ID(ntrode)->hw_chan[chan-1];
        }
    }



    // PARSE STREAMING CONFIGURATION
    list = root.elementsByTagName("StreamDisplay");
    if (list.isEmpty() || list.length() > 1) {
        qDebug() << "[ParseTrodesConfig] Error: either no or multiple streamDisplay sections found in configuration file.";
        return -4;
    }
    QDomNode eegvisnode = list.item(0);
    streamConf = new streamConfiguration();
    int streamErrorCode = streamConf->loadFromXML(eegvisnode);
    if (streamErrorCode != 0) {
        return streamErrorCode;
    }


    qDebug() << "[ParseTrodesConfig] Configurations successfully loaded.";


    if (isRecFile) {
        return filePos; //return the position of the file where data begins
    }
    else {
        return 0;
    }
}*/

SingleSpikeTrodeConf::SingleSpikeTrodeConf() :
    nTrodeId(0),
    nTrodeIndex(0),
    refNTrode(-1),
    refNTrodeID(-1),
    refChan(0),
    refChanID(0),
    lowFilter(0),
    highFilter(0),
    lfpDataChan(0),
    lfpHighFilter(0),
    lfpLowFilter(0),
    refOn(0),
    filterOn(0),
    moduleDataOn(0),
    lfpRefOn(0),
    lfpFilterOn(0),
    notchFilterOn(0),
    notchFreq(60),
    notchBW(10),
    rawRefOn(0),
    refGroup(0),
    groupRefOn(0),
    lfp_scaling_to_uV(.195),
    spike_scaling_to_uV(.195),
    raw_scaling_to_uV(.195)
{
}

SingleSpikeTrodeConf::SingleSpikeTrodeConf(const SingleSpikeTrodeConf *s){
    this->nTrodeId = s->nTrodeId;
    this->nTrodeIndex = s->nTrodeIndex;
    this->refNTrode = s->refNTrode;
    this->refNTrodeID = s->refNTrodeID;
    this->refChan = s->refChan;
    this->refChanID = s->refChanID;
    this->lowFilter = s->lowFilter;
    this->highFilter = s->highFilter;
    this->lfpDataChan = s->lfpDataChan;
    this->lfpHighFilter = s->lfpHighFilter;
    this->lfpLowFilter = s->lfpLowFilter;
    this->refOn = s->refOn;
    this->filterOn = s->filterOn;
    this->moduleDataOn = s->moduleDataOn;
    this->lfpRefOn = s->lfpRefOn;
    this->rawRefOn = s->rawRefOn;
    this->lfpFilterOn = s->lfpFilterOn;
    this->notchFilterOn = s->notchFilterOn;
    this->notchFreq = s->notchFreq;
    this->notchBW = s->notchBW;
    this->groupRefOn = s->groupRefOn;

    this->channelSettings = s->channelSettings;

    this->tags = s->tags; this->tags.detach();
    this->gTags = s->gTags; this->gTags.detach();
    this->raw_scaling_to_uV = s->raw_scaling_to_uV;
    this->spike_scaling_to_uV = s->spike_scaling_to_uV;
    this->lfp_scaling_to_uV = s->lfp_scaling_to_uV;
    this->spikeViewMode = s->spikeViewMode;
    this->lfpViewMode = s->lfpViewMode;
    this->stimViewMode = s->stimViewMode;

    this->color = s->color;
    this->maxDisp = s->maxDisp; this->maxDisp.detach();
    this->thresh = s->thresh; this->thresh.detach();
    this->hw_chan = s->hw_chan; this->hw_chan.detach();
    this->unconverted_hw_chan = s->unconverted_hw_chan; this->unconverted_hw_chan.detach();
    this->thresh_rangeconvert = s->thresh_rangeconvert; this->thresh_rangeconvert.detach();
    this->streamingChannelLookup = s->streamingChannelLookup; this->streamingChannelLookup.detach();
    this->triggerOn = s->triggerOn; this->triggerOn.detach();
    this->stimCapable = s->stimCapable; this->stimCapable.detach();
    this->coord_ap = s->coord_ap; this->coord_ap.detach();
    this->coord_ml = s->coord_ml; this->coord_ml.detach();
    this->coord_dv = s->coord_dv; this->coord_dv.detach();
    this->spikeSortingGroup = s->spikeSortingGroup; this->spikeSortingGroup.detach();


}
SpikeConfStructure::SpikeConfStructure(int n){
    hash.reserve(n);
    list.reserve(n);
    nitems = n;
}

void SpikeConfStructure::setSize(int n) {
    hash.clear();
    hash.reserve(n);
    list.clear();
    list.reserve(n);
    nitems = n;

}

int SpikeConfStructure::size(){
    return list.size();
}
int SpikeConfStructure::length(){
    return list.length();
}
int SpikeConfStructure::index(int id){
    return hash[id]->nTrodeIndex;
}
bool SpikeConfStructure::isEmpty(){
    return hash.isEmpty() || list.isEmpty();
}

bool SpikeConfStructure::exists(int id){
    return hash[id] != nullptr;
}

/*
 //Defined inline in header
SingleSpikeTrodeConf* SpikeConfStructure::at(const int i){ //Access by index
    return list[i];
}
SingleSpikeTrodeConf* SpikeConfStructure::operator[](const int i){ //Access by index
    return list[i];
}
SingleSpikeTrodeConf* SpikeConfStructure::refOfIndex(int ind){ //Access the refNtrode object of the ntrode at index ind
    return list[list[ind]->refNTrode];
}
SingleSpikeTrodeConf* SpikeConfStructure::ID(const int i){ //Access by ID (may increase latency)
    return hash[i];
}
SingleSpikeTrodeConf* SpikeConfStructure::refOfID(int id){ //Access the refNtrode object of the ntrode with ID id (may increase latency)
    return list[hash[id]->refNTrode];
}*/

SingleSpikeTrodeConf* SpikeConfStructure::takeLast(){ //Removes last item from both and returns it
    //return hash.take(list.takeLast()->nTrodeId);
    return hash.take(list.takeLast().nTrodeId);
}

void SpikeConfStructure::clear(){ //Removes all items from containers
    hash.clear();
    list.clear();
}

void  SpikeConfStructure::copyFrom(const SpikeConfStructure& cs) {
    clear();
    hash.reserve(cs.list.length());
    list.reserve(cs.list.length());
    nitems = cs.list.length();
    for (int i=0;i<cs.list.length();i++) {
        SingleSpikeTrodeConf ntrodeconfCopy(cs.list[i]);
        append(cs.list.at(i).nTrodeId, &ntrodeconfCopy);
    }
}

void SpikeConfStructure::append(int id, SingleSpikeTrodeConf* ntrodeconf){
    SingleSpikeTrodeConf ntrodeconfCopy(ntrodeconf);
    ntrodeconfCopy.nTrodeIndex = list.size();
    list.append(ntrodeconfCopy);
    //hash.insert(id, ntrodeconf);
    hash.insert(id, &list.last());
}

void SpikeConfStructure::append(int id){
    SingleSpikeTrodeConf ntrodeconf;
    ntrodeconf.nTrodeIndex = list.size();
    list.append(ntrodeconf);
    //hash.insert(id, ntrodeconf);
    hash.insert(id, &list.last());
}


/*TrodesDataStream::TrodesDataStream() :
    QDataStream()
{
    setByteOrder(TrodesDataStream::LittleEndian);
}

TrodesDataStream::TrodesDataStream(QIODevice *d) :
    QDataStream(d)
{
    setByteOrder(TrodesDataStream::LittleEndian);
}

TrodesDataStream::TrodesDataStream(QByteArray *a, QIODevice::OpenMode mode) :
    QDataStream(a, mode)
{
    setByteOrder(TrodesDataStream::LittleEndian);
}

TrodesDataStream::TrodesDataStream(const QByteArray & a) :
    QDataStream(a)
{
    setByteOrder(TrodesDataStream::LittleEndian);
}*/



template <class T>
void avgCalculator<T>::insert(T obj) {
    data.append(obj);
    sum += obj;
    movAvgData.append(obj);
    movAvgSum += obj;
    if (movAvgData.length() > movAvgMaxSetSize) { //keep moving average set within the size we've specified
        T objToRemove = movAvgData.first();
        movAvgData.removeFirst();
        movAvgSum -= objToRemove;
    }
    if (obj < min || min == -1) {
        min = obj;
    }
    if (obj > max) {
        max = obj;
    }
}

//define all known types here that will be used in the future
template class avgCalculator<int>;
template class avgCalculator<float>;
template class avgCalculator<qreal>;

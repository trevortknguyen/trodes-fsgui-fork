#ifndef TRODESCENTRALSERVER_H
#define TRODESCENTRALSERVER_H
#include <networkincludes.h>
#include "src-config/hardwaresettings.h"
#include <QObject>
#include <QDebug>
#include <QString>

/*! \class TrodesCentralServer
 *  The TrodesCentralServer is a basic derivation of MlmWrap who's purpose is to act as the Trodes Main Server
 */
class TrodesCentralServer :public QObject, public MlmWrap {
Q_OBJECT
public:
    TrodesCentralServer(const char *id, const char *addr, int port);
    ~TrodesCentralServer(void);

    int start(void);

    void setModuleTimePtr(uint32_t *timeptr);
    void setModuleTimeRate(int rate);

    void sendConfigToModules(void);

    void setConfig(TrodesConfig config);

protected:

    int processCommandMsg(std::string cmdType, TrodesMsg &msg);
    int processEventMsg(const char *sender, const char *event, TrodesMsg &msg);
    int processRequestMsg(const char *sender, std::string reqType, TrodesMsg &msg);
    int processReplyMsg(std::string repType, TrodesMsg &msg);
    int processNotification(const char *sender, std::string noteType, TrodesMsg &msg);

private:

    uint32_t *currentTime;
    int sourceTimeRate;

    bool handleHardwareRequest(TrodesMsg &msg);
public slots:
    void initializeContinousStream(void);
    void initializeSpikeStream(void);


    void sendFileOpened(QString);
    void sendFileClose();
    void sendSourceConnect(int src);
    void sendRecordingStarted();
    void sendRecordingStopped();
    void sendPlaybackCommand(QString, uint32_t);
    void sendQuit();

    void provideEventSlot(QString event);
    void removeEventSlot(QString event);
    void registerNewContData(HighFreqDataType nData);
    void deregisterContData(HighFreqDataType dType);

    void ntrodeChanSelected(int ntrode, int chan, int hwchan);

    void sendQuitToModule(QString module);
    QVector<QString> getClientList();

    void newHeadstageSettings(HeadstageSettings hs);
    void newControllerSettings(HardwareControllerSettings mcu);
signals:
    void moduleCrashed(QString);
    void moduleClosed(QString);
    void moduleConnected(QString);
    void sig_acquisitionReceived(QString, quint32);
    void settleCommandTriggered();
    void trodesServerError(QString);

    void sendSetStimulationParams(StimulationCommand params); //TODO: add stimlationcommand obj
    void sendClearStimulationParams(uint16_t slot);
    void sendStimulationStartSlot(uint16_t slot);
    void sendStimulationStartGroup(uint16_t group);
    void sendStimulationStopSlot(uint16_t slot);
    void sendStimulationStopGroup(uint16_t group);
    void sendGlobalStimulationSettings(GlobalStimulationSettings settings);
    void sendGlobalStimulationCommand(GlobalStimulationCommand command);

    void annotationRequest(uint32_t timestamp, QString module, QString message);

    void sendECUShortcutMessage(uint16_t fn);
};

#endif // TRODESCENTRALSERVER_H

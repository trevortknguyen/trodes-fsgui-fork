#pragma once

/**
 * In trodesnetwork-v2, the main connections with TrodesNetwork will be exposed
 * through a module called `TrodesModule`. Unlike in v1, the term module
 * doesn't refer to anything specific other than a collection of endpoints all
 * managed by the same class lifecycle.
 * 
 * This class also establishes a server. Because the server is necessary for
 * other connections to establish, it is important that it exists so other
 * operations don't block while waiting for the server to come online.
 *
 * Note on Qt:
 *   This module is designed so it does not rely on Qt at all, but it is a
 *   QObject only so that it can be referenced within the slots and signals
 *   system for Qt. It is externally connected to a MainWindow so certain
 *   functions are automatically called or referenced.
 */

#include <QObject>


#include <TrodesNetwork/Server.h>
#include <TrodesNetwork/Resources/ServiceProvider.h>
#include <TrodesNetwork/Resources/SourcePublisher.h>
#include <TrodesNetwork/Generated/AcquisitionCommand.h>
#include <TrodesNetwork/Generated/AnnotationRequest.h>
#include <TrodesNetwork/Generated/Event.h>
#include <TrodesNetwork/Generated/InfoRequest.h>
#include <TrodesNetwork/Generated/InfoResponse.h>
#include <TrodesNetwork/Generated/SourceStatus.h>
#include <TrodesNetwork/Generated/FileStatus.h>



// for GlobalStimulationCommand, etc.
#include <trodesglobaltypes.h>
// for HardwareControllerSettings
#include <hardwaresettings.h>

#include <memory>
#include <string>

namespace trodes {

class TrodesModule : public QObject {
Q_OBJECT

public:
    TrodesModule(std::string address, int port);

    void setModuleTimePtr(uint32_t* ptr);
    void setModuleTimeRate(int32_t rate);
    void sendConfigToModules(void);
    void setConfig(TrodesConfig config);
    
    QVector<QString> getClientList();

    std::string getAddress();
    int getPort();


// slots are things that are triggered to run by another module
public slots:
    // acquisition-related from main window
    void sendFileOpened(QString);
    void sendFileClose();
    void sendSourceConnect(int src);
    void sendRecordingStarted();
    void sendRecordingStopped();
    void sendAcquisitionStarted();
    void sendAcquisitionStopped();
    void sendPlaybackCommand(QString, uint32_t);
    void sendQuit();

    // something
    void ntrodeChanSelected();

    // module stuff
    void sendQuitToModule();

    void newControllerSettings(HardwareControllerSettings mcu);

    // legacy stuff
    int provideEvent(const char *event);

    void registerNewContData(HighFreqDataType nData);
    void deregisterContData(HighFreqDataType dType);

    void provideEventSlot(QString event);
    void removeEventSlot(QString event);


// signals, each called by a service, trigger other modules, no definition
signals:
    // hardware-related with SourceController
    // managed by hw_service_thread
    void settleCommandTriggered();
    void sendSetStimulationParams(StimulationCommand params);
    void sendClearStimulationParams(uint16_t slot);
    void sendStimulationStartSlot(uint16_t slot);
    void sendStimulationStartGroup(uint16_t group);
    void sendStimulationStopSlot(uint16_t slot);
    void sendStimulationStopGroup(uint16_t group);
    void sendGlobalStimulationSettings(GlobalStimulationSettings settings);
    void sendGlobalStimulationCommand(GlobalStimulationCommand command);
    void sendECUShortcutMessage(uint16_t fn);

    // module status
    // TODO: should be managed by a directory_service_thread
    void moduleCrashed(QString);
    void moduleClosed(QString);
    void moduleConnected(QString);

    // annotation, with CommentDialog
    // managed by annotation_service_thread
    void annotationRequest(uint32_t timestamp, QString module, QString message);

    // acquisition-related
    // managed by acquisition_service_thread
    void sig_acquisitionReceived(QString, quint32);

    // file service
    // called happens when client requests .rec files to be opened and recorded
    void sig_rec_open(QString);
    void sig_rec_start();
    void sig_rec_pause();
    void sig_rec_close();

private:
    std::string address;
    int port;

    network::Server server;

    trodes::network::SourcePublisher<trodes::network::AcquisitionCommand> acqCmdPub;
    trodes::network::SourcePublisher<trodes::network::Event> eventPub;
    trodes::network::SourcePublisher<trodes::network::SourceStatus> sourceStatusPub;
    trodes::network::SourcePublisher<trodes::network::FileStatus> fileStatusPub;

    std::thread hw_service_thread;
    std::thread info_service_thread;
    std::thread annotation_service_thread;
    std::thread acquisition_service_thread;
    std::thread file_service_thread;
    std::thread event_service_thread;

    // stateful things
    uint32_t* timePtr;
    int32_t  sourceTimeRate;
};

}  // namespace trodes


#include "TrodesCentralServer.h"

TrodesCentralServer::TrodesCentralServer(const char* id, const char* addr, int port) :
    MlmWrap(id, addr, port),
    sourceTimeRate(30000)
{
//    provideEvent(hardware_update);
    qDebug() << "[TrodesNetwork] Network started at [" << getEndpoint().c_str() << "]";
}

TrodesCentralServer::~TrodesCentralServer() {
    qDebug() << "Closing central server";

}

int TrodesCentralServer::start() {
//    zclock_sleep(50);
    if(initialize() == -1){
        qDebug() << "Could not start trodescentralserver!";
    }
    //create trodes data streams
    setProducer(TRODES_NETWORK_ID);
    init_hardware_connection();
    return(0);
}

void TrodesCentralServer::setModuleTimePtr(uint32_t *timeptr){
    currentTime = timeptr;
}

void TrodesCentralServer::setModuleTimeRate(int rate){
    sourceTimeRate = rate;
}

void TrodesCentralServer::setConfig(TrodesConfig config){
    setTrodesConfig(config);
}

void TrodesCentralServer::sendConfigToModules() {
    if (getTrodesConfig().isValid()) {
        TrodesMsg configUpdate("sn", NOTE_CONFIG_CHANGED, getTrodesConfig().encode());
        sendMessageToAll(TRODES_NETWORK_NOTIFICATION, configUpdate);
    }
}

int TrodesCentralServer::processCommandMsg(std::string cmdType, TrodesMsg &msg) {
    int rc = 0;
    if (cmdType == acquisition_CMD) {
        std::string acqCmd;
        uint32_t time;
        msg.popcontents("s4", acqCmd, time);

        emit sig_acquisitionReceived(QString::fromStdString(acqCmd), time);
    }
    else {
        std::cout << "[TCS]: Command type [" << cmdType << "] not recognized.\n";
    }
    return(rc);
}

int TrodesCentralServer::processEventMsg(const char *sender, const char *event, TrodesMsg &msg) {
    int rc = 0;
    std::cout << "[TCS]: Event Message not recognized.\n";
    return(rc);
}

int TrodesCentralServer::processRequestMsg(const char *sender, std::string reqType, TrodesMsg &msg) {
    int rc = 0;
    if (reqType == INFO_TIME) {
        uint32_t time = *currentTime;
        TrodesMsg m("s4", INFO_TIME, time);
        sendMessage(sender, TRODES_INFO_REP, m);
    }
    else if (reqType == INFO_TIMERATE) {
        TrodesMsg m("si", INFO_TIMERATE, sourceTimeRate);
        sendMessage(sender, TRODES_INFO_REP, m);
    }
    else if (reqType == INFO_CONFIG) {
//        if (getTrodesConfig().isValid()) {
            TrodesMsg m("snn", INFO_CONFIG, getTrodesConfig().encode(), state->encode());
            sendMessage(sender, TRODES_INFO_REP, m);
//        }
    }
    else if (reqType == "HARDWARE"){
        if(handleHardwareRequest(msg)){
            msg.addstr("SUCCESS");
        }
        else{
            msg.addstr("FAILURE");
        }
    }
    else if (reqType == ANNOTATION_REQ) {
        uint32_t timestamp;
        std::string message;
        msg.popcontents("4s", timestamp, message);
        emit annotationRequest(timestamp, sender, QString::fromStdString(message));
    }
    else {
        std::cout << "[TCS]: Request type [" << reqType << "] not recognized.\n";
    }

    return(rc);
}

bool TrodesCentralServer::handleHardwareRequest(TrodesMsg &msg){
    //Trodes gets a hardware request to act on.
    //1. parse it and emit signal (make sure its a direct signal)
    //  - get first value (start    stop    set     clear)
    //  - get second value(  slot/group     set     clear)
    //  - get third value (slot/group num   stimcmd slot)
    std::string cmd = msg.popstr();
    bool success = false;
    if(cmd == "START" || cmd == "STOP"){
        msg.setformat("s2");
        std::string slotgroup = msg.popstr();
        uint16_t num=0;
        msg.popcontents("2", num);
        if(slotgroup == "SLOT" && cmd == "START"){
            emit sendStimulationStartSlot(num);
            success = true;
        }
        else if(slotgroup == "GROUP" && cmd == "START"){
            emit sendStimulationStartGroup(num);
            success = true;
        }
        else if(slotgroup == "SLOT" && cmd == "STOP"){
            emit sendStimulationStopSlot(num);
            success = true;
        }
        else if(slotgroup == "GROUP" && cmd == "STOP"){
            emit sendStimulationStopGroup(num);
            success = true;
        }
    }
    else if(cmd == "SET"){
        //TODO: get stimcommand object
        StimulationCommand params;
        binarydata paramsdata;
        msg.setformat("n");
        msg.popcontents("n", paramsdata);
        params.decode(paramsdata);
        emit sendSetStimulationParams(params);
        success = true;
    }
    else if(cmd == "CLEAR"){
        uint16_t num = 0;
        msg.setformat("2");
        msg.popcontents("2", num);
        emit sendClearStimulationParams(num);
        success = true;
    }
    else if(cmd == settle_CMD){
        emit settleCommandTriggered();
        success = true;
    }
    else if(cmd == "SETGS"){
        binarydata sdata;
        GlobalStimulationSettings s;
        msg.setformat("n");
        msg.popcontents("n", sdata);
        s.decode(sdata);
        emit sendGlobalStimulationSettings(s);
        success = true;
    }
    else if(cmd == "SETGC"){
        binarydata cdata;
        GlobalStimulationCommand c;
        msg.setformat("n");
        msg.popcontents("n", cdata);
        c.decode(cdata);
        emit sendGlobalStimulationCommand(c);
        success = true;
    }
    else if(cmd == "SCTRIG"){
        uint16_t fn = 0;
        msg.setformat("2");
        msg.popcontents("2", fn);
        emit sendECUShortcutMessage(fn);
        success = true;
    }
    return success && msg.numContents() == 0;
}

int TrodesCentralServer::processReplyMsg(std::string repType, TrodesMsg &msg) {
    int rc = 0;
    std::cout << "[TCS]: Reply type [" << repType << "] not recognized.\n";
    return(rc);
}

int TrodesCentralServer::processNotification(const char *sender, std::string noteType, TrodesMsg &msg) {
    int rc = 0;
    if (!strcmp(sender, BROKER_NETWORK_ID)) {
        if (noteType == CLIENT_EXPIRED_MSG) { //Client has improperly disconnected from broker
            std::string who = msg.popstr();
            emit moduleCrashed(who.c_str());
        }
        else if(noteType == CLIENT_DISCONNECT_MSG) {
            std::string who = msg.popstr();
            emit moduleClosed(who.c_str());
        }
        else if(noteType == CLIENT_CONNECT_MSG) {
            std::string who = msg.popstr();
            emit moduleConnected(who.c_str());
        }
    }
    else {
        std::cout << "[TCS]: Notification type [" << noteType << "] not recognized.\n";
    }

    return(rc);
}

//*************************************************************
//Slots
//*************************************************************

void TrodesCentralServer::initializeContinousStream() {
    qDebug() << "Setting up continuous streaming socks";
}

void TrodesCentralServer::initializeSpikeStream() {
    qDebug() << "Setting up spike streaming socks";
}


void TrodesCentralServer::sendFileOpened(QString f){
    TrodesMsg msg;
    msg.addcontents("sss", file_CMD, file_OPEN, f.toStdString().c_str());
    sendStream(TRODES_CMD, msg);
    state->setTrodes_filename(f.toStdString());
}

void TrodesCentralServer::sendFileClose(){
    TrodesMsg msg;
    msg.addcontents("ss", file_CMD, file_CLOSE);
    sendStream(TRODES_CMD, msg);
    state->setTrodes_filename("");
}

void TrodesCentralServer::sendSourceConnect(int src){
    TrodesSource source = (TrodesSource)src;
    state->setTrodes_source(source);
    TrodesMsg msg;
    msg.addcontents("si", source_CMD, src);
    sendStream(TRODES_CMD, msg);
}

void TrodesCentralServer::sendRecordingStarted(){
    TrodesMsg msg;
    msg.addcontents("ss4", acquisition_CMD, acq_RECORD, *currentTime);
    sendStream(TRODES_CMD, msg);
    state->setTrodes_acquisition(Play);
}

void TrodesCentralServer::sendRecordingStopped(){
    TrodesMsg msg;
    msg.addcontents("ss4", acquisition_CMD, acq_STOPRECORD, *currentTime);
    sendStream(TRODES_CMD, msg);
    state->setTrodes_acquisition(Stop);
}

void TrodesCentralServer::sendPlaybackCommand(QString cmd, uint32_t t){
    TrodesMsg msg;
    msg.addcontents("ss4", acquisition_CMD, cmd.toStdString().c_str(), t);
    sendStream(TRODES_CMD, msg);

}

void TrodesCentralServer::sendQuit(){
    TrodesMsg msg;
    msg.addcontents("s", quit_CMD);
    sendStream(TRODES_CMD, msg);
}


void TrodesCentralServer::provideEventSlot(QString event){
    provideEvent(event.toStdString().c_str());
}

void TrodesCentralServer::removeEventSlot(QString event){
    unprovideEvent(event.toStdString().c_str());
}
void TrodesCentralServer::registerNewContData(HighFreqDataType nData) {
    if(nData.getName() == TIMESTAMPS_SOCK){
        state->setTimestamp_endpoint(nData.getSockAddr());
        TrodesMsg msg;
        msg.addcontents("ss", "TIMESTAMP_ADDRESS", state->getTimestamp_endpoint());
        sendStream(TRODES_NETWORK_NOTIFICATION, msg);
//        return;
    }
//    qDebug() << "Registering new data [" << nData.getName().c_str() << "] [" << nData.getSockAddr().c_str() << "] [" << nData.getDataFormat().c_str() << "] [" << nData.getByteSize() << " bytes]";
    registerHighFreqData(nData);
}

void TrodesCentralServer::deregisterContData(HighFreqDataType dType) {
    if(dType.getName() == TIMESTAMPS_SOCK){
        state->setTimestamp_endpoint("");
        TrodesMsg msg;
        msg.addcontents("s", "TIMESTAMP_ADDRESS_DEREG");
        sendStream(TRODES_NETWORK_NOTIFICATION, msg);
//        return;
    }
//    qDebug() << "TCS:  Deregistering data type [" << dType.getName().c_str() << "]";
    deregisterHighFreqData(dType);
}

void TrodesCentralServer::ntrodeChanSelected(int ntrode, int chan, int hwchan){
    TrodesMsg msg("iii", ntrode, chan, hwchan);
    sendEvent("NtrodeChanSelect", msg);
}

void TrodesCentralServer::sendQuitToModule(QString module){
    TrodesMsg msg;
    msg.addcontents("s", quit_CMD);
//    sendStream(TRODES_CMD, msg);
    sendMessage(module.toStdString().c_str(), TRODES_CMD, msg);
}

QVector<QString> TrodesCentralServer::getClientList(){
    auto c = getClients();
    QVector<QString> clients;
    for(auto const &cl : c){
//        if(cl != TRODES_NETWORK_ID)
            clients.append(QString::fromStdString(cl));
    }
    return clients;
}

void TrodesCentralServer::newHeadstageSettings(HeadstageSettings hs)
{

}

void TrodesCentralServer::newControllerSettings(HardwareControllerSettings mcu)
{

}

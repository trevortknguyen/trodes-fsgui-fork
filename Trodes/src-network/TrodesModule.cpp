#include <TrodesModule.h>


#include <TrodesNetwork/Resources/ServiceProvider.h>
#include <TrodesNetwork/Resources/SinkSubscriber.h>
#include <TrodesNetwork/Resources/SourcePublisher.h>
#include <TrodesNetwork/Generated/AcquisitionCommand.h>
#include <TrodesNetwork/Generated/AnnotationRequest.h>
#include <TrodesNetwork/Generated/Event.h>
#include <TrodesNetwork/Generated/FileCommand.h>
#include <TrodesNetwork/Generated/HardwareRequest.h>
#include <TrodesNetwork/Generated/InfoRequest.h>
#include <TrodesNetwork/Generated/InfoResponse.h>
#include <TrodesNetwork/Generated/FileStatus.h>
#include <TrodesNetwork/Generated/TrodesEvent.h>



// for GlobalStimulationCommand, etc.
#include <trodesglobaltypes.h>
// for HardwareControllerSettings
#include <hardwaresettings.h>

#include <memory>
#include <variant>

#include <QVector>

namespace trodes {

TrodesModule::TrodesModule(std::string address, int port)
      : address(address)
      , port(port)
      , server(address, port)
      , acqCmdPub(address, port, "trodes.acquisition")
      , eventPub(address, port, "trodes.events")
      , sourceStatusPub(address, port, "trodes.source.pub")
      , fileStatusPub(address, port, "trodes.file.pub") {

        std::shared_ptr<TrodesModule> trodesModule(this);


        // info service (time, timerate, config)
        info_service_thread = std::thread([ centralServer = trodesModule ]() {
            // change this to InfoRequest (type specified by string)
            // change this to InfoResponse
            trodes::network::ServiceProvider<trodes::network::InfoRequest, trodes::network::InfoResponse> service(centralServer->getAddress(), centralServer->getPort(), "trodes.info");
            while (true) {
                service.handle([ centralServer ](trodes::network::InfoRequest input) -> trodes::network::InfoResponse {
                    if (input.request == "time") {
                        // TIME
                        uint32_t time = *centralServer->timePtr;
                        trodes::network::ITime data{time};
                        // TrodesMsg m("s4", INFO_TIME, time);
                        // trodes::network::InfoResponse result = trodes::network::IRTime{time};
                        trodes::network::InfoResponse result = trodes::network::IRTime{data};

                        return result;
                    } else if (input.request == "timerate") {
                        // TIMERATE
                        int32_t timeRate = centralServer->sourceTimeRate;

                        //auto timeRate = -1;

                        trodes::network::ITimerate data{timeRate};
                        // TrodesMsg m("si", INFO_TIMERATE, sourceTimeRate);
                        // sendMessage(sender, TRODES_INFO_REP, m);
                        // trodes::network::InfoResponse result = trodes::network::IRTimerate{sourceTimeRate};
                        trodes::network::InfoResponse result = trodes::network::IRTimerate{data};

                        return result;
                    } else if (input.request == "config") {
                        // CONFIG
                        // TrodesConfig config;
                        // network_pimpl


                        // TrodesConfig from networkDataTypes.cpp
                        // devices :: [NDevice]
                        // nTrodes :: [NTrodeObj]
                        /*
                        NetworkDataType::deserializedata(data, devices, nTrodes);
                        */

                        // network_pimpl from networkincludes.cpp
                        //
                        /*
                        std::string endpoint;
                        std::string address;
                        int port;
                        std::set<std::string> clients;

                        std::vector<EventDataType>      events;
                        std::vector<HighFreqDataType>   hfdts;

                        TrodesSource                    trodes_source;
                        TrodesAcquisition               trodes_acquisition;
                        std::string                     trodes_filename;

                        std::string                     hardware_endpoint;
                        std::string                     hardware_address;
                        int                             hardware_port;

                        std::string                     timestamp_endpoint;

                        int tsource, tacq;
                        NetworkDataType::deserializedata(data, endpoint, address, port, clients, events, hfdts,
                                                         tsource, tacq, trodes_filename,
                                                         hardware_endpoint, hardware_address, hardware_port,
                                                         timestamp_endpoint);
                        trodes_source = (TrodesSource)tsource;
                        trodes_acquisition = (TrodesAcquisition)tacq;*/


                        // TrodesMsg m("snn", INFO_CONFIG, getTrodesConfig().encode(), state->encode());
                        // sendMessage(sender, TRODES_INFO_REP, m);
                        // trodes::network::InfoResponse result = trodes::network::IRTrodesConfig{getTrodesConfig(), state};
                        trodes::network::InfoResponse result = trodes::network::IRTrodesConfig();
                        return result;
                    } else {
                        throw "incomplete pattern matching";
                    }
                });
            }
        });
        info_service_thread.detach();

        // hardware service (set, stop, clear, etc.)
        hw_service_thread = std::thread([ centralServer = trodesModule ]() {
            trodes::network::ServiceProvider<trodes::network::HardwareRequest, std::string> service(centralServer->getAddress(), centralServer->getPort(), "trodes.hardware");
            while (true) {
                service.handle([ centralServer ](trodes::network::HardwareRequest input) -> std::string {
                    auto response = std::visit([ centralServer, input ](auto&& arg) {
                            using T = std::decay_t<decltype(arg)>;
                            if constexpr (std::is_same_v<T, trodes::network::HRStartStopCommand>) {
                                trodes::network::HWStartStop data = std::get<trodes::network::HRStartStopCommand>(input).a;

                                std::string cmd = data.startstop;
                                std::string slotgroup = data.slotgroup;
                                uint16_t num = data.number;

                                if(slotgroup == "SLOT" && cmd == "START"){
                                    centralServer->sendStimulationStartSlot(num);
                                    return "success";
                                }
                                else if(slotgroup == "GROUP" && cmd == "START"){
                                    centralServer->sendStimulationStartGroup(num);
                                    return "success";
                                }
                                else if(slotgroup == "SLOT" && cmd == "STOP"){
                                    centralServer->sendStimulationStopSlot(num);
                                    return "success";
                                }
                                else if(slotgroup == "GROUP" && cmd == "STOP"){
                                    centralServer->sendStimulationStopGroup(num);
                                    return "success";
                                } else {
                                    return "failure: message not understood";
                                }
                            } else if constexpr (std::is_same_v<T, trodes::network::HRSet>) {
                                trodes::network::HWStimulationCommand data =
                                    std::get<trodes::network::HRSet>(input).a;
                                StimulationCommand params(data);
                                centralServer->sendSetStimulationParams(params);
                                return "success";
                            } else if constexpr (std::is_same_v<T, trodes::network::HRClear>) {
                                trodes::network::HWClear data =
                                    std::get<trodes::network::HRClear>(input).a;
                                uint16_t num = data.number;
                                centralServer->sendClearStimulationParams(num);
                                return "success";
                            } else if constexpr (std::is_same_v<T, trodes::network::HRSettle>) {
                                centralServer->settleCommandTriggered();
                                return "success";
                            } else if constexpr (std::is_same_v<T, trodes::network::HRSetGS>) {
                                trodes::network::HWGlobalStimulationSettings data =
                                    std::get<trodes::network::HRSetGS>(input).a;
                                GlobalStimulationSettings s(data);
                                centralServer->sendGlobalStimulationSettings(s);
                                return "success";
                            } else if constexpr (std::is_same_v<T, trodes::network::HRSetGC>) {
                                trodes::network::HWGlobalStimulationCommand data = std::get<trodes::network::HRSetGC>(input).a;
                                GlobalStimulationCommand c(data);
                                centralServer->sendGlobalStimulationCommand(c);
                                return "success";
                            } else if constexpr (std::is_same_v<T, trodes::network::HRSCTrig>) {
                                trodes::network::HWTrigger data = std::get<trodes::network::HRSCTrig>(input).a;
                                uint16_t fn = data.fn;
                                centralServer->sendECUShortcutMessage(fn);
                                return "success";
                            } else {
                                return "failure: unrecognized type";
                            }
                        },
                        input
                    );

                    return response;
                });
            }
        });
        hw_service_thread.detach();

        annotation_service_thread = std::thread([ centralServer = trodesModule ]() {
            trodes::network::ServiceProvider<trodes::network::AnnotationRequest, std::string> service(centralServer->getAddress(), centralServer->getPort(), "trodes.annotation");
            while (true) {
                service.handle([ centralServer ](trodes::network::AnnotationRequest input) -> std::string {
                    uint32_t timestamp = input.timestamp;
                    std::string sender = input.sender;
                    std::string message = input.event;
                    centralServer->annotationRequest(timestamp, sender.c_str(), message.c_str());
                    return "success";
                });
            }
        });
        annotation_service_thread.detach();

        acquisition_service_thread = std::thread([ centralServer = trodesModule ]() {
            trodes::network::ServiceProvider<trodes::network::AcquisitionCommand, std::string> service(centralServer->getAddress(), centralServer->getPort(), "trodes.acquisition.service");
            while (true) {
                service.handle([ centralServer ](trodes::network::AcquisitionCommand input) -> std::string {
                    std::string acqCmd = input.command;
                    uint32_t time = input.timestamp;
                    centralServer->sig_acquisitionReceived(acqCmd.c_str(), time);
                    return "success";
                });
            }
        });
        acquisition_service_thread.detach();

        file_service_thread = std::thread([ centralServer = trodesModule ]() {
            trodes::network::ServiceProvider<trodes::network::FileCommand, std::string> service(centralServer->getAddress(), centralServer->getPort(), "trodes.file.service");
            while (true) {
                service.handle([ centralServer ](trodes::network::FileCommand input) -> std::string {
                    if (input.command == "open") {
                        centralServer->sig_rec_open(input.filename.c_str());
                        return "success";
                    }

                    if (input.command == "start") {
                        centralServer->sig_rec_start();
                        return "success";
                    }

                    if (input.command == "pause") {
                        centralServer->sig_rec_pause();
                        return "success";
                    }

                    if (input.command == "close") {
                        centralServer->sig_rec_close();
                        return "success";
                    }

                    return "error";
                });
            }
        });
        file_service_thread.detach();

        event_service_thread = std::thread([ centralServer = trodesModule ]() {
            trodes::network::SinkSubscriber<trodes::network::TrodesEvent> sub(centralServer->getAddress(), centralServer->getPort(), "trodes.event.inbox");
            trodes::network::SourcePublisher<trodes::network::TrodesEvent> pub(centralServer->getAddress(), centralServer->getPort(), "trodes.event");

            while (true) {
              auto msg = sub.receive();
              pub.publish(msg);
            }
        });
        event_service_thread.detach();
    }

// called by service

void TrodesModule::sendFileOpened(QString f){
    qDebug("sending file open");
    // send via trodesnetwork-v2
    trodes::network::FileStatus fileStatus = {"open", f.toStdString()};
    fileStatusPub.publish(fileStatus);
}

void TrodesModule::sendFileClose(){
    qDebug("sending file close");
    // send via trodesnetwork-v2
    trodes::network::FileStatus fileStatus = {"close", ""};
    fileStatusPub.publish(fileStatus);
}

void TrodesModule::sendSourceConnect(int src){
    network::SourceStatus status{"connect"};
    sourceStatusPub.publish(status);
}

void TrodesModule::sendRecordingStarted(){
    // send via trodesnetwork-v2
    trodes::network::AcquisitionCommand acqCmdData = {acq_RECORD, *timePtr};
    acqCmdPub.publish(acqCmdData);
}

void TrodesModule::sendRecordingStopped(){
    // send via trodesnetwork-v2
    trodes::network::AcquisitionCommand acqCmdData = {acq_STOPRECORD, *timePtr};
    acqCmdPub.publish(acqCmdData);
}

void TrodesModule::sendPlaybackCommand(QString cmd, uint32_t t){
    // send via trodesnetwork-v2
    trodes::network::AcquisitionCommand acqCmdData = {cmd.toStdString(), t};
    acqCmdPub.publish(acqCmdData);
}

// can be for streaming or file playback
void TrodesModule::sendAcquisitionStarted() {
    // send via trodesnetwork-v2
    trodes::network::AcquisitionCommand acqCmdData = {acq_PLAY, *timePtr};
    acqCmdPub.publish(acqCmdData);
}

void TrodesModule::sendAcquisitionStopped() {
    // send via trodesnetwork-v2
    trodes::network::AcquisitionCommand acqCmdData = {acq_STOP, *timePtr};
    acqCmdPub.publish(acqCmdData);
}

// other slots
//

void TrodesModule::ntrodeChanSelected() {
    trodes::network::Event eventData = {"NtrodeChanSelect"};
    eventPub.publish(eventData);
}

// module stuff
void TrodesModule::sendQuitToModule() {
    qDebug() << "sendQuitToModule() is a no-op.";
}

void TrodesModule::newControllerSettings(HardwareControllerSettings mcu)
{
    // the original TrodesNetwork implementation left this method empty
    std::cerr << "TrodesModule::newControllerSettings()" << std::endl;
}

// setting state
//

void TrodesModule::setModuleTimePtr(uint32_t* ptr) {
    timePtr = ptr;
}

void TrodesModule::setModuleTimeRate(int32_t rate) {
    sourceTimeRate = rate;
}

std::string TrodesModule::getAddress() {
    return server.address;
}

int TrodesModule::getPort() {
    return server.port;
}

void TrodesModule::sendConfigToModules(void) {
    qDebug() << "sendConfigToModules is a no-op.";
}

void TrodesModule::setConfig(TrodesConfig config) {
    qDebug() << "setConfig is a no-op.";
}

void TrodesModule::sendQuit() {
    qDebug("sending quit to all");
    // send via trodesnetwork-v2
    trodes::network::FileStatus fileStatus = {"quit", ""};
    fileStatusPub.publish(fileStatus);

    // also notify subscribers to acquisition status
    trodes::network::AcquisitionCommand acqCmdData = {acq_STOP, *timePtr};
    acqCmdPub.publish(acqCmdData);
}

void TrodesModule::registerNewContData(HighFreqDataType nData) {
    qDebug() << "registerNewContData is a no-op.";
}

void TrodesModule::deregisterContData(HighFreqDataType dType) {
    qDebug() << "deregisterContData is a no-op.";
}

void TrodesModule::provideEventSlot(QString event) {
    qDebug() << "provideEventSlot is a no-op.";
}

void TrodesModule::removeEventSlot(QString event) {
    qDebug() << "removeEventSlot is a no-op.";
}

QVector<QString> TrodesModule::getClientList() {
    qDebug() << "getClientList is a no-op (returning empty list).";
    
    QVector<QString> vector;

    // needs to return something
    return vector;
}

int TrodesModule::provideEvent(const char *event) {
    qDebug() << "provideEvent is a no-op (returning zero)";

    return 0;
}

}  // namespace trodes


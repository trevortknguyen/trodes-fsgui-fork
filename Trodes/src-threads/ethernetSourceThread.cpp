/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "ethernetSourceThread.h"
#include "globalObjects.h"
#include "CZHelp.h"
#include <chrono>
//On mac, set ethernet port to:
//IP: 192.168.0.1
//Subnet: 255.255.255.0

//On linux, set ethernet port to:
//IP:192.168.0.2 (or probably anything but .1)
//Subnet: 255.255.255.0


EthernetSourceRuntime::EthernetSourceRuntime(QObject *parent) {


}

EthernetSourceRuntime::~EthernetSourceRuntime() {
    udpDataSocket->close();
    udpDataSocket->deleteLater();

}


void EthernetSourceRuntime::Run() {

  emit startHelperThread();
  udpDataSocket = new QUdpSocket(this);
  //Listing to port 8200 on all adresses
  //udpDataSocket->setSocketOption(QAbstractSocket::ReceiveBufferSizeSocketOption, 8388607);
  udpDataSocket->setSocketOption(QAbstractSocket::ReceiveBufferSizeSocketOption, 8388607); //Since the default value is 0 (unlimited), this probably does not help.
  bool connected  = udpDataSocket->bind(TRODESHARDWARE_DATAPORT);
  if (!connected) {
       qDebug() << "Error in socket binding";
       emit failure();
       emit finished();
       return;
  }

  packets = 0;


  //DWORD BytesReceived;
  //int PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + 1 + 1;

  calculateHeaderSize();
  PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + (2*headerSize); //Packet size in bytes
  QByteArray datagram;
  datagram.resize(PACKET_SIZE);
  QHostAddress sender;
  quint16 senderPort;


  //unsigned char buffer[PACKET_SIZE];
  //buffer = new unsigned char[PACKET_SIZE];
  //buffer.resize(PACKET_SIZE);

  char *RxBuffer;
  int remainingSamples = 0;
  int leftInBuffer;
  double dTimestamp = 0.0;
  int tic = 0;
  int tempMax;
  int maxAvailable = 0;
  quitNow = false;
  acquiring = true;
  int numConsecErrors = 0;
  runLoopActive = true;

  packetSizeErrorThrown = false;
  lastTimeStamp = 0;

  qDebug() << "Ethernet handle events loop running....";

  while (quitNow != true) {


      if (!acquiring) {
          //Not acquiring, but connected to source
          if (packetSizeErrorThrown) {
            packetSizeErrorThrown = false;
            emit timeStampError(false);
          }
      }

      //Wait until a packet is available for reading.  This function blocks the
      //thread until data is available (or until the function times out)
      udpDataSocket->waitForReadyRead(500);


      //If data is available, read it one packet at a time
      while (udpDataSocket->hasPendingDatagrams()) {

          if (udpDataSocket->pendingDatagramSize() != PACKET_SIZE) {
              //if (aquiring) {

                  if (!packetSizeErrorThrown) {
                    qDebug() << "Error in socket acquisition. Incoming packet size is: " << udpDataSocket->pendingDatagramSize() << " expected: " << PACKET_SIZE;
                  }
                  int errorPacketSize =  udpDataSocket->pendingDatagramSize();

                  //Read in the bad data from the buffer
                  while (errorPacketSize > 0) {
                        if (errorPacketSize >= (int) PACKET_SIZE) {
                            udpDataSocket->readDatagram(datagram.data(), datagram.size(),
                                              &sender, &senderPort);
                            errorPacketSize -= PACKET_SIZE;
                        } else {
                            udpDataSocket->readDatagram(datagram.data(), errorPacketSize,
                                              &sender, &senderPort);
                            errorPacketSize = 0;
                        }

                  }

                  numConsecErrors++;
                  if (!packetSizeErrorThrown && numConsecErrors > 5) {
                      //Here we should display a message that something has gone wrong,
                      //but perhaps not stop data acquisition altogether?

                      packetSizeErrorThrown = true;
                      emit timeStampError(true);
                      //quitNow = true;
                      emit failure();


                  }
                  break;
              //}
          } else {




              //Process the received data packet.
              //The packet contains a 16-bit header for binary info,
              //a 32-bit time stamp, and hardwareConf->NCHAN 16-bit samples.

              leftInBuffer = PACKET_SIZE;

              udpDataSocket->readDatagram(datagram.data(), datagram.size(),
                                          &sender, &senderPort);


              RxBuffer = datagram.data();

              if (*RxBuffer != 0x55) {
                  //We have bad alignment.  Report error and toss packet.
                  qDebug() << "Bad frame alignment!  Tossing packet.";
                  /*int actualSyncLoc = 0;
                  for (unsigned int i = 0; i < PACKET_SIZE; i++) {
                      if (*(RxBuffer+i) == 0x55) {
                          actualSyncLoc = i;
                          qDebug() << "Potential sync byte at: " << actualSyncLoc;
                          //emit resetInterface();
                          //udpDataSocket->readDatagram(datagram.data(),datagram.size()-actualSyncLoc,&sender,&senderPort);

                          //emit finished();
                          //return;
                          break;
                      }
                  }*/
                  continue;
              }
              checkHardwareStatus((const uchar*)RxBuffer+1);

              numConsecErrors = 0;
              packets++;

              //publishDataPacket((unsigned char*)RxBuffer, PACKET_SIZE, CZHelp::systemTimeMSecs());

              //Read header info
              memcpy(&(rawData.digitalInfo[rawData.writeIdx*headerSize]), \
                      RxBuffer, headerSize*sizeof(uint16_t));

              RxBuffer += (2*headerSize);
              leftInBuffer -= (2*headerSize);


              //int16_t *dataPtr16 = (int16_t *)(RxBuffer);
              //rawData.digitalInfo[rawData.writeIdx] = *dataPtr16;
              //RxBuffer += 2;
              //leftInBuffer -= 2;



              //Process time stamp
              uint32_t* dataPtr = (uint32_t *)(RxBuffer);
              currentTimeStamp = *dataPtr;

              if (checkForCorrectTimeSequence()) {
                  rawData.timestamps[rawData.writeIdx] = *dataPtr;
                  RxBuffer += 4;
                  leftInBuffer -= 4;
                  dTimestamp += 1.0;
                  rawData.dTime[rawData.writeIdx] = dTimestamp;
                  // system time in nanoseconds
//                  rawData.sysClock[rawData.writeIdx] = QDateTime::currentMSecsSinceEpoch() * 1000000;
                  rawData.sysClock[rawData.writeIdx] = AbstractSourceRuntime::getSystemTimeNanoseconds();

                  //rawData.sysTimestamps[rawData.writeIdx] = CZHelp::systemTimeMSecs();
                  rawData.sysTimestamps[rawData.writeIdx] = QTime::currentTime().msecsSinceStartOfDay();


                  //Process the data in the packet
                  remainingSamples = hardwareConf->NCHAN;
                  memcpy(&(rawData.data[rawData.writeIdx*hardwareConf->NCHAN]), \
                          RxBuffer, remainingSamples*sizeof(uint16_t));
                  RxBuffer += remainingSamples * 2;
                  leftInBuffer -= remainingSamples * 2;
                  remainingSamples = 0;

                  calculateReferences();

                  //Advance the write markers and release a semaphore

                  writeMarkerMutex.lock();
                  rawData.writeIdx = (rawData.writeIdx + 1) % EEG_BUFFER_SIZE;
                  rawDataWritten++;
                  writeMarkerMutex.unlock();

                  for (int a = 0; a < rawDataAvailable.length(); a++) {
                      rawDataAvailable[a]->release(1);
                  }
                  sourceDataAvailable->release(1);
                  //rawDataAvailableForSave.release(1);
              }



              if ((tempMax = rawDataAvailable[0]->available()) > maxAvailable)
                  maxAvailable = tempMax;

              if ((++tic % 10000) == 0) {
                  maxAvailable = 0;
              }
          }
      }

  }
  runtimeHelper->quitNow = true;
  runLoopActive = false;

  qDebug() << "Ethernet loop finished.";

  emit finished();


}


EthernetInterface::EthernetInterface(QObject *) {
  state = SOURCE_STATE_NOT_CONNECTED;
  UDPDataProcessor = nullptr;
  udpControlSocket = nullptr;
  udpControlSocketReceiver = nullptr;
  udpECUDirectPort = nullptr;

}

EthernetInterface::~EthernetInterface() {


}

bool EthernetInterface::sendCommandToHardware(QByteArray &command) {

    QHostAddress tmpAddress;
    quint16 tmpPort;
    if (networkConf != NULL) {

      tmpAddress.setAddress(networkConf->hardwareAddress);
      tmpPort = networkConf->hardwarePort;
    } else {
        tmpAddress.setAddress(TRODESHARDWARE_DEFAULTIP);
        tmpPort = TRODESHARDWARE_CONTROLPORT;
    }

    int bytesWritten = udpControlSocket->writeDatagram(command.data(), command.size(),
                              tmpAddress, tmpPort);
    udpControlSocket->flush();

    if (bytesWritten == -1) {
        qDebug() << "Error writing to Ethernet source";
        return false;
    } else {
        return true;
    }
}

bool EthernetInterface::getDataStreamFromHardware(QByteArray* data, int numBytes) {
    //This function should be redefined for each inheriting class
    //It sends the start command 'sCommand' to the hardware, then puts all of the streaming bytes that
    //come back into *data until numBytes have been collected.
    //Returns true if all went well and false if something went wrong.

    if(UDPDataProcessor->acquiring){
        return false;
    }
    bool failed = false;
    QByteArray datagram;

    //HeadstageSettings settings = lastHeadstageSettings;
    QHostAddress tmpAddress;
    uint tmpPort;
    tmpAddress.setAddress(TRODESHARDWARE_DEFAULTIP);
    tmpPort = TRODESHARDWARE_CONTROLPORT;


    /*CloseInterface();

    udpControlSocket = new QUdpSocket(this);

    if (!udpControlSocket->bind(QHostAddress::Any,udpControlSocket->localPort())) {
        //emit newDiagnosticMessage("Error connecting: could not bind to control port.");
        qDebug() << "Error connecting: could not bind to control port.";
        return false;
    }*/



   /* datagram.resize(1);
    datagram[0] = 0x62; // 0x62 = stop data capture
    udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                              tmpAddress, tmpPort);*/



    /*if (!udpControlSocket->waitForReadyRead(1000)) {
        //emit newDiagnosticMessage("Open UDP communication failed: connection timed out.");
        qDebug() << "Open UDP communication failed: connection timed out.";
        CloseInterface();
        return false;
    }


    connect(udpControlSocket,SIGNAL(readyRead()),this,SLOT(controlSocketAcknowledgeReceived()));*/






    QUdpSocket *udpDataSocket = new QUdpSocket(this);
    //Listing to port 8200 on all adresses
    //udpDataSocket->setSocketOption(QAbstractSocket::ReceiveBufferSizeSocketOption, 8388607);
    udpDataSocket->setSocketOption(QAbstractSocket::ReceiveBufferSizeSocketOption, 8388607);
    bool connected  = udpDataSocket->bind(TRODESHARDWARE_DATAPORT);
    if (!connected) {
        //emit newDiagnosticMessage("Error in socket binding for data stream.");
        qDebug() << "Error in socket binding for data stream.";
        return false;
    }
    udpDataSocket->flush();




    datagram.resize(2);
    datagram[1] = 0x01;
    if (lastControllerSettings.ECUDetected) {
         datagram[0] = command_startWithECU;
    } else {
        datagram[0] = command_startNoECU;
    }

    // send start capture command
    udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                              tmpAddress, networkConf->hardwarePort);




    datagram.resize(10000);
    QHostAddress sender;
    quint16 senderPort;

    int dataGramSize;
    int numBytesRecieved = 0;


    while (numBytesRecieved < numBytes) {



        //Wait until a packet is available for reading.  This function blocks the
        //thread until data is available (or until the function times out)
        udpDataSocket->waitForReadyRead(1000);
        if (!(udpDataSocket->hasPendingDatagrams())) {
            //emit newDiagnosticMessage("Error: no data coming from hardware.");
            qDebug() << "Error: no data coming from hardware.";
            failed = true;
            break;
        }


        //If data is available, read it one packet at a time
        while (udpDataSocket->hasPendingDatagrams()) {


            //Process the received data packet.
            //The packet contains a 16-bit header for binary info,
            //a 32-bit time stamp, and hardwareConf->NCHAN 16-bit samples.

            dataGramSize = udpDataSocket->pendingDatagramSize();

            udpDataSocket->readDatagram(datagram.data(), datagram.size(),
                                        &sender, &senderPort);

            data->append(datagram.data(),dataGramSize);
            numBytesRecieved += dataGramSize;
        }


    }


    datagram.resize(1);

    //datagram[0] = 0x01; //dummy byte until we have the command byte working
    datagram[0] = command_stop; // 0x62 = stop data capture

    // send stop capture command
    udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                                    tmpAddress, tmpPort);
    udpControlSocket->flush();

    //CloseInterface();
    udpDataSocket->close();
    delete udpDataSocket;

    if (failed) {
        return false;
    } else {
        return true;
    }

}

bool EthernetInterface::RunDiagnostic(QByteArray *data, HeadstageSettings &hsSettings, HardwareControllerSettings &cnSettings) {

    bool failed = false;

    udpControlSocket = new QUdpSocket(this);
    //udpControlSocketReceiver = new QUdpSocket(this);
    QHostAddress tmpAddress;
    uint tmpPort;

    tmpAddress.setAddress(TRODESHARDWARE_DEFAULTIP);
    tmpPort = TRODESHARDWARE_CONTROLPORT;

    if (!udpControlSocket->bind(QHostAddress::Any,udpControlSocket->localPort())) {
        emit newDiagnosticMessage("Error connecting: could not bind to control port.");
        return false;
    }


    QByteArray datagram;
    datagram.resize(1);
    datagram[0] = 0x62; // 0x62 = stop data capture
    udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                              tmpAddress, tmpPort);

    // send stop capture command in order to recieve ack message
    //Windows allows writing in broadcast mode, but Macs do not.  Not sure about linux yet.
  /*#ifdef WIN32
    udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                            QHostAddress::Broadcast, tmpPort);
  #else
    udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                              tmpAddress, tmpPort);
  #endif*/


    if (!udpControlSocket->waitForReadyRead(1000)) {
        emit newDiagnosticMessage("Open UDP communication failed: connection timed out.");
        CloseInterface();
        return false;
    }


    connect(udpControlSocket,SIGNAL(readyRead()),this,SLOT(controlSocketAcknowledgeReceived()));


    hsSettings = GetHeadstageSettings();
    cnSettings = GetControllerSettings();

    bool headstageDetected = false;
    bool ECUReported = false;

    if (cnSettings.valid) {
        if (cnSettings.ECUDetected) {
            ECUReported = true;
            setECUConnected(true);
        }
    }
    if (hsSettings.valid) {
       headstageDetected = true;
    }


    if (headstageDetected) {


        QUdpSocket *udpDataSocket = new QUdpSocket(this);
        //Listing to port 8200 on all adresses
        //udpDataSocket->setSocketOption(QAbstractSocket::ReceiveBufferSizeSocketOption, 8388607);
        udpDataSocket->setSocketOption(QAbstractSocket::ReceiveBufferSizeSocketOption, 8388607);
        bool connected  = udpDataSocket->bind(TRODESHARDWARE_DATAPORT);
        if (!connected) {
            emit newDiagnosticMessage("Error in socket binding for data stream.");
            CloseInterface();
            return false;
        }
        udpDataSocket->flush();

        datagram.resize(2);
        datagram[1] = 0x01; // default mode: only card 0 enabled = 32 channels
        datagram[0] = startCommandValue;


        // send start capture command
        udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                                        tmpAddress, tmpPort);

/*#ifdef WIN32
        udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                                        QHostAddress::Broadcast, tmpPort);
#else

        udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                                        tmpAddress, tmpPort);

#endif*/


        datagram.resize(10000);
        QHostAddress sender;
        quint16 senderPort;

        int dataGramSize;
        int numBytesRecieved = 0;



        //data->resize(210000);
        while (numBytesRecieved < 200000) {



            //Wait until a packet is available for reading.  This function blocks the
            //thread until data is available (or until the function times out)
            udpDataSocket->waitForReadyRead(1000);
            if (!(udpDataSocket->hasPendingDatagrams())) {
                emit newDiagnosticMessage("Error: no data coming from hardware.");
                failed = true;
                break;
            }


            //If data is available, read it one packet at a time
            while (udpDataSocket->hasPendingDatagrams()) {


                //Process the received data packet.
                //The packet contains a 16-bit header for binary info,
                //a 32-bit time stamp, and hardwareConf->NCHAN 16-bit samples.

                dataGramSize = udpDataSocket->pendingDatagramSize();

                udpDataSocket->readDatagram(datagram.data(), datagram.size(),
                                            &sender, &senderPort);

                data->append(datagram.data(),dataGramSize);
                numBytesRecieved += dataGramSize;
            }


        }


        datagram.resize(1);

        //datagram[0] = 0x01; //dummy byte until we have the command byte working
        datagram[0] = command_stop; // 0x62 = stop data capture

        // send stop capture command
        udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                                        tmpAddress, tmpPort);
        udpControlSocket->flush();


        //Windows allows writing in broadcast mode, but Macs do not.  Not sure about linux yet.
/*#ifdef WIN32
        udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                                        QHostAddress::Broadcast, tmpPort);
#else

        udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                                        tmpAddress, tmpPort);
        udpControlSocket->flush();

#endif*/

    }


    CloseInterface();

    if (failed) {
        return false;
    } else {
        return true;
    }


}

void EthernetInterface::InitInterface() {


  connectErrorThrown = false;
  restartThreadAfterShutdown = true;
  udpControlSocket = new QUdpSocket(this);
  //udpControlSocketReceiver = new QUdpSocket(this);
  QHostAddress tmpAddress;
  uint tmpPort;
  if (networkConf != NULL) {
    qDebug() << networkConf->hardwareAddress <<networkConf->hardwarePort ;
tmpAddress.setAddress(networkConf->hardwareAddress);
    tmpPort = networkConf->hardwarePort;
  } else {
      tmpAddress.setAddress(TRODESHARDWARE_DEFAULTIP);
      tmpPort = TRODESHARDWARE_CONTROLPORT;
  }
  if (!udpControlSocket->bind(QHostAddress::Any,udpControlSocket->localPort())) {
      qDebug() << "Error binding to control port";
      return;
  }


  //Using a SharedAddress setting does not work on mac or windows!!!!
  /*
  if (!udpControlSocket->bind(QHostAddress::Any,udpControlSocket->localPort() ,QUdpSocket::ShareAddress)) {
      qDebug() << "Error binding to control port";
      return;
  }*/


  QByteArray datagram;
  datagram.resize(1);

  //datagram[0] = 0x01; //dummy byte until we have the command byte working
  datagram[0] = 0x62; // 0x62 = stop data capture

  // send stop capture command in order to recieve ack message
  //Windows allows writing in broadcast mode, but Macs do not.  Not sure about linux yet.

  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                            tmpAddress, tmpPort);
 /*
#ifdef WIN32
  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                          QHostAddress::Broadcast, tmpPort);
#else

  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                            tmpAddress, tmpPort);
#endif
*/

  if (!udpControlSocket->waitForReadyRead(1000)) {
      qDebug() << "Open UDP communication failed";
      connectErrorThrown = true;      
      CloseInterface();
      emit stateChanged(SOURCE_STATE_CONNECTERROR);
      return;
  }


  connect(udpControlSocket,SIGNAL(readyRead()),this,SLOT(controlSocketAcknowledgeReceived()));

  //udpControlSocket->connectToHost(tmpAddress,TRODESHARDWARE_CONTROLPORT);


  //initialization went ok, so start the runtime thread
  UDPDataProcessor = new EthernetSourceRuntime(NULL);
  setUpThread(UDPDataProcessor);

  connect(UDPDataProcessor,SIGNAL(resetInterface()),this,SLOT(ResetInterface()));

  /*
  workerThread = new QThread();
  UDPDataProcessor->moveToThread(workerThread);
  //connect(workerThread, SIGNAL(started()), usbDataProcessor, SLOT(Run()));
  UDPDataProcessor->connect(this, SIGNAL(startRuntime()), SLOT(Run()));
  connect(UDPDataProcessor, SIGNAL(finished()), workerThread, SLOT(quit()));
  connect(UDPDataProcessor, SIGNAL(finished()), UDPDataProcessor, SLOT(deleteLater()));
  connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
  workerThread->start();
  */



  //get headstage and controller settings if this is the first init (and not a reinit).
  //We need to do this here so that the settings can be logged onto the headers of recording files.
  if (!reinitMode) {
      HeadstageSettings settingsRead = GetHeadstageSettings();
      if (settingsRead.valid) {
          emit headstageSettingsReturned(settingsRead);
      }

      HardwareControllerSettings hardwareRead = GetControllerSettings();
      if (hardwareRead.valid) {
          emit controllerSettingsReturned(hardwareRead);
      }
  }
  state = SOURCE_STATE_INITIALIZED;

  emit stateChanged(SOURCE_STATE_INITIALIZED);

}

void EthernetInterface::StartAcquisition(void) {

    if (!threadCreated) {
        restartThread();
    }


  static int runtimeStarted  = 0;
  //unsigned char TxBuffer[256]; // Contains data to write to device


  //Each command will eventually start with a 'type' byte, which will designate
  //what the following data is, and how many bytes are being sent. For now, no type byte exists.

  QByteArray datagram;
  datagram.resize(2);

  rawData.writeIdx = 0; // location where we're currently writing

  // Send "start data capture" command

  switch (hardwareConf->NCHAN) {
    case 0:
      datagram[1] = 0x00; // only card 0 enabled = 32 channels
      break;
    case 32:
      datagram[1] = 0x01; // only card 0 enabled = 32 channels
      break;
    case 64:
      datagram[1] = 0x03; // card 0 and 1 enabled = 64 channels
      break;
    case 96:
      datagram[1] = 0x07; // card 0,1,and 2 enabled = 96 channels
      break;
    case 128:
      datagram[1] = 0x0F; // card 0,1,2, and 3 enabled = 128 channels
      break;
    case 160:
      datagram[1] = 0x1F; // card 0,1,2,3, and 4 enabled = 160 channels
      break;
    default:
      datagram[1] = 0x01; // default mode: only card 0 enabled = 32 channels
      break;

  }

  //datagram[1] = 0x61; // 0x61 = start data capture without ECU
  //datagram[1] = 0x64; // 0x64 = start data capture with ECU

  datagram[0] = startCommandValue;
  //nsUSBRuntime->aquiring = true;
  UDPDataProcessor->resetStreamingStats();
  UDPDataProcessor->acquiring = true;


  // send start capture command
  QHostAddress tmpAddress;
  tmpAddress.setAddress(networkConf->hardwareAddress);
  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                            tmpAddress, networkConf->hardwarePort);

/*#ifdef WIN32
  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                          QHostAddress::Broadcast, networkConf->hardwarePort);
#else
  QHostAddress tmpAddress;
  tmpAddress.setAddress(networkConf->hardwareAddress);
  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                            tmpAddress, networkConf->hardwarePort);

  //udpControlSocket->write(datagram);

  //QThread::msleep(100);

#endif*/


  if (runtimeStarted == 0) {
    emit startRuntime();
    qDebug() << "Told runtime to start";
  }

  emit acquisitionStarted();
  state = SOURCE_STATE_RUNNING;
  emit stateChanged(SOURCE_STATE_RUNNING);
}

void EthernetInterface::StartSimulation() {

  if (!threadCreated) {
      restartThread();
  }



  static int runtimeStarted  = 0;
  //unsigned char TxBuffer[256]; // Contains data to write to device


  //Each command will eventually start with a 'type' byte, which will designate
  //what the following data is, and how many bytes are being sent. For now, no type byte exists.


  int requestAuxBytes = (hardwareConf->headerSize*2)-2;


  qDebug() << "Sending simulation command. Aux bytes:" << requestAuxBytes << "Num channel banks:" << ((hardwareConf->NCHAN/32)-1);

  QByteArray datagram;
  datagram.resize(3);
  QDataStream msg(&datagram, QIODevice::ReadWrite);
  msg.setByteOrder(QDataStream::LittleEndian);
  msg << (quint8)command_startSimulationNoECU;
  msg << (quint8)(requestAuxBytes); //Number of auxilliary bytes to add to the packet
  msg << (quint8)((hardwareConf->NCHAN/32)-1);

  rawData.writeIdx = 0; // location where we're currently writing

  UDPDataProcessor->resetStreamingStats();
  UDPDataProcessor->acquiring = true;

  // send start capture command
  QHostAddress tmpAddress;
  tmpAddress.setAddress(networkConf->hardwareAddress);
  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                            tmpAddress, networkConf->hardwarePort);

/*#ifdef WIN32
  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                          QHostAddress::Broadcast, networkConf->hardwarePort);
#else
  QHostAddress tmpAddress;
  tmpAddress.setAddress(networkConf->hardwareAddress);
  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                            tmpAddress, networkConf->hardwarePort);


#endif*/


  if (runtimeStarted == 0) {
    emit startRuntime();
    qDebug() << "Told runtime to start";
  }

  emit acquisitionStarted();
  state = SOURCE_STATE_RUNNING;
  emit stateChanged(SOURCE_STATE_RUNNING);
}


void EthernetInterface::StopAcquisition(void) {
  //unsigned char TxBuffer[256]; // Contains data to write to device


 // if (!threadCreated) return;

  QByteArray datagram;
  datagram.resize(1);

  //datagram[0] = 0x01; //dummy byte until we have the command byte working
  datagram[0] = command_stop; // 0x62 = stop data capture

  // send stop capture command
  QHostAddress tmpAddress;
  tmpAddress.setAddress(networkConf->hardwareAddress);
  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                            tmpAddress, networkConf->hardwarePort);
  udpControlSocket->flush();

  //Windows allows writing in broadcast mode, but Macs do not.  Not sure about linux yet.
/*#ifdef WIN32
  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                          QHostAddress::Broadcast, networkConf->hardwarePort);
#else
  QHostAddress tmpAddress;
  tmpAddress.setAddress(networkConf->hardwareAddress);
  udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                            tmpAddress, networkConf->hardwarePort);
  udpControlSocket->flush();
  //udpControlSocket->write(datagram);
#endif*/

  emit acquisitionStopped();
  state = SOURCE_STATE_INITIALIZED;
  emit stateChanged(SOURCE_STATE_INITIALIZED);

  if (!threadCreated) return;

  if (connectErrorThrown) {
      restartThreadAfterShutdown = false;
  }

  //The worker thread is stopped, and will restart again if restartThreadAfterShutdown is true
  qDebug() << "Stopped acquisition. Timestamp:" << currentTimeStamp;
  UDPDataProcessor->printStreamingStats();
  UDPDataProcessor->quitNow = true;
  UDPDataProcessor->acquiring = false;






  if (connectErrorThrown) {
    CloseInterface();
  }


}

void EthernetInterface::restartThread() {

    //start the runtime thread
    qDebug() << "Restarting ethernet source thread.";
    UDPDataProcessor = new EthernetSourceRuntime(NULL);
    setUpThread(UDPDataProcessor);
    connect(UDPDataProcessor,SIGNAL(resetInterface()),this,SLOT(ResetInterface()));
    state = SOURCE_STATE_INITIALIZED;
    emit stateChanged(SOURCE_STATE_INITIALIZED);


}

void EthernetInterface::ResetInterface(void) {
    qDebug() << "Resetting UDP interface";

    StopAcquisition();
    RunTimeError();



}

void EthernetInterface::CloseInterface(void) {

    restartThreadAfterShutdown = false;

    if (state == SOURCE_STATE_RUNNING) {
          StopAcquisition();
    }

    udpControlSocket->close();
    udpControlSocket->deleteLater();

    emit stateChanged(SOURCE_STATE_NOT_CONNECTED);
    lastControllerSettings = HardwareControllerSettings();
    lastHeadstageSettings = HeadstageSettings();
}

void EthernetInterface::SendImpendaceMeasureCommand(HardwareImpedanceMeasureCommand s) {
    qDebug() << "Ethernet interface got impedance measure request.";
    //Temporarily disconnect the ack receiver
    disconnect(udpControlSocket,SIGNAL(readyRead()),this,SLOT(controlSocketAcknowledgeReceived()));
    char inputarray[1000];
    if (s.numberOfCycles > 256) {
        return; //Not allowed
    }

    while (udpControlSocket->hasPendingDatagrams()) {
        udpControlSocket->readDatagram(inputarray, 90);
    }

    QByteArray datagram;
    datagram.resize(s.WRITESETTINGSBYTES);
    s.writeSettings(datagram.data());
    sendMessageToHardware(datagram);


    int messageSize = 0;
    bool success = false;

    if (udpControlSocket->waitForReadyRead(500)) {
        while (udpControlSocket->hasPendingDatagrams()) {
            int readSize = udpControlSocket->pendingDatagramSize();
            if (readSize > 1000) {
                break;
            }

            int rs = udpControlSocket->readDatagram(inputarray+messageSize,readSize);
            messageSize += rs;

        }



        int expectedBytes = 3;
        uint32_t zCheck_diff = 0;
        if ((messageSize == expectedBytes) && inputarray[0] == (char)received_impedanceValues) {
            zCheck_diff = (uint32_t)((reinterpret_cast<unsigned char&>(inputarray[2])))*256;
            zCheck_diff = zCheck_diff + ((reinterpret_cast<unsigned char&>(inputarray[1])));
            //printf("zCheck peak-to-peak amplitude: %u\n", zCheck_diff);
        }  else {
            printf("Error reading impedance value.\n");
            return;
        }
        success = true;
        double meanPeakVoltage_nV = 0.0;
        double impedanceCalc_Ohm = 0.0;
        if (spikeConf->deviceType == "intan") {
            meanPeakVoltage_nV = (zCheck_diff*195.0)/2;
            double peakCurrent_nA;

            if (s.notchHz == 0) {
                //0.1 pF
                peakCurrent_nA = (((double)(s.frequency))/1000.0)*.38; //nA

            } else if (s.notchHz == 1) {
                //1.0 pF
                peakCurrent_nA = (((double)(s.frequency))/1000.0)*3.8; //nA

            } else if (s.notchHz == 2) {
                //10 pF
                peakCurrent_nA = (((double)(s.frequency))/1000.0)*38; //nA

            } else {
                return;
            }

            impedanceCalc_Ohm = meanPeakVoltage_nV/peakCurrent_nA;
        }



        emit impedanceValueReturned(s.channel,(int)impedanceCalc_Ohm);


    }

    if (!success) {
        qDebug() << "No impedance values recieved from hardware.";

    }

    connect(udpControlSocket,SIGNAL(readyRead()),this,SLOT(controlSocketAcknowledgeReceived()));



}

quint64 EthernetInterface::getTotalUnresponsiveHeadstagePackets() {
    quint64 rVal = 0;
    if (UDPDataProcessor != NULL) {
        rVal = UDPDataProcessor->totalUnresponsiveHeadstagePackets;
    }
    return rVal;
}

quint64 EthernetInterface::getTotalDroppedPacketEvents() {
    quint64 rVal = 0;
    if (UDPDataProcessor != NULL) {
        rVal = UDPDataProcessor->totalDroppedPacketEvents;
    }
    return rVal;
}

HeadstageSettings EthernetInterface::GetHeadstageSettings() {

    //Temporarily disconnect the ack receiver
    disconnect(udpControlSocket,SIGNAL(readyRead()),this,SLOT(controlSocketAcknowledgeReceived()));
    char inputarray[100];

    while (udpControlSocket->hasPendingDatagrams()) {
        udpControlSocket->readDatagram(inputarray, 90);
    }

    HeadstageSettings s;
    QByteArray datagram;
    datagram.resize(1);
    datagram[0] = (quint8)command_getHeadstageSettings;
    sendMessageToHardware(datagram);
    qDebug() << "Requested headstage settings.";

    int messageSize = 0;

    if (udpControlSocket->waitForReadyRead(1500)) {
        while (udpControlSocket->hasPendingDatagrams()) {
            int readSize = udpControlSocket->pendingDatagramSize();
            if (readSize > 50) {
                break;
            }
            int rs = udpControlSocket->readDatagram(inputarray+messageSize,readSize);
            messageSize += rs;           
            if (messageSize >= 27 && inputarray[0] == (char)received_headstageSettings) {
                qDebug() << "Headstage settings returned from hardware.";
                QByteArray datagram(inputarray,s.READSETTINGSBYTES);
                s = AbstractTrodesSource::convertHeadstageSettings(datagram);
                emit headstageSettingsReturned(s);

                lastHeadstageSettings = s;
                messageSize = 0;

            }
        }

    }
    else if(lastHeadstageSettings.valid){
        //previously stored settings is valid
        s = lastHeadstageSettings;
    }
    else {
        qDebug() << "Error reading headstage settings message from hardware.";

    }

    connect(udpControlSocket,SIGNAL(readyRead()),this,SLOT(controlSocketAcknowledgeReceived()));

    return s;

}

void EthernetInterface::SendControllerSettings(HardwareControllerSettings s) {
    //SET CONTROLLER SETTINGS (MESSAGE TO HARDWARE)-- 9 BYTES
    //<0x84><uint8 rfChannel><uint8 samplingRateKhz><uint8 s3><uint8 s4><uint8 s5><uint8 s6><uint8 s7><uint8 s8>

    QByteArray datagram;
    datagram.resize(s.WRITESETTINGSBYTES);
    s.writeSettings(datagram.data());
    sendMessageToHardware(datagram);

    //Next we send the command for the headstage to save settings to flash memory
    /*QByteArray datagram2;
    datagram2.resize(1);
    QDataStream msg2(&datagram2, QIODevice::ReadWrite);
    msg2.setByteOrder(QDataStream::LittleEndian);
    msg2 << (quint8)command_writeHSSettingsToFlash;
    sendMessageToHardware(datagram2);*/

    /*QThread::msleep(5000);
    //emit headstageSettingsReturned(s);
    HeadstageSettings newRead = GetHeadstageSettings();
    if (newRead == s) {
        qDebug() << "Verified new controller settings";
    } else {


        qDebug() << "Error while verifying new headstage settings.";
        QMessageBox messageBox;
        messageBox.setWindowFlags(Qt::WindowStaysOnTopHint);
        messageBox.critical(0,"Error","Settings change could not be verified from MCU. Please check that the settings were recieved and properly recieved.");
        messageBox.setFixedSize(500,200);
    }*/

}

HardwareControllerSettings EthernetInterface::GetControllerSettings() {

    //Temporarily disconnect the ack receiver
    disconnect(udpControlSocket,SIGNAL(readyRead()),this,SLOT(controlSocketAcknowledgeReceived()));

    HardwareControllerSettings s;
    QByteArray datagram;
    datagram.resize(1);
    datagram[0] = (quint8)command_getControllerSettings;
    sendMessageToHardware(datagram);
    qDebug() << "Requested controller settings.";
    char inputarray[40];

    if (udpControlSocket->waitForReadyRead(1500)) {
        while (udpControlSocket->hasPendingDatagrams()) {
            //qDebug() << "Message size: " << udpControlSocket->pendingDatagramSize();
            udpControlSocket->readDatagram(inputarray,30);

            if (inputarray[0] == (char)received_controllerSettings) {
                qDebug() << "Controller settings returned from hardware.";
                QByteArray datagram(inputarray, s.READSETTINGSBYTES);
                s = AbstractTrodesSource::convertHardwareControllerSettings(datagram);
                emit controllerSettingsReturned(s);
                lastControllerSettings = s;

            } else {
                qDebug() << "Wrong return message code: " << (quint8)inputarray[0];
            }
        }

    }
    else if(lastControllerSettings.valid){
        //previously stored hardware settings is valid
        s = lastControllerSettings;
    }
    else {
        qDebug() << "Error reading controller settings message from hardware.";

    }

    //Reconnect the ack slot
    connect(udpControlSocket,SIGNAL(readyRead()),this,SLOT(controlSocketAcknowledgeReceived()));

    return s;

}

bool EthernetInterface::SendNeuroPixelsSettings(NeuroPixelsSettings s) {
    qDebug() << "Sending neuropixels settings via ethernet";
    bool success = true;
    //Temporarily disconnect the ack receiver
    disconnect(udpControlSocket,SIGNAL(readyRead()),this,SLOT(controlSocketAcknowledgeReceived()));
    char inputarray[100];

    QByteArray datagram;
    datagram.resize(s.WRITESETTINGSBYTES);


    for (int p=0; p<s.numProbes();p++) {
        s.writeSettings(datagram.data(),p);
        sendMessageToHardware(datagram);


        //Check the return ack to make sure the message was properly recieved.
        if (udpControlSocket->waitForReadyRead(1500)) {
            while (udpControlSocket->hasPendingDatagrams()) {
                udpControlSocket->readDatagram(inputarray,30);

                if (inputarray[0] == (char)command_setNeuroPixelsSettings) {
                    qDebug() << "Got response from hardware after neuropixels command";
                    uint8_t response = (uint8_t)inputarray[1];
                    if (response == 0x06) {
                        qDebug() << "Successful transmission.";

                    } else {
                        qDebug() << "Transmission failed.";
                        success = false;
                    }

                }
            }
        }
    }

    connect(udpControlSocket,SIGNAL(readyRead()),this,SLOT(controlSocketAcknowledgeReceived()));

    return success;

}

void EthernetInterface::SendSaveHeadstageSettings() {
    //We send the command for the headstage to save settings to flash memory
    QByteArray datagram2;
    datagram2.resize(1);
    QDataStream msg2(&datagram2, QIODevice::ReadWrite);
    msg2.setByteOrder(QDataStream::LittleEndian);
    msg2 << (quint8)command_writeHSSettingsToFlash;
    sendMessageToHardware(datagram2);
}

void EthernetInterface::SendHeadstageSettings(HeadstageSettings s) {


    //SET HEADSTAGE SETTINGS (MESSAGE TO HARDWARE)-- 16 BYTES
    //<0x82><uint8 autoSettleOn><uint16 autosettle number of channels><uint16 autosettle thresh><uint8 smartRefOn><uint8 sensors on><uint8 s1><uint8 s2><uint8 s3><uint8 s4><uint8 s5><uint8 s6><uint8 s7><uint8 s8>
    //sensors on is an 8-bit code, least significant bit last <unused unused unused unused unused mag gyro accel>
    QByteArray datagram;
    datagram.resize(s.WRITESETTINGSBYTES);
    s.writeSettings(datagram.data());    sendMessageToHardware(datagram);

    //Next we send the command for the headstage to save settings to flash memory
    QByteArray datagram2;
    datagram2.resize(1);
    QDataStream msg2(&datagram2, QIODevice::ReadWrite);
    msg2.setByteOrder(QDataStream::LittleEndian);
    msg2 << (quint8)command_writeHSSettingsToFlash;
    sendMessageToHardware(datagram2);

    QThread::msleep(5000);
    //emit headstageSettingsReturned(s);
    HeadstageSettings newRead = GetHeadstageSettings();
    if (newRead == s) {
        qDebug() << "Verified new headstage settings";
    } else {


        qDebug() << "Error while verifying new headstage settings.";
        QMessageBox messageBox;
        messageBox.setWindowFlags(Qt::WindowStaysOnTopHint);
        messageBox.critical(0,"Error","Settings change could not be verified from headstage. Please make sure headstage is properly connected.");
        messageBox.setFixedSize(500,200);
    }

}

void EthernetInterface::SetStimulationParams(StimulationCommand s) {
    //SEND STIMULATION PATTERN DEFINITION (MESSAGE TO HARDWARE)-- 24 BYTES

    QByteArray datagram;
    //Serialize the information in the object into a byte array containing the critical state machine information
    if(!convertToStateMachineCommand(s, &datagram)){
        qDebug() << "Stimulation command parameters are not valid!";
        return;
    }

    //Send the packet
    sendMessageToHardware_wait(datagram);
    qDebug() << "EthernetInterface::SetStimulationParams message sent to hardware";
}

bool EthernetInterface::SendGlobalStimulationSettings(GlobalStimulationSettings s) {
    //SEND GLOBAL STIM SETTINGS (MESSAGE TO HARDWARE)

    QByteArray datagram;

    //Serialize the information in the object into a byte array containing the critical state machine information
    if(!convertToStateMachineCommand(s, &datagram)){
        qDebug() << "Global stimulation settings parameters are not valid!";
        return false;
    }

    //Send the packet
    sendMessageToHardware_wait(datagram);
    return true;
}

void EthernetInterface::SendGlobalStimulationAction(GlobalStimulationCommand s) {
    //SEND GLOBAL STIMULATION ACTION (MESSAGE TO HARDWARE)

    QByteArray datagram;

    //Serialize the information in the object into a byte array containing the critical state machine information
    if(!convertToStateMachineCommand(s, &datagram)){
        qDebug() << "Global stimulation command parameters are not valid!";
        return;
    }

    //Send the packet
    sendMessageToHardware_wait(datagram);
}


void EthernetInterface::ClearStimulationParams(uint16_t slot) {
    //CLEAR STIMULATION PATTERN DEFINITION IN ONE MEMORY SLOT-- 3 BYTES

    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);

    //Fill the packet
    msg << (quint8)command_clearStimulateParams; //0x6F
    msg << (uint8_t)slot;
    //Calculate a checksum
    uint8_t checksum = 0;
    for (int i = 0;i < datagram.length()-1; i++) {
        checksum += datagram.at(i);
    }
    msg << checksum;

    //Send the packet
    sendMessageToHardware_wait(datagram);

}

void EthernetInterface::SendStimulationStartSlot(uint16_t slot) {
    //START STIMULATION PATTERN DEFINED IN ONE MEMORY SLOT-- 3 BYTES

    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);

    //Fill the packet
    msg << (quint8)command_stimulateStart; //0x70
    msg << (uint8_t)slot;
    //Calculate a checksum
    uint8_t checksum = 0;
    for (int i = 0;i < datagram.length()-1; i++) {
        checksum += datagram.at(i);
    }
    msg << checksum;

    //Send the packet
    sendMessageToHardware_wait(datagram);
}

void EthernetInterface::SendStimulationStartGroup(uint16_t group) {
    //START ALL STIMULATION PATTERNS DEFINED-- 3 BYTES

    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);

    //Fill the packet
    msg << (quint8)command_stimulateStartGroup;
    msg << (uint8_t)group;
    //Calculate a checksum
    uint8_t checksum = 0;
    for (int i = 0;i < datagram.length()-1; i++) {
        checksum += datagram.at(i);
    }
    msg << checksum;

    //Send the packet
    sendMessageToHardware_wait(datagram);
}

void EthernetInterface::SendStimulationStopSlot(uint16_t slot) {
    //STOP RUNNING STIMULATION PATTERN DEFINED IN ONE MEMORY SLOT-- 3 BYTES
    //slot must be a number between

    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);

    //Fill the packet
    msg << (quint8)command_stimulateStop;
    msg << (uint8_t)slot;
    //Calculate a checksum
    uint8_t checksum = 0;
    for (int i = 0;i < datagram.length()-1; i++) {
        checksum += datagram.at(i);
    }
    msg << checksum;

    //Send the packet
    sendMessageToHardware_wait(datagram);
}

void EthernetInterface::SendStimulationStopGroup(uint16_t group) {
    //STOP ALL RUNNING STIMULATION PATTERNS FOR SELECTED GROUP
    //NOT IMPLEMENTED IN HARDWARE!!

    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);

    //Fill the packet
    msg << (quint8)command_stimulateStopGroup;
    msg << (uint8_t)group;
    //Calculate a checksum
    uint8_t checksum = 0;
    for (int i = 0;i < datagram.length()-1; i++) {
        checksum += datagram.at(i);
    }
    msg << checksum;

    //Send the packet
    sendMessageToHardware_wait(datagram);
}

void EthernetInterface::EnableECUShortcutMessages() {
//    udpECUDirectPort = new QUdpSocket();
//    udpECUDirectPort->bind(TRODESHARDWARE_ECUDIRECTPORT);
}

void EthernetInterface::SendECUShortcutMessage(uint16_t function) {
    SendFunctionTrigger(function);
//    QByteArray datagram(2, '\0');
//    QDataStream msg(&datagram, QIODevice::ReadWrite);
//    msg << function;
//    udpECUDirectPort->writeDatagram(datagram);
}



void EthernetInterface::SendFunctionTrigger(int funcNum) {


        QByteArray datagram;
        datagram.resize(3);
        QDataStream msg(&datagram, QIODevice::ReadWrite);
        msg.setByteOrder(QDataStream::LittleEndian);
        msg << (quint8)command_trigger; // 0x69 = trigger command
        msg << (uint16_t)(funcNum);
        //msg << 0x01;

        sendMessageToHardware(datagram);
        qDebug() << "Sending trigger";





        /*QByteArray datagram;
        datagram.resize(1);

        //datagram[0] = 0x01; //dummy byte until we have the command byte working
        datagram[0] = 0x69; // 0x69 = trigger command
        datagram[1] = 1;

        sendMessageToHardware(datagram);

        datagram[0] = 0x69; // 0x69 = trigger command
        datagram[1] = 0;
        sendMessageToHardware(datagram);*/
}

void EthernetInterface::SendSettleCommand() {
    if (state == SOURCE_STATE_RUNNING) {
        qDebug() << "Sending settle command.";
        QByteArray datagram;
        datagram.resize(1);

        //datagram[0] = 0x01; //dummy byte until we have the command byte working
        datagram[0] = (quint8)command_settle; // 0x66 = settle command

        sendMessageToHardware(datagram);
    }
}

void EthernetInterface::SendSettleChannel(int byteInPacket, quint8 bit, int delay, quint8 triggerState) {
    if ((state == SOURCE_STATE_RUNNING)||(state == SOURCE_STATE_INITIALIZED)) {

        QString tStateString;
        if (triggerState == 0) {
            tStateString = "Off";
        } else if (triggerState == 1) {
            tStateString = "Rising edge";
        } else if (triggerState == 2) {
            tStateString = "Falling edge";
        } else if (triggerState == 3) {
            tStateString = "Both upward and downward edge";
        }

        qDebug() << "Sending settle channel." << byteInPacket << "Byte" << bit << "Bit" << tStateString;

        QByteArray datagram;
        datagram.resize(7);
        QDataStream msg(&datagram, QIODevice::ReadWrite);
        msg.setByteOrder(QDataStream::LittleEndian);
        msg << (quint8)command_setSettleTriggerChannel; // 0x6A = set settle channel
        msg << (uint16_t)(byteInPacket); //the byte in the packet to use
        msg << bit; //uint8, the bit within the given byte to use (0-7)
        msg << (uint16_t)(delay); //delay for the settle to occur in number of samples
        msg << triggerState; //uint8 (0 for off, 1 for downward edge, 2 for upward edge)

        sendMessageToHardware(datagram);
    }
}

void EthernetInterface::SendSDCardUnlock() {
    qDebug() << "Sending SD card unlock command.";
    QByteArray datagram;
    datagram.resize(1);

    //datagram[0] = 0x01; //dummy byte until we have the command byte working
    datagram[0] = (quint8)command_sdCardUnlock; // 0x65 = SD card unlock

    sendMessageToHardware(datagram);

}

void EthernetInterface::ConnectToSDCard() {
   // qDebug() << "Sending SD card connect command.";
   // QByteArray datagram;
   // datagram.resize(1);

    //datagram[0] = 0x01; //dummy byte until we have the command byte working
    //datagram[0] = (quint8)command_connectToSD; // 0x67 = SD card unlock

    //sendMessageToHardware(datagram);

}

void EthernetInterface::ReconfigureSDCard(int numChannels) {
    qDebug() << "Sending SD card reconfigure command.";
    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);
    //msg << 0x01; //dummy byte until we have the command byte working
    msg << (quint8)command_configureSD; // 0x68 = ping the MCU for the card
    msg << (uint16_t)numChannels;

    sendMessageToHardware(datagram);
}

void EthernetInterface::sendMessageToHardware(QByteArray datagram) {
    //Windows allows writing in broadcast mode, but Macs do not.  Not sure about linux yet.
    QHostAddress tmpAddress;
    quint16 tmpPort;
    if (networkConf != NULL) {

      tmpAddress.setAddress(networkConf->hardwareAddress);
      tmpPort = networkConf->hardwarePort;
    } else {
        tmpAddress.setAddress(TRODESHARDWARE_DEFAULTIP);
        tmpPort = TRODESHARDWARE_CONTROLPORT;
    }

    udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                              tmpAddress, tmpPort);
    udpControlSocket->flush();

  /*#ifdef WIN32
    udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                            QHostAddress::Broadcast, tmpPort);
  #else

    udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                              tmpAddress, tmpPort);
    udpControlSocket->flush();

  #endif*/
}

void EthernetInterface::sendMessageToHardware_wait(QByteArray datagram, int waitMsecs){
    if(waitMsecs && lastSendTime.isValid() && lastSendTime.elapsed() < waitMsecs){
        //If timer has been started from last hardware send and its been less than 5 msecs
        QThread::msleep(waitMsecs-lastSendTime.elapsed());
    }
    sendMessageToHardware(datagram);
    lastSendTime.restart();
    qDebug() << "Message sent to hardware," << waitMsecs << "delay from last send";
}

void EthernetInterface::controlSocketAcknowledgeReceived() {
    char inputarray[40];
    //qDebug() << "Got a response from hardware";

    while (udpControlSocket->hasPendingDatagrams()) {
        udpControlSocket->readDatagram(inputarray,30);

        if (inputarray[0] == (char)received_headstageSettings) {
            qDebug() << "Headstage settings returned from hardware.";
            HeadstageSettings s;
            QByteArray datagram(inputarray,27);
            s = AbstractTrodesSource::convertHeadstageSettings(datagram);

            //emit headstageSettingsReturned(s);

        } else if (inputarray[0] == (char)command_setNeuroPixelsSettings) {
            qDebug() << "Recieved response from hardware after neuropixels command:" << (uint8_t)inputarray[1];
            //TODO:  second byte is ack/nack, we should use this to resend if nack
        }
    }

}

int EthernetInterface::MeasurePacketLength(HeadstageSettings settings){
    if(!UDPDataProcessor->acquiring && state == SOURCE_STATE_INITIALIZED && settings.valid){
        //start streaming
        char buf[20] = {0};
        buf[0] = startCommandValue;
        buf[1] = 0xff;

        QUdpSocket *udpDataSocket = new QUdpSocket(this);
        bool connected  = udpDataSocket->bind(TRODESHARDWARE_DATAPORT);
        QByteArray datagram;
        datagram.resize(settings.numberOfChannels*sizeof(int16_t)+sizeof(uint32_t)+2);

        if(!connected){
            delete udpDataSocket;
            return -1;
        }
        QHostAddress tmpAddress;
        tmpAddress.setAddress(networkConf->hardwareAddress);
        udpControlSocket->writeDatagram(buf, 2, tmpAddress, TRODESHARDWARE_CONTROLPORT);

      /*#ifdef WIN32
        udpControlSocket->writeDatagram(buf, 2, QHostAddress::Broadcast, TRODESHARDWARE_CONTROLPORT);
      #else
        QHostAddress tmpAddress;
        tmpAddress.setAddress(networkConf->hardwareAddress);
        udpControlSocket->writeDatagram(buf, 2, tmpAddress, TRODESHARDWARE_CONTROLPORT);
      #endif*/
        QVector<int> lengths;
        constexpr int numpackets = 20;
        for(int i = 0; i < numpackets; ++i){
            if(udpDataSocket->waitForReadyRead(50)){
                int len = udpDataSocket->pendingDatagramSize();
                if(len > datagram.size()){
                    datagram.resize(len);
                }
                udpDataSocket->readDatagram(datagram.data(), datagram.size());
                if(datagram.data()[0] == 0x55){
                    lengths.push_back(len);
                }
            }
            else{
                break;
            }
        }

        buf[0] = command_stop;
        tmpAddress.setAddress(networkConf->hardwareAddress);
        udpControlSocket->writeDatagram(buf, 1, tmpAddress, TRODESHARDWARE_CONTROLPORT);
        /*#ifdef WIN32
        udpControlSocket->writeDatagram(buf, 1, QHostAddress::Broadcast, TRODESHARDWARE_CONTROLPORT);
        #else
        tmpAddress.setAddress(networkConf->hardwareAddress);
        udpControlSocket->writeDatagram(buf, 1, tmpAddress, TRODESHARDWARE_CONTROLPORT);
        #endif*/

        udpDataSocket->close();
        delete udpDataSocket;

        if(lengths.size() < numpackets){
            return -1;
        }

        //"vote" for what packet size is by majority (mode)
        //qSort(lengths);
        std::sort(lengths.data(),lengths.data()+lengths.length()-1);
        int num = lengths[0];
        int mode = num;
        int count = 1;
        int countMode = 1;
        for(int i = 0; i < lengths.length(); ++i){
            if(lengths[i] == num){
                ++count;
            }
            else{
                if(count > countMode){
                    countMode = count;
                    mode = num;
                }
                count = 1;
                num = lengths[i];
            }
        }
        if(count > countMode){
            countMode = count;
            mode = num;
        }

        if(countMode > numpackets/2){
            //must still be majority
            return mode;
        }
        else{
            return -1;
        }
    }
    return -1;
}

bool EthernetInterface::isSourceAvailable(){
    QUdpSocket *udpControlSocket = new QUdpSocket();
    QHostAddress tmpAddress;
    uint tmpPort;
    if (networkConf != NULL) {
      tmpAddress.setAddress(networkConf->hardwareAddress);
      tmpPort = networkConf->hardwarePort;
    } else {
        tmpAddress.setAddress(TRODESHARDWARE_DEFAULTIP);
        tmpPort = TRODESHARDWARE_CONTROLPORT;
    }
    if (!udpControlSocket->bind(QHostAddress::Any,udpControlSocket->localPort())) {
        return false;
    }

    QByteArray datagram;
    datagram.resize(1);

    datagram[0] = 0x62; // 0x62 = stop data capture
    udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                              tmpAddress, tmpPort);

    // send stop capture command in order to recieve ack message
    //Windows allows writing in broadcast mode, but Macs do not.  Not sure about linux yet.
  /*#ifdef WIN32
    udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                            QHostAddress::Broadcast, tmpPort);
  #else

    udpControlSocket->writeDatagram(datagram.data(), datagram.size(),
                              tmpAddress, tmpPort);
  #endif*/


    if (!udpControlSocket->waitForReadyRead(1000)) {
        return false;
    }
    else{
        return true;
    }
}

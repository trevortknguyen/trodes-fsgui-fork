#ifndef SPIKEPROCESSORTHREAD_H
#define SPIKEPROCESSORTHREAD_H
#include <QObject>
#include <QtCore>

#include "configuration.h"
//#include "trodesSocket.h"
#include "trodesglobaltypes.h"
#include "trodesSocket.h"
#include <memory>
#include <TrodesNetwork/Resources/SourcePublisher.h>
#include <TrodesNetwork/Generated/TrodesSpikeWaveformData.h>
#include <TrodesNetwork/Generated/SpikePacket.h>
#include "streamprocesshandlers.h"


class SpikeProcessingBuffer
{
public:
    SpikeProcessingBuffer();
    ~SpikeProcessingBuffer();
    void addSpikes(const QVector<SpikeWaveform> &sp, int sourceIndex);
    int addSourceBuffer();
    bool fetchSpikes(QVector<SpikeWaveform>&sp);
private:
    int numSources;
    QAtomicInt lock;

    QVector<QVector<QVector<SpikeWaveform> > > spikeBuffer;
    QVector<int> readHeads;
    QVector<int> writeHeads;
    //QVector<QAtomicInt> buffStatus;
    //int readHead;
    int bufferSize;
    //QVector<bool> locked;
    QVector<QSemaphore*> spikesAvailableInSource;
    QSemaphore* spikesAvailable;

};

class SpikeProcessorThread : public QObject
{
    Q_OBJECT
public:
    SpikeProcessorThread(QObject *parent, TrodesConfigurationPointers c_ptrs);
    SpikeProcessingBuffer spikeBuffer;
    bool quitNow;
private:
    TrodesConfigurationPointers conf_ptrs;


    // need to start the publisher
    std::unique_ptr<trodes::network::SourcePublisher<trodes::network::SpikePacket>> spikeTrodesPub;
    std::unique_ptr<trodes::network::SourcePublisher<trodes::network::TrodesSpikeWaveformData>> waveformTrodesPub;
    // allocated when the publisher is created
    unsigned char *spikeDataBlock;

private slots:

public slots:
    //void receiveNewSpikes(QVector<SpikeWaveform>);
    void runLoop();
    void setUp();

signals:
    void newProcessedSpikes(QVector<SpikeWaveform>);

};

#endif // SPIKEPROCESSORTHREAD_H

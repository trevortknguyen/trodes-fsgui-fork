/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef STREAMPROCESSOR_H
#define STREAMPROCESSOR_H

#include <QtGui>
//#include <QGLWidget>
#include "configuration.h"
#include "iirFilter.h"
#include "globalObjects.h"
#include "sharedVariables.h"
#include "spikeDetectorThread.h"
#include "spikeprocessorthread.h"
//#include <concurrentqueue.h>
#include "sgringqueue.h"

//#include "trodesSocket.h"

#include <highfreqclasses.h>


#include <iostream>
#include "frequencyspectrum.h"
#include "spectrum.h"
#include "FFTRealFixLenParam.h"
#include "streamprocesshandlers.h"

#include <memory> // for unique_ptr


#include <TrodesNetwork/Resources/SourcePublisher.h>
#include <TrodesNetwork/Generated/TrodesLFPData.h>

#define NUMCHANNELSPERSTREAMPROCESSOR 64 //this defines the max number of channels per processor thread

#define NUMDIGSTATECHANGESTOKEEP 10000

static const int LFP_BUFFER_SIZE = 1024;
#include "sourceController.h"

extern eegDataBuffer rawData;

typedef struct {
  int ch, ref;
} chan;

Q_DECLARE_METATYPE(QVector<int>);

struct lfp_msg_t {
    int pnum;
    uint32_t ts;
    MSGPACK_DEFINE_MAP(pnum, ts);
};

class StreamProcessorManager;

class StreamProcessor: public QObject {

  Q_OBJECT

public:
  StreamProcessor(QObject *parent, int groupNumber, QList<int> nTList, StreamProcessorManager* managerPtr, TrodesConfigurationPointers c_ptrs);

  ~StreamProcessor();

  TrodesConfigurationPointers conf_ptrs;
  int groupNum;
  int quitNow;
  bool isLooping;
  QAtomicInt updateChannelsFlag; // triggers run loop to update channels
  QAtomicInt updateDataLengthFlag; // triggers run loop to update channels
  double newDataLength;
  int dataIdx;

  QVector<QVector<int>> nTrodeHWChans;

  QList<int> nTrodeList;
  QList<int> nTrodeIdList;
  QVector<int> nTrodeToLocal;

  QVector<int> auxChannelList;

  quint64 streamDataRead;

  void addAuxChannels(QList<int> auxList);

  int rawIdx;
  QVector <int> raw_increment;


private:
  AbstractNeuralDataHandler* neuralDataHandler;

//  AbstractNeuralDataHandler* neuralDataHandler2;
  int nChan; // number of eeg channels to display
  int nAuxChan;
  bool hasDigIO;
  bool hasAuxAnalog;

  double dataLength;
  int numLoopsWithNoData;
  int numLoopsWithNoHeadstageData;

  bool isSetup;
  int lfpDecimation;

  int spikeSourceIndex;

  StreamProcessorManager* streamManager;
  //QVector<vertex2d>* dataMinMax;

  QVector<bool> notchFiltersOn;
  QVector<bool> spikeFiltersOn; //Renamed from filtersOn
  QVector<bool> lfpFiltersOn;
  QVector<bool> triggered;
  QVector<bool> lfpModeOn;
  QVector<bool> spikeModeOn;
  QVector<bool> lfpRefOn;
  QVector<bool> refOn;
  QVector<bool> stimCapableRef;
  QVector<bool> rawRefOn;
  QVector<int>  moduleDataChan;
  QVector<int>  refChan;
  QVector<bool> groupRefOn;
  QVector<int> refGroup;


  QVector<int> ntrodesUpdated;
  QVector<QVector<BesselFilter*> > spikeFilters;
  QVector<BesselFilter*> lfpFilters;
  QVector<QVector<NotchFilter*> > notchFilters;

//  SpikeConfStructure
  //each processor has their own set of spikeconf->ntrode objects
  //or just have each processor have their own set of local arrays for spikeConf

  QList<BesselFilter*> filtersForContinuousSendChannels;
  QList<BesselFilter*> filtersForBlockContinuousSendChannels;

  //BesselFilter* dataFilters;

  QMutex waitConditionMutex;
  int rawDataAvailableIdx;


  //Older qsockets-based network
  //--------------
  // If we are sending out continuous data to another module, we have a list of message handlers (one for each NTrode)
  // In addition, we have a single data handler for blocks of continuous data, and separate data handlers for digital or analog IO data
  //QList<TrodesSocketMessageHandler *> contDataHandlers;
  //TrodesSocketMessageHandler *blockContinuousHandler;
  //TrodesSocketMessageHandler *analogIOHandler;
  //TrodesSocketMessageHandler *digitalIOHandler;
  //QVector<quint16> dataHandlersOn; // Copies the "moduleDataOn" flag from the handler
  //QList<int> chanDataHandler; // the indeces of data handlers for the selected channels
  //TrodesServer *dataServer;
  //DataTypeSpec dataProvided;
  //-------------------------


  QList<int> mhNTrodeIndex;  //



  QHash<QString, int> interleavedAuxChannelStates;
  QVector<bool> digStates;

  //bool *digInState; // the last state of the configured digital input ports ; allocated during Setup
  //bool *digOutState; // the last state of the configured digital output ports ; allocated during Setup

  int decimation; // the factor by which the data should be decimated.

  int16_t** dataPoints;

public:
  //avx
  QVector<int> chans;
  QVector<int> hwChanToNTrode;
  QVector<int> hwChanToChan;
  void updateDataLength(void);
  void updateChannels(void);
  void updateChannelsRef();
  void updateFilters();

  int getLfpDecimation() const;
  void setLfpDecimation(int value);

public slots:
  void runLoop();
//  void runLoopAVX();
//  void runAuxLoopAVX();
  void setUp();
//  void setUpAVX();
//  void setUpAuxAVX();
  void updateModuleDataChan();
  void newGlobalStimulationSettings(GlobalStimulationSettings s);

  void setupdateFiltersFlag();
private slots:
  //void newDataHandler(TrodesSocketMessageHandler* messageHandler, qint16 requestedNTrodeIndex);
  //void removeDataHandler();


signals:
   void bufferOverrun();
   //void addDataProvided(DataTypeSpec *dp);
   void sourceFail(bool fail);
   void noHeadstageFail(bool fail);
   void digitalStateChanged(int channelNum, quint32 t, bool state);
   void functionTriggerRequest(int funcNum);
   void newSpikes(QVector<SpikeWaveform>);


};


struct DigitalStateChangeInfo {
    QList<uint32_t> timeStamps;
    QList<bool> states;
};
struct timingstruct
{
    timingstruct(int n, uint32_t t, int64_t s) : ntrodeid(n), timestamp(t), systime(s){}
    int ntrodeid;
    uint32_t timestamp;
    int64_t systime;
};


class LFPAggregator : public QObject {
    Q_OBJECT

public:
    LFPAggregator(StreamProcessorManager *sm);
    ~LFPAggregator();
private:
//    QSemaphore *readySendSemaphore;
//    int16_t *senddata;
//    int numNtrodes;
    bool quit;
//    bool started;

    // because the previous code assumed ZMQ-based network is always active, we have
    // this code always running
    std::unique_ptr<trodes::network::SourcePublisher<trodes::network::TrodesLFPData>> lfpTrodesPub;

//    lfpPacket packet;
    StreamProcessorManager *streamManager;//Needed for lock free queue
//    moodycamel::ConsumerToken consumertok;//consumer token, speeds things up when we explicitly tell the queue which consumer (currently only 1) is consuming
//    std::vector<timingstruct> exports;
    std::vector<bool> expecting;
    std::vector<int> producer_ntrodes;
    std::vector<int> buffer_start_pos;
//    struct producer{
//        moodycamel::ProducerToken tok; //producer token
//        const int ntrodes;//number of ntrodes this producer manages
//        int expecting;//if the lfp thread is still expecting data from here or not
//    };
public slots:
    void initialize();
    void runloop();
    void endloop();
signals:
    void provideDataType(HighFreqDataType hfdt);
};

struct LFPQueueTraits : public moodycamel::ConcurrentQueueDefaultTraits
{
//    static const size_t BLOCK_SIZE = 2;
    static const std::uint32_t EXPLICIT_CONSUMER_CONSUMPTION_QUOTA_BEFORE_ROTATE = 2;
};

class StreamProcessorManager : public QObject {

  Q_OBJECT

public:
  StreamProcessorManager(QWidget *parent, TrodesConfigurationPointers c_ptrs, bool enableAVX=false);
  ~StreamProcessorManager();

  TrodesConfigurationPointers conf_ptrs;
  QVector<uint32_t>            getDigitalEventTimes();
  QString                      getPSTHTriggerID();
  bool                         getPSTHTriggerState();
  void                         startLFPPubThread();

  vertex2d                     *neuralDataMinMax;
  vertex2d                     *auxDataMinMax;
//  QVector<QVector<bool> >         spikeTickBins;
  bool **spikeTickBins; //something about using qt containers across threads causes crashes.
  bool                          displaySpikeTicks;
  //QList <Trigger*>             nTrodeTriggerProcessors; //each nTrode gets one thread to detect spike triggers
  QList<StreamProcessor*>      streamProcessors; //threads to process the continous streams of data
  QList<SpikeDetectorManager*>  spikeDetectorManagers; // pointers to containers of spike detectors
  QList<ThresholdSpikeDetector*> spikeDetectors; // list of spike detectors by ntrode

  SpikeProcessorThread*         spikeProcessor;
//  QSemaphore    *lfpSendSemaphore;  //Each streamprocessor decrements when it finishes its lfp calc
//  int16_t       *lfpData;           //Each streamprocessor places its calcs into this array


  //For the zmq-based network. It is used by the
  //zmq-based network to send out LFP data to other modules.
  //---------------------------
  QThread       *lfpthread;
  SGRingQueue<int16_t, uint32_t, 1024> *sglfpqueue;
  std::vector<producer> sgproducertoks;
  //  moodycamel::ConcurrentQueue<int16_t> lfpqueue;
  //  std::vector<moodycamel::ProducerToken>     producertoks;
  zmq::context_t zmqContext; // stream processors and lfp aggregator need this to communicate with each other
  //-------------------------

  int64_t                       *sumSquares; //used to calculate RMS
  int                           *sumSquaresN; //number of samples;
  int64_t                       *sumSquaresCopy;
  int                           *sumSquaresNCopy;
  bool                          RMSEnabled;
  QTimer                        RMSTimer;
  void enableRMSCalculations(bool on);

  bool avxsupported;
private:
  QList<QThread*>               processorThreads;
  QThread*                      spikeProcThread;
  QList<DigitalStateChangeInfo>    dioStateChanges;
  int                           PSTHAuxTriggerChannel;
  bool                          PSTHAuxTriggerState;



  bool                          gotSourceFailSignal;
  bool                          gotHeadstageFailSignal;
  void                          createNewProcessorThread(QList<int> nTrodeList);
  void                          createNewProcessorThread(QList<int> nTrodeLis,QList<int> auxChanList);
  void                          createSpikeProcessorThread();
  QList<QThread*>               spikeDetectorThreads;
  void                          createNewSpikeManagerThread(QList<int> nTrodeList);

  QVector<int> nTrodeToProcessorThread;

public slots:
    void startAcquisition();
    void stopAcquisition();
    void removeAllProcessors();
    void updateDataLength(double tlength);
    void updateFilters(QList<int>);
    void updateChannels();
    void updateChannelsRef();
    void createSpikeLogs(QString dataDir);
    void sourceFail(bool fail);
    void headstageFail(bool fail);
    void setPSTHTrigger(int headerChannel, bool state);
    void clearAllDigitalStateChanges();
    void receiveSpikeEvent(int nTrode, uint32_t time);
    void processNewSpikes(QVector<SpikeWaveform>);
    void setDisplaySpikeTicks(bool on);
    void updateGlobalStimSettings(GlobalStimulationSettings s);

    void setOneSecBin();
    void setTenSecBin();

private slots:
    void digitalStateChanged(int headerChannelInd, quint32 t, bool state);
    void calculateRMS();

signals:

    void startAllProcessorLoops();
    //void addDataProvided(DataTypeSpec *dp);
    void bufferOverrun();
    void createSpikeLogs_signalRelay(QString dataDir);
//    void signal_newContinuousHandler(TrodesSocketMessageHandler* messageHandler, qint16 nTrode);
    void moduleDataChannelChanged(int nTrode, int chan);
    void moduleDataChannelFilterChanged(int nTrode, int upperCutOff);
    void sourceFail_Sig(bool fail);
    void headstageFail_Sig(bool fail);
    void functionTriggerRequest(int funcNum);
    void stopLFPAggregator();
    void newContinuousData(HighFreqDataType);
    void sendRMSValues(QList<qreal> values);
    void relayNewGlobalStimulationSettings(GlobalStimulationSettings s);

};


#endif // STREAMPROCESSOR_H

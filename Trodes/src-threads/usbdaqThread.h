/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef USBDAQTHREAD_H
#define USBDAQTHREAD_H



#include <QThread>
#include <QVector>
#include "abstractTrodesSource.h"


#ifdef WIN32
    #include <windows.h>
	#include "ftd2xx.h"   
#endif
#ifdef __APPLE__
    #include "WinTypes.h"
    #include "ftd2xx.h"   
#endif
#ifdef linux
    #include "WinTypes.h"
    #include "ftd2xx.h"
#endif




#define VENDOR 0x0403
#define DEVICE 0x6010

extern FT_STATUS res;
extern FT_HANDLE ftdi;

class USBDAQRuntime : public AbstractSourceRuntime {
  Q_OBJECT
public:
  USBDAQRuntime(QObject *parent);
  QVector<unsigned char> buffer;
  QByteArray datagram;


public slots:
  void Run(void);

  void ft_write_slot(QByteArray datagram);
signals:
  void ft_write_status_return(FT_STATUS, DWORD);
};

class USBDAQInterface : public AbstractTrodesSource {
  Q_OBJECT

public:
  USBDAQInterface(QObject *parent);
  ~USBDAQInterface(void);
  int state;
  quint64 getTotalDroppedPacketEvents() override;
  quint64 getTotalUnresponsiveHeadstagePackets() override;
  void SendImpendaceMeasureCommand(HardwareImpedanceMeasureCommand s) override;

  enum USBSourceTypes {
      MCU = 0x01,
      DockingStation = 0x02
  };

  //HardwareControllerSettings lastControllerSettings;
  //HeadstageSettings          lastHeadstageSettings;

private:
  //struct libusb_transfer **transfers;
  //int n_transfers;
  USBDAQRuntime *usbDataProcessor;
  QThread       *workerThread;

  QElapsedTimer lastSendTime;
  //A function that waits before calling FT_Write, making sure waitMSecs ms has passed since last message
  FT_STATUS FT_WRITE_wrapper(QByteArray &data);
  FT_STATUS retval;
  DWORD byteswritten;
  QTimer writeSignalTimer;
  int waittime;
  QElapsedTimer timer;

protected:
  bool getDataStreamFromHardware(QByteArray* data, int numBytes) override;
  bool sendCommandToHardware(QByteArray &command) override;

signals:
  void ft_write_signal(QByteArray);
private slots:
  void restartThread() override;
  void ft_write_slot(FT_STATUS status, DWORD bytes);

public slots:
  void InitInterface(void) override;
  void StartAcquisition(void) override;
  void StartSimulation(void) override;
  void StopAcquisition(void) override;
  void CloseInterface(void) override;
  void SendSettleCommand(void) override;
  void SendSDCardUnlock(void) override;
  void ConnectToSDCard(void) override;
  void ReconfigureSDCard(int numChannels) override;
  void SendHeadstageSettings(HeadstageSettings s) override;
  bool SendNeuroPixelsSettings(NeuroPixelsSettings s) override;
  void SendSaveHeadstageSettings() override;
  void SetStimulationParams(StimulationCommand s) override;
  bool SendGlobalStimulationSettings(GlobalStimulationSettings s) override;
  void SendGlobalStimulationAction(GlobalStimulationCommand s) override;
  void ClearStimulationParams(uint16_t slot) override;
  void SendStimulationStartSlot(uint16_t slot) override;
  void SendStimulationStartGroup(uint16_t group) override;
  void SendStimulationStopSlot(uint16_t slot) override;
  void SendStimulationStopGroup(uint16_t group) override;
  void SendControllerSettings(HardwareControllerSettings s) override;
  HeadstageSettings GetHeadstageSettings() override;
  HardwareControllerSettings GetControllerSettings() override;
  void SendSettleChannel(int byteInPacket, quint8 bit, int delay, quint8 triggerState) override;
  int MeasurePacketLength(HeadstageSettings settings) override;
  bool RunDiagnostic(QByteArray *data,HeadstageSettings &hsSettings, HardwareControllerSettings &cnSettings);
  uint16_t isSourceAvailable();
};



#endif // USBDAQTHREAD_H

/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TRIGGER_H
#define TRIGGER_H


/*
// *****Trigger Thread is not being used anywhere
#include <QGLWidget>
#include <QThread>
#include <QTimer>
#include <QElapsedTimer>
#include "configuration.h"
#include "trodesSocket.h"
#include "sharedVariables.h"
#include "stdint.h"

#define REPORTDELAYS 0 //if set to 1, delays from the time
                       //data is available to the time of spike detection will be displayed

#define CHECKINTERVAL_US 1000 //the interval in uS whereby the trigger checker is called
//#define CHECKINTERVAL_US 100000

#define POINTSTOREWIND 10


class nTrodeStream {

public:

    nTrodeStream(int numOfChannels);
    ~nTrodeStream();

    void writeData(int channel, int value, uint32_t time);
    bool readData(int numSamples, int rewind, QVector<vertex2d>* samplePtr, int* peaks, QVector<qint16>* int16samplePtr);

    void setThresh(int channel, int newThresh);
    void setTriggerMode(int channel, bool triggerOn);
    bool findEvent(void);
    int numChannels;
    QVector <qint64> samplesWritten;
    QVector <qint64> samplesRead;
    int bufferSize;
    uint32_t lastTriggerTime;


private:
    //QVector <QVector<int> > dataBuffer;
    int16_t** dataBuffer;
    uint32_t* timeBuffer;
    bool totalTriggerState;
    int numSamplesBelowThresh;
    QVector <int> writeBufferPosition;
    QVector <int> readBufferPosition;
    QVector <int> thresholds;
    QVector <bool> triggersOn;
    QVector <bool> triggerStates;
    QElapsedTimer stopWatch; //for for benchmarking the trigger delay
    QVector <qint64> stopWatchValues;

};


class Trigger: public QObject
{
    Q_OBJECT
public:
    Trigger(QObject *parent, int trode, int numOfChannels);
    ~Trigger();

    //void run();
    int nTrodeNum;
    int numChannels;
    //int pointsInWaveform;
    //int pointsToRewind;
    QVector <QVector<vertex2d> > waveForms;
    QVector <QVector<qint16> > waveforms_int16;
    QVector<int> peaks;
    QVector<bool> channelsHaveData;
    int dataCounter;
    bool quitNow;
    //QVector <QVector<int> > waveForms;

private:
    nTrodeStream* data;
    QTimer*  pullTimer;
    bool keepLooping;
    //bool checkForTrigger;

    TrodesSocketMessageHandler *dataHandler;
    QFile       *logFile;
    TrodesDataStream *outStream;
    bool        writeSpikesToDisk;


signals:
    void triggerEvent(const QVector<vertex2d>* waveForms, const int* peaks);
    void finished();
    void bufferOverrun();
    //void triggerEvent(QVector<int>*, int numPoints);

private slots:

    void pullTimerExpired();
    void removeTriggerHandler();

public slots:
    void setUp();
    void runLoop();
    void createLogFile(QString path);
    void closeLogFile();
    void setThresh(int channel, int newthresh);
    void setTriggerMode(int channel, bool triggerOn);
    void endTrigger();
    void addValue(int channel, int value, uint32_t time);
    void newNTrodeTriggerHandler(TrodesSocketMessageHandler* messageHandler, qint16 nTrode);
};
*/
#endif // TRIGGER_H

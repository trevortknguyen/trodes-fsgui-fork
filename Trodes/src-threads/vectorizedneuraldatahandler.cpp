#include "vectorizedneuraldatahandler.h"
#include <memory>
#include <limits>

NeuroPixelsLFPGrabber::NeuroPixelsLFPGrabber(TrodesConfigurationPointers conf, bool *ok)
{

    readyForProcessing = false;
    foundLFPSection = false;

    //384 channels come in per probe.
    for (int devInd=0; devInd < conf.hardwareConf->devices.length(); devInd++) {
        if (conf.hardwareConf->devices[devInd].name == "neuropixels_LFP") {

            ADCChannelByte = conf.hardwareConf->devices[devInd].byteOffset;
            numberOfProbes = (conf.hardwareConf->devices[devInd].numBytes-2)/64; //There are 64 bytes per probe (32 16-bit channels per probe)
            foundLFPSection = true;
            break;
        }
    }

    if (!foundLFPSection) {
        *ok = foundLFPSection;
        return;
    }

    numchans = 384*numberOfProbes;
    numchans_rounded16 = (numchans+15)-(numchans+15)%16;//round up to nearest 16

    //This defines the total amount of aligned memory to be allocated.
    size_t space = 32 + numchans_rounded16*(sizeof(int16_t));
    full_buffer = new uint8_t[space];
    memset(full_buffer, 0, space);
    Aligned32Allocator a(full_buffer, space);
    fullLFPpacket = a.aligned_alloc<int16_t>(numchans_rounded16);
    std::fill(fullLFPpacket,fullLFPpacket+numchans_rounded16,0); //Fill the full packet with zeros

    *ok = foundLFPSection;

}

NeuroPixelsLFPGrabber::~NeuroPixelsLFPGrabber(){

    if (foundLFPSection) {
        delete full_buffer; //Should this be delete []?
    }
}

bool NeuroPixelsLFPGrabber::isReady()
{
    return readyForProcessing;
}

const int16_t* NeuroPixelsLFPGrabber::getFullLFPBlock()
{
    return fullLFPpacket;
}


void NeuroPixelsLFPGrabber::fillLFPDataPoints(const char* auxDataBlock)
{

    //indices is 384 * numberofProbes long. It is divided up into 12 sections. Each section has 32*numberofProbes values
    //indicating where in output to put the values. The goal is to create a "full" packet with the same
    //channel order as the spike band data. This full packet will fill over 12 cycles.
    //We will know which of the 12 sections we should use from the 2-byte flag in the beginning of the device section
    //for neuropixels_LFP. If the nth bit is set, we should use the nth section of the indices.


    int16_t sectionFlag =  *((int16_t*)(auxDataBlock+ADCChannelByte));
    int16_t i = 1, pos = 0;


    // Iterate through bits of sectionFlag until we find a set bit
    // i&n will be non-zero only when 'i' and 'n' have a set bit
    // at same position
    while ((!(i & sectionFlag)) && (pos < 16)) {
        // Unset current bit and set the next bit in 'i'
        i = i << 1;
        // increment position
        ++pos;
    }



    if (pos < 12) {
        int16_t* startOfOutputSection = fullLFPpacket + (pos*32*numberOfProbes);
        int16_t* startOfDataSection = ((int16_t*)(auxDataBlock+ADCChannelByte+2));
        std::copy(startOfDataSection,startOfDataSection+(32*numberOfProbes),startOfOutputSection);
    } else {
        //Should not happen with neuropixels hardware
        readyForProcessing = false;
        qDebug() << "Unexpected Neuropixels LFP section flag: " << pos;
        return;
    }
    if (pos == 11) {
        //This is the final chunk of data to complete all channels, so we are ready for processing
        readyForProcessing = true;
    } else {
        readyForProcessing = false;
    }

}

VectorizedNeuralDataHandler::VectorizedNeuralDataHandler(TrodesConfigurationPointers conf, QList<int> nTrodeList_in, uint32_t extraSteps)
    : AbstractNeuralDataHandler(conf,nTrodeList_in, extraSteps)
{
    ringBufferWriteHead = samplesBeforeThresh_preAlignment-1; //advanced before first sample processed
    ringBufferStartWaveformHead = -1; //advanced before first sample processed
    npLFP = nullptr;

    int ind = 0;
    for(int nt : nTrodeList_in){
        nt2Ind.push_back(ind);
        ind += conf_ptrs.spikeConf->ntrodes[nt]->hw_chan.length();
        numChannelsPerNtrode.push_back(conf_ptrs.spikeConf->ntrodes[nt]->hw_chan.length());
        ntrodeTriggered.push_back(false);
        ntrodeWaveformStart.push_back(0);
        ntrodeWaveformLeftToCollect.push_back(0);
        ntrodeTriggerChannel.push_back(0);
        ntrodeTriggerMaxVal.push_back(0);
        ntrodeTriggerMaxValInd.push_back(0);
        ntrodeTriggerPeakTimestamp.push_back(0);      
    }



    spike_out_packets.resize(ringBufferSize);

    numchans = ind;
    numchans_rounded16 = (numchans+15)-(numchans+15)%16;//round up to nearest 16

    size_t pspace = 32 + numchans_rounded16*sizeof(int32_t)*7;
    param_buffer = new uint8_t[pspace];
    Aligned32Allocator palloc(param_buffer, pspace);

    chanByteIndices = palloc.aligned_alloc<int32_t>(numchans_rounded16);
    refIndices      = palloc.aligned_alloc<int32_t>(numchans_rounded16);
    refOn           = palloc.aligned_alloc<int32_t>(numchans_rounded16);
    lfpRefOn        = palloc.aligned_alloc<int32_t>(numchans_rounded16);
    rawRefOn        = palloc.aligned_alloc<int32_t>(numchans_rounded16);
    grouprefIndices = palloc.aligned_alloc<int32_t>(numchans_rounded16);
    grouprefOn      = palloc.aligned_alloc<int32_t>(numchans_rounded16);


    std::fill(chanByteIndices,chanByteIndices+numchans_rounded16,0);
    std::fill(refIndices,refIndices+numchans_rounded16,0);
    std::fill(refOn,refOn+numchans_rounded16,0);
    std::fill(lfpRefOn,lfpRefOn+numchans_rounded16,0);
    std::fill(grouprefIndices,grouprefIndices+numchans_rounded16,0);
    std::fill(grouprefOn,grouprefOn+numchans_rounded16,0);

    //This defines the total amount of aligned memory to be allocated.
    size_t space = 32 + numchans_rounded16*(sizeof(float) + sizeof(float) + sizeof(float) + sizeof(float) + sizeof(float) + sizeof(float) + sizeof(float) +
                                    sizeof(float) + sizeof(float) + sizeof(int16_t) + sizeof(int16_t) + sizeof(int16_t) + sizeof(int16_t) + sizeof(int16_t) + sizeof(int32_t) + (sizeof(int16_t)*ringBufferSize));
    full_buffer = new uint8_t[space];
    memset(full_buffer, 0, space);
    Aligned32Allocator a(full_buffer, space);

    raw_ref_float   = a.aligned_alloc<float>(numchans_rounded16);
    spike_ref_float = a.aligned_alloc<float>(numchans_rounded16);
    lfp_ref_float   = a.aligned_alloc<float>(numchans_rounded16);
    spike_out_float = a.aligned_alloc<float>(numchans_rounded16);
    spike_thresh_float = a.aligned_alloc<float>(numchans_rounded16);
    stim_float =     a.aligned_alloc<float>(numchans_rounded16);
    lfp_out_float   = a.aligned_alloc<float>(numchans_rounded16);
    minValues       = a.aligned_alloc<float>(numchans_rounded16);
    maxValues       = a.aligned_alloc<float>(numchans_rounded16);
    raw_unref       = a.aligned_alloc<int16_t>(numchans_rounded16);
    raw_lfp_unref       = a.aligned_alloc<int16_t>(numchans_rounded16);
    raw_ref         = a.aligned_alloc<int16_t>(numchans_rounded16);
    lfp             = a.aligned_alloc<int16_t>(numchans_rounded16);
    stim            = a.aligned_alloc<int16_t>(numchans_rounded16);
    spikeChannelAboveThresh    = a.aligned_alloc<int32_t>(numchans_rounded16);

    for (int packet=0;packet<ringBufferSize;packet++) {
        spike_out_packets[packet] = a.aligned_alloc<int16_t>(numchans_rounded16);
    }
    std::fill(minValues, minValues+numchans_rounded16, std::numeric_limits<float>::max());
    std::fill(maxValues, maxValues+numchans_rounded16, std::numeric_limits<float>::min());
//    std::fill(minValues, minValues+numchans_rounded16, 0);
//    std::fill(maxValues, maxValues+numchans_rounded16, 0);
    std::fill(stim,stim+numchans_rounded16,0); //We fill the stim vector with zeros. Only the stim-capable channels will change.
    std::fill(stim_float,stim_float+numchans_rounded16,0); //We fill the stim vector with zeros. Only the stim-capable channels will change.


//    notchprocs = new NotchProcessor[numchans_rounded16/16];
    numprocessors = numchans_rounded16/16;
    spikeprocs = new BandPassProcessor[numprocessors];
    lfpprocs = new LowPassProcessor[numprocessors];
    //lfpprocs = new BandPassProcessor[numprocessors];
    dispprocs = new DisplayProcessor[numprocessors];
    for(int i = 0; i < numprocessors; ++i){
//        notchprocs[i].setInputFreq(conf_ptrs.hardwareConf->sourceSamplingRate);
        spikeprocs[i].setSamplingRate(conf_ptrs.hardwareConf->sourceSamplingRate);
        if (extraProcessingModes & ExtraProcessorType::NeuroPixels1) {
            //The neuropixels probes sample LFP separately at 2500 Hz
            lfpprocs[i].setSamplingRate(2500);
        } else {
            lfpprocs[i].setSamplingRate(conf_ptrs.hardwareConf->sourceSamplingRate);
        }
//        dispprocs[i].setStreamSize(dataLength, traceLength);
    }

    channelTriggersOn.fill(true,numchans);


    //Some channels may be stimulation capable. We need to check them
    //at every packet to see if they are currently stimulating. We keep track
    //of the indices (in the gathered vectors) of the stim capable channels to reduce overhead of the check loop
    for (int n = 0; n < nTrodeList.length(); n++) {
        int nt = nTrodeList.at(n);
        for (int c = 0; c < conf_ptrs.spikeConf->ntrodes[nt]->hw_chan.length(); c++) {
            if (conf_ptrs.spikeConf->ntrodes[nt]->stimCapable[c]) {
                stimCapableChannels.push_back(nt2Ind[n]+c);
            }
        }
    }
    if (extraProcessingModes & ExtraProcessorType::IntanStim) {
        //stimCurrentScale_nA = globalStimSettings.currentScaling() << 8;
    }

    if (extraProcessingModes & ExtraProcessorType::NeuroPixels1) {
        //Neuropixels 1.0 headstage data stream separates out the LFP data. It is sampled at
        //lower sampling rate.
        //qDebug() << "Adding extra steps to data handler for Neurpixels 1.0 LFP processing.";

        bool ok;
        npLFP = new  NeuroPixelsLFPGrabber(conf, &ok);
         //TODO lfpprocs set sampling rate to 2500

    }

    if (extraProcessingModes & ExtraProcessorType::Export) {
        //This defines the total amount of aligned memory to be allocated for interpolation of missing packets.
        size_t interpspace = 32 + numchans_rounded16*(sizeof(float) + sizeof(float) + sizeof(float));
        interp_buffer = new uint8_t[interpspace];
        memset(interp_buffer, 0, interpspace);
        Aligned32Allocator intp(interp_buffer, interpspace);
        raw_ref_float_interp   = intp.aligned_alloc<float>(numchans_rounded16);
        spike_ref_float_interp = intp.aligned_alloc<float>(numchans_rounded16);
        lfp_ref_float_interp   = intp.aligned_alloc<float>(numchans_rounded16);
    }


    updateFilters();
}

VectorizedNeuralDataHandler::~VectorizedNeuralDataHandler(){
    delete full_buffer; //Should this be delete []?
    delete param_buffer; //Should this be delete []?
    //delete interp_buffer; //Should this be delete []?
    if (npLFP != nullptr) delete npLFP;
}
void VectorizedNeuralDataHandler::processNextSamples(uint32_t timestamp, const int16_t *neuralDataBlock, const int16_t *CARDataBlock, const char *auxDataBlock)
{

    bool interpGapDetected = false;


    if (extraProcessingModes & ExtraProcessorType::Export) {

        samplesProcessed++;
        timestamp_previous = timestamp_latest;

        interpValuesReadyToFetch = false;
        if (samplesProcessed > 1) {
            if (interpMode && (timestamp-timestamp_previous > 1) && (timestamp-timestamp_previous <= maxInterpGap)) {
                interpGapDetected = true;
                numSamplesLeftToInterp = timestamp-timestamp_previous;
                std::copy(raw_ref_float,raw_ref_float+numchans_rounded16,raw_ref_float_interp);
                std::copy(spike_ref_float,spike_ref_float+numchans_rounded16,spike_ref_float_interp);
                std::copy(lfp_ref_float,lfp_ref_float+numchans_rounded16,lfp_ref_float_interp);

            } else if (interpMode && (timestamp_latest-timestamp_previous > maxInterpGap)) {
                resetFilters();
                gapInDataOccured = true;
            } else if (timestamp-timestamp_previous > 1) {
                gapInDataOccured = true;
            } else {
                gapInDataOccured = false;
            }
        }
        //switchDataPointers();
    }

    timestamp_latest = timestamp;


    if (!interpGapDetected) {

        ringBufferWriteHead = (ringBufferWriteHead + 1)%ringBufferSize; //advance the ring buffer write head
        ringBufferStartWaveformHead = (ringBufferStartWaveformHead + 1)%ringBufferSize;
    }


    //Process 16 channels at a time with the processors. This allows us to use either 256-bit AVX (8 channels done twice)
    //or 512-bit AVX (all 16 channels done at once). 512-bit is not yet implemented.

    //First gather the data from the raw buffer, reorganizing the data to match the order that they appear
    //in the workspace.


    for(int i = 0; i < numchans_rounded16; i+=MAXPROCESSORSIZE){

        ReferenceProcessor::gatherUnreferenced(neuralDataBlock, chanByteIndices, raw_unref+i,i);
    }


    if (extraProcessingModes & ExtraProcessorType::NeuroPixels1) {


        //Neuropixels 1.0 headstage data stream separates out the LFP data. It is sampled at
        //lower sampling rate.
        npLFP->fillLFPDataPoints(auxDataBlock);
        if (npLFP->isReady()) {
            for(int i = 0; i < numchans_rounded16; i+=MAXPROCESSORSIZE){
                ReferenceProcessor::gatherUnreferenced(npLFP->getFullLFPBlock(), chanByteIndices, raw_lfp_unref+i,i);
            }
        }

        //This main loop performs the referencing
        //filtering, and threshold detection.
        //As above, each loop deals with 16 channels at a time for 256-bit or 512-bit AVX processing.
        for(int i = 0; i < numchans_rounded16; i+=MAXPROCESSORSIZE){
            //processor num
            int pnum = i/MAXPROCESSORSIZE; //since each processor deals with 16 at a time

            //We split data into three bands (Raw, LFP, and Spike), and we apply the references (or not) to each band
            //Processor takes into account whether group or single chan ref or no ref is applied
            ReferenceProcessor::applyReferences(raw_unref+i, neuralDataBlock, CARDataBlock, raw_ref_float+i,
                                                refIndices+i, rawRefOn+i, grouprefIndices+i, grouprefOn+i);
            ReferenceProcessor::applyReferences(raw_unref+i, neuralDataBlock, CARDataBlock, spike_ref_float+i,
                                                refIndices+i, refOn+i, grouprefIndices+i, grouprefOn+i);


            if (npLFP->isReady()) {
                //Neuropixels 1.0 headstage data stream separates out the LFP data. It is sampled at
                //lower sampling rate and the data come in across multiple packets. If isReady() is true, all channels
                //have finished streaming in, and we are ready to apply references and filter the data.
                ReferenceProcessor::applyReferences(raw_lfp_unref+i, npLFP->getFullLFPBlock(), CARDataBlock, lfp_ref_float+i,
                                                    refIndices+i, lfpRefOn+i, grouprefIndices+i, grouprefOn+i);


                //calculate LFP (lowpass) data from broadband, 2500 Hz data
                lfpprocs[pnum].newSamples(lfp_ref_float+i, lfp_out_float+i);

                //The LFP band is also converted back to int16
                AVXFloatToShort::convertToShort(lfp_out_float+i, lfp+i);

            }

            //The RAW band is converted back to int16 for the output
            AVXFloatToShort::convertToShort(raw_ref_float+i, raw_ref+i);

    //        //apply notch to raw
    //        notchprocs[pnum].newSamples(raw_ref_float+i);


            //calculate spike (bandpass) data, and invert signal if the invert setting is turned on
            spikeprocs[pnum].newSamples(spike_ref_float+i, spike_out_float+i);
            if (spikeInvert) InvertProcessor::invert(spike_out_float+i,spike_out_float+i);

            //Any threshold crossings will be used below to initiate spike snippet extraction. The
            //calculation of whether the signal is above or below the spike detection threshold
            //is calculated here in a vectorized operation.
            ThresholdProcessor::checkThreshCrossings(spike_out_float+i, spike_thresh_float+i, spikeChannelAboveThresh+i);

            //The spike band is converted back to int16 values and stored in a ring buffer. The buffer
            //is used below for spike extraction
            AVXFloatToShort::convertToShort(spike_out_float+i, spike_out_packets[ringBufferWriteHead]+i);



        } //END: 16-channel AVX main processing loop


    } else {


        //This main loop performs the referencing
        //filtering, and threshold detection.
        //As above, each loop deals with 16 channels at a time for 256-bit or 512-bit AVX processing.
        //std::cerr << "P1 ";
        for(int i = 0; i < numchans_rounded16; i+=MAXPROCESSORSIZE){
            //processor num
            int pnum = i/MAXPROCESSORSIZE; //since each processor deals with 16 at a time

            //We split data into three bands (Raw, LFP, and Spike), and we apply the references (or not) to each band
            //Processor takes into account whether group or single chan ref or no ref is applied
            ReferenceProcessor::applyReferences(raw_unref+i, neuralDataBlock, CARDataBlock, raw_ref_float+i,
                                                refIndices+i, rawRefOn+i, grouprefIndices+i, grouprefOn+i);

            ReferenceProcessor::applyReferences(raw_unref+i, neuralDataBlock, CARDataBlock, spike_ref_float+i,
                                                refIndices+i, refOn+i, grouprefIndices+i, grouprefOn+i);


            //Default mode: we apply LFP referencing and filters to full-rate data
            ReferenceProcessor::applyReferences(raw_unref+i, neuralDataBlock, CARDataBlock, lfp_ref_float+i,
                                                refIndices+i, lfpRefOn+i, grouprefIndices+i, grouprefOn+i);

            //We are going to interpolate for export, so we do not perform filtering now
            if (interpGapDetected) {
                continue;
            }

            //calculate LFP (lowpass) data from broadband, full rate data
            lfpprocs[pnum].newSamples(lfp_ref_float+i, lfp_out_float+i);

            //The RAW band is converted back to int16 for the output
            AVXFloatToShort::convertToShort(raw_ref_float+i, raw_ref+i);

            //        //apply notch to raw
            //        notchprocs[pnum].newSamples(raw_ref_float+i);


            //calculate spike (bandpass) data, and invert signal if the invert setting is turned on
            spikeprocs[pnum].newSamples(spike_ref_float+i, spike_out_float+i);
            if (spikeInvert) InvertProcessor::invert(spike_out_float+i,spike_out_float+i);

            //Any threshold crossings will be used below to initiate spike snippet extraction. The
            //calculation of whether the signal is above or below the spike detection threshold
            //is calculated here in a vectorized operation.
            ThresholdProcessor::checkThreshCrossings(spike_out_float+i, spike_thresh_float+i, spikeChannelAboveThresh+i);

            //The spike band is converted back to int16 values and stored in a ring buffer. The buffer
            //is used below for spike extraction
            AVXFloatToShort::convertToShort(spike_out_float+i, spike_out_packets[ringBufferWriteHead]+i);

            //The LFP band is also converted back to int16
            AVXFloatToShort::convertToShort(lfp_out_float+i, lfp+i);


        } //END: 16-channel AVX main processing loop


        if (extraProcessingModes & ExtraProcessorType::IntanStim) {
            //Now we check if any channels are currently stimulating (stim-capable channels only)
            //We may want to vectorize this code using AVX.

            if (extraProcessingModes & ExtraProcessorType::Export) {
                //If we are in export mode, we won't get global settings updates, so we need to check if the packet contains any updates to the global stimulation settings
                //The info is contained in the "sensor" section of the packet, which allows asyncronous data
                //to be transmitted as long as that data is lower sampling rate than the full rate.
                //A flag that identifies the type of data allows multiple data types to use the same part of the packet.
                //A change in global stim settings uses a flag byte value where the most sig bit is set



                const char* sensorSection = auxDataBlock+sensorSectionStartByte;
                if (*sensorSection & 0x80) {
                    //a new global stim setting has come in. The settings come in a 16-bit value occupying bytes 2 and 3 (with 0-based index)
                    //only the first 9 bits are used:
                    //settle_all_headstages <= rx_data[0];
                    //settle_this_headstage <= rx_data[1];
                    //amp_settle_mode <= rx_data[2];
                    //charge_recov_mode <= rx_data[3];
                    //stim_stepsize <= rx_data[7:4]
                    //DC_amp_convert <= rx_data[8];
                    //We are mainly interested in the stim_stepsize (4 bit) value

                    int16_t stimSettings = *(int16_t*)(sensorSection+2);

                    //Get the 4-bit stim_stepsize value, bit-shift down, then cast to GlobalStimulationSettings::CurrentScaling enum
                    GlobalStimulationSettings::CurrentScaling sc = (GlobalStimulationSettings::CurrentScaling)((stimSettings & (0x00F0)) >> 4);
                    switch(sc) {
                    case GlobalStimulationSettings::max10nA :
                        qDebug() << "Stim max scale: 10nA";
                        stimCurrentScale_nA = 10.0/255.0;
                        break;
                    case GlobalStimulationSettings::max20nA   :
                        qDebug() << "Stim max scale: 20nA";
                        stimCurrentScale_nA = 20.0/255.0;
                        break;
                    case GlobalStimulationSettings::max50nA  :
                        qDebug() << "Stim max scale: 50nA";
                        stimCurrentScale_nA = 50.0/255.0;
                        break;
                    case GlobalStimulationSettings::max100nA  :
                        qDebug() << "Stim max scale: 100nA";
                        stimCurrentScale_nA = 100.0/255.0;
                        break;
                    case GlobalStimulationSettings::max200nA   :
                        qDebug() << "Stim max scale: 200nA";
                        stimCurrentScale_nA = 200.0/255.0;
                        break;
                    case GlobalStimulationSettings::max500nA   :
                        qDebug() << "Stim max scale: 500nA";
                        stimCurrentScale_nA = 500.0/255.0;
                        break;
                    case GlobalStimulationSettings::max1uA   :
                        qDebug() << "Stim max scale: 1uA";
                        stimCurrentScale_nA = 1000.0/255.0;
                        break;
                    case GlobalStimulationSettings::max2uA   :
                        qDebug() << "Stim max scale: 2uA";
                        stimCurrentScale_nA = 2000.0/255.0;
                        break;
                    case GlobalStimulationSettings::max5uA   :
                        qDebug() << "Stim max scale: 5uA";
                        stimCurrentScale_nA = 5000.0/255.0;
                        break;
                    case GlobalStimulationSettings::max10uA   :
                        qDebug() << "Stim max scale: 10uA";
                        stimCurrentScale_nA = 10000.0/255.0;
                        break;
                    }


                }

            }


            for (int sInd : stimCapableChannels) {
                stim_float[sInd] = 0;
                stim[sInd] = 0;
                if (raw_unref[sInd] & 0x0001) {
                    //This channel is currently in stim mode.
                    const int16_t rawVal = raw_unref[sInd];
                    stim[sInd] = rawVal; //We keep a record of the full stim info
                    if (rawVal & 0x0002) {
                        //Stimultion current is on. We need to calculate what to actually display on the screen. We use stim_float for that.
                        //stimPulseAnywhere = true;
                        if (rawVal & 0x0004) {
                            //Anode
                            //stim_float[sInd] = ((rawVal & 0xFF00)>>8)|stimCurrentScale_nA;
                            stim_float[sInd] = ((rawVal & 0xFF00)>>4);
                        } else {
                            //Cathode
                            //stim_float[sInd] = -(((rawVal & 0xFF00)>>8)|stimCurrentScale_nA);
                            stim_float[sInd] = -((rawVal & 0xFF00)>>4);
                        }
                    }


                    //We zero out all neural recording bands
                    spike_out_packets[ringBufferWriteHead][sInd] = 0;
                    raw_ref[sInd] = 0;
                    lfp[sInd] = 0;
                    spike_out_float[sInd] = 0;
                    raw_ref_float[sInd] = 0;
                    lfp_out_float[sInd] = 0;

                    lfp_ref_float[sInd] = 0;
                    spike_ref_float[sInd] = 0;


                }
            }
        }

    }

    //If a gap was detected, then we start the interpolation process, and we dont continue with the rest of the processing since we have already processed
    if (interpGapDetected) {
        timestamp_latest = timestamp_previous;

        return;
    }


    //Now we perform spike snippet extraction of any spikes that are ready for processing.
    processSpikes();


}

void VectorizedNeuralDataHandler::processSpikes()
{
    //Perform spike snippet extraction of any spikes that are ready for processing.
    //Loop through each ntrode (since spikes trigger on the entire ntrode)
    for (int ntInd=0; ntInd < nt2Ind.length(); ntInd++) {

        int startInd = nt2Ind[ntInd];

        //Is the ntrode in the middle of extracting a spike?
        if (!ntrodeTriggered[ntInd]) {
            //This ntrode is not already in a triggered (extraction) state, which means it can be triggered. So we need to check for threshold crossing for all of its channels
            for (int ch=0; ch < numChannelsPerNtrode[ntInd]; ch++) {
                int channelIndex = startInd+ch;

                //Check if any channels are above threshold (already calculated above)
                if (channelTriggersOn[channelIndex] && (spikeChannelAboveThresh[channelIndex]!=0)) {
                    //Threshold crossing for that channel means that the nTrode has entered trigger mode
                    int16_t dataVal = *(spike_out_packets[ringBufferWriteHead]+channelIndex);
                    ntrodeTriggered[ntInd] = true; //set the triggered flag
                    ntrodeWaveformStart[ntInd] = ringBufferStartWaveformHead; //remember where the start of the waveform occured
                    ntrodeWaveformLeftToCollect[ntInd] = samplesAfterThresh_preAlignment-1; //remember when to stop collecting points for this waveform
                    ntrodeTriggerChannel[ntInd] = ch; //we will use this channel to calculate the sample that coincides with the spike peak

                    //To temporally align each spike by their peaks, we will need to find the max value of the spike.
                    //Here, we seed the calculation of peak value (and associated buffer index and system time) with the current value.
                    //As larger values come in, this value will be replaced until we reach the peak
                    ntrodeTriggerMaxValInd[ntInd] = ringBufferWriteHead;
                    ntrodeTriggerMaxVal[ntInd] = dataVal;
                    ntrodeTriggerPeakTimestamp[ntInd] = timestamp_latest;

                    //No need to look at the other channels in the ntrode because we now know it is in a triggered state
                    break;
                }
            }
        } else {
            //This ntrode is already in a triggered (extraction) state, so there is no need to check for threshold crossings. Instead,
            //continue searching for the spike's peak value until the entire waveform window has finished
            //streaming in.

            ntrodeWaveformLeftToCollect[ntInd]--;
            //We use the channel that first passed threshold to find the peak location.
            int16_t focusChannelDataVal = *(spike_out_packets[ringBufferWriteHead]+startInd+ntrodeTriggerChannel[ntInd]);
            if (focusChannelDataVal > ntrodeTriggerMaxVal[ntInd]) {
                ntrodeTriggerMaxVal[ntInd] = focusChannelDataVal;
                ntrodeTriggerMaxValInd[ntInd] = ringBufferWriteHead;
                ntrodeTriggerPeakTimestamp[ntInd] = timestamp_latest; //we replace the spike's timestamp with the current one
            }


            //Has the entire waveform window has finished streaming in?
            if  (ntrodeWaveformLeftToCollect[ntInd] == 0) {
                //This is the last point to collect in the waveform, so it is now ready to be processed and sent out for visualization, saving, etc.
                ntrodeTriggered[ntInd] = false;  //reset the trigger flag for the ntrode so that it can be triggered again.


                //We need to align all spikes so that their peaks fall on the same point in the window.
                int samplesToPeak; //how many samples from the beginning of the window until the waveform peak?
                if (ntrodeTriggerMaxValInd[ntInd] > ntrodeWaveformStart[ntInd]) {
                    //This part of the window does not loop around in the ring buffer, so no extra manipulation is needed
                    samplesToPeak = ntrodeTriggerMaxValInd[ntInd]-ntrodeWaveformStart[ntInd]+1;
                } else {
                    //The window looped inside the ring buffer, so we need to do a little more math
                    samplesToPeak = ntrodeTriggerMaxValInd[ntInd]+ringBufferSize-ntrodeWaveformStart[ntInd]+1;
                }
                int alignmentShift = samplesToPeak-alignmentMargins-peakAlignInd; //How much do we need to shift the window to center the peak on the desired point (peakAlignInd)?
                if (alignmentShift > alignmentMargins) {
                    //The peak happended really late in the window, and alignment is not possible with the available margins.
                    //To deal with this, we shift the window over by the margin length, and set the trigger flag again.
                    //The spike will be processed after more points are collected.
                    ntrodeWaveformStart[ntInd]=(ntrodeWaveformStart[ntInd]+alignmentMargins)%ringBufferSize;
                    ntrodeWaveformLeftToCollect[ntInd] = alignmentMargins;
                    ntrodeTriggered[ntInd] = true;
                } else if (alignmentShift < -alignmentMargins) {
                    //The peak happended really early in the window, and alignment is not possible with the available margins.
                    //With the right settings, it should be rare or impossible to get here. But if we do, we throw the event out.
                    //Do nothing.
                } else {
                    //The peak occured within the alignment margins, we are ok to copy the aligned waveform data over to a list.
                    //First, we copy the waveform data from the ring buffer to a new allocated memory location.
                    //This must be deleted by the last consumer, or we get a nasty memory leak.
                    //We aim the start of the data copy window so that the peak will fall on the desired index (peakAlignInd)
                    int snippetLocation = (ntrodeWaveformStart[ntInd]+alignmentMargins+alignmentShift)%ringBufferSize;

                    int16_t** waveform = new int16_t*[numChannelsPerNtrode[ntInd]];
                    for (unsigned int i=0;i<numChannelsPerNtrode[ntInd];i++) {
                        waveform[i] = new int16_t[numSamplesPerSpike];
                    }
                    for (int pointCopyInd = 0; pointCopyInd < numSamplesPerSpike; pointCopyInd++) {
                        for (unsigned int i=0;i<numChannelsPerNtrode[ntInd];i++) {
                            waveform[i][pointCopyInd] = spike_out_packets[snippetLocation][startInd+i];
                        }
                        snippetLocation = (snippetLocation+1)%ringBufferSize;
                    }

                    //package the spike waveform along with required info (timestamp, ntrode index, etc) and add to the list of available spikes.
                    SpikeWaveform sp(waveform,nTrodeList[ntInd],ntrodeTriggerPeakTimestamp[ntInd],numChannelsPerNtrode[ntInd],numSamplesPerSpike);
                    availableSpikes.push_back(sp);
                }

            } //END: the entire waveform window has finished streaming in
        } //END: This ntrode is already in a triggered (extraction) state
    } //END: Loop through each ntrode for spike extraction

}

const int16_t* VectorizedNeuralDataHandler::getEntirePacket(DataBand d, int& numSamples)
{

    numSamples = numchans;
    if (d == RAW_UNREFERENCED) {
        return raw_unref;
    } else if (d == RAW_REFERENCED) {
        return raw_ref;
    } else if (d == LFP) {
        return lfp;
    }  else if (d == STIMULATION) {
        return stim;
    }
}

const int16_t *VectorizedNeuralDataHandler::getNTrodeSamples(int nTrodeIndex, AbstractNeuralDataHandler::DataBand d)
{


    if (d == RAW_UNREFERENCED) {
        return raw_unref + nt2Ind[nTrodeIndex];
    } else if (d == RAW_REFERENCED) {
        return raw_ref + nt2Ind[nTrodeIndex];
    } else if (d == LFP) {      
        return lfp + nt2Ind[nTrodeIndex];
    } else if (d == SPIKE) {
        //return spike + nt2Ind[nTrodeIndex];
        return spike_out_packets[ringBufferWriteHead]+nt2Ind[nTrodeIndex];
        //return spike + ringBufferWriteHead + nt2Ind[nTrodeIndex];
    } else if (d == STIMULATION) {
        return stim + nt2Ind[nTrodeIndex];
    }

    return nullptr;
}

void VectorizedNeuralDataHandler::processDisplaySamples(bool newbin)
{

    for(int i = 0; i < numchans_rounded16; i+=MAXPROCESSORSIZE){
        int pnum = i/16; //since each processor deals with 16 at a time
        dispprocs[pnum].neuralSamples(raw_ref_float+i, spike_out_float+i, lfp_out_float+i, stim_float+i, minValues+i, maxValues+i, newbin);
    }
}

const float* VectorizedNeuralDataHandler::getMinDisplaySamples(int nTrodeIndex)
{
    return minValues + nt2Ind[nTrodeIndex];
}

const float* VectorizedNeuralDataHandler::getMaxDisplaySamples(int nTrodeIndex)
{
    return maxValues + nt2Ind[nTrodeIndex];
}

void VectorizedNeuralDataHandler::stepPreviousSample()
{
    if (numSamplesLeftToInterp > 0) {
        //Do linear interpolation for each of the neural channels

        timestamp_latest++;

        float interpolatedDataPtFloat;
        float lastDataPtFloat;
        float nextDataPtFloat;

        ringBufferWriteHead = (ringBufferWriteHead + 1)%ringBufferSize; //advance the ring buffer write head
        ringBufferStartWaveformHead = (ringBufferStartWaveformHead + 1)%ringBufferSize;


        for(int i = 0; i < numchans_rounded16; i+=MAXPROCESSORSIZE){
            //processor num
            int pnum = i/MAXPROCESSORSIZE; //since each processor deals with 16 at a time


            for (int interpInd = 0; interpInd < MAXPROCESSORSIZE; interpInd++) {
                //Perform a simple linear interpolation between the last read point and the next one
                lastDataPtFloat = *(raw_ref_float_interp+i+interpInd); //+ ((double)(nextReferencedDataVal-lastReferencedDataVal) / numSamplesLeftToInterp);
                nextDataPtFloat = *(raw_ref_float+i+interpInd);
                interpolatedDataPtFloat = lastDataPtFloat + (nextDataPtFloat-lastDataPtFloat)/numSamplesLeftToInterp;
                *(raw_ref_float_interp+i+interpInd) = interpolatedDataPtFloat;

                lastDataPtFloat = *(lfp_ref_float_interp+i+interpInd); //+ ((double)(nextReferencedDataVal-lastReferencedDataVal) / numSamplesLeftToInterp);
                nextDataPtFloat = *(lfp_ref_float+i+interpInd);
                interpolatedDataPtFloat = lastDataPtFloat + (nextDataPtFloat-lastDataPtFloat)/numSamplesLeftToInterp;
                *(lfp_ref_float_interp+i+interpInd) = interpolatedDataPtFloat;

                lastDataPtFloat = *(spike_ref_float_interp+i+interpInd); //+ ((double)(nextReferencedDataVal-lastReferencedDataVal) / numSamplesLeftToInterp);
                nextDataPtFloat = *(spike_ref_float+i+interpInd);
                interpolatedDataPtFloat = lastDataPtFloat + (nextDataPtFloat-lastDataPtFloat)/numSamplesLeftToInterp;
                *(spike_ref_float_interp+i+interpInd) = interpolatedDataPtFloat;


            }

            //calculate LFP (lowpass) data from broadband, full rate data
            lfpprocs[pnum].newSamples(lfp_ref_float_interp+i, lfp_out_float+i);

            //The RAW band is converted back to int16 for the output
            AVXFloatToShort::convertToShort(raw_ref_float_interp+i, raw_ref+i);


            //calculate spike (bandpass) data, and invert signal if the invert setting is turned on
            spikeprocs[pnum].newSamples(spike_ref_float_interp+i, spike_out_float+i);
            if (spikeInvert) InvertProcessor::invert(spike_out_float+i,spike_out_float+i);

            //Any threshold crossings will be used below to initiate spike snippet extraction. The
            //calculation of whether the signal is above or below the spike detection threshold
            //is calculated here in a vectorized operation.
            ThresholdProcessor::checkThreshCrossings(spike_out_float+i, spike_thresh_float+i, spikeChannelAboveThresh+i);


            //The spike band is converted back to int16 values and stored in a ring buffer. The buffer
            //is used below for spike extraction
            AVXFloatToShort::convertToShort(spike_out_float+i, spike_out_packets[ringBufferWriteHead]+i);

            //The LFP band is also converted back to int16
            AVXFloatToShort::convertToShort(lfp_out_float+i, lfp+i);

        }

        //Now we perform spike snippet extraction of any spikes that are ready for processing.
        processSpikes();


        numSamplesLeftToInterp--;
    }
}

void VectorizedNeuralDataHandler::updateChannels(){
    //qDebug() << "Updating data handler channels";
    for(int n = 0; n < nTrodeList.length(); ++n){
        int nt = nTrodeList[n];
        auto const ntsetting = conf_ptrs.spikeConf->ntrodes[nt];
        for(int c = 0; c < ntsetting->hw_chan.length(); ++c){
            spike_thresh_float[nt2Ind[n] + c] = (float)conf_ptrs.spikeConf->ntrodes[nt]->thresh_rangeconvert[0];
            channelTriggersOn[nt2Ind[n] + c] = conf_ptrs.spikeConf->ntrodes[nt]->triggerOn[c];           
            chanByteIndices[nt2Ind[n] + c] = ntsetting->hw_chan[c] * sizeof(int16_t);
            refIndices[nt2Ind[n] + c] = conf_ptrs.spikeConf->ntrodes.refOfIndex(nt)->hw_chan[ntsetting->refChan] * sizeof(int16_t);
            grouprefIndices[nt2Ind[n] + c] = (ntsetting->refGroup -1) * sizeof(int16_t);
            if (grouprefIndices[nt2Ind[n] + c] < 0) {
                //This happens if the group ref is off. We can't have a negative index, so we change it to 0.
                grouprefIndices[nt2Ind[n] + c] = 0;
            }
            refOn[nt2Ind[n] + c] = ntsetting->refOn ? 0x80000000 : 0x00000000;
            lfpRefOn[nt2Ind[n] + c] = ntsetting->lfpRefOn ? 0x80000000 : 0x00000000;
            rawRefOn[nt2Ind[n] + c] = ntsetting->rawRefOn ? 0x80000000 : 0x00000000;
            grouprefOn[nt2Ind[n] + c] = ntsetting->groupRefOn ? 0x80000000 : 0x00000000;

            int ind = nt2Ind[n] + c;
//            notchprocs[ind/16].setBandwidth(ind%16, ntsetting->notchBW);
//            notchprocs[ind/16].setNotchFreq(ind%16, ntsetting->notchFreq);
            spikeprocs[ind/16].setBandPassFilter(ind%16, ntsetting->lowFilter, ntsetting->highFilter, ntsetting->filterOn);
            lfpprocs[ind/16].setLowPassFilter(ind%16, ntsetting->lfpHighFilter, ntsetting->lfpFilterOn);
            //lfpprocs[ind/16].setBandPassFilter(ind%16, ntsetting->lfpLowFilter, ntsetting->lfpHighFilter, ntsetting->lfpFilterOn);

            DisplayProcessor::DisplaySetting view;
            if(ntsetting->spikeViewMode){

                view = DisplayProcessor::DisplaySetting::spike;
            }
            else if(ntsetting->lfpViewMode){

                view = DisplayProcessor::DisplaySetting::lfp;
            }
            else if(ntsetting->stimViewMode){

                view = DisplayProcessor::DisplaySetting::stim;
            }
            else{
                view = DisplayProcessor::DisplaySetting::raw;
            }
            dispprocs[ind/16].setDisplay(ind%16, view);
        }
    }
}

void VectorizedNeuralDataHandler::updateFilters()
{
    VectorizedNeuralDataHandler::updateChannels();
}

void VectorizedNeuralDataHandler::updateChannelsRef()
{
    VectorizedNeuralDataHandler::updateChannels();
}

void VectorizedNeuralDataHandler::resetFilters()
{
    for(int i = 0; i < numprocessors; ++i){
        spikeprocs[i].reset();
        lfpprocs[i].reset();
    }
}

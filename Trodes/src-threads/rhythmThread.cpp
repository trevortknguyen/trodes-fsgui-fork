/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2015 Mattias Karlsson, Caleb Kemere

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "rhythmThread.h"
#include "globalObjects.h"
#include "QMessageBox"

RhythmRuntime::RhythmRuntime(QObject *parent) {
    boardInitialized = false;
    fastSettleEnabled = false;
    toggleFastSettle.storeRelease(false);
}


void RhythmRuntime::Run() {
    if (!boardInitialized) {
        qDebug() << "Trying to run Run loop without initialized board";
        return;
    }

    PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + (2*hardwareConf->headerSize); //Packet size in bytes
    int t, channel, stream, block;
    double dTimestamp = 0.0;
    rawData.writeIdx = 0; // location where we're currently writing

    int numDataStreams = rhythmInterfaceBoard->getNumEnabledDataStreams();
    size_t packetSize = Rhd2000DataBlock::calculateDataBlockSizeInWords(numDataStreams);

    qDebug() << "Number of channels " << hardwareConf->NCHAN << ". Number of data streams" <<
                rhythmInterfaceBoard->getNumEnabledDataStreams() << ". Data block size: " <<
                packetSize;

    double samplePeriod = 1.0 / boardSampleRate;
    int fifoCapacity = Rhd2000EvalBoard::fifoCapacityInWords();

    int dataBlockSize = Rhd2000DataBlock::calculateDataBlockSizeInWords(
                rhythmInterfaceBoard->getNumEnabledDataStreams());



    quitNow = false;
    qDebug() << "Rhythm handle events loop running....";

    acquiring = true;

    rhythmInterfaceBoard->setContinuousRunMode(true);
    rhythmInterfaceBoard->run();

    while (quitNow != true) {
        bool newDataReady = rhythmInterfaceBoard->readDataBlocks(numUsbBlocksToRead, dataQueue);    // takes about 17 ms at 30 kS/s with 256 amplifiers
        if (!newDataReady) {
            QThread::usleep(20);
            continue;
        } else {

            // Check the number of words stored in the Opal Kelly USB interface FIFO.
            int wordsInFifo = rhythmInterfaceBoard->numWordsInFifo();
            double latency = 1000.0 * Rhd2000DataBlock::getSamplesPerDataBlock() *
                    (wordsInFifo / dataBlockSize) * samplePeriod;

            double fifoPercentageFull = 100.0 * wordsInFifo / fifoCapacity;

            // Alert the user if the number of words in the FIFO is getting to be significant or nearing FIFO capacity.
            if (latency > 50.0) {
                qDebug() << "Fifo approaching capacitiy. Currently full percentage: " << fifoPercentageFull;
            }

            // Read waveform data from USB interface board.
            for (block = 0; block < numUsbBlocksToRead; ++block) {
                Rhd2000DataBlock *dataBlock = &dataQueue.front();
                for (t = 0; t < SAMPLES_PER_DATA_BLOCK; ++t) {
                    //Process time stamp
                    dTimestamp += 1.0;
                    currentTimeStamp = ((qint32) dataBlock->timeStamp[t]);
                    rawData.timestamps[rawData.writeIdx] = ((qint32) dataBlock->timeStamp[t]);
                    rawData.dTime[rawData.writeIdx] = dTimestamp;
                    if (benchConfig->isRecordingSysTime()) {
                        rawData.sysTimestamps[rawData.writeIdx] = QTime::currentTime().msecsSinceStartOfDay();
                    }

                    int wtIdx = rawData.writeIdx*hardwareConf->NCHAN;
                    // Load  RHD2000 amplifier waveforms (scaled @ default)
                    // NOTE THAT FOR SPIKE-GADGETS COMPATIBILITY, DATA ARE ORDERED BY:
                    //  Card 0 Channel 0, Card 1 Channel 0, etc..
                    int channelOffset = 0;
                    for (channel = 0; channel < 32; ++channel) {
                        for (stream = 0; stream < numDataStreams; ++stream) {
                            rawData.data[rawData.writeIdx*hardwareConf->NCHAN + channelOffset + stream] =
                                    dataBlock->amplifierData[stream][channel][t] - 32768;
                        }
                        channelOffset += numDataStreams;
                    }

                    if (hardwareConf->headerSize >= 10) {
                        // Load and scale USB interface board ADC waveforms (sampled at amplifier sampling rate)
                        for (channel = 0; channel < 8; channel++) {
                            // ADC waveform units = volts
                            rawData.digitalInfo[rawData.writeIdx*hardwareConf->headerSize + channel] =
                                    0.000050354 * dataBlock->boardAdcData[channel][t];
                        }

                        // Load USB interface board digital input and output waveforms
                        rawData.digitalInfo[rawData.writeIdx*hardwareConf->headerSize + 8] =
                                dataBlock->ttlIn[t];
                        // rawData.digitalInfo[rawData.writeIdx*hardwareConf->headerSize + 9] =
                        //         dataQueue.front().ttlOut[t];

                        // Load and scale RHD2000 auxiliary input waveforms
                        // (sampled at 1/4 amplifier sampling rate)
                        for (stream = 0; stream < numDataStreams; ++stream) {
                            // Auxiliary input waveform units = volts
                            rawData.digitalInfo[rawData.writeIdx*hardwareConf->headerSize + 10] =
                                    0.0000374 * dataBlock->auxiliaryData[stream][1][t];
                        }
                    }

                    publishDataPacket();

                    //Advance the write markers and release a semaphore
                    writeMarkerMutex.lock();
                    rawData.writeIdx = (rawData.writeIdx + 1) % EEG_BUFFER_SIZE;
                    rawDataWritten++;
                    writeMarkerMutex.unlock();


                    for (int a = 0; a < rawDataAvailable.length(); a++)
                        rawDataAvailable[a]->release(1);
                }

                //                // We are done with this Rhd2000DataBlock object; remove it from dataQueue
                dataQueue.pop();
            }
        }
        if (toggleFastSettle & fastSettleEnabled) {
            fastSettleEnabled = false;
            enableFastSettle(fastSettleEnabled);
            toggleFastSettle.storeRelease(false);
        }
        else if (toggleFastSettle & !fastSettleEnabled) {
            fastSettleEnabled = true;
            enableFastSettle(fastSettleEnabled);
            toggleFastSettle.storeRelease(false);
        }
    }

    // Stop data acquisition (when running == false)
    rhythmInterfaceBoard->setContinuousRunMode(false);
    rhythmInterfaceBoard->setMaxTimeStep(0);

    // Flush USB FIFO on XEM6010
    rhythmInterfaceBoard->flush();

    int ledArray2[8] = {1, 0, 0, 0, 0, 0, 0, 0};
    rhythmInterfaceBoard->setLedDisplay(ledArray2);


    // If external control of chip auxiliary output pins was enabled, make sure
    // all auxout pins are turned off when acquisition stops.

    acquiring = false;
    emit stopped();
}

int RhythmRuntime::initializeBoard(void)
{
    qDebug() << "Initializing";

    // Setup Rhythm interface
    rhythmInterfaceBoard = new Rhd2000EvalBoard;

    // Open Opal Kelly XEM6010 board.
    int errorCode = rhythmInterfaceBoard->open();

    if (errorCode == -1) {
        //emit finished();
        //delete rhythmInterfaceBoard;
        return FRONTPANEL_LIB_LOAD_ERROR;
    }
    else if (errorCode == -2) {
        //emit finished();
        //delete rhythmInterfaceBoard;
        return XEM6010_NOT_FOUND;
    }
    else if (errorCode < 0) {
        //emit finished();
        //delete rhythmInterfaceBoard;
        return OTHER_ERROR;
    }

    deviceFound = true;

    // upload bitfile and restore default settings
    qDebug() << "Intializing board.";

    QString bitfilename =
            QString(QCoreApplication::applicationDirPath() + "/main.bit");

    if (!QFile::exists(bitfilename)) {
        emit finished();
        return BITFILE_NOT_FOUND;
    }
    else if (!rhythmInterfaceBoard->uploadFpgaBitfile(bitfilename.toStdString())) {
        emit finished();
        return BITFILE_UPLOAD_ERROR; // don't initialize
    }
    // Initialize the board
    rhythmInterfaceBoard->initialize();
    // This applies the following settings:
    //  - sample rate to 30 kHz
    //  - aux command banks to zero
    //  - aux command lengths to zero
    //  - continuous run mode to 'true'
    //  - maxTimeStep to 2^32 - 1
    //  - all cable lengths to 3 feet
    //  - dspSettle to 'false'
    //  - data source mapping as 0->PortA1, 1->PortB1, 2->PortC1, 3->PortD1, etc.
    //  - enables all data streams
    //  - clears the ttlOut
    //  - disables all DACs and sets gain to 0


    // Read 4-bit board mode.
    rhythmInterfaceBoard->getBoardMode();

    changeSampleRate(hardwareConf->sourceSamplingRate);

    // Select RAM Bank 0 for AuxCmd3 initially, so the ADC is calibrated.
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortA, Rhd2000EvalBoard::AuxCmd3, 0);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortB, Rhd2000EvalBoard::AuxCmd3, 0);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortC, Rhd2000EvalBoard::AuxCmd3, 0);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortD, Rhd2000EvalBoard::AuxCmd3, 0);

    // Since our longest command sequence is 60 commands, we run the SPI
    // interface for 60 samples.
    rhythmInterfaceBoard->setMaxTimeStep(60);
    rhythmInterfaceBoard->setContinuousRunMode(false);

    // Start SPI interface.
    rhythmInterfaceBoard->run();

    // Wait for the 60-sample run to complete.
    while (rhythmInterfaceBoard->isRunning()) {
    }

    // Read the resulting single data block from the USB interface.
    Rhd2000DataBlock *dataBlock = new Rhd2000DataBlock(rhythmInterfaceBoard->getNumEnabledDataStreams());
    rhythmInterfaceBoard->readDataBlock(dataBlock);

    // We don't need to do anything with this data block; it was used to configure
    // the RHD2000 amplifier chips and to run ADC calibration.
    delete dataBlock;

    // Now that ADC calibration has been performed, we switch to the command sequence
    // that does not execute ADC calibration.
    bool fastSettleEnabled = false;
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortA, Rhd2000EvalBoard::AuxCmd3,
                                               fastSettleEnabled ? 2 : 1);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortB, Rhd2000EvalBoard::AuxCmd3,
                                               fastSettleEnabled ? 2 : 1);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortC, Rhd2000EvalBoard::AuxCmd3,
                                               fastSettleEnabled ? 2 : 1);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortD, Rhd2000EvalBoard::AuxCmd3,
                                               fastSettleEnabled ? 2 : 1);

    // Set default configuration for all eight DACs on interface board.
    rhythmInterfaceBoard->enableDac(0, false);
    rhythmInterfaceBoard->enableDac(1, false);
    rhythmInterfaceBoard->enableDac(2, false);
    rhythmInterfaceBoard->enableDac(3, false);
    rhythmInterfaceBoard->enableDac(4, false);
    rhythmInterfaceBoard->enableDac(5, false);
    rhythmInterfaceBoard->enableDac(6, false);
    rhythmInterfaceBoard->enableDac(7, false);
    rhythmInterfaceBoard->selectDacDataStream(0, 8);   // Initially point DACs to DacManual1 input
    rhythmInterfaceBoard->selectDacDataStream(1, 8);
    rhythmInterfaceBoard->selectDacDataStream(2, 8);
    rhythmInterfaceBoard->selectDacDataStream(3, 8);
    rhythmInterfaceBoard->selectDacDataStream(4, 8);
    rhythmInterfaceBoard->selectDacDataStream(5, 8);
    rhythmInterfaceBoard->selectDacDataStream(6, 8);
    rhythmInterfaceBoard->selectDacDataStream(7, 8);
    rhythmInterfaceBoard->selectDacDataChannel(0, 0);
    rhythmInterfaceBoard->selectDacDataChannel(1, 0);
    rhythmInterfaceBoard->selectDacDataChannel(2, 0);
    rhythmInterfaceBoard->selectDacDataChannel(3, 0);
    rhythmInterfaceBoard->selectDacDataChannel(4, 0);
    rhythmInterfaceBoard->selectDacDataChannel(5, 0);
    rhythmInterfaceBoard->selectDacDataChannel(6, 0);
    rhythmInterfaceBoard->selectDacDataChannel(7, 0);
    rhythmInterfaceBoard->setDacManual(32768);
    rhythmInterfaceBoard->setDacGain(0);
    rhythmInterfaceBoard->setAudioNoiseSuppress(0);

    rhythmInterfaceBoard->setCableLengthMeters(Rhd2000EvalBoard::PortA, 0.0);
    rhythmInterfaceBoard->setCableLengthMeters(Rhd2000EvalBoard::PortB, 0.0);
    rhythmInterfaceBoard->setCableLengthMeters(Rhd2000EvalBoard::PortC, 0.0);
    rhythmInterfaceBoard->setCableLengthMeters(Rhd2000EvalBoard::PortD, 0.0);


    // Set sample rate
    changeSampleRate(hardwareConf->sourceSamplingRate);

    int totalChannelCount = scanPorts();

    if (totalChannelCount == hardwareConf->NCHAN) {
        boardInitialized = true;
        return INITIALIZATION_SUCCESS;
    }
    else if (totalChannelCount > hardwareConf->NCHAN) {
        qDebug() << "Warning - more channels [" << totalChannelCount <<
                    "] detected than expected [" << hardwareConf->NCHAN << "]";
        boardInitialized = true;
        return INITIALIZATION_SUCCESS;
    }
    else {
        boardInitialized = false;
        return TOO_FEW_CHANNELS;
    }

}

void RhythmRuntime::closeBoard()
{
    // We now want to disconnect from the interface, stop the thread, and clean up
    // the finished signal will tell the worker thread to quit and will deleteLater
    // me

    delete rhythmInterfaceBoard;
    emit finished();

}


int RhythmRuntime::scanPorts()
{
    int stream;
    //int delay, id, i, channel, port, auxName, vddName;
    //int register59Value;
    int delay, id;
    int register59Value;
    int numChannelsOnPort[4] = {0, 0, 0, 0};
    QVector<int> portIndex, portIndexOld;

    portIndex.resize(MAX_NUM_DATA_STREAMS);
    portIndexOld.resize(MAX_NUM_DATA_STREAMS);
    chipId.resize(MAX_NUM_DATA_STREAMS);

    chipId.fill(-1);
    portIndexOld.fill(-1);
    portIndex.fill(-1);

    Rhd2000EvalBoard::BoardDataSource initStreamPorts[8] =
    {
        Rhd2000EvalBoard::PortA1,
        Rhd2000EvalBoard::PortA2,
        Rhd2000EvalBoard::PortB1,
        Rhd2000EvalBoard::PortB2,
        Rhd2000EvalBoard::PortC1,
        Rhd2000EvalBoard::PortC2,
        Rhd2000EvalBoard::PortD1,
        Rhd2000EvalBoard::PortD2
    };

    Rhd2000EvalBoard::BoardDataSource initStreamDdrPorts[8] =
    {
        Rhd2000EvalBoard::PortA1Ddr,
        Rhd2000EvalBoard::PortA2Ddr,
        Rhd2000EvalBoard::PortB1Ddr,
        Rhd2000EvalBoard::PortB2Ddr,
        Rhd2000EvalBoard::PortC1Ddr,
        Rhd2000EvalBoard::PortC2Ddr,
        Rhd2000EvalBoard::PortD1Ddr,
        Rhd2000EvalBoard::PortD2Ddr
    };

    if (!deviceFound) //Safety to avoid crashes if board not present
    {
        return 0;
    }

    qDebug() << "Scanning for connected chips";

    // Set sampling rate to highest value for maximum temporal resolution.
    changeSampleRate(30000);

    // Enable all data streams, and set sources to cover one or two chips on Ports A-D.
    rhythmInterfaceBoard->setDataSource(0, initStreamPorts[0]);
    rhythmInterfaceBoard->setDataSource(1, initStreamPorts[1]);
    rhythmInterfaceBoard->setDataSource(2, initStreamPorts[2]);
    rhythmInterfaceBoard->setDataSource(3, initStreamPorts[3]);
    rhythmInterfaceBoard->setDataSource(4, initStreamPorts[4]);
    rhythmInterfaceBoard->setDataSource(5, initStreamPorts[5]);
    rhythmInterfaceBoard->setDataSource(6, initStreamPorts[6]);
    rhythmInterfaceBoard->setDataSource(7, initStreamPorts[7]);

    portIndexOld[0] = 0;
    portIndexOld[1] = 0;
    portIndexOld[2] = 1;
    portIndexOld[3] = 1;
    portIndexOld[4] = 2;
    portIndexOld[5] = 2;
    portIndexOld[6] = 3;
    portIndexOld[7] = 3;

    rhythmInterfaceBoard->enableDataStream(0, true);
    rhythmInterfaceBoard->enableDataStream(1, true);
    rhythmInterfaceBoard->enableDataStream(2, true);
    rhythmInterfaceBoard->enableDataStream(3, true);
    rhythmInterfaceBoard->enableDataStream(4, true);
    rhythmInterfaceBoard->enableDataStream(5, true);
    rhythmInterfaceBoard->enableDataStream(6, true);
    rhythmInterfaceBoard->enableDataStream(7, true);

    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortA,
                                               Rhd2000EvalBoard::AuxCmd3, 0);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortB,
                                               Rhd2000EvalBoard::AuxCmd3, 0);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortC,
                                               Rhd2000EvalBoard::AuxCmd3, 0);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortD,
                                               Rhd2000EvalBoard::AuxCmd3, 0);

    // Since our longest command sequence is 60 commands, we run the SPI
    // interface for 60 samples.
    rhythmInterfaceBoard->setMaxTimeStep(60);
    rhythmInterfaceBoard->setContinuousRunMode(false);

    Rhd2000DataBlock *dataBlock =
            new Rhd2000DataBlock(rhythmInterfaceBoard->getNumEnabledDataStreams());

    QVector<int> sumGoodDelays(MAX_NUM_DATA_STREAMS, 0);
    QVector<int> indexFirstGoodDelay(MAX_NUM_DATA_STREAMS, -1);
    QVector<int> indexSecondGoodDelay(MAX_NUM_DATA_STREAMS, -1);
    QVector<int> chipIdOld(MAX_NUM_DATA_STREAMS, -1);


    // Run SPI command sequence at all 16 possible FPGA MISO delay settings
    // to find optimum delay for each SPI interface cable.
    for (delay = 0; delay < 16; ++delay) {
        rhythmInterfaceBoard->setCableDelay(Rhd2000EvalBoard::PortA, delay);
        rhythmInterfaceBoard->setCableDelay(Rhd2000EvalBoard::PortB, delay);
        rhythmInterfaceBoard->setCableDelay(Rhd2000EvalBoard::PortC, delay);
        rhythmInterfaceBoard->setCableDelay(Rhd2000EvalBoard::PortD, delay);

        // Start SPI interface.
        rhythmInterfaceBoard->run();

        // Wait for the 60-sample run to complete.
        while (rhythmInterfaceBoard->isRunning()) {
            qApp->processEvents();
        }

        // Read the resulting single data block from the USB interface.
        rhythmInterfaceBoard->readDataBlock(dataBlock);

        // Read the Intan chip ID number from each RHD2000 chip found.
        // Record delay settings that yield good communication with the chip.
        for (stream = 0; stream < MAX_NUM_DATA_STREAMS; ++stream) {
            id = deviceId(dataBlock, stream, register59Value);

            if (id == CHIP_ID_RHD2132 || id == CHIP_ID_RHD2216 ||
                    (id == CHIP_ID_RHD2164 && register59Value == REGISTER_59_MISO_A)) {
                qDebug() << "Delay: " << delay << " on stream " << stream << " is good.";
                sumGoodDelays[stream] = sumGoodDelays[stream] + 1;
                if (indexFirstGoodDelay[stream] == -1) {
                    indexFirstGoodDelay[stream] = delay;
                    chipIdOld[stream] = id;
                } else if (indexSecondGoodDelay[stream] == -1) {
                    indexSecondGoodDelay[stream] = delay;
                    chipIdOld[stream] = id;
                }
            }
        }
    }

    delete dataBlock;

    // Set cable delay settings that yield good communication with each
    // RHD2000 chip.
    QVector<int> optimumDelay(MAX_NUM_DATA_STREAMS, 0);
    for (int stream = 0; stream < MAX_NUM_DATA_STREAMS; ++stream) {
        if (sumGoodDelays[stream] == 1 || sumGoodDelays[stream] == 2) {
            optimumDelay[stream] = indexFirstGoodDelay[stream];
        } else if (sumGoodDelays[stream] > 2) {
            optimumDelay[stream] = indexSecondGoodDelay[stream];
        }
    }

    rhythmInterfaceBoard->setCableDelay(Rhd2000EvalBoard::PortA,
                                        qMax(optimumDelay[0], optimumDelay[1]));
    rhythmInterfaceBoard->setCableDelay(Rhd2000EvalBoard::PortB,
                                        qMax(optimumDelay[2], optimumDelay[3]));
    rhythmInterfaceBoard->setCableDelay(Rhd2000EvalBoard::PortC,
                                        qMax(optimumDelay[4], optimumDelay[5]));
    rhythmInterfaceBoard->setCableDelay(Rhd2000EvalBoard::PortD,
                                        qMax(optimumDelay[6], optimumDelay[7]));

    qDebug() << "Port A cable delay: " << qMax(optimumDelay[0], optimumDelay[1]);
    qDebug()  << "Port B cable delay: " << qMax(optimumDelay[2], optimumDelay[3]);
    qDebug()  << "Port C cable delay: " << qMax(optimumDelay[4], optimumDelay[5]);
    qDebug()  << "Port D cable delay: " << qMax(optimumDelay[6], optimumDelay[7]);


    cableLengthPortA =
            rhythmInterfaceBoard->estimateCableLengthMeters(qMax(optimumDelay[0], optimumDelay[1]));
    cableLengthPortB =
            rhythmInterfaceBoard->estimateCableLengthMeters(qMax(optimumDelay[2], optimumDelay[3]));
    cableLengthPortC =
            rhythmInterfaceBoard->estimateCableLengthMeters(qMax(optimumDelay[4], optimumDelay[5]));
    cableLengthPortD =
            rhythmInterfaceBoard->estimateCableLengthMeters(qMax(optimumDelay[6], optimumDelay[7]));

    // // ----

    // Now that we know which RHD2000 amplifier chips are plugged into each SPI port,
    // add up the total number of amplifier channels on each port and calcualate the number
    // of data streams necessary to convey this data over the USB interface.
    int numStreamsRequired = 0;
    int totalChannelCount = 0;
    bool rhd2216ChipPresent = false;
    for (stream = 0; stream < MAX_NUM_DATA_STREAMS; ++stream) {
        if (chipIdOld[stream] == CHIP_ID_RHD2216) {
            numStreamsRequired++;
            if (numStreamsRequired <= MAX_NUM_DATA_STREAMS) {
                numChannelsOnPort[portIndexOld[stream]] += 16;
                totalChannelCount += 16;
            }
            rhd2216ChipPresent = true;
        }
        if (chipIdOld[stream] == CHIP_ID_RHD2132) {
            numStreamsRequired++;
            if (numStreamsRequired <= MAX_NUM_DATA_STREAMS) {
                numChannelsOnPort[portIndexOld[stream]] += 32;
                totalChannelCount += 32;
            }
        }
        if (chipIdOld[stream] == CHIP_ID_RHD2164) {
            numStreamsRequired += 2;
            if (numStreamsRequired <= MAX_NUM_DATA_STREAMS) {
                numChannelsOnPort[portIndexOld[stream]] += 64;
                totalChannelCount += 64;
            }
        }
    }

    /*
    // If the user plugs in more chips than the USB interface can support, throw
    // up a warning that not all channels will be displayed.
    if (numStreamsRequired > 8) {}
    */

    // Reconfigure USB data streams in consecutive order to accommodate all connected chips.
    stream = 0;
    for (int oldStream = 0; oldStream < MAX_NUM_DATA_STREAMS; ++oldStream) {
        if ((chipIdOld[oldStream] == CHIP_ID_RHD2216) && (stream < MAX_NUM_DATA_STREAMS)) {
            chipId[stream] = CHIP_ID_RHD2216;
            portIndex[stream] = portIndexOld[oldStream];
            rhythmInterfaceBoard->enableDataStream(stream, true);
            rhythmInterfaceBoard->setDataSource(stream, initStreamPorts[oldStream]);
            stream++;
        } else if ((chipIdOld[oldStream] == CHIP_ID_RHD2132) && (stream < MAX_NUM_DATA_STREAMS)) {
            chipId[stream] = CHIP_ID_RHD2132;
            portIndex[stream] = portIndexOld[oldStream];
            rhythmInterfaceBoard->enableDataStream(stream, true);
            rhythmInterfaceBoard->setDataSource(stream, initStreamPorts[oldStream]);
            stream++ ;
        } else if ((chipIdOld[oldStream] == CHIP_ID_RHD2164) && (stream < MAX_NUM_DATA_STREAMS - 1)) {
            chipId[stream] = CHIP_ID_RHD2164;
            chipId[stream + 1] =  CHIP_ID_RHD2164_B;
            portIndex[stream] = portIndexOld[oldStream];
            portIndex[stream + 1] = portIndexOld[oldStream];
            rhythmInterfaceBoard->enableDataStream(stream, true);
            rhythmInterfaceBoard->enableDataStream(stream + 1, true);
            rhythmInterfaceBoard->setDataSource(stream, initStreamPorts[oldStream]);
            rhythmInterfaceBoard->setDataSource(stream + 1, initStreamDdrPorts[oldStream]);
            stream += 2;
        }
    }

    // Disable unused data streams.
    for (; stream < MAX_NUM_DATA_STREAMS; ++stream) {
        rhythmInterfaceBoard->enableDataStream(stream, false);
    }

    // Set sample rate
    changeSampleRate(hardwareConf->sourceSamplingRate);

    // TRODES UPDATE
    // Add =auxiliary input channels and supply voltage channels for each chip.

    return totalChannelCount;
}

int RhythmRuntime::deviceId(Rhd2000DataBlock* dataBlock, int stream, int& register59Value)
{
    bool intanChipPresent;

    // First, check ROM registers 32-36 to verify that they hold 'INTAN', and
    // the initial chip name ROM registers 24-26 that hold 'RHD'.
    // This is just used to verify that we are getting good data over the SPI
    // communication channel.
    intanChipPresent = ((char) dataBlock->auxiliaryData[stream][2][32] == 'I' &&
            (char) dataBlock->auxiliaryData[stream][2][33] == 'N' &&
            (char) dataBlock->auxiliaryData[stream][2][34] == 'T' &&
            (char) dataBlock->auxiliaryData[stream][2][35] == 'A' &&
            (char) dataBlock->auxiliaryData[stream][2][36] == 'N' &&
            (char) dataBlock->auxiliaryData[stream][2][24] == 'R' &&
            (char) dataBlock->auxiliaryData[stream][2][25] == 'H' &&
            (char) dataBlock->auxiliaryData[stream][2][26] == 'D');

    // If the SPI communication is bad, return -1.  Otherwise, return the Intan
    // chip ID number stored in ROM regstier 63.
    if (!intanChipPresent)
    {
        register59Value = -1;
        return -1;
    }
    else
    {
        register59Value = dataBlock->auxiliaryData[stream][2][23]; // Register 59
        return dataBlock->auxiliaryData[stream][2][19]; // chip ID (Register 63)
    }
}


// Enable or disable RHD2000 amplifier fast settle function.
void RhythmRuntime::enableFastSettle(bool fastSettleEnabled)
{
    if (boardInitialized) {
        rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortA, Rhd2000EvalBoard::AuxCmd3,
                                        fastSettleEnabled ? 2 : 1);
        rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortB, Rhd2000EvalBoard::AuxCmd3,
                                        fastSettleEnabled ? 2 : 1);
        rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortC, Rhd2000EvalBoard::AuxCmd3,
                                        fastSettleEnabled ? 2 : 1);
        rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortD, Rhd2000EvalBoard::AuxCmd3,
                                        fastSettleEnabled ? 2 : 1);
    }
}

// Change RHD2000 interface board amplifier sample rate.
// This function also updates the Aux1, Aux2, and Aux3 SPI command
// sequences that are used to set RAM registers on the RHD2000 chips.
void RhythmRuntime::changeSampleRate(int samplingRate)
{
    Rhd2000EvalBoard::AmplifierSampleRate sampleRate =
            Rhd2000EvalBoard::SampleRate1000Hz;

    // Note: numUsbBlocksToRead is set to give an approximate frame rate of
    // 30 Hz for most sampling rates.

    switch (samplingRate) {
    case 1000:
        sampleRate = Rhd2000EvalBoard::SampleRate1000Hz;
        boardSampleRate = 1000.0;
        numUsbBlocksToRead = 1;
        break;
    case 1250:
        sampleRate = Rhd2000EvalBoard::SampleRate1250Hz;
        boardSampleRate = 1250.0;
        numUsbBlocksToRead = 1;
        break;
    case 1500:
        sampleRate = Rhd2000EvalBoard::SampleRate1500Hz;
        boardSampleRate = 1500.0;
        numUsbBlocksToRead = 1;
        break;
    case 2000:
        sampleRate = Rhd2000EvalBoard::SampleRate2000Hz;
        boardSampleRate = 2000.0;
        numUsbBlocksToRead = 1;
        break;
    case 2500:
        sampleRate = Rhd2000EvalBoard::SampleRate2500Hz;
        boardSampleRate = 2500.0;
        numUsbBlocksToRead = 1;
        break;
    case 3000:
        sampleRate = Rhd2000EvalBoard::SampleRate3000Hz;
        boardSampleRate = 3000.0;
        numUsbBlocksToRead = 2;
        break;
    case int(10000.0 / 3.0):
        sampleRate = Rhd2000EvalBoard::SampleRate3333Hz;
        boardSampleRate = 10000.0 / 3.0;
        numUsbBlocksToRead = 2;
        break;
    case 4000:
        sampleRate = Rhd2000EvalBoard::SampleRate4000Hz;
        boardSampleRate = 4000.0;
        numUsbBlocksToRead = 2;
        break;
    case 5000:
        sampleRate = Rhd2000EvalBoard::SampleRate5000Hz;
        boardSampleRate = 5000.0;
        numUsbBlocksToRead = 3;
        break;
    case 6250:
        sampleRate = Rhd2000EvalBoard::SampleRate6250Hz;
        boardSampleRate = 6250.0;
        numUsbBlocksToRead = 3;
        break;
    case 8000:
        sampleRate = Rhd2000EvalBoard::SampleRate8000Hz;
        boardSampleRate = 8000.0;
        numUsbBlocksToRead = 4;
        break;
    case 10000:
        sampleRate = Rhd2000EvalBoard::SampleRate10000Hz;
        boardSampleRate = 10000.0;
        numUsbBlocksToRead = 6;
        break;
    case 12500:
        sampleRate = Rhd2000EvalBoard::SampleRate12500Hz;
        boardSampleRate = 12500.0;
        numUsbBlocksToRead = 7;
        break;
    case 15000:
        sampleRate = Rhd2000EvalBoard::SampleRate15000Hz;
        boardSampleRate = 15000.0;
        numUsbBlocksToRead = 8;
        break;
    case 20000:
        sampleRate = Rhd2000EvalBoard::SampleRate20000Hz;
        boardSampleRate = 20000.0;
        numUsbBlocksToRead = 12;
        break;
    case 25000:
        sampleRate = Rhd2000EvalBoard::SampleRate25000Hz;
        boardSampleRate = 25000.0;
        numUsbBlocksToRead = 14;
        break;
    case 30000:
        sampleRate = Rhd2000EvalBoard::SampleRate30000Hz;
        boardSampleRate = 30000.0;
        numUsbBlocksToRead = 16;
        break;
    default:
        qDebug() << "Hardware sampling rate not selectable. Using 30 kHz.";
        sampleRate = Rhd2000EvalBoard::SampleRate30000Hz;
        boardSampleRate = 30000.0;
        numUsbBlocksToRead = 16;
        break;
    }

    // Set up an RHD2000 register object using this sample rate to
    // optimize MUX-related register settings.
    Rhd2000Registers chipRegisters(boardSampleRate);

    int commandSequenceLength;
    vector<int> commandList;

    rhythmInterfaceBoard->setSampleRate(sampleRate);

    // Now that we have set our sampling rate, we can set the MISO sampling delay
    // which is dependent on the sample rate.
    rhythmInterfaceBoard->setCableLengthMeters(Rhd2000EvalBoard::PortA, cableLengthPortA);
    rhythmInterfaceBoard->setCableLengthMeters(Rhd2000EvalBoard::PortB, cableLengthPortB);
    rhythmInterfaceBoard->setCableLengthMeters(Rhd2000EvalBoard::PortC, cableLengthPortC);
    rhythmInterfaceBoard->setCableLengthMeters(Rhd2000EvalBoard::PortD, cableLengthPortD);

    // Create a command list for the AuxCmd1 slot.  This command sequence will create a 250 Hz,
    // zero-amplitude sine wave (i.e., a flatline).  We will change this when we want to perform
    // impedance testing.
    // commandSequenceLength = chipRegisters.createCommandListZcheckDac(commandList, 250.0, 0.0);

    // Create a command list for the AuxCmd1 slot.  This command sequence will continuously
    // update Register 3, which controls the auxiliary digital output pin on each RHD2000 chip.
    // In concert with the v1.4 Rhythm FPGA code, this permits real-time control of the digital
    // output pin on chips on each SPI port.
    chipRegisters.setDigOutLow();   // Take auxiliary output out of HiZ mode.
    commandSequenceLength = chipRegisters.createCommandListUpdateDigOut(commandList);
    rhythmInterfaceBoard->uploadCommandList(commandList, Rhd2000EvalBoard::AuxCmd1, 0);
    rhythmInterfaceBoard->selectAuxCommandLength(Rhd2000EvalBoard::AuxCmd1, 0, commandSequenceLength - 1);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortA, Rhd2000EvalBoard::AuxCmd1, 0);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortB, Rhd2000EvalBoard::AuxCmd1, 0);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortC, Rhd2000EvalBoard::AuxCmd1, 0);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortD, Rhd2000EvalBoard::AuxCmd1, 0);

    // Next, we'll create a command list for the AuxCmd2 slot.  This command sequence
    // will sample the temperature sensor and other auxiliary ADC inputs.

    commandSequenceLength = chipRegisters.createCommandListTempSensor(commandList);
    rhythmInterfaceBoard->uploadCommandList(commandList, Rhd2000EvalBoard::AuxCmd2, 0);
    rhythmInterfaceBoard->selectAuxCommandLength(Rhd2000EvalBoard::AuxCmd2, 0, commandSequenceLength - 1);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortA, Rhd2000EvalBoard::AuxCmd2, 0);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortB, Rhd2000EvalBoard::AuxCmd2, 0);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortC, Rhd2000EvalBoard::AuxCmd2, 0);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortD, Rhd2000EvalBoard::AuxCmd2, 0);

    // For the AuxCmd3 slot, we will create three command sequences.  All sequences
    // will configure and read back the RHD2000 chip registers, but one sequence will
    // also run ADC calibration.  Another sequence will enable amplifier 'fast settle'.

    // Before generating register configuration command sequences, set amplifier
    // bandwidth paramters.
    double desiredLowerBandwidth = 0.1;
    double desiredUpperBandwidth = 7500.0;
    double desiredDspCutoffFreq = 1.0;
    bool dspEnabled = true;

    double actualDspCutoffFreq = chipRegisters.setDspCutoffFreq(desiredDspCutoffFreq);
    double actualLowerBandwidth = chipRegisters.setLowerBandwidth(desiredLowerBandwidth);
    double actualUpperBandwidth = chipRegisters.setUpperBandwidth(desiredUpperBandwidth);
    chipRegisters.enableDsp(dspEnabled);

    qDebug() << "Setting HighPass cutoff to" << actualLowerBandwidth <<
                ". DSP enabled with cutoff" << actualDspCutoffFreq;
    qDebug() << "Setting LowPass cutoff (UpperBandwidth) to" << actualUpperBandwidth;


    chipRegisters.createCommandListRegisterConfig(commandList, true);
    // Upload version with ADC calibration to AuxCmd3 RAM Bank 0.
    rhythmInterfaceBoard->uploadCommandList(commandList, Rhd2000EvalBoard::AuxCmd3, 0);
    rhythmInterfaceBoard->selectAuxCommandLength(Rhd2000EvalBoard::AuxCmd3, 0,
                                      commandSequenceLength - 1);

    commandSequenceLength = chipRegisters.createCommandListRegisterConfig(commandList, false);
    // Upload version with no ADC calibration to AuxCmd3 RAM Bank 1.
    rhythmInterfaceBoard->uploadCommandList(commandList, Rhd2000EvalBoard::AuxCmd3, 1);
    rhythmInterfaceBoard->selectAuxCommandLength(Rhd2000EvalBoard::AuxCmd3, 0,
                                      commandSequenceLength - 1);

    chipRegisters.setFastSettle(true);
    commandSequenceLength = chipRegisters.createCommandListRegisterConfig(commandList, false);
    // Upload version with fast settle enabled to AuxCmd3 RAM Bank 2.
    rhythmInterfaceBoard->uploadCommandList(commandList, Rhd2000EvalBoard::AuxCmd3, 2);
    rhythmInterfaceBoard->selectAuxCommandLength(Rhd2000EvalBoard::AuxCmd3, 0,
                                      commandSequenceLength - 1);
    chipRegisters.setFastSettle(false);

    bool fastSettleEnabled = false;

    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortA, Rhd2000EvalBoard::AuxCmd3,
                                    fastSettleEnabled ? 2 : 1);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortB, Rhd2000EvalBoard::AuxCmd3,
                                    fastSettleEnabled ? 2 : 1);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortC, Rhd2000EvalBoard::AuxCmd3,
                                    fastSettleEnabled ? 2 : 1);
    rhythmInterfaceBoard->selectAuxCommandBank(Rhd2000EvalBoard::PortD, Rhd2000EvalBoard::AuxCmd3,
                                    fastSettleEnabled ? 2 : 1);

}


RhythmInterface::RhythmInterface(QObject *) {
    state = SOURCE_STATE_NOT_CONNECTED;
    rhythmDataProcessor = NULL;
}

RhythmInterface::~RhythmInterface() {

}

void RhythmInterface::InitInterface() {

    //start the runtime thread for initialization
    rhythmDataProcessor = new RhythmRuntime(NULL);
    setUpThread(rhythmDataProcessor);

    int errorCode = RhythmRuntime::OTHER_ERROR;
    QMetaObject::invokeMethod(rhythmDataProcessor, "initializeBoard",
                              Qt::BlockingQueuedConnection,
                              Q_RETURN_ARG(int, errorCode));

    if (errorCode != RhythmRuntime::INITIALIZATION_SUCCESS) {
        qDebug() << "Failure to start Rhythm Runtime" << errorCode;
        switch (errorCode) {
        case RhythmRuntime::FRONTPANEL_LIB_LOAD_ERROR :
            QMessageBox::critical(NULL, tr("Cannot load Opal Kelly FrontPanel DLL"),
                                  tr("Opal Kelly USB drivers not installed.  "
                                     "<p>To use the RHD2000 Interface, click Ok, load the correct "
                                     "Opal Kelly drivers, then restart the application."));
            break;
        case RhythmRuntime::XEM6010_NOT_FOUND :
            QMessageBox::critical(NULL, tr("Intan RHD2000 USB Interface Board Not Found"),
                                  tr("Intan Technologies RHD2000 Interface not found on any USB port.  "
                                     "<p>To use the RHD2000 Interface, click Ok, connect the device "
                                     "to a USB port, then restart the application."));
            break;
        case RhythmRuntime::BITFILE_NOT_FOUND :
            QMessageBox::critical(NULL, tr("FPGA Configuration File Upload Error"),
                                  tr("Cannot find FPGA configuration file.  Make sure file main.bit "
                                     "is in the same directory as the executable file."));
            break;
        case RhythmRuntime::BITFILE_UPLOAD_ERROR :
            QMessageBox::critical(NULL, tr("FPGA Configuration File Upload Error"),
                                  tr("Cannot upload configuration file to FPGA though bitfile is present."));
            break;
        case RhythmRuntime::TOO_FEW_CHANNELS :
            QMessageBox::critical(NULL, tr("Interface Configuration Incompatibility"),
                                  tr("Fewer data acquisition channels than needed were detected."));
            break;

        }

        QMetaObject::invokeMethod(rhythmDataProcessor, "closeBoard",
                                  Qt::BlockingQueuedConnection);
        //emit stateChanged(SOURCE_STATE_NOT_CONNECTED);
        emit stateChanged(SOURCE_STATE_CONNECTERROR);
        return;
    }

    state = SOURCE_STATE_INITIALIZED;

    emit stateChanged(SOURCE_STATE_INITIALIZED);

}

void RhythmInterface::StartAcquisition(void) {
    static int runtimeStarted  = 0;

    if (runtimeStarted == 0) {
        emit startRuntime();
        qDebug() << "Told runtime to start";
        // COULD POLL "AQUIRING" NOW
    }

    emit acquisitionStarted();
    state = SOURCE_STATE_RUNNING;
    emit stateChanged(SOURCE_STATE_RUNNING);
}


void RhythmInterface::StopAcquisition(void) {

    // "RUN" METHOD KEEPS GOING IN INFINITE LOOP. WE STOP IT BY SETTING "QUITNOW"
    rhythmDataProcessor->quitNow = true;
    // COULD POLL ON AQUIRING HERE
    QThread::msleep(100);

    emit acquisitionStopped();
    state = SOURCE_STATE_INITIALIZED;
    emit stateChanged(SOURCE_STATE_INITIALIZED);
}

void RhythmInterface::CloseInterface(void) {
    if (state == SOURCE_STATE_RUNNING)
        StopAcquisition();

    if (state == SOURCE_STATE_INITIALIZED) {
        //if the runtime thread is running, kill it
        rhythmDataProcessor->deleteDedicatedDataPub();
        QMetaObject::invokeMethod(rhythmDataProcessor, "closeBoard",
                                  Qt::BlockingQueuedConnection);

        // SHOULD PROBABLY WAIT HERE FOR THREAD TO REPORT ITSELF DEAD

        emit stateChanged(SOURCE_STATE_NOT_CONNECTED);
    }
}


void RhythmInterface::SendSettleCommand() {
    if (state == SOURCE_STATE_RUNNING) {
        rhythmDataProcessor->toggleFastSettle.store(true);
    }
}

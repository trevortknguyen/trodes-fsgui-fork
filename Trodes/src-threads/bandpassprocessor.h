#ifndef BANDPASSPROCESSOR_H
#define BANDPASSPROCESSOR_H

#include <stdint.h>
#include <array>
#include "filtercoefcalculator.h"

//16 channel bandpass filter using single precision floating point
/*
Coefficients were obtained from http://www-users.cs.york.ac.uk/~fisher/mkfilter
BESSEL, MATCHED-Z TRANSFORM
***
ALL B COEFFICIENTS ARE COMBINED WITH THE 1/GAIN.
PRECALCULATING THE DIVISION AHEAD OF TIME SAVES CPU EFFORT
***
*/


class BandPassProcessor
{
public:
    BandPassProcessor();
    ~BandPassProcessor();

    //caller must guarantee output is aligned to 32-byte boundary
//    void setOutputBuffer(float *output);

    //must be 32-bit floats AND must be aligned along 32-byte boundary
    //Algorithm:
    //  * shift over filter history data
    //  * perform multiply operations
    //  * sum of multiply operations
    void newSamples(const float *data, float *output);
//    const float* getBandpassOutput() const {return bp_y4;}

    //Set filter params
    int setBandPassFilter(int ind, int low, int high, bool on=true);

    //set samplingrate
    int setSamplingRate(int samplingratehz);

    //reset filter
    void reset();

private:

    FilterCoefCalculator coefCalculator;



    float *bp_x0; //16 values
    float *bp_x1; //16 values
    float *bp_x2; //16 values

    //bandpass history and coefficients
    float *bp_y0; //16 values
    float *bp_y1; //16 values
    float *bp_y2; //16 values
    float *bp_y3; //16 values
    float *bp_b0; //16 values
    float *bp_b1; //16 values
    float *bp_b2; //16 values
    float *bp_a0; //16 values
    float *bp_a1; //16 values
    float *bp_a2; //16 values
    float *bp_a3; //16 values

    float *bp_y4; //

    //filter settings
    int bp_lo[16];
    int bp_hi[16];
    int freq;

    char *full_buffer;
};


class alignas(32) InvertProcessor
{
public:
    InvertProcessor();
    ~InvertProcessor();

    //caller must guarantee output is aligned to 32-byte boundary
    //must be 32-bit floats AND must be aligned along 32-byte boundary

    static void invert(const float *data, float *output);



private:


};


#endif // BANDPASSPROCESSOR_H

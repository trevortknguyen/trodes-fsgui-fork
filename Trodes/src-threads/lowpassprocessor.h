#ifndef LOWPASSPROCESSOR_H
#define LOWPASSPROCESSOR_H

#include "filtercoefcalculator.h"


//16 channel lowpass filter using single precision floating point
/*
Coefficients were obtained from http://www-users.cs.york.ac.uk/~fisher/mkfilter
BESSEL, MATCHED-Z TRANSFORM
***
ALL B COEFFICIENTS ARE COMBINED WITH THE 1/GAIN.
PRECALCULATING THE DIVISION AHEAD OF TIME SAVES CPU EFFORT
***

*/
class LowPassProcessor
{
public:
    LowPassProcessor();
    ~LowPassProcessor();
    //caller must guarantee output is aligned to 32-byte boundary
//    void setOutputBuffer(float *output);

    //must be 32-bit floats AND must be aligned along 32-byte boundary
    //Algorithm:
    //  * shift over filter history data
    //  * perform multiply operations
    //  * sum of multiply operations
    void newSamples(const float *data, float *output);
//    const float* getLowpassOutput() const {return lp_y2;}

    //Set filter params
    int setLowPassFilter(int ind, int high, bool on=true);

    //set sampling rate
    int setSamplingRate(int samplingratehz);

    //reset filter
    void reset();

private:
//    typedef std::array<float, 8> avxps;

    //lowpass history and coefficients
//    avxps lp_y[2][2];
//    float lp_y2[16];
//    avxps lp_b[2];
//    avxps lp_a[2][2];
    float *lp_y0;
    float *lp_y1;
    float *lp_y2;

    float *lp_a0;
    float *lp_a1;

    float *lp_b0;

    //filter settings
    int lp_hi[16];
    int freq;

    char *full_buffer;
};

#endif // LOWPASSPROCESSOR_H

#include "streamprocesshandlers.h"

SpikeWaveform::SpikeWaveform():
    waveData(nullptr),
    peakTime(0),
    ntrodeIndex(0),
    numChannels(0),
    numSamples(0)
{

}

SpikeWaveform::SpikeWaveform(const int16_t* const* waveform, const int &ntrodeInd, const uint32_t &pTime, const int &numChan, const int &numSamp):
    waveData(waveform),
    peakTime(pTime),
    ntrodeIndex(ntrodeInd),
    numChannels(numChan),
    numSamples(numSamp)
{

}

QVector<int16_t> SpikeWaveform::getPeaks() const
//returns a vector the is twice as long as the number of channels in the ntrode.
//The first half of the values are the channel peak values.  The second half are the channel
//minimum values.
{
    QVector<int16_t> rValue;
    rValue.fill(0,numChannels*2);
    for (int ch=0;ch<numChannels;ch++) {

        int16_t tempMax = 0;
        int16_t tempMin = 0;
        for (int s=0;s<numSamples;s++) {

            if (waveData[ch][s] > tempMax) tempMax=waveData[ch][s];
            if (waveData[ch][s] < tempMin) tempMin=waveData[ch][s];
        }
        rValue[ch] = tempMax;
        rValue[ch+numChannels] = tempMin;

    }

    return rValue;
}


void SpikeWaveform::clearMemory() const
{
    if (waveData == nullptr) return;

    for (int ch=0; ch<numChannels; ch++) {
        delete [] waveData[ch];
    }
    delete [] waveData;
}

AbstractNeuralDataHandler::AbstractNeuralDataHandler(TrodesConfigurationPointers conf, QList<int> nTrodeList_in, uint32_t extraSteps):
    conf_ptrs(conf)
{

    //headerSize =

    extraProcessingModes = 0;
    NCHAN = conf_ptrs.hardwareConf->NCHAN;
    latestDataHasNan = false;
    spikeInvert = true;
    interpMode = false; //default no interpolation
    samplesProcessed = 0;
    interpValuesReadyToFetch = false;
    maxInterpGap = 0;
    numSamplesLeftToInterp = 0;
    gapInDataOccured = false;
    extraProcessingModes = extraSteps;

    for (int i=0; i<nTrodeList_in.length();i++) {
        nTrodeList.push_back(nTrodeList_in.at(i));
    }



    //Some channels may be stimulation capable. We need to check them
    //at every packet to see if they are currently stimulating. We keep track
    //of the indices (in the gathered vectors) of the stim capable channels to reduce overhead of the check loop
    for (int n = 0; n < nTrodeList.length(); n++) {
        int nt = nTrodeList.at(n);
        for (int c = 0; c < conf_ptrs.spikeConf->ntrodes[nt]->hw_chan.length(); c++) {
            if (conf_ptrs.spikeConf->ntrodes[nt]->stimCapable[c]) {
                extraProcessingModes |= ExtraProcessorType::IntanStim;
            }
        }
    }

    //The "sensor" section contains asynchonous data sampled at lower rates.
    sensorSectionStartByte = -1;

    for (int i=0; i < conf.hardwareConf->devices.length();i++) {
        if ((conf.hardwareConf->devices.at(i).name == "headstageSensor")||(conf.hardwareConf->devices.at(i).name.contains("multiplexed",Qt::CaseInsensitive))) {
            sensorSectionStartByte = conf.hardwareConf->devices.at(i).byteOffset;
            sensorSectionSize = conf.hardwareConf->devices.at(i).numBytes;
            break;
        }
    }
    if ((extraSteps & ExtraProcessorType::IntanStim) && (sensorSectionStartByte < 0)) {
        //if we don't find the sensor section, there is something wrong. We turn off
        //IntanStim mode
        qDebug() << "[StreamProcessHandler] Error: no headstage sensor section found in workspace. Stimulation processing will be turned off.";
        extraSteps &= !ExtraProcessorType::IntanStim;
    }








    timestamp_latest = 0;
    timestamp_previous = 0;
    numLoopsWithNoHeadstageData = 0;

    for (int i=0; i<nTrodeList.length(); i++) {

        int nt = nTrodeList.at(i);
        moduleDataChan.push_back(conf_ptrs.spikeConf->ntrodes[nt]->lfpDataChan);
        refChan.push_back(conf_ptrs.spikeConf->ntrodes.refOfIndex(nt)->hw_chan[conf_ptrs.spikeConf->ntrodes[nt]->refChan]);

        notchFiltersOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->notchFilterOn);
        spikeFiltersOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->filterOn);
        lfpFiltersOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->lfpFilterOn);
        lfpRefOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->lfpRefOn);
        refOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->refOn);
        rawRefOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->rawRefOn);
        groupRefOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->groupRefOn);
        refGroup.push_back(conf_ptrs.spikeConf->ntrodes[nt]->refGroup);
        lfpModeOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->lfpViewMode);
        spikeModeOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->spikeViewMode);
        stimViewModeOn.push_back(conf_ptrs.spikeConf->ntrodes[nt]->stimViewMode);
    }

//    stream_inc = 0;
//    dataIdx = 0;

    /*for (int auxCh = 0; auxCh < auxChannelList.length(); auxCh++) {
        //Reset any interleaved aux channels to 0
        int hdrChan = auxChannelList.at(auxCh);
        if ((headerConf->headerChannels[hdrChan].dataType == DeviceChannel::INT16TYPE) &&
                    (headerConf->headerChannels[hdrChan].interleavedDataIDByte != -1)) {
            interleavedAuxChannelStates[headerConf->headerChannels[hdrChan].idString] = 0;
        } else if ((headerConf->headerChannels[hdrChan].dataType == DeviceChannel::UINT32TYPE) &&
                   (headerConf->headerChannels[hdrChan].interleavedDataIDByte != -1)) {
           interleavedAuxChannelStates[headerConf->headerChannels[hdrChan].idString] = 0;
       }
    }*/

    //Interpolation during export not supported with neuropixels
    if (conf_ptrs.spikeConf->deviceType == "neuropixels1") {
        interpMode = false;
    }

}

AbstractNeuralDataHandler::~AbstractNeuralDataHandler()
{
}

void AbstractNeuralDataHandler::updateChannels(void)
{  
    for (int trode = 0; trode < nTrodeList.length(); trode++) {
        const int nt = nTrodeList[trode];
        notchFiltersOn[trode] = conf_ptrs.spikeConf->ntrodes[nt]->notchFilterOn;
        spikeFiltersOn[trode] = conf_ptrs.spikeConf->ntrodes[nt]->filterOn;
        lfpFiltersOn[trode] = conf_ptrs.spikeConf->ntrodes[nt]->lfpFilterOn;
        moduleDataChan[trode] = conf_ptrs.spikeConf->ntrodes[nt]->lfpDataChan;
        spikeModeOn[trode] = conf_ptrs.spikeConf->ntrodes[nt]->spikeViewMode;
        stimViewModeOn[trode] = conf_ptrs.spikeConf->ntrodes[nt]->stimViewMode;
        lfpModeOn[trode] = conf_ptrs.spikeConf->ntrodes[nt]->lfpViewMode;
    }
}

void AbstractNeuralDataHandler::updateGlobalStimSettings(GlobalStimulationSettings globalStim)
{
    globalStimSettings = globalStim;

    switch(globalStim.currentScaling()) {
    case GlobalStimulationSettings::max10nA :
        stimCurrentScale_nA = 10.0/255.0;
        break;
    case GlobalStimulationSettings::max20nA   :
        stimCurrentScale_nA = 20.0/255.0;
        break;
    case GlobalStimulationSettings::max50nA  :
        stimCurrentScale_nA = 50.0/255.0;
        break;
    case GlobalStimulationSettings::max100nA  :
        stimCurrentScale_nA = 100.0/255.0;
        break;
    case GlobalStimulationSettings::max200nA   :
        stimCurrentScale_nA = 200.0/255.0;
        break;
    case GlobalStimulationSettings::max500nA   :
        stimCurrentScale_nA = 500.0/255.0;
        break;
    case GlobalStimulationSettings::max1uA   :
        stimCurrentScale_nA = 1000.0/255.0;
        break;
    case GlobalStimulationSettings::max2uA   :
        stimCurrentScale_nA = 2000.0/255.0;
        break;
    case GlobalStimulationSettings::max5uA   :
        stimCurrentScale_nA = 5000.0/255.0;
        break;
    case GlobalStimulationSettings::max10uA   :
        stimCurrentScale_nA = 10000.0/255.0;
        break;

    }

}


void AbstractNeuralDataHandler::updateChannelsRef()
{

    for (int trode = 0; trode < nTrodeList.length(); trode++) {
        const int nt = nTrodeList[trode];
        lfpRefOn[trode] = conf_ptrs.spikeConf->ntrodes[nt]->lfpRefOn;
        refOn[trode] = conf_ptrs.spikeConf->ntrodes[nt]->refOn;
        rawRefOn[trode] = conf_ptrs.spikeConf->ntrodes[nt]->rawRefOn;
        refChan[trode] = conf_ptrs.spikeConf->ntrodes.refOfIndex(nt)->hw_chan[conf_ptrs.spikeConf->ntrodes[nt]->refChan];
        groupRefOn[trode] = conf_ptrs.spikeConf->ntrodes[nt]->groupRefOn;
        refGroup[trode] = conf_ptrs.spikeConf->ntrodes[nt]->refGroup;
     }
}

void AbstractNeuralDataHandler::updateFilters()
{
    for(int i = 0; i < nTrodeList.length(); ++i){
        const int nt = nTrodeList[i];
        moduleDataChan[i] = conf_ptrs.spikeConf->ntrodes[nt]->lfpDataChan;
    }
}

void AbstractNeuralDataHandler::setDataLength(double dataLength, int traceLength)
{
//    this->dataLength = dataLength;
//    this->traceLength = traceLength;
//    //Calculate how many data points go into one display pixel on the stream plot
//    raw_increment.resize(traceLength);
//    for (int j = 0; j < traceLength; j++) {
//        raw_increment[j] = (int)((double)(j + 1) * (double)dataLength / (double)traceLength) -
//                           (int)((double)j * (double)dataLength / (double)traceLength);
//    }
}


void AbstractNeuralDataHandler::setSpikeInvert(bool invert)
{
    spikeInvert = invert;
}

void AbstractNeuralDataHandler::setInterpMode(bool on, uint32_t maxGapSize)
{

    //if (conf_ptrs.spikeConf->deviceType == "neuropixels1") return; //No interpolation allowed with neuropixels probes

    interpMode = on;
    maxInterpGap = maxGapSize;
    numSamplesLeftToInterp = 0;


}


int AbstractNeuralDataHandler::getSamplesLeftToInterpolate()
{
    return numSamplesLeftToInterp;
}

bool AbstractNeuralDataHandler::gapOccured()
{
    return gapInDataOccured;
}

uint32_t AbstractNeuralDataHandler::getSamplesProcessed()
{
    return samplesProcessed;
}

void AbstractNeuralDataHandler::getAvailableWaveforms(QVector<SpikeWaveform> &waveformCopies)
{
    //Copy the availble spikes, then clear the vector.
    waveformCopies = availableSpikes;
    availableSpikes.clear();
}

void AbstractNeuralDataHandler::clearAvailableWaveforms()
{
    for (int i=0; i < availableSpikes.length(); i++) {
        availableSpikes.at(i).clearMemory();
    }
    availableSpikes.clear();
}

float AbstractNeuralDataHandler::getStimScale_nA() {
    return stimCurrentScale_nA;
}



//-------------------------------------------------------------------------------------------------
NeuralDataHandler::NeuralDataHandler(TrodesConfigurationPointers conf, QList<int> nTrodeList_in, uint32_t extraSteps):
    AbstractNeuralDataHandler(conf,nTrodeList_in, extraSteps)
{

    spike_band_dataPoints = new int16_t*[nTrodeList.length()]; //vector of pointers (multiple values per nTrode)
    raw_band_dataPoints = new int16_t*[nTrodeList.length()]; //vector of pointers (multiple values per nTrode)
    raw_unreferenced_band_dataPoints = new int16_t*[nTrodeList.length()]; //vector of pointers (multiple values per nTrode)
    lfp_band_dataPoints = new int16_t[nTrodeList.length()]; //vector of int16 values (one value per nTrode)
    stim_band_dataPoints = new int16_t*[nTrodeList.length()]; //vector of pointers (multiple values per nTrode)

    spike_band_dataPoints2 = new int16_t*[nTrodeList.length()]; //vector of pointers (multiple values per nTrode)
    raw_band_dataPoints2 = new int16_t*[nTrodeList.length()]; //vector of pointers (multiple values per nTrode)
    raw_unreferenced_band_dataPoints2 = new int16_t*[nTrodeList.length()]; //vector of pointers (multiple values per nTrode)
    lfp_band_dataPoints2 = new int16_t[nTrodeList.length()]; //vector of int16 values (one value per nTrode)

    spike_band_dataPoints_interpolated = new int16_t*[nTrodeList.length()]; //vector of pointers (multiple values per nTrode)
    raw_band_dataPoints_interpolated = new int16_t*[nTrodeList.length()]; //vector of pointers (multiple values per nTrode)
    raw_unreferenced_band_dataPoints_interpolated = new int16_t*[nTrodeList.length()]; //vector of pointers (multiple values per nTrode)
    lfp_band_dataPoints_interpolated = new int16_t[nTrodeList.length()]; //vector of int16 values (one value per nTrode)

    minValues = new float*[nTrodeList.length()];
    maxValues = new float*[nTrodeList.length()];

    storageGate = true;

    for (int i=0; i<nTrodeList.length(); i++) {

        int nt = nTrodeList.at(i);

        lfpFilters.push_back(new BesselFilter());
        lfpFilters.last()->setSamplingRate(conf_ptrs.hardwareConf->sourceSamplingRate);
        lfpFilters.last()->setFilterRange(0,conf_ptrs.spikeConf->ntrodes[nt]->lfpHighFilter);

        QVector<BesselFilter*> spkfilts;
//        QVector<NotchFilter*> notchfilts;
        int hwchan_len = conf_ptrs.spikeConf->ntrodes[nt]->hw_chan.length();
        for(int c = 0; c < hwchan_len; c++){
            spkfilts.push_back(new BesselFilter());
            spkfilts.last()->setSamplingRate(conf_ptrs.hardwareConf->sourceSamplingRate);
            spkfilts.last()->setFilterRange(conf_ptrs.spikeConf->ntrodes[nt]->lowFilter,conf_ptrs.spikeConf->ntrodes[nt]->highFilter);
//            notchfilts.push_back(new NotchFilter());
//            notchfilts.last()->setSamplingRate(conf_ptrs.hardwareConf->sourceSamplingRate);

        }
        spikeFilters.push_back(spkfilts);
//        notchFilters.push_back(notchfilts);

        spike_band_dataPoints[i] = new int16_t[hwchan_len];
        raw_band_dataPoints[i] = new int16_t[hwchan_len];
        raw_unreferenced_band_dataPoints[i] = new int16_t[hwchan_len];
        stim_band_dataPoints[i] = new int16_t[hwchan_len];

        spike_band_dataPoints2[i] = new int16_t[hwchan_len];
        raw_band_dataPoints2[i] = new int16_t[hwchan_len];
        raw_unreferenced_band_dataPoints2[i] = new int16_t[hwchan_len];

        spike_band_dataPoints_interpolated[i] = new int16_t[hwchan_len];
        raw_band_dataPoints_interpolated[i] = new int16_t[hwchan_len];
        raw_unreferenced_band_dataPoints_interpolated[i] = new int16_t[hwchan_len];

        minValues[i] = new float[hwchan_len];
        maxValues[i] = new float[hwchan_len];
        std::fill(minValues[i], minValues[i]+hwchan_len, std::numeric_limits<float>::max());
        std::fill(maxValues[i], maxValues[i]+hwchan_len, std::numeric_limits<float>::min());
    }

}

NeuralDataHandler::~NeuralDataHandler()
{
    for (int i = 0; i < nTrodeList.length(); i++) {
        delete [] spike_band_dataPoints[i];
        delete [] stim_band_dataPoints[i];
        delete [] raw_band_dataPoints[i];
        delete [] raw_unreferenced_band_dataPoints[i];
        delete [] spike_band_dataPoints2[i];
        delete [] raw_band_dataPoints2[i];
        delete [] raw_unreferenced_band_dataPoints2[i];
        delete [] spike_band_dataPoints_interpolated[i];
        delete [] raw_band_dataPoints_interpolated[i];
        delete [] raw_unreferenced_band_dataPoints_interpolated[i];
        delete [] minValues[i];
        delete [] maxValues[i];
    }
    delete [] spike_band_dataPoints;
    delete [] stim_band_dataPoints;
    delete [] raw_band_dataPoints;
    delete [] raw_unreferenced_band_dataPoints;
    delete [] lfp_band_dataPoints;

    delete [] spike_band_dataPoints2;
    delete [] raw_band_dataPoints2;
    delete [] raw_unreferenced_band_dataPoints2;
    delete [] lfp_band_dataPoints2;

    delete [] spike_band_dataPoints_interpolated;
    delete [] raw_band_dataPoints_interpolated;
    delete [] raw_unreferenced_band_dataPoints_interpolated;
    delete [] lfp_band_dataPoints_interpolated;

    delete [] minValues;
    delete [] maxValues;
}

void NeuralDataHandler::processNextSamples(uint32_t timestamp, const int16_t *neuralDataBlock, const int16_t *CARDataBlock, const char *auxDataBlock)
{
    //This is where the raw data is referenced, filtered, and split into different 'bands'. The inputs to this function are:
    //1) the packet's timestamp
    //2) a pointer to the start of the neural data block (for all of the raw channel data, even if this class is not using all of it)
    //3) a pointer to the start of the common average referencing (CAR) data

    //This function (and all versions of it) should do the following things:
    //1) If any channels were NANs, set latestDataHasNan to true and increment numLoopsWithNoHeadstageData. Otherwise, set numLoopsWithNoHeadstageData to 0
    //2) Calculate the following four data bands (applying the user-set referencing and filtering settings for each band):
    //      i) raw unreferenced (all channels in each ntrode) -> raw_unreferenced_band_dataPoints_latest[nTrodeList_index][channel_index]
    //      ii) raw referenced (all channels in each ntrode) -> raw_band_dataPoints_latest[nTrodeList_index][channel_index]
    //      iii) lfp (one designated channel per ntrode) -> lfp_band_dataPoints_latest[nTrodeList_index]
    //      iv) spikes (all channels in each ntrode) -> spike_band_dataPoints_latest[nTrodeList_index][channel_index]

    //This section is required for all versions of processNextSamples
    //***********************
    bool interpGapDetected = false;
    samplesProcessed++;
    timestamp_previous = timestamp_latest;
    timestamp_latest = timestamp;
    interpValuesReadyToFetch = false;
    if (samplesProcessed > 1) {
        if (interpMode && (timestamp_latest-timestamp_previous > 1) && (timestamp_latest-timestamp_previous <= maxInterpGap)) {
            interpGapDetected = true;
            numSamplesLeftToInterp = timestamp_latest-timestamp_previous;
        } else if (interpMode && (timestamp_latest-timestamp_previous > maxInterpGap)) {
            resetFilters();
            gapInDataOccured = true;
        } else if (timestamp_latest-timestamp_previous > 1) {
            gapInDataOccured = true;
        } else {
            gapInDataOccured = false;
        }
    }
    switchDataPointers();
    //*************************

    int16_t tmpDataPoint = 0;
    int16_t tmpDataPointspk = 0;
    int16_t tmpDataPointlfp = 0;
    int16_t tmpReferencedValue = 0;
    int16_t tmpUnReferencedValue = 0;
    int16_t valuefromlfp = 0;
    int16_t valueforSpikeDetector = 0;

    int hw_chan = 0;
    latestDataHasNan = false;
    bool stimPulseAnywhere = false;

    //stimCurrentScale_nA = globalStimSettings.currentScaling() << 8;


    //First we process the neural channels
    for (int n = 0; n < nTrodeList.length(); n++) {
        // the nTrodeList may not be identical to all the nTrodes in the case were there are multple streamProcessorThreads
        int nt = nTrodeList.at(n);
        int modDataChan = moduleDataChan[n];
        int refChanInd = refChan[n];


        for (int c = 0; c < conf_ptrs.spikeConf->ntrodes[nt]->hw_chan.length(); c++) {
            hw_chan = conf_ptrs.spikeConf->ntrodes[nt]->hw_chan[c];
            if (conf_ptrs.spikeConf->ntrodes[nt]->stimCapable[c] && (neuralDataBlock[hw_chan] & 0x0001)) {
                //This channel is capable of stimulating and is currently in stim mode. We set all the neural bands to 0. We may want to switch to
                //using -32768 for the raw unreferenced to indicate a NaN.
                raw_unreferenced_band_dataPoints_latest[n][c] = 0;
                spike_band_dataPoints_latest[n][c] = 0;
                raw_band_dataPoints_latest[n][c] = 0;
                if (modDataChan==c){
                   valuefromlfp = 0;
                }
                if (neuralDataBlock[hw_chan] & 0x0002) {
                    //Stimultion current is on
                    stimPulseAnywhere = true;
                    if (neuralDataBlock[hw_chan] & 0x0004) {
                        //Anode
                        //stim_band_dataPoints[n][c] = ((neuralDataBlock[hw_chan] & 0xFF00)>>8)|stimCurrentScale_nA;
                        stim_band_dataPoints[n][c] = ((neuralDataBlock[hw_chan] & 0xFF00)>>4);
                    } else {
                        //Cathode
                        //stim_band_dataPoints[n][c] = -(((neuralDataBlock[hw_chan] & 0xFF00)>>8)|stimCurrentScale_nA);
                        stim_band_dataPoints[n][c] = -((neuralDataBlock[hw_chan] & 0xFF00)>>4);
                    }
                } else {
                    //Stimulation current off
                    stim_band_dataPoints[n][c] = 0;
                }
                spike_band_dataPoints_latest[n][c] = stim_band_dataPoints[n][c];
                continue;

            } else {
                stim_band_dataPoints[n][c] = 0;
            }

            //Set all tmpdatapoints to the rawdata
            tmpUnReferencedValue = neuralDataBlock[hw_chan];
            if (tmpUnReferencedValue == -32768) {
                //This is a NAN, replace with 0
                tmpUnReferencedValue = 0;
                latestDataHasNan = true;
            }
            raw_unreferenced_band_dataPoints_latest[n][c] = tmpUnReferencedValue;

            bool anyRefApplied = false;
            //Perform perform referencing calculation
            if(groupRefOn[n] && (rawRefOn[n] || refOn[n] || lfpRefOn[n])){
                //This is for combined average referencing (CAR)
                const int carvalind = refGroup[n]-1;
                const int16_t tempval = CARDataBlock[carvalind];
                tmpReferencedValue = tmpUnReferencedValue - tempval;
                anyRefApplied = true;
            }
            else if (rawRefOn[n] || refOn[n] || lfpRefOn[n]) {
                //if no CAR but any references are on, calculate standard reference using a single channel
                tmpReferencedValue = tmpUnReferencedValue-neuralDataBlock[refChanInd];
                anyRefApplied = true;
            }


//            //After referencing, we apply the notch filter to both referenced and unreferenced data
//            if(notchFiltersOn[n] && !interpGapDetected){
//                tmpUnReferencedValue = notchFilters[n][c]->addValue(tmpUnReferencedValue);
//                if (anyRefApplied) {
//                    tmpReferencedValue = notchFilters[n][c]->addValue(tmpReferencedValue);
//                }
//            }

            // Next, we split the data stream into three channels: Raw, Spike, and LFP.

            //Raw referencing
            if(rawRefOn[n]){
                tmpDataPoint = tmpReferencedValue;
            } else {
                tmpDataPoint = tmpUnReferencedValue;
            }

            //Spike referencing
            if (refOn[n]) {
                tmpDataPointspk = tmpReferencedValue;
            } else {
                tmpDataPointspk = tmpUnReferencedValue;
            }


            //LFP referencing
            if(lfpRefOn[n]){
                tmpDataPointlfp = tmpReferencedValue;
            } else {
                tmpDataPointlfp = tmpUnReferencedValue;
            }

            // Run the signal through lfp and spike filters

            //If lfp filters on and lfp channel set to this one, add to the filter
            //Todo: lfp decimation for sending out
            if(lfpFiltersOn[n] && modDataChan==c && !interpGapDetected){
                valuefromlfp = lfpFilters[n]->addValue(tmpDataPointlfp);
            } else if (modDataChan==c){
                valuefromlfp = tmpDataPointlfp;
            }


            //If spike filters on, pass on to spike detector.
            if(spikeFiltersOn[n] && !interpGapDetected){
                tmpDataPointspk = spikeFilters[n][c]->addValue(tmpDataPointspk);
            }
             if (spikeInvert) {
                valueforSpikeDetector = tmpDataPointspk * -1; //Reverse polarity to make spikes go upward
             } else {
                valueforSpikeDetector = tmpDataPointspk;
             }
            // Send data to spike detector
            spike_band_dataPoints_latest[n][c] = valueforSpikeDetector;
            raw_band_dataPoints_latest[n][c] = tmpDataPoint;

        }
        lfp_band_dataPoints_latest[n] = valuefromlfp;


    }
    stimHappeningNow = stimPulseAnywhere; //true if there is active stim on any channels
    if (latestDataHasNan) {
        numLoopsWithNoHeadstageData++;
    } else {
        numLoopsWithNoHeadstageData = 0;
    }

}

const int16_t* NeuralDataHandler::getEntirePacket(DataBand d, int &numSamples)
{
    numSamples = 0;
    return nullptr;
}

const int16_t* NeuralDataHandler::getNTrodeSamples(int nTrodeIndex,DataBand d)
{

    if (interpValuesReadyToFetch) {
        if (d == RAW_UNREFERENCED) {
            return raw_unreferenced_band_dataPoints_interpolated[nTrodeIndex];
        } else if (d == RAW_REFERENCED) {
            return raw_band_dataPoints_interpolated[nTrodeIndex];
        } else if (d == LFP) {
            return lfp_band_dataPoints_interpolated+nTrodeIndex;
        } else if (d == SPIKE) {
            return spike_band_dataPoints_interpolated[nTrodeIndex];
        }

    } else {

        if (d == RAW_UNREFERENCED) {
            return raw_unreferenced_band_dataPoints_latest[nTrodeIndex];
        } else if (d == RAW_REFERENCED) {
            return raw_band_dataPoints_latest[nTrodeIndex];
        } else if (d == LFP) {
            return lfp_band_dataPoints_latest+nTrodeIndex;
        } else if (d == SPIKE) {
            return spike_band_dataPoints_latest[nTrodeIndex];
        } else if (d == STIMULATION) {
            return stim_band_dataPoints[nTrodeIndex];
        }
    }

    return nullptr;

}

void NeuralDataHandler::processDisplaySamples(bool newbin)
{
    for (int n = 0; n < nTrodeList.length(); n++) {
        int nt = nTrodeList.at(n);
        for (int c = 0; c < conf_ptrs.spikeConf->ntrodes[nt]->hw_chan.length(); c++) {
            int hw_chan = conf_ptrs.spikeConf->ntrodes[nt]->hw_chan[c];

            //if/elseif/else statement to pick spike/lfp/raw data
            int16_t tmpDataPoint;
            if(lfpModeOn[n]){
                // Assign display to lfp
                tmpDataPoint = getNTrodeSamples(n,AbstractNeuralDataHandler::LFP)[moduleDataChan[n]];
            } else if(spikeModeOn[n]){
                // Assign stream display whatever the spike display gets
                tmpDataPoint = getNTrodeSamples(n,AbstractNeuralDataHandler::SPIKE)[c];

            } else if(stimViewModeOn[n]){
                // Assign stream display whatever the spike display gets
                tmpDataPoint = getNTrodeSamples(n,AbstractNeuralDataHandler::STIMULATION)[c];

            } else {
                tmpDataPoint = getNTrodeSamples(n,AbstractNeuralDataHandler::RAW_REFERENCED)[c];
            }

            //update minValues[n] bin and maxValues[n] bin (or restart bin if stream_inc is 0)
            if (!newbin) {
               //Old bin, so compare to existing value
                maxValues[n][c] = qMax((float)tmpDataPoint, maxValues[n][c]);
                minValues[n][c] = qMin((float)tmpDataPoint, minValues[n][c]);
            }
            else {
                //New bin, so reset the value
                maxValues[n][c] = (float)tmpDataPoint;
                minValues[n][c] = (float)tmpDataPoint;
            }
        }
    }
}

const float* NeuralDataHandler::getMinDisplaySamples(int nTrodeIndex)
{
    return minValues[nTrodeIndex];
}

const float* NeuralDataHandler::getMaxDisplaySamples(int nTrodeIndex)
{
    return maxValues[nTrodeIndex];
}


void NeuralDataHandler::updateFilters()
{
    for(int i = 0; i < nTrodeList.length(); ++i){
        const int nt = nTrodeList[i];
        for(int c = 0; c < conf_ptrs.spikeConf->ntrodes[nt]->hw_chan.length(); ++c){
            spikeFilters[i][c]->setFilterRange(conf_ptrs.spikeConf->ntrodes[nt]->lowFilter, conf_ptrs.spikeConf->ntrodes[nt]->highFilter);
//            notchFilters[i][c]->setBandwidth(conf_ptrs.spikeConf->ntrodes[nt]->notchBW);
//            notchFilters[i][c]->setNotchFreq(conf_ptrs.spikeConf->ntrodes[nt]->notchFreq);
        }
        lfpFilters[i]->setFilterRange(0, conf_ptrs.spikeConf->ntrodes[nt]->lfpHighFilter);
        moduleDataChan[i] = conf_ptrs.spikeConf->ntrodes[nt]->lfpDataChan;
    }
    AbstractNeuralDataHandler::updateFilters();
}

void NeuralDataHandler::resetFilters()
{
    for(int i = 0; i < nTrodeList.length(); ++i){
        const int nt = nTrodeList[i];
        for(int c = 0; c < conf_ptrs.spikeConf->ntrodes[nt]->hw_chan.length(); ++c){
            spikeFilters[i][c]->resetHistory();
            //notchFilters[i][c]->resetHistory();
        }
        lfpFilters[i]->resetHistory();

    }
}

void NeuralDataHandler::stepPreviousSample()
{

    if (numSamplesLeftToInterp > 0) {
        //Do linear interpolation for each of the neural channels
        int16_t interpolatedDataPtInt_REF;
        double interpolatedDataPtFloat_REF;
        int16_t interpolatedDataPtInt_NOREF;
        double interpolatedDataPtFloat_NOREF;
        int16_t nextUnreferencedDataVal;
        int16_t lastUnreferencedDataVal;
        int16_t nextReferencedDataVal;
        int16_t lastReferencedDataVal;

        int16_t tmpDataPoint = 0;
        int16_t tmpDataPointspk = 0;
        int16_t tmpDataPointlfp = 0;
        int16_t valuefromlfp = 0;
        int16_t valueforSpikeDetector = 0;

        uint32_t totalGapSize = timestamp_latest - timestamp_previous;

        for (int n = 0; n < nTrodeList.length(); n++) {
            // the nTrodeList may not be identical to all the nTrodes in the case were there are multple streamProcessorThreads
            int nt = nTrodeList.at(n);
            int modDataChan = moduleDataChan[n];
            int refChanInd = refChan[n];

            for (int c = 0; c < conf_ptrs.spikeConf->ntrodes[nt]->hw_chan.length(); c++) {
                //hw_chan = conf_ptrs.spikeConf->ntrodes[nt]->hw_chan[c];
                //Get the next value for the channel after the gap

                nextUnreferencedDataVal = raw_unreferenced_band_dataPoints_latest[n][c];
                lastUnreferencedDataVal = raw_unreferenced_band_dataPoints_previous[n][c];

                nextReferencedDataVal = raw_band_dataPoints_latest[n][c];
                lastReferencedDataVal = raw_band_dataPoints_previous[n][c];



                //Perform a simple linear interpolation between the last read point and the next one
                interpolatedDataPtFloat_REF = (double)lastReferencedDataVal + ((double)(nextReferencedDataVal-lastReferencedDataVal) / totalGapSize) * (totalGapSize - numSamplesLeftToInterp + 1);
                //interpolatedDataPtFloat_REF = (double)lastReferencedDataVal + ((double)(nextReferencedDataVal-lastReferencedDataVal) / numSamplesLeftToInterp);
                interpolatedDataPtInt_REF = round(interpolatedDataPtFloat_REF);

                interpolatedDataPtFloat_NOREF = (double)lastUnreferencedDataVal + ((double)(nextUnreferencedDataVal-lastUnreferencedDataVal) / totalGapSize) * (totalGapSize - numSamplesLeftToInterp + 1);

                //interpolatedDataPtFloat_NOREF = (double)lastUnreferencedDataVal + ((double)(nextUnreferencedDataVal-lastUnreferencedDataVal) / numSamplesLeftToInterp);
                interpolatedDataPtInt_NOREF = round(interpolatedDataPtFloat_NOREF);

                raw_unreferenced_band_dataPoints_interpolated[n][c] = interpolatedDataPtInt_NOREF;

                // Next, we split the data stream into three channels: Raw, Spike, and LFP.

                //Raw referencing
                if(rawRefOn[n]){
                    tmpDataPoint = interpolatedDataPtInt_REF;
                } else {
                    tmpDataPoint = interpolatedDataPtInt_NOREF;
                }
                //Spike referencing
                if (refOn[n]) {
                    tmpDataPointspk = interpolatedDataPtInt_REF;
                } else {
                    tmpDataPointspk = interpolatedDataPtInt_NOREF;
                }
                //LFP referencing
                if(lfpRefOn[n]){
                    tmpDataPointlfp = interpolatedDataPtInt_REF;
                } else {
                    tmpDataPointlfp = interpolatedDataPtInt_NOREF;
                }

                // Run the signal through lfp and spike filters

                //If lfp filters on and lfp channel set to this one, add to the filter
                //Todo: lfp decimation for sending out
                if(lfpFiltersOn[n] && modDataChan==c){
                    valuefromlfp = lfpFilters[n]->addValue(tmpDataPointlfp);
                } else if (modDataChan==c){
                    valuefromlfp = tmpDataPointlfp;
                }

                //If spike filters on, pass on to spike detector.
                if(spikeFiltersOn[n]){
                    tmpDataPointspk = spikeFilters[n][c]->addValue(tmpDataPointspk);
                }
                if (spikeInvert) {
                    valueforSpikeDetector = tmpDataPointspk * -1; //Reverse polarity to make spikes go upward
                } else {
                    valueforSpikeDetector = tmpDataPointspk;
                }

                // Send data to spike detector
                spike_band_dataPoints_interpolated[n][c] = valueforSpikeDetector;
                raw_band_dataPoints_interpolated[n][c] = tmpDataPoint;

            }
            lfp_band_dataPoints_interpolated[n] = valuefromlfp;
        }
        interpValuesReadyToFetch = true;
        numSamplesLeftToInterp--;
    }


}

void NeuralDataHandler::switchDataPointers()
{
    //efficient method to keep track of the previous data point for interpolation without copying data

    if (storageGate) {
        spike_band_dataPoints_latest = spike_band_dataPoints;
        raw_band_dataPoints_latest = raw_band_dataPoints;
        raw_unreferenced_band_dataPoints_latest = raw_unreferenced_band_dataPoints;
        lfp_band_dataPoints_latest = lfp_band_dataPoints;
        spike_band_dataPoints_previous = spike_band_dataPoints2;
        raw_band_dataPoints_previous = raw_band_dataPoints2;
        raw_unreferenced_band_dataPoints_previous = raw_unreferenced_band_dataPoints2;
        lfp_band_dataPoints_previous = lfp_band_dataPoints2;
    } else {
        spike_band_dataPoints_latest = spike_band_dataPoints2;
        raw_band_dataPoints_latest = raw_band_dataPoints2;
        raw_unreferenced_band_dataPoints_latest = raw_unreferenced_band_dataPoints2;
        lfp_band_dataPoints_latest = lfp_band_dataPoints2;
        spike_band_dataPoints_previous = spike_band_dataPoints;
        raw_band_dataPoints_previous = raw_band_dataPoints;
        raw_unreferenced_band_dataPoints_previous = raw_unreferenced_band_dataPoints;
        lfp_band_dataPoints_previous = lfp_band_dataPoints;
    }

    storageGate = !storageGate; //flips which container to store data in for the next cycle
}

//---------------------------------------
//-------------------------------------------------------------------------------------------------
/*NeuroPixelsDataHandler::NeuroPixelsDataHandler(TrodesConfigurationPointers conf, QList<int> nTrodeList_in):
    AbstractNeuralDataHandler(conf,nTrodeList_in)
{

}

void NeuroPixelsDataHandler::processNextSamples(uint32_t timestamp, const int16_t *neuralDataBlock, const int16_t *CARDataBlock, const char *auxDataBlock)
{
    //This is where the raw data is referenced, filtered, and split into different 'bands'. The inputs to this function are:
    //1) the packet's timestamp
    //2) a pointer to the start of the neural data block (for all of the raw channel data, even if this class is not using all of it)
    //3) a pointer to the start of the common average referencing (CAR) data

    //This function (and all versions of it) should do the following things:
    //1) If any channels were NANs, set latestDataHasNan to true and increment numLoopsWithNoHeadstageData. Otherwise, set numLoopsWithNoHeadstageData to 0
    //2) Calculate the following four data bands (applying the user-set referencing and filtering settings for each band):
    //      i) raw unreferenced (all channels in each ntrode) -> raw_unreferenced_band_dataPoints_latest[nTrodeList_index][channel_index]
    //      ii) raw referenced (all channels in each ntrode) -> raw_band_dataPoints_latest[nTrodeList_index][channel_index]
    //      iii) lfp (one designated channel per ntrode) -> lfp_band_dataPoints_latest[nTrodeList_index]
    //      iv) spikes (all channels in each ntrode) -> spike_band_dataPoints_latest[nTrodeList_index][channel_index]

    //This section is required for all versions of processNextSamples
    //***********************
    bool interpGapDetected = false;
    samplesProcessed++;
    timestamp_previous = timestamp_latest;
    timestamp_latest = timestamp;
    interpValuesReadyToFetch = false;
    if (samplesProcessed > 1) {
        if (interpMode && (timestamp_latest-timestamp_previous > 1) && (timestamp_latest-timestamp_previous < maxInterpGap)) {
            interpGapDetected = true;
            numSamplesLeftToInterp = timestamp_latest-timestamp_previous;
        } else if (interpMode && (timestamp_latest-timestamp_previous > maxInterpGap)) {
            resetFilters();
            gapInDataOccured = true;
        } else if (timestamp_latest-timestamp_previous > 1) {
            gapInDataOccured = true;
        } else {
            gapInDataOccured = false;
        }
    }
    switchDataPointers();
    //*************************

    int16_t tmpDataPoint = 0;
    int16_t tmpDataPointspk = 0;
    int16_t tmpDataPointlfp = 0;
    int16_t tmpReferencedValue = 0;
    int16_t tmpUnReferencedValue = 0;
    int16_t valuefromlfp = 0;
    int16_t valueforSpikeDetector = 0;

    int hw_chan = 0;
    latestDataHasNan = false;
    //process the neural channels
    for (int n = 0; n < nTrodeList.length(); n++) {
        // the nTrodeList may not be identical to all the nTrodes in the case were there are multple streamProcessorThreads
        int nt = nTrodeList.at(n);
        int modDataChan = moduleDataChan[n];
        int refChanInd = refChan[n];

        for (int c = 0; c < conf_ptrs.spikeConf->ntrodes[nt]->hw_chan.length(); c++) {
            hw_chan = conf_ptrs.spikeConf->ntrodes[nt]->hw_chan[c];

            //Set all tmpdatapoints to the rawdata
            tmpUnReferencedValue = neuralDataBlock[hw_chan];
            if (tmpUnReferencedValue == -32768) {
                //This is a NAN, replace with 0
                tmpUnReferencedValue = 0;
                latestDataHasNan = true;
            }
            raw_unreferenced_band_dataPoints_latest[n][c] = tmpUnReferencedValue;

            bool anyRefApplied = false;
            //Perform perform referencing calculation
            if(groupRefOn[n] && (rawRefOn[n] || refOn[n] || lfpRefOn[n])){
                //This is for combined average referencing (CAR)
                const int carvalind = refGroup[n]-1;
                const int16_t tempval = CARDataBlock[carvalind];
                tmpReferencedValue = tmpUnReferencedValue - tempval;
                anyRefApplied = true;
            }
            else if (rawRefOn[n] || refOn[n] || lfpRefOn[n]) {
                //if no CAR but any references are on, calculate standard reference using a single channel
                tmpReferencedValue = tmpUnReferencedValue-neuralDataBlock[refChanInd];
                anyRefApplied = true;
            }


//            //After referencing, we apply the notch filter to both referenced and unreferenced data
//            if(notchFiltersOn[n] && !interpGapDetected){
//                tmpUnReferencedValue = notchFilters[n][c]->addValue(tmpUnReferencedValue);
//                if (anyRefApplied) {
//                    tmpReferencedValue = notchFilters[n][c]->addValue(tmpReferencedValue);
//                }
//            }

            // Next, we split the data stream into three channels: Raw, Spike, and LFP.

            //Raw referencing
            if(rawRefOn[n]){
                tmpDataPoint = tmpReferencedValue;
            } else {
                tmpDataPoint = tmpUnReferencedValue;
            }

            //Spike referencing
            if (refOn[n]) {
                tmpDataPointspk = tmpReferencedValue;
            } else {
                tmpDataPointspk = tmpUnReferencedValue;
            }


            //LFP referencing
            if(lfpRefOn[n]){
                tmpDataPointlfp = tmpReferencedValue;
            } else {
                tmpDataPointlfp = tmpUnReferencedValue;
            }

            // Run the signal through lfp and spike filters

            //If lfp filters on and lfp channel set to this one, add to the filter
            //Todo: lfp decimation for sending out
            if(lfpFiltersOn[n] && modDataChan==c && !interpGapDetected){
                valuefromlfp = lfpFilters[n]->addValue(tmpDataPointlfp);
            } else if (modDataChan==c){
                valuefromlfp = tmpDataPointlfp;
            }


            //If spike filters on, pass on to spike detector.
            if(spikeFiltersOn[n] && !interpGapDetected){
                tmpDataPointspk = spikeFilters[n][c]->addValue(tmpDataPointspk);
            }
            if (spikeInvert) {
                valueforSpikeDetector = tmpDataPointspk * -1; //Reverse polarity to make spikes go upward
            } else {
                valueforSpikeDetector = tmpDataPointspk;
            }
            // Send data to spike detector
            spike_band_dataPoints_latest[n][c] = valueforSpikeDetector;
            raw_band_dataPoints_latest[n][c] = tmpDataPoint;

        }
        lfp_band_dataPoints_latest[n] = valuefromlfp;


    }

    if (latestDataHasNan) {
        numLoopsWithNoHeadstageData++;
    } else {
        numLoopsWithNoHeadstageData = 0;
    }

}*/


//---------------------------------------


AuxDataHandler::AuxDataHandler(TrodesConfigurationPointers conf, QVector<int> auxChannelList_in):
    conf_ptrs(conf)
{
    for (int i=0; i<auxChannelList_in.length();i++) {
        auxChannelList.push_back(auxChannelList_in.at(i));
    }

    timestamp_latest = 0;
    timestamp_previous = 0;

}

AuxDataHandler::~AuxDataHandler()
{

}

void AuxDataHandler::processNextSamples(uint32_t timestamp, int16_t *auxDataBlock)
{
    //bool newDigIOState;
    //int port;
    //char* startBytePtr; //for processing header channels
    //uint8_t* interleavedDataIDBytePtr; //for processing header channels that are interleaved

    /*
    //Now we process the auxilliary channels (digial I/O, analog I/O), if any
    for (int h = 0; h < auxChannelList.length(); h++) {
        int hch = auxChannelList[h];
        startBytePtr = ((char*)(rawData.digitalInfo + (rawIdx * headerSize))) + headerConf->headerChannels[h].startByte;

        if (headerConf->headerChannels[hch].dataType == DeviceChannel::DIGITALTYPE) {
            if (headerConf->headerChannels[hch].interleavedDataIDByte != -1) {
                interleavedDataIDBytePtr = ((uint8_t*)(rawData.digitalInfo + (rawIdx * headerSize))) + headerConf->headerChannels[hch].interleavedDataIDByte;

                if (*interleavedDataIDBytePtr & (1 << headerConf->headerChannels[hch].interleavedDataIDBit)) {
                    //This interleaved data point belongs to this channel, so update the channel. Otherwise, no update occurs.

                    tmpDataPoint = (int16_t)((*startBytePtr & (1 << headerConf->headerChannels[hch].digitalBit)) >>
                                             headerConf->headerChannels[hch].digitalBit);
                    interleavedAuxChannelStates[headerConf->headerChannels[hch].idString] = tmpDataPoint;
                } else {
                    //Use the last data point received
                    tmpDataPoint = interleavedAuxChannelStates[headerConf->headerChannels[hch].idString];
                }
            } else {
                tmpDataPoint = (int16_t)((*startBytePtr & (1 << headerConf->headerChannels[hch].digitalBit)) >>
                                         headerConf->headerChannels[hch].digitalBit);
            }

        }
        else if (headerConf->headerChannels[hch].dataType == DeviceChannel::INT16TYPE) {
            // TO DO: add analogIO output
            if (headerConf->headerChannels[hch].interleavedDataIDByte != -1) {
                interleavedDataIDBytePtr = ((uint8_t*)(rawData.digitalInfo + (rawIdx * headerSize))) + headerConf->headerChannels[hch].interleavedDataIDByte;

                if (*interleavedDataIDBytePtr & (1 << headerConf->headerChannels[hch].interleavedDataIDBit)) {
                    //This interleaved data point belongs to this channel, so update the channel. Otherwise, no update occurs.
                    tmpDataPoint = *((int16_t*)(startBytePtr)); //change to 16-bit pointer, then dereference
                    interleavedAuxChannelStates[headerConf->headerChannels[hch].idString] = tmpDataPoint;
                } else {
                    //Use the last data point received
                    tmpDataPoint = interleavedAuxChannelStates[headerConf->headerChannels[hch].idString];
                }
            } else {

                tmpDataPoint = *((int16_t*)(startBytePtr)); //change to 16-bit pointer, then dereference
            }
        }


        if (headerConf->headerChannels[hch].dataType == DeviceChannel::DIGITALTYPE) {
            newDigIOState = false;

            port = headerConf->headerChannels[hch].port;

            // check if this is an input, and if so, if the state of the port has changed
            if ((bool)tmpDataPoint != digStates[hch]) {
                newDigIOState = true;
                digStates[hch] = (bool)tmpDataPoint;

                //Send the change event info to the StreamProcessorManager, which
                //keeps a record of all the events.
                //We should have a gate for this to allow users to exclude
                //channels that are changing often

                if (headerConf->headerChannels[hch].storeStateChanges) {

                    //For latency testing
                    if (closedLoopLatencyTest) {
                        if (digStates[hch]){
                            emit functionTriggerRequest(1);
                            latencyReceiveEventTime = timestamp;
                        } else {
                            latencyReturnEventTime = timestamp;
                            qDebug() << "Closed loop latency: " << latencyReturnEventTime-latencyReceiveEventTime;
                        }
                    }

                    // check to see if we are could send out data
                    if (digitalIOHandler != nullptr) {
                        // if the state changed and we're supposed to stream data, send out the new port status.
                        if ((newDigIOState) && (digitalIOHandler->isModuleDataStreamingOn())) {
                            digitalIOHandler->sendDigitalIOData(timestamp, port,
                                                                (char)headerConf->headerChannels[hch].input,
                                                                (char)tmpDataPoint);
                        }
                    }


                    emit digitalStateChanged(hch,timestamp,digStates[hch]);
                }
            }


        }


    }
    */


}

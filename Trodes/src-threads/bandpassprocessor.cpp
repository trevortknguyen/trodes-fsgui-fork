#include "bandpassprocessor.h"
#include "avxutils.h"
#include <immintrin.h> // for AVX
#include <string.h>




//******************************************
BandPassProcessor::BandPassProcessor()
    : freq(30000)
{

    size_t N = 16*15*sizeof(float) + 31; //14 arrays of 16 floats each. +31 for extra alignment.
    full_buffer = new char[N];
    memset(full_buffer, 0, N);

    Aligned32Allocator a(full_buffer, N);

    bp_x0 = a.aligned_alloc<float>(16);
    bp_x1 = a.aligned_alloc<float>(16);
    bp_x2 = a.aligned_alloc<float>(16);

    bp_y0 = a.aligned_alloc<float>(16);
    bp_y1 = a.aligned_alloc<float>(16);
    bp_y2 = a.aligned_alloc<float>(16);
    bp_y3 = a.aligned_alloc<float>(16);
    bp_y4 = a.aligned_alloc<float>(16);

    bp_b0 = a.aligned_alloc<float>(16);
    bp_b1 = a.aligned_alloc<float>(16);
    bp_b2 = a.aligned_alloc<float>(16);

    bp_a0 = a.aligned_alloc<float>(16);
    bp_a1 = a.aligned_alloc<float>(16);
    bp_a2 = a.aligned_alloc<float>(16);
    bp_a3 = a.aligned_alloc<float>(16);
//    std::cout << "bp: " << bp_x0 <<" "<<bp_y0<<" "<<bp_b0<<" "<<bp_a0<<" "<<bp_a3<<"\n";
}

BandPassProcessor::~BandPassProcessor(){
    delete full_buffer;
}

//void BandPassProcessor::setOutputBuffer(float *output)
//{
//    bp_y4 = output;
//    std::cout << "output:" << bp_y4 << "\n";
//}
void BandPassProcessor::newSamples(const float *data, float *output){
    //shift input data over and copy new data
    std::copy(bp_x1, bp_x1+16, bp_x0);
    std::copy(bp_x2, bp_x2+16, bp_x1);
    std::copy(data, data+16, bp_x2);

    //shift y data over
    std::copy( bp_y1, bp_y1+16, bp_y0 ); //bp_y0 = bp_y1
    std::copy( bp_y2, bp_y2+16, bp_y1 ); //bp_y1 = bp_y2
    std::copy( bp_y3, bp_y3+16, bp_y2 ); //bp_y2 = bp_y3
    std::copy( bp_y4, bp_y4+16, bp_y3 ); //bp_y3 = bp_y4

    //=============================================================
    // bandpass filter
    //  y4 = b0*x0 + b1*x1 + b2*x2 + a0*y0 + a1*y1 + a2*y2 + a3*y3
    //=============================================================

    for(int i = 0; i < 2; i++){

        //load data y and a
        __m256 y0 = _mm256_load_ps( &bp_y0[i*8] );
        __m256 y1 = _mm256_load_ps( &bp_y1[i*8] );
        __m256 y2 = _mm256_load_ps( &bp_y2[i*8] );
        __m256 y3 = _mm256_load_ps( &bp_y3[i*8] );
        __m256 a0 = _mm256_load_ps( &bp_a0[i*8] );
        __m256 a1 = _mm256_load_ps( &bp_a1[i*8] );
        __m256 a2 = _mm256_load_ps( &bp_a2[i*8] );
        __m256 a3 = _mm256_load_ps( &bp_a3[i*8] );

        //multiply step.
        __m256 ay0 = _mm256_mul_ps(y0, a0);
        __m256 ay1 = _mm256_mul_ps(y1, a1);
        __m256 ay2 = _mm256_mul_ps(y2, a2);
        __m256 ay3 = _mm256_mul_ps(y3, a3);

        //load data x and b
        __m256 b0 = _mm256_load_ps( &bp_b0[i*8] );
        __m256 b1 = _mm256_load_ps( &bp_b1[i*8] );
        __m256 b2 = _mm256_load_ps( &bp_b2[i*8] );
        __m256 x0 = _mm256_load_ps( &bp_x0[i*8] );
        __m256 x1 = _mm256_load_ps( &bp_x1[i*8] );
        __m256 x2 = _mm256_load_ps( &bp_x2[i*8] );

        //multiply step.
        __m256 bx0 = _mm256_mul_ps(x0, b0);
        __m256 bx1 = _mm256_mul_ps(x1, b1);
        __m256 bx2 = _mm256_mul_ps(x2, b2);

        //combine step
        __m256 temp0 = _mm256_add_ps(ay0, ay1);
        __m256 temp1 = _mm256_add_ps(ay2, ay3);
        __m256 temp2 = _mm256_add_ps(bx0, bx1);
        temp0 = _mm256_add_ps( _mm256_add_ps(temp0, temp1), _mm256_add_ps(temp2, bx2) );

        //store value to y[4] and output
        _mm256_store_ps( &bp_y4[i*8], temp0 );
        _mm256_store_ps( output+i*8, temp0 );
    }
}

struct BPCoeffs{
    float a[4];
    float b[3];
};

BPCoeffs calculateBandPassCoeffs(int low, int high, int freq){
    BPCoeffs coeffs;

    //Calculate filter coefficients. We use a 2nd-order bandpass Bessel with z-transorm
    FilterCoefCalculator coefCalculator;

    if (low > 0) {
        coefCalculator.setBesselBandPass(low,high,freq);
    } else {
        coefCalculator.setBesselLowPass(high,freq);
    }
    coefCalculator.getACoef(coeffs.a);
    coefCalculator.getBCoef(coeffs.b);

    //coefCalculator.printresults();

    return coeffs;


}

int BandPassProcessor::setBandPassFilter(int ind, int low, int high, bool on){
    if(ind < 0 || ind > 15){
        //bad chan ind
        return -1;
    }

    if(!on){
        //Turn off filter by turning equation into y = 1*x
        bp_b2[ind] = 1.0;
        bp_b0[ind] = 0.0;
        bp_b1[ind] = 0.0;
        bp_a0[ind] = 0.0;
        bp_a1[ind] = 0.0;
        bp_a2[ind] = 0.0;
        bp_a3[ind] = 0.0;
        return 0;
    }
    bp_lo[ind] = low;
    bp_hi[ind] = high;


    BPCoeffs coeffs = calculateBandPassCoeffs(low, high, freq);



    //ind/8: 0-7 (0) or 8-15 (1)
    //ind%8: map 0-15 to 0-7
    bp_a0[ind] = coeffs.a[0];
    bp_a1[ind] = coeffs.a[1];
    bp_a2[ind] = coeffs.a[2];
    bp_a3[ind] = coeffs.a[3];

    bp_b0[ind] = coeffs.b[0];
    bp_b1[ind] = coeffs.b[1];
    bp_b2[ind] = coeffs.b[2];

    return 0;
}

int BandPassProcessor::setSamplingRate(int samplingratehz){
    /*if(samplingratehz != 20000 && samplingratehz != 30000){
        return -1;
    }*/

    freq = samplingratehz;
    return 0;
}

void BandPassProcessor::reset()
{
    memset(bp_x0, 0, 16);
    memset(bp_x1, 0, 16);
    memset(bp_x2, 0, 16);
    memset(bp_y0, 0, 16);
    memset(bp_y1, 0, 16);
    memset(bp_y2, 0, 16);
    memset(bp_y3, 0, 16);
}


//-----------------------------------

InvertProcessor::InvertProcessor()

{
    /*size_t N = 16*15*sizeof(float) + 31; //14 arrays of 16 floats each. +31 for extra alignment.
    full_buffer = new char[N];
    memset(full_buffer, 0, N);

    Aligned32Allocator a(full_buffer, N);*/


}

InvertProcessor::~InvertProcessor(){

}

void InvertProcessor::invert(const float *data, float *aligned_output)
{
    //__m256i ones = _mm256_set1_epi8(0xFF);
    __m256 negativeOnes = _mm256_set1_ps(-1.0);


    //load, compare, store first 8 elements
    __m256 dps = _mm256_load_ps(data);
    __m256 multResult = _mm256_mul_ps (dps, negativeOnes);
    _mm256_store_ps(aligned_output, multResult);

    //load, compare, store second 8 elements
    __m256 dps2 = _mm256_load_ps(data + 8);
    __m256 multResult2 = _mm256_mul_ps (dps2, negativeOnes);
    _mm256_store_ps(aligned_output+8, multResult2);



}

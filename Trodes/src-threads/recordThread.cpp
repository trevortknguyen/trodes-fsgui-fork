/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "recordThread.h"
#include "globalObjects.h"


//Used to record data to disk.  Run as a separate thread.

RecordThread::RecordThread(TrodesConfiguration* acquisitionWorkspace, bool saveDisplayedChan, int MBPerFileChunk, QObject *parent)
    : QObject(parent)
    , fileOpen(false)
    , recording(false)
    , overrideSaveDisplayedChan(false)
    , bytesWritten(0)
    , packetsWritten(0)
    , currentFilePart(0)
    , saveDisplayedChanOnly(saveDisplayedChan)
    , MBPerFileChunk(MBPerFileChunk){

    workspace = acquisitionWorkspace;
    nextFilePartTriggered = false;
    /*pullTimer = new QTimer(this);
    connect(pullTimer, SIGNAL(timeout()), SLOT(pullTimerExpired()));
    saveMarker = 0;
    pullTimer->start(100);*/




    /*QObject::moveToThread(this);
    start();*/
}

RecordThread::~RecordThread() {

}

void RecordThread::setUp() {
    int tosave = 0;

    //saveHWChan.resize(hardwareConf->NCHAN);
    saveHWChan.resize(workspace->hardwareConf.NCHAN);
    for (int i = 0; i <workspace->hardwareConf.NCHAN; i++) {
        if (workspace->streamConf.saveHWChan[i]) {
            tosave++;
        }
        saveHWChan[i] = workspace->streamConf.saveHWChan[i];
    }
    nChanConfigured = workspace->streamConf.nChanConfigured;
    if(tosave == workspace->hardwareConf.NCHAN){
        overrideSaveDisplayedChan = true;
    }
    pullTimer = new QTimer(this);
    connect(pullTimer, SIGNAL(timeout()), SLOT(pullTimerExpired()));
    saveMarker = 0;
    pullTimer->start(100);
}

/*void RecordThread::run()
{
    exec();
}*/

bool RecordThread::startNextFilePart() {
    delete outStream;
    delete file;
    currentFilePart++;

    file = new QFile;
    file->setFileName(baseName+QString(".part%1").arg(currentFilePart+1)+".rec");
    if (file->exists()) {
        return false;
    }
    qDebug() << "Creating next file part";
    writeRecConfig(baseName+QString(".part%1").arg(currentFilePart+1)+".rec", currentTimeStamp); //write the current configuration and settings to disk

    //append recorded data after the config info
    if (!file->open(QIODevice::Append)) {
        return false;
    }

    bytesWritten = 0;

    outStream = new QDataStream(file); //link outStream to the file
    return file->flush();

}

int RecordThread::openFile(QString fileName, HardwareControllerSettings hardwareSettings, HeadstageSettings headstageSettings) {

    qDebug() << "Creating recording file" << fileName;
    if (!fileOpen) {

        nextFilePartTriggered = false;
        file = new QFile;
        file->setFileName(fileName);
        if (file->exists()) {
            return -1;
        }

        QFileInfo fInfo(fileName);
        baseName = fInfo.absolutePath() + "/" + fInfo.completeBaseName();
        currentFilePart = 0;

        //GlobalConf is set when new hardware settings are returned in MainWindow

        workspace->writeRecConfig(fileName, currentTimeStamp);
        //writeRecConfig(fileName, currentTimeStamp); //write the current configuration and settings to disk

//        //find "systemTimeAtCreation=" position
//        QFile f(baseName+".rec");
//        qDebug() << "opening file:" << baseName << f.open(QIODevice::ReadOnly);
//        QString line;
//        int64_t pos = 0;
//        do {
//            line = f.readLine();
//            if (line.contains("systemTimeAtCreation=", Qt::CaseSensitive)) {
//                //get position within line
//                startRecordFieldPos = pos+line.indexOf("systemTimeAtCreation=\"");
//                break;
//            }
//            pos += f.pos();
//        } while (!line.isNull());
//        f.close();

//        rewriteStartRecordField = false;

        //append recorded data after the config info
        if (!file->open(QIODevice::Append)) {
//        if (!file->open(QIODevice::ReadWrite)) {
            return -2;
        }

        bytesWritten = 0;
        packetsWritten = 0;
        outStream = new QDataStream(file); //link outStream to the file
        file->flush();
        fileOpen = true;

        qDebug() << "File opened. Bytes free on disk:" << getBytesFree();
        if (getBytesFree() < 5000000000) {//5GB
            emit lowDiskSpaceWarning();
        }

    } else {
        qDebug() << "Error: a recording file was already open. Aborting file creation.";
    }

    return 0;
}

void RecordThread::closeFile() {

    delete outStream;
    delete file;
    fileOpen = false;
}

void RecordThread::startRecord() {

    writeMarkerMutex.lock();
    saveMarker = rawDataWritten;
    bufferLocation = rawData.writeIdx;
    writeMarkerMutex.unlock();
    recording = true;
    errorSignalEmitted = false;
    currentRecordTimestamp = 0;
    lastRecordTimestamp = 0;

}

void RecordThread::pauseRecord() {

    recording = false;
}

void RecordThread::setupSaveDisplayedChan() {

    if (saveDisplayedChanOnly) {
        dataArray = new short[nChanConfigured];
    }

}

void RecordThread::pullTimerExpired() {

    if (recording) {
//        if(rewriteStartRecordField){
//            QByteArray config = file->readAll();
//            QByteArray time = QString("%1").arg(QDateTime::currentMSecsSinceEpoch(), 20).toUtf8();
//            memcpy(config.data()+startRecordFieldPos, time.data(), 20);
//            file->seek(0);
//            file->write(config);
//            rewriteStartRecordField = false;
//        }


       writeMarkerMutex.lock();
       quint64 currentWriteCount = rawDataWritten;
       writeMarkerMutex.unlock();

       int packetsToSave = currentWriteCount-saveMarker;
       int originalPacketsToSave = packetsToSave;

       char *charPtr;

       //If the buffer has overflowed, it probably means the hard drive has become unresponsive for a while. Bad problem that the users need sto know about. We dump the entire buffer to catch up and trigger a popup window.
       if(packetsToSave > EEG_BUFFER_SIZE) {
           qDebug() << "CONTINUOUS DATA BUFFER OVERFLOW: packetsToSave =" << packetsToSave;
           qDebug() << "Skipping ahead.";
           writeMarkerMutex.lock();
           saveMarker = rawDataWritten;
           bufferLocation = rawData.writeIdx;
           writeMarkerMutex.unlock();
           emit writeError();
           lastRecordTimestamp = currentTimeStamp;
           return;
       }



       while (packetsToSave > 0) {

           //Check the time stamp of the packet
           charPtr = (char*)(rawData.timestamps+bufferLocation);
           tsPtr = (uint32_t *)(charPtr);
           currentRecordTimestamp = *tsPtr;


           if (lastRecordTimestamp > 0) {
               if (currentRecordTimestamp < lastRecordTimestamp) {

                   //Secondary check for overlow issues using the timestamp. A backward timestamp probably means that the read head was lapped by the write head.


                   qDebug() << "Record buffer overflow by" <<  lastRecordTimestamp-currentRecordTimestamp << "at time" << lastRecordTimestamp << "and buffer pos" << bufferLocation << ".Packets left to save:" << packetsToSave << ".Original number of packets to save:" << originalPacketsToSave;
                   qDebug() << "Skipping ahead.";
                   writeMarkerMutex.lock();
                   saveMarker = rawDataWritten;
                   bufferLocation = rawData.writeIdx;
                   writeMarkerMutex.unlock();
                   lastRecordTimestamp = currentTimeStamp;
                   break;

               }
           }

           lastRecordTimestamp = currentRecordTimestamp;

           int headerSize;
//           if (hardwareConf->sysTimeIncluded) {
//               headerSize = hardwareConf->headerSize-4;  //Remove the 8 bytes (4 16-bit values) defined in the config for sys clock
//           } else {
               headerSize = hardwareConf->headerSize;
//           }

           //Write the digitalInfo of the packet
           charPtr = (char*)(rawData.digitalInfo+(bufferLocation*headerSize));
           if (outStream->writeRawData(charPtr,headerSize*2) != (headerSize*2)) {
               if (!errorSignalEmitted) {
                    qDebug() << "Error writing to disk!";
                    emit noSpaceLeftError();
                    errorSignalEmitted = true;
                    pauseRecord();
                    break;
               }

           }
           bytesWritten += (headerSize*2);

           if (hardwareConf->sysTimeIncluded) {
               //Write the system clock when the packet came in
               charPtr = (char*)(rawData.sysClock+bufferLocation);
               bytesWritten += outStream->writeRawData(charPtr,8);
           }

           //charPtr = (char*)(rawData.digitalInfo+bufferLocation);
           //outStream->writeRawData(charPtr,2);
           //bytesWritten += 2;

           //Write the time stamp of the packet
           charPtr = (char*)(rawData.timestamps+bufferLocation);
           bytesWritten += outStream->writeRawData(charPtr,4);
           tsPtr = (uint32_t *)(charPtr);

           //Write the stimulation info if it exists

           /*if (hardwareConf->numStimChan > 0) {
               int numStimBytes = ceil(hardwareConf->numStimChan/8.0);
               charPtr = (char*)(rawData.stimCommands + (bufferLocation*2) );
               bytesWritten += outStream->writeRawData(charPtr,2);

               charPtr = (char*)(rawData.stimStatus + (bufferLocation*numStimBytes) );
               bytesWritten += outStream->writeRawData(charPtr,numStimBytes);

           }*/

           //bytesWritten += 4;

           charPtr = (char*)(rawData.data+(bufferLocation*hardwareConf->NCHAN));
           if (overrideSaveDisplayedChan || !saveDisplayedChanOnly) {
               //Write the data in the packet
               bytesWritten += outStream->writeRawData(charPtr,hardwareConf->NCHAN*2);
               //bytesWritten += (hardwareConf->NCHAN*2);
           }
           else {
               short *dataPtr = dataArray;
               // pull out only the channels that are in the configuration file
               for (int i = 0; i < hardwareConf->NCHAN; i++, charPtr+=2) {
                   if (saveHWChan[i]) {
                      memcpy((char *) dataPtr, charPtr, 2);
                      dataPtr++;
                   }
               }
               bytesWritten += outStream->writeRawData((char *) dataArray, nChanConfigured * 2);
               //bytesWritten += (streamConf->nChanConfigured * 2);
           }

           packetsToSave--;
           packetsWritten++;
           bufferLocation = (bufferLocation+1) % EEG_BUFFER_SIZE;
           saveMarker++;
       }

       if ((MBPerFileChunk > 0) && (bytesWritten/1000000 > (quint64)MBPerFileChunk) && (!nextFilePartTriggered)) {
            nextFilePartTriggered = true;
            pauseRecord();
            emit triggerNextFilePart();

           //startNextFilePart(); //returned value not being used
       }

       //file->flush();
    }


}

quint64 RecordThread::getBytesWritten() {
    return bytesWritten;
}

quint64 RecordThread::getPacketsWritten() {
    return packetsWritten;
}

qint64 RecordThread::getBytesFree() {
    qint64 bytesfree = -1;
    if (fileOpen) {
        QStorageInfo driveInfo;
        QFileInfo fi(file->fileName());
        driveInfo.setPath(fi.absolutePath());
        bytesfree = driveInfo.bytesFree();

    }

    return bytesfree;

}

void RecordThread::endRecordThread() {
    //pullTimer->stop();
    //quit();

    pullTimer->stop();
    emit finished();


}

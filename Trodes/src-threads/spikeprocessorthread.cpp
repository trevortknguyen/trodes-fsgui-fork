#include "spikeprocessorthread.h"


SpikeProcessingBuffer::SpikeProcessingBuffer()
{
    /*This buffer object stores spikes from multiple streamprocessor threads.
     * Therefore, this is a multiple writers, one reader situation.
     * Each writer is assigned a unique 'source index' to keep the writing locations separate.
     * This way, we can use a simple ring buffer for each writer.
     */


    numSources = 0;
    lock = 0;
    bufferSize = 1000; //This is the size of each ring buffer. Each item in the ring buffer honds all the spikes processed in one loop of the streamprocessor
    spikesAvailable = new QSemaphore(0); //User to signal that a new spike has come in from one of the sources

}

SpikeProcessingBuffer::~SpikeProcessingBuffer()
{

    //delete all of the semaphores
    while (spikesAvailable->tryAcquire(1)) {}
    delete spikesAvailable;
    while (spikesAvailableInSource.length() > 0) {
        while (spikesAvailableInSource[0]->tryAcquire(1)) {}
        delete spikesAvailableInSource.takeFirst();
    }

}

int SpikeProcessingBuffer::addSourceBuffer() {

    /*This is where we assign a unique source index to each streamprocessor thread.
     * The index is returned to the calling streamprocessor.
     * We use a lock to make sure that only one index is being assigned at once.
     */

    if (lock.testAndSetRelaxed(0,1)) {

        int retSource;
        numSources++;
        QVector<QVector<SpikeWaveform> > newBuf;
        for (int i=0;i<bufferSize;i++) {
            QVector<SpikeWaveform> s;
            newBuf.push_back(s);
        }
        spikeBuffer.push_back(newBuf);

        spikesAvailableInSource.push_back(new QSemaphore(0));
        readHeads.push_back(0);
        writeHeads.push_back(0);
        retSource = numSources-1;
        lock = 0;
        return retSource; //The dedicated index of that source
    } else {
        return -1;
    }
}

void SpikeProcessingBuffer::addSpikes(const QVector<SpikeWaveform> &sp, int sourceIndex)
{
    /*Here the calling streamprocessor adds spikes to it's dedicated ring buffer
    */


    if (numSources > sourceIndex && spikesAvailableInSource[sourceIndex]->available() < bufferSize) {
        spikeBuffer[sourceIndex][writeHeads.at(sourceIndex)] = sp;
        writeHeads[sourceIndex] = (writeHeads[sourceIndex]+1)%bufferSize;
        spikesAvailableInSource[sourceIndex]->release(1); //We release a semaphore for this specific source
        spikesAvailable->release(1); //We also realease a separate sepaphore to indicate that a new spike came in
    } else if (spikesAvailableInSource[sourceIndex]->available() >= bufferSize) {
        qDebug() << "Error: Spike processor buffer overflow.";
    }

}
bool SpikeProcessingBuffer::fetchSpikes(QVector<SpikeWaveform> &sp)
{
        /*This is called from the spikeprocessor thread. If new spikes are available from any of the streamprocessor threads,
         * Then the spike is processed.
         */

        if (spikesAvailableInSource.isEmpty() || (lock==1)) {
            return false;
        }

        if (spikesAvailable->tryAcquire(1,100)) {

            for (int i=0; i<numSources; i++) {
                if (spikesAvailableInSource[i]->tryAcquire(1)) {                    
                    sp = spikeBuffer[i][readHeads.at(i)];
                    readHeads[i] = (readHeads[i]+1)%bufferSize;
                    return true;
                }
            }
            qDebug() << "[SpikeProcessor] Error in spike processing queue";
            return false;
        } else {
            return false;
        }

}

SpikeProcessorThread::SpikeProcessorThread(QObject *parent, TrodesConfigurationPointers c_ptrs) :
    conf_ptrs(c_ptrs)
{

}

void SpikeProcessorThread::runLoop()
{
    /*This is the main loop of the spike processing thread, and it handles all spikes from all streamprocessor threads.
    */

    quitNow = false;
    QVector<SpikeWaveform> newSpikes;
    while (!quitNow) {
        if (spikeBuffer.fetchSpikes(newSpikes)) {            
            emit newProcessedSpikes(newSpikes); //For now, we simply send the spike to the spike display window, but more processing will be done here eventually.
        }
    }

    qDebug() << "[SpikeProcessor] Spike processor thread ended.";

}
void SpikeProcessorThread::setUp()
{

}

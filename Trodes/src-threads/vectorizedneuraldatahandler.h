#ifndef VECTORIZEDNEURALDATAHANDLER_H
#define VECTORIZEDNEURALDATAHANDLER_H
#include "streamprocesshandlers.h"
#include "auxchanprocessor.h"
#include "displayprocessor.h"
#include "lowpassprocessor.h"
#include "bandpassprocessor.h"
#include "referenceprocessor.h"
#include "thresholdprocessor.h"
#include "notchprocessor.h"
#include "avxfloattoshort.h"
#include "avxutils.h"

/* Vectorized data handler
 *
 * Implements the AbstractNeuralDataHandler interface while using
 * avx2 instructions.
 * https://en.wikipedia.org/wiki/Advanced_Vector_Extensions#Advanced_Vector_Extensions_2
 * https://software.intel.com/sites/landingpage/IntrinsicsGuide/#
 *
 * These instructions work differently than normal cpu instructions, so
 * the data storage will be different than the other implementation
 *
 *
 * Each VectorizedNeuralDataHandler is consisted of arrays of "avx processors",
 * like bandpassprocessor, referenceprocessor, etc. Look at the #include's for info.
 *
 *
 */

class NeuroPixelsLFPGrabber
{
public:
    NeuroPixelsLFPGrabber(TrodesConfigurationPointers conf, bool *ok);
    ~NeuroPixelsLFPGrabber();
    void fillLFPDataPoints(const char *auxDataBlock);
    bool isReady();
    const int16_t* getFullLFPBlock();
private:
    //There are a total of 32 ADC's, each taking 12 samples. Each incoming packet constains
    //one sample per ADC. After 12 packets, all channels have been sampled. Then we can pass the complete
    //packet on the the reference code
    int numchans;
    int numchans_rounded16;
    int ADCChannelByte;
    int numberOfProbes;
    bool foundLFPSection;
    bool readyForProcessing;
    int16_t* fullLFPpacket;

    //Pointer holding actual full buffer (need to save for new and delete)
    uint8_t *full_buffer;
};


class VectorizedNeuralDataHandler : public AbstractNeuralDataHandler
{
public:
    VectorizedNeuralDataHandler(TrodesConfigurationPointers conf, QList<int> nTrodeList_in, uint32_t extraSteps = 0);
    ~VectorizedNeuralDataHandler();
    void processNextSamples(uint32_t timestamp,const int16_t* neuralDataBlock, const int16_t* CARDataBlock, const char* auxDataBlock);
    const int16_t* getNTrodeSamples(int nTrodeIndex,DataBand d);
    const int16_t* getEntirePacket(DataBand d, int& numSamples);


    void processDisplaySamples(bool newbin);
    const float* getMinDisplaySamples(int nTrodeIndex);
    const float* getMaxDisplaySamples(int nTrodeIndex);

    void stepPreviousSample();
    virtual void updateChannels();
    virtual void updateChannelsRef();
    virtual void updateFilters();
    void updateThresholds();
private:
    void resetFilters();

    int numchans;
    int numchans_rounded16;
    int numprocessors;

    //These are the channels that are stim capable. We need to check them
    //at every packet to see if they are currently stimulating. We keep track
    //of the indices of the stim capable channels to reduce overhead of the check loop
    QVector<int> stimCapableChannels;

    //Special processors (extra steps)
    NeuroPixelsLFPGrabber *npLFP;


    //these pointers are aligned to 32byte boundaries from the full_buffer
    float *raw_ref_float, *spike_ref_float, *lfp_ref_float; //intermediate float buffers to hold references
    float *spike_out_float, *lfp_out_float, *spike_thresh_float, *stim_float;
    int16_t *raw_unref, *raw_lfp_unref, *raw_ref, *spike, *lfp, *stim; //output buffers saved on every sample
    QVector<int16_t*> spike_out_packets;
    float *minValues, *maxValues; //display buffers, aligned

    //Pointer holding actual full buffer (need to save for new and delete)
    uint8_t *full_buffer;

    //Pointer holding full buffer for interpolation (need to save for new and delete)
    uint8_t *interp_buffer;
    float *raw_ref_float_interp, *spike_ref_float_interp, *lfp_ref_float_interp; //intermediate float buffers to hold references

    //All ntrodes and their channels are laid out in one contiguous buffer
    QVector<int> nt2Ind; //Convert a nTrodeList index to the index along the above buffers
    QVector<unsigned int> numChannelsPerNtrode; //Convert a nTrodeList index to the index along the above buffers

    //All parameters for each channel for processing vectorized operations
    int32_t* chanByteIndices;
    int32_t* refIndices;
    int32_t* refOn;
    int32_t* lfpRefOn;
    int32_t* rawRefOn;
    int32_t* grouprefIndices;
    int32_t* grouprefOn;
    int32_t* spikeChannelAboveThresh;

    uint8_t *param_buffer;

//    NotchProcessor *notchprocs;
    BandPassProcessor *spikeprocs;
    //BandPassProcessor *lfpprocs;
    LowPassProcessor *lfpprocs;
    DisplayProcessor *dispprocs;


    //Spike detection variables
    //---------------------------
    void processSpikes();
    int ringBufferWriteHead;
    int ringBufferStartWaveformHead;
    const int numSamplesPerSpike = 40; //final number of samples in the spike waveform
    const int samplesBeforeThresh = 10; //final number of samples retained before the threshold crossing occurs
    const int peakAlignInd = 13; //Points into window where peaks are aligned (needs to be > samplesBeforeThresh)
    const int alignmentMargins = 10; //number of extra samples to collect on each end of the waveform to enable alignment
    const int numSamplesPerSpike_preAlignment = numSamplesPerSpike+alignmentMargins+alignmentMargins; //pre aligment window size
    const int samplesBeforeThresh_preAlignment = samplesBeforeThresh+alignmentMargins; //pre alignment number of samples before thresh
    const int samplesAfterThresh_preAlignment = (numSamplesPerSpike-samplesBeforeThresh)+alignmentMargins; //pre alignment number of samples after thresh (including first point after thresh crossing)
    const int ringBufferSize = numSamplesPerSpike_preAlignment*2; //number of samples to keep in the ring buffer. Should be two full windows.
    QVector<bool>  ntrodeTriggered; //is this ntrode currently collecting samples in a spike?
    QVector<int>   ntrodeWaveformStart; //if triggered, remember where the waveform window started (pre alignment)
    QVector<int>   ntrodeWaveformLeftToCollect; //if triggered, keep track of how many points are left to collect (pre alignment)
    QVector<int>   ntrodeTriggerChannel; //if triggered, keep track of which channel was the first to trigger (assumed to be the biggest signal)
    QVector<int16_t>   ntrodeTriggerMaxVal;
    QVector<int>   ntrodeTriggerMaxValInd;
    QVector<uint32_t>  ntrodeTriggerPeakTimestamp;
    QVector <bool> channelTriggersOn; //TODO: use this!!
    //-----------------------------

};

#endif // VECTORIZEDNEURALDATAHANDLER_H

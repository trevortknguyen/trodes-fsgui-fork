#ifndef SGRINGQUEUE_H
#define SGRINGQUEUE_H

/* SG (scatter-gather) ring queue
 * Status: Put on hold for now, I just wanted to type out my design first before I forget it
 *
 * A concurrent queue where multiple producer threads can place the result of their calculations (scatter) and
 * a single consumer can wait on each "bucket" to fill up and consume it
 *
 * Use cases: stream processors (producers) sending split up data to consumer threads to piece back together and process
 *   , like sort spikes or stream out via API
 *
 * Implementation:
 *  - each "bucket" contains slots for each producer to place their array in
 *  - atomic semaphore that counts up each time a producer fills the bucket, and back to 0 when the consumer empties it
 *  - main issues:
 *      - slow consumer: what happens when head catches up to tail? if the soln is to overwrite and push the consumer, then
 *          the consumer could be starved out forever
 *      - fast consumer: if tail is always at the head, not that much of a problem (i think). timed wait or return immediately.
 *
 * Resources to look at:
 *  https://www.linuxjournal.com/content/lock-free-multi-producer-multi-consumer-queue-ring-buffer
 *  https://preshing.com/20150316/semaphores-are-surprisingly-versatile/
 *
 * constructor: data type T, n producers, length of array for each producer, header type H, header producer
 *
 * enqueue(producertok p, T* firstitemptr, size_t count):
 *      1. using token, get head bucket of this producer p
 *      2. if bucket is already full:
 *          - means that this producer has circled around to the tail of the buffer.
 *          - bump consumer tail to next, reset bucket to 0, add data, increment bucket
 *      3. add data, increment bucket count
 *      4. if count==numproducers, increment head
 *
 * dequeue(T* bufferstart, size_t count):
 *      1. check tail bucket
 *      2. if full, consume data, set bucket to 0. else, return false
 *      3.
 */
#include <cstddef>
#include <stdint.h>
#include <vector>
#include <atomic>
#include <iostream>
#include <memory>
#include "benaphore.h"

#include <limits.h>
#include <stddef.h>

#if SIZE_MAX == UINT_MAX
typedef int ssize_t;        /* common 32 bit case */
#elif SIZE_MAX == ULONG_MAX
typedef long ssize_t;       /* linux 64 bits */
#elif SIZE_MAX == ULLONG_MAX
typedef long long ssize_t;  /* windows 64 bits */
#elif SIZE_MAX == USHRT_MAX
typedef short ssize_t;      /* is this even possible? */
#else
#error platform has exotic SIZE_MAX
#endif

struct producer_imp{
    uint64_t head;
    uint32_t offset;
//    size_t datalen;
};

typedef producer_imp* producer;

template<typename T, typename H, size_t queueLength>
class SGRingQueue
{
public:
    //totalDataLength: length of each bucket
    //numProducers: number of producers before each bucket is marked as done
    SGRingQueue(size_t totalDataLength, size_t numProducers);
    ~SGRingQueue();

    //Producer API
    //  - designate one producer to enqueue the header
    //  - order of registered producers is order of data stored in buffer
    //  - no checking is done by the queue to ensure producers are properly passing in enough data
    producer registerProducer(size_t dataLength);//register producers.

    bool enqueue(producer &ptok, const T* itemFirst, size_t count);           //enqueue data from this producer
    bool enqueue(producer &ptok, H header, const T* itemFirst, size_t count); //enqueue data from this producer as well as header

    //Consumer API
    //  - functions for the consumer to construct large enough buffers
    //  -
    size_t totalDataLength() const;
    size_t numberOfProducers() const;

    size_t availableBlocks() const;                                        //number of available blocks
    bool dequeue(H* header, T* itemFirst);                                 //dequeue no wait
//    bool dequeueBlocking(H* header, T* itemFirst);                         //fully blocking dequeue
//    bool dequeueWaitUSecs(H* header, T* itemFirst, int64_t timeoutUSecs);  //timeout dequeue in microseconds
private:
    struct block{
//        std::atomic<uint32_t> count;
        LightweightSemaphore countsem;
        H header;
        T* data;
        block()/* : count(0)*/{}
    };

    NonRecursiveBenaphore taillock;
    std::atomic<uint64_t> tail;
    std::atomic<uint64_t> head;
    block* queue;
    T* buffer;
    ssize_t num_registered_producers;
    ssize_t total_data_length;
    ssize_t total_queue_length;
    ssize_t next_producer_offset;
    NonRecursiveBenaphore register_producer_lock;

    inline unsigned int q_ind(producer &ptok){
        return ptok->head & (total_queue_length-1);
    }
    inline bool isfull(producer &ptok){
        return queue[q_ind(ptok)].countsem.availableApprox() == num_registered_producers;
    }
    inline void producer_fill(producer &ptok){
        queue[q_ind(ptok)].countsem.signal();
        ++ptok->head;
    }
    inline void empty_bucket(producer &ptok){
        queue[q_ind(ptok)].countsem.waitMany(num_registered_producers);
    }
    inline unsigned int q_ind_consumer(){
        return tail & (total_queue_length-1);
    }
    inline bool isfull_consumer(){
        return queue[q_ind_consumer()].countsem.availableApprox() == num_registered_producers;
    }
    inline void empty_bucket_consumer(){
        queue[q_ind_consumer()].countsem.waitMany(num_registered_producers);
    }
};


template<typename T, typename H, size_t queueLength>
SGRingQueue<T, H, queueLength>::SGRingQueue(size_t totalDataLength, size_t numProducers)
    : tail(0),
      head(0),
      buffer(new T[totalDataLength*queueLength]),
      num_registered_producers(0),
      total_data_length(totalDataLength),
      total_queue_length(queueLength),
      next_producer_offset(0)
{
    static_assert (queueLength && ((queueLength & (queueLength-1)) == 0), "Queue length must be a power of two!" );

    queue = new block[total_queue_length];
    for(unsigned int i = 0; i < total_queue_length; ++i){
        queue[i].data = buffer + total_data_length*i;
//        queue[i].count = 0;
    }
}

template<typename T, typename H, size_t queueLength>
SGRingQueue<T, H, queueLength>::~SGRingQueue()
{
    delete [] queue;
    delete [] buffer;
    queue = nullptr;
    buffer = nullptr;
}

template<typename T, typename H, size_t queueLength>
producer SGRingQueue<T, H, queueLength>::registerProducer(size_t dataLength)
{
    register_producer_lock.lock();

    producer_imp *p = new producer_imp;
    p->head = 0;
    p->offset = next_producer_offset;
//    p->datalen = dataLength;

    //only increment num_producers after finished with masks.
    num_registered_producers++;
    next_producer_offset += dataLength;

    register_producer_lock.unlock();
    return p;
}

template<typename T, typename H, size_t queueLength>
bool SGRingQueue<T, H, queueLength>::enqueue(producer &ptok, const T *itemFirst, size_t count)
{
    //queue at head is still full
    //do i still need to check if tail is here as well? tail should only point to the oldest full block
    if(isfull(ptok)){
        //if consumer is falling behind, there will be contention with that bucket.
        return false;
        /*taillock.lock();
        empty_bucket(ptok);
        ++tail;
        taillock.unlock();*/
    }

    //continue with enqueue. copy data
    std::copy(itemFirst, itemFirst+count, queue[q_ind(ptok)].data+ptok->offset);

    producer_fill(ptok);

    if(isfull(ptok)){
        //if after incrementing count reached num_producers, then increment head
        ++head;
    }

    return true;
}

template<typename T, typename H, size_t queueLength>
bool SGRingQueue<T, H, queueLength>::enqueue(producer &ptok, H header, const T *itemFirst, size_t count)
{
    //queue at head is still full
    //do i still need to check if tail is here as well?
    if(isfull(ptok)){
        return false;
        /*taillock.lock();
        empty_bucket(ptok);
        ++tail;
        taillock.unlock();*/
    }

    //continue with enqueue. copy data and header
    std::copy(itemFirst, itemFirst+count, queue[q_ind(ptok)].data+ptok->offset);
    queue[q_ind(ptok)].header = header;

    producer_fill(ptok);

    if(isfull(ptok)){
        //if after incrementing count reached num_producers, then increment head
        ++head;
    }

    return true;
}

template<typename T, typename H, size_t queueLength>
size_t SGRingQueue<T, H, queueLength>::totalDataLength() const
{
    return total_data_length;
}

template<typename T, typename H, size_t queueLength>
size_t SGRingQueue<T, H, queueLength>::numberOfProducers() const
{
    return num_registered_producers;
}

template<typename T, typename H, size_t queueLength>
size_t SGRingQueue<T, H, queueLength>::availableBlocks() const
{
    return head - tail;
}

template<typename T, typename H, size_t queueLength>
bool SGRingQueue<T, H, queueLength>::dequeue(H *header, T *itemFirst)
{
    if(isfull_consumer()){
        //tail is full, dequeue data, empty bucket, increment tail.
        taillock.lock();
        *header = queue[q_ind_consumer()].header;
        std::copy(queue[q_ind_consumer()].data, queue[q_ind_consumer()].data+total_data_length, itemFirst);
        empty_bucket_consumer();
        ++tail;
        taillock.unlock();
    }
    else{
        return false;
    }
    return true;
}


//template<typename T, typename H, size_t queueLength>
//bool SGRingQueue<T, H, queueLength>::dequeueBlocking(H *header, T *itemFirst)
//{

//}

//template<typename T, typename H, size_t queueLength>
//bool SGRingQueue<T, H, queueLength>::dequeueWaitUSecs(H *header, T *itemFirst, int64_t timeoutUSecs)
//{

//}


#endif // SGRINGQUEUE_H

#ifndef THRESHOLDPROCESSOR_H
#define THRESHOLDPROCESSOR_H

#include <stdint.h>
#include <array>

//16 channel threshold processor
class alignas(32) ThresholdProcessor
{
public:
    ThresholdProcessor();

    //must be 32-bit floats AND must be aligned along 32-byte boundary
    //Algorithm:
    //  1. threshpassed = data >= threshold ? 0xff : 0x00
    //  2. threshexists = test threshpassed
    //void newSamples(const float *data);
    static void checkThreshCrossings(const float *data, const float *thresh, int32_t *output);

    //Get array of ints of whether each channel passed thresh or not
    //const int32_t* getThreshPassed() const {return threshpassed;}

    //If any of the channels of this processor returned that a threshold was passed
    //const int32_t  getThreshExists() const {return threshexists;}

    //Set threshold values
    //int setThreshold(int ind, float thresh);

private:
    //int32_t threshpassed[16];
    //float thresholds[16];
    //int32_t threshexists;
};

#endif // THRESHOLDPROCESSOR_H

﻿#include "lowpassprocessor.h"
#include "avxutils.h"
#include <immintrin.h> // for AVX
#include <string.h>
LowPassProcessor::LowPassProcessor()
    : freq(30000)
{
    size_t N = 16*6*sizeof(float) + 31;
    full_buffer = new char[N];
    memset(full_buffer, 0, N);

    Aligned32Allocator a(full_buffer, N);

    lp_y0 = a.aligned_alloc<float>(16);
    lp_y1 = a.aligned_alloc<float>(16);
    lp_y2 = a.aligned_alloc<float>(16);

    lp_a0 = a.aligned_alloc<float>(16);
    lp_a1 = a.aligned_alloc<float>(16);

    lp_b0 = a.aligned_alloc<float>(16);
}

LowPassProcessor::~LowPassProcessor(){
    delete full_buffer;
}
//void LowPassProcessor::setOutputBuffer(float *output)
//{
//    lp_y2 = output;
//}

void LowPassProcessor::newSamples(const float *data, float *output){

    //shift y data over
    std::copy( lp_y1, lp_y1+16, lp_y0 ); //lp_y0 = lp_y1
    std::copy( lp_y2, lp_y2+16, lp_y1 ); //lp_y1 = lp_y2

    for(int i = 0; i < 2; ++i){
        //=============================================================
        // lowpass filter
        //  y2 = b0*x0 + a0*y0 + a1*y1
        //=============================================================

        //load data
        __m256 b0 = _mm256_load_ps( &lp_b0[i*8] );
        __m256 x0 = _mm256_load_ps( &data[i*8] );
        __m256 a0 = _mm256_load_ps( &lp_a0[i*8] );
        __m256 a1 = _mm256_load_ps( &lp_a1[i*8] );
        __m256 y0 = _mm256_load_ps( &lp_y0[i*8] );
        __m256 y1 = _mm256_load_ps( &lp_y1[i*8] );

        //multiply step.
        __m256 bx0 = _mm256_mul_ps(x0, b0);
        __m256 ay0 = _mm256_mul_ps(a0, y0);
        __m256 ay1 = _mm256_mul_ps(a1, y1);

        //combine step
        __m256 temp0 = _mm256_add_ps(bx0, _mm256_add_ps(ay0, ay1));

        //store value back to y[2]
        _mm256_store_ps( &lp_y2[i*8], temp0);
        _mm256_store_ps( output+i*8, temp0 );
    }
}

struct LPCoeffs{
    float a[2];
    float b;
};
LPCoeffs calculateLowPassCoeffs(int high, int freq){
    LPCoeffs coeffs;

    //Calculate filter coefficients. We use a 2nd-order lowpass Bessel with z-transorm
    FilterCoefCalculator coefCalculator;
    coefCalculator.setBesselLowPass(high,freq);
    coefCalculator.getACoef(coeffs.a);
    coefCalculator.getBCoef(&coeffs.b);

    //coefCalculator.printresults();

    return coeffs;

    /*
    if(freqkhz == 20){
        switch (high){
            case 100:
                coeffs.a[0] = -9.33125427e-01;coeffs.a[1] = 1.93158271e+00;
                coeffs.b = 1.0/6.48206982e+02;
                break;
            case 200:
                coeffs.a[0] = -8.70723063e-01;coeffs.a[1] = 1.86476091e+00;
                coeffs.b = 1.0/1.67724692e+02;
                break;
            case 300:
                coeffs.a[0] = -8.12493830e-01;coeffs.a[1] = 1.79953099e+00;
                coeffs.b = 1.0/7.71436104e+01;
                break;
            case 400:
                coeffs.a[0] = -7.58158652e-01;coeffs.a[1] = 1.73588713e+00;
                coeffs.b = 1.0/4.49003913e+01;
                break;
            case 500:
                coeffs.a[0] = -7.07457116e-01;coeffs.a[1] = 1.67382144e+00;
                coeffs.b = 1.0/2.97303395e+01;
                break;
            default:
                break;
        }
    }else
    if(freqkhz == 25){
        switch (high){
            case 100:
                coeffs.a[0] = -9.46132627e-01; coeffs.a[1] = 1.94513845e+00;
                coeffs.b = 1.0/1.00586139e+03;
                break;
            case 200:
                coeffs.a[0] = -8.95166947e-01; coeffs.a[1] = 1.89129835e+00;
                coeffs.b = 1.0/2.58491728e+02;
                break;
            case 300:
                coeffs.a[0] = -8.46946655e-01; coeffs.a[1] =.83847820e+00;
                coeffs.b = 1.0/1.18085246e+02;
                break;
            case 400:
                coeffs.a[0] = -8.01323864e-01; coeffs.a[1] = 1.78667556e+00;
                coeffs.b = 1.0/6.82672909e+01;
                break;
            case 500:
                coeffs.a[0] = -7.58158652e-01; coeffs.a[1] = 1.73588713e+00;
                coeffs.b = 1.0/4.49003913e+01;
                break;
            default:
                break;
        }
    }else
    if(freqkhz == 30){
        switch (high){
            case 100:
                coeffs.a[0] = -9.54904667e-01; coeffs.a[1] = 1.95421109e+00;
                coeffs.b = 1.0/1.44179090e+03;
                break;
            case 200:
                coeffs.a[0] = -9.11842923e-01; coeffs.a[1] = 1.90913163e+0;
                coeffs.b = 1.0/3.68827887e+02;
                break;
            case 300:
                coeffs.a[0] = -8.70723063e-01; coeffs.a[1] = 1.86476091e+00;
                coeffs.b = 1.0/1.67724692e+02;
                break;
            case 400:
                coeffs.a[0] = -8.31457516e-01; coeffs.a[1] = 1.821097740e+00;
                coeffs.b = 1.0/9.65271770e+01;
                break;
            case 500:
                coeffs.a[0] = -7.93962663e-01; coeffs.a[1] = 1.778140493e+00;
                coeffs.b = 1.0/6.32024557e+01;
                break;
            default:
                break;
        }
    }else
    if(freqkhz == 1){
        switch (high){
            case 100:
                coeffs.a[0] = -2.50495817e-01; coeffs.a[1] = 9.22123218e-01;
                coeffs.b = 1.0/3.04532109e+00;
                break;
            case 200:
                coeffs.a[0] = -6.27481541e-02; coeffs.a[1] = 3.49319595e-01;
                coeffs.b = 1.0/1.40168204e+00;
                break;
            case 300:
                coeffs.a[0] = -1.57181501e-02; coeffs.a[1] = 9.11277009e-02;
                coeffs.b = 1.0/1.081559950e+00;
                break;
            default:
                break;
        }
    }

    return coeffs;
    */
}

int LowPassProcessor::setLowPassFilter(int ind, int high, bool on){
    if(ind < 0 || ind > 15){
        //bad chan ind
        return -1;
    }

    if(!on){
        //Turn off filter by turning equation into y = 1*x
        lp_b0[ind] = 1.0;
        lp_a0[ind] = 0.0;
        lp_a1[ind] = 0.0;
        return 0;
    }

    LPCoeffs coeffs = calculateLowPassCoeffs(high, freq);
    lp_hi[ind] = high;

    //ind/8: 0-7 (0) or 8-15 (1)
    //ind%8: map 0-15 to 0-7
    lp_a0[ind] = coeffs.a[0];
    lp_a1[ind] = coeffs.a[1];

    lp_b0[ind] = coeffs.b;

    return 0;
}

int LowPassProcessor::setSamplingRate(int samplingratehz){
    /*if(samplingratehz != 20000 && samplingratehz != 30000){
        return -1;
    }*/
    freq = samplingratehz;
    return 0;
}

void LowPassProcessor::reset()
{
    memset(lp_y0, 0, 16);
    memset(lp_y1, 0, 16);
    memset(lp_y2, 0, 16);
}

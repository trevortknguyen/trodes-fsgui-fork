/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2015 Mattias Karlsson, Caleb Kemere

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RHYTHMTHREAD_H
#define RHYTHMTHREAD_H


#include <QThread>
#include <QVector>
#include "abstractTrodesSource.h"
#include "rhythm-api/okFrontPanelDLL.h"
#include "rhythm-api/rhd2000datablock.h"
#include "rhythm-api/rhd2000evalboard.h"
#include "rhythm-api/rhd2000registers.h"


// RHD2000 chip ID numbers from ROM register 63
#define CHIP_ID_RHD2132  1
#define CHIP_ID_RHD2216  2
#define CHIP_ID_RHD2164  4

// Constant used in software to denote RHD2164 MISO B data source
#define CHIP_ID_RHD2164_B  1000

// RHD2164 MISO ID numbers from ROM register 59
#define REGISTER_59_MISO_A  53
#define REGISTER_59_MISO_B  58


class RhythmRuntime : public AbstractSourceRuntime {
  Q_OBJECT
public:
  RhythmRuntime(QObject *parent);
  QVector<unsigned char> buffer;

  enum initializationError {
      INITIALIZATION_SUCCESS,
      FRONTPANEL_LIB_LOAD_ERROR,
      XEM6010_NOT_FOUND,
      BITFILE_NOT_FOUND,
      BITFILE_UPLOAD_ERROR,
      TOO_FEW_CHANNELS,
      OTHER_ERROR
  };

public:
  QAtomicInteger<bool> toggleFastSettle;
  bool fastSettleEnabled;

public slots:
  void Run(void);
  int initializeBoard(void);
  void closeBoard(void);
  void changeSampleRate(int samplingRate);

signals:
  void stopped(void);

private:
  Rhd2000EvalBoard *rhythmInterfaceBoard;

  bool deviceFound, boardInitialized;
  double cableLengthPortA, cableLengthPortB, cableLengthPortC, cableLengthPortD;

  double boardSampleRate;
  int numUsbBlocksToRead;

  QVector<int> chipId;
  //QVector<bool> auxDigOutEnabled;
  queue<Rhd2000DataBlock> dataQueue;

  int scanPorts(void);
  int deviceId(Rhd2000DataBlock* dataBlock, int stream, int& register59Value);
  void enableFastSettle(bool fastSettleEnabled);

};

class RhythmInterface : public AbstractTrodesSource {
  Q_OBJECT

public:
  RhythmInterface(QObject *parent);
  ~RhythmInterface(void);
  int state;

private:
  //struct libusb_transfer **transfers;
  //int n_transfers;
  RhythmRuntime *rhythmDataProcessor;
  QThread *workerThread;

public slots:
  void InitInterface(void);
  void StartAcquisition(void);
  void StopAcquisition(void);
  void CloseInterface(void);
  void SendSettleCommand(void);


};



#endif // RHYTHMTHREAD_H

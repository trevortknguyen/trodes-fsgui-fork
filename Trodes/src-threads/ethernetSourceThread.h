/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ETHERNETSOURCETHREAD_H
#define ETHERNETSOURCETHREAD_H


#include <QThread>
#include <QVector>
#include <QtNetwork>
#include "abstractTrodesSource.h"
#include "trodesSocketDefines.h"


/*
* the port address for incoming ephys data is 8200
* the port address for ephys control (start, stop, etc) is 8100
* the port address for ECU stateScript data is 8110
*/

class EthernetSourceRuntime : public AbstractSourceRuntime {
  Q_OBJECT
public:
  EthernetSourceRuntime(QObject *parent);
  ~EthernetSourceRuntime(void);
  QVector<unsigned char> buffer;

private:

  QUdpSocket *udpDataSocket;
  int packets;


public slots:
  void Run(void);

signals:
  void resetInterface();

};

class EthernetInterface : public AbstractTrodesSource {
  Q_OBJECT

public:
  EthernetInterface(QObject *parent);
  ~EthernetInterface(void);
  int state;
  quint64 getTotalDroppedPacketEvents();
  quint64 getTotalUnresponsiveHeadstagePackets();
  void SendImpendaceMeasureCommand(HardwareImpedanceMeasureCommand s);
  //HardwareControllerSettings lastControllerSettings;
  //HeadstageSettings          lastHeadstageSettings;

private:
  //struct libusb_transfer **transfers;
  //int n_transfers;
  EthernetSourceRuntime *UDPDataProcessor;
  //QThread       *workerThread;

  QUdpSocket *udpControlSocket;
  QUdpSocket *udpControlSocketReceiver;
  QUdpSocket *udpECUDirectPort;

  void sendMessageToHardware(QByteArray datagram);
  void sendMessageToHardware_wait(QByteArray datagram, int waitMsecs = 5);
  QElapsedTimer lastSendTime;

protected:
  bool getDataStreamFromHardware(QByteArray* data, int numBytes) override;
  bool sendCommandToHardware(QByteArray &command) override;


private slots:
  void controlSocketAcknowledgeReceived();
  void restartThread();

public slots:
  void InitInterface(void);
  bool RunDiagnostic(QByteArray *data,HeadstageSettings &hsSettings, HardwareControllerSettings &cnSettings);
  void ResetInterface(void);
  void StartAcquisition(void);
  void StartSimulation(void);
  void StopAcquisition(void);
  void CloseInterface(void);
  void SendSettleCommand(void);
  void SendSettleChannel(int byteInPacket, quint8 bit, int delay, quint8 triggerState);
  void SendFunctionTrigger(int funcNum);
  void SendSDCardUnlock(void);
  void ConnectToSDCard(void);
  void ReconfigureSDCard(int numChannels);
  void SendHeadstageSettings(HeadstageSettings s);
  bool SendNeuroPixelsSettings(NeuroPixelsSettings s);
  void SendSaveHeadstageSettings();
  void SetStimulationParams(StimulationCommand s);
  bool SendGlobalStimulationSettings(GlobalStimulationSettings s);
  void SendGlobalStimulationAction(GlobalStimulationCommand s);
  void ClearStimulationParams(uint16_t slot);
  void SendStimulationStartSlot(uint16_t slot);
  void SendStimulationStartGroup(uint16_t group);
  void SendStimulationStopSlot(uint16_t slot);
  void SendStimulationStopGroup(uint16_t group);

  void EnableECUShortcutMessages();
  void SendECUShortcutMessage(uint16_t function);

  void SendControllerSettings(HardwareControllerSettings s);
  HeadstageSettings GetHeadstageSettings();
  HardwareControllerSettings GetControllerSettings();
  int MeasurePacketLength(HeadstageSettings settings);
  static bool isSourceAvailable();
};


#endif // ETHERNETSOURCETHREAD_H

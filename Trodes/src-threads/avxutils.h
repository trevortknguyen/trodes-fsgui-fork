#ifndef AVXUTILS_H
#define AVXUTILS_H

#include <memory>
#include <iostream>
#include <QProcess>
//Utilities and defines for vectorized processors

static const int MAXPROCESSORSIZE=16;
static const int NUMCHANNELSPERAVXPROCESSOR = 128;
static const int VECMAXNUMPROCS = NUMCHANNELSPERAVXPROCESSOR/MAXPROCESSORSIZE;


//Takes a larger buffer of memory, and allocates 32-byte aligned memory along it.
//Design taken straight from the example at bottom of https://en.cppreference.com/w/cpp/memory/align
//********************************************************************************************************
//***When allocating the full_buffer, make sure to add at least 31 bytes to the total needed # of bytes***
//*** This ensures that there is a 32-byte aligned start address in the buffer allocated.
struct Aligned32Allocator
{
private:
    void* p;
    std::size_t sz;
public:
    Aligned32Allocator(void* full_buffer, size_t N) : p(full_buffer), sz(N) {}
    template <typename T>
    T* aligned_alloc(size_t num_elems)
    {
        if (std::align(32, sizeof(T)*num_elems, p, sz))
        {
            T* result = reinterpret_cast<T*>(p);
            p = (char*)p + sizeof(T)*num_elems;
            sz -= sizeof(T)*num_elems;
            return result;
        }
        return nullptr;
    }
};


static bool detectAVXCapabilities(){

    QProcess p;
#ifdef Q_OS_WIN
//https://superuser.com/questions/1251865/is-there-a-way-to-tell-if-my-hardware-supports-specific-instructions
    p.start("./Resources/SetupHelp/Windows/Coreinfo");
    if(p.waitForFinished()){
        if(p.exitCode()){
            return false;
        }
        else{
            QString output = p.readAll();
            return output.contains("Supports AVX intruction extensions", Qt::CaseInsensitive);
        }
    }
#endif

#ifdef Q_OS_LINUX
//https://stackoverflow.com/questions/37480071/how-to-tell-if-a-linux-machine-supports-avx-avx2-instructions
    p.start("grep", {"avx2", "/proc/cpuinfo"});
    if(p.waitForFinished()){
        if(p.exitCode()){
            //returned 1 or 2, didn't find avx2
            return false;
        }
        else{
            //returned 0, found avx2
            return true;
        }
    }
#endif

#ifdef Q_OS_MACOS
    //if (__builtin_cpu_supports("avx2")) {
    //    //this might work too?
    //}
//http://csharpmulticore.blogspot.com/2014/12/how-to-check-intel-avx2-support-on-mac-os-x-haswell.html
    p.start("bash", {"-c", "sysctl -a | grep AVX2",});
    if(p.waitForFinished()){
        if(p.exitCode()){
            //returned 1 or 2, didn't find AVX2
            return false;
        }
        else{
            //return 0, found AVX2
            return true;
        }
    }
#endif
    return false;
}

#endif // AVXUTILS_H

#include "thresholdprocessor.h"
#include <immintrin.h> // for AVX

ThresholdProcessor::ThresholdProcessor()
{

}

/*void ThresholdProcessor::newSamples(const float *data)
{
    __m256i ones = _mm256_set1_epi8(0xFF);

    //load, compare, store first 8 elements
    __m256 dps = _mm256_load_ps(data);
    __m256 thresh = _mm256_load_ps(thresholds);
    __m256i results = _mm256_castps_si256(_mm256_cmp_ps(dps, thresh, _CMP_GE_OQ));
    _mm256_store_si256((__m256i*)threshpassed, results);
    threshexists = _mm256_testz_si256(results, ones);

    //load, compare, store second 8 elements
    __m256 dps2 = _mm256_load_ps(data + 8);
    __m256 thresh2 = _mm256_load_ps(thresholds + 8);
    __m256i results2 = _mm256_castps_si256(_mm256_cmp_ps(dps2, thresh2, _CMP_GE_OQ));
    _mm256_store_si256((__m256i*)(threshpassed+8), results2);
    threshexists = threshexists | _mm256_testz_si256(results, ones);
}*/

void ThresholdProcessor::checkThreshCrossings(const float *data, const float *thresholds, int32_t *output)
{
    //__m256i ones = _mm256_set1_epi8(0xFF);
    //int32_t threshexists = 0;
    /*for (int i=0;i<16;i++) {
         if (data[i] >= thresholds[i]) {
             output[i] = 1;
         }
    }*/

    //load, compare, store first 8 elements
    __m256 dps = _mm256_load_ps(data);
    __m256 thresh = _mm256_load_ps(thresholds);
    __m256i results = _mm256_castps_si256(_mm256_cmp_ps(dps, thresh, _CMP_GE_OQ));
    _mm256_store_si256((__m256i*)output, results);
    //threshexists = _mm256_testz_si256(results, ones);

    //load, compare, store second 8 elements
    __m256 dps2 = _mm256_load_ps(data + 8);
    __m256 thresh2 = _mm256_load_ps(thresholds + 8);
    __m256i results2 = _mm256_castps_si256(_mm256_cmp_ps(dps2, thresh2, _CMP_GE_OQ));
    _mm256_store_si256((__m256i*)(output+8), results2);
    //threshexists = threshexists | _mm256_testz_si256(results, ones);

}

/*int ThresholdProcessor::setThreshold(int ind, float thresh)
{
    if(ind < 0 || ind > 15){
        //bad chan ind
        return -1;
    }
    thresholds[ind] = thresh;
    return 0;
}*/

#include "referenceprocessor.h"
#include "avxfloattoshort.h"
#include <immintrin.h> // for AVX






//ReferenceProcessor::ReferenceProcessor() :
//    x(),
//    refindices(),
//    refmasks(),
//    refs(),
//    refgroups()
//{

//}

//int ReferenceProcessor::setReference(int ind, int refchan, int refgroup){
//    //calculate and modify refindices and refmasks
////    int ind = ind - firsthwchan;
//    if(ind < 0 || ind > 15){
//        //bad chan ind
//        return -1;
//    }

//    //ind/8: 0-7 (0) or 8-15 (1)
//    //ind%8: map 0-15 to 0-7

//    if(refgroup >= 0){
//        //refgroup is on, so refchan is turned off
//        refindices[ind/8][ind%8] = 0;
//        refmasks[ind/8][ind%8] = 0x00000000 ;

//        refgroup_indices[ind/8][ind%8] = refgroup*sizeof(int16_t);
//        refgroup_masks[ind/8][ind%8] = 0x80000000;

//    }
//    else if(refchan >= 0){
//        //refgroup is off, refchan is on
//        refindices[ind/8][ind%8] = refchan*sizeof(int16_t);
//        refmasks[ind/8][ind%8] = 0x80000000 ;

//        refgroup_indices[ind/8][ind%8] = 0;
//        refgroup_masks[ind/8][ind%8] = 0x00000000;
//    }
//    else{
//        //none are on
//        refindices[ind/8][ind%8] = 0;
//        refmasks[ind/8][ind%8] = 0x00000000 ;

//        refgroup_indices[ind/8][ind%8] = 0;
//        refgroup_masks[ind/8][ind%8] = 0x00000000;
//    }

//    return 0;
//}

void ReferenceProcessor::applyReferences(const int16_t *data, const void *packetstart, const void *carvaluestart, float *aligned_output,
                                    const int32_t *refindices, const int32_t *refmasks, const int32_t *refgroup_indices, const int32_t *refgroup_masks){
    float *x = aligned_output;
    for(int i = 0; i < 2; ++i){
        //convert and load data into int32
        __m256i di32 = _mm256_cvtepi16_epi32(_mm_load_si128((__m128i*)(&data[i*8])));

        //check if NaN (-32768). if NaN, set to 0. else, keep value
        __m256i nancmp32 = _mm256_cmpeq_epi32(di32, _mm256_set1_epi32(-32768)); //compare. FFFFFFFF if equal, 0 else.
        di32 = _mm256_andnot_si256(nancmp32, di32); //~cmp32 & di32. 0's if num was equal to NaN.

        //The single channel reference is used if the ref checkbox is on AND the group ref is off.
        __m256i comRefMask = _mm256_andnot_si256(_mm256_load_si256((__m256i*)(&refgroup_masks[i*8])),_mm256_load_si256((__m256i*)(&refmasks[i*8])));
        //gather reference channel data from packet. subtract data if ref on, 0 if ref off.
        //since gather only exists for 32-bit integers, we gather 32-bits of data from the packet for each ref chan.
        //but the packet contains 16-bit integers, so we have to shift away and manually convert the 32-bit integers back to 16-bit values
        __m256i dref = _mm256_mask_i32gather_epi32(
                        _mm256_set1_epi32(0), (int*)packetstart,  // src = 0 array. packetstart = ptr to data packet cast to int*
                        _mm256_load_si256((__m256i*)(&refindices[i*8])), //refindices = byte offset from packet start of desired ref value
                        comRefMask, //refmasks = 1 if channel uses ref, 0 if channel refs 0
                        1); //scale all values by 1


        //shift away upper 16 bits, shift back in sign bits to "cast" to int16 number
        dref = _mm256_srai_epi32( _mm256_slli_epi32(dref, 16), 16);

       //The group ref is used if the ref checkbox is on and the group ref is on.
        __m256i comGroupRefMask = _mm256_and_si256(_mm256_load_si256((__m256i*)(&refgroup_masks[i*8])),_mm256_load_si256((__m256i*)(&refmasks[i*8])));
        //gather reference group data
        __m256i drefgroup = _mm256_mask_i32gather_epi32(
                    _mm256_set1_epi32(0), (int*)carvaluestart,  // src = 0 array. packetstart = ptr to data packet cast to int*
                    _mm256_load_si256((__m256i*)(&refgroup_indices[i*8])), //refindices = byte offset from packet start of desired ref value
                    comGroupRefMask, //refmasks = 1 if channel uses ref, 0 if channel refs 0
                    1); //scale all values by 1


        //shift away upper 16 bits, shift back in sign bits to "cast" to int16 number
        drefgroup = _mm256_srai_epi32( _mm256_slli_epi32(drefgroup, 16), 16);

        //subtract dref and drefgroup (data - refs - refgroups)
        //dref and drefgroup will never both be set for a single channel. one or both will be zero.
        di32 = _mm256_sub_epi32(_mm256_sub_epi32(di32, dref),drefgroup);


        //convert data to single precision
        __m256 dps = _mm256_cvtepi32_ps( di32 );

        //store data
        _mm256_store_ps(&x[i*8], dps);
    }
}

void ReferenceProcessor::gatherUnreferenced(const void *packetstart, const int32_t *indices, int16_t *output, int startIndex){
    float output_float[16];
    for(int i = 0; i < 2; ++i){

        __m256i di32 = _mm256_mask_i32gather_epi32(
                        _mm256_set1_epi32(0), (int*)packetstart,  // src = 0 array. packetstart = ptr to data packet cast to int*
                        _mm256_load_si256((__m256i*)(&indices[(i*8)+startIndex])),
                        _mm256_set1_epi32(0x80000000),
                        1); //scale all values by 1
        //shift away upper 16 bits, shift back in sign bits to "cast" to int16 number
        di32= _mm256_srai_epi32( _mm256_slli_epi32(di32, 16), 16);


        //check if NaN (-32768). if NaN, set to 0. else, keep value
        __m256i nancmp32 = _mm256_cmpeq_epi32(di32, _mm256_set1_epi32(-32768)); //compare. FFFFFFFF if equal, 0 else.
        di32 = _mm256_andnot_si256(nancmp32, di32); //~cmp32 & di32. 0's if num was equal to NaN.

        //convert data to single precision
        __m256 dps = _mm256_cvtepi32_ps( di32 );

        //store data float
        _mm256_storeu_ps(&output_float[i*8], dps);
    }

    AVXFloatToShort::convertToShort(output_float, output);
}

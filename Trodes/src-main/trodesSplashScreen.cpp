#include "trodesSplashScreen.h"

AnimatedIcon::AnimatedIcon(QWidget *parent) : QLabel(parent) {
    setAttribute(Qt::WA_NoSystemBackground); //this makes it so the icon's background doesn't overlap with the parent widgets background
    rotationInterval = 0;
    currentAngle = 0;
    rotating = false;
}

void AnimatedIcon::setImage(QString imgPath, QSize imgSize) {
    path = imgPath;
    imageSize = imgSize;

    QPixmap imgPix(path);
    if (imgPix.isNull()) {
        qDebug() << "Bad Path"; //if the image file could not be found and/or loaded
    }
    else {
        //map the img's pixel map onto this object
        this->setPixmap(imgPix.scaled(imageSize.width(),imageSize.height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
        this->setAlignment(Qt::AlignCenter);
    }
}

void AnimatedIcon::startRotating(int angle) {
    rotationInterval = angle;
    rotating = true;
    rotate(rotationInterval);
}

void AnimatedIcon::stopRotating() {

    rotating = false;
    currentAngle = 0;
    rotate(0);
}

void AnimatedIcon::paintEvent(QPaintEvent *ev) {
//    qDebug() << "Paint icon";

    if (rotating)
        rotate(rotationInterval);

    QLabel::paintEvent(ev);
}

void AnimatedIcon::rotate(int angle) {
    currentAngle = (currentAngle + angle)%360;

    QPixmap pix = QPixmap(path);
    QTransform transform;

    QTransform rotated = transform.rotate(currentAngle);
    QSize logoSize(150,150);

    QPixmap rotatedPix = QPixmap(pix.transformed(rotated));

    this->setPixmap(rotatedPix.scaled(imageSize.width(),imageSize.height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
}

LoadingScreen::LoadingScreen(QWidget *parent) : QFrame(parent) {
    newParent();
    setStyleSheet("background-color: rgba(128,128,128,75)");
    visible = false;
//    currentAngle = 0;

    //create animations
    QGraphicsOpacityEffect *loadingScreenFade = new QGraphicsOpacityEffect(this);
    this->setGraphicsEffect(loadingScreenFade);
    a_fade = new QPropertyAnimation(loadingScreenFade, "opacity");
//    connect(a_fade, SIGNAL(finished()), this, SLOT(f))
    a_fade->setDuration(1);
    a_fade->setStartValue(1);
    a_fade->setEndValue(1);
    a_fade->start();
    connect(a_fade, SIGNAL(finished()), this, SLOT(sendFadeCompleteSig()));


    //create layout
    QVBoxLayout *mainLayout = new QVBoxLayout();

    trodesIcon = new AnimatedIcon();
    trodesIcon->setObjectName("icon");
    QString logoPath = ":/trodesIcon.png";
    QSize logoSize(150,150);
    trodesIcon->setImage(logoPath, logoSize);

    mainLayout->addWidget(trodesIcon);

    setLayout(mainLayout);
    setVisible(visible);
}

void LoadingScreen::debug() {
//    qDebug() << "debug function -";
    if (visible) {
        fadeOut();
    }
    else
        fadeIn();
}

void LoadingScreen::fadeIn() {
    setVisible(true);
    raise();
    runAnimation_fade(1000, true);
    trodesIcon->startRotating(2);
}

void LoadingScreen::fadeOut() {
    runAnimation_fade(1000, false);
}

void LoadingScreen::runAnimation_fade(int time, bool fadeIn) {
    if (time <= 0)
        time = 1000;

    visible = fadeIn;
    a_fade->setDuration(time);
    if (fadeIn) {
        a_fade->setStartValue(0);
        a_fade->setEndValue(1);
    }
    else {
        a_fade->setStartValue(1);
        a_fade->setEndValue(0);
    }
    a_fade->start();
}

bool LoadingScreen::eventFilter(QObject *obj, QEvent *ev) {
    if (obj == parent()) {
        if (ev->type() == QEvent::Resize) {
            resize(static_cast<QResizeEvent*>(ev)->size());
        }
        else if (ev->type() == QEvent::ChildAdded) {
            raise();
        }
    }
    return(QFrame::eventFilter(obj, ev));
}

bool LoadingScreen::event(QEvent *ev) {
    if (ev->type() == QEvent::ParentAboutToChange) {
        if(parent())
            parent()->removeEventFilter(this);
    }
    else if (ev->type() == QEvent::ParentChange) {
        newParent();
    }
    return(QFrame::event(ev));
}

void LoadingScreen::sendFadeCompleteSig() {
    if (!visible) {
        this->setVisible(visible);
        trodesIcon->stopRotating();
    }
    emit sig_fadeAnimationComplete(visible);
}

EmbeddedButton::EmbeddedButton(const QString &text, int maxTextLen, QWidget *parent) : QLabel(parent) {
    maxWidth = maxTextLen;
    hyperLink = "";
    setButtonText(text);
    truncateText();
    //format the buttons to have a grey highlight when hovered over
    setStyleSheet(
               #if defined (__WIN32__) //for some reason the light grey windows color doesn't show up on linux hence this ugly if preprocessor statement
                   "EmbeddedButton::hover {background-color:#e7e7e7}"
               #elif defined (__APPLE__)
                   "EmbeddedButton::hover {background-color:#e7e7e7}"
               #else
                   "EmbeddedButton::hover {background-color:#C5C5C5}"
               #endif
                  "EmbeddedButton::pressed {background-color:#d4d4d4}"
                  );
}

//set and format the button's text
//NOTE: use this instead of setText()!!!
void EmbeddedButton::setButtonText(QString text) {
    unformatedText = text;
    //format the text so it is blue
    text = QString("<span style=color:blue;>%1</span>").arg(text);
    setText(text);
    setMinimumHeight(sizeHint().height());
}

void EmbeddedButton::setHyperLink(QString link) {
    hyperLink = link;
    setToolTip(hyperLink);
}

void EmbeddedButton::setMaxWidth(int width) {
    maxWidth = width;
    truncateText(); //when you set the max width, truncate the text automatically
}

//Truncate the text label to fit the specified maxWidth
void EmbeddedButton::truncateText() {
    if (maxWidth < 0)
        return;
    QString str = unformatedText;
    //subtract one character from the end of the string until it fits within the maxWidth
    //WARNING: this loop is not safe (unaccounted for conditions: maxWidth < 3 will never finish
    while (sizeHint().width() > maxWidth) {
        str = str.left(str.length()-1);
        setButtonText(QString("%1...").arg(str));
    }
}

bool EmbeddedButton::isTextEmpty() {
    if (text() == "<span style=color:blue;></span>" || text() == "") {
        return(true);
    }
    else
        return(false);
}

void EmbeddedButton::setVisible(bool visible) {
//    if (isTextEmpty()) {
//        QLabel::setVisible(false);
//        return;
//    }
    QLabel::setVisible(visible);
}

void EmbeddedButton::mousePressEvent(QMouseEvent *event) {
    emit sig_clicked();
    emit sig_sendLink(hyperLink);
}

void EmbeddedButton::mouseReleaseEvent(QMouseEvent *event) {
}

ExpandableFrame::ExpandableFrame(bool nonExpandable, QWidget *parent) : QFrame(parent) {
    isStaticFrame = nonExpandable;
    expanded = false;
    baseHeight = 0;
    baseWidth = 0;
    maxHeight = 0;
    maxWidth = 0;

    defaultColor = "";
    hoverColor = "";

    //initialize frame animations
    a_geometry = new QPropertyAnimation(this, "geometry");
    connect(a_geometry,SIGNAL(finished()),this,SLOT(resizeAnimationFinished()));
    a_minSize = new QPropertyAnimation(this, "minimumSize");
    a_maxSize = new QPropertyAnimation(this, "maximumSize");

    a_borderColor = new QPropertyAnimation(this, "borderColor");
    a_backgroundColor = new QPropertyAnimation(this, "backgroundColor");
    connect(a_backgroundColor,SIGNAL(finished()),this,SLOT(backgroundColorAnimationFinished()));

    mainLayout = new QVBoxLayout();
    setLayout(mainLayout);
}

//Adds a widget to the ExpandableFrame
void ExpandableFrame::addWidget(QWidget *widget, int spacing, Qt::Alignment alignment) {
    mainLayout->addWidget(widget, spacing, alignment);
    if (widgets.length() == 0) { //for the first widget
        //set the base and max height/widths with the proper spacing
        baseHeight = widget->sizeHint().height() + SS_EF_BASE_VERTICAL_SPACING;
        baseWidth = widget->sizeHint().width() + SS_EF_BASE_HORIZONTAL_SPACING;
        maxHeight = baseHeight;
        maxWidth = baseWidth;
        setMaximumHeight(baseHeight);
        setMaximumWidth(baseWidth);
        setMinimumHeight(baseHeight);
        setMinimumWidth(baseWidth);
    }
    else if (widgets.length() >0) { //for all other widgets after the firstW
        maxHeight += widget->sizeHint().height()+(widget->contentsMargins().bottom());
        if (maxWidth < (widget->sizeHint().width()+SS_EF_HORIZONTAL_SPACING)) { //if we need to set a new maxHeight
            maxWidth = widget->sizeHint().width()+SS_EF_HORIZONTAL_SPACING;
        }
        widget->setVisible(false);
    }
    verifyWidth(); //this function makes sure that baseWidth and maxWidth don't exceed SS_EF_ABSOLUTE_MAX_WIDTH
    widgets.append(widget); //keep track of all the frame's added widgets

}

void ExpandableFrame::setExpanded(bool expand) {
    expanded = expand;
    //put in expand animation call
    if (widgets.length() > 0) {
        if (expanded) { //if expanding the frame
            //make sure the frame actually can expand to the max width/height
            setMaximumWidth(maxWidth);
            setMaximumHeight(maxHeight);

            //resize minimum size geometry (this resizes the widget IN the layout)
            a_minSize->setDuration(250);
            a_minSize->setStartValue(QSize(baseWidth,baseHeight));
            a_minSize->setEndValue(QSize(maxWidth,maxHeight));
            a_minSize->start();

            //resize geometry animation (this resizes the widget outside the layout)
            a_geometry->setDuration(250);
            a_geometry->setStartValue(QRect(this->pos().x(), this->pos().y(), baseWidth, baseHeight ));
            a_geometry->setEndValue(QRect(this->pos().x(), this->pos().y(), maxWidth, maxHeight ));
            a_geometry->start();

            if (ANIMATED_BORDER_FILL) { //border/backgound color animation transitions
                backgroundColorAnimation(250, QColor(hoverColor), QColor("white"));
                borderColorAnimation(250,QColor("#d0d0d0"),QColor(hoverColor));
            }
            emit sig_frameExpanding(this);
        }
        else { //else, hiding the frame...
            setWidgetsVisible(false); //turn off all of the frame's interior widgets
            setMinimumWidth(maxWidth);
            setMinimumHeight(maxHeight);

            a_minSize->setDuration(250);
            a_minSize->setStartValue(QSize(maxWidth,maxHeight));
            a_minSize->setEndValue(QSize(baseWidth,baseHeight));
            a_minSize->start();

            a_geometry->setDuration(250);
            a_geometry->setStartValue(QRect(this->pos().x(), this->pos().y(), maxWidth, maxHeight ));
            a_geometry->setEndValue(QRect(this->pos().x(), this->pos().y(), baseWidth, baseHeight ));
            a_geometry->start();

            if (ANIMATED_BORDER_FILL) {
                if (underMouse() || CLOSE_FRAME_FULL_ANIMATION) //only animate color if the mouse is hovering over the frame or if the CLOSE_FRAME_FULL_ANIMATION flag is set
                    backgroundColorAnimation(250, QColor("white"), QColor(hoverColor));
                borderColorAnimation(250,QColor(hoverColor),QColor("#d0d0d0"));
            }
        }
    }
}

//function resizes the frame based on the sizes of the interior widgets
void ExpandableFrame::resizeFrame(void) {
    int width = 0;
    int height = 0;

    for (int i = 0; i < widgets.length(); i++) {
        QWidget *curWidget = widgets.at(i);
        if (i == 0) {//special case for the first widget
            width = curWidget->sizeHint().width() + SS_EF_BASE_HORIZONTAL_SPACING;
            height = curWidget->sizeHint().height() + SS_EF_BASE_VERTICAL_SPACING;

        }
        else {
            if (width < (curWidget->sizeHint().width() + SS_EF_HORIZONTAL_SPACING)) {
                width = curWidget->sizeHint().width() + SS_EF_HORIZONTAL_SPACING;
            }

            height += curWidget->sizeHint().height() + (curWidget->contentsMargins().bottom());
        }
    }

    maxWidth = width;
    maxHeight = height;
    //MAYBE: put in setMax/setMin if the frame is visible...
    verifyWidth();
}

//function to exectue a border color change over the given duration (ms)
void ExpandableFrame::borderColorAnimation(int duration, QColor startColor, QColor endColor) {
    a_borderColor->setDuration(duration);
    a_borderColor->setStartValue(startColor);
    a_borderColor->setEndValue(endColor);
    a_borderColor->start();
}

//function to exectue a background color change over the given duration (ms)
void ExpandableFrame::backgroundColorAnimation(int duration, QColor startColor, QColor endColor) {
    a_backgroundColor->setDuration(duration);
    a_backgroundColor->setStartValue(startColor);
    a_backgroundColor->setEndValue(endColor);
    a_backgroundColor->start();
}

//fucntion sets the frame's border color to the specified color
void ExpandableFrame::setBorderColor(QColor color) {
    QString rgbStr = QString("rgb(%1,%2,%3)").arg(color.red()).arg(color.green()).arg(color.blue());
    replaceStyleSheetPropertyValue("border-color",rgbStr);
}

//function sets the frame's background color to the specified color
void ExpandableFrame::setBackgroundColor(QColor color) {
    QString rgbStr = QString("rgb(%1,%2,%3)").arg(color.red()).arg(color.green()).arg(color.blue());
    replaceStyleSheetPropertyValue("background-color", rgbStr);
    curBackgroundColor = color; //keep track of the current background color
}

//FUNCTION DEPRECIATED BY setBackgroundColor(QColor color)
void ExpandableFrame::setBackgroundColor(QString color) {
    replaceStyleSheetPropertyValue("background-color", color);
    curBackgroundColor = QColor(color);
}

//NOTE: when using this function you can not use any stylesheet settings with multiple values delimited by whitespaces.
void ExpandableFrame::replaceStyleSheetPropertyValue(QString property, QString newValue) {
    //parse the stylesheet by splitting it along the delimeters { OR }
    //QRegExp delimiters("(\\{|\\})");
    QRegularExpression delimiters("(\\{|\\})");
    QStringList sheetL = styleSheet().split(delimiters);
    QString newStyleSheet = "";
    for (int i = 0; i < sheetL.length(); i++) {
        QString curStr = sheetL.at(i);
        if (i == 0)
            newStyleSheet = curStr;
        else if (i%2 != 0) { //if an odd element, i.e. stylesheet string inside of {}
            //if the curStr contains the string 'property' then we need to replace the current value with our new one
            if (curStr.contains(property)) {
//                qDebug() << "   change: " << curStr;
                //parse the curStr by splitting it along the delimeters : OR ;
                QRegularExpression delim("(\\:|\\;)");
                QStringList curStrL = curStr.split(delim);
                bool inputValue = false;
                QString newCurStr = "";
                //replace all instances of the old property value with the new value
                for (int j = 0; j < curStrL.length(); j++) {
                    QString strElement = curStrL.at(j);
                    strElement.replace(" ",""); //this line is why we can't have multiple values assigned to attributes separated by a space
//                    qDebug() << strElement;
                    if (j == 0) {
                        newCurStr = strElement;
                    }
                    else if (j%2 != 0) {
                        if (inputValue) { //if the previous str element was 'property' we know that we should input the new value here
                            newCurStr = QString("%1:%2").arg(newCurStr).arg(newValue);
                            inputValue = false;
                        }
                        else {
                            newCurStr = QString("%1:%2").arg(newCurStr).arg(strElement);
                        }
                    }
                    else {
                        newCurStr = QString("%1;%2").arg(newCurStr).arg(strElement);
                    }
                    //if the current str element is "property" flip bool 'inputValue' to signify that we are going to input the new value in the next loop interation
                    if (strElement == property) {
                        inputValue = true;
                    }
                    else
                        inputValue = false;

                }
                curStr = newCurStr;
//                qDebug() << "   now: " << newCurStr;

            }
            newStyleSheet = QString("%1{%2}").arg(newStyleSheet).arg(curStr);
        }
        else { //even stylesheet elements, elements outside of the brackets
            newStyleSheet = QString("%1%2").arg(newStyleSheet).arg(curStr);
        }
    }
    setStyleSheet(newStyleSheet); //set the new styleSheet
}

//function to manually set the size of an expandable frame.  Ostensibly used to normalize the
//base size of the ExpandableFrames so they all match each other in the Splash Screen constructor
void ExpandableFrame::setSize(QSize size) {
    baseWidth = size.width();
    baseHeight = size.height();
    setMinimumWidth(baseWidth);
    setMinimumHeight(baseHeight);
    if (maxWidth < baseWidth) { // we don't want the base to exceed the base...
        maxWidth = baseWidth;
        setMaximumWidth(baseWidth);
    }

    if (maxHeight < baseHeight) {
        maxHeight = baseHeight;
        setMaximumHeight(baseHeight);
    }
    if (verifyWidth()) {
        qDebug() << "Error: Specified width exceeds allowable maximum value. (ExpandableFrame::setSize)";
    }
}

bool ExpandableFrame::isWidgetInFrame(QWidget *ptr) {
    for (int i = 0; i < widgets.length(); i++) {
        if (widgets.at(i) == ptr) {
//            qDebug() << "found you";
            return(true);
        }
    }
    return(false);
}

void ExpandableFrame::mousePressEvent(QMouseEvent *event) {
    if (!isStaticFrame)
        setExpanded(!expanded);
    emit sig_clicked();
}

void ExpandableFrame::enterEvent(QEnterEvent *event) {

    if (!expanded)
        if (hoverColor != "") {
            if (defaultColor != "") {
                backgroundColorAnimation(250, QColor(defaultColor), QColor(hoverColor));
            }
            else
                backgroundColorAnimation(250, QColor("white"), QColor(hoverColor));
        }
}

void ExpandableFrame::leaveEvent(QEvent *event) {

    if (!expanded) { //if expanded, keep the hover color, otherwise, go back to default
        if (defaultColor != "") {
            backgroundColorAnimation(250, QColor(hoverColor), QColor(defaultColor));
        }
        else //if defaultColor is not set, just set the background to white
            backgroundColorAnimation(250, QColor(hoverColor), QColor("white"));
    }
}

//This function makes sure that frames cannot be stretched horizontally past a defined maximum
bool ExpandableFrame::verifyWidth(void) {
    bool retVal = false;
    if (baseWidth > SS_EF_ABSOLUTE_MAX_WIDTH) {
        baseWidth = SS_EF_ABSOLUTE_MAX_WIDTH;
        setMinimumWidth(baseWidth);
        retVal = true;
    }
    if (maxWidth > SS_EF_ABSOLUTE_MAX_WIDTH) {
        maxWidth = SS_EF_ABSOLUTE_MAX_WIDTH;
        setMaximumWidth(maxWidth);
        retVal = true;
    }
    return(retVal);
}

void ExpandableFrame::resizeAnimationFinished() {
    setWidgetsVisible(expanded);
    if (!expanded) { //if the frame shrunk, make sure the max height/width values are properly adjusted
        setMaximumHeight(baseHeight);
        setMaximumWidth(baseWidth);
    }
}

void ExpandableFrame::backgroundColorAnimationFinished() {
    if (!underMouse() && CLOSE_FRAME_FULL_ANIMATION) { //only execute if the mouse is not hoving over the frame
        if (curBackgroundColor.name() == hoverColor) { //if the current background color needs to be faded out
            backgroundColorAnimation(250, QColor(hoverColor), QColor("white"));
        }
    }
}

void ExpandableFrame::setWidgetsVisible(bool visible) {
    //set all widgets but the top one to 'visible' because the top widget is the expandable frame's label
    for (int i = 1; i < widgets.length(); i++) {
        widgets.at(i)->setVisible(visible);
    }
}


TrodesSplashScreen::TrodesSplashScreen(QWidget *parent) : QGroupBox(parent)
{
    setAttribute(Qt::WA_NoSystemBackground); //tells Qt to not set a default background
    newParent(); //set the spash screen to recieve and process resizing events from its' parent widget
    setCanLoadFile(true);

    //give the spash screen a white background
    setStyleSheet("QGroupBox {background-color:white;}");

    //*** initialize the Trodes logo ***
    isFadedOut = false;
    flameOn = false; //for fun, turn on to get a cool flaming label
    mainLayout = new QVBoxLayout();

    trodesLogoLayout = new QHBoxLayout();
    trodesLogo = new QLabel();
    //trodesLogo->setScaledContents(true);
    //load the logo img file

    QString logoPath = ":/trodesIcon.png";
    QPixmap trodesPix(logoPath);
    if (trodesPix.isNull()) {
        qDebug() << "Bad Path"; //if the image file could not be found and/or loaded
    }
    else {
        QSize logoSize(75,75);
        trodesLogo->setMaximumWidth(logoSize.width());
        trodesLogo->setMaximumHeight(logoSize.height());
        //map the logo's pixel map onto the trodesLogo QLabel
        trodesLogo->setPixmap(trodesPix.scaled(logoSize.width(),logoSize.height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));
//        trodesLogo->setMask(trodesPix.mask());
    }
    //retrieve the trodes version information from the GlobalConfiguration
    QString trodesVersion = GlobalConfiguration::getVersionInfo(false).split("\n").first();

    //Mark: Flaming Logo, just for fun, delete later
    if (flameOn) {
        QString flamingPath = QString("%1/trodesLogoGif.gif").arg(QCoreApplication::applicationDirPath());
        labelLogoText = new QLabel();
        flamingGif = new QMovie(flamingPath);
        labelLogoText->setMovie(flamingGif);
        trodesLogoLayout->addWidget(trodesLogo, 3, Qt::AlignRight);
        trodesLogoLayout->addWidget(labelLogoText, 5, Qt::AlignLeft);
    }
    else {
        //Actual Logo, don't delete
        QLabel *horizontalSpacer = new QLabel(""); //create a horizontal spacer to push the logo into the center of the screen
        horizontalSpacer->setAlignment(Qt::AlignLeft);
        int logoSpacerWidth;
#if defined (__WIN32__) //Again, linux doesn't like to cooperate with windows formating so we have to use a preprocessor if statement to set sys specific dimensions
        logoSpacerWidth = 100;
#elif defined (__APPLE__)
        logoSpacerWidth = 85;
#else
        logoSpacerWidth = 150;
#endif
        horizontalSpacer->setMinimumWidth(logoSpacerWidth);
        horizontalSpacer->setMaximumWidth(logoSpacerWidth);

        QFont font("Calibri Light", 45);
        font.setPointSize(45);
        labelLogoText = new QLabel("Trodes");
        labelLogoText->setFont(font);
        trodesLogoLayout->addWidget(trodesLogo, 0, Qt::AlignRight);
        trodesLogoLayout->addWidget(labelLogoText, 0, Qt::AlignLeft);
        trodesLogoLayout->addWidget(horizontalSpacer);
    }
    QLabel *topSpacer = new QLabel("");
    mainLayout->addWidget(topSpacer, 1); //put a spacer on top of the logo so it's more centered in the screen
    mainLayout->addLayout(trodesLogoLayout, 1);
//    fonts.append(QString("Calibri Light"));
//    fonts.append(QString("Gentium Basic"));
//    fonts.append(QString("Leelawadee UI"));
//    fonts.append(QString("Leelawadee UI Semilight"));
//    fonts.append(QString("Microsoft Tai Le"));
//    fonts.append(QString("Microsoft Tai Le Light"));
//    fonts.append(QString("Yu Gothic"));

    //*** Expanding Frame Layout ***
    verticalButtonLayout = new QVBoxLayout();
    //create bottom spacer, this will be added onto the bottom of the layout to push the expanding frames to the top of the layout
    QLabel *spacerLabel_1 = new QLabel("");
    QSize minFrameSize;
    QFont f;

        //  *** General Info Frame
    //Note for Frame initialization: Each frame will be added to the 'expandingFrames' QList to provide easy
    //access to all Frames
    infoFrame = new ExpandableFrame();
    infoFrame->setObjectName("infoFrame");
    //NOTE for all frames: these if statements contain separate graphics initializations for frames.  See the
    //header file defines for UI controls.
    if (JUST_BORDERS)
        infoFrame->setStyleSheet("#infoFrame {border: 2px solid; border-radius: 5px;} "
                                 "#infoFrame::hover {border: 2px solid #ff4646; border-radius: 5px;} "
                                 "QFrame {background-color:white;} "
                                 "QLabel {background-color:white;}"
                                 );
    else if (UI_FILL) {
        infoFrame->setStyleSheet("#infoFrame {border: 2px solid; border-radius: 5px;} "
                                 "QFrame {background-color:white;} "
                                 "QLabel {background-color:white;}"
                                 );
        infoFrame->setHoverColor("#ff4646");
    }
    else if (ANIMATED_BORDER_FILL) {
        infoFrame->setStyleSheet("#infoFrame {border: 2px; border-style: solid; border-radius: 5px; border-color:#d0d0d0;} "
                                 "QFrame {background-color:white;} "
                                 "QLabel {background-color:white;}"
                                 );
        infoFrame->setHoverColor("#ff4646");

    }

    labelInfoFrameTitle = new QLabel("Information");
    f = labelInfoFrameTitle->font();
    f.setFamily(SS_EF_FRAME_LABEL_FONT);
    f.setPointSize(SS_EF_FRAME_LABEL_FONT_SIZE);
    labelInfoFrameTitle->setFont(f);

    infoFrame->addWidget(labelInfoFrameTitle, 1, Qt::AlignCenter | Qt::AlignTop);
    //start tracking the minimum frame size across all frames
    minFrameSize = infoFrame->getBaseSize();


    //add in version information
    versionFrame = new QFrame;
    QHBoxLayout *versionLayout = new QHBoxLayout;
    /*labelWarningIcon = new QLabel;
    labelWarningIcon->setContentsMargins(0,0,0,0);
    labelWarningIcon->setAlignment(Qt::AlignCenter);
    versionLayout->addWidget(labelWarningIcon);*/

    labelInfoFrameTrodesVersion = new QLabel(GlobalConfiguration::getVersionInfo(false).split("\n").first());
    labelInfoFrameTrodesVersion->setAlignment(Qt::AlignLeft);
    f = labelInfoFrameTrodesVersion->font();
    f.setFamily("Calibri Light");
    f.setPointSize(SS_VERSION_INFO_FONT_SIZE);
    labelInfoFrameTrodesVersion->setFont(f);
//    infoFrame->addWidget(labelInfoFrameTrodesVersion);
    labelInfoFrameTrodesVersion->setContentsMargins(0,0,0,0);
    labelInfoFrameTrodesVersion->setAlignment(Qt::AlignCenter);
    versionLayout->addWidget(labelInfoFrameTrodesVersion);

    versionFrame->setLayout(versionLayout);
    versionFrame->setContentsMargins(0,SS_VERTICAL_SPACING,0,SS_VERTICAL_SPACING);
    infoFrame->addWidget(versionFrame);

    labelGeneralInfo = new QLabel();
    //General info text displayed when opening the 'information' frame
    labelGeneralInfo->setText("<a href=\"https://docs.spikegadgets.com/en/latest/\">Online Trodes Manual</a>");
    labelGeneralInfo->setTextFormat(Qt::RichText);
    labelGeneralInfo->setTextInteractionFlags(Qt::TextBrowserInteraction);
    labelGeneralInfo->setOpenExternalLinks(true);
    /*labelGeneralInfo->setText(
                "Welcome to Trodes!\n"
                "To get started using Trodes, you must first either create or load\n"
                "a workspace (.trodesconf file). Alternatively, if you want to re-\n"
                "view a previously recorded session, you can load a .rec file using\n"
                "the dialog option below.\n"
                );*/
    labelGeneralInfo->setWordWrap(true);
    f = labelGeneralInfo->font();
    f.setFamily("Calibri Light");
    f.setPointSize(SS_INFO_TEXT_FONT_SIZE);
    labelGeneralInfo->setFont(f);
    labelGeneralInfo->setAlignment(Qt::AlignCenter | Qt::AlignTop);
    labelGeneralInfo->setContentsMargins(0,SS_VERTICAL_SPACING+10,0,SS_VERTICAL_SPACING+10);
    infoFrame->addWidget(labelGeneralInfo);

        //  *** Create Workspace Frame
    createWorkspaceFrame = new ExpandableFrame();
    createWorkspaceFrame->setObjectName("createWorkspaceFrame");
    if (JUST_BORDERS) {
        createWorkspaceFrame->setStyleSheet("#createWorkspaceFrame {border: 2px solid; border-radius: 5px;}"
                                            "#createWorkspaceFrame::hover {border: 2px solid #ffff00; border-radius: 5px;}"
                                            "QFrame {background-color:white;}"
                                            "QLabel {background-color:white;}"
                                            );
    }
    else if (UI_FILL) {
        createWorkspaceFrame->setStyleSheet("#createWorkspaceFrame {border: 2px solid; border-radius: 5px;}"
                                            "QFrame {background-color:white;}"
                                            "QLabel {background-color:white;}"
                                            );
        createWorkspaceFrame->setHoverColor("#ffff00");
    }
    else if (ANIMATED_BORDER_FILL) {
        createWorkspaceFrame->setStyleSheet("#createWorkspaceFrame {border: 2px; border-style: solid; border-radius: 5px; border-color:#d0d0d0;}"
                                            "QFrame {background-color:white;}"
                                            "QLabel {background-color:white;}"
                                            );
        createWorkspaceFrame->setHoverColor("#ffff00");
    }

    labelCreateWorkspaceSelection = new QLabel("Create/Edit Workspace");
    f = labelCreateWorkspaceSelection->font();
    f.setFamily(SS_EF_FRAME_LABEL_FONT);
    f.setPointSize(SS_EF_FRAME_LABEL_FONT_SIZE);
    labelCreateWorkspaceSelection->setFont(f);
    createWorkspaceFrame->addWidget(labelCreateWorkspaceSelection, 1, Qt::AlignCenter | Qt::AlignTop);
//    connect(createWorkspaceFrame,SIGNAL(sig_clicked()),this,SLOT(buttonCreateNewPressed()));
    //check if the minimum frame size needs to be adjusted
    if (createWorkspaceFrame->getBaseSize().width() > minFrameSize.width()) {
        minFrameSize = createWorkspaceFrame->getBaseSize();
    }

    buttonCreateNewScratch = new EmbeddedButton("From Scratch");
    connect(buttonCreateNewScratch, SIGNAL(sig_clicked()), this, SLOT(buttonCreateNewPressed()));
    buttonCreateNewScratch->setAlignment(Qt::AlignCenter | Qt::AlignTop);
    buttonCreateNewScratch->setContentsMargins(0,SS_VERTICAL_SPACING,0,SS_VERTICAL_SPACING);
    createWorkspaceFrame->addWidget(buttonCreateNewScratch);

    if (UI_TEMPLATE_LIST) {
        labelTemplateWorkpaces = new QLabel("Edit Existing File");
        f = labelTemplateWorkpaces->font();
        f.setFamily(SS_EF_FRAME_LABEL_FONT);
        f.setPointSize(SS_EF_FRAME_LABEL_FONT_SIZE);
        labelTemplateWorkpaces->setFont(f);

        labelTemplateWorkpaces->setAlignment(Qt::AlignCenter | Qt::AlignTop);
        labelTemplateWorkpaces->setContentsMargins(0,SS_VERTICAL_SPACING,0,SS_VERTICAL_SPACING);
        createWorkspaceFrame->addWidget(labelTemplateWorkpaces);

        //add in template embedded buttons
        for (int i = 0; i < 5; i++) {
            EmbeddedButton *newLabel = new EmbeddedButton(""); //initialize text as "", we will set this later with the function setTemplateLabelsAndLinks()
            newLabel->setAlignment(Qt::AlignCenter | Qt::AlignTop);
            newLabel->setContentsMargins(0,SS_VERTICAL_SPACING,0,SS_VERTICAL_SPACING); //make sure to add spacing buffers between each button
            connect(newLabel,SIGNAL(sig_sendLink(QString)),this,SIGNAL(sig_loadTemplateWorkspace(QString)));
            templateLabels.append(newLabel);
        }
        setTemplateLabelsAndLinks();
    }
    else if (UI_TEMPLATE_BROWSE) {
        buttonUseTemplate = new EmbeddedButton("Edit Existing File...");
        buttonUseTemplate->setAlignment(Qt::AlignCenter | Qt::AlignTop);
        buttonUseTemplate->setContentsMargins(0,SS_VERTICAL_SPACING,0,SS_VERTICAL_SPACING);
        connect(buttonUseTemplate,SIGNAL(sig_clicked()),this, SLOT(chooseTemplateFile())); //SIGNAL(sig_loadTemplateWorkspace(QString)));
        createWorkspaceFrame->addWidget(buttonUseTemplate);

        QLabel *recentworkspacelabel = new QLabel("Recently opened...");
        f = recentworkspacelabel->font();
        f.setFamily(SS_EF_FRAME_LABEL_FONT);
        f.setPointSize(SS_EF_FRAME_LABEL_FONT_SIZE);
        recentworkspacelabel->setFont(f);

        recentworkspacelabel->setAlignment(Qt::AlignCenter | Qt::AlignTop);
        recentworkspacelabel->setContentsMargins(0,SS_VERTICAL_SPACING,0,SS_VERTICAL_SPACING);
        createWorkspaceFrame->addWidget(recentworkspacelabel);

        //add in template embedded buttons
        for (int i = 0; i < SPLASH_SCREEN_NUM_REMEMBERED_FILES; i++) {
            EmbeddedButton *newLabel = new EmbeddedButton(""); //initialize text as "", we will set this later with the function setTemplateLabelsAndLinks()
            newLabel->setAlignment(Qt::AlignCenter | Qt::AlignTop);
            newLabel->setContentsMargins(0,SS_VERTICAL_SPACING,0,SS_VERTICAL_SPACING); //make sure to add spacing buffers between each button
            connect(newLabel,SIGNAL(sig_sendLink(QString)),this,SIGNAL(sig_loadTemplateWorkspace(QString)));
            templateLabels.append(newLabel);
        }
        setTemplateLabelsAndLinks();
    }

        //  *** Open Workspace Frame
    workspaceFrame = new ExpandableFrame();    
    workspaceFrame->setObjectName("workspaceFrame");
    if (JUST_BORDERS) {
        workspaceFrame->setStyleSheet("#workspaceFrame {border: 2px solid; border-radius: 5px;}"
                                      "#workspaceFrame::hover {border: 2px solid #24c600; border-radius: 5px;}"
                                      "QFrame {background-color:white;}"
                                      "QLabel {background-color:white;}"
                                      "EmbeddedButton {background-color:white;}"
                                      );
    }
    else if (UI_FILL) {
        workspaceFrame->setStyleSheet("#workspaceFrame {border: 2px solid; border-radius: 5px;}"
                                      "QFrame {background-color:white;}"
                                      "QLabel {background-color:white;}"
                                      "EmbeddedButton {background-color:white;}"
                                      );
        workspaceFrame->setHoverColor("#24c600");
    }
    else if (ANIMATED_BORDER_FILL) {
        workspaceFrame->setStyleSheet("#workspaceFrame {border: 2px; border-style: solid; border-radius: 5px; border-color:#d0d0d0;}"
                                      "QFrame {background-color:white;}"
                                      "QLabel {background-color:white;}"
                                      "EmbeddedButton {background-color:white;}"
                                      );
        workspaceFrame->setHoverColor("#24c600");

    }

    //#0c96ff   #03b4ff
    labelWorkspaceSelection = new QLabel("Open Workspace");
    f = labelWorkspaceSelection->font();
    f.setFamily(SS_EF_FRAME_LABEL_FONT);
    f.setPointSize(SS_EF_FRAME_LABEL_FONT_SIZE);
    labelWorkspaceSelection->setFont(f);

    /*QSize spacerSize = */labelWorkspaceSelection->sizeHint(); //returned value not being used
    labelWorkspaceSelection->setAlignment(Qt::AlignCenter | Qt::AlignTop);
    workspaceFrame->addWidget(labelWorkspaceSelection, 1, Qt::AlignCenter | Qt::AlignTop);
    //check if the minimum frame size needs to be adjusted
    if (workspaceFrame->getBaseSize().width() > minFrameSize.width()) {
        minFrameSize = workspaceFrame->getBaseSize();
    }

    buttonLoad = new EmbeddedButton("Browse...");
    connect(buttonLoad,SIGNAL(sig_clicked()),this,SLOT(buttonLoadPressed()));
    buttonLoad->setAlignment(Qt::AlignCenter | Qt::AlignTop);
    buttonLoad->setContentsMargins(0,SS_VERTICAL_SPACING,0,SS_VERTICAL_SPACING);
    workspaceFrame->addWidget(buttonLoad);

    labelPrevWorkspaceSelection = new QLabel("Recently opened...");
    f = labelPrevWorkspaceSelection->font();
    f.setFamily(SS_EF_FRAME_LABEL_FONT);
    f.setPointSize(SS_EF_FRAME_LABEL_FONT_SIZE);
    labelPrevWorkspaceSelection->setFont(f);

    labelPrevWorkspaceSelection->setAlignment(Qt::AlignCenter | Qt::AlignTop);
    labelPrevWorkspaceSelection->setContentsMargins(0,SS_VERTICAL_SPACING,0,SS_VERTICAL_SPACING);
    workspaceFrame->addWidget(labelPrevWorkspaceSelection);

    //create embedded buttons for each file we want to be able to open
    for (int i = 0; i < SPLASH_SCREEN_NUM_REMEMBERED_FILES; i++) {
        EmbeddedButton *newLabel = new EmbeddedButton(""); //initialize text as "", we will set this later with the function setFileLabelsAndLinks()
        newLabel->setAlignment(Qt::AlignCenter | Qt::AlignTop);
        newLabel->setContentsMargins(0,SS_VERTICAL_SPACING,0,SS_VERTICAL_SPACING); //make sure to add spacing buffers between each button
//        connect(newLabel,SIGNAL(sig_sendLink(QString)),this,SIGNAL(sig_loadWorkspace(QString)));
        connect(newLabel,SIGNAL(sig_sendLink(QString)),this,SLOT(loadWorkspace(QString)));
        workspaceLabels.append(newLabel);
    }

        //  *** Open Playback File Frame
    playbackFrame = new ExpandableFrame();
    //#10cf80   #28b951
    playbackFrame->setObjectName("playbackFrame");
    if (JUST_BORDERS) {
        playbackFrame->setStyleSheet("#playbackFrame {border: 2px solid; border-radius: 5px}"
                                     "#playbackFrame::hover {border: 2px solid #55bdf4; border-radius: 5px}"
                                     "QFrame {background-color:white;}"
                                     "QLabel {background-color:white;}"
                                     );
    }
    else if (UI_FILL) {
        playbackFrame->setStyleSheet("#playbackFrame {border: 2px solid; border-radius: 5px}"
                                     "QFrame {background-color:white;}"
                                     "QLabel {background-color:white;}"
                                     );
        playbackFrame->setHoverColor("#55bdf4");
    }
    else if (ANIMATED_BORDER_FILL) {
        playbackFrame->setStyleSheet("#playbackFrame {border: 2px; border-style: solid; border-radius: 5px; border-color:#d0d0d0;}"
                                     "QFrame {background-color:white;}"
                                     "QLabel {background-color:white;}"
                                     );
        playbackFrame->setHoverColor("#55bdf4");
    }

    labelPlaybackFileSelection = new QLabel(tr("Open Playback File"));
    f = labelPlaybackFileSelection->font();
    f.setFamily(SS_EF_FRAME_LABEL_FONT);
    f.setPointSize(SS_EF_FRAME_LABEL_FONT_SIZE);
    labelPlaybackFileSelection->setFont(f);
    playbackFrame->addWidget(labelPlaybackFileSelection, 1, Qt::AlignCenter | Qt::AlignTop);
    //check if the minimum frame size needs to be adjusted
    if (playbackFrame->getBaseSize().width() > minFrameSize.width()) {
        minFrameSize = playbackFrame->getBaseSize();
    }

    buttonPlayback = new EmbeddedButton("Browse...");
    connect(buttonPlayback,SIGNAL(sig_clicked()),this,SLOT(buttonPlaybackPressed()));
    buttonPlayback->setAlignment(Qt::AlignCenter | Qt::AlignTop);
    buttonPlayback->setContentsMargins(0,SS_VERTICAL_SPACING,0,SS_VERTICAL_SPACING);
    playbackFrame->addWidget(buttonPlayback);

    labelPrevPlaybackFileSelection = new QLabel("Recently opened...");
    f = labelPrevPlaybackFileSelection->font();
    f.setFamily(SS_EF_FRAME_LABEL_FONT);
    f.setPointSize(SS_EF_FRAME_LABEL_FONT_SIZE);
    labelPrevPlaybackFileSelection->setFont(f);

    labelPrevPlaybackFileSelection->setAlignment(Qt::AlignCenter | Qt::AlignTop);
    labelPrevPlaybackFileSelection->setContentsMargins(0,SS_VERTICAL_SPACING,0,SS_VERTICAL_SPACING);
    playbackFrame->addWidget(labelPrevPlaybackFileSelection);

    for (int i = 0; i < SPLASH_SCREEN_NUM_REMEMBERED_FILES; i++) {
        EmbeddedButton *newLabel = new EmbeddedButton("");
        newLabel->setAlignment(Qt::AlignCenter | Qt::AlignTop);
        newLabel->setContentsMargins(0,SS_VERTICAL_SPACING,0,SS_VERTICAL_SPACING);
//        connect(newLabel,SIGNAL(sig_sendLink(QString)),this,SIGNAL(sig_initializeFilePlayback(QString)));
        connect(newLabel,SIGNAL(sig_sendLink(QString)),this,SLOT(loadFile(QString)));
        playbackFileLabels.append(newLabel);
    }

    //  *** Quick Start Frame
    /*quickstartFrame = new ExpandableFrame();
    //#10cf80   #28b951
    quickstartFrame->setObjectName("quickstartFrame");
    if (JUST_BORDERS) {
        quickstartFrame->setStyleSheet("#quickstartFrame {border: 2px solid; border-radius: 5px}"
                                     "#quickstartFrame::hover {border: 2px solid #50C0C0; border-radius: 5px}"
                                     "QFrame {background-color:white;}"
                                     "QLabel {background-color:white;}"
                                     );
    }
    else if (UI_FILL) {
        quickstartFrame->setStyleSheet("#quickstartFrame {border: 2px solid; border-radius: 5px}"
                                     "QFrame {background-color:white;}"
                                     "QLabel {background-color:white;}"
                                     );
        quickstartFrame->setHoverColor("#50C0C0");
    }
    else if (ANIMATED_BORDER_FILL) {
        quickstartFrame->setStyleSheet("#quickstartFrame {border: 2px; border-style: solid; border-radius: 5px; border-color:#d0d0d0;}"
                                     "QFrame {background-color:white;}"
                                     "QLabel {background-color:white;}"
                                     );
        quickstartFrame->setHoverColor("#50C0C0");
//        quickstartFrame->setBaseColor("#80F0F0");
    }

    labelQuickStart = new QLabel(tr("Auto-Connect"));
    f = labelQuickStart->font();
    f.setFamily(SS_EF_FRAME_LABEL_FONT);
    f.setPointSize(SS_EF_FRAME_LABEL_FONT_SIZE);
    labelQuickStart->setFont(f);
    quickstartFrame->addWidget(labelQuickStart, 1, Qt::AlignCenter | Qt::AlignTop);
    //check if the minimum frame size needs to be adjusted
    if (quickstartFrame->getBaseSize().width() > minFrameSize.width()) {
        minFrameSize = quickstartFrame->getBaseSize();
    }

    buttonEthConnect = new EmbeddedButton("Connect to hardware (Ethernet)");
    connect(buttonEthConnect,SIGNAL(sig_clicked()),this,SLOT(buttonEthPressed()));
    buttonEthConnect->setAlignment(Qt::AlignCenter | Qt::AlignTop);
    buttonEthConnect->setContentsMargins(0,SS_VERTICAL_SPACING,0,SS_VERTICAL_SPACING);
    quickstartFrame->addWidget(buttonEthConnect);

    buttonUsbConnect = new EmbeddedButton("Connect to hardware (USB)");
    connect(buttonUsbConnect,SIGNAL(sig_clicked()),this,SLOT(buttonUsbPressed()));
    buttonUsbConnect->setAlignment(Qt::AlignCenter | Qt::AlignTop);
    buttonUsbConnect->setContentsMargins(0,SS_VERTICAL_SPACING,0,SS_VERTICAL_SPACING);
    quickstartFrame->addWidget(buttonUsbConnect);*/

    for (int i = 0; i < SPLASH_SCREEN_NUM_REMEMBERED_FILES; i++) {
        EmbeddedButton *newLabel = new EmbeddedButton("");
        newLabel->setAlignment(Qt::AlignCenter | Qt::AlignTop);
        newLabel->setContentsMargins(0,SS_VERTICAL_SPACING,0,SS_VERTICAL_SPACING);
    //        connect(newLabel,SIGNAL(sig_sendLink(QString)),this,SIGNAL(sig_initializeFilePlayback(QString)));
        connect(newLabel,SIGNAL(sig_sendLink(QString)),this,SLOT(loadFile(QString)));
        playbackFileLabels.append(newLabel);
    }


    expandingFrames.append(infoFrame);
    //expandingFrames.append(quickstartFrame); Disabling Auto-Connect until we figure out how it should work

    //quickstartFrame->setVisible(true);

    expandingFrames.append(createWorkspaceFrame);
    expandingFrames.append(workspaceFrame);
    expandingFrames.append(playbackFrame);
    setFileLabelsAndLinks(); //set the labels and links for all embedded buttons in the frames

    //Add all frames to the layout
    for (int i = 0; i < expandingFrames.length(); i++) {
        expandingFrames.at(i)->setSize(minFrameSize);
        verticalButtonLayout->addWidget(expandingFrames.at(i), 0, Qt::AlignCenter);
        connect(expandingFrames.at(i), SIGNAL(sig_frameExpanding(ExpandableFrame*)), this, SLOT(closeOtherFrames(ExpandableFrame*)));
    }
    verticalButtonLayout->addWidget(spacerLabel_1, 1);
    mainLayout->addLayout(verticalButtonLayout, 4);

    setLayout(mainLayout);
}

TrodesSplashScreen::~TrodesSplashScreen() {

}

void TrodesSplashScreen::initializeAnimations() {
    //To work properly, animations must be initialized as class object variables.  If you don't do this, the
    //animations tend to get deleted after the instance it was created in closes.

    //create a opacity QGraphics effecet
    QGraphicsOpacityEffect *splashScreenEffect = new QGraphicsOpacityEffect(this);
    this->setGraphicsEffect(splashScreenEffect); //assign the graphics effect ot the Splash Screen
    a_fade = new QPropertyAnimation(splashScreenEffect, "opacity"); //create an animation to utilize the graphics effect
    this->setWindowOpacity(100); //prevent the splash screen from initially being faded
    connect(a_fade, SIGNAL(finished()), this, SLOT(fadeAnimationFinished()));
    //Super janky solution below for fixing the initial opacity problem (screen is slightly opaque)
    a_fade->setDuration(1);
    a_fade->setStartValue(1);
    a_fade->setEndValue(1);
    a_fade->start();
    connect(a_fade,SIGNAL(finished()),this,SIGNAL(sig_fadeAnimationCompleted()));
}

//this function is only passed the press event if the press happens outside of other intercepting objects
//this can be changed by passing the QMouseEvent from the child objects to the parent...
void TrodesSplashScreen::mousePressEvent(QMouseEvent *event) {
    //close all expanded frames
    closeAllFrames();
}

//this function dynamically resizes the splash screen whenever the parent widget it is covering is resized
bool TrodesSplashScreen::eventFilter(QObject *obj, QEvent *ev) {
    if (obj == parent()) {
        if (ev->type() == QEvent::Resize) {

            resize(static_cast<QResizeEvent*>(ev)->size());
        }
        else if (ev->type() == QEvent::ChildAdded) {
            raise();
        }
    }
    return(QGroupBox::eventFilter(obj, ev));
}

//this function allows the splash screen to cover multiple widgets over the course of its' life
bool TrodesSplashScreen::event(QEvent *ev) {
    if (ev->type() == QEvent::ParentAboutToChange) {
        if(parent())
            parent()->removeEventFilter(this);
    }
    else if (ev->type() == QEvent::ParentChange) {
        newParent();
    }
    return(QGroupBox::event(ev));
}

void TrodesSplashScreen::setCanLoadFile(bool canLoadF) {
    canLoadFile = canLoadF;
}

void TrodesSplashScreen::setVisible(bool visible) {
    if (visible) {
        setCanLoadFile(true);
        setFileLabelsAndLinks(); //reload previously accessed files.  This allows the file lists to be updated durring specific Trodes instances
        //make sure that the frames can accomodate the new file name lengths
        for (int i = 0; i < expandingFrames.length(); i++) {
            expandingFrames.at(i)->resizeFrame();
        }
        //MARK: Flaming, just for fun
        if (flameOn)
            flamingGif->start();
    }
    else {
        //Close all frames if fading the Splash Screen.
        closeAllFrames();
    }
//    setHidden(!visible);

    QWidget::setVisible(visible);

}

//This function runs a simple Splash Screen fade in/out animation over the specified time (in ms)
void TrodesSplashScreen::runAnimation_windowFade(int time, bool fadeIn) {
    if (time <= 0)
        time = SPLASH_SCREEN_DEFAULT_FADE_TIME;

    a_fade->setDuration(time);
    isFadedOut = !fadeIn;
    if (fadeIn) {
        a_fade->setStartValue(0);
        a_fade->setEndValue(1);
    }
    else {
        a_fade->setStartValue(1);
        a_fade->setEndValue(0);
    }
    a_fade->start();
}

//Slot that receives a workspace path and attempts to load it if no other workspace is currently open
//Used in conjunction with the Embedded Button's 'sig_clicked(QString)' signal
void TrodesSplashScreen::loadWorkspace(QString file) {
    if (canLoadFile) {
        setCanLoadFile(false);
        emit sig_loadWorkspace(file);
    }
    else
        qDebug() <<" Cannot load workspace because one is already open.";
}

//Slot that receives a file at the specified path for playback if no other file or workspace is currently open
//Used in conjunction with the Embedded Button's 'sig_clicked(QString)' signal
void TrodesSplashScreen::loadFile(QString file) {
    if (canLoadFile) {
        setCanLoadFile(false);
        emit sig_initializeFilePlayback(file);
    }
    else
        qDebug() <<" Cannot load file for playback because one is already open.";
}

void TrodesSplashScreen::closeOtherFrames(ExpandableFrame *expFrame) {
    for (int i = 0; i < expandingFrames.length(); i++) {
        expandingFrames.at(i)->raise();
        if (expandingFrames.at(i) != expFrame && expandingFrames.at(i)->isExpanded())
            expandingFrames.at(i)->setExpanded(false);
    }
    expFrame->raise();
}

//simple function to close all expanding frames in the splash screen
void TrodesSplashScreen::closeAllFrames() {
    for (int i = 0; i < expandingFrames.length(); i++) {
        if (expandingFrames.at(i)->isExpanded()) {
            expandingFrames.at(i)->setExpanded(false);
        }
    }
}

//slot to fire whenever a fade animation is finished
void TrodesSplashScreen::fadeAnimationFinished() {
    if (isFadedOut) {
        setVisible(false); //if fade out, make sure to set visibility off so that the window under the splash screen is interactable
    }
    else {
        raise(); //if faded in, the visibility should have already been set in the covered widget, simply raise the splash screen to the top of the Z stack
    }
}

void TrodesSplashScreen::setTemplateLabelsAndLinks() {
    for (int i = 0; i < templateLabels.length(); i++) {
//        EmbeddedButton *curButton = templateLabels.at(i);

        QString templateLabel = "";
        QString templatePath = "";

        switch(i) {
        case 0: {
            templateLabel = "32_Singles_ECU";
            templatePath = QString("%1/Resources/SampleWorkspaces/32_Singles_ECU.trodesconf").arg(QCoreApplication::applicationDirPath());
            break;
        }
        case 1: {
            templateLabel = "32_Singles_ECU_Sensors_MCUDigIn";
            templatePath = QString("%1/Resources/SampleWorkspaces/32_Singles_ECU_Sensors_MCUDigIn.trodesconf").arg(QCoreApplication::applicationDirPath());
            break;
        }
        case 2: {
            templateLabel = "128_Tetrodes_ECU_Sensors";
            templatePath = QString("%1/Resources/SampleWorkspaces/128_Tetrodes_ECU_Sensors.trodesconf").arg(QCoreApplication::applicationDirPath());
            break;
        }
        case 3: {
            templateLabel = "128_Tetrodes_NoECU_Sensors";
            templatePath = QString("%1/Resources/SampleWorkspaces/128_Tetrodes_NoECU_Sensors.trodesconf").arg(QCoreApplication::applicationDirPath());
            break;
        }
        case 4: {
            templateLabel = "Config256_singles";
            templatePath = QString("%1/Resources/SampleWorkspaces/Config256_singles.trodesconf").arg(QCoreApplication::applicationDirPath());
            break;
        }
        default: {
            break;
        }
        }

        templateLabels.at(i)->setButtonText(templateLabel);
        //setting the max width here will automatically truncate the label name if its' width exceeds the max width
        templateLabels.at(i)->setMaxWidth(SS_EF_ABSOLUTE_MAX_WIDTH-(SS_EF_HORIZONTAL_SPACING));
        templateLabels.at(i)->setHyperLink(templatePath);
        if (templatePath != "")
            createWorkspaceFrame->addWidget(templateLabels.at(i));
    }
}

//This function retrieves previous file labels/paths from the storing operating system variable
//and reassigns the labels to the expanding frame's embedded buttons
void TrodesSplashScreen::setFileLabelsAndLinks() {
    //access the operating system storage variable
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("paths")); //access the variable group 'paths'
    //retrieve the 'prevWorkspacePaths' and 'prevPlaybackPaths' values from the variable group
    QStringList prevWorkspaces = settings.value("prevWorkspacePaths").toStringList();
    QStringList prevPlaybackFiles = settings.value("prevPlaybackPaths").toStringList();

    //assign prev. workspaces for editing
    for(int i = 0; i < SPLASH_SCREEN_NUM_REMEMBERED_FILES && i < prevWorkspaces.length(); ++i){
        QString workspacePath = prevWorkspaces.at(i);
        QString workspaceLabel = workspacePath.split("/").last(); //retrieve only the file name as the label
        templateLabels.at(i)->setButtonText(workspaceLabel);
        //setting the max width here will automatically truncate the label name if its' width exceeds the max width
        templateLabels.at(i)->setMaxWidth(SS_EF_ABSOLUTE_MAX_WIDTH-(SS_EF_HORIZONTAL_SPACING));
        templateLabels.at(i)->setHyperLink(workspacePath);
        //if the label is not already in the Frame, add it
        if (!createWorkspaceFrame->isWidgetInFrame(templateLabels.at(i)))
            createWorkspaceFrame->addWidget(templateLabels.at(i));
    }

    //assign workspace label text
    for (int i = 0; i < prevWorkspaces.length(); i++) { //for all labels
        if (i < SPLASH_SCREEN_NUM_REMEMBERED_FILES) { //only display a specified number of remembered paths
            QString workspacePath = prevWorkspaces.at(i);
            QString workspaceLabel = workspacePath.split("/").last(); //retrieve only the file name as the label
            if (i == 0) {
                //special label for the first one
                workspaceLabel = QString("Most Recent (%1)").arg(workspaceLabel);
            }
            workspaceLabels.at(i)->setButtonText(workspaceLabel);
            //setting the max width here will automatically truncate the label name if its' width exceeds the max width
            workspaceLabels.at(i)->setMaxWidth(SS_EF_ABSOLUTE_MAX_WIDTH-(SS_EF_HORIZONTAL_SPACING));
            workspaceLabels.at(i)->setHyperLink(workspacePath);
            //if the label is not already in the Frame, add it
            if (!workspaceFrame->isWidgetInFrame(workspaceLabels.at(i)))
                workspaceFrame->addWidget(workspaceLabels.at(i));
        }
    }
    //assign playbackFile label text
    for (int i = 0; i < prevPlaybackFiles.length(); i++) {
        if (i < SPLASH_SCREEN_NUM_REMEMBERED_FILES) {
            QString playbackPath = prevPlaybackFiles.at(i);
            QString playbackLabel = playbackPath.split("/").last();
            if (i == 0) {
                //special label for the first one
                playbackLabel = QString("Most Recent (%1)").arg(playbackLabel);
            }
            playbackFileLabels.at(i)->setButtonText(playbackLabel);
            playbackFileLabels.at(i)->setMaxWidth(SS_EF_ABSOLUTE_MAX_WIDTH-(SS_EF_HORIZONTAL_SPACING));
            playbackFileLabels.at(i)->setHyperLink(playbackPath);
            //if the label is not already in the Frame, add it
            if (!playbackFrame->isWidgetInFrame(playbackFileLabels.at(i)))
                playbackFrame->addWidget(playbackFileLabels.at(i));
        }
    }
    //Note that for both workspace and playbackFiles we don't need to check if a label is an empty str
    //because non-empty labels are already guarenteed by the loop parameters

    settings.endGroup(); //close the system variable access
}

void TrodesSplashScreen::buttonCreateNewPressed() {
    //qDebug() << "Create new workspace.";
    emit sig_createNewWorkspaceButtonPressed();
}

void TrodesSplashScreen::buttonLoadPressed() {
//    qDebug() << "Load Workspace.";
    emit sig_loadWorkspaceButtonPressed();
}

void TrodesSplashScreen::buttonPlaybackPressed() {
//    qDebug() << "Initialize File Playback.";
    emit sig_loadFileForPlaybackButtonPressed();
}

void TrodesSplashScreen::chooseTemplateFile() {
    //NOTE: this path will be different on mac and linux
//    QString templateFilesPath;// = QString("%1/Resources/SampleWorkspaces").arg(QCoreApplication::applicationDirPath());
//#if defined (__APPLE__)
//templateFilesPath = QString("%1/../../../Resources/SampleWorkspaces").arg(QCoreApplication::applicationDirPath());
//#else
//templateFilesPath = QString("%1/Resources/SampleWorkspaces").arg(QCoreApplication::applicationDirPath());
//#endif
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("paths"));
    QStringList tempPathList = settings.value(QLatin1String("prevWorkspacePaths")).toStringList();
    QString tempPath;
    if(tempPathList.isEmpty())
        tempPath = QDir::currentPath();
    else
        tempPath = tempPathList.first();
    settings.endGroup();

#if defined (__linux__)
        //QString fileName = QFileDialog::getOpenFileName(this, QString("Choose Template Workspace"), tempPath, "Trodes config files (*.trodesconf *.xml)",nullptr, QFileDialog::DontUseNativeDialog);
        QString fileName = QFileDialog::getOpenFileName(this, QString("Choose Template Workspace"), tempPath, "Trodes config files (*.trodesconf *.xml)");
#else
        QString fileName = QFileDialog::getOpenFileName(this, QString("Choose Template Workspace"), tempPath, "Trodes config files (*.trodesconf *.xml)");
#endif


    if (!fileName.isEmpty()) {
        emit sig_loadTemplateWorkspace(fileName);
    }
//    else
//        qDebug() << "Error: Bad Path. (TrodesSplashScreen::chooseTemplateFile)";
}

void TrodesSplashScreen::versionChecker(bool isLatest, QString version, bool maintenancetool){
    if(BETA_VERSION || !isLatest){

        QString iconpath = ":/error.png";
        labelWarningIcon->setPixmap(QPixmap(iconpath).scaled(SS_VERSION_ERROR_IMG_DIM,SS_VERSION_ERROR_IMG_DIM));
        labelWarningIcon->setVisible(true);
        labelInfoFrameTrodesVersion->setContentsMargins(0,0,(SS_VERSION_ERROR_IMG_DIM/2),0);
        labelInfoFrameTrodesVersion->setText(
                    QString("<a href=\"https://bitbucket.org/mkarlsso/trodes/downloads/\">") +
                    QString(BETA_VERSION ? "This version is a BETA version. Click for a stable version" :
                                           "There is a newer version available. Click here to update") +
                    QString((version.isEmpty()) ? "" : " ("+ version +")") +
                    QString("</a>"));
        infoFrame->resizeFrame();
        infoFrame->setExpanded(true);
        labelInfoFrameTrodesVersion->setTextFormat(Qt::RichText);
        if(!maintenancetool){
            labelInfoFrameTrodesVersion->setTextInteractionFlags(Qt::TextBrowserInteraction);
            labelInfoFrameTrodesVersion->setOpenExternalLinks(true);
        }
        connect(labelInfoFrameTrodesVersion, &QLabel::linkActivated, this, [this](const QString& link){
            emit launchUpdater();
        });
    }
    else {
        labelWarningIcon->setVisible(false);
        labelInfoFrameTrodesVersion->setContentsMargins(0,0,0,0);
    }
    update();
}

void TrodesSplashScreen::buttonEthPressed(){
    emit quickstartEthernet();
}

void TrodesSplashScreen::buttonUsbPressed(){
    emit quickstartUSB();
}

void TrodesSplashScreen::expandQuickStart(){
    //quickstartFrame->setExpanded(true);
}

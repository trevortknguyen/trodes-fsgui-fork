/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QApplication>
#include <QWidget>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <fstream>

#include "mainWindow.h"
#include "configuration.h"
#include "triggerThread.h"
#include "globalObjects.h"

//#include "usbdaqThread.h"
#include "sourceController.h"
#include "customApplication.h"

//#define Num_Char_In_Debug_Buffer 1000

//char Debug_Message_Buffer[Num_Char_In_Debug_Buffer];
//uint32_t Location_In_Debug_Buffer = 0;

int main(int argc, char *argv[])
{
//    bool testFlag = false;
//    if (argc > 1) {
//        QString testStringCheck = QString(argv[1]);
//        if (testStringCheck.compare("test") == 0) {
//            testFlag = true;
//        }
//    }

    //qInstallMessageHandler(myMessageOutput_NoScreenOutput);

    qRegisterMetaType<TrodesEvent>("TrodesEvent");
    qRegisterMetaType<TrodesEventMessage>("TrodesEventMessage");
    qRegisterMetaType<QVector<TrodesEvent> >("QVector<TrodesEvent>");
    qRegisterMetaType<uint32_t>("uint32_t");
    qRegisterMetaType<size_t>("size_t");
    qRegisterMetaType<HighFreqDataType>("HighFreqDataType");
    qRegisterMetaType<HardwareControllerSettings>("HardwareControllerSettings");
    qRegisterMetaType<HeadstageSettings>("HeadstageSettings");
    qunsetenv("OPENSSL_CONF");
    customApplication app(argc, argv);

    fflush(NULL); //Fixes rare bug where path is too long or certain permutations of these long path names and Trodes crashes.
    QCoreApplication::addLibraryPath(QCoreApplication::applicationDirPath());
    app.setStyle(new Style_tweaks);

    //QApplication app(argc, argv);





    //MainWindow w;
    //w.show();

    if (!app.testMode) {
        int retval = app.exec();
/*#if defined (__WINDOWS__)
    zsys_shutdown(); //line is necessary to prevent crash on exit in windows
#endif*/
        return(retval);
    } else {
/*#if defined (__WINDOWS__)
        zsys_shutdown(); //line is necessary to prevent crash on exit in windows
#endif*/
        return(0);
    }

}

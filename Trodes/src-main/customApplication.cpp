/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "customApplication.h"
#include "configuration.h"

std::ofstream outfile;
QString lastDebugMsg;
QMutex debugLock;

QStringList debugOutputRecord; //There might be a way to keep a log of debug output without a global variable, but couldn't think of a way
QMutex testDebugLock;

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{

    debugLock.lock();
    QByteArray localMsg = msg.toLocal8Bit();
    lastDebugMsg = msg;

    //std::ofstream outfile;
    QDateTime messageTime;
    outfile <<  QDateTime::currentDateTime().toString().toLocal8Bit().constData() << " ";

    switch (type) {
    case QtDebugMsg:


        fprintf(stderr, "[Trodes] %s\n", localMsg.constData());
        fflush(stderr);


        //outfile.open("trodesDebugLog.txt", std::ios_base::app);


        outfile << localMsg.constData() << "\n";
        outfile.flush();


        //outfile.close();


        //for (int i=0; i < localMsg.length(); i++) {
        //    Debug_Message_Buffer[Location_In_Debug_Buffer] = localMsg.at(i);
        //    Location_In_Debug_Buffer = (Location_In_Debug_Buffer+1) % Num_Char_In_Debug_Buffer;
        //}

        break;
    //case QtInfoMsg:
    //    fprintf(stderr, "Info: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
    //    break;
    case QtWarningMsg:
        fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fflush(stderr);

        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fflush(stderr);

        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fflush(stderr);
        abort();
    default:
        //outfile.open("trodesDebugLog.txt", std::ios_base::app);
        outfile << localMsg.constData() << "\n";
        outfile.flush();
        //outfile.close();
    }
    debugLock.unlock();

}

void myMessageOutput_NoScreenOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
}

void noMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    testDebugLock.lock();
    debugOutputRecord << msg;
    testDebugLock.unlock();
}

UnitTest::UnitTest()
{

}

void UnitTest::workspace_loading()
{

    //This test loads a set of workspaces contained within a subdirectory named
    //"Testing/WorkspaceLoading" located in the same directory as the Trodes executable (or bundle for Mac).
    //All workspaces need to open, and all modules need to call "sendModuleIsReady()" on the network

    QTest::qWaitForWindowExposed(window);


    //QTest::keyClicks(&lineEdit, "hello world");
    QDir testPath = getTestPath();

    testPath.cd("WorkspaceLoading");

    QStringList filters;
    QStringList confFiles;
    int numLoads = 0;
    filters << "*.trodesconf";
    confFiles = testPath.entryList(filters);


    //Load up each workspace in the directory, one by one
    for (int i=0; i<confFiles.length();i++) {
        int loadSuccess = window->openWorkspaceFileForAcquisition(testPath.absolutePath() +"/" + confFiles[i]);
        if (loadSuccess==0) {

            int numChecks = 0;

            while (numChecks < 10) {
                QTest::qWait(500);
                //TODO: need to move testing to new network as well
//                QList<qint8> readyMods = window->trodesNet->tcpServer->getModulesReady();

                //Make sure all modules have returned the sendModuleIsReady() call
//                if (readyMods.length() == moduleConf->singleModuleConf.length()) {
//                    numLoads++;
//                    break;
//                } else {
//                    numChecks++;
//                }
            }
        }

        //Close the workspace before opening the next
        window->closeConfig();
    }


    QCOMPARE(numLoads,confFiles.length()); //anything but zero is a fail code

}

void UnitTest::playback()
{

    //This test loads a set of recordings contained within a subdirectory named
    //"Testing/Playback" located in the same directory as the Trodes executable (or bundle for Mac).
    //All workspaces need to open, and


    QStringList debugSearchPhrases;
    debugSearchPhrases << "Signal error-- check config file"; //This means that the timestamps are not incrementing normally-- probably due to bad frame alignment
    debugSearchPhrases << "Bad frame alignment in source packet";
    debugSearchPhrases << "Buffer overrun!";  //This means that the streamProcessorThreads are not keeping up with the data


    QTest::qWaitForWindowExposed(window);


    //QTest::keyClicks(&lineEdit, "hello world");
    QDir testPath = getTestPath();

    testPath.cd("Playback");

    QStringList filters;
    QStringList confFiles;
    int numLoads = 0;
    filters << "*.rec";
    confFiles = testPath.entryList(filters);


    //Load up each workspace in the directory, one by one
    for (int i=0; i<confFiles.length();i++) {

        bool loadSuccess = window->openPlaybackFile(testPath.absolutePath() +"/" + confFiles[i]);
        if (loadSuccess) {
            debugOutputRecord.clear();
            window->actionPlaySelected();
            QTest::qWait(2000);
            window->disconnectFromSource();

            bool searchPhraseFound = false;
            for (int dr=0; dr < debugOutputRecord.length(); dr++ ) {
                for (int sp=0; sp < debugSearchPhrases.length(); sp++) {
                    if (debugOutputRecord[dr].contains(debugSearchPhrases[sp])) {
                        searchPhraseFound = true;
                        fprintf(stderr, "Error debug phrase during playback of file %s: %s\n", confFiles[i].toLocal8Bit().constData(),debugSearchPhrases[sp].toLocal8Bit().constData());
                    }
                }
            }

            if (!searchPhraseFound) {
                numLoads++;
            }

        }

        //Close the workspace before opening the next
        window->closeConfig();
    }


    QVERIFY(numLoads==confFiles.length()); //anything but zero is a fail code

}


void UnitTest::hardware_connect_ethernet()
{

    QStringList debugSearchPhrases;
    debugSearchPhrases << "Connection to source failed";

    QTest::qWaitForWindowExposed(window);

    window->setSource(SourceEthernet);
    window->setSource(SourceNone);

    bool connectionFailed = false;
    for (int dr=0; dr < debugOutputRecord.length(); dr++ ) {
        for (int sp=0; sp < debugSearchPhrases.length(); sp++) {
            if (debugOutputRecord[dr].contains(debugSearchPhrases[sp])) {
                connectionFailed = true;
            }
        }
    }

    QVERIFY(connectionFailed == false); //anything but zero is a fail code

}

void UnitTest::hardware_connect_usb()
{

    QStringList debugSearchPhrases;
    debugSearchPhrases << "Connection to source failed";

    QTest::qWaitForWindowExposed(window);

    window->setSource(SourceUSBDAQ);
    window->setSource(SourceNone);

    bool connectionFailed = false;
    for (int dr=0; dr < debugOutputRecord.length(); dr++ ) {
        for (int sp=0; sp < debugSearchPhrases.length(); sp++) {
            if (debugOutputRecord[dr].contains(debugSearchPhrases[sp])) {
                connectionFailed = true;
            }
        }
    }

    QVERIFY(connectionFailed == false); //anything but zero is a fail code

}

void UnitTest::hardware_stream_usb()
{
    QTest::qWaitForWindowExposed(window);

    QStringList debugSearchPhrases;
    debugSearchPhrases << "Signal error-- check config file"; //This means that the timestamps are not incrementing normally-- probably due to bad frame alignment
    debugSearchPhrases << "Bad frame alignment in source packet";
    debugSearchPhrases << "Buffer overrun!";  //This means that the streamProcessorThreads are not keeping up with the data
    debugSearchPhrases << "No data coming from hardware. Please reset the system and try again."; //If no data coming after set interval
    debugSearchPhrases << "An error occured during data acquisition.";
    //debugSearchPhrases << "Jump in timestamps:"; //At least one data packet dropped
    debugSearchPhrases << "Signal error-- check config file";


    QDir testPath = getTestPath();

//    window->setSource(SourceUSBDAQ);
    window->actionSourceUSB->trigger();
    bool connectSuccess = false;
    if (window->sourceControl->currentSource == SourceUSBDAQ) {
        connectSuccess = true;
    }
//    window->setSource(SourceNone);
    window->actionSourceNone->trigger();

    testPath.cd("WorkspaceHardwareStreaming");

    QStringList filters;
    QStringList confFiles;
    int numLoads = 0;
    filters << "*.trodesconf";
    confFiles = testPath.entryList(filters);

    if (connectSuccess) {
        //Load up each workspace in the directory, one by one
        for (int i=0; i<confFiles.length();i++) {
            int loadSuccess = window->openWorkspaceFileForAcquisition(testPath.absolutePath() +"/" + confFiles[i]);
            if (loadSuccess==0) {

                debugOutputRecord.clear();
                quint64 numDropEvents = 0;
                bool streamFail = false;

                // Now connect to hardware.
//                window->setSource(SourceUSBDAQ);
                window->actionSourceUSB->trigger();
                if (window->sourceControl->currentSource != SourceUSBDAQ) {
                    break;
                }
                window->sourceControl->connectToSource_Simulation();
                QTest::qWait(10000);
                numDropEvents = window->sourceControl->getTotalDroppedPacketEvents();
                if (numDropEvents > 1) {
                    fprintf(stderr, "Too many data drop events during USB streaming using workspace %s\n", confFiles[i].toLocal8Bit().constData());
                    streamFail  = true;
                }

                window->disconnectFromSource();

                bool searchPhraseFound = false;
                for (int dr=0; dr < debugOutputRecord.length(); dr++ ) {
//                    fprintf(stderr, "%s\n", debugOutputRecord[dr].toLocal8Bit().constData());
                    for (int sp=0; sp < debugSearchPhrases.length(); sp++) {
                        if (debugOutputRecord[dr].contains(debugSearchPhrases[sp])) {
                            searchPhraseFound = true;
                            fprintf(stderr, "Error debug phrase during USB streaming using workspace %s: %s\n", confFiles[i].toLocal8Bit().constData(),debugSearchPhrases[sp].toLocal8Bit().constData());
                        }
                    }
                    if (searchPhraseFound) {
                        break;
                    }
                }

                if (!searchPhraseFound &&!streamFail) {
                    numLoads++;
                }


            }
        }

        //Close the workspace before opening the next
        window->closeConfig();

    }


    QVERIFY(numLoads==confFiles.length()); //anything but zero is a fail code

}

void UnitTest::hardware_stream_ethernet()
{
    QTest::qWaitForWindowExposed(window);

    QStringList debugSearchPhrases;
    debugSearchPhrases << "Signal error-- check config file"; //This means that the timestamps are not incrementing normally-- probably due to bad frame alignment
    debugSearchPhrases << "Bad frame alignment in source packet";
    debugSearchPhrases << "Buffer overrun!";  //This means that the streamProcessorThreads are not keeping up with the data
    debugSearchPhrases << "No data coming from hardware. Please reset the system and try again."; //If no data coming after set interval
    debugSearchPhrases << "An error occured during data acquisition.";
    //debugSearchPhrases << "Jump in timestamps:"; //At least one data packet dropped
    debugSearchPhrases << "Signal error-- check config file";

    QDir testPath = getTestPath();

    window->setSource(SourceEthernet);
    bool connectSuccess = false;
    if (window->sourceControl->currentSource == SourceEthernet) {
        connectSuccess = true;
    }
    window->setSource(SourceNone);

    testPath.cd("WorkspaceHardwareStreaming");

    QStringList filters;
    QStringList confFiles;
    int numLoads = 0;
    filters << "*.trodesconf";
    confFiles = testPath.entryList(filters);

    if (connectSuccess) {
        //Load up each workspace in the directory, one by one
        for (int i=0; i<confFiles.length();i++) {
            int loadSuccess = window->openWorkspaceFileForAcquisition(testPath.absolutePath() +"/" + confFiles[i]);
            if (loadSuccess==0) {

                debugOutputRecord.clear();
                quint64 numDropEvents = 0;
                bool streamFail = false;

                // Now connect to hardware.
                window->setSource(SourceEthernet);
                if (window->sourceControl->currentSource != SourceEthernet) {
                    break;
                }
                window->sourceControl->connectToSource_Simulation();
                QTest::qWait(10000);
                numDropEvents = window->sourceControl->getTotalDroppedPacketEvents();
                if (numDropEvents > 1) {
                    fprintf(stderr, "Too many data drop events during Ethernet streaming using workspace %s", confFiles[i].toLocal8Bit().constData());
                    streamFail  = true;
                }
                window->disconnectFromSource();

                bool searchPhraseFound = false;
                for (int dr=0; dr < debugOutputRecord.length(); dr++ ) {
                    for (int sp=0; sp < debugSearchPhrases.length(); sp++) {
                        if (debugOutputRecord[dr].contains(debugSearchPhrases[sp])) {
                            searchPhraseFound = true;
                            fprintf(stderr, "Error debug phrase during USB streaming using workspace %s: %s\n", confFiles[i].toLocal8Bit().constData(),debugSearchPhrases[sp].toLocal8Bit().constData());
                        }
                    }
                    if (searchPhraseFound) {
                        break;
                    }
                }

                if (!searchPhraseFound  && !streamFail) {
                    numLoads++;
                }


            }
        }

        //Close the workspace before opening the next
        window->closeConfig();

    }


    QVERIFY(numLoads==confFiles.length()); //anything but zero is a fail code

}

//Called before each test
void UnitTest::init()
{
    //Clear the debug output record from the previous test
    debugOutputRecord.clear();
}

//First test is to start GUI
void UnitTest::initTestCase()
{
    qInstallMessageHandler(noMessageOutput); //We route qDebug() statements to a QStringList in order to use them in tests.
    window = new MainWindow();
    window->setSplashScreenAnimation(false);
    window->show();
    window->unitTestFlag = true;
    window->startThreads();

    //  TODO: CHECK TO MAKE SURE WINDOW STARTED OK
}

//Final test is to close GUI
void UnitTest::cleanupTestCase()
{
    window->close();

    //TODO: CHECK TO MAKE SURE CLOSING BEHAVIOR WAS OK
}

//------------------------------------------------------
//Helper functions

QDir UnitTest::getTestPath()
{
    QDir path = QDir(QCoreApplication::applicationDirPath());
#ifdef __APPLE__
    path.cdUp();
    path.cdUp();
    path.cdUp();
#endif

    path.cd("Testing");
    return path;
}



//------------------------------------------------------


//This class inherits QApplication, and is used to process
//the command line arguments or OS events such as double clicking
//on a file associated with this program

customApplication::customApplication( int & argc, char **argv )
    : QApplication(argc, argv)
{
    audioOn = true;
    parseBenchmarkingArgs(argc, argv);
    parseMiscArgs(argc, argv);
    //Check if the first argument is 'test'.  If so, we are doing automated
    //testing of the program.
    testMode = false;
    bool testFlag = false;
    if (argc > 1) {
        QString testStringCheck = QString(argv[1]);
        if (testStringCheck.compare("test") == 0) {
            testFlag = true;
            testMode = true;
        }
    }

    if (!testFlag) {
        time_t rawtime;
        struct tm * timeinfo;
        char buffer [100];

        time (&rawtime);
        timeinfo = localtime (&rawtime);

        strftime (buffer,80,"%m-%d-%Y(%H_%M_%S)",timeinfo);
//        char filename[100];
        QString path;
        path = QString("%1/debugLogs").arg(QCoreApplication::applicationDirPath());
#ifdef __APPLE__
        path = QString("%1/../../../debugLogs").arg(QCoreApplication::applicationDirPath());
#endif
        QDir debugDir(path);
        if (!debugDir.exists())
            debugDir.mkdir(path);
        QString fName = QString("%1/trodesDebugLog_%2.txt").arg(path).arg(buffer);
//        sprintf(filename, "%s", qPrintable(fName));
        outfile.open(qPrintable(fName), std::ios_base::trunc);
        qInstallMessageHandler(myMessageOutput);
    } else {
        qInstallMessageHandler(myMessageOutput_NoScreenOutput);
    }

    if (testFlag) {
        //We are doing automated testing, so the GUI is started in one of the tests
        UnitTest test;
        int newArgc = argc-1;
        QTest::qExec(&test, newArgc, argv+1);

    } else {

        //No testing, so start the GUI normally

        win = new MainWindow(audioOn);
        if(arguments().contains("-highperf")){
            qDebug() << "High performance mode enabled (work in progress)";
            win->setEnableAVXFlag(true);
        }
        win->show();        
        win->startThreads();        
        if (argc > 1) {
           loadFile(argv[1]);
        }
    }
}
//
customApplication::~customApplication()
{
    //delete benchConfig;
}
//

void customApplication::parseBenchmarkingArgs(int &argc, char **argv) {
    bool recSysTime, pSpikeDetect, pSpikeSent, pSpikeReceived, pPositionStreaming, pEventSys;
    int pFreqSpikeDetect, pFreqSpikeSent, pFreqSpikeReceived, pFreqPositionStream, pFreqEventSys;
    recSysTime = false;
    pSpikeDetect = false;
    pSpikeSent = false;
    pSpikeReceived = false;
    pPositionStreaming = false;
    pEventSys = false;
//    bool cmdLineCalled = false;

    pFreqSpikeDetect = BENCH_FREQ_SPIKE_DETECT_DEFAULT;
    pFreqSpikeSent = BENCH_FREQ_SPIKE_SENT_DEFAULT;
    pFreqSpikeReceived = BENCH_FREQ_SPIKE_RECEIVE_DEFAULT;
    pFreqPositionStream = BENCH_FREQ_POS_STREAM_DEFAULT;
    pFreqEventSys = BENCH_FREQ_EVENTSYS_DEFAULT;

    for (int i = 0; i < argc; i++) {

        if (strcmp(argv[i],"benchmarking") == 0) {

            //qDebug() << "activate benchmarking";

            for (int j = i+1; j < argc; j++) {
                if (strcmp(argv[j],"recordSysTime") == 0) {
                    recSysTime = true;
                }
                else if (strcmp(argv[j],"spikeDetect") == 0) {
                    recSysTime = true;
                    pSpikeDetect = true;
                    //read next arg for freq value
                    if ((j+1) < argc) {
                        int val = atoi(argv[j+1]);
                        if (val > 0) { // if a valid int flag was passed
                            pFreqSpikeDetect = val;
                            j++;
                        }
                    }
                }
                else if (strcmp(argv[j],"spikeSent") == 0) {
                    recSysTime = true;
                    pSpikeSent = true;
                    //read next arg for freq value
                    if ((j+1) < argc) {
                        int val = atoi(argv[j+1]);
                        if (val > 0) { // if a valid int flag was passed
                            pFreqSpikeSent = val;
                            j++;
                        }
                    }
                }
                else if (strcmp(argv[j],"spikeReceived") == 0) {
                    recSysTime = true;
                    pSpikeReceived = true;
                    //read next arg for freq value
                    if ((j+1) < argc) {
                        int val = atoi(argv[j+1]);
                        if (val > 0) { // if a valid int flag was passed
                            pFreqSpikeReceived = val;
                            j++;
                        }
                    }
                }
                else if (strcmp(argv[j],"positionStreaming") == 0) {
                    pPositionStreaming = true;
                    //read next arg for freq value
                    if ((j+1) < argc) {
                        int val = atoi(argv[j+1]);
                        if (val > 0) { // if a valid int flag was passed
                            pFreqPositionStream = val;
                            j++;
                        }
                    }
                }
                else if (strcmp(argv[j],"eventSys") == 0) {
                    pEventSys = true;
                    //read next arg for freq value
                    if ((j+1) < argc) {
                        int val = atoi(argv[j+1]);
                        if (val > 0) { // if a valid int flag was passed
                            pFreqEventSys = val;
                            j++;
                        }
                    }
                }
                else {
                    qDebug() << "Error: argument [" << argv[j] << "] not recognized as a valid Benchmarking flag.";
                }

            }
        }
    }


    BenchmarkConfig *myConfig = new BenchmarkConfig(recSysTime, pSpikeDetect, pSpikeSent, pSpikeReceived, pPositionStreaming, pEventSys);
    benchConfig = myConfig;
    benchConfig->setFreqSpikeDetect(pFreqSpikeDetect);
    benchConfig->setFreqSpikeSent(pFreqSpikeSent);
    benchConfig->setFreqSpikeReceived(pFreqSpikeReceived);
    benchConfig->setFreqPositionStream(pFreqPositionStream);
    benchConfig->setFreqEventSys(pFreqEventSys);

    if (recSysTime || pPositionStreaming || pEventSys)
        benchConfig->intiatedFromCommandLine();
}

void customApplication::parseMiscArgs(int &argc, char **argv) {
    for (int i = 0; i < argc; i++) {
        if (strcmp(argv[i],"audioOff") == 0) {
            qDebug() << "[Trodes] Audio Outputs Turned Off";
            audioOn = false;

        }
    }
}

void customApplication::loadFile(const QString &fileName)
{
    QFileInfo fi(fileName);
    if (fi.suffix().compare("trodesconf") == 0) {

        win->openWorkspaceFileForAcquisition(fileName);
    } else if (fi.suffix().compare("rec") == 0) {

        win->openPlaybackFile(fileName);
    }

}


bool customApplication::event(QEvent *event)
{
        switch (event->type()) {
        case QEvent::FileOpen:
            loadFile(static_cast<QFileOpenEvent *>(
                     event)->file());
            return true;
        default:
            return QApplication::event(event);
    }
}

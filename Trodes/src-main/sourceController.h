/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SOURCECONTROLLER_H
#define SOURCECONTROLLER_H

#include <QtGui>
#include "simulateDataThread.h"
#include "simulateSpikesThread.h"
#include "fileSourceThread.h"
#include "ethernetSourceThread.h"
//#include "dialogs.h"
#include "trodesglobaltypes.h"
#include "hardwaresettings.h"


#ifdef RHYTHM
#include "rhythmThread.h"
#endif

#ifdef USE_D3XX
    #include "usb3thread.h"
#else
    #include "dockusbthread.h"
    #include "usbdaqThread.h"
#endif

/*
#define SOURCE_STATE_NOT_CONNECTED 0
#define SOURCE_STATE_INITIALIZED   1
#define SOURCE_STATE_RUNNING       2
#define SOURCE_STATE_CONNECTERROR  3
#define SOURCE_STATE_PAUSED        4

#define SENDSTARTCOMMAND    97
#define SENDSTOPCOMMAND     98
#define SENDCHANNELCONFIGCOMMAND 99 //followed by 4 bytes designating which cards are active (up to 32)
#define SENDDIGITALOUTSTATE 10 // followed by one byte with the channel and another byte with the state (0 or 1)
#define SENDANALOGOUTSTATE  20 // followed by one byte with the channel and then two bytes with the value (16 bits)
#define SENDSTATESCRIPTCHARACTER 30 // follwed by one byte with the character to send to stateScript
#define SENDSTATESCRIPTFUNCTIONTRIGGER 31 // followed by one byte with the function number to trigger

#define EEG_BUFFER_SIZE 60000*/

//Hardcoded memory allocation is not great.  TODO: change to dynamic allocation once the workspace is loaded.
//...pros of hardcoded memory: alignment for vectorized instructions


class RawDataSample {
public:
  RawDataSample(uint32_t ts, double dt);

  uint32_t timestamp;
  double dTime;
  QList< QVector<int16_t> > neuralData;
  QList<int16_t> headerData;
};

enum DataSource{
    SourceNone = TrodesSource::None,
    SourceFake = TrodesSource::Fake,
    SourceFakeSpikes = TrodesSource::FakeSpikes,
    SourceFile = TrodesSource::File,
    SourceEthernet = TrodesSource::Ethernet,
    SourceUSBDAQ = TrodesSource::USBDAQ,
    SourceRhythm = TrodesSource::Rhythm,
    SourceDockUSB = TrodesSource::USBDock,
    SourceUSB3 = TrodesSource::USB3
};
Q_DECLARE_METATYPE(DataSource)



class SourceController : public QObject {
  Q_OBJECT

public:
  SourceController(QObject *parent);


  #ifdef USE_D3XX
  USB3Interface *USB3Source; //USB3 input
  #else
  DockUSBInterface *dockUSBSource;
  USBDAQInterface *USBSource; //USB2 input
  #endif

  simulateDataInterface *waveGeneratorSource; //Internal signal generator
  simulateSpikesInterface *spikesGeneratorSource; //Internal signal generator
  fileSourceInterface *fileSource; //File playback
  EthernetInterface *ethernetSource; //Ethernet input
  AbstractTrodesSource *currentSourceObj;
#ifdef RHYTHM
  RhythmInterface *rhythmSource; //rhd2000/opal kelly
#endif

  int state;
  DataSource currentSource;
  quint64 getTotalDroppedPacketEvents();
  quint64 getTotalUnresponsiveHeadstagePackets();

private:
  int numConnectionTries;
  HeadstageSettings currentHSSettings;
  HardwareControllerSettings currentControllerSettings;


public slots:

  //For messages going from the source back to Trodes mainwindow
  void StartAcquisition(void);
  void StopAcquisition(void);
  void PauseAcquisition(void);
  void newHeadstageSettings(HeadstageSettings s);
  void newControllerSettings(HardwareControllerSettings s);

  //For inbound commands going to the source
  void sendSettleCommand(void);
  void sendSettleChannel(int byteInPacket, quint8 bit, int delay, quint8 triggerState);
  void sendFunctionTriggerCommand(int funcNum);
  void setHeadstageSettings(HeadstageSettings s);
  void setNeuroPixelsSettings(NeuroPixelsSettings s);
  void saveHeadstageSettings();
  void setControllerSettings(HardwareControllerSettings s);
  HeadstageSettings getHeadstageSettings();
  HardwareControllerSettings getControllerSettings();

  void SetStimulationParams(StimulationCommand s);
  void SendGlobalStimulationSettings(GlobalStimulationSettings s);
  void SendGlobalStimulationCommand(GlobalStimulationCommand s);
  void ClearStimulationParams(uint16_t slot);
  void SendStimulationStart(uint16_t slot);
  void SendStimulationStartGroup(uint16_t group);
  void SendStimulationStop(uint16_t slot);
  void SendStimulationStopGroup(uint16_t group);

  void SendECUShortcutMessage(uint16_t function);

  void disconnectFromSource();
  void connectToSource();
  void connectToSource_Simulation();
  void pauseSource(); //file source only

  void connectToSDCard();
  void enableSDCard();
  void reconfigureSDCard(int numChannels);

  void measureImpedance(HardwareImpedanceMeasureCommand s);

  void setSource(DataSource source);
  void setSourceState(int);
  void clearBuffers();
  void waitForThreads();
  void dataError();
  void noDataComing(bool c);


//  void dummySlot(uint32_t, uint32_t);

signals:
  void stateChanged(int);
  void acquisitionStarted(void);
  void acquisitionStopped(void);
  void acquisitionPaused(void);
  void SDCardStatus(bool cardConnected, int numChan, bool unlocked, bool hasData);
  void setTimeStamps(uint32_t,uint32_t);
  void jumpFileTo(qreal value);
  void updateSlider(qreal);
  void headstageSettingsReturned(HeadstageSettings s);
  void controllerSettingsReturned(HardwareControllerSettings s);
  void newGlobalStimSettings(GlobalStimulationSettings s);

  void newTimestamp(uint32_t ts);
  void packetSizeError(bool c);
  void signal_UnresponsiveHeadstage(bool u);
  void impedanceValueReturned(int HWChan, int value);

  void newContinuousData(HighFreqDataType newData);
  void deregisterHighFreqData(HighFreqDataType dType);

};

#endif // SOURCECONTROLLER_H

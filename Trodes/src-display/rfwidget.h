#ifndef RFWIDGET_H
#define RFWIDGET_H

#include <QtWidgets>
//#include "dockusbthread.h"

class RFWidget : public QWidget
{
    Q_OBJECT
public:
    explicit RFWidget(QWidget *parent = nullptr);

    //void dockConnected(QString name, QString connection, DockingStation_t settings);
    void dockDisconnected(uint16_t serial, QString connection);

signals:

public slots:
    void idlePayloadReceived(int dockid, QByteArray bytes);
    void streamPayloadReceived(int dockid, QByteArray bytes);

private:
    QLabel      *dockInfo;
    QLabel      *loggerInfo;
    QLabel      *liveStreamInfo;
    void updateDeviceInfo();

    QString name;
    QString description;
    QString connection;
    uint16_t serial;
    uint16_t model;
    int rfchan;
    int samplingRateKhz;
    QString status;

    uint16_t logger_model;
    uint16_t logger_serial;
    QString logger_status;
    int logger_numchan;
    int logger_samplingRateKhz;
    int logger_sampleSizeBits;
    bool logger_magOn;
    bool logger_gyrOn;
    bool logger_accOn;
    bool logger_filtOn;

    uint32_t logger_sd_blocks_written;

    QElapsedTimer lastupdate;
    QTimer updateTimer;
};

#endif // RFWIDGET_H

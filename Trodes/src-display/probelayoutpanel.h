#ifndef PROBELAYOUTPANEL_H
#define PROBELAYOUTPANEL_H

#include <QObject>

#include <QDialog>
#include <QSlider>
#include <QLabel>
#include <QtWidgets>
#include <QMenuBar>
#include "sharedtrodesstyles.h"
#include <QtCharts/QChartView>
#include <QtCharts/QChartGlobal>
#include <QtCharts/QScatterSeries>
#include <QtCharts/QLineSeries>
#include <QtCharts/QAreaSeries>
#include <QtCharts/QLegendMarker>
#include <QtCharts/QValueAxis>

#include "configuration.h"

//#include <QtCharts/QLineSeries>
//#include <QtCharts/QValueAxis>
//#include <QtCharts/QLogValue>


class ProbeLayoutPanel: public QChartView
{
    Q_OBJECT
public:
    enum selectionMode
    {
        singleContinuousGroup,
        longSingleContinuousGroup,
        multiHighDensityGroups,
        singleChannelToggling
    };

    struct selectionModeData
    {
        QString toolName;
        QList<qreal> arg; //up to 10 arguments per selection mode
        QList<QLineSeries*> controlGraphics;
        QList<QColor> controlColor;

    };

    ProbeLayoutPanel(const QList<QList<selectionModeData> > &allSelectionModeArgs, QWidget *parent = nullptr);

    ~ProbeLayoutPanel();
    void updateChannels();
    void calculateChannelStates();
    void setupProbeInfo(const QList<QList<selectionModeData> > &allSelectionModeArgs);
    void setSelectionModeForAllProbes(QList<int> selectionModes);
    QList<QList<selectionModeData> > getSelectionData();
    QList<QVector<bool> > getChannelsTurnedOn();
    QList<QList<QColor> > getChannelColors();
    QList<QPointF> getChannelLocations();

    QList<int> getAllSelectionModes();
    void copyProbeSettings(int sourceIndex, int destIndex);




protected:
    void mouseReleaseEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);


private slots:

    void channelBoxClicked(const QPointF &point);
    void selectionToolPressed(const QPointF &point);
    void selectionToolReleased(const QPointF &point);
    void openColorSelector();
    void openRangeLengthEditor();

public slots:
    void setViewMin(int value);
    void setViewRange(int range);
    void setSelectionMode(int m);
    void setChannelPlotSize(int s);
    void setProbeShown(int pInd);
    void setChannelHighlight(int chInd);

private:

    bool rightButtonDown;

    QList<QAreaSeries*> channel_boxes;
    QList<QPointF> channelLocations;
    QList<QList<QColor> > channelColors;

    //QList<QVector<int> > off_scatter_index_to_on_scatter;
    int viewRange;
    int viewMinimum;
    selectionMode mode;
    int currentProbeShown;
    int numberOfProbes;
    bool selectionToolCurrentlyPressed;
    QList<QList<selectionModeData> > allSelectionModeData;
    QList<int> selectionModeForEachProbe;
    QList<QVector<bool> > channelsTurnedOn;


    QLineSeries *selectionLineSeriesPressed;
    QPointF clickToFirstPoint;
    QList<QPointF> lineSeriesPointsAtPress;



    void turnOnSingleContinuousRange(qreal lowerlimit, qreal upperlimit, bool shortLengthMode);
    void turnOnMultiHighDensityRanges();
    void toggleSinglePad(int padIndex, bool state);


    int findOutputChannelForPad(int padIndex);
    int calcActiveChannelCount();





    QVector<int>  deleteFlag;

    QList<QList<int> > channelRoutingRules;

signals:
    void newActiveChannelCount(int ch);
    void selectionModeChanged(int sMode);
    void selectionChanged();
    void newStatus(QString status);


};


class ProbeLayoutWidget: public QWidget
{
    Q_OBJECT
public:
    ProbeLayoutWidget(const TrodesConfiguration &c, QWidget *parent = nullptr);
    void applySettings();

protected:
   void wheelEvent(QWheelEvent *event);
   void resizeEvent(QResizeEvent *event);
   void closeEvent(QCloseEvent *event);

private:
    //TrodesConfigurationPointers editWorkspace;
    TrodesConfiguration editWorkspace;
    TrodesConfiguration originalWorkspace;
    ProbeLayoutPanel *plotPanel;
    QScrollBar       *plotScrollControl;
    QButtonGroup     *toolButtonGroup;
    QButtonGroup     *zoomButtonGroup;
    QLabel           *activeChannelCountIndicator;
    QComboBox        *probeNumberSelector;
    QPushButton      *copySettingsButton;

    QComboBox        *APGainSelector;
    QComboBox        *LFPGainSelector;
    QComboBox        *referenceModeSelector;

    QLineEdit        *ap_editbox;
    QLineEdit        *ml_editbox;
    QLineEdit        *dv_editbox;


    QPushButton      *closeButton;
    QPushButton      *applyButton;

    QLabel           *statusIndicator;

    int              numberOfProbes;
    QList<int>       selectionModeForEachProbe; //duplicate of same thing in panel
    QList<int>       APGainForEachProbe;
    QList<int>       LFPGainForEachProbe;
    QList<int>       refModeForEachProbe;
    QList<QVector<double> > probeLocations;

    QList<QList<ProbeLayoutPanel::selectionModeData> > allSelectionModeArgs;
    //QList<QList<QList<qreal> > > allSelectionModeArgs; //Probe->Selection tool->Args

    bool probeChanging;
    int zoomMode;
    QTimer* workspaceShutdownCheckTimer;

private slots:
    void zoomButtonPressed(int m);
    void selectionToolInChartChanged(int sMode);
    void readFromWorkspace();
    void probeSettingsChanged();
    void currentProbeChanged(int probeIndex);
    void copySettingsButtonPushed();
    void applyButtonPressed();
    void setStatus(QString status);
    void continueApplyingSettings();
    void apLocEditingFinished();
    void mlLocEditingFinished();
    void dvLocEditingFinished();



public slots:
    void updateActiveChannelCount(int ch);
    void writeToWorkspace();
    void setFocusChannel(int hw);

signals:
    void newWorkspace(const TrodesConfiguration &w);
    void closeWorkspace();
    void newNeuroPixelsSettings(NeuroPixelsSettings s);
    void requestStopAcquire();
    void closing(QString sourceName);

};

#endif // PROBELAYOUTPANEL_H

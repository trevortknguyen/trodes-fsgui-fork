#include "cargrouppanel.h"
#include "configuration.h"
//#include "globalObjects.h"

CARGroupPanel::CARGroupPanel(QWidget *parent)
    : QWidget(parent)
{
    setWindowTitle("CAR group settings");
    QVBoxLayout *mainlayout = new QVBoxLayout;
    setLayout(mainlayout);

    QSplitter *splitter = new QSplitter;
    mainlayout->addWidget(splitter,1);
    //*************************************************************
    //CAR groups listing
    //*************************************************************
    QLabel *changroupslabel = new QLabel("Groups");
    listview = new QListView();
    model = new QStringListModel;
    model->setStringList(list);
    listview->setModel(model);
    listview->setSelectionMode(QAbstractItemView::SingleSelection);
    listview->setEditTriggers(QAbstractItemView::NoEditTriggers);
    connect(listview, &QListView::pressed, this, [this](const QModelIndex &index){
        this->groupselected(index.row());
    });

    QWidget *dummy1 = new QWidget;
    QVBoxLayout *listlayout = new QVBoxLayout;
    dummy1->setLayout(listlayout);
    listlayout->addWidget(changroupslabel);
    listlayout->addWidget(listview, 0);
    splitter->addWidget(dummy1);

    QVBoxLayout *grouplayout = new QVBoxLayout;
    QWidget *dummy = new QWidget;
    dummy->setLayout(grouplayout);
    splitter->addWidget(dummy);

    QHBoxLayout *grouptitlelayout = new QHBoxLayout;
    grouplayout->addLayout(grouptitlelayout);

    currentGroup = -1;
    grouplabel = new QLabel("Group Label");
    grouptitlelayout->addWidget(grouplabel, 1);

    groupdescription = new QLineEdit("Group description here");
    groupdescription->setBackgroundRole(QPalette::Window);
    grouplayout->addWidget(groupdescription);


    editgroupbutton = new QPushButton("Edit");
    applychangesbtn = new QPushButton("Apply");
    editgroupbutton->setEnabled(false);
    applychangesbtn->setEnabled(false);
    grouptitlelayout->addWidget(editgroupbutton);
    grouptitlelayout->addWidget(applychangesbtn);

    connect(editgroupbutton, &QPushButton::released, this, &CARGroupPanel::editgrouptable);
    connect(applychangesbtn, &QPushButton::released, this, &CARGroupPanel::applychanges);

    grouptable = new QTableWidget();
    grouptable->setColumnCount(2);
    grouptable->setHorizontalHeaderLabels({"NTrode", "Channel"});
    grouptable->verticalHeader()->hide();
    grouptable->setSortingEnabled(true);
    grouptable->setRowCount(MAXCHANPERGROUP);
    grouptable->setEditTriggers(QAbstractItemView::AllEditTriggers);
    for(int i = 0; i < MAXCHANPERGROUP; ++i){
        grouptable->setItem(i, 0, new QTableWidgetItem());
        grouptable->setItem(i, 1, new QTableWidgetItem());
    }
    grouplayout->addWidget(grouptable);
    grouptable->setEnabled(false);

    //*************************************************************
    //Add/remove buttons
    //*************************************************************
    QHBoxLayout *addremovelayout = new QHBoxLayout;
    addbutton = new QPushButton("Add group");
    rmvbutton = new QPushButton("Remove group");

    QWidget *fillerwidget = new QWidget;
    addremovelayout->addWidget(addbutton, 0, Qt::AlignLeft);
    addremovelayout->addWidget(rmvbutton, 0, Qt::AlignLeft);
    addremovelayout->addWidget(fillerwidget, 1);
    mainlayout->addLayout(addremovelayout, 0);

    connect(addbutton, &QPushButton::pressed, this, &CARGroupPanel::addbuttonclicked);
    connect(rmvbutton, &QPushButton::pressed, this, &CARGroupPanel::rmvbuttonclicked);

    //*************************************************************
    //General
    //*************************************************************
    grouptable->setEnabled(false);
    groupdescription->setEnabled(false);
}

CARGroupPanel::~CARGroupPanel(){

}

void CARGroupPanel::loadGroups(SpikeConfiguration *spikeConf){
    cargroups.clear();
    cargroups = spikeConf->carGroups;

    list.clear();
    for(int i = 0; i < cargroups.length(); ++i){
        QString l = "Group " + QString::number(i+1);
        if(!cargroups[i].description.isEmpty()){
            l += " (" + cargroups[i].description + ")";
        }
        else{
            l += " (no description)";
        }
        list << l;
    }
    model->setStringList(list);
    if(cargroups.length()){
        listview->setCurrentIndex(model->index(0));
        groupselected(0);
    }
}

QVector<CARGroup> CARGroupPanel::getGroups(){
    return cargroups;
}

void CARGroupPanel::groupselected(int id){
    //clear fields
    grouptable->clear();
    grouptable->setColumnCount(2);
    grouptable->setHorizontalHeaderLabels({"NTrode", "Channel"});
    grouptable->verticalHeader()->hide();
    grouptable->setSortingEnabled(true);
    int numRows = MAXCHANPERGROUP;
    if(id >= 0 && id < cargroups.length()){
        if (cargroups[id].chans.length() > MAXCHANPERGROUP) {
            numRows = cargroups[id].chans.length();
        }
    }
    grouptable->setRowCount(numRows);
    for(int i = 0; i < numRows; ++i){
        grouptable->setItem(i, 0, new QTableWidgetItem());
        grouptable->setItem(i, 1, new QTableWidgetItem());
    }

    for(int i = 0; i < numRows; ++i){
        grouptable->item(i, 0)->setText("");
        grouptable->item(i, 0)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
        grouptable->item(i, 1)->setText("");
        grouptable->item(i, 1)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
//        grouptable->item(i, 2)->setText("");
    }
    groupdescription->setText("");
    grouplabel->setText("");
    currentGroup = id;
    grouptable->setEnabled(false);
    groupdescription->setEnabled(false);
    if(id >= 0 && id < cargroups.length()){
        //fill in fields
        QVector<CARChan> group = cargroups[id].chans;
        groupdescription->setText(cargroups[id].description);
        grouplabel->setText("Group " + QString::number(id+1));
        for(int i = 0; i < group.length(); ++i){
            grouptable->item(i,0)->setText(QString::number(group[i].ntrodeid));
            grouptable->item(i,1)->setText(QString::number(group[i].chan));
        }

        editgroupbutton->setEnabled(true);
        applychangesbtn->setEnabled(true);
    }
}


void CARGroupPanel::addbuttonclicked(){
    CARGroup group;
    cargroups.append(group);
    list.append("Group " + QString::number(cargroups.length()) +  " (no description)");
    model->setStringList(list);
    listview->setCurrentIndex(model->index(list.length()-1));
    groupselected(cargroups.length()-1);
}

void CARGroupPanel::rmvbuttonclicked(){
    int group = listview->selectionModel()->selectedRows().first().row();
    list.removeAt(group);
    model->removeRow(group);
    cargroups.remove(group);
    if(group < cargroups.length()){
        listview->setCurrentIndex(model->index(group));
        groupselected(group);
    }
    else if(cargroups.length()){
        listview->setCurrentIndex(model->index(list.length()-1));
        groupselected(cargroups.length()-1);
    }
}

void CARGroupPanel::editgrouptable(){
    grouptable->setEnabled(true);
    groupdescription->setEnabled(true);
}

void CARGroupPanel::applychanges(){
    cargroups[currentGroup].chans.clear();
//    qDebug() << "changes to group" << currentGroup;
    for(int i = 0; i < grouptable->rowCount(); ++i){
        int ntrodeid = -1, chan = -1, hwchan = -1;
        if(!grouptable->item(i,0)->text().isEmpty()){
            ntrodeid = grouptable->item(i, 0)->text().toInt();
        }
        if(!grouptable->item(i,1)->text().isEmpty()){
            chan = grouptable->item(i, 1)->text().toInt();
        }
        if(ntrodeid!= -1 && chan!=-1){
            cargroups[currentGroup].chans.append({ntrodeid, chan, hwchan});
        }
    }

    //description
    cargroups[currentGroup].description = groupdescription->text();
    list[currentGroup] = "Group " + QString::number(currentGroup+1);
    if(!groupdescription->text().isEmpty()){
        list[currentGroup] += " (" + groupdescription->text() + ")";
    }
    else{
        list[currentGroup] += " (no description)";
    }


    model->setStringList(list);
    listview->setCurrentIndex(model->index(currentGroup));
    grouptable->setEnabled(false);
    groupdescription->setEnabled(false);
}

void CARGroupPanel::hwchansReceived(QList<int> hwchans){
//    for(int i = 0; i < hwchans.length(); ++i){
//        grouptable->item(i,0)->setBackgroundColor(QColor(Qt::white));
//        grouptable->item(i,1)->setBackgroundColor(QColor(Qt::white));
////        grouptable->item(i,2)->setBackgroundColor(QColor(Qt::white));

//        //invalid hw chan
//        if(hwchans[i] == -1){
//            if(grouptable->item(i,0)->text().isEmpty() && grouptable->item(i,1)->text().isEmpty()){
//                //if text already empty, do nothing
//            }
//            else{
//                grouptable->item(i,0)->setBackgroundColor(QColor(Qt::red));
//                grouptable->item(i,1)->setBackgroundColor(QColor(Qt::red));
////                grouptable->item(i,2)->setBackgroundColor(QColor(Qt::red));
////                grouptable->item(i,2)->setText("");
//            }
//        }
//        else{
////            grouptable->item(i,2)->setText(QString::number(hwchans[i]));
//        }
//    }
//    grouptable->setEnabled(false);
//    groupdescription->setEnabled(false);
}

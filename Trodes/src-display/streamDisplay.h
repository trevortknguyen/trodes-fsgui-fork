/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STREAMDISPLAY_H
#define STREAMDISPLAY_H

#include <QtGui>
//#include <QGLWidget>
#include <QOpenGLWidget>
#include <QOpenGLShaderProgram>
#include <QOpenGLFramebufferObject>
#include <QOpenGLPaintDevice>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QOpenGLDebugLogger>
#include "configuration.h"
#include "iirFilter.h"
#include "streamProcessorThread.h"
#include "sharedVariables.h"
#include <QtWidgets>
#include "sharedtrodesstyles.h"


// StreamWidgetGL does fast draws of continuous traces
//class StreamWidgetGL : public QGLWidget {
class StreamWidgetGL : public QOpenGLWidget{

  Q_OBJECT

public:
  StreamWidgetGL(QWidget *parent, QList<int> nTrodes, bool isHeaderDisplay, StreamProcessorManager* managerPtr, int maxColCount, int pageIndex);
  ~StreamWidgetGL();

public:
  //StreamProcessor *dataObj;
  bool headerDisplay; //if true, the widget displays the header channels
  int getPageIndex();

private:

  StreamProcessorManager* streamManager;
  QList<int> dispHWChannels;
  QList<int> dispNtrodes;
  QList<int> nTrodeIndList;
  int nChan; // number of channels to display
  int maxColScale;// scaling so all headers and all lfp trace columns are scaled equally
  int currentHighlightChannel;
  QColor backgroundColor;
  int pageIndex;


  double TLength;
  double FS;

  QImage bkg;

  QVector<int> maxAmplitude;
  QVector<QColor> traceColor;

  int PSTHTriggerHeaderChannel;
  bool PSTHTriggerState;

  int settleControlChannel; //the header channel that controls auto settle command
  //int settleControlChannelDelay; //delay for settle to occute after trigger edge (in samples)
  quint8 settleControlChannelTriggerState; //0 = off, 1 = downward edge, 2 = upward edge

  QOpenGLShaderProgram *m_program;
  int m_projMatrixLoc;
  int m_mvMatrixLoc;
  int m_colorLoc;
  QMatrix4x4 m_proj;
  QMatrix4x4 m_modelView;
  QOpenGLVertexArrayObject m_vao;
  QOpenGLBuffer *m_vbo;
  GLuint m_linesBuf;
  GLuint m_rectVertBuf;
  GLuint m_rectElemBuf;


  QOpenGLDebugLogger *logger;

  bool frozen;
  vertex2d * frozenNeural;
  vertex2d * frozenAux;
//  QVector<QVector<bool> > frozenSpikeTickBins;
  bool **frozenSpikeTickBins;
  int frozenidx;

  QVector<bool> spikeModeOn;
  QVector<bool> lfpModeOn;
  QVector<bool> stimViewModeOn;
  bool invertspikes;
protected:
  void setupViewport(int width, int height);
  void resizeGL(int w, int h);
  void initializeGL();
//  void paintEvent(QPaintEvent *event);
  void paintGL();
  void mousePressEvent(QMouseEvent *);
  void wheelEvent(QWheelEvent *);

signals:

  void channelClicked(int hwchannel);
  void nTrodeClicked(int nTrodeIndex, Qt::KeyboardModifiers mods);
  void settingsMenuRequested(int hwchannel);
  void newPSTHTrigger(int headerChannel, bool state);
  void newSettleControlChannel(int headerChannel, quint8 triggerState);
  void setNewMaxDisp(int newMD);

  void sig_rightClickAction(QString actionStr);

  void auxMaxChanged();

public slots:
  //void newData();
  void setHighlightChannel(int hardwareChannel);
  void updateAxes();
  void setTLength(double T);
  void setChannels(void);
  //void setFS(double F) { FS = F; updateAxes();}
  void updateMaxDisplay(void);
  void updateTraceColor(void);
  void updateBackgroundColor();
  void stopAcquisition(void);
  void setPSTHTrigger(int headerChannel, bool state);
  void setSettleControlChannel(int headerChannel, quint8 triggerState);
  void freezeDisplay(bool frz, vertex2d *ndata, vertex2d *adata, bool **frozenspiketicks);
  void updateAllModes();
  void setInvert(bool invert);

private slots:
   void showChannelContextMenu(const QPoint& pos, int channel);
   void showNtrodeColorSelector(int channel);
   void nTrodeColorChosen(QColor c);
   void nTrodeColorSelectorCanceled();

   void showBackgroundColorSelector();
   void backgroundColorChosen(QColor c);
   void backgroundColorSelectorCanceled();
};


// StreamDisplayManager wraps StreamWidgetGL and provides controls
class StreamDisplayManager : public QWidget {

  Q_OBJECT

public:
  StreamDisplayManager(QWidget *parent, StreamProcessorManager* managerPtr);
  ~StreamDisplayManager();
  void stopAllUpdates();

  QList<QWidget*>           eegDisplayWidgets;
  QList<QList<int> >        streamDisplayChannels;
  QList<QList<int> >        nTrodeIDs;
  QList<StreamWidgetGL *>   glStreamWidgets;
  QList<int>                columnsPerPage;
  QList<bool>               isHeaderDisplayPage;
  void                      setActivePage(int);


  //Used to plot spike ticks alongside the continuous traces
  //QList<QList<uint32_t> >   spikeTickTimes;
  //QList<int>                spikeTickNTRodeIDs;



private:

  int                       currentActivePage;
  QList<QLabel*>            channelLabels;
  QList<int>                channelLabelLookup;
  QList<QGridLayout*>       eegDisplayLayout;
  StreamProcessorManager*   streamManager;
  bool                      displayFrozen;
  QTimer*                   updateTimer;
  bool                      stopped;
  QVector<vertex2d>         frozenNeural;
  QVector<vertex2d>         frozenAux;
  bool **frozenSpikeTicks;
  bool                      invertSpikes;

private slots:
  void updateAllColumns();

public slots:
  void setNTrodeLabelColor(int labelIndex, QColor newColor);
  void setNTrodeSelected(int nTrodeIndex, bool isSelected);

//  void nTrodeClicked(int nTrodeInd, Qt::KeyboardModifiers mods);
  void relayChannelClick(int hwChannel);
  void updateAudioHighlightChannel(int hwChan);
  void freezeDisplay(bool);
  void setSettleControlChannel(int headerChannel, quint8 triggerState);
  void nTrodeLabelClicked(int ntIndex, Qt::KeyboardModifiers mods);
  void nTrodeLabelRightClicked(int ntIndex);
  void setInvertSpikes(bool invert);
  void setUpdateInterval(int interval);

signals:
  void streamChannelClicked(int channel);
  void nTrodeClicked(int nTrodeInd, Qt::KeyboardModifiers mods);
  void trodeSelected(int nTrode);
  void newPSTHTrigger(int headerChannel, bool state);
  void newSettleControlChannel(int headerChannel, quint8 triggerState);
  void newSettleControlChannel(int byteInPacket, quint8 bit, quint8 triggerState);
  void setNewMaxDisp(int newMaxDisp);

  void sig_rightClickAction(QString actionStr);
  void setInvert(bool);
  void nTrode_chan_selected(int ntrode, int chan, int hwchan);
  void auxMaxChanged();
};

class AnalogInfoDialog : public QWidget{
    Q_OBJECT
public:
    AnalogInfoDialog(int headerchan);
//    ~MaxDispWin();
signals:
    void newMaxDisp(int maxdisp);
private:
//    QAbstractSpinBox *maxdispbox;
};

#endif // STREAMDISPLAY_H

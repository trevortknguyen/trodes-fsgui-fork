/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SOUNDDIALOG_H
#define SOUNDDIALOG_H

#include <QDialog>
#include <QSlider>
#include <QLabel>
#include <QtWidgets>
#include <QMenuBar>
#include "../Modules/workspaceGUI/workspaceEditor.h"
#include "sharedtrodesstyles.h"
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>
#include <QtCharts/QLogValueAxis>
#include <QBarSeries>
#include <QBarSet>
#include <QBarCategoryAxis>
#include "hardwaresettings.h"
#include "workspaceeditordialog.h"

//-----------------------------------------

//--------------------------------------

class ExportProgressDialog : public QWidget {

    Q_OBJECT

public:
    ExportProgressDialog(QString exportProgram, QStringList argumentList, QWidget *parent = 0);

private:
    QTextEdit *console;
    QPushButton *abortButton;
    QPushButton *startButton;
    QProcess* exportProcess;
    QString _exportProgram;
    QStringList _argumentList;
    QStringList _dynamicArgumentList;
    QStringList _supportedTypes;
    bool _running;
    QList<QCheckBox*> checkBoxes;

private slots:
    void readDataFromProcess();
    void programFinished(int exitCode);
    void abortButtonPushed();
    void startButtonPushed();

};


class ExportDialog : public QWidget {


Q_OBJECT

public:
    ExportDialog(QWidget *parent = 0);

    QGroupBox     *spikeBox;
    QComboBox     *triggerSelector;
    QComboBox     *noiseRemoveSelector;
    QGroupBox     *ModuleDataBox;
    QComboBox     *ModuleDataChannelSelector;
    QComboBox     *ModuleDataFilterSelector;

    QLabel        *triggerModeLabel;
    QLabel        *noiseLabel;
    QLabel        *ModuleDataChannelLabel;
    QLabel        *ModuleDataFilterLabel;

    QPushButton   *cancelButton;
    QPushButton   *exportButton;

    QProgressBar  *progressBar;
    QTimer        *progressCheckTimer;


private:

signals:

    void startExport(bool spikesOn, bool ModuleDataon, int triggerSetting, int noiseSetting, int ModuleDataChannelSetting, int ModuleDataFilterSetting);
    void exportCancelled();
    void closing();

private slots:

    void exportButtonBushed();
    void cancelButtonPushed();
    void timerExpired();
};


class soundDialog : public QWidget
{

Q_OBJECT

public:
    soundDialog(int currentGain, int currentThresh, QWidget *parent = 0);
    QSlider *gainSlider;
    QSlider *threshSlider;
    QLabel *gainDisplay;
    QLabel *threshDisplay;
    QLabel *gainTitle;
    QLabel *threshTitle;
    QComboBox *deviceCombo;

public slots:
    void closeDialog();

private:

signals:

};


class waveformGeneratorDialog : public QWidget
{

Q_OBJECT

public:
    waveformGeneratorDialog(double currentModulatorFreq, int currentFreq, int currentAmp, int currentThresh, QWidget *parent = 0);
 //   waveformGeneratorDialog(int currentFreq, int currentAmp, QWidget *parent = 0);

    QDoubleSpinBox *modulatorFreqSpinBox;
    QSlider *freqSlider;
    QSlider *ampSlider;
    QSlider *threshSlider;
    QLabel *freqDisplay;
    QLabel *ampDisplay;
    QLabel *threshDisplay;
    QLabel *modulatorFreqTitle;
    QLabel *freqTitle;
    QLabel *ampTitle;
    QLabel *threshTitle;

protected:
    void closeEvent(QCloseEvent* event);

private:

signals:
    void windowClosed(void);
};


class spikeGeneratorDialog : public QWidget
{

Q_OBJECT

public:
    spikeGeneratorDialog(double currentModulatorFreq, int currentFreq, int currentAmp, int currentThresh, QWidget *parent = 0);
 //   waveformGeneratorDialog(int currentFreq, int currentAmp, QWidget *parent = 0);

    QDoubleSpinBox *modulatorFreqSpinBox;
    QSlider *freqSlider;
    QSlider *ampSlider;
    QSlider *threshSlider;
    QLabel *freqDisplay;
    QLabel *ampDisplay;
    QLabel *threshDisplay;
    QLabel *modulatorFreqTitle;
    QLabel *freqTitle;
    QLabel *ampTitle;
    QLabel *threshTitle;

protected:
    void closeEvent(QCloseEvent* event);

private:

signals:
    void windowClosed(void);
};


//-------------------------------------------------
class CommentDialog : public QWidget {

Q_OBJECT

public:
    CommentDialog(QString fileName, QWidget *parent = 0);
    void enableCommenting(QString filename);
    void setLiveMode(bool yes);
    void setSamplingRate(int s);

private:

    QLabel* historyLabel;
    QLineEdit* newCommentEdit;

    QLabel* singleKeyModeLabel;
    QCheckBox* singleKeyModeCheckbox;


    QPushButton* saveButton;

    QLabel* commentLabel;
    //QLabel* lastComment;
    QTextEdit* lastComment;

    QString fileName;
    int samplingRate;
    bool singleKeyMode;

    void    getHistory();
    void    saveLine_internal();


private slots:

    void saveCurrentComment();
    void somethingEntered();


public slots:
    void saveLine(uint32_t timestamp, QString writer, QString contents);

protected:
    void closeEvent(QCloseEvent* event);

signals:

    void windowOpenState(bool);
    void windowClosed();

};


class HeadstageSettingsDialog : public QWidget
{

Q_OBJECT

public:
    HeadstageSettingsDialog(HeadstageSettings settings, QWidget *parent = 0);
 //   waveformGeneratorDialog(int currentFreq, int currentAmp, QWidget *parent = 0);


    QGroupBox       *autoSettleBox;
    QSlider         *percentChannelsSlider;
    QSlider         *threshSlider;
    QLabel          *percentIndicator;
    QLabel          *threshIndicator;
    QLabel          *percentTitle;
    QLabel          *threshTitle;

    QGroupBox       *smartReferenceBox;
    QCheckBox       *smartRefCheckBox;

    QGroupBox       *sensorBox;
    QCheckBox       *accelCheckBox;
    QCheckBox       *gyroCheckBox;
    QCheckBox       *magnetCheckBox;

    QGroupBox       *rfBox;
    QSpinBox        *rfChannelSpinBox;
    QCheckBox       *rfSessionIDModeBox;

    QGroupBox       *samplingRateBox;
    QComboBox       *samplingRateDropDown;


    TrodesButton    *okButton;
    TrodesButton    *cancelButton;

protected:
    void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent* event);

private:

    HeadstageSettings currentSettings;
    bool settingsChanged;

private slots:

    void percentSliderChanged(int value);
    void threshSliderChanged(int value);
    void autoSettleOnToggled(bool on);

    void smartRefToggled(bool on);
    void accelToggled(bool on);
    void gyroToggled(bool on);
    void magToggled(bool on);

    void rfChannelChanged(int value);
    void rfSessionIDModeChanged(int value);
    void samplingRateChanged(const QString &sVal);


    void okButtonPressed();
    void cancelButtonPressed();

signals:
    void windowClosed(void);
    void newSettings(HeadstageSettings settings);
};

//-----------------------------------
class ControllerSettingsDialog : public QWidget
{

Q_OBJECT

public:
    ControllerSettingsDialog(HardwareControllerSettings settings, QWidget *parent = 0);
 //   waveformGeneratorDialog(int currentFreq, int currentAmp, QWidget *parent = 0);


    QGroupBox       *rfBox;
    QSpinBox        *rfChannelSpinBox;
    QCheckBox       *rfSessionIDModeCheckBox;

    QGroupBox       *samplingRateBox;
    QSpinBox       *samplingRateSpinBox;


    TrodesButton    *okButton;
    TrodesButton    *cancelButton;

protected:
    void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent* event);

private:

    HardwareControllerSettings currentSettings;
    bool settingsChanged;

private slots:



    void rfChannelChanged(int value);
    void rfSessionIDModeChanged(int value);
    void samplingRateChanged(int value);


    void okButtonPressed();
    void cancelButtonPressed();

signals:
    void windowClosed(void);
    void newSettings(HardwareControllerSettings settings);
};
//----------------------------------
//-----------------------------------
class Abstract2DDataPlotWidget : public QWidget {

    Q_OBJECT

public:
    Abstract2DDataPlotWidget(QWidget *parent);

    void clear();
    void setXRange(qreal minR, qreal maxR);
    void setYRange(qreal minR, qreal maxR);
    void setXLabel(QString l);
    void setYLabel(QString l);
    void setTitle(QString l);
    void setNumXTicks(int t);
    void setNumYTicks(int t);

protected:

    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);

private:


    qreal maxX;
    qreal minX;
    qreal maxY;
    qreal minY;
    QVector<qreal> xTickLabels;
    QVector<qreal> yTickLabels;
    QString xLabel;
    QString yLabel;
    QString title;
    int numXTicks;
    int numYTicks;

    void calulateTickLabels();



public slots:

private slots:

signals:

};

//----------------------------------
class SignalAnalysisDialog : public QWidget {
//Used to plot the power spectral density of the active channel


Q_OBJECT

public:
    SignalAnalysisDialog(QWidget *parent = 0);



private:

    QLabel   *rangeLabel;
    QSpinBox *rangeSpinBox;

    TrodesClickableLabel  *infoLabel;

    QChart *chart;
    QChartView *chartView;
    QLineSeries* data;

    QLogValueAxis *axisX;
    QValueAxis *axisY;


    qreal binSize;
    int numBins;
    qreal absTimeRange;



    void calculateSpectrum();




private slots:

    //void setRange(int msecRange);
    void update();
    void showInfoBox();



public slots:
    void plot(QList<QPointF> points);

protected:
    void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent *event);
//    void paintEvent(QPaintEvent *event);

signals:


    void windowClosed();



};

class PlatingProtocolTable : public QTableWidget
{
    Q_OBJECT
public:
    PlatingProtocolTable(QWidget *parent = 0);
    void setEditProcessorLock(bool);
    bool isEditing();

    enum ProcessingType {Clean, Plate};
    void setProcessingType(ProcessingType t);
    QDomElement toXML(QDomDocument &doc, QString tagName);
    bool fromXML(XMLContainer *container);


public slots:

    //void setHighlightChannel(int HWChan);
    void rightClickMenuRequested(QPoint pos);
    void rightClickHeaderMenuRequested(QPoint pos);
    void addPlateStep();
    void addCleanStep();
    void addStep();
    void deleteSelectedRow();

protected:
    void keyPressEvent(QKeyEvent *event);

private:

    bool editProcessorLocked;
    bool _isEditing;
    QStringList columnHeaders;
    ProcessingType pType;

private slots:
    void processCellEdit(int row, int col);
    void processCellClicked(int row, int col);
    void processNewItemSelection();
    void copyToClipboard();
    void pasteFromClipboard();
    void pasteFromClipboard(int valueToAdd);
    void deleteSelected();
    void typeTextChanged(const QString &text);

signals:

    //void channelClicked(int HWChan);


};

//----------------------------------
class CustomChartView : public QChartView {

Q_OBJECT

public:
    CustomChartView(QChart *chart, QWidget *parent = nullptr);
    bool getLeftMouseButtonState();

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;

private:
     bool leftMouseButtonState;

signals:
     void mousePosChanged(QPointF p);
     void leftMouseButtonStateChanged(bool state, bool shift, bool cntrl, QPointF location);


};

class ChannelHistogramWidget : public QWidget {

Q_OBJECT

public:
    ChannelHistogramWidget(const TrodesConfiguration &conf, QWidget *parent = 0);
    enum SelectionMode {Interactive, Threshold, All, External};
    void getSelectedChannels(QVector<int> *nTrodeIndex, QVector<int> *channelIndex);


private:

    //Histogram plot
    bool constantYMaxOn;
    qreal constantYMax;
    QChart *chart;
    QScrollBar       *plotScrollControl;
    QString yUnits;
    CustomChartView *chartView;
    QBarSeries* data;
    qreal minXViewRange;
    qreal maxXViewRange;
    qreal xViewWidth;
    QValueAxis *axisX;
    QValueAxis *axisY;
    QList<QGraphicsRectItem*> highlightBoxes;


    //Variables
    TrodesConfiguration workspace;
    QVector<int> nTrodeIndexLookup;
    QVector<int> channelIndexLookup;
    QStringList channelNames;
    QList<qreal> barData;
    QVector<int> selectedChannels;
    QVector<int> stagedSelectedChannels;
    bool leftButtonState;
    QPointF lastMouseLoc;
    QPointF currentMouseLoc;
    SelectionMode channelSelectionMode;
    qreal threshold;


public slots:
    void plot(QList<qreal> values);
    void setYUnits(QString units);
    void setYMax(bool on, qreal value);
    void setTitle(QString title);
    void setChannelSelectionMode(SelectionMode mode);
    void setThreshold(qreal thresh);

private slots:

    //void setRange(int msecRange);
    void update();
    void mouseHoveredOverBars(bool status, int index);
    void mousePosChanged(QPointF p);
    void mouseLeftButtonStateChanged(bool state, bool shift, bool cntrl, QPointF location);
    void setViewMin(int value);
    //void activateThreshTool();
    //void activateInteractiveTool();


};

class ImpedanceDialog : public QWidget {
//Used to plot the power spectral density of the active channel


Q_OBJECT

public:
    ImpedanceDialog(const TrodesConfiguration &conf, QWidget *parent = 0);


private:

    //Status text box
    QTextEdit *statusBox;

    //Impedance controls
    QLabel      *freqLabel;
    QComboBox    *freqControl;
    QLabel      *amplitudeLabel;
    QComboBox   *notchControl;
    QPushButton *measureButton;
    QPushButton *abortButton;
    QList<qreal> impendaceValues;

    //Plating controls
    QTabWidget           *protocolTabs;
    PlatingProtocolTable *platingProtocolTable;
    PlatingProtocolTable *cleaningProtocolTable;
    QPushButton          *startPlatingButton;
    QPushButton          *addPlatingStepButton;
    QPushButton          *startCleaningButton;
    QPushButton          *deleteStepButton;
    QPushButton          *extraSettingsButton;
    QPushButton          *loadSettingsButton;
    QPushButton          *saveSettingsButton;

    //Selection controls
    QPushButton *threshSelectionButton;
    QPushButton *mouseSelectionButton;
    QSpinBox    *selectionThreshSpinBox;
    QPushButton *fileSelectionButton;
    QPushButton *allSelectionButton;

    //Histogram plot
    ChannelHistogramWidget *histChart;

    //Data and variables
    TrodesConfiguration workspace;
    QStringList channelNames;
    QList<int> queuedChannelsForImpedanceTest;
    QTimer impTimer;

    //Methods
    bool loadSettings(QString filename);
    bool saveSettings(QString filename);

private slots:

    void showInfoBox();
    void activateThreshTool();
    void activateInteractiveTool();
    void activateFileTool();
    void activateAllTool();
    void threshValueChanged(int t);
    void addStepButtonPushed();
    void deleteStepButtonPushed();
    void loadButtonPushed();
    void saveButtonPushed();
    void measureImpedanceButtonPressed();
    void abortMeasureImpedanceButtonPressed();
    void requestNextImpedanceMeasure();


public slots:

    void setWorkspace(const TrodesConfiguration & conf);
    void newImpedanceValue(int HWChan, int value);

protected:
    void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent *event);


signals:

    void windowClosed();
    void impedanceMeasureRequested(HardwareImpedanceMeasureCommand s);

};



//-----------------------------------
class RasterPlot : public QWidget {

    Q_OBJECT

public:
    RasterPlot(QWidget *parent);

    void addRaster(const QVector<qreal> &times);
    void setRasters(const QVector<QVector<qreal> > &times);
    void clearRasters();
    void setXRange(qreal minR, qreal maxR);
    void setXLabel(QString l);
    void setYLabel(QString l);

protected:

    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);

private:


    qreal maxX;
    qreal minX;
    QVector<QVector<qreal> > eventTimes;
    QVector<qreal> xTickLabels;
    QVector<qreal> yTickLabels;
    QString xLabel;
    QString yLabel;


public slots:

private slots:

signals:

};
//-----------------------------------
class HistogramPlot : public QWidget {

    Q_OBJECT

public:
    HistogramPlot(QWidget *parent);

    void setXRange(qreal minR, qreal maxR);
    void setXLabel(QString l);
    void setYLabel(QString l);
    void setColor(QColor c);
    void setData(QVector<qreal> bvalues);
    void setErrorBars(QVector<QVector<qreal> > hilowErrValues);
    void setChecked(bool errBar[4]);
    void setBinValues(qreal bin1Start,
                      qreal bin1Size,
                      qreal bin2Start,
                      qreal bin2Size);
    void clearData();
    void clickingOn(bool on, int cl);

protected:

    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);

    //void paint()

//    void mouseReleaseEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    /*
    void mouseDoubleClickEvent(QMouseEvent *event);
    void leaveEvent(QEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void wheelEvent(QWheelEvent *);
    */

private:

//    QPainter painter;
//    QPen pen;
    int margin;
    int topOfGraph;

    qreal maxX;
    qreal minX;
    qreal maxY;
    qreal maxYDisplay;
    QVector<qreal> barValues;
    QColor barColor;
    QVector<QVector<qreal> > hiloerrorBarValues;
    bool errBar[4];
    QVector<qreal> xTickLabels;
    QVector<qreal> yTickLabels;
    QString xLabel;
    QString yLabel;
    qreal bin1Start;
    qreal bin1Size;
    qreal bin2Start;
    qreal bin2Size;
    QRectF *bin1;
    QRectF *bin2;
    bool errorsIndicator;
    bool binsIndicator;
    bool clicking;
    int cluster;

public slots:

private slots:

signals:
    void PSTHRequest(int);
    void highlightBorder(int);
};

class PSTHDialog : public QWidget {

Q_OBJECT

public:
    PSTHDialog(QWidget *parent = 0);
    void plot(const QVector<uint32_t> &trialTimes, const QVector<uint32_t> &eventTimes);


private:

    QLabel   *rangeLabel;
    QLabel   *binsLabel;
    QLabel   *binOneStartLabel;
    QLabel   *binOneSizeLabel;
    QLabel   *binTwoStartLabel;
    QLabel   *binTwoSizeLabel;
    QLabel   *minLabel1;
    QLabel   *maxLabel1;
    QLabel   *medLabel1;
    QLabel   *meanLabel1;
    QLabel   *numLabel1;
    QLabel   *minLabel2;
    QLabel   *maxLabel2;
    QLabel   *medLabel2;
    QLabel   *meanLabel2;
    QLabel   *numLabel2;

    QSpinBox *rangeSpinBox;
    QSpinBox *numBinsSpinBox;
    QSpinBox *binOneStartBox;
    QSpinBox *binOneSizeBox;
    QSpinBox *binTwoStartBox;
    QSpinBox *binTwoSizeBox;
//    QCheckBox *SDCheckbox;
    QCheckBox *SECheckbox;
//    QCheckBox *RCheckbox;
//    QCheckBox *CICheckbox;
    QTextEdit *binSumStatsText;
    QGridLayout *binSumStatsGrid;

    QGridLayout *controlLayout;
    HistogramPlot *window;
    RasterPlot *rasterWindow;

    qreal binSize;
    int numBins;
    qreal absTimeRange;
    QVector<int> counts;
    int numTrials;
    QVector<uint32_t> trialTimes;
    QVector<uint32_t> eventTimes;
    bool checked[4];

    qreal binOneStart, binOneSize, binTwoStart, binTwoSize;
    QMap<QString, qreal> binOneSumStats, binTwoSumStats;

    void setUpControlPanel();
    QString generateSumStatsText();
    void generateSumStatsLabel();
    void calcDisplay();
    void calcSumStats(QVector<int> data, int bin);
    QVector<QVector<qreal> > calcErrorBars(QVector<QVector<int> > data, qreal pct);
    QVector<QVector<qreal> > stdDev(QVector<QVector<int> > data);
    QVector<QVector<qreal> > stdError(QVector<QVector<int> > data);
    QVector<QVector<qreal> > range(QVector<QVector<int> > data);
    QVector<QVector<qreal> > confInt(QVector<QVector<int> > data, qreal pct);



private slots:
    void setNumBins(int nBins);
    void setRange(int msecRange);
    void setSDChecked(int c);
    void setSEChecked(int c);
    void setRChecked(int c);
    void setCIChecked(int c);
    void setBinOneStart(int num);
    void setBinOneSize(int num);
    void setBinTwoStart(int num);
    void setBinTwoSize(int num);


public slots:


protected:
    //void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent *event);
//    void paintEvent(QPaintEvent *event);

signals:

    void windowOpenState(bool);
    void windowClosed();
    void binsChanged(int);
    void rangeChanged(int);

};

class MultiPlotDialog : public QWidget {

Q_OBJECT

public:
    MultiPlotDialog(QWidget *parent = 0, QColor c = QColor(100,100,100));
    void plot(const QVector<uint32_t> &trialTimesIn,
              const QVector<uint32_t> &eventTimesIn,
              const QVector<QVector<uint32_t> > &clusterEventTimesIn);
    void setupClusterHistograms();

private:

    HistogramPlot *topHistogram;
    QGridLayout   *clusterPlotGrid;
    QLabel        *topPlotLabel;
    QLabel        *botPlotLabel;

    QPushButton   *updateButton;

    QLabel        *rangeLabel;
    QLabel        *binsLabel;
    QLabel        *numPlotsLabel;
    QSpinBox      *rangeSpinBox;
    QSpinBox      *numBinsSpinBox;

    QColor        plotColor;

    QMap<int, HistogramPlot*> clusterPlots;

    QVector<uint32_t> trialTimes;
    QVector<uint32_t> eventTimes;
    QVector<QVector<uint32_t> > clusterEventTimes;
    int currClInd;
    int currentNTrode;

    int plotsPerPage;
    int gridRows;
    int gridCols;
    int currPage;
    int numBins;
    int numTrials;
    qreal binSize;
    qreal absTimeRange;

    bool newNTrode;

    void calcTopDisplay();
    void calcClusterHistogramDisplay(int clusterInd);

private slots:
    void setNumBins(int nBins);
    void setRange(int msecRange);
    void getPSTH(int cl);
    void displayClusterGrid();
//    void organizeClusterGrid();
    void setupGridSize(QString numplots);


public slots:
    void update(int, QVector<uint32_t>, QVector<uint32_t> , QVector<QVector<uint32_t> >);

protected:
    //void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent *event);
//    void paintEvent(QPaintEvent *event);

signals:

    void windowOpenState(bool);
    void windowClosed();
    void requestPSTH(int, int);
    void binsChanged(int);
    void rangeChanged(int);

};


enum FilterOperation {FO_NULL, FO_OR, FO_AND};

class NTrodeSelectionDialog : public QWidget {
    Q_OBJECT
public:
    NTrodeSelectionDialog(QWidget *parent = 0);

public slots:
    void update(void); //updates categories and tags

private:
    QHash<GroupingTag, int> selectedTags;
    FilterOperation operation;


    QVBoxLayout *mainLayout;

    QLabel      *labelCategory;
    QLabel      *labelTags;
    QLabel      *labelOperation;

    QComboBox   *categorySelector;
    QComboBox   *operationSelector;

    QCheckBox   *selectAllTags;
    QListWidget *tagList;

    QPushButton *buttonClear;

private slots:
    void categorySelected(int categoryIndex);
    void allTagsButtonSelected(int state);
    void tagSelected(QListWidgetItem *tag);
    void operationSelected(int opIndex);

    void buttonClearPressed(void);

signals:
    void sig_selectTags(int op, QHash<GroupingTag,int> sTags);
    void sig_clearSelection(void);

};

class StreamDisplayOptionsDialog : public QWidget {
    Q_OBJECT

public:
    StreamDisplayOptionsDialog(bool lfp, bool spike, bool raw, bool stim, QWidget *parent = 0);

private:
    QVBoxLayout     *mainLayout;

    TrodesButton    *lfpbutton;
    TrodesButton    *spikebutton;
    TrodesButton    *rawbutton;
    TrodesButton    *stimbutton;
    TrodesButton    *displayTicksButton;
signals:
    void lfpChosen();
    void spikeChosen();
    void rawChosen();
    void stimChosen();
    void ticksToggled(bool on);
private slots:
    void lfpChosenslot();
    void spikeChosenslot();
    void rawChosenslot();
    void stimChosenslot();
    void ticksToggledSlot(bool on);
};



//TriggerScopeSettingsWidget contains the settings controls for each nTrode
class  NTrodeSettingsWidget : public QWidget
{
    Q_OBJECT
    void newParent() {
        if (!parent()) return;
        qDebug() << "Installing Event Filter";
        parent()->installEventFilter(this);
        raise();
    }
public:
    NTrodeSettingsWidget(QWidget *parent, int trodeNum);
    ~NTrodeSettingsWidget();

public:

    int nTrodeNumber;
    QList<QGridLayout*>  widgetLayouts;
    QList<QLabel*> labels;
    QList<int> selectedIndicis;

    QLabel              *emptyLabel;

    QLabel              *trodeLabel;
    QCheckBox           *linkCheckBox;
    QCheckBox           *spikeRefCheckBox;
    QCheckBox           *lfpRefCheckBox;
    QCheckBox           *rawRefCheckBox;
    QGroupBox           *refBox;
//    QGroupBox           *notchFilterBox;
    QGroupBox           *filterBox;
    QGroupBox           *triggerBox;
    QGroupBox           *ModuleDataFilterBox;
    QGroupBox           *groupingTagBox;
    QGroupBox           *displayBox;
    QListWidget         *groupingTagList; //all grouping tags for the currentNtrode
    QListWidget         *groupingTagSharedList; //shared tags among selected nTrodes
    QListWidget         *groupingTagAllList; //All selected ntrodes tags
    QTabWidget          *groupingTabView;

    TrodesButton        *buttonAddRemoveTags;
    QHBoxLayout         *layoutSelectButtons;
    TrodesButton        *buttonSelectBy;
    TrodesButton        *buttonSelectAll;

    TagGroupingPanel    *groupingDialog;
    QSpinBox            *threshSpinBox;
    QSpinBox            *maxDispSpinBox;
    QComboBox           *RefTrodeBox;
    QComboBox           *RefChannelBox;
    QLabel              *cargrouplabel;
    QComboBox           *RefGroupBox;
    TrodesButton        *addCARGroupButton;

//    QComboBox           *notchFrequencyBox;
//    QComboBox           *notchBandwidthBox;
    QComboBox           *lowFilterCutoffBox;
    QComboBox           *highFilterCutoffBox;
    QComboBox           *ModuleDataHighFilterCutoffBox;
    QComboBox           *ModuleDataChannelBox;
    ClickableFrame      *buttonColorBox;

//    CARGroupPanel       *cargrouppanel;
    void setSelectedNTrodes(QHash<int,int> selectedNTrodes);

protected:
    bool eventFilter(QObject *obj, QEvent *ev);
    bool event(QEvent *ev);

private:
    bool attachedMode; //set whether or not the window is attached to it's parent window.  This enables/disables animations

    QWidget *attachedWidget;
    QPoint posOffset;
    QSize panelSize;
    bool multipleSelected;

    QHash<GroupingTag, int> allTags; //all tags shared by the currently selected nTrodes

    QList<QWidget*> widgets; //list of all widgets, for the purpose of hiding and revealing everything
    QPropertyAnimation *a_horizontalExpand;
    QPropertyAnimation *a_horizontalClose;
    QPropertyAnimation *a_minSize;

    int LOWFILTER_LABEL_INDEX;
    int HIGHFILTER_LABEL_INDEX;
    int REFNTRODE_LABEL_INDEX;
    int REFCHAN_LABEL_INDEX;
    int MODDATACHAN_LABEL_INDEX;
    int MODDATAHIGH_LABEL_INDEX;
    int THRESH_LABEL_INDEX;
    int MAXDISP_LABEL_INDEX;
    int COLOR_LABEL_INDEX;
    int NOTCHFREQ_LABEL_INDEX;
    int NOTCHBW_LABEL_INDEX;

    void iniReferenceBox(int columnPos, TrodesFont font);
    void iniSpikeFilterBox(int columnPos, TrodesFont font);
    void iniSpikeTriggerBox(int columnPos, TrodesFont font);
    void iniLFPBox(int columnPos, TrodesFont font);
//    void iniNotchFilterBox(int columnPos, TrodesFont font);
    void iniGroupingTagControls(int columnPos, TrodesFont font);
    void iniDispBox(int columnPos, TrodesFont font);

signals:
    void updateAudioSettings();
    void changeAllRefs(int nTrode, int channel);
    void changeAllFilters(int low, int high);
    void changeAllThresholds(int thresh);
    void changeAllMaxDisp(int maxDisp);
    void toggleAllRefs(bool on);
    void toggleAllFilters(bool on);
    void toggleAllTriggers(bool on);
//    void toggleLinkChanges(bool link);
    void moduleDataChannelChanged(int nTrode, int chan);
    void configChanged();
    void closing(void);
    void saveGeo(QRect geo);
    void openSelectByDialog(void); //open select ntrode dialog
    void selectAllNtrodes();

    //value changed signals
    void sig_threshUpdated(int newThresh);
    void sig_maxDisplayUpdated(int newMaxDisp);

    void carPanelReviewed(QList<int> hwchans);
public slots:
    void attachToWidget(QWidget *obj);

    void loadNTrodeIntoPanel(int nTrodeID);
    void loadRef(SingleSpikeTrodeConf *nTrode);
//    void loadNotchFilter(SingleSpikeTrodeConf *nTrode);
    void loadSpikeFilter(SingleSpikeTrodeConf *nTrode);
    void loadSpikeTrigger(SingleSpikeTrodeConf *nTrode);
    void loadLFP(SingleSpikeTrodeConf *nTrode);
    void loadTags(SingleSpikeTrodeConf *nTrode);
    void loadDisplaySettings(SingleSpikeTrodeConf *nTrode);

    //Public Accessor functions
    void setMaxDisplay(int newMaxDisp);
    void setThresh(int newThresh);

    void setChanColorBox(QColor newColor);

    //Save to config and update the gui element
    void updateRefTrode();
    void updateRefTrode(int);
    void updateRefChan();
    void updateRefChan(int);
    void updateRefGroup();
    void updateRefGroupId(int id);
    void validateSelectedRef();
    void updateRefSwitch(bool on);
    void updateRawRefOn(bool on);
//    void updateNotchFilterSwitch(bool on);
//    void updateNotchFilterFreq();
//    void updateNotchFilterBW();
    void updateLFPFilterSwitch(bool on);
    void updateFilterSwitch(bool on);
    void updateLowerFilter();
    void updateUpperFilter();
    void updateModuleDataFilterSwitch(bool on);
    void updateModuleDataChannel();
    void updateModuleDataUpperFilter();
    void updateSpikeTrigger(bool on);
    void updateThresh();
    void updateMaxDisp();
    void updateChanColor();
    void updateTags(void);

    void addRemoveTagsButtonPressed(void);
    void addCategoryToDict(QString nCata);
    void addTagToDict(QString tCata, QString nTag);

//    void linkBoxClicked(bool checked);
    void setEnabledForStreaming(bool streamOn);  // disables everything if data streaming is on

    bool checkNTrodesForConflicts(void);
    bool checkSelectedNTrodesForConflicts(SingleSpikeTrodeConf *checkedNTrode);

    void resetAllLabelsToDefault(void);
    void setAllLabelColors(QColor newColor);

    void setPanelDim(int width, int height);
    void setPositionOffset(int xOffset, int yOffset); //sets the x and y offsets of the panel with relation to which widget it's attached to
    void setPosition(int xpos, int ypos);

    //animation stuff
    void setVisible(bool visible);

    void runAnimation_Expand(int time = 0, bool expandRight = true);
    void runAnimation_Close(int time = 0, bool closeFromRight = true);

    void streamingStarted();
    void streamingStopped();
private slots:
    void getColorDialog(void);
    void addCARGroupButtonPressed();

    void setWidgetsVisible(void);
    void setWidgetsHidden(void);
    void setGroupingTagWidgetsVisibility(void);
    void verifyCARgroupEdits(int groupid, QList<int> ntrodes, QList<int> chans, QString description);
};

class PreferencesPanel : public QWidget {
    Q_OBJECT
public:
    PreferencesPanel(QWidget *parent = nullptr);
    ~PreferencesPanel();

    void saveSettings();
    bool createSubDirectory() const;
    bool invertSpikes() const;

public slots:

signals:
    void invertSpikesSet(bool);
private:
    QByteArray geo;
    QCheckBox *createSubDirectoryBox;
    QCheckBox *invertSpikesBox;
    QByteArray exportSettings() const;
    void importSettings(const QByteArray &settings);
protected:
    void showEvent(QShowEvent *event);
    void closeEvent(QCloseEvent *event);
};

class NetworkPanel : public QWidget{
    Q_OBJECT
public:
    NetworkPanel(QWidget *parent = nullptr);
    ~NetworkPanel();

public slots:
    void setAddressAndPort(QString address, int port);
    void updateClients(QVector<QString> clients);
    void clientConnected(QString client);
    void clientClosed(QString client);
    void clientCrashed(QString client);
signals:
    void sendQuit(QString module);

private:
    QLabel *header;
    QVector<QString> clients;
    QByteArray geo;
    QGridLayout *clientsLayout;
    void addNewClient(QString client, int row);
};

#endif // SOUNDDIALOG_H

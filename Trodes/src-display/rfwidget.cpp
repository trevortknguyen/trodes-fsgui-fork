#include "rfwidget.h"
RFWidget::RFWidget(QWidget *parent)
    : QWidget(parent),
      name(""),
      description(""),
      connection(""),
      serial(0),
      rfchan(0),
      samplingRateKhz(0),
      status("IDLE"),
      logger_model(0),
      logger_serial(0),
      logger_status("DISCONNECTED"),
      logger_numchan(0),
      logger_samplingRateKhz(0),
      logger_sampleSizeBits(0),
      logger_magOn(0),
      logger_gyrOn(0),
      logger_accOn(0),
      logger_filtOn(0),
      logger_sd_blocks_written(0)
{
    QGridLayout *infolayout = new QGridLayout;

    dockInfo = new QLabel;
    infolayout->addWidget(dockInfo, 0, 0, Qt::AlignLeft);
    liveStreamInfo = new QLabel;
    infolayout->addWidget(liveStreamInfo, 1, 0, Qt::AlignLeft);
    loggerInfo = new QLabel;
    infolayout->addWidget(loggerInfo, 0, 1, 2, 1, Qt::AlignLeft);

    setLayout(infolayout);

    logger_status = "DISCONNECTED";
}

/*void RFWidget::dockConnected(QString name, QString connection, DockingStation_t settings)
{
    this->name = name;
    this->description = settings.description;
    this->connection = connection;
    this->serial = settings.serial;
    this->rfchan = settings.rfchan;
    this->samplingRateKhz = settings.samplingRateKhz;
    updateDeviceInfo();

    connect(&updateTimer, &QTimer::timeout, this, &RFWidget::updateDeviceInfo);
    updateTimer.start(1000);
}*/

void RFWidget::dockDisconnected(uint16_t serial, QString connection)
{
    this->name = "";
    this->description = "";
    this->connection = "";
    this->serial = 0;
    this->rfchan = 0;
    this->samplingRateKhz = 0;
    updateDeviceInfo();
}

void RFWidget::idlePayloadReceived(int dockid, QByteArray bytes){
    uint8_t config = bytes[bytes.length()-1];
//    for(int i = 0; i < bytes.length(); ++i)printf("%02x ", (uint8_t)bytes.data()[i]); printf("\n");
    logger_serial = *(uint16_t*)(bytes.data()+2);
    logger_numchan = *(uint8_t*)(bytes.data()+4);
    logger_sampleSizeBits = (bool)(config&0x20) ? 12 : 16;
    logger_samplingRateKhz = (bool)(config&0x10)? 20 : 30;
    logger_filtOn = (bool)(config&0x08);
    logger_magOn = (bool)(config&0x04);
    logger_gyrOn = (bool)(config&0x02);
    logger_accOn = (bool)(config&0x01);

    logger_status = "CONNECTED, IDLE";

    lastupdate.restart();
    updateDeviceInfo();
}

void RFWidget::streamPayloadReceived(int dockid, QByteArray bytes){
    unsigned char *data = (unsigned char*)bytes.data();
//    for(int i=0;i<bytes.length();++i) printf("%02X ", data[i]); printf("\n");
    uint32_t blocks = (data[0] << 24) | (data[1] << 16) | (data[2] << 8) | (data[3]);
    uint16_t battery = (data[4] << 8) | (data[5]);
    logger_sd_blocks_written = blocks;

    lastupdate.restart();
    updateDeviceInfo();
}

void RFWidget::updateDeviceInfo()
{
    dockInfo->setText(QString(  "<h3>%1 (Serial # %2)</h3>"
                                "<table>"
                                "<tr><td>Device Status: </td><td> %3</td></tr>"
                                "<tr><td>RF channel: </td><td> %4</td></tr>"
                                "<tr><td>Sampling rate: </td><td> %5khz</td></tr>"
                                "</table>"
                                "<hr/>")
                        .arg(name)
                        .arg(serial)
                        .arg(status)
                        .arg(rfchan)
                        .arg(samplingRateKhz)
                      );

    loggerInfo->setText(QString("<h3>Logger (Serial # %1)</h3>"
                                "<table>"
                                "<tr><td>Status: </td><td>%2</td></tr>"
                                "<tr><td>Last wireless update: </td><td>%3 sec ago</td></tr>"
                                "<tr><td># Channels: </td><td>%4</td></tr>"
                                "<tr><td>Sampling rate: </td><td>%5khz</td></tr>"
                                "<tr><td>Sample size: </td><td>%6 bits</td></tr>"
                                "<tr><td>Magnetometer: </td><td>%7</td></tr>"
                                "<tr><td>Gyroscope: </td><td>%8</td></tr>"
                                "<tr><td>Accelerometer: </td><td>%9</td></tr>"
                                "<tr><td>Smart filter: </td><td>%10</td></tr>"
                                "</table>"
                                "<hr/>")
                        .arg(logger_serial)
                        .arg(logger_status)
                        .arg(lastupdate.isValid() ? lastupdate.elapsed()/1000 : 0)
                        .arg(logger_numchan)
                        .arg(logger_samplingRateKhz)
                        .arg(logger_sampleSizeBits)
                        .arg(logger_magOn?"On":"Off")
                        .arg(logger_gyrOn?"On":"Off")
                        .arg(logger_accOn?"On":"Off")
                        .arg(logger_filtOn?"On":"Off")
                        );
    liveStreamInfo->setText(QString("<h3>Logger streaming status</h3>"
                                    "<table>"
                                    "<tr><td>SD Progress: %1MB recorded</td></tr>"
                                    "<tr><td>Battery status: Unavailable</td></tr>"
                                    "</table>"
                                    "<hr/>")
                            .arg((logger_sd_blocks_written/2)/1024)
                            );
}

/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "spikeDisplay.h"
#include "streamDisplay.h"
#include "globalObjects.h"
#include <QtConcurrent>


#include <TrodesNetworkUtil.h>
#include <TrodesNetwork/Generated/SpikePacket.h>
#include <TrodesNetwork/Generated/TrodesSpikeWaveformData.h>
#include <TrodesNetwork/Util.h> // for get_timestamp


//Q_DECLARE_METATYPE(QList<int>)
//------------------------------------------------------------------
RubberBandPolygonNode::RubberBandPolygonNode(int nodeNum, QGraphicsItem *parent):
    QGraphicsRectItem(parent),
    nodeNum(nodeNum) {

    //This object is a movable node in a user-drawn polygon. It is a child
    //of a RubberBandPolygon

    dragging = false;
    color = QColor(255,255,255);

}

void RubberBandPolygonNode::setColor(QColor c) {
    color = c;
}


void RubberBandPolygonNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {

     //Set composition mode to be the inverse of the background
    //painter->setCompositionMode(QPainter::CompositionMode_Difference);
    //painter->setCompositionMode(QPainter::RasterOp_SourceAndNotDestination);

    painter->setOpacity(1);

    QPen pen(color,1);
    painter->setPen(pen);
    painter->setBrush(Qt::NoBrush);
    painter->drawRect(rect());
}

void RubberBandPolygonNode::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    //The node is being dragged
    QGraphicsItem::mouseMoveEvent(event);
    dragging = true;
    emit nodeMoved(nodeNum);
}

void RubberBandPolygonNode::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    QGraphicsItem::mouseReleaseEvent(event);
    if (dragging) {
        emit nodeMoveFinished();
    }
    dragging = false;
}
//----------------------------------------------------------
RubberBandPolygon::RubberBandPolygon(bool showing, QGraphicsItem *parent)
    : QGraphicsPolygonItem(parent),
      clusterShowing(showing)
{

    //This is a user-drawn polygon to define ROI's
    dragging = false;
    clusterNum = 0;
    setFlag(QGraphicsItem::ItemIsMovable,true);
    //setFlag(QGraphicsItem::ItemIsSelectable,true);

    //Default axes
    xAxis = -1;
    yAxis = -1;

    color = QColor(255,255,255);
}

RubberBandPolygon::~RubberBandPolygon() {

    //Not sure if this is actually needed
    while (points.length() > 0) {
        removeLastPoint();
    }
}

void RubberBandPolygon::setAxes(int x, int y) {
    xAxis = x;
    yAxis = y;
}

RubberBandPolygon::Axes RubberBandPolygon::getAxes() {
    RubberBandPolygon::Axes a;
    a.x = xAxis;
    a.y = yAxis;
    return a;
}

void RubberBandPolygon::setClusterNum(int cNum) {
    assignedCluster = cNum;
    clusterNum = clusterShowing*cNum;
}

void RubberBandPolygon::setPolyBitInd(int bitInd) {
    polyBitInd = bitInd;
}

int RubberBandPolygon::getPolyBitInd() {
    return polyBitInd;
}

void RubberBandPolygon::setColor(QColor c) {
    //setBrush(c);
    color = c;
    for (int i=0;i<nodes.length();i++) {
        nodes[i]->setColor(color);
    }
}

void RubberBandPolygon::setPoints(QVector<QPoint> amppoints) {

    //Clear any existing data for the polygon
    amplitudeSpacePoints.clear();
    relativePoints.clear();
    points.clear();
    while (nodes.length()>0) {
        delete nodes.takeLast();
    }

    setPos(0,0);

    //Loop for each point in the polygon
    for (int i=0;i<amppoints.length();i++) {

        QPointF tmpPoint;

        //Add to amplitude space points
        tmpPoint.setX((qreal) amppoints[i].x());
        tmpPoint.setY((qreal) amppoints[i].y());
        amplitudeSpacePoints.append(tmpPoint);

        //Add to relative space points (1 = edge of window)
        QPointF newRelativePoint;
        newRelativePoint.setX((tmpPoint.x()-ampWindow.left())/ampWindow.width());
        newRelativePoint.setY(1-((tmpPoint.y()-ampWindow.top())/ampWindow.height()));
        relativePoints.append(newRelativePoint);

        //Add to pixel space points
        tmpPoint.setX(relativePoints.last().x()*this->scene()->width());
        tmpPoint.setY(relativePoints.last().y()*this->scene()->height());
        points.append(tmpPoint);

        //We also create a node that can be dragged by the user.
        RubberBandPolygonNode *tmpNode = new RubberBandPolygonNode(points.length()-1,this);
        nodes.append(tmpNode);
        tmpNode->setRect(0,0,6,6);
        tmpNode->setPos(points.last().x()-3,points.last().y()-3);
        tmpNode->setFlag(QGraphicsItem::ItemIsMovable,true);
        tmpNode->setColor(color);
        tmpNode->setVisible(false);
        connect(tmpNode,SIGNAL(nodeMoved(int)),this,SLOT(childNodeMoved(int)));
        connect(tmpNode,SIGNAL(nodeMoveFinished()),this,SLOT(childNodeMoved()));
    }

    setPolygon(QPolygonF(points));

}

QVector<QPoint> RubberBandPolygon::getPoints() {
    //Returns the points in the polygon in amplitude space
    //(integer format, scaled to fit 16-bit integer range)
    QVector<QPoint> polyPoints;
    for (int i=0;i<amplitudeSpacePoints.length();i++) {
        QPoint tmpPoint;
        tmpPoint.setX((int) amplitudeSpacePoints[i].x());
        tmpPoint.setY((int) amplitudeSpacePoints[i].y());
        polyPoints.append(tmpPoint);
    }
    return polyPoints;
}

int RubberBandPolygon::getAssignedClusterNum() {

    //Assigned cluster number, 1-based index
    return assignedCluster;
}

int RubberBandPolygon::getClusterNum(){
    //{0, assignedCluster}. Either this cluster is enabled (assignedCluster), or disabled (0).
    return clusterNum;
}

void RubberBandPolygon::removeLastPoint() {
    if (points.length()>0) {
        points.removeLast();
        relativePoints.removeLast();
        amplitudeSpacePoints.removeLast();
        setPolygon(QPolygonF(points));
        delete nodes.takeLast();

    }
}

void RubberBandPolygon::moveLastPoint(QPointF newLoc) {
    //move the last point in the polygon to the new location
    //Used as the polygon is being drawn by the user
    if (points.length()>0) {
        points.last().setX(newLoc.x());
        points.last().setY(newLoc.y());
        QPointF newRelativePoint;
        newRelativePoint.setX(newLoc.x()/this->scene()->width());
        newRelativePoint.setY(newLoc.y()/this->scene()->height());
        relativePoints.last().setX(newRelativePoint.x());
        relativePoints.last().setY(newRelativePoint.y());

        amplitudeSpacePoints.last().setX(relativePoints.last().x()*ampWindow.width() + ampWindow.left());

        //Because the polygon drawing origin is at the top-left, we need to convert to an origin that is bottom-left
        amplitudeSpacePoints.last().setY((1-relativePoints.last().y())*ampWindow.height() + ampWindow.top());


        setPolygon(QPolygonF(points));
        nodes.last()->setPos(newLoc.x()-3,newLoc.y()-3);
    }
}

void RubberBandPolygon::setCurrentAmpWindow(QRectF win) {
    ampWindow = win; //(origin at top left)

    //We are either translating or zooming/out.  To calculate
    //polygon position on the screen, we convert from amplitude
    //space to pixel space.
    QPointF tmpPoint;
    for (int i=0; i < points.length(); i++) {

        //Calculate the new relative location (between 0 and 1 on the screen)
        relativePoints[i].setX((amplitudeSpacePoints[i].x()-ampWindow.left())/ampWindow.width());

        //Because the polygon drawing origin is at the top-left, we need to convert to an origin that is bottom-left
        relativePoints[i].setY(1-((amplitudeSpacePoints[i].y()-ampWindow.top())/ampWindow.height()));

        //From the relative location, calculate the x and y pixels
        tmpPoint.setX(relativePoints[i].x()*this->scene()->width());
        tmpPoint.setY(relativePoints[i].y()*this->scene()->height());

        //map from the scene coordinate system to the polygon's
        tmpPoint = mapFromScene(tmpPoint);

        points[i].setX(tmpPoint.x());
        points[i].setY(tmpPoint.y());
        nodes[i]->setX(tmpPoint.x()-3);
        nodes[i]->setY(tmpPoint.y()-3);

    }

    setPolygon(QPolygonF(points));
}

void RubberBandPolygon::addPoint(QPointF newPoint) {
    //Add a new point to the polygon

    points.append(newPoint);
    //We need to calculate the point's relative location on
    //the drawing area (between 0 and 1 for both x and y
    //dimensions. This is important for resizing and for
    //calculating what data falls inside the polygon.


    QPointF newRelativePoint;
    newRelativePoint.setX(newPoint.x()/this->scene()->width());
    newRelativePoint.setY(newPoint.y()/this->scene()->height());
    relativePoints.append(newRelativePoint);

    QPointF pointInAmpSpace;
    pointInAmpSpace.setX(newRelativePoint.x()*ampWindow.width() + ampWindow.left());

    //Because the polygon drawing origin is at the top-left, we need to convert to an origin that is bottom-left
    pointInAmpSpace.setY((1-newRelativePoint.y())*ampWindow.height() + ampWindow.top());
    amplitudeSpacePoints.append(pointInAmpSpace);

    setPolygon(QPolygonF(points));

    //We also create a node that can be dragged by the user.
    RubberBandPolygonNode *tmpNode = new RubberBandPolygonNode(points.length()-1,this);
    nodes.append(tmpNode);
    tmpNode->setRect(0,0,6,6);
    tmpNode->setPos(newPoint.x()-3,newPoint.y()-3);
    tmpNode->setFlag(QGraphicsItem::ItemIsMovable,true);
    tmpNode->setColor(color);
    tmpNode->setVisible(false);
    connect(tmpNode,SIGNAL(nodeMoved(int)),this,SLOT(childNodeMoved(int)));
    connect(tmpNode,SIGNAL(nodeMoveFinished()),this,SLOT(childNodeMoved()));

}

void RubberBandPolygon::childNodeMoved() {
    //this is called after any of the nodes has finished moving
    //emit shapeChanged();
    emit shapeChanged(clusterNum, polyBitInd);
}

void RubberBandPolygon::childNodeMoved(int nodeNum) {

    //this is called as the node is moving-- update polygon shape

    points[nodeNum].setX(nodes[nodeNum]->x()+3);
    points[nodeNum].setY(nodes[nodeNum]->y()+3);
    prepareGeometryChange();
    setPolygon(QPolygonF(points));

    QPolygonF scenePoly = mapToScene(polygon());
    //Also, recalculate the new relative locations
    for (int i=0; i < points.length(); i++) {
        relativePoints[i].setX((scenePoly[i].x()/this->scene()->width()));
        relativePoints[i].setY((scenePoly[i].y()/this->scene()->height()));

        amplitudeSpacePoints[i].setX(relativePoints[i].x()*ampWindow.width() + ampWindow.left());

        //Because the polygon drawing origin is at the top-left, we need to convert to an origin that is bottom-left
        amplitudeSpacePoints[i].setY((1-relativePoints[i].y())*ampWindow.height() + ampWindow.top());
    }

}

void RubberBandPolygon::updateSize() {

    //Called after a resize event
    QPointF tmpPoint;
    setPos(0,0);
    for (int i=0; i < points.length(); i++) {

        tmpPoint.setX(relativePoints[i].x()*this->scene()->width());
        tmpPoint.setY(relativePoints[i].y()*this->scene()->height());
        points[i].setX(tmpPoint.x());
        points[i].setY(tmpPoint.y());
        nodes[i]->setX(tmpPoint.x()-3);
        nodes[i]->setY(tmpPoint.y()-3);

    }
    setPolygon(QPolygonF(points));
}

void RubberBandPolygon::highlight() {
    //Make the draggable nodes visible when the polygon is clicked
    //with the edit tool
    for (int i=0; i<nodes.length();i++) {
        nodes[i]->setVisible(true);
    }
}

void RubberBandPolygon::removeHighlight() {
    //Hide the draggable nodes
    for (int i=0; i<nodes.length();i++) {
        nodes[i]->setVisible(false);
    }
}

bool RubberBandPolygon::isIncludeType() {
    if (type == 0) {
        return true;
    } else {
        return false;
    }
}

bool RubberBandPolygon::isExcludeType() {
    if (type == 1) {
        return true;
    } else {
        return false;
    }
}

bool RubberBandPolygon::isZoneType() {
    if (type == 2) {
        return true;
    } else {
        return false;
    }
}

bool RubberBandPolygon::isClusterShowing(){
    return clusterShowing;
}

void RubberBandPolygon::setClusterShowing(bool b){

    clusterShowing = b;
    color.setAlphaF(b ? 1.0 : 0.75);
    //assignedCluster = b*clusterNum;
    clusterNum = b*assignedCluster;
    //emit shapeChanged(assignedCluster, polyBitInd);
    emit shapeChanged(clusterNum, polyBitInd);
}

void RubberBandPolygon::setIncludeType() {
    //This is an include polygon
    type = 0;
}

void RubberBandPolygon::setExcludeType() {
    //This is an exclude polygon
    type = 1;
}

void RubberBandPolygon::setZoneType() {
    type = 2;

}

void RubberBandPolygon::calculateIncludedPoints(bool *inside, int imageWidth, int imageHeight) {
    //Calculate whether or not each pixel in included.
    //The output depends on the type of polygon (include or exclude polygon)

    //Create a new polygon in the shape of this polygon and scale
    //it to the pixel locations of the underlying image
    QVector<QPointF> absPoints;
    absPoints.resize(points.length());
    for (int i=0; i < points.length(); i++) {
        absPoints[i].setX(relativePoints[i].x()*imageWidth);
        absPoints[i].setY(relativePoints[i].y()*imageHeight);
    }
    QPolygonF absPolygon(absPoints);

    //Now we decide if each pixel in the image is included, where 'included'
    //means inside the polygon for include polygons and outside
    //the polygon for exclude polygons
    uint32_t pixnum = 0;
    if (isIncludeType()) {
        for (int h = 0; h < imageHeight; h++) {
            for (int w = 0; w < imageWidth; w++) {
                if (!absPolygon.containsPoint(QPointF(w,h),Qt::OddEvenFill)) {
                    *(inside+pixnum) = false;
                }
                pixnum++;
            }
        }
    } else if (isExcludeType()) {
        for (int h = 0; h < imageHeight; h++) {
            for (int w = 0; w < imageWidth; w++) {
                if (absPolygon.containsPoint(QPointF(w,h),Qt::OddEvenFill)) {
                    *(inside+pixnum) = false;
                }
                pixnum++;
            }
        }
    }
}

void RubberBandPolygon::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    //Paint the polygon

    //QPen pen(Qt::red,1);
//    Qt::PenStyle s = ;
    QPen pen(color,1);
    if(!clusterShowing)
        pen.setBrush(QBrush(color, Qt::DiagCrossPattern));
    else
        painter->setBrush(Qt::NoBrush);

    //pen.setColor(Qt::white);
    painter->setPen(pen);

    if (points.length() > 2) {
        painter->drawPolygon(polygon());
    } else if (points.length() == 2) {
        painter->drawLine(QLineF(points[0],points[1]));
    }
}

void RubberBandPolygon::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    //The polygon was clicked
    emit hasHighlight();
    highlight();

    if (event->button() == Qt::RightButton) {
        emit rightClicked(event->pos().toPoint());
    }
}

void RubberBandPolygon::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {

  //The polygon is being dragged.  We need to recalculate
  //the new relative points
  QGraphicsItem::mouseMoveEvent(event);
  QPolygonF scenePoly = mapToScene(polygon());



  /*
  QPointF newRelativePoint;
  newRelativePoint.setX(newPoint.x()/this->scene()->width());
  newRelativePoint.setY(newPoint.y()/this->scene()->height());
  relativePoints.append(newRelativePoint);

  QPointF pointInAmpSpace;
  pointInAmpSpace.setX(newRelativePoint.x()*ampWindow.width() + ampWindow.left());

  //Because the polygon drawing origin is at the top-left, we need to convert to an origin that is bottom-left
  pointInAmpSpace.setY((1-newRelativePoint.y())*ampWindow.height() + ampWindow.top());
  amplitudeSpacePoints.append(pointInAmpSpace);
  */

  for (int i=0; i < points.length(); i++) {
      relativePoints[i].setX((scenePoly[i].x()/this->scene()->width()));
      relativePoints[i].setY((scenePoly[i].y()/this->scene()->height()));

      amplitudeSpacePoints[i].setX(relativePoints[i].x() *ampWindow.width() + ampWindow.left());

      //Because the polygon drawing origin is at the top-left, we need to convert to an origin that is bottom-left
      amplitudeSpacePoints[i].setY((1-relativePoints[i].y())*ampWindow.height() + ampWindow.top());
  }

  dragging = true;


}

void RubberBandPolygon::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    QGraphicsItem::mouseReleaseEvent(event);

    if (dragging) {
        emit shapeChanged(clusterNum, polyBitInd);
    }
    dragging = false;
}

//--------------------------------------------

void ClusterPushButton::mousePressEvent(QMouseEvent *event){
    if (event->button()==Qt::RightButton){
        emit rightClicked(event->pos());
    }
    else if(event->button()==Qt::LeftButton){
        TrodesButton::mousePressEvent(event);
    }
}

//--------------------------------------------
ScatterPlotMipmap::ScatterPlotMipmap(QRectF space, int numXPixels, int numYPixels)
        :scatterSpace(space), //the full space the the points are plotted in
         currentSpaceWindow(space), //the default view window is the full space
         destWindow(QRect(0,0,100,100)), //default destination is 100 by 100
         xPixels(numXPixels), //max number of pixels
         yPixels(numYPixels)
{
//    int tempXRes = numXPixels;
//    int tempYRes = numYPixels;

    initialized = false;

    //The images use a common color table to save memory
    colorTable.push_back(qRgb(0,0,0));
    colorTable.push_back(qRgb(255,255,255));

}

void ScatterPlotMipmap::setColorTable(const QList<QColor> &colors) {



    colorTable.clear();
    //colorTable.push_back(qRgb(0,0,0));
    //colorTable.push_back(qRgb(255,255,255));

    //Every color gets 16 shades for density display (16 clusters allowed). The
    //darkestVal variable sets how much to scale the darkest shade
    double darkestVal = 1.0;
    double step = (1.0-darkestVal)/16;
    for (int i=0; i<colors.length();i++) {
        for (int j=0; j < 16; j++) {
            int b = colors[i].blue()*(darkestVal+(step*j));
            int r = colors[i].red()*(darkestVal+(step*j));
            int g = colors[i].green()*(darkestVal+(step*j));
            colorTable.push_back(qRgb(r,g,b));

        }
    }


    /*
    for (int i=0; i<colors.length();i++) {
        colorTable.push_back(colors[i].rgb());
    }
    for (int i=colors.length(); i<128;i++) {
        colorTable.push_back(qRgb(0,0,0));
    }
    for (int i=128; i<255; i++) {
        int greyScale = 255-(2*(i-128));
        //colorTable.push_back(qRgb(255,255,255));
        colorTable.push_back(qRgb(greyScale,greyScale,greyScale));
    }
    */

}

void ScatterPlotMipmap::createImages() {
    if (!initialized) {
        int tempXRes = xPixels;
        int tempYRes = yPixels;
        //We store at least one image (full resolution)
        imageVector.push_back(new QImage(tempXRes,tempYRes,QImage::Format_Indexed8));
        imageVector.last()->setColorTable(colorTable);
        imageVector.last()->fill(255);
        tempXRes = tempXRes/2;
        tempYRes = tempYRes/2;

        //We also store images at repeatedly halved resolution (until we get to a set minimum)
        while ((tempXRes > 50) && (tempYRes > 50)) {
            imageVector.push_back(new QImage(tempXRes,tempYRes,QImage::Format_Indexed8));
            imageVector.last()->setColorTable(colorTable);
            imageVector.last()->fill(255);
            tempXRes = tempXRes/2;
            tempYRes = tempYRes/2;
        }

        initialized = true;
        setWindows(currentSpaceWindow,destWindow);

    }
}

void ScatterPlotMipmap::deleteImages() {
    while (imageVector.length() > 0) {
        delete imageVector.takeLast();
    }
    initialized = false;
}

void ScatterPlotMipmap::addPoint(qreal x, qreal y, int colorIndex) {

    if (initialized) {
        qreal xPixelsPerSpaceUnit;
        qreal yPixelsPerSpaceUnit;

        /*
    if (x > scatterSpace.width()) {
        x = scatterSpace.width()-1;
    }
    if (y > scatterSpace.height()) {
        x = scatterSpace.width()-1;
    }*/
        if (x > scatterSpace.width()) {
            x = scatterSpace.width()-1;
        }
        if (y > scatterSpace.height()) {
            y = scatterSpace.height()-1;
        }

        int xpix;
        int ypix;
        for (int i=0; i<imageVector.length(); i++) {
            xPixelsPerSpaceUnit = (qreal)imageVector[i]->width()/scatterSpace.width();
            yPixelsPerSpaceUnit = (qreal)imageVector[i]->height()/scatterSpace.height();
            xpix = (x-scatterSpace.left())*xPixelsPerSpaceUnit;
            ypix = (y-scatterSpace.top())*yPixelsPerSpaceUnit;
            if (xpix<0) xpix = 0;
            if (ypix<0) ypix = 0;


            int oldPixVal = imageVector[i]->pixelIndex(xpix,ypix);
            if (oldPixVal==255) {
                imageVector[i]->setPixel(xpix,ypix,(colorIndex << 4));
            } else if ((oldPixVal & 0x0f) < 15) {
                imageVector[i]->setPixel(xpix,ypix, (colorIndex<< 4)+(oldPixVal & 0x0f)+1);

            }

        }
    }

}

void ScatterPlotMipmap::clearData() {
    for (int i=0; i<imageVector.length(); i++) {
        imageVector[i]->fill(255);
    }
}

void ScatterPlotMipmap::setPoints(int startInd, int endInd, const QVector<int>* inputData, const QVector<quint8> &clusterIDs) {
    //This function repplots the mixmaps using the raw data



    if (initialized) {
        const int* tmpXdata = inputData[axes.first].constData();
        const int* tmpYdata = inputData[axes.second].constData();

        qreal xPixelsPerSpaceUnit;
        qreal yPixelsPerSpaceUnit;

        int x;
        int y;
        for (int i=0; i<imageVector.length(); i++) {
            xPixelsPerSpaceUnit = (qreal)imageVector[i]->width()/scatterSpace.width();
            yPixelsPerSpaceUnit = (qreal)imageVector[i]->height()/scatterSpace.height();
            imageVector[i]->fill(255);
            int xpix;
            int ypix;
            int oldPixVal;
            for (int j = startInd; j < endInd; j++) {
                x = tmpXdata[j];
                y = tmpYdata[j];
                if (x > scatterSpace.width()) {
                    x = scatterSpace.width()-1;
                }
                if (y > scatterSpace.height()) {
                    y = scatterSpace.height()-1;
                }
                xpix = (x-scatterSpace.left())*xPixelsPerSpaceUnit;
                ypix = (y-scatterSpace.top())*yPixelsPerSpaceUnit;
                if (xpix < 0) xpix = 0;
                if (ypix < 0) ypix = 0;

                oldPixVal = imageVector[i]->pixelIndex(xpix,ypix);
                if (oldPixVal==255) {
                    imageVector[i]->setPixel(xpix,ypix,(clusterIDs[j] << 4));
                } else if ((oldPixVal & 0x0f) < 15) {
                    imageVector[i]->setPixel(xpix,ypix, (clusterIDs[j]<< 4)+(oldPixVal & 0x0f)+1);
                }




            }
        }

    }
}


void ScatterPlotMipmap::setDestWindow(QRect destWindow) {
    setWindows(currentSpaceWindow,destWindow);
}

void ScatterPlotMipmap::setWindows(QRectF sourceWindow, QRect destinationWindow) {
    currentSpaceWindow = sourceWindow;
    destWindow = destinationWindow;

    //Calculate which of the images should be used, based on the x axis

    if (initialized) {

        qreal desiredXUnitsPerPixel = currentSpaceWindow.width()/destWindow.width();
        qreal xUnitsPerPixel;

        int minIndex = 0;
        for (int i=0; i < imageVector.length(); i++) {
            xUnitsPerPixel = scatterSpace.width()/imageVector[i]->width();
            if (i==0) {
                //minDiffToDesired = abs(desiredXUnitsPerPixel-xUnitsPerPixel); //abs difference
                minIndex = i;
            }  else {

                if (xUnitsPerPixel <= desiredXUnitsPerPixel) {
                    //The detail more than we need, so we keep this index as the best so far
                    minIndex = i;
                } else {
                    //We are now at too coarse of pixel detail, so stop
                    if (i>2) {
                        minIndex = i-2;
                    }
                    break;
                }

            }
        }

        //Remember which image came the closest
        currentImageIndex = minIndex;
        //qDebug() << minIndex << imageVector[minIndex]->width() << imageVector[minIndex]->height();
        //Calculate the pixel window to use for the current mipmap image
        qreal xPixelsPerSpaceUnit = imageVector[minIndex]->width()/scatterSpace.width();
        qreal yPixelsPerSpaceUnit = imageVector[minIndex]->height()/scatterSpace.height();

        sourceImageWindow.setTop((currentSpaceWindow.top()-scatterSpace.top())*yPixelsPerSpaceUnit);
        sourceImageWindow.setBottom((currentSpaceWindow.bottom()-scatterSpace.top())*yPixelsPerSpaceUnit);
        sourceImageWindow.setLeft((currentSpaceWindow.left()-scatterSpace.left())*xPixelsPerSpaceUnit);
        sourceImageWindow.setRight((currentSpaceWindow.right()-scatterSpace.left())*xPixelsPerSpaceUnit);

    }

}

QImage ScatterPlotMipmap::getCurrentImage() {
    //return imageVector[currentImageIndex]->copy(sourceImageWindow).mirrored(false,true);
    if (initialized) {
        //return imageVector[currentImageIndex]->copy();
        return deAlias(imageVector[currentImageIndex]->copy(sourceImageWindow).mirrored(false,true),destWindow);
    } else {
        return QImage(100,100,QImage::Format_Indexed8);
    }
    //return imageVector[currentImageIndex]->copy(sourceImageWindow).mirrored(false,true).scaled(destWindow.size(),Qt::IgnoreAspectRatio,Qt::SmoothTransformation);
}

QImage ScatterPlotMipmap::getCurrentImage(QRect outputWindow) {

    if (initialized) {

        //return imageVector[currentImageIndex]->copy(sourceImageWindow);
        return deAlias(imageVector[currentImageIndex]->copy(sourceImageWindow).mirrored(false,true),outputWindow);
    } else {
        return QImage(100,100,QImage::Format_Indexed8);
    }
}

QImage ScatterPlotMipmap::deAlias(const QImage &orig, QRect window) {
//    qDebug() << "replotting" << orig.rect() << window;
    //Replots an image using a different resolution, making sure that any non-zero points are still visible
//    QImage img = orig.copy();
//    img.setColorTable(colorTable);
//    img.fill(255);
//    memcpy(img.bits(), orig.bits(), img.height()*img.bytesPerLine());
//    return img.scaled(window.width(), window.height());

    //The above does not work because:
    //scaling to smaller resolution causes data loss
    //scaling to larger resolution causes pixels to be literally enlarged
    QImage img = QImage(window.size(),QImage::Format_Indexed8);
    img.setColorTable(colorTable);
    img.fill(255);

    qreal xOutPixPerInPix = (window.width()-1)/(qreal)orig.width(); //the ratio of the output image res / the original image res
    qreal yOutPixPerInPix = (window.height()-1)/(qreal)orig.height();
    int xpix;
    int ypix;
    //qDebug() << orig.size();
    for (int i = 0; i < orig.width(); i++) {
        for (int j=0; j < orig.height(); j++) {
            if (orig.pixelIndex(i,j) != 255) {
                xpix = round(i*xOutPixPerInPix);
                ypix = round(j*yOutPixPerInPix);
                if (xpix < 0) xpix = 0;
                if (ypix < 0) ypix = 0;
                img.setPixel(xpix,ypix,orig.pixelIndex(i,j));
            }
        }
    }
    return img;
}


WaveFormPlotImage::WaveFormPlotImage(int totalbuffersize, int numCh, int ntr):
    numWaveForms(0),
    startOfBuffer(0),
    numChannels(numCh),
    ntrode(ntr),
    totalBufferSize(totalbuffersize),
    latestIndex(0),
    meanPeak(0),
    TOTALPOINTS(POINTSINWAVEFORM*numChannels),
    dataShowing(false),
    firstCopy(true)
{
    //Arrays of data: gImageData has opengl data. v is a dummy array to use to quickly memcpy when resetting values
    //Keeping gImageData a single continuous array instad of a 2d or 3d array is good for memcpy, needed for quickly
    //memcpy'ing data without delaying the GUI
    //Clusters is an array of quint8's, meant to keep track of which cluster each of the -totalBufferSize- waveforms belongs

    //X coord is scaled, y coord is not scaled, scaled with maxAmplitude in opengl w parallel/vectorization/gpu magic, faster than looping
    gImageData = new int2d[totalBufferSize*TOTALPOINTS];
    clusters = new quint8[totalBufferSize];
    v.resize(TOTALPOINTS);
    for(int j = 0; j < TOTALPOINTS; j++){
        v[j].x = j;
        v[j].y = 0;
    }
    for(int i = 0; i < totalBufferSize; i++) {
        memcpy(gImageData+i*TOTALPOINTS, v.data(), TOTALPOINTS*sizeof(int2d));
        clusters[i] = 0;
    }


    /*for(int i = 0; i < totalBufferSize; i++){
        for(int j = 0; j < TOTALPOINTS; j++){
            gImageData[i*TOTALPOINTS + j].x = j;
            gImageData[i*TOTALPOINTS + j].y = 0;
        }
        clusters[i] = 0;
    }*/
}

WaveFormPlotImage::~WaveFormPlotImage(){
    delete[] gImageData;
    delete[] clusters;
}


int2d* WaveFormPlotImage::latestWaveform(){
    //Return pointer to the latest waveform.
    return &gImageData[latestIndex*POINTSINWAVEFORM*numChannels];

}

//Not used anymore because of the fact that memcpy can't be used. Too slow
ClusterWaveformSet* WaveFormPlotImage::latestNWaveforms(){
    //Copy over the last n waveforms (limited to buffersize)
    //Cannot memcpy because its possible that the last n waveforms that came in loop around the buffer
    //Ok because generally not more than a few waveforms

    if(firstCopy){
        firstCopy = false;
        return allWaveforms();
    }

    ClusterWaveformSet* set = new ClusterWaveformSet;
    int nwaveforms = numWaveForms;
    int start = startOfBuffer;

    if((nwaveforms-start) >= totalBufferSize*.95)
        return allWaveforms();

    imageDataMutex.lock();
    for(int i = start; i < nwaveforms; i++){
        for(int j = 0; j < TOTALPOINTS; j++){
            int2d v;
//            v.x =  (GLfloat)2*j/TOTALPOINTS - 1.0;
//            v.y = gImageData[(i%totalBufferSize)*TOTALPOINTS + j].y;
            v.x = j;
            v.y = gImageData[(i%totalBufferSize)*TOTALPOINTS + j].y;
            set->points.append(v);
        }
    }

    for(int i = start; i < nwaveforms; i++){
        set->clusters.append(clusters[(i%totalBufferSize)]);
    }
    startOfBuffer = nwaveforms;

    //Sort by cluster
    for(int i = 0; i < set->clusters.size(); i++)
        set->indices.append(i);
    std::sort(set->indices.begin(), set->indices.end(), *set);
    set->hidden = hiddenClusters;
    imageDataMutex.unlock();
    return set;
}

ClusterWaveformSet* WaveFormPlotImage::allWaveforms(){
    //Create vector of all waveforms. This can be memcpy'd because all in one contiguous chunk of memory

    QElapsedTimer t; t.start();
    int nwaveforms;

    //Min(numWaveforoms, totalBufferSize), since we only store totalBufferSize number of waveforms
    if(numWaveForms > totalBufferSize)
        nwaveforms = totalBufferSize;
    else
        nwaveforms = numWaveForms;
    ClusterWaveformSet* set = new ClusterWaveformSet;
    int2d v; v.x = 0; v.y = 0;
    set->points = QVector<int2d>(nwaveforms*TOTALPOINTS, v);
    set->clusters = QVector<quint8>(nwaveforms, 0);
    set->hidden = hiddenClusters;

    //Memcpy over all data.
    imageDataMutex.lock();
    memcpy(set->points.data(), gImageData, sizeof(int2d)*nwaveforms*TOTALPOINTS);
    memcpy(set->clusters.data(), clusters, nwaveforms*sizeof(quint8));

    //Sort by cluster
    for(int i = 0; i < set->clusters.size(); i++)
        set->indices.append(i);
    std::sort(set->indices.begin(), set->indices.end(), *set);
    imageDataMutex.unlock();
    return set;
}

void WaveFormPlotImage::receiveNewWaveform(const QVector<QVector<int2d> > &waveForm, quint8 cl, const int currBufferIndex){
    //Add to GL Data block gImageData
    //If only input's x's were scaled, then can directly memcpy
    imageDataMutex.lock();

    for(int ch = 0; ch < numChannels; ch++){
        for(int i = 0; i < POINTSINWAVEFORM; i++){
            gImageData[(currBufferIndex)*TOTALPOINTS + ch*POINTSINWAVEFORM + i].y = waveForm[ch][i].y;
        }
    }
    clusters[currBufferIndex] = cl;
    numWaveForms++;
    latestIndex = currBufferIndex;
    imageDataMutex.unlock();
}

void WaveFormPlotImage::clearData(){
    //Clear data, copy over v to preserve x coordinate values
    //Cluster data is cleared by ntrodedisplaydata
    imageDataMutex.lock();
    /*for(int i = 0; i < totalBufferSize; i++)
        memcpy(gImageData+i*TOTALPOINTS, v.data(), TOTALPOINTS);*/

    memset(clusters, 0, totalBufferSize);
    numWaveForms = 0;
    imageDataMutex.unlock();
}

void WaveFormPlotImage::setClusters(quint8 *clust){
    imageDataMutex.lock();
    memcpy(clusters, clust, totalBufferSize);
    imageDataMutex.unlock();
}

void WaveFormPlotImage::setClusterShowing(bool showing, quint8 cluster){
    imageDataMutex.lock();
    if(showing) hiddenClusters.remove(cluster);
    else        hiddenClusters.insert(cluster);
    imageDataMutex.unlock();
}

void WaveFormPlotImage::setDataShowing(bool b){
    //Currently not used, but could be useful?
    //Variable if this waveform data is being shown or not
    dataShowing = b;
}

//----------------------------------------------
CustomScene::CustomScene(QWidget *parent, int bufferSizeInput)
    :QGraphicsScene(parent) {

    initialized = false;
    currentXdisplay = 0;
    currentYdisplay = 1;
    plotNewPoints = true;
    replotScatter = true;
    displayMulipleProjections = false;

    bufferSize = bufferSizeInput;
    displayData.resize(bufferSize);

    backgroundPic = NULL;

    for (int i = 0; i < bufferSize; i++) {
        displayData[i].x = 0;
        displayData[i].y = 0;
    }



    //mipMap = new ScatterPlotMipmap(QRectF(0.0,0.0,(2000.0*65536)/AD_CONVERSION_FACTOR,(2000.0*65536)/AD_CONVERSION_FACTOR),1000,1000);

}

void CustomScene::setBackgroundPic(QImage *pic) {
    backgroundPic = pic;
}

void CustomScene::setMultipleProjections(bool multiple) {
    displayMulipleProjections = multiple;
    update();
}

void CustomScene::setChannels(int channelX, int channelY, const QVector<int>* inputData) {

    plotNewPoints = false;
    currentXdisplay = channelX;
    currentYdisplay = channelY;
    const int* tmpXdata = inputData[currentXdisplay].constData();
    const int* tmpYdata = inputData[currentYdisplay].constData();

    for (int i = 0; i < bufferSize; i++) {

        displayData[i].x = tmpXdata[i];
        displayData[i].y = tmpYdata[i];
    }
    update();
    plotNewPoints = true;

}

void CustomScene::setDataPoint(const QVector<int> *inputData, int currentBufferIndex) {

    if (plotNewPoints) {
        replotScatter = true;
        displayData[currentBufferIndex].x = inputData[currentXdisplay][currentBufferIndex];
        displayData[currentBufferIndex].y = inputData[currentYdisplay][currentBufferIndex];


        replotScatter = true;
        //update();
    }
}

void CustomScene::mousePressEvent(QGraphicsSceneMouseEvent *event) {

    //check to see if the item pressed was a polygon or the background (a widget showing video)
    if(itemAt(event->scenePos(),QTransform())) {
        QGraphicsItem *clickedItem = itemAt(event->scenePos(),QTransform());
        if (!clickedItem->isWidget()) {
            //a polygon was clicked
            QGraphicsScene::mousePressEvent((event));
            return;
        }
    }
    emit emptySpaceClicked();

}

void CustomScene::drawBackground(QPainter *painter, const QRectF &rect) {
    //if (replotScatter) {


    //painter->setRenderHint(QPainter::SmoothPixmapTransform, true);
    painter->setPen(Qt::green);
    painter->setViewTransformEnabled(true);
    painter->setViewport(0,0,width(), height());
    //Support for high DPI monitors
    QWidget* p = (QWidget*)this->parent();
    painter->scale(p->devicePixelRatio(),p->devicePixelRatio());
    if (backgroundPic != NULL) {
        painter->drawImage(painter->viewport(),*backgroundPic,backgroundPic->rect());

    }




/*
        painter->setWindow(minAmplitude[currentXdisplay], minAmplitude[currentYdisplay], maxAmplitude[currentXdisplay]-minAmplitude[currentXdisplay], maxAmplitude[currentYdisplay]-minAmplitude[currentYdisplay]);
        painter->fillRect(minAmplitude[currentXdisplay], minAmplitude[currentYdisplay], maxAmplitude[currentXdisplay]-minAmplitude[currentXdisplay], maxAmplitude[currentYdisplay]-minAmplitude[currentYdisplay],QBrush(QColor(0,0,0)));
        //painter->fillRect(rect,QBrush(QColor(0,0,0)));
        painter->beginNativePainting();
        glViewport(0, 0, (GLint)width(), (GLint)height());
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        //glOrtho(0, 1.0, 0, 1.0, -1.0, 1.0);
        glOrtho((GLdouble)minAmplitude[currentXdisplay], (GLdouble)maxAmplitude[currentXdisplay], (GLdouble)minAmplitude[currentYdisplay], (GLdouble)maxAmplitude[currentYdisplay], -1.0, 1.0);
        //glOrtho((GLdouble)rect.left(),(GLdouble)rect.right(),(GLdouble)rect.bottom(),(GLdouble)rect.top(),-1.0,1.0);
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glPointSize(1);
        glColor3f((GLfloat)1.0,(GLfloat)1.0,(GLfloat)1.0);
        //qglColor(QColor(255,255,255));
        glLoadIdentity();
        //glScalef(1.0/(maxAmplitude[currentXdisplay]), 1.0/(maxAmplitude[currentYdisplay]), 1.0);
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(2, GL_FLOAT, 0, displayData.constData());
        glDrawArrays(GL_POINTS,0,bufferSize);
        glDisableClientState(GL_VERTEX_ARRAY);
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();
        painter->endNativePainting();

*/


    //}


}


/*
//-----------------------------------------------------
//The GraphicsWindow contains the polygons and the underlying video image



//The GraphicsWindow contains the polygons and the underlying video image
GraphicsWindow::GraphicsWindow(QWidget *parent, int bufferSizeInput, int numChannels)
    : QGraphicsView(parent) {

    //dispWin = new VideoDisplayWindow(NULL);

    currentTool = INCLUDETOOL_ID;
    currentCluster = 1;

    currentXdisplay = 0;
    currentYdisplay = 1;
    currentMipmapIndex = 0;
    bufferSize = bufferSizeInput;

    displayMultipleProjections = false;
    isShown = false;



    //setViewport(new QGLWidget(QGLFormat(QGL::DoubleBuffer| QGL::DirectRendering)));
    setViewport(new QWidget());
    setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
    update();

    maxAmplitude.resize(numChannels);
    for (int i = 0; i < numChannels; i++) {
        maxAmplitude[i] = (400*65536)/AD_CONVERSION_FACTOR;
    }

    minAmplitude.resize(numChannels);
    for (int i = 0; i < numChannels; i++) {
        minAmplitude[i] = 0;
    }

    lowRangeMipmapMax = 1000.0; //in uV, for the low-range mipmap (not yet implemented)
    for (int i = 0; i < numChannels; i++) {
        for (int j = i+1; j < numChannels; j++) {
            mipMaps.push_back(new ScatterPlotMipmap(QRectF(0.0,0.0,(2500.0*65536)/AD_CONVERSION_FACTOR,(2500.0*65536)/AD_CONVERSION_FACTOR),5000,5000));
            mipMaps.last()->axes = qMakePair(i,j);

            //mipMaps_lowRange.push_back(new ScatterPlotMipmap(QRectF(0.0,0.0,(lowRangeMipmapMax*65536)/AD_CONVERSION_FACTOR,(lowRangeMipmapMax*65536)/AD_CONVERSION_FACTOR),5000,5000));
            //mipMaps_lowRange.last()->axes = qMakePair(i,j);
        }
    }

    scene = new CustomScene(this, bufferSizeInput);
    connect(scene,SIGNAL(emptySpaceClicked()),this,SLOT(spaceClicked()));
    //connect(dispWin,SIGNAL(resolutionChanged()),this,SLOT(calculateConsideredPixels()));
    //scene->addWidget(dispWin,Qt::Widget);
    this->setScene(scene);

    currentlyDrawing = false;
    currentlySelectedPolygon = -1;



    //dispWin->show();

    QPen markerPen;
    markerPen.setWidth(1);
    markerPen.setColor(Qt::green);

    setMouseTracking(true);

    setMultipleProjections(false);

}

void GraphicsWindow::setWindowActive(bool active) {


    if (!isShown && active) {
        isShown = true;
        for (int i=0;i < mipMaps.length(); i++) {
            mipMaps[i]->createImages();

            //mipMaps_lowRange[i]->createImages();
        }

    } else if (isShown && !active) {
        isShown = false;
        for (int i=0;i < mipMaps.length(); i++) {
            mipMaps[i]->deleteImages();
            //mipMaps_lowRange[i]->deleteImages();
        }
    }

}

void GraphicsWindow::setData(const QVector<int> *inputData, const QVector<quint8> &clustID) {
    if (isShown) {
        for (int i=0;i < mipMaps.length(); i++) {
            mipMaps[i]->setPoints(0,bufferSize,inputData, clustID);
            //mipMaps_lowRange[i]->setPoints(0,bufferSize,inputData);
        }
    }
}

void GraphicsWindow::setMultipleProjections(bool multiple) {
    displayMultipleProjections = multiple;

    if (displayMultipleProjections) {
        for (int i=0; i<polygons.length(); i++) {
           polygons[i]->setVisible(false);
        }
    } else {
        //Only show the polygons for the current axes
        RubberBandPolygon::Axes ax;
        for (int i=0; i<polygons.length(); i++) {
            ax = polygons[i]->getAxes();
            if ((ax.x == currentXdisplay) && (ax.y == currentYdisplay)) {
                polygons[i]->setVisible(true);
            } else {
                polygons[i]->setVisible(false);
            }
        }


    }

    if (isShown) {
        scene->update();
    }

}
void GraphicsWindow::updateBackgroundPlot() {

    if (isShown) {
        if (displayMultipleProjections) {

            currentBackgroundPic = QImage(width(),height(),QImage::Format_RGB32);

            currentBackgroundPic.fill(qRgb(0,0,0));
            QPainter paint;
            paint.begin(&currentBackgroundPic);
            paint.setViewTransformEnabled(true);

            int numCol;
            int numRows;
            QPen pn;
            pn.setColor(qRgb(100,100,100));
            pn.setWidth(1);

            if (mipMaps.length() <= 6) {
                numCol = 2;
            } else {
                numCol = 3;
            }
            numRows = ceil(mipMaps.length()/numCol);
            int tmpMipmapIndex = 0;
            QRect currentDrawWindow;

            currentDrawWindow.setWidth(currentBackgroundPic.width()/numCol);
            currentDrawWindow.setHeight(currentBackgroundPic.height()/numRows);

            int panelWidth = currentBackgroundPic.width()/numCol;
            int panelHeight = currentBackgroundPic.height()/numRows;

            //Draw the scatter images
            for (int i=0; i<numCol; i++) {
                for (int j=0; j<numRows; j++) {
                    QImage tempImage = mipMaps[tmpMipmapIndex]->getCurrentImage(currentDrawWindow);
                    paint.drawImage(i*panelWidth,j*panelHeight,tempImage);
                    tmpMipmapIndex++;
                }
            }

            //Draw the image borders
            paint.setPen(pn);
            for (int i=0; i<numCol; i++) {
                for (int j=0; j<numRows; j++) {
                    paint.drawRect(i*panelWidth,j*panelHeight,panelWidth,panelHeight);
                }
            }

            paint.end();

        } else {
            //Just plot one panel
            currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();

        }
        scene->setBackgroundPic(&currentBackgroundPic);
    }
}

void GraphicsWindow::setDataPoint(const QVector<int> *inputData, int currentBufferIndex, int clustID) {

    //if (displayMultipleProjections) {
        for (int i=0; i<mipMaps.length(); i++) {
            mipMaps[i]->addPoint((qreal)inputData[mipMaps[i]->axes.first][currentBufferIndex],(qreal)inputData[mipMaps[i]->axes.second][currentBufferIndex],clustID);
        }

        //currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();
        //scene->setBackgroundPic(&currentBackgroundPic);
    //} else {


        scene->setDataPoint(inputData, currentBufferIndex);
    //}
    //displayData[currentBufferIndex].x = inputData[currentXdisplay][currentBufferIndex];
    //displayData[currentBufferIndex].y = inputData[currentYdisplay][currentBufferIndex];

}

void GraphicsWindow::clearData() {
    for (int i=0; i<mipMaps.length(); i++) {
        mipMaps[i]->clearData();
    }

    updateBackgroundPlot();
}

void GraphicsWindow::setChannels(int channelX, int channelY, const QVector<int> *inputData, const QVector<quint8> &clustID) {
    currentXdisplay = channelX;
    currentYdisplay = channelY;

    if (!displayMultipleProjections) {
        for (int i=0; i<mipMaps.length(); i++) {
            if ((mipMaps[i]->axes.first == currentXdisplay) && (mipMaps[i]->axes.second == currentYdisplay)) {
                currentMipmapIndex = i;
                mipMaps[i]->setPoints(0,bufferSize,inputData, clustID);
                break;
            }
        }

        currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();
        //scene->setBackgroundPic(&currentBackgroundPic);
    }

    //For older opengl plotting
    //scene->setChannels(channelX, channelY, inputData);

    //Only show the polygons for the current axes
    RubberBandPolygon::Axes ax;
    for (int i=0; i<polygons.length(); i++) {
        ax = polygons[i]->getAxes();
        if ((ax.x == currentXdisplay) && (ax.y == currentYdisplay)) {
            polygons[i]->setVisible(true);
        } else {
            polygons[i]->setVisible(false);
        }
    }

    scene->update();
}

void GraphicsWindow::setMaxDisplay(int channel, int maxDisplayVal) {
    //Changes the maximum amplitude of the display range
    //maxAmplitude[channel] = (maxDisplayVal*65536)/AD_CONVERSION_FACTOR;


    setMaxDisplay(maxDisplayVal);

    //scene->setMaxDisplay(channel,maxDisplayVal);



}

void GraphicsWindow::setMaxDisplay(int maxDisplayVal) {

    if (maxDispVal_uV != maxDisplayVal) {


        for (int i=0; i<maxAmplitude.length(); i++) {
            maxAmplitude[i] = (maxDisplayVal*65536)/AD_CONVERSION_FACTOR;
        }

        maxDispVal_uV = maxDisplayVal;



        QRectF dWin = QRectF(0.0,0.0,(qreal)maxAmplitude[0],(qreal)maxAmplitude[0]);

        for (int i=0; i<mipMaps.length();i++) {
            mipMaps[i]->setWindows(dWin,QRect(0,0,width(),height()));
        }

        updateBackgroundPlot();

        //currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();
        //scene->setBackgroundPic(&currentBackgroundPic);


        //For older opengl plotting
        //scene->setMaxDisplay(maxDisplayVal);

        for (int i=0;i<polygons.length();i++) {
            polygons[i]->setCurrentAmpWindow(QRectF(0.0,0.0,(qreal)maxAmplitude[currentXdisplay],(qreal)maxAmplitude[currentYdisplay]));
        }





    }
}


void GraphicsWindow::addExcludePolygon() {
    RubberBandPolygon *newPoly = new RubberBandPolygon();
    newPoly->setExcludeType();

    connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));
    connect(newPoly,SIGNAL(shapeChanged(int,int)),this,SLOT(calculatePointsInsidePolygon(int,int)));

    polygons.append(newPoly);
    scene->addItem(newPoly);
}

void GraphicsWindow::addZonePolygon() {
    RubberBandPolygon *newPoly = new RubberBandPolygon();
    newPoly->setZoneType();

    connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));

    polygons.append(newPoly);
    scene->addItem(newPoly);
}

void GraphicsWindow::addIncludePolygon() {
    int polyBitInd;
    bool foundEmptySlot = false;
    for (int i=0; i<polygonsTaken.length(); i++) {
        if (!polygonsTaken[i]) {
            polyBitInd = i;
            polygonsTaken[i] = true;
            foundEmptySlot = true;
        }
    }
    if (!foundEmptySlot) {
        polygonsTaken.append(true);
        polyBitInd = polygonsTaken.length()-1;
    }

    RubberBandPolygon *newPoly = new RubberBandPolygon();
    newPoly->setIncludeType();
    newPoly->setPolyBitInd(polyBitInd);
    newPoly->setAxes(currentXdisplay,currentYdisplay);

    connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));
    connect(newPoly,SIGNAL(shapeChanged(int,int)),this,SLOT(calculatePointsInsidePolygon(int,int)));

    polygons.append(newPoly);

    polygons.last()->setCurrentAmpWindow(QRectF(0.0,0.0,(qreal)maxAmplitude[currentXdisplay],(qreal)maxAmplitude[currentYdisplay]));
    polygons.last()->setColor(QColor(0,255,0));
    scene->addItem(newPoly);

}

void GraphicsWindow::spaceClicked() {
    //the background was clicked, so turn off all highlights
    setNoHighlight();
}

void GraphicsWindow::setNoHighlight() {
    for (int i=0; i<polygons.length(); i++) {
        polygons[i]->removeHighlight();
    }
    currentlySelectedPolygon = -1;
}

void GraphicsWindow::polygonHighlighted() {
    RubberBandPolygon* highlightedPoly = dynamic_cast<RubberBandPolygon*>(sender());

    for (int i=0; i<polygons.length(); i++) {
        if (polygons[i] == highlightedPoly) {
            currentlySelectedPolygon = i;
        } else {
            polygons[i]->removeHighlight();
        }
    }
}

void GraphicsWindow::setTool(int toolNum) {
    currentTool = toolNum;
    if (currentTool != EDITTOOL_ID) {
        //We only allow highlighting when the edit tool is active
        setNoHighlight();
    }
}

void GraphicsWindow::setClusterColors(const QList<QColor> colors) {
    clusterColors = colors;
    for (int i = 0; i < mipMaps.length(); i++) {
        mipMaps[i]->setColorTable(colors);
    }
}

void GraphicsWindow::setCurrentCluster(int clust) {
    currentCluster = clust;
}

void GraphicsWindow::calculatePointsInsidePolygon(int clustInd, int polygonBitIndex) {


    //Find which polygon send the message (can be done with sender() too)


    emit lockSpikeSorting();
    emit newClusterCalculationNeeded(clustInd, polygonBitIndex);
    emit unlockSpikeSorting();

}

QList<ClusterPolygonSet> GraphicsWindow::getPolygonsForCluster(int clusterInd) {
    QList<ClusterPolygonSet> polyInfo;
    for (int i=0; i<polygons.length(); i++) {
        if (polygons[i]->getAssignedClusterNum() == clusterInd) {
            //This polygon belongs to the desired cluster
            ClusterPolygonSet newPoly;
            newPoly.xAxis = polygons[i]->getAxes().x;
            newPoly.yAxis = polygons[i]->getAxes().y;
            newPoly.points = polygons[i]->getPoints();
            newPoly.bitInd = polygons[i]->getPolyBitInd();
            polyInfo.append(newPoly);
        }
    }
    return polyInfo;
}

void GraphicsWindow::calculateConsideredPixels() {
    //This function is called every time a polygon
    //is changed or whenever we need to recalculate
    //which pixels are included in the current set of
    //polygons.


}

void GraphicsWindow::mousePressEvent(QMouseEvent *event) {
    //When the window is clicked, the behavior depends on
    //which tool is currently selected.

    if (currentlyDrawing) {
        polygons.last()->addPoint(event->localPos());
        return;
    }

    if (!displayMultipleProjections) {
        if (currentTool == INCLUDETOOL_ID) {

            for (int i = 0; i < polygons.length(); i++) {
                if ((polygons[i]->getAssignedClusterNum() == currentCluster) && (polygons[i]->getAxes().x == currentXdisplay) && (polygons[i]->getAxes().y == currentYdisplay)){
                    //There is already a polygon for this cluster on this projection
                    ClusterPolygonSet p;
                    p.bitInd = polygons[i]->getPolyBitInd();
                    p.xAxis = polygons[i]->getAxes().x;
                    p.yAxis = polygons[i]->getAxes().y;
                    p.points = polygons[i]->getPoints();


                    polygonsTaken[polygons[i]->getPolyBitInd()] = false;
                    delete polygons.takeAt(i);
                    //setNoHighlight();
                    emit lockSpikeSorting();
                    emit polygonDeleted(currentCluster,p.bitInd);
                    emit unlockSpikeSorting();
                    break;
                }
            }

            addIncludePolygon();
            polygons.last()->setClusterNum(currentCluster);
            polygons.last()->setColor(clusterColors[currentCluster]);


            currentlyDrawing = true;
            polygons.last()->addPoint(event->localPos());
            polygons.last()->addPoint(event->localPos());

        } else if ((currentTool == EDITTOOL_ID)&&(itemAt(event->pos()))) {
            // Clicked on polygon, pass on event to children
            QGraphicsView::mousePressEvent(event);
        } else if (currentTool == PANTOOL_ID) {
            //used to manually designate animal location
        }
    }

}

void GraphicsWindow::mouseMoveEvent(QMouseEvent *event) {

    if (currentlyDrawing) {
        polygons.last()->moveLastPoint(event->localPos());
    } else {
        QGraphicsView::mouseMoveEvent(event);
    }
}

void GraphicsWindow::mouseDoubleClickEvent(QMouseEvent *event) {
    if (currentlyDrawing) {
        currentlyDrawing = false;
        polygons.last()->removeLastPoint();
        //calculateConsideredPixels();
        calculatePointsInsidePolygon(currentCluster, polygons.last()->getPolyBitInd());
    }
}

void GraphicsWindow::wheelEvent(QWheelEvent *wheelEvent) {
    //qreal ypos = wheelEvent->pos().y(); // UNUSED

    int changeAmount;
    if (wheelEvent->delta() > 0) {
        changeAmount = -25;
    } else {
        changeAmount = 25;
    }

    emit zoom(changeAmount);

}

void GraphicsWindow::keyPressEvent(QKeyEvent *event) {

    if ((event->key() == Qt::Key_Delete)||(event->key() == Qt::Key_Backspace)) {
        //delete the selected polygon
        if (currentlySelectedPolygon != -1) {

            int selectedClusterInd = polygons[currentlySelectedPolygon]->getAssignedClusterNum();
            int selectedPolyBitIndex = polygons[currentlySelectedPolygon]->getPolyBitInd();
            ClusterPolygonSet p;
            p.bitInd = polygons[currentlySelectedPolygon]->getPolyBitInd();
            p.xAxis = polygons[currentlySelectedPolygon]->getAxes().x;
            p.yAxis = polygons[currentlySelectedPolygon]->getAxes().y;
            p.points = polygons[currentlySelectedPolygon]->getPoints();
            polygonsTaken[polygons[currentlySelectedPolygon]->getPolyBitInd()] = false;
            delete polygons.takeAt(currentlySelectedPolygon);
            setNoHighlight();
            //calculateConsideredPixels();
            //calculatePointsInsidePolygon(selectedClusterInd,selectedPolyBitIndex);

            emit polygonDeleted(selectedClusterInd,selectedPolyBitIndex);

        }
    }
}

void GraphicsWindow::refresh() {
    for (int i=0; i<mipMaps.length();i++) {
        mipMaps[i]->setDestWindow(QRect(0,0,this->width(),this->height()));
    }

    scene->setSceneRect(0,0,this->width(),this->height());

    for (int i=0; i<polygons.length(); i++) {
        polygons[i]->updateSize();
    }

    updateBackgroundPlot();
    //currentBackgroundPic = mipM
}

void GraphicsWindow::resizeEvent(QResizeEvent *event) {

    //dispWin->resize(event->size().width(),event->size().height());

    for (int i=0; i<mipMaps.length();i++) {
        mipMaps[i]->setDestWindow(QRect(0,0,event->size().width(),event->size().height()));
    }

    scene->setSceneRect(0,0,event->size().width(),event->size().height());

    for (int i=0; i<polygons.length(); i++) {
        polygons[i]->updateSize();
    }

    updateBackgroundPlot();
    //currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();
    //scene->setBackgroundPic(&currentBackgroundPic);


} */








//The GraphicsWindow contains the polygons and the underlying video image
ScatterPlotWindow::ScatterPlotWindow(QWidget *parent, int bufferSizeInput)
    : QGraphicsView(parent) {

    //dispWin = new VideoDisplayWindow(NULL);

    currentTool = INCLUDETOOL_ID;
    currentCluster = 1;
    currentNTrode = 0;

    currentMouseXPanel = -1;
    currentMouseYPanel = -1;

    currentXdisplay = 0;
    currentYdisplay = 1;
    currentMipmapIndex = 0;
    bufferSize = bufferSizeInput;

    displayMultipleProjections = false;

    int numTrodes = spikeConf->ntrodes.length();
    polygons.resize(numTrodes);
    polygonsTaken.resize(numTrodes);

    //setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers | QGL::DirectRendering)));
    //setViewport(new QGLWidget(QGLFormat(QGL::DoubleBuffer| QGL::DirectRendering)));
    setViewport(new QOpenGLWidget());
    setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
    //update();

    /*
    maxAmplitude.resize(numTrodes);
    minAmplitude.resize(numTrodes);
    for (int trNum=0;trNum<numTrodes;trNum++) {
        maxAmplitude[trNum].resize(numChannels);
        for (int i = 0; i < numChannels; i++) {
            maxAmplitude[trNum][i] = (400*65536)/AD_CONVERSION_FACTOR;
        }


        minAmplitude[trNum].resize(numChannels);
        for (int i = 0; i < numChannels; i++) {
            minAmplitude[trNum][i] = 0;
        }
    }*/

    lowRangeMipmapMax = 1000.0; //in uV, for the low-range mipmap (not yet implemented)

    scene = new CustomScene(this, bufferSizeInput);
    connect(scene,SIGNAL(emptySpaceClicked()),this,SLOT(spaceClicked()));
    connect(this, SIGNAL(newBackgroundReady()),this,SLOT(showNewBackgroundPlot()),Qt::QueuedConnection);
    //connect(dispWin,SIGNAL(resolutionChanged()),this,SLOT(calculateConsideredPixels()));
    //scene->addWidget(dispWin,Qt::Widget);
    this->setScene(scene);

    currentlyDrawing = false;
    currentlySelectedPolygon = -1;


    QPen markerPen;
    markerPen.setWidth(1);
    markerPen.setColor(Qt::green);

    setMouseTracking(true);
    setMultipleProjections(false);

}



void ScatterPlotWindow::setDisplayedNTrode(int nTrodeInd, QVector<int> minDisplay, QVector<int> maxDisplay, QList<ScatterPlotMipmap*> mmPtrs) {


    for (int i=0; i<polygons[currentNTrode].length(); i++) {
       polygons[currentNTrode][i]->setVisible(false);
    }



    currentNTrode = nTrodeInd;
    setMipMapPtrs(mmPtrs);
    setDisplayWindow(minDisplay, maxDisplay);


    if (!displayMultipleProjections) {


        for (int i=0; i<polygons[nTrodeInd].length(); i++) {
            polygons[nTrodeInd][i]->setVisible(true);
        }

    }

    updateCurrentClusterPanels();


    updateBackgroundPlot();
    scene->update();


}

void ScatterPlotWindow::setMipMapPtrs(QList<ScatterPlotMipmap *> mmPtrs) {
    mipMaps = mmPtrs;
}


void ScatterPlotWindow::setData(const QVector<int> *inputData, const QVector<quint8> &clustID) {

    for (int i=0;i < mipMaps.length(); i++) {
        mipMaps[i]->setPoints(0,bufferSize,inputData, clustID);
        //mipMaps_lowRange[i]->setPoints(0,bufferSize,inputData);
    }

}

void ScatterPlotWindow::setMultipleProjections(bool multiple) {
    displayMultipleProjections = multiple;

    if (displayMultipleProjections) {
        for (int i=0; i<polygons[currentNTrode].length(); i++) {
           polygons[currentNTrode][i]->setVisible(false);
        }
    } else {
        //Only show the polygons for the current axes
        RubberBandPolygon::Axes ax;
        for (int i=0; i<polygons[currentNTrode].length(); i++) {
            ax = polygons[currentNTrode][i]->getAxes();
            if ((ax.x == currentXdisplay) && (ax.y == currentYdisplay)) {
                polygons[currentNTrode][i]->setVisible(true);
            } else {
                polygons[currentNTrode][i]->setVisible(false);
            }
        }


    }

    scene->update();

}
void ScatterPlotWindow::calculateBackgroundPlot() {
    int panelWidth = width()*devicePixelRatio();
    int panelHeight = height()*devicePixelRatio();
    if (displayMultipleProjections) {
        int numCol, numRows;
        numCol = floor(sqrt(mipMaps.length()));

        numRows = ceil(mipMaps.length()/(double)numCol);
        panelWidth = width()*devicePixelRatio()/numCol;
        panelHeight = height()*devicePixelRatio()/numRows;

    }

    for (int i=0; i<mipMaps.length();i++) {
        mipMaps[i]->setDestWindow(QRect(0,0,panelWidth,panelHeight));
    }

    if (displayMultipleProjections) {

        currentBackgroundPic = QImage(width()*devicePixelRatio(),height()*devicePixelRatio(),QImage::Format_RGB32);

        currentBackgroundPic.fill(qRgb(50,50,50));
        QPainter paint;
        paint.begin(&currentBackgroundPic);
        paint.setViewTransformEnabled(true);

        int numCol;
        int numRows;
        QPen pn;
        pn.setColor(qRgb(100,100,100));
        pn.setWidth(1);


        numCol = floor(sqrt(mipMaps.length()));
        numRows = ceil(mipMaps.length()/(double)numCol);

        int tmpMipmapIndex = 0;
        QRect currentDrawWindow;

        currentDrawWindow.setWidth(currentBackgroundPic.width()/numCol);
        currentDrawWindow.setHeight(currentBackgroundPic.height()/numRows);

        int panelWidth = currentBackgroundPic.width()/numCol;
        int panelHeight = currentBackgroundPic.height()/numRows;

        int marginY = ((height()*devicePixelRatio())-(panelHeight*numRows))/2;
        int marginX = ((width()*devicePixelRatio())-(panelWidth*numCol))/2;


        //Draw the scatter images
        for (int i=0; i<numRows; i++) {
            for (int j=0; j<numCol; j++) {

                if (tmpMipmapIndex < mipMaps.length()) {
                    QImage tempImage = mipMaps[tmpMipmapIndex]->getCurrentImage(currentDrawWindow);

                    QPixmap tempPixMap;
                    tempPixMap.convertFromImage(tempImage);
                    //paint.drawImage((j*panelWidth)+marginX,(i*panelHeight)+marginY,tempImage);
                    paint.drawPixmap((j*panelWidth)+marginX,(i*panelHeight)+marginY,tempPixMap);
                    tmpMipmapIndex++;
                }
            }
        }

        //Draw the image borders
        paint.setPen(pn);
        for (int i=0; i<numCol; i++) {
            for (int j=0; j<numRows; j++) {
                paint.drawRect((i*panelWidth)+marginX,(j*panelHeight)+marginY,panelWidth,panelHeight);
            }
        }

        //Highlight the panels that have polygons defined for the currently selected cluster
        pn.setColor(clusterColors.at(currentCluster));
        paint.setPen(pn);
        for (int CDind = 0; CDind < currentClusterDefinedXPanels.length(); CDind++) {
            paint.drawRect((currentClusterDefinedXPanels.at(CDind)*panelWidth)+marginX+2,(currentClusterDefinedYPanels.at(CDind)*panelHeight)+marginY+2,panelWidth-4,panelHeight-4);
        }

        //Hightlight the panel that the mouse is over

        if (currentMouseXPanel > -1) {
            //pn.setColor(qRgb(150,20,20));
            pn.setColor(qRgb(200,200,200));
            pn.setWidth(2);
            paint.setPen(pn);
            paint.drawRect((currentMouseXPanel*panelWidth)+marginX,(currentMouseYPanel*panelHeight)+marginY,panelWidth,panelHeight);
        }

        paint.end();

    } else {
        //Just plot one panel

        currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();

    }

    emit newBackgroundReady();
}
void ScatterPlotWindow::showNewBackgroundPlot() {
    scene->setBackgroundPic(&currentBackgroundPic);
}
void ScatterPlotWindow::updateBackgroundPlot() {


    //QFuture<void> future = QtConcurrent::run(&ScatterPlotWindow::calculateBackgroundPlot,this);
    calculateBackgroundPlot();

    /*int panelWidth = width()*devicePixelRatio();
    int panelHeight = height()*devicePixelRatio();
    if (displayMultipleProjections) {
        int numCol, numRows;
        numCol = floor(sqrt(mipMaps.length()));

        numRows = ceil(mipMaps.length()/(double)numCol);
        panelWidth = width()*devicePixelRatio()/numCol;
        panelHeight = height()*devicePixelRatio()/numRows;

    }

    for (int i=0; i<mipMaps.length();i++) {
        mipMaps[i]->setDestWindow(QRect(0,0,panelWidth,panelHeight));
    }

    if (displayMultipleProjections) {

        currentBackgroundPic = QImage(width()*devicePixelRatio(),height()*devicePixelRatio(),QImage::Format_RGB32);

        currentBackgroundPic.fill(qRgb(50,50,50));
        QPainter paint;
        paint.begin(&currentBackgroundPic);
        paint.setViewTransformEnabled(true);

        int numCol;
        int numRows;
        QPen pn;
        pn.setColor(qRgb(100,100,100));
        pn.setWidth(1);


        numCol = floor(sqrt(mipMaps.length()));
        numRows = ceil(mipMaps.length()/(double)numCol);

        int tmpMipmapIndex = 0;
        QRect currentDrawWindow;

        currentDrawWindow.setWidth(currentBackgroundPic.width()/numCol);
        currentDrawWindow.setHeight(currentBackgroundPic.height()/numRows);

        int panelWidth = currentBackgroundPic.width()/numCol;
        int panelHeight = currentBackgroundPic.height()/numRows;

        int marginY = ((height()*devicePixelRatio())-(panelHeight*numRows))/2;
        int marginX = ((width()*devicePixelRatio())-(panelWidth*numCol))/2;


        //Draw the scatter images
        for (int i=0; i<numRows; i++) {
            for (int j=0; j<numCol; j++) {

                if (tmpMipmapIndex < mipMaps.length()) {
                    QImage tempImage = mipMaps[tmpMipmapIndex]->getCurrentImage(currentDrawWindow);

                    QPixmap tempPixMap;
                    tempPixMap.convertFromImage(tempImage);
                    //paint.drawImage((j*panelWidth)+marginX,(i*panelHeight)+marginY,tempImage);
                    paint.drawPixmap((j*panelWidth)+marginX,(i*panelHeight)+marginY,tempPixMap);
                    tmpMipmapIndex++;
                }
            }
        }

        //Draw the image borders
        paint.setPen(pn);
        for (int i=0; i<numCol; i++) {
            for (int j=0; j<numRows; j++) {
                paint.drawRect((i*panelWidth)+marginX,(j*panelHeight)+marginY,panelWidth,panelHeight);                             
            }
        }

        //Highlight the panels that have polygons defined for the currently selected cluster
        pn.setColor(clusterColors.at(currentCluster));
        paint.setPen(pn);
        for (int CDind = 0; CDind < currentClusterDefinedXPanels.length(); CDind++) {
            paint.drawRect((currentClusterDefinedXPanels.at(CDind)*panelWidth)+marginX+2,(currentClusterDefinedYPanels.at(CDind)*panelHeight)+marginY+2,panelWidth-4,panelHeight-4);
        }

        //Hightlight the panel that the mouse is over

        if (currentMouseXPanel > -1) {
            //pn.setColor(qRgb(150,20,20));
            pn.setColor(qRgb(200,200,200));
            pn.setWidth(2);
            paint.setPen(pn);
            paint.drawRect((currentMouseXPanel*panelWidth)+marginX,(currentMouseYPanel*panelHeight)+marginY,panelWidth,panelHeight);
        }

        paint.end();

    } else {
        //Just plot one panel

        currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();

    }
    scene->setBackgroundPic(&currentBackgroundPic);*/




}

void ScatterPlotWindow::setDataPoint(const QVector<int> *inputData, int currentBufferIndex, int clustID) {

    //if (displayMultipleProjections) {
        for (int i=0; i<mipMaps.length(); i++) {
            mipMaps[i]->addPoint((qreal)inputData[mipMaps[i]->axes.first][currentBufferIndex],(qreal)inputData[mipMaps[i]->axes.second][currentBufferIndex],clustID);
        }

        //currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();
        //scene->setBackgroundPic(&currentBackgroundPic);
    //} else {


        scene->setDataPoint(inputData, currentBufferIndex);
    //}
    //displayData[currentBufferIndex].x = inputData[currentXdisplay][currentBufferIndex];
    //displayData[currentBufferIndex].y = inputData[currentYdisplay][currentBufferIndex];

}

void ScatterPlotWindow::clearData() {
    for (int i=0; i<mipMaps.length(); i++) {
        mipMaps[i]->clearData();
    }

    updateBackgroundPlot();
}

void ScatterPlotWindow::setChannels(int channelX, int channelY, const QVector<int> *inputData, const QVector<quint8> &clustID) {
    currentXdisplay = channelX;
    currentYdisplay = channelY;

    if (!displayMultipleProjections) {
        for (int i=0; i<mipMaps.length(); i++) {
            if ((mipMaps[i]->axes.first == currentXdisplay) && (mipMaps[i]->axes.second == currentYdisplay)) {
                currentMipmapIndex = i;
                //mipMaps[i]->setPoints(0,bufferSize,inputData, clustID);
                break;
            }
        }


        currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();
        scene->setBackgroundPic(&currentBackgroundPic);
    }

    //For older opengl plotting
    //scene->setChannels(channelX, channelY, inputData);

    //Only show the polygons for the current axes
    if (!displayMultipleProjections) {
        RubberBandPolygon::Axes ax;
        for (int i=0; i<polygons[currentNTrode].length(); i++) {
            ax = polygons[currentNTrode][i]->getAxes();
            if ((ax.x == currentXdisplay) && (ax.y == currentYdisplay)) {
                polygons[currentNTrode][i]->setVisible(true);
            } else {
                polygons[currentNTrode][i]->setVisible(false);
            }
        }
    }

    scene->update();
}

void ScatterPlotWindow::setDisplayWindow(QVector<int> minDisplay, QVector<int> maxDisplay) {

    maxAmplitude.resize(maxDisplay.length());
    minAmplitude.resize(minDisplay.length());
    for (int i=0; i<maxAmplitude.length(); i++) {

        //maxAmplitude[i] = (maxDisplay[i]*65536)/AD_CONVERSION_FACTOR;
        //minAmplitude[i] = (minDisplay[i]*65536)/AD_CONVERSION_FACTOR;

        maxAmplitude[i] = maxDisplay[i]*(1/spikeConf->ntrodes[currentNTrode]->spike_scaling_to_uV);
        minAmplitude[i] = minDisplay[i]*(1/spikeConf->ntrodes[currentNTrode]->spike_scaling_to_uV);
    }

    int panelWidth = width();
    int panelHeight = height();
    if (displayMultipleProjections) {
        int numCol, numRows;


        numCol = floor(sqrt(mipMaps.length()));
        /*
        if (mipMaps.length() <= 6) {
            numCol = 2;
        } else {
            numCol = 3;
        }*/
        numRows = ceil(mipMaps.length()/(double)numCol);
        panelWidth = currentBackgroundPic.width()/numCol;
        panelHeight = currentBackgroundPic.height()/numRows;

    }

    for (int i=0; i<mipMaps.length();i++) {

        int tmpX = mipMaps[i]->axes.first;
        int tmpY = mipMaps[i]->axes.second;
        QRectF dWin = QRectF((qreal)minAmplitude[tmpX],(qreal)minAmplitude[tmpY],(qreal)maxAmplitude[tmpX],(qreal)maxAmplitude[tmpY]);

        mipMaps[i]->setWindows(dWin,QRect(0,0,panelWidth,panelHeight));

    }

    //For older opengl plotting
    //scene->setMaxDisplay(maxDisplayVal);

    for (int i=0; i<polygons[currentNTrode].length(); i++) {
        RubberBandPolygon::Axes ax = polygons[currentNTrode][i]->getAxes();
        polygons[currentNTrode][i]->setCurrentAmpWindow(QRectF((qreal)minAmplitude[ax.x],(qreal)minAmplitude[ax.y],(qreal)maxAmplitude[ax.x],(qreal)maxAmplitude[ax.y]));
    }

}

void ScatterPlotWindow::setMaxDisplay(int channel, int maxDisplayVal) {
    //Changes the maximum amplitude of the display range
    //maxAmplitude[channel] = (maxDisplayVal*65536)/AD_CONVERSION_FACTOR;

    setMaxDisplay(maxDisplayVal);

}

void ScatterPlotWindow::setMaxDisplay(int maxDisplayVal) {

        for (int i=0; i<maxAmplitude.length(); i++) {
            //maxAmplitude[i] = (maxDisplayVal*65536)/AD_CONVERSION_FACTOR;
            maxAmplitude[i] = maxDisplayVal*(1/spikeConf->ntrodes[currentNTrode]->spike_scaling_to_uV);
        }

        maxDispVal_uV = maxDisplayVal;
        QRectF dWin = QRectF(0.0,0.0,(qreal)maxAmplitude[0],(qreal)maxAmplitude[0]);

        for (int i=0; i<mipMaps.length();i++) {
            mipMaps[i]->setWindows(dWin,QRect(0,0,width(),height()));
        }

        updateBackgroundPlot();

        //For older opengl plotting
        //scene->setMaxDisplay(maxDisplayVal);

        for (int i=0;i<polygons[currentNTrode].length();i++) {
            polygons[currentNTrode][i]->setCurrentAmpWindow(QRectF(0.0,0.0,(qreal)maxAmplitude[currentXdisplay],(qreal)maxAmplitude[currentYdisplay]));
        }

}


void ScatterPlotWindow::addExcludePolygon() {
    RubberBandPolygon *newPoly = new RubberBandPolygon(isClusterShowing(currentCluster));
    newPoly->setExcludeType();

    connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));
    connect(newPoly,SIGNAL(shapeChanged(int,int)),this,SLOT(calculatePointsInsidePolygon(int,int)));

    polygons[currentNTrode].append(newPoly);
    scene->addItem(newPoly);
}

void ScatterPlotWindow::addZonePolygon() {
    RubberBandPolygon *newPoly = new RubberBandPolygon(isClusterShowing(currentCluster));
    newPoly->setZoneType();

    connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));

    polygons[currentNTrode].append(newPoly);
    scene->addItem(newPoly);
}

void ScatterPlotWindow::addIncludePolygon() {
    int polyBitInd;
    bool foundEmptySlot = false;

    for (int i=0; i<polygonsTaken[currentNTrode].length(); i++) {
        if (!polygonsTaken[currentNTrode][i]) {
            polyBitInd = i;
            polygonsTaken[currentNTrode][i] = true;
            foundEmptySlot = true;
        }
    }

    if (!foundEmptySlot) {
        polygonsTaken[currentNTrode].append(true);
        polyBitInd = polygonsTaken[currentNTrode].length()-1;
    }

    RubberBandPolygon *newPoly = new RubberBandPolygon();
    newPoly->setIncludeType();
    newPoly->setPolyBitInd(polyBitInd);
    newPoly->setAxes(currentXdisplay,currentYdisplay);

    connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));
    connect(newPoly,SIGNAL(shapeChanged(int,int)),this,SLOT(calculatePointsInsidePolygon(int,int)));
    connect(newPoly,SIGNAL(rightClicked(QPoint)),this,SLOT(polygonRightClicked(const QPoint&)));
    polygons[currentNTrode].append(newPoly);

    polygons[currentNTrode].last()->setCurrentAmpWindow(QRectF(0.0,0.0,(qreal)maxAmplitude[currentXdisplay],(qreal)maxAmplitude[currentYdisplay]));
    polygons[currentNTrode].last()->setColor(QColor(0,255,0));
    scene->addItem(newPoly);

    updateCurrentClusterPanels();


}

void ScatterPlotWindow::spaceClicked() {
    //the background was clicked, so turn off all highlights
    setNoHighlight();
}

void ScatterPlotWindow::setNoHighlight() {
    for (int i=0; i<polygons[currentNTrode].length(); i++) {
        polygons[currentNTrode][i]->removeHighlight();
    }
    currentlySelectedPolygon = -1;
}

void ScatterPlotWindow::abandonDrawing() {
    if (currentlyDrawing) {
        //A polygon is currently being drawn, so we delete the polygon
        delete polygons[currentNTrode].takeLast();
        currentlyDrawing = false;
    }
}

void ScatterPlotWindow::polygonHighlighted() {
    RubberBandPolygon* highlightedPoly = dynamic_cast<RubberBandPolygon*>(sender());

    for (int i=0; i<polygons[currentNTrode].length(); i++) {
        if (polygons[currentNTrode][i] == highlightedPoly) {
            currentlySelectedPolygon = i;
        } else {
            polygons[currentNTrode][i]->removeHighlight();
        }
    }
}

void ScatterPlotWindow::polygonRightClicked(const QPoint& pos) {


    RubberBandPolygon* clickedPoly = dynamic_cast<RubberBandPolygon*>(sender());
    int polygonNumberClicked = -1;
    for (int i=0; i<polygons[currentNTrode].length(); i++) {
        if (polygons[currentNTrode][i] == clickedPoly) {
            polygonNumberClicked = i;
            break;
        }
    }

    if (polygonNumberClicked > -1) {
        showPolygonContextMenu(this->mapToGlobal(pos),currentNTrode,polygonNumberClicked);
    }
}

void ScatterPlotWindow::showPolygonContextMenu(const QPoint& pos, int nTrode, int polygonNumber) {

//    QPoint globalPos = this->mapToGlobal(pos);
    QPoint globalPos = pos;
    QMenu channelMenu;
    RubberBandPolygon * currRband = polygons[nTrode].value(polygonNumber, NULL);
    //channelMenu.addAction("nTrode settings...");
    if(currRband == NULL)
        return;


    QString deleteOption = "Delete entire cluster";
    QString psthOption = "Create PSTH...";
    QString showOption = "Enable Cluster";
    QString hideOption = "Disable Cluster";
    channelMenu.addAction(deleteOption);
    channelMenu.addAction(psthOption);
    channelMenu.addAction(showOption);
    channelMenu.addAction(hideOption);

    QAction* selectedItem = channelMenu.exec(globalPos);
    if (selectedItem) {
       // something was chosen, do stuff
       if (selectedItem->text() == psthOption) {
            //Open PSTH dialog
           emit PSTHRequested(currRband->getClusterNum());

       }
       else if (selectedItem->text() == deleteOption) {
           deleteCluster(nTrode,currRband->getClusterNum());
       }

       else if (selectedItem->text() == hideOption){
           //Hides this rubber band's cluster
           enableDisableClusters(false, currRband->getAssignedClusterNum(), nTrode);
       }
       else if (selectedItem->text() == showOption){
           //Shows this rubber band's cluster
           enableDisableClusters(true, currRband->getAssignedClusterNum(), nTrode);
       }
    } else {
      // nothing was chosen
    }
}

//! Controls showing/hiding of clusters
//! @param show If true, clusters are shown, else, are hidden
//! @param cluster Cluster number
//! @param nTrode NTrode number
//! @param singleCluster If true, 'show' is applied to the single cluster. Else, 'show' is applied to all except the given cluster
void ScatterPlotWindow::enableDisableClusters(bool show, quint8 cluster, int nTrode){

    for(QVector<RubberBandPolygon*>::iterator poly = polygons[nTrode].begin(); poly != polygons[nTrode].end(); ++poly){
        RubberBandPolygon *r = *poly;
        if(r->getAssignedClusterNum() == cluster){
            r->setClusterShowing(show);
        }
    }
}

bool ScatterPlotWindow::isClusterShowing(quint8 cluster){
    for(int i = 0; i < polygons[currentNTrode].length(); i++){
        if(polygons[currentNTrode][i]->getAssignedClusterNum() == cluster)
            return polygons[currentNTrode][i]->isClusterShowing();
    }
    return true;
}

void ScatterPlotWindow::setTool(int toolNum) {
    currentTool = toolNum;
    if (currentTool != EDITTOOL_ID) {
        //We only allow highlighting when the edit tool is active
        setNoHighlight();
    }
}

void ScatterPlotWindow::setClusterColors(const QList<QColor> colors) {
    clusterColors = colors;
    /*
    for (int i = 0; i < mipMaps.length(); i++) {
        mipMaps[i]->setColorTable(colors);
    }*/
}

void ScatterPlotWindow::updateCurrentClusterPanels() {
    int numCol;
    int numRows;
    numCol = floor(sqrt(mipMaps.length()));
    numRows = ceil(mipMaps.length()/(double)numCol);

    currentClusterDefinedXPanels.clear();
    currentClusterDefinedYPanels.clear();
    for (int i=0; i<polygons[currentNTrode].length(); i++) {
        //Does this polygon belong to the selected cluster number?
        if (polygons[currentNTrode][i]->getAssignedClusterNum() == currentCluster) {

            //Go through all of the scatter plots to calculate which panel indeces the polygon is in
            //We do this to highlight those panels in multi view mode
            for (int MMsearch = 0; MMsearch < mipMaps.length(); MMsearch++) {
                if ((mipMaps[MMsearch]->axes.first == polygons[currentNTrode][i]->getAxes().x) && (mipMaps[MMsearch]->axes.second == polygons[currentNTrode][i]->getAxes().y)) {
                    currentClusterDefinedYPanels.push_back(floor((double)MMsearch/numCol));
                    currentClusterDefinedXPanels.push_back(MMsearch%numCol);
                }
            }
        }
    }



}

void ScatterPlotWindow::setCurrentCluster(int clust) {
    //A new cluster number was selected

    currentCluster = clust;
    updateCurrentClusterPanels();

}

void ScatterPlotWindow::calculatePointsInsidePolygon(int clustInd, int polygonBitIndex) {

    ClusterPolygonSet polyInfo;
    for (int i=0; i<polygons[currentNTrode].length(); i++) {
        if (polygons[currentNTrode][i]->getPolyBitInd() == polygonBitIndex) {
            //This polygon has the right bit index, so it's a match
            polyInfo.xAxis = polygons[currentNTrode][i]->getAxes().x;
            polyInfo.yAxis = polygons[currentNTrode][i]->getAxes().y;
            polyInfo.points = polygons[currentNTrode][i]->getPoints();
            polyInfo.bitInd = polygonBitIndex;
            polyInfo.clusterInd = clustInd;
            break;
        }
    }


    emit newClusterCalculationNeeded(clustInd, polyInfo, polygonBitIndex );


}

QList<ClusterPolygonSet> ScatterPlotWindow::getPolygonsForCluster(int clusterInd) {
    QList<ClusterPolygonSet> polyInfo;
    for (int i=0; i<polygons[currentNTrode].length(); i++) {
        if (polygons[currentNTrode][i]->getAssignedClusterNum() == clusterInd) {
            //This polygon belongs to the desired cluster
            ClusterPolygonSet newPoly;
            newPoly.xAxis = polygons[currentNTrode][i]->getAxes().x;
            newPoly.yAxis = polygons[currentNTrode][i]->getAxes().y;
            newPoly.points = polygons[currentNTrode][i]->getPoints();
            newPoly.bitInd = polygons[currentNTrode][i]->getPolyBitInd();
            polyInfo.append(newPoly);
        }
    }
    return polyInfo;
}


void ScatterPlotWindow::addPolygons(QVector<QList<ClusterPolygonSet> > polylist) {
    //Used to load all polygons from a saved file

    //Loop through the ntrodes
    for (int nt = 0; nt < polylist.length(); nt++) {
        //Loop through the polygons for this ntrode
        for (int polyInd = 0; polyInd < polylist[nt].length(); polyInd++) {

            //Determine the id slot that this polygon will have
            int polyBitInd;
            bool foundEmptySlot = false;
            for (int i=0; i<polygonsTaken[nt].length(); i++) {
                if (!polygonsTaken[nt][i]) {
                    polyBitInd = i;
                    polygonsTaken[nt][i] = true;
                    foundEmptySlot = true;
                }
            }
            if (!foundEmptySlot) {
                polygonsTaken[nt].append(true);
                polyBitInd = polygonsTaken[nt].length()-1;
            }

            polylist[nt][polyInd].bitInd = polyBitInd;

            //Create the graphical polygon object
            RubberBandPolygon *newPoly = new RubberBandPolygon();
            newPoly->setIncludeType();
            newPoly->setPolyBitInd(polyBitInd);
            newPoly->setAxes(polylist[nt].at(polyInd).xAxis, polylist[nt].at(polyInd).yAxis);

            connect(newPoly,SIGNAL(hasHighlight()),this,SLOT(polygonHighlighted()));
            connect(newPoly,SIGNAL(shapeChanged(int,int)),this,SLOT(calculatePointsInsidePolygon(int,int)));
            connect(newPoly,SIGNAL(rightClicked(QPoint)),this,SLOT(polygonRightClicked(const QPoint&)));
            polygons[nt].append(newPoly);

            polygons[nt].last()->setCurrentAmpWindow(QRectF(0.0,0.0,(qreal)maxAmplitude[polylist[nt].at(polyInd).xAxis],(qreal)maxAmplitude[polylist[nt].at(polyInd).yAxis]));
            polygons[nt].last()->setClusterNum(polylist[nt].at(polyInd).clusterInd);
            polygons[nt].last()->setColor(clusterColors[polylist[nt].at(polyInd).clusterInd]);

            //Add the new polygon to the scene
            scene->addItem(newPoly);

            //Set the points of the polygon
            polygons[nt].last()->setPoints(polylist[nt].at(polyInd).points);
            polygons[nt].last()->setVisible(false);

        }
    }

    emit newClusterCalculationNeededForAllPolygons(polylist);
    updateCurrentClusterPanels();
}


void ScatterPlotWindow::mousePressEvent(QMouseEvent *event) {
    //When the window is clicked, the behavior depends on
    //which tool is currently selected.

    if (event->button() == Qt::LeftButton) {
        if (currentlyDrawing) {
            polygons[currentNTrode].last()->addPoint(event->localPos());
            return;
        }

        if (!displayMultipleProjections) {
            if (currentTool == INCLUDETOOL_ID) {
                //The polygon drawing tool is currently selected, so we create a new polygon.  If a polygon
                //for the current cluster number exists in the current axes, we delete it first.
                for (int i = 0; i < polygons[currentNTrode].length(); i++) {

                    if ((polygons[currentNTrode][i]->getAssignedClusterNum() == currentCluster) && (polygons[currentNTrode][i]->getAxes().x == currentXdisplay) && (polygons[currentNTrode][i]->getAxes().y == currentYdisplay)){
                        //There is already a polygon for this cluster on this projection
                        ClusterPolygonSet p;
                        p.bitInd = polygons[currentNTrode][i]->getPolyBitInd();
                        p.xAxis = polygons[currentNTrode][i]->getAxes().x;
                        p.yAxis = polygons[currentNTrode][i]->getAxes().y;
                        p.points = polygons[currentNTrode][i]->getPoints();

                        polygonsTaken[currentNTrode][polygons[currentNTrode][i]->getPolyBitInd()] = false;
                        delete polygons[currentNTrode].takeAt(i);
                        //setNoHighlight();

                        emit polygonDeleted(currentCluster,p.bitInd);

                        break;
                    }
                }

                addIncludePolygon();

                polygons[currentNTrode].last()->setClusterNum(currentCluster);
                polygons[currentNTrode].last()->setColor(clusterColors[currentCluster]);



                currentlyDrawing = true;
                polygons[currentNTrode].last()->addPoint(event->localPos());
                polygons[currentNTrode].last()->addPoint(event->localPos());


            } else if ((currentTool == EDITTOOL_ID)&&(itemAt(event->pos()))) {
                // Clicked on polygon, pass on event to children
                QGraphicsView::mousePressEvent(event);
            } else if (currentTool == EDITTOOL_ID) {
                setNoHighlight();
            } else if (currentTool == PANTOOL_ID) {
                //used to pan around.  Not yet working.
            }
        } else {
            if (currentMouseXPanel > -1) {
                emit mouseOverChannels(-1,-1);
                emit channelPairSelected(currentPairIndex);
            }
        }
    } else {

        QGraphicsView::mousePressEvent(event);
    }

}

void ScatterPlotWindow::mouseMoveEvent(QMouseEvent *event) {

    if (currentlyDrawing) {
        //A poolygon is currently being drawn, so we update the shape of the polygon
        //to track the mouse's location.

        polygons[currentNTrode].last()->moveLastPoint(event->localPos());
    } else if (displayMultipleProjections) {
        //If all projection pairs are displayed, the user can click
        //on a panel to select it for single-view mode. Here we calculate
        //which panel the mouse is over.

        int numCol;
        int numRows;

        numCol = floor(sqrt(mipMaps.length()));
        numRows = ceil(mipMaps.length()/(double)numCol);

        int panelWidth = currentBackgroundPic.width()/numCol;
        int panelHeight = currentBackgroundPic.height()/numRows;

        int marginY = ((height()*devicePixelRatio())-(panelHeight*numRows))/2;
        int marginX = ((width()*devicePixelRatio())-(panelWidth*numCol))/2;



        currentMouseXPanel = ((event->localPos().x()*devicePixelRatio())-marginX)/panelWidth;
        currentMouseYPanel = ((event->localPos().y()*devicePixelRatio())-marginY)/panelHeight;

        if (((currentMouseYPanel*numCol)+currentMouseXPanel >= mipMaps.length()) || ((currentMouseYPanel*numCol)+currentMouseXPanel < 0)) {
            currentMouseXPanel = -1;
            currentMouseYPanel = -1;
            currentMouseChannels.first = -1;
            currentMouseChannels.second = -1;
            currentPairIndex = -1;
        } else {

            currentMouseChannels.first = mipMaps[(currentMouseYPanel*numCol)+currentMouseXPanel]->axes.first;
            currentMouseChannels.second = mipMaps[(currentMouseYPanel*numCol)+currentMouseXPanel]->axes.second;
            if ((currentMouseChannels.first != lastMouseChannels.first)||(currentMouseChannels.second != lastMouseChannels.second)) {
                updateBackgroundPlot();
                scene->update();
            }
            lastMouseChannels.first = currentMouseChannels.first;
            lastMouseChannels.second = currentMouseChannels.second;
            currentPairIndex = (currentMouseYPanel*numCol)+currentMouseXPanel;
        }

        emit mouseOverChannels(currentMouseChannels.first, currentMouseChannels.second);

    } else {
        QGraphicsView::mouseMoveEvent(event);
    }
}

void ScatterPlotWindow::leaveEvent(QEvent *event) {

    //Mouse is no longer over any panels
    currentMouseXPanel = -1;
    currentMouseYPanel = -1;
    currentMouseChannels.first = -1;
    currentMouseChannels.second = -1;
    currentPairIndex = -1;
    emit mouseOverChannels(currentMouseChannels.first, currentMouseChannels.second);


    //When mouse leaves the window, (alt tab, move away from plot, etc), don't remove last point, just make the polygon if user has already made the first two points
    /*if(currentlyDrawing){
        currentlyDrawing = false;
        if(polygons[currentNTrode].last()->getPoints().length() < 3){
            polygons[currentNTrode].last()->removeLastPoint();
            return;
        }
        calculatePointsInsidePolygon(currentCluster, polygons[currentNTrode].last()->getPolyBitInd());

        QString spikeEvent = QString("spike_ntrode_%1_cluster_%2").arg(spikeConf->ntrodes.at(currentNTrode)->nTrodeId).arg(currentCluster);
        emit broadcastNewEventReq(spikeEvent);
    }*/
}

void ScatterPlotWindow::mouseDoubleClickEvent(QMouseEvent *event) {
    if (currentlyDrawing) {
        currentlyDrawing = false;
        polygons[currentNTrode].last()->removeLastPoint();
        enableDisableClusters(true, currentCluster, currentNTrode);
        //calculateConsideredPixels();
        calculatePointsInsidePolygon(currentCluster, polygons[currentNTrode].last()->getPolyBitInd());

        QString spikeEvent = QString("spike_ntrode_%1_cluster_%2").arg(spikeConf->ntrodes.at(currentNTrode)->nTrodeId).arg(currentCluster);
        emit broadcastNewEventReq(spikeEvent);
        updateCurrentClusterPanels();
        updateBackgroundPlot();
        scene->update();

    }
}

void ScatterPlotWindow::wheelEvent(QWheelEvent *wheelEvent) {
    // qreal ypos = wheelEvent->pos().y(); // UNUSED
    double changeAmount;
    //int currentValue = plotScrollControl->value();
    //int newValue;
    double scrollDivisor = 5.0;


    changeAmount = (float)wheelEvent->angleDelta().y()/scrollDivisor;
    if ((changeAmount > 0.0) && (changeAmount < 1.0)) {
        changeAmount = 1.0;
    } else if ((changeAmount < 0.0) && (changeAmount > -1.0)) {
        changeAmount = -1.0;
    }

    //newValue = currentValue+changeAmount;


    //int changeAmount;
    /*if (wheelEvent->delta() > 0) {
        changeAmount = -25;
    } else {
        changeAmount = 25;
    }*/

    emit zoom(-changeAmount);

}

void ScatterPlotWindow::deleteAllClusters() {
    //Deletes all clusters from all nTrodes

    bool somethingDeleted = false;

    for (int nt=0;nt < polygons.length(); nt++) {
        bool done=false;

        while (!done) {
            done = true;
            for (int i=0; i<polygons[nt].length(); i++) {

                somethingDeleted = true;
                polygonsTaken[nt][polygons[nt][i]->getPolyBitInd()] = false;
                delete polygons[nt].takeAt(i);
                done = false;
                break;
            }
        }
    }

    if (somethingDeleted) {
        //At least one polygon was deleted
        setNoHighlight();

        emit allClustersDeleted();
        updateCurrentClusterPanels();


        //This needs to be fixed, but sending out multiple messages appears to not work well.  Wilcard method could work, be we should wait for new networking API before deciding.
        //QString spikeEventToDelete = QString("Spike occured in NTrode*"); //Use wildcard to remove all events that begin like this.  TODO: add wildcard processing in network code.
        //emit broadcastEventRemoveReq(spikeEventToDelete);



    }

}

void ScatterPlotWindow::deleteCluster(int nTrodeIndex, quint8 clusterInd) {
    //Deletes an entire cluster.  All polygons associated with the cluster are deleted.

    if (polygons.length() <= nTrodeIndex) {
        return;
    }

    bool done=false;
    bool somethingDeleted = false;
    while (!done) {
        done = true;
        for (int i=0; i<polygons[nTrodeIndex].length(); i++) {
            if (polygons[nTrodeIndex][i]->getClusterNum() == clusterInd) {
                //This polygon belongs to the desired cluster
                somethingDeleted = true;
                polygonsTaken[nTrodeIndex][polygons[nTrodeIndex][i]->getPolyBitInd()] = false;
                delete polygons[nTrodeIndex].takeAt(i);
                done = false;
                break;
            }
        }
    }

    if (somethingDeleted) {
        //At least one polygon was deleted
        setNoHighlight();

        emit clusterDeleted(nTrodeIndex,clusterInd);

        //Remove the spike event menu item from all modules
        QString spikeEventToDelete = QString("Spike occured in NTrode %1 Cluster %2").arg(spikeConf->ntrodes.at(nTrodeIndex)->nTrodeId).arg(clusterInd);
        emit broadcastEventRemoveReq(spikeEventToDelete);

        updateCurrentClusterPanels();
    }


}

void ScatterPlotWindow::keyPressEvent(QKeyEvent *event) {

    if ((event->key() == Qt::Key_Delete)||(event->key() == Qt::Key_Backspace)) {
        //delete the selected polygon
        if (currentlySelectedPolygon != -1) {

            int selectedClusterInd = polygons[currentNTrode][currentlySelectedPolygon]->getAssignedClusterNum();
            int selectedPolyBitIndex = polygons[currentNTrode][currentlySelectedPolygon]->getPolyBitInd();
            ClusterPolygonSet p;
            p.bitInd = polygons[currentNTrode][currentlySelectedPolygon]->getPolyBitInd();
            p.xAxis = polygons[currentNTrode][currentlySelectedPolygon]->getAxes().x;
            p.yAxis = polygons[currentNTrode][currentlySelectedPolygon]->getAxes().y;
            p.points = polygons[currentNTrode][currentlySelectedPolygon]->getPoints();
            polygonsTaken[currentNTrode][polygons[currentNTrode][currentlySelectedPolygon]->getPolyBitInd()] = false;
            delete polygons[currentNTrode].takeAt(currentlySelectedPolygon);
            setNoHighlight();
            //calculateConsideredPixels();
            //calculatePointsInsidePolygon(selectedClusterInd,selectedPolyBitIndex);

            emit polygonDeleted(selectedClusterInd,selectedPolyBitIndex);
            updateCurrentClusterPanels();

            //This should only occur if it was the last polygon for the cluster
            QList<ClusterPolygonSet> pList = getPolygonsForCluster(selectedClusterInd);
            if (pList.isEmpty()) {
                QString spikeEventToDelete = QString("Spike occured in NTrode %1 Cluster %2").arg(spikeConf->ntrodes.at(currentNTrode)->nTrodeId).arg(selectedClusterInd);
                emit broadcastEventRemoveReq(spikeEventToDelete);
            }

            QString spikeEventToDelete = QString("spike_ntrode_%1_cluster_%2").arg(spikeConf->ntrodes.at(currentNTrode)->nTrodeId).arg(currentCluster);
            emit broadcastEventRemoveReq(spikeEventToDelete);
        }
    } else if (event->key() == Qt::Key_Escape) {
        if (currentlyDrawing) {
            //A polygon is currently being drawn, and the esc key cancels the drawing.
            delete polygons[currentNTrode].takeLast();
            currentlyDrawing = false;
        }
    }
}

void ScatterPlotWindow::refresh() {


    for (int i=0; i<mipMaps.length();i++) {
        mipMaps[i]->setDestWindow(QRect(0,0,this->width(),this->height()));
    }


    scene->setSceneRect(0,0,this->width(),this->height());



    /*for (int i=0; i<polygons.length(); i++) {
        polygons[currentNTrode][i]->updateSize();
    }*/


    updateBackgroundPlot();

    //currentBackgroundPic = mipM
}

void ScatterPlotWindow::resizeEvent(QResizeEvent *event) {


    //dispWin->resize(event->size().width(),event->size().height());
    int panelWidth = event->size().width()*devicePixelRatio();
    int panelHeight = event->size().height()*devicePixelRatio();
    if (displayMultipleProjections) {
        int numCol, numRows;
        numCol = floor(sqrt(mipMaps.length()));

        numRows = ceil(mipMaps.length()/(double)numCol);
        panelWidth = event->size().width()*devicePixelRatio()/numCol;
        panelHeight = event->size().height()*devicePixelRatio()/numRows;

    }

    for (int i=0; i<mipMaps.length();i++) {
        mipMaps[i]->setDestWindow(QRect(0,0,panelWidth,panelHeight));
    }

    scene->setSceneRect(0,0,event->size().width(),event->size().height());



    for (int i=0; i<polygons[currentNTrode].length(); i++) {

        polygons[currentNTrode][i]->updateSize();
    }

    updateBackgroundPlot();

    //currentBackgroundPic = mipMaps[currentMipmapIndex]->getCurrentImage();
    //scene->setBackgroundPic(&currentBackgroundPic);




}










//NtrodeDisplayWidget contains all plot and control objects for one nTrode.
NtrodeDisplayData::NtrodeDisplayData(QObject *parent, int nTrodeID, int bufferSize) :
  QObject(parent)
{



    nTrodeNumber = nTrodeID;
    currentBufferIndex = 0;
    bufferRecentlyCleared = true;
    currentXdisplay = 0;
    currentYdisplay = 1;
    currentChannelListIndex = 0;
    writeBlock = 0;
    beginningOfLog = 0;

    for (int i=0; i<NUMCLUSTERS; i++) {
        clustersDefined[i] = false;
    }

    numberOfChannels = spikeConf->ntrodes[nTrodeID]->hw_chan.length();
    int totalNumChannels = hardwareConf->NCHAN;

    //For very large nTrodes, we need to limit the resolution of the mipmaps
    int mipMapRes;
    if ((numberOfChannels <= 4)&&(totalNumChannels < 1024)) {
        mipMapRes = 5000;
    } else if ((numberOfChannels > 4) && (numberOfChannels <= 8) && (totalNumChannels < 1024))
        mipMapRes = 2500;
    else {
        mipMapRes = 1000;
    }

    scatterBufferSize = bufferSize;

    if (numberOfChannels > 1) {
        scatterData.resize(numberOfChannels);
    } else {

        scatterData.resize(2);
    }

//    meanPeak = 0;
    wavePeaks.fill(0, scatterBufferSize);
    for (int i = 0; i < scatterData.size() ; i++) {
        scatterData[i].fill(-1, scatterBufferSize);
    }

    pointClusterOwnership.resize(scatterBufferSize);
    pointClusterOwnership.fill(0);

    spikeTimes.resize(scatterBufferSize);
    spikeTimes.fill(0);

    if (numberOfChannels > 1) {
        maxAmplitude.resize(numberOfChannels);
        minAmplitude.resize(numberOfChannels);
        for (int i = 0; i < numberOfChannels; i++) {
            maxAmplitude[i] = spikeConf->ntrodes[nTrodeID]->maxDisp[i];
            minAmplitude[i] = 0;
            for (int j = i+1; j < numberOfChannels; j++) {
                //mipMaps.push_back(new ScatterPlotMipmap(QRectF(0.0,0.0,(2500.0*65536)/AD_CONVERSION_FACTOR,(2500.0*65536)/AD_CONVERSION_FACTOR),mipMapRes,mipMapRes));
                mipMaps.push_back(new ScatterPlotMipmap(QRectF(0.0,0.0,2500.0*(1/spikeConf->ntrodes[nTrodeID]->spike_scaling_to_uV),2500.0*(1/spikeConf->ntrodes[nTrodeID]->spike_scaling_to_uV)),mipMapRes,mipMapRes));

                mipMaps.last()->axes = qMakePair(i,j);

                //mipMaps_lowRange.push_back(new ScatterPlotMipmap(QRectF(0.0,0.0,(lowRangeMipmapMax*65536)/AD_CONVERSION_FACTOR,(lowRangeMipmapMax*65536)/AD_CONVERSION_FACTOR),5000,5000));
                //mipMaps_lowRange.last()->axes = qMakePair(i,j);
            }
        }
    } else {
        maxAmplitude.resize(2);
        minAmplitude.resize(2);
        for (int i = 0; i < 2; i++) {
            maxAmplitude[i] = spikeConf->ntrodes[nTrodeID]->maxDisp[0];
            minAmplitude[i] = 0;
            for (int j = i+1; j < 2; j++) {
                mipMaps.push_back(new ScatterPlotMipmap(QRectF(0.0,0.0,2500.0*(1/spikeConf->ntrodes[nTrodeID]->spike_scaling_to_uV),2500.0*(1/spikeConf->ntrodes[nTrodeID]->spike_scaling_to_uV)),mipMapRes,mipMapRes));
                mipMaps.last()->axes = qMakePair(i,j);


            }
        }
    }

    waveImage = new WaveFormPlotImage(scatterBufferSize, numberOfChannels, nTrodeNumber);
    QString tmpChannelCombo;
    if (numberOfChannels > 1) {
        for (int i = 0; i < numberOfChannels; i++) {

            for (int j = i+1; j < numberOfChannels; j++) {
                channelViewXList.push_back(i);
                channelViewYList.push_back(j);
                tmpChannelCombo = QString("Channels %1").arg(i+1) + QString(", ") + QString("%1").arg(j+1);
                channelViewStrings << tmpChannelCombo;
            }

        }
    } else {
        channelViewXList.push_back(0);
        channelViewYList.push_back(1);
        tmpChannelCombo = QString("Ch: 1 Max, 1 Min");
        channelViewStrings << tmpChannelCombo;

    }

}

NtrodeDisplayData::~NtrodeDisplayData() {

    for (int i=0; i<polyIncludeRecord.length();i++) {
        delete[] polyIncludeRecord[i];
        delete[] polyExcludeRecord[i];
    }

    while (mipMaps.length() > 0) {
        mipMaps.last()->deleteImages();
        delete mipMaps.takeLast();
    }
//    while(!waveImages.empty()){
//        waveImages.last()->deleteImages();
//        delete waveImages.takeLast();
//    }
    if(waveImage != NULL)
        delete waveImage;

}

void NtrodeDisplayData::setDataShowing() {

        for (int i=0;i < mipMaps.length(); i++) {
            mipMaps[i]->createImages();
        }
        setMipMapData();
//        setWaveImageData();
//        waveImage->setDataShowing(true);
}

void NtrodeDisplayData::setDataNotShowing() {
    for (int i=0;i < mipMaps.length(); i++) {
        mipMaps[i]->deleteImages();
        //mipMaps_lowRange[i]->deleteImages();
    }

//    for(int i = 0; i < waveImages.length(); i++){
//        waveImages[i]->deleteImages();
//    }
//    if(waveImage)
//        delete waveImage;
    waveImage->setDataShowing(false);

}

void NtrodeDisplayData::setMipMapData() {

    //We need use the current buffer with spike history to create the scatterplot
    //mipmaps.  The problem is that making all the mipmaps takes time and spikes are still coming in to
    //the buffer from other threads.  So, we quickly make a copy of the buffer (while locking
    //the threads that send spike data in) and use the copy instead.

    QVector<quint8>         pointClusterOwnershipCopy; //Each point can belong to one of 256 clusters
    pointClusterOwnershipCopy.resize(scatterBufferSize);

    QVector<QVector<int> >  scatterDataCopy;
    scatterDataCopy.resize(scatterData.length());

    for (int i = 0; i < scatterData.size() ; i++) {
        scatterDataCopy[i].resize(scatterBufferSize);
    }

    //Here we lock the threads that adds data to the buffer.  We want this to be as brief as possible
    newSpikeMutex.lock();
    for (int i = 0; i < scatterData.size() ; i++) {
        memcpy(scatterDataCopy[i].data(), scatterData[i].constData(), scatterBufferSize*sizeof(int));
    }
    memcpy(pointClusterOwnershipCopy.data(),pointClusterOwnership.constData(),scatterBufferSize*sizeof(quint8));
    newSpikeMutex.unlock();


    for (int i=0;i < mipMaps.length(); i++) {
        if (bufferRecentlyCleared && currentBufferIndex > -1) {
            mipMaps[i]->setPoints(0,currentBufferIndex,scatterDataCopy.constData(), pointClusterOwnershipCopy);
        } else if (!bufferRecentlyCleared) {
            mipMaps[i]->setPoints(0,scatterBufferSize,scatterDataCopy.constData(), pointClusterOwnershipCopy);
        }
        //mipMaps_lowRange[i]->setPoints(0,bufferSize,inputData);
    }
    waveImage->setClusters(pointClusterOwnershipCopy.data());

}

void NtrodeDisplayData::setWaveImageData(){
//    QVector<QVector<QVector<vertex2d> > > waveDataCopy;
//    int c = currentBufferIndex;
//    if(c<0)
//        return;

//    waveDataCopy.resize(numberOfChannels);
//    for(int j = 0; j < numberOfChannels; j++){
//        waveDataCopy[j].resize(c);
//        for(int i = 0; i < waveDataCopy[j].size(); i++){
//            waveDataCopy[j][i].resize(POINTSINWAVEFORM);
//        }
//    }
//    newSpikeMutex.lock();
//    for(int j = 0; j < numberOfChannels; j++){
//        for(int i = 0; i < waveDataCopy[j].size(); i++){
//            memcpy(waveDataCopy[j][i].data(), waveForms[i][j].constData(), POINTSINWAVEFORM*sizeof(vertex2d));
//        }
//    }
//    newSpikeMutex.unlock();

//    for(int j = 0; j < numberOfChannels; j++){
//        waveImages[j]->addAllWaveForms(waveDataCopy[j]);
//    }
//    waveImage->repaintAllWaveforms();
}

void NtrodeDisplayData::setZeroToMax(int newMaxAmplitude) {
    for (int i=0; i<maxAmplitude.length();i++) {
        maxAmplitude[i] = newMaxAmplitude;
        minAmplitude[i] = 0;
    }


}

void NtrodeDisplayData::setColorTable(const QList<QColor> &colors) {
    for (int i=0;i<mipMaps.length();i++) {
        mipMaps[i]->setColorTable(colors);
    }
}

QList<ScatterPlotMipmap*> NtrodeDisplayData::getMipMapPtrs() {
    QList<ScatterPlotMipmap*> list;
    for (int i = 0; i<mipMaps.length();i++) {
        list.append(mipMaps[i]);
    }
    return list;
}

QList<ClusterPolygonSet> NtrodeDisplayData::getPolygonsForCluster(int clusterInd) {
    QList<ClusterPolygonSet> polyInfo;
    for (int i=0; i<polygonData.length(); i++) {
        if (polygonData[i].clusterInd == clusterInd) {
            //This polygon belongs to the desired cluster
            ClusterPolygonSet newPoly;
            newPoly.xAxis = polygonData[i].xAxis;
            newPoly.yAxis = polygonData[i].yAxis;
            newPoly.points = polygonData[i].points;
            newPoly.bitInd = polygonData[i].bitInd;
            newPoly.clusterInd = polygonData[i].clusterInd;
            polyInfo.append(newPoly);
        }
    }
    return polyInfo;
}

ClusterPolygonSet NtrodeDisplayData::getPolygonForBitInd(int polyBitIndex) {
    int index = -1;
    for (int i=0; i<polygonData.length(); i++) {
        if (polygonData[i].bitInd == polyBitIndex) {
            index = i;
            break;
        }
    }
    if (index > -1) {
        return polygonData[index];
    }   else {
        ClusterPolygonSet p;
        return p;
    }
}

void NtrodeDisplayData::addPolygonData(ClusterPolygonSet p) {
    bool replacedEntry = false;
    //Check if an entry for this bitind already exists, if so replace it
    for (int i=0; i<polygonData.length(); i++) {
        if (polygonData[i].bitInd == p.bitInd) {
            polygonData[i].xAxis = p.xAxis;
            polygonData[i].yAxis = p.yAxis;
            polygonData[i].points = p.points;
            polygonData[i].bitInd = p.bitInd;
            polygonData[i].clusterInd = p.clusterInd;
            replacedEntry = true;
            break;
        }
    }
    if (!replacedEntry) {
        polygonData.append(p);
    }
}

void NtrodeDisplayData::deleteAllClusterData() {
    //Deletes all clusters in the nTrode
    for (int bitIndIndex = 0; bitIndIndex < bitClusterOwnership.length(); bitIndIndex++) {
        if (bitClusterOwnership[bitIndIndex] != -1) {
            for (int i=0; i<polygonData.length(); i++) {
                if (polygonData[i].bitInd == bitIndIndex) {
                    polygonData.takeAt(i);
                    bitClusterOwnership[bitIndIndex] = -1; //keep a record of which cluster the polygon belongs to
                    break;
                }
            }
        }
    }

    for (int cl =0; cl < NUMCLUSTERS; cl++) {
        toggleClusterOn(cl,false);
        //clustersDefined[clusterInd] = false;
    }

    //This storage method assumes there will be no overlap in clusters, which may not be the case.
    for (int pointInd = 0; pointInd < scatterBufferSize; pointInd++) {
       pointClusterOwnership[pointInd] = 0;

    }



}

void NtrodeDisplayData::deleteClusterData(int clusterInd) {
    if(bitClusterOwnership.isEmpty())
        return;

    for (int bitIndIndex = 0; bitIndIndex < bitClusterOwnership.length(); bitIndIndex++) {
        if (bitClusterOwnership[bitIndIndex] == clusterInd) {
            for (int i=0; i<polygonData.length(); i++) {
                if (polygonData[i].bitInd == bitIndIndex) {
                    polygonData.takeAt(i);
                    bitClusterOwnership[bitIndIndex] = -1; //keep a record of which cluster the polygon belongs to
                    break;
                }
            }
        }
    }

    calculateCluster(clusterInd);
}


void NtrodeDisplayData::deletePolygonData(int clusterInd, int bitInd) {
    if(bitClusterOwnership.isEmpty())
        return;
    bitClusterOwnership[bitInd] = -1; //keep a record of which cluster the polygon belongs to
    for (int i=0; i<polygonData.length(); i++) {
        if (polygonData[i].bitInd == bitInd) {
            polygonData.takeAt(i);
            break;
        }
    }
    calculateCluster(clusterInd);

    //Replot the mipmaps
    //graphicsWin->setData(scatterData.constData(), pointClusterOwnership);


}

uint32_t NtrodeDisplayData::getStartOfLogTime() {
    //Returns the start of the time range for the saved spikes
    return beginningOfLog;
}

QVector<uint32_t> NtrodeDisplayData::getSpikeTimesForCluster(int clusterInd) {
    QVector<uint32_t> outTimes;
    if (clustersDefined[clusterInd]) {
        //Here we lock the threads that adds data to the buffer.  We want this to be as brief as possible
        newSpikeMutex.lock();
        for (int i = 0; i < pointClusterOwnership.length(); i++) {
            if (pointClusterOwnership[i] == clusterInd) {
                outTimes.append(spikeTimes[i]);
            }
        }
        newSpikeMutex.unlock();

    }
    return outTimes;

}

QVector<uint32_t> NtrodeDisplayData::getAllSpikeTimes() {
    QVector<uint32_t> outTimes;
    //Here we lock the threads that adds data to the buffer.  We want this to be as brief as possible
    newSpikeMutex.lock();
    outTimes = spikeTimes;
    newSpikeMutex.unlock();

    //If buffer isn't filled, removes the ones at the end still with 0 as value
    int i = outTimes.length()-1;
    //Keep decrementing i while the time == 0
    while(i>-1 && !outTimes[i--])
        outTimes.pop_back();
    //Subset outTimes
    return outTimes;

}

QVector<QVector<uint32_t> > NtrodeDisplayData::getAllClusterSpikeTimes(){
    QVector<QVector<uint32_t> > outTimes;
    for(int i = 0; i < NUMCLUSTERS; i++){
        outTimes.append(getSpikeTimesForCluster(i));
    }
    return outTimes;
}

void NtrodeDisplayData::calculatePointsInsidePolygon(int clusterInd, int bitInd) {
    //qDebug() << "Calculating points inside cluster" ;
    ClusterPolygonSet polySet = getPolygonForBitInd(bitInd);

    if (!clustersDefined[clusterInd]) {
        toggleClusterOn(clusterInd,true);
    }

    if ( (floor(bitInd/8)+1) > polyIncludeRecord.length() ) {
        //We need to increase the size of the include and exclude records
        polyIncludeRecord.append(new quint8[scatterBufferSize]);
        polyExcludeRecord.append(new quint8[scatterBufferSize]);
        for (int i=0;i<8;i++) {
            bitClusterOwnership.append(-1);
        }
    }

    int recordVectorIndex = floor(bitInd/8);  //The index into polyIncludeRecord
    int subIndex = bitInd % 8;  //The bit to use of the 8-bit uint
    bitClusterOwnership[(recordVectorIndex*8)+subIndex] = clusterInd; //keep a record of which cluster the polygon belongs to

    QPolygon amplitudeSpacePolygon(polySet.points);

    QPoint p;

    for (int pointInd = 0; pointInd < scatterBufferSize; pointInd++) {

        p.setX(scatterData[polySet.xAxis][pointInd]);
        p.setY(scatterData[polySet.yAxis][pointInd]);

        //We keep a record of which points are included and exluded by each polygon
        //If all points are in scope for every polygon, these are exact compliments,
        //but not if some polygons anly have scope over a subset of points. Might be overkill
        //but will allow easy growth to full clustering capabilities if needed.

        //A point is considered part of a cluster if it is included at least once and never
        //excluded by any polygons

        if (amplitudeSpacePolygon.containsPoint(p,Qt::OddEvenFill)) {
            //The point is inside the polygon
            polyIncludeRecord[recordVectorIndex][pointInd] |= (1 << subIndex); //turns bit to 1
            polyExcludeRecord[recordVectorIndex][pointInd] &= ~(1 << subIndex); //turns bit to 0

        } else {
            //The point is outside the polygon
            polyExcludeRecord[recordVectorIndex][pointInd] |= (1 << subIndex);
            polyIncludeRecord[recordVectorIndex][pointInd] &= ~(1 << subIndex);
        }
    }

    //Calculate which points now belong to the cluster
    //calculateCluster(clusterInd);


}

void NtrodeDisplayData::calculateCluster(int clusterInd) {
    //Calculates which points belong to the given cluster
    bool *beenIncluded = new bool[scatterBufferSize];
    bool *beenExcluded = new bool[scatterBufferSize];
    for (int pointInd = 0; pointInd < scatterBufferSize; pointInd++) {
        beenExcluded[pointInd] = false;
        beenIncluded[pointInd] = false;
    }

    bool foundPolygon = false;
    for (int bitInd=0; bitInd< bitClusterOwnership.length(); bitInd++) {
        if (bitClusterOwnership[bitInd] == clusterInd) {
            foundPolygon = true;
            //This bit has a polygon for the designated cluster
            int recordVectorIndex = floor(bitInd/8);  //The index into polyIncludeRecord
            int subIndex = bitInd % 8;  //The bit to use of the 8-bit uint
            for (int pointInd = 0; pointInd < scatterBufferSize; pointInd++) {
                if (polyIncludeRecord[recordVectorIndex][pointInd] & (1 << subIndex)) {
                    beenIncluded[pointInd] = true;
                }

                if (polyExcludeRecord[recordVectorIndex][pointInd] & (1 << subIndex)) {
                    beenExcluded[pointInd] = true;
                }

            }

        }
    }

    if (!foundPolygon) {
        toggleClusterOn(clusterInd,false);
        //clustersDefined[clusterInd] = false;
    }

    //This storage method assumes there will be no overlap in clusters, which may not be the case.
    for (int pointInd = 0; pointInd < scatterBufferSize; pointInd++) {
        if (beenIncluded[pointInd] && !beenExcluded[pointInd]) {
            pointClusterOwnership[pointInd] = (quint8)clusterInd;
        } else if (pointClusterOwnership[pointInd] == clusterInd) {
            //This point no longer belongs in the cluster
            pointClusterOwnership[pointInd] = 0;
        }
    }

    delete[] beenIncluded;
    delete[] beenExcluded;

}

int NtrodeDisplayData::sortDataPoint(const QVector<int> &coord, int pointInd) {
    //Determine which cluster the point belongs to.

    bool beenExluded = false;
    bool beenIncluded = false;
    int bitInd;
    int recordVectorIndex;
    int subIndex;
    int outCluster = 0;
    bool foundCluster = false;

    for (int cl=0;cl<NUMCLUSTERS;cl++) {
        if (clustersDefined[cl]) {
            beenExluded = false;
            beenIncluded = false;
            QList<ClusterPolygonSet> polyInfo = getPolygonsForCluster(cl);
            for (int polyInd=0; polyInd<polyInfo.length();polyInd++) {
               bitInd = polyInfo[polyInd].bitInd;
               recordVectorIndex = floor(bitInd/8);  //The index into polyIncludeRecord
               subIndex = bitInd % 8;  //The bit to use of the 8-bit uint
               QPolygon amplitudeSpacePolygon(polyInfo[polyInd].points);
               QPoint p(coord[polyInfo[polyInd].xAxis],coord[polyInfo[polyInd].yAxis]);
               if (amplitudeSpacePolygon.containsPoint(p,Qt::OddEvenFill)) {
                   //The point is inside the polygon
                   beenIncluded = true;
                   polyIncludeRecord[recordVectorIndex][pointInd] |= (1 << subIndex); //turns bit to 1
                   polyExcludeRecord[recordVectorIndex][pointInd] &= ~(1 << subIndex); //turns bit to 0

               } else {
                   //The point is outside the polygon
                   beenExluded = true;
                   polyExcludeRecord[recordVectorIndex][pointInd] |= (1 << subIndex);
                   polyIncludeRecord[recordVectorIndex][pointInd] &= ~(1 << subIndex);
               }

            }

            if (!(foundCluster) && (beenIncluded) && !(beenExluded)) {
                outCluster = cl;
            }
        }

    }
    //This storage method assumes there will be no overlap in clusters, which may not be the case.
    pointClusterOwnership[pointInd] = (quint8)outCluster;

    return outCluster; //default 0

}

void NtrodeDisplayData::toggleClusterOn(int clusterInd, bool on) {
    clustersDefined[clusterInd] = on;

}


void NtrodeDisplayData::lockSpikeSortMutex() {
    spikeSortMutex.lock();
}

void NtrodeDisplayData::unlockSpikeSortMutex() {
    spikeSortMutex.unlock();
}



void NtrodeDisplayData::clearButtonPressed() {

    bufferRecentlyCleared = true;
    currentBufferIndex = 0;
    for (int i = 0; i < scatterData.size(); i++) {
        scatterData[i].fill(0);
    }
    wavePeaks.fill(0);
//    meanPeak = 0;
    pointClusterOwnership.fill(0);
    spikeTimes.fill(0);
    beginningOfLog = currentTimeStamp;
    waveImage->clearData();

    //graphicsWin->clearData();

}

void NtrodeDisplayData::updateScatterChannels(int channelCombo) {

    currentXdisplay = channelViewXList[channelCombo];
    currentYdisplay = channelViewYList[channelCombo];
    //graphicsWin->setChannels(currentXdisplay, currentYdisplay, scatterData.constData(),pointClusterOwnership);

}

void NtrodeDisplayData::receiveNewSpike(const SpikeWaveform &s)
{
    int numChannels = s.numChannels;
    QVector<int2d> templateSpike;
    templateSpike.resize(s.numSamples);
    for(int16_t samp = 0; samp < s.numSamples; samp++){
        templateSpike[samp].x = samp;
    }
    QVector<QVector<int2d> > wvform;
    wvform.resize(numChannels);
    for(int channel = 0; channel < numChannels; channel++){
        wvform[channel] = templateSpike;
        for (int samp = 0; samp < s.numSamples; samp++){
            wvform[channel][samp].y = s.waveData[channel][samp];
        }
        //memcpy(wvform[i].data(), waveForm[i].constData(), sizeof(int2d)*POINTSINWAVEFORM);
    }

    QVector<int16_t> peaks = s.getPeaks();
    //the original spike snippet data will not be used after this point, so we delete it
    //to prevent a big memory leak.
    s.clearMemory();


    //const int* peaks, uint32_t time

    newSpikeMutex.lock();


    //Iterate to find a spike to replace




    //We can either try to replace spikes below the mean peak...
    /*

    //Iterate to find a spike to replace
    //Oldest spike with peak lower than mean peak
    int oldmax = 0;

    for(int skip = 0; skip < scatterBufferSize; skip++){
        currentBufferIndex = (currentBufferIndex+1) % scatterBufferSize;
        qreal max = -1;
        //If numchannel > 1, checks all channels, else, only checks i=0
        for (int i = 0; i < numChannels; i++)
            max = qMax(max, (qreal)scatterData[i][currentBufferIndex]);

        //Compare spike peak to meanpeak
        if(max <= meanPeak){
            oldmax = max;
            break;
        }
    }
*/




    //use new currentbufferindex
    QVector<int> coord;
    if (numChannels > 1) {
        qreal max = -1;
        for (int i = 0; i < numChannels; i++) {
            scatterData[i][currentBufferIndex] = peaks[i];
            coord.append(peaks[i]);
            max = qMax(max, (qreal)peaks[i]);
        }
//        meanPeak = qreal(meanPeak*scatterBufferSize - oldmax + max)/(qreal)scatterBufferSize;
//        wavePeaks[currentBufferIndex] = max;
    } else {
        //there is only one channel, so in order to plot a scatter, we need to display two different parameters
        scatterData[0][currentBufferIndex] = peaks[0]; //peak amplitude
        scatterData[1][currentBufferIndex] = -peaks[1]; //min amplitude
        coord.append(peaks[0]);
        coord.append(-peaks[1]);
//        meanPeak = qreal(meanPeak*scatterBufferSize - oldmax + peaks[0])/(qreal)scatterBufferSize;
    }

    //If the buffer has looped, then the beginning of the log
    //range will be the spike time we are about to replace in memory
    if (spikeTimes[currentBufferIndex] != 0) {
        beginningOfLog = spikeTimes[currentBufferIndex];
    }
    spikeTimes[currentBufferIndex] = s.peakTime;


    //Sort the spike
    int spikeCluster = sortDataPoint(coord,currentBufferIndex);

    //MARK: nrRefactor this is where we put a signal telling the spike socket to send out data
    //we need ntrode ID, cluster number, waveform(40pts * 4 bytes * numChannels/nTrode), timestamp
    emit broadcastSpike(spikeConf->ntrodes.at(nTrodeNumber)->nTrodeId, spikeCluster, s.peakTime, numChannels, wvform);

    //Update waveform plots
    waveImage->receiveNewWaveform(wvform, spikeCluster, currentBufferIndex);



    //We now know which cluster the spike belongs to, so we emit the message
    if (spikeCluster > 0) {
        QString spikeEvent = QString("Spike occured in NTrode %1 Cluster %2").arg(spikeConf->ntrodes.at(nTrodeNumber)->nTrodeId).arg(spikeCluster);
        emit broadcastEvent(TrodesEventMessage(spikeEvent));
        emit spike(nTrodeNumber,spikeCluster,s.peakTime);

    }

    currentBufferIndex = (currentBufferIndex+1) % scatterBufferSize;
    if (bufferRecentlyCleared && currentBufferIndex == 0) {
        bufferRecentlyCleared = false;
    }


    newSpikeMutex.unlock();

    //graphicsWin->setDataPoint(scatterData.constData(),currentBufferIndex, spikeCluster);

    //spikeSortMutex.unlock();

}

void NtrodeDisplayData::receiveNewEvent(const QVector<int2d>* waveForm, const int* peaks, uint32_t time) {
    //spikeSortMutex.lock();

    //waveform contains the waveform data for all channels
    //peaks contains the peak amplitude for all channels, followed by the minimum amplitude for all channels

    //int numChannels = triggerScope->scopeWindows.length();

    int numChannels = spikeConf->ntrodes[nTrodeNumber]->hw_chan.length();
    QVector<QVector<int2d> > wvform;
    wvform.resize(numChannels);
    for(int i = 0; i < numChannels; i++){
        wvform[i].resize(waveForm[i].size());
        memcpy(wvform[i].data(), waveForm[i].constData(), sizeof(int2d)*POINTSINWAVEFORM);
    }
    newSpikeMutex.lock();


    //Iterate to find a spike to replace




    //We can either try to replace spikes below the mean peak...
    /*

    //Iterate to find a spike to replace
    //Oldest spike with peak lower than mean peak
    int oldmax = 0;

    for(int skip = 0; skip < scatterBufferSize; skip++){
        currentBufferIndex = (currentBufferIndex+1) % scatterBufferSize;
        qreal max = -1;
        //If numchannel > 1, checks all channels, else, only checks i=0
        for (int i = 0; i < numChannels; i++)
            max = qMax(max, (qreal)scatterData[i][currentBufferIndex]);

        //Compare spike peak to meanpeak
        if(max <= meanPeak){
            oldmax = max;
            break;
        }
    }
*/




    //use new currentbufferindex
    QVector<int> coord;
    if (numChannels > 1) {
        qreal max = -1;
        for (int i = 0; i < numChannels; i++) {
            scatterData[i][currentBufferIndex] = peaks[i];
            coord.append(peaks[i]);
            max = qMax(max, (qreal)peaks[i]);
        }
//        meanPeak = qreal(meanPeak*scatterBufferSize - oldmax + max)/(qreal)scatterBufferSize;
//        wavePeaks[currentBufferIndex] = max;
    } else {
        //there is only one channel, so in order to plot a scatter, we need to display two different parameters
        scatterData[0][currentBufferIndex] = peaks[0]; //peak amplitude
        scatterData[1][currentBufferIndex] = -peaks[1]; //min amplitude
        coord.append(peaks[0]);
        coord.append(-peaks[1]);
//        meanPeak = qreal(meanPeak*scatterBufferSize - oldmax + peaks[0])/(qreal)scatterBufferSize;
    }

    //If the buffer has looped, then the beginning of the log
    //range will be the spike time we are about to replace in memory
    if (spikeTimes[currentBufferIndex] != 0) {
        beginningOfLog = spikeTimes[currentBufferIndex];
    }
    spikeTimes[currentBufferIndex] = time;


    //Sort the spike
    int spikeCluster = sortDataPoint(coord,currentBufferIndex);

    //MARK: nrRefactor this is where we put a signal telling the spike socket to send out data
    //we need ntrode ID, cluster number, waveform(40pts * 4 bytes * numChannels/nTrode), timestamp
    emit broadcastSpike(spikeConf->ntrodes.at(nTrodeNumber)->nTrodeId, spikeCluster, time, numChannels, wvform);

    //Update the waveform plot
    waveImage->receiveNewWaveform(wvform, spikeCluster, currentBufferIndex);



    //We now know which cluster the spike belongs to, so we emit the message
    if (spikeCluster > 0) {
        QString spikeEvent = QString("Spike occured in NTrode %1 Cluster %2").arg(spikeConf->ntrodes.at(nTrodeNumber)->nTrodeId).arg(spikeCluster);
        emit broadcastEvent(TrodesEventMessage(spikeEvent));
        emit spike(nTrodeNumber,spikeCluster,time);

    }
    currentBufferIndex = (currentBufferIndex+1) % scatterBufferSize;
    if (bufferRecentlyCleared && currentBufferIndex == 0) {
        bufferRecentlyCleared = false;
    }

    newSpikeMutex.unlock();

    //graphicsWin->setDataPoint(scatterData.constData(),currentBufferIndex, spikeCluster);

    //spikeSortMutex.unlock();

}









//NtrodeDisplayWidget contains all plot and control objects for one nTrode.
MultiNtrodeDisplayWidget::MultiNtrodeDisplayWidget(QWidget *parent) :
  QWidget(parent)
{

    multiViewMode = false;
    showOnlySelectedClusterMode = false;

    currentNTrode = -1;
    currentHWAudioChannel = 0;
    currentXdisplay = 0;
    currentYdisplay = 1;
    writeBlock = 0;
    changingDisplayedNtrode = false;
    streamManager = NULL;

    //Graphics driver check
    QStringList opengl;
    opengl << QString((char*)QOpenGLContext::currentContext()->functions()->glGetString(GL_VERSION));
    opengl << QString((char*)QOpenGLContext::currentContext()->functions()->glGetString(GL_VENDOR));
    opengl << QString((char*)QOpenGLContext::currentContext()->functions()->glGetString(GL_RENDERER));
    validDriver = !opengl.contains("nouveau", Qt::CaseInsensitive);
//    validDriver = false;

    //Buffer size for number of data points to store (waveforms and scatterplot)
    //int buffer = 2048;


    int totalNumChannels = 0;
        for(int i = 0; i < spikeConf->ntrodes.length(); i++)
            totalNumChannels += spikeConf->ntrodes[i]->hw_chan.length();
    int buffer;

    if (totalNumChannels <= 128) {
        buffer = 32768*8;
    } else if (totalNumChannels <= 256) {
        buffer = 32768*4;
    } else if (totalNumChannels <= 512) {
        buffer = 32768*2;
    } else if (totalNumChannels <= 1024) {
        buffer = 32768*1;
    } else if (totalNumChannels <= 2048){
        buffer = 32768;
    } else {
        buffer = 4096;
    }
    //int buffer = 10; //use a small buffer if in debug mode

    /*int channels = 0;
    for(int i = 0; i < spikeConf->ntrodes.length(); i++)
        channels += spikeConf->ntrodes[i]->hw_chan.length();

    if(channels > 512) //1024 is max channel count so far
        buffer = buffer*1.5;
    else if(channels > 256)
        buffer = buffer*4;
    else if(channels > 128)
        buffer = buffer*6;
    else
        buffer = buffer*8;*/

    //default cluster colors
    clusterColors.push_back(QColor(255,255,255));
    clusterColors.push_back(QColor(255,0,0));
    clusterColors.push_back(QColor(0,255,0));
    clusterColors.push_back(QColor(0,0,255));
    clusterColors.push_back(QColor(200,200,0));
    clusterColors.push_back(QColor(0,255,255));
    clusterColors.push_back(QColor(255,0,255));
    clusterColors.push_back(QColor(135,25,255));
    clusterColors.push_back(QColor(125,90,50));

    clusterColors.push_back(QColor(125,90,50));
    clusterColors.push_back(QColor(125,90,50));
    clusterColors.push_back(QColor(125,90,50));
    clusterColors.push_back(QColor(125,90,50));
    clusterColors.push_back(QColor(125,90,50));
    clusterColors.push_back(QColor(125,90,50));
    clusterColors.push_back(QColor(0,0,0));

    qRegisterMetaType<QVector<QVector<int2d> > >("QVector<QVector<int2d> >");
    for (int i=0; i < spikeConf->ntrodes.length(); i++) {
        nTrodeData.append(new NtrodeDisplayData(NULL,i, buffer));
        nTrodeData.last()->setColorTable(clusterColors);
        if (networkConf->networkType == NetworkConfiguration::qsocket_based) {
            connect(nTrodeData.at(i),&NtrodeDisplayData::broadcastEvent,this,&MultiNtrodeDisplayWidget::broadcastEvent);
        } else if (networkConf->networkType == NetworkConfiguration::zmq_based) {
            connect(nTrodeData.at(i), &NtrodeDisplayData::broadcastSpike,this, &MultiNtrodeDisplayWidget::publishSpike,Qt::DirectConnection);
        }
    }

    panelSplitter = new QSplitter;
    panelSplitter->setOrientation(Qt::Horizontal);
    connect(panelSplitter,SIGNAL(splitterMoved(int,int)),this,SLOT(newSplitterPos(int,int)));

    mainLayout = new QGridLayout(this);
//    scopeLayout = new QGridLayout();
    scatterLayout = new QGridLayout();
    scatterLayout->setVerticalSpacing(0);
    scatterControlLayout = new QGridLayout();
    //scatterControlLayout->setSpacing(2);
    scatterControlLayout->setHorizontalSpacing(10);
    scatterControlLayout->setVerticalSpacing(2);
    scatterControlLayout->setContentsMargins(1,5,1,1);
    QGridLayout *viewSelectLayout = new QGridLayout();
    viewSelectLayout->setSpacing(1);



    nTrodeLabel = new QLabel(this);
    nTrodeLabel->setAlignment(Qt::AlignCenter);
    QFont labelFont;
    labelFont.setPixelSize(20);
    labelFont.setFamily("Arial");
    nTrodeLabel->setFont(labelFont);
    clearButton = new TrodesButton(this);
    clearAllButton = new TrodesButton(this);
    //clearButton->setFlat(true);

    clearButton->setText("Clear");
    //clearButton->setFixedWidth(60);
    clearButton->setToolTip("Clear spike history for this nTrode");
    connect(clearButton, SIGNAL(released()), this, SLOT(clearButtonPressed()));

    clearAllButton->setText("Clear all");
    //clearAllButton->setFixedWidth(70);
    clearAllButton->setToolTip("Clear spike history for all nTrodes");
    connect(clearAllButton, SIGNAL(released()), this, SLOT(clearAllButtonPressed()));


    //Single/multi scatter plot view selection
    selectSingleView = new TrodesButton(this);
    selectSingleView->setText("Single");
    selectSingleView->setCheckable(false);
    selectSingleView->setDown(true);
    selectSingleView->setToolTip("Show one plot");
    connect(selectSingleView,SIGNAL(pressed()),this,SLOT(singleViewButtonPressed()));
    connect(selectSingleView,SIGNAL(released()),this,SLOT(singleViewButtonReleased()));
    selectMultiView = new TrodesButton(this);
    selectMultiView->setText("Multi");
    selectMultiView->setCheckable(false);
    selectMultiView->setToolTip("Show all plot combinations");
    connect(selectMultiView,SIGNAL(pressed()),this,SLOT(multiViewButtonPressed()));
    connect(selectMultiView,SIGNAL(released()),this,SLOT(multiViewButtonReleased()));



    scatterControlLayout->addWidget(nTrodeLabel,0,0);

    QGridLayout* clearButtonLayout = new QGridLayout();
    clearButtonLayout->setSpacing(1);
    clearButtonLayout->addWidget(clearButton,0,0);
    clearButtonLayout->addWidget(clearAllButton,0,1);
    scatterControlLayout->addLayout(clearButtonLayout,0,1);


    QGridLayout* controlButtonLayout = new QGridLayout();
    controlButtonLayout->setSpacing(2);

    //-------------nTrode multiple histograms-------
    QGridLayout* PETHControlLayout = new QGridLayout();
    pethButton = new TrodesButton();
    pethButton->setText("PETH overview");
    pethButton->setToolTip("Display perievent time histogram");
    PETHControlLayout->addWidget(pethButton, 0, 0, Qt::AlignLeft);
    PETHControlLayout->setColumnStretch(1,1);
    scatterControlLayout->addLayout(PETHControlLayout, 2, 2, Qt::AlignLeft);

    connect(pethButton, SIGNAL(released()), this, SLOT(openMultiHistogramWindow()));

    viewSelectLayout->addWidget(selectSingleView,0,0);
    viewSelectLayout->addWidget(selectMultiView,0,1);
    scatterControlLayout->addLayout(viewSelectLayout,1,0);

    //Orientaion button and layout
    QGridLayout* layoutControlLayout = new QGridLayout();
    layoutControlLayout->setSpacing(2);
    //QLabel *orientationLabel = new QLabel("Window orientation:");
    orientationButton = new TrodesButton();
    QChar arrowchar(0xBB,0x21);
    orientationButton->setText(arrowchar);
    orientationButton->setCheckable(false);
    connect(orientationButton,&TrodesButton::clicked,this,&MultiNtrodeDisplayWidget::orientationButtonClicked);
    //layoutControlLayout->addWidget(orientationLabel,0,0);
    layoutControlLayout->addWidget(orientationButton,0,0);
    layoutControlLayout->setColumnStretch(1,1);
    scatterControlLayout->addLayout(layoutControlLayout, 0, 2, Qt::AlignLeft);



    channelViewCombo = new QComboBox(this);
    TrodesFont dispFont;
    channelViewCombo->setFont(dispFont);
    //scatterDisplay = new scatterWindow(this, scatterBufferSize, scatterData.size());

    connect(channelViewCombo,SIGNAL(currentIndexChanged(int)),this,SLOT(updateScatterChannels(int)));

    //--------------------------Cluster buttons
    clusterSelectButtons = new QButtonGroup(this);
    for (int i=0; i < MAXCLUSTERS; i++) {
        ClusterPushButton * clusterButton = new ClusterPushButton();
        clusterButton->setFixedSize(25,25);

        clusterButton->setProperty("userData",i+1);
        clusterButton->setCheckable(true);
        clusterButton->setToolTip(QString("Select cluster %1").arg(i+1));

        //Set the text color to the cluster's color
        QPalette pal = clusterButton->palette();
        pal.setBrush(QPalette::ButtonText, clusterColors[i+1]);
        //pal.setBrush(QPalette::Background  , clusterColors[i+1]);
        clusterButton->setAutoFillBackground(true);
        clusterButton->setPalette(pal);
        clusterButton->setText(QString("%1").arg(i+1));

        //clusterButton->setStyleSheet("background-color: #ff0000");
        connect(clusterButton,SIGNAL(pressed()),this,SLOT(clusterButtonPressed()));
        connect(clusterButton,SIGNAL(rightClicked(const QPoint&)),this,SLOT(clusterRightClick(const QPoint&)));

        clusterSelectButtons->addButton(clusterButton,i);
    }

    clusterSelectButtons->setExclusive(true);
    clusterSelectButtons->button(0)->setChecked(true);
    selectedClusterInd = 1;

    //----------Show/hide unclustered points button
    showUnclustered = new TrodesButton(this);
    showUnclustered->setText("Show unclustered");
    showUnclustered->setCheckable(true);
    showUnclustered->setChecked(true);
    showUnclustered->setToolTip("Toggle to show/hide spikes that are not sorted into clusters");
    controlButtonLayout->addWidget(showUnclustered, 0, 0, Qt::AlignLeft);
    connect(showUnclustered, &TrodesButton::toggled, this, &MultiNtrodeDisplayWidget::setUnclusteredShowing);

    showOnlySelectedCluster = new TrodesButton(this);
    showOnlySelectedCluster->setText("Hide unselected");
    showOnlySelectedCluster->setCheckable(true);
    showOnlySelectedCluster->setChecked(false);
    showOnlySelectedCluster->setToolTip("Toggle to show/hide spikes that are part of clusters that are not currently part of the selected cluster");
    controlButtonLayout->addWidget(showOnlySelectedCluster, 1, 0, Qt::AlignLeft);
    scatterControlLayout->addLayout(controlButtonLayout, 1, 2, Qt::AlignLeft);
    connect(showOnlySelectedCluster, &TrodesButton::toggled, this, &MultiNtrodeDisplayWidget::setOnlySelectedClusterShowing);


    QPixmap arrowPixmap(":/buttons/arrow.png");
    QPixmap handPixmap(":/buttons/hand.png");
    QPixmap polyPixmap(":/buttons/polygon.png");

    QIcon includeButtonIcon(polyPixmap);
    QIcon editButtonIcon(arrowPixmap);
    QIcon panButtonIcon(handPixmap);

    toolSelectButtons = new QButtonGroup(this);
    TrodesButton *includeButton = new TrodesButton();
    includeButton->setFixedSize(25,25);
    includeButton->setCheckable(true);
    includeButton->setChecked(true);
    includeButton->setIcon(includeButtonIcon);
    includeButton->setToolTip("Tool: draw polyons");
    toolSelectButtons->addButton(includeButton,INCLUDETOOL_ID);

    TrodesButton *polyEditButton = new TrodesButton();
    polyEditButton->setFixedSize(25,25);
    polyEditButton->setCheckable(true);
    polyEditButton->setIcon(editButtonIcon);
    polyEditButton->setToolTip("Tool: edit existing polygons");
    toolSelectButtons->addButton(polyEditButton,EDITTOOL_ID);

    /*TrodesButton *panButton = new TrodesButton();
    panButton->setFixedSize(25,25);
    panButton->setCheckable(true);
    panButton->setIcon(panButtonIcon);
    toolSelectButtons->addButton(panButton,PANTOOL_ID);*/

    toolSelectButtons->setExclusive(true);

    currentTool = INCLUDETOOL_ID;
    //connect(toolSelectButtons,SIGNAL(buttonPressed(int)),SLOT(toolButtonPressed(int)));
    connect(toolSelectButtons, &QButtonGroup::idPressed, this, &MultiNtrodeDisplayWidget::toolButtonPressed);

    //The layout for the cluster selection buttons
    QGridLayout *clusterButtonLayout = new QGridLayout();
    clusterButtonLayout->setSpacing(1);
    for (int i=0; i < clusterSelectButtons->buttons().length(); i++) {
        clusterButtonLayout->addWidget(clusterSelectButtons->button(i),floor(i/4),i%4);
    }
    scatterControlLayout->addLayout(clusterButtonLayout,1,1);

    QGridLayout *toolButtonLayout = new QGridLayout();
    toolButtonLayout->setSpacing(1);
    toolButtonLayout->addWidget(includeButton,0,0);
    toolButtonLayout->addWidget(polyEditButton,0,1);
    //toolButtonLayout->addWidget(panButton,0,2);

    scatterControlLayout->addLayout(toolButtonLayout,2,1);
    scatterControlLayout->addWidget(channelViewCombo,2,0);
    //scatterControlLayout->addWidget(refEdit,2,1);
    scatterControlLayout->setColumnStretch(2,2);
    QFrame *controlFrame = new QFrame();
    controlFrame->setStyleSheet("QFrame {border: 2px solid lightgray;"
                          "border-radius: 4px;"
                          "padding: 2px;}");
    controlFrame->setLayout(scatterControlLayout);
    nTrodeLabel->setStyleSheet("QFrame {border: none;}");

    //scatterLayout->addLayout(scatterControlLayout,0,0); //done with the scatter control layout
    scatterLayout->addWidget(controlFrame,0,0); //done with the scatter control layout
    scatterLayout->setContentsMargins(1,1,1,1);


    //Add the scatter plot to the layout

    graphicsWin = new ScatterPlotWindow(nullptr,200000);
    //QFrame *scopeFrame = new QFrame();
    graphicsWin->setStyleSheet("QFrame {border: 2px solid lightgray;"
                          "border-radius: 4px;"
                          "padding: 2px;}");
    graphicsWin->setClusterColors(clusterColors);
    connect(graphicsWin,SIGNAL(newClusterCalculationNeeded(int,ClusterPolygonSet, int)),this,SLOT(calculatePointsInsidePolygon(int,ClusterPolygonSet, int)));
    connect(graphicsWin,SIGNAL(newClusterCalculationNeededForAllPolygons(QVector<QList<ClusterPolygonSet> >)),this,SLOT(calculatePointsInsideAllPolygons(QVector<QList<ClusterPolygonSet> >)));
    connect(graphicsWin,SIGNAL(polygonDeleted(int,int)),this,SLOT(deletePolygonData(int,int)));
    connect(graphicsWin,SIGNAL(clusterDeleted(int,int)),this,SLOT(deleteClusterData(int,int)));
    connect(graphicsWin,SIGNAL(allClustersDeleted()),this,SLOT(deleteAllClusters()));
    connect(graphicsWin,SIGNAL(zoom(int)),this,SLOT(zoom(int)));
    connect(graphicsWin,SIGNAL(lockSpikeSorting()),this,SLOT(lockSpikeSortMutex()));
    connect(graphicsWin,SIGNAL(unlockSpikeSorting()),this,SLOT(unlockSpikeSortMutex()));
    connect(graphicsWin,SIGNAL(channelPairSelected(int)),this,SLOT(scatterPairSelected(int)));
    connect(graphicsWin,SIGNAL(PSTHRequested(int)),this,SLOT(openPSTHWindow(int)));
    connect(graphicsWin,SIGNAL(setClusterShowing(bool,quint8)),this,SLOT(setClusterShowing(bool,quint8)));
    //graphicsWin->setViewport(graphicsWin);

    //MARK: cur
//    connect(graphicsWin, SIGNAL(addEditModuleToConfigEvent(TrodesEventMessage)), this, SIGNAL(addEditModuleToConfigEvent(TrodesEventMessage)));
//    connect(graphicsWin, SIGNAL(addEditModuleToConfigNewEventReq(QString)), this, SIGNAL(addEditModuleToConfigNewEventReq(QString)));
    connect(graphicsWin, SIGNAL(broadcastEventRemoveReq(QString)), this, SIGNAL(broadcastEventRemoveReq(QString)));
    connect(graphicsWin, SIGNAL(broadcastNewEventReq(QString)), this, SIGNAL(broadcastNewEventReq(QString)));

    scatterLayout->addWidget(graphicsWin,1,0);

    //Everything created so far gets added to the left side of the splitter
    QWidget *leftSide = new QWidget(this);
    leftSide->setLayout(scatterLayout);
    panelSplitter->addWidget(leftSide);

    //On the right side of the splitter we place the spike waveform display and controls
    triggerScope = new TriggerScopeDisplayWidget(this, nTrodeData[0]->scatterBufferSize, validDriver);

    //connect(triggerScope, SIGNAL(newThreshold(int,int,int)), spikeConf, SLOT(setThresh(int,int,int)) );
    //connect(triggerScope, SIGNAL(newThreshold(int,int)), triggerThreads[nTrodeID], SLOT(setThresh(int,int)) );
    //connect(triggerScope, SIGNAL(newMaxDisplaySignal(int,int)),graphicsWin,SLOT(setMaxDisplay(int,int)));
    connect(triggerScope,SIGNAL(zoom(int)),this,SLOT(zoom(int)));
    connect(triggerScope,SIGNAL(setAudioChannel(int)),this,SIGNAL(channelClicked(int)));

    connect(triggerScope, SIGNAL(newThreshSignal(int)), this, SIGNAL(sig_newThresh(int)));

    connect(this,SIGNAL(sig_newMaxDispRecv(int)), triggerScope, SLOT(setMaxDisplay(int)));
    connect(this,SIGNAL(sig_newThreshRecv(int)),triggerScope,SLOT(setThreshold(int)));

    connect(graphicsWin,SIGNAL(mouseOverChannels(int,int)),triggerScope,SLOT(setCurrentDisplayPair(int,int)));
    //triggerScope->setTraceColor(spikeConf->ntrodes[nTrodeID]->color);
    panelSplitter->addWidget(triggerScope);
    panelSplitter->setContentsMargins(0,0,0,0);
    triggerScope->scopeWindows->clusterColors = clusterColors;

    mainLayout->addWidget(panelSplitter,0,0);
    mainLayout->setContentsMargins(1,0,1,0);

    setShownNtrode(0);




    scatterPlotTimer = new QTimer(this);
    scopePlotTimer = new QTimer(this);
    waveformPlotTimer = new QTimer(this);
    scopePlotMinimumIntervalTimer.restart();

    connect(scatterPlotTimer, SIGNAL(timeout()), SLOT(scatterPlotTimerExpired()));
    connect(scopePlotTimer, SIGNAL(timeout()), SLOT(scopePlotTimerExpired()));
    connect(waveformPlotTimer, SIGNAL(timeout()), SLOT(waveformPlotTimerExpired()));
    //connect(waveformPlotTimer, SIGNAL(timeout()), SLOT(updateScatter()));
    connect(this, &MultiNtrodeDisplayWidget::spikeUpdateNeeded, this, &MultiNtrodeDisplayWidget::scopePlotTimerExpired,Qt::QueuedConnection);

    setRealTimeMode(false);


    //Remembered settings...
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    //Place the window where it was last
    settings.beginGroup(QLatin1String("spikeWindow"));
    bool ok;
    int tempOrientation = settings.value(QLatin1String("orientation")).toInt(&ok);
    if (ok) {
        setOrientation(tempOrientation);
    }


    QRect tempPosition = settings.value(QLatin1String("position")).toRect();
    if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
    }
    settings.endGroup();

    show();
    raise();

}

MultiNtrodeDisplayWidget::~MultiNtrodeDisplayWidget() {
    emit windowClosed();

    // if there is a spike publisher, we allocated a spikeDataBloc
    if (spikeTrodesPub){
        delete [] spikeDataBlock;
    }

    scopePlotTimer->stop();
    scatterPlotTimer->stop();
    while (nTrodeData.length() > 0) {
        delete nTrodeData.takeLast();
    }
    QList<MultiPlotDialog*> peth = pethWindows.values();
    for(int i = 0; i < peth.length(); i++)
        delete peth[i];

}

void MultiNtrodeDisplayWidget::setRealTimeMode(bool on) {
    realTimeMode = on; //controls how often the plots are updated to improve performance

    int scatterUpdateInterval;
    int primaryWaveFormUpdateInterval;
    int secondaryWaveformUpdateInterval;

    if (!realTimeMode) {
        scatterUpdateInterval = 1000;
        primaryWaveFormUpdateInterval = 200;
        secondaryWaveformUpdateInterval = 2000;
        scatterPlotTimer->start(scatterUpdateInterval); //this value dictates how often the scatter plots are plotted (in ms)
        scopePlotTimer->start(primaryWaveFormUpdateInterval); //this value dictates how often the single spike waveforms are plotted (in ms)
        waveformPlotTimer->start(secondaryWaveformUpdateInterval); //How often the clusters of waveforms are replotted
    } else {
        scatterUpdateInterval = 1000;
        primaryWaveFormUpdateInterval = 200;
        secondaryWaveformUpdateInterval = 2000;
        //scatterPlotTimer->start(scatterUpdateInterval); //this value dictates how often the scatter plots are plotted (in ms)
        scopePlotTimer->start(primaryWaveFormUpdateInterval); //this value dictates how often the single spike waveforms are plotted (in ms)
        //waveformPlotTimer->start(secondaryWaveformUpdateInterval); //How often the clusters of waveforms are replotted

        scatterPlotTimer->stop(); //this value dictates how often the scatter plots are plotted (in ms)
        //scopePlotTimer->stop(); //this value dictates how often the single spike waveforms are plotted (in ms)
        waveformPlotTimer->stop(); //How often the clusters of waveforms are replotted
    }
}

void MultiNtrodeDisplayWidget::setShownNtrode(int nTrodeInd) {

    graphicsWin->abandonDrawing();

    writeToMipmapMutex.lock();
    changingDisplayedNtrode = true;
    if (currentNTrode > -1) {
        nTrodeData[currentNTrode]->setDataNotShowing();
    }
    currentNTrode = nTrodeInd;
    nTrodeLabel->setText(QString("%1").arg(spikeConf->ntrodes[currentNTrode]->nTrodeId));


//    int numChannels = spikeConf->ntrodes[currentNTrode]->hw_chan.length();

    channelViewCombo->clear();
    channelViewCombo->addItems(nTrodeData[currentNTrode]->channelViewStrings);
    channelViewCombo->setCurrentIndex(nTrodeData[currentNTrode]->currentChannelListIndex);

    for (int i=1; i < clusterSelectButtons->buttons().length()+1; i++) {
        toggleClusterOn(i, nTrodeData[currentNTrode]->clustersDefined[i]);
    }

    //Sets triggers and initializes images
    triggerScope->setDisplayedNTrode(currentNTrode,
                                     nTrodeData[currentNTrode]->waveImage);

    //Sets images to display new data
    nTrodeData[currentNTrode]->setDataShowing();
    graphicsWin->setDisplayedNTrode(currentNTrode, nTrodeData[currentNTrode]->minAmplitude,nTrodeData[currentNTrode]->maxAmplitude,nTrodeData[currentNTrode]->mipMaps);
    changingDisplayedNtrode = false;
    updateScatterChannels(channelViewCombo->currentIndex());
    updateMultiPlotWindow();
    writeToMipmapMutex.unlock();

    //If the user has enabled the button to show only the selected cluster, hide the other clusters
    if (showOnlySelectedClusterMode) {
        for (int i=0; i<NUMCLUSTERS; i++) {
            if (i==selectedClusterInd) {
                graphicsWin->enableDisableClusters(true, i, currentNTrode);
            } else {
                graphicsWin->enableDisableClusters(false, i, currentNTrode);
            }
        }
    } else {
        //Reset display so all clusters are enabled
        for (int i=0; i<NUMCLUSTERS; i++) {
            graphicsWin->enableDisableClusters(true, i, currentNTrode);
        }
    }

//    updateMultiPlotWindow();
}

void MultiNtrodeDisplayWidget::singleViewButtonReleased() {
    if (!multiViewMode) {
        selectSingleView->setDown(true);
        selectMultiView->setDown(false);
    }

}

void MultiNtrodeDisplayWidget::multiViewButtonReleased() {
    if (multiViewMode) {
        selectMultiView->setDown(true);
        selectSingleView->setDown(false);
    }

}

void MultiNtrodeDisplayWidget::singleViewButtonPressed() {

    graphicsWin->abandonDrawing();
    setSingleView();
    emit singleViewSet();
    graphicsWin->updateBackgroundPlot();
    graphicsWin->scene->update();

}

void MultiNtrodeDisplayWidget::multiViewButtonPressed() {
    graphicsWin->abandonDrawing();
    if (nTrodeData[currentNTrode]->numberOfChannels > 1) {
        setMultiView();
        emit multiViewSet();
        graphicsWin->updateBackgroundPlot();
        graphicsWin->scene->update();
    } else {

        //selectMultiView->setChecked(false);
        selectMultiView->setDown(false);
        //selectSingleView->setChecked(true);
        selectSingleView->setDown(true);
    }

}

void MultiNtrodeDisplayWidget::clusterButtonPressed() {

    graphicsWin->abandonDrawing();
    selectedClusterInd = QObject::sender()->property("userData").toInt();
    graphicsWin->setCurrentCluster(selectedClusterInd);

    //If the user has enabled the button to show only the selected cluster, hide the other clusters
    if (showOnlySelectedClusterMode) {
        for (int i=0; i<NUMCLUSTERS; i++) {
            if (i==selectedClusterInd) {
                //nTrodeData[currentNTrode]->waveImage->setClusterShowing(true, i);
                graphicsWin->enableDisableClusters(true, i, currentNTrode);
            } else {
                //nTrodeData[currentNTrode]->waveImage->setClusterShowing(false, i);
                graphicsWin->enableDisableClusters(false, i, currentNTrode);
            }
        }
    }
}

void MultiNtrodeDisplayWidget::clusterRightClick(const QPoint &pos){

    int cluster = QObject::sender()->property("userData").toInt();
    QPoint globalPos = this->mapToParent(clusterSelectButtons->button(cluster-1)->pos());
    showClusterContextMenu(globalPos,currentNTrode,cluster);

}

void MultiNtrodeDisplayWidget::toolButtonPressed(int toolNum) {
    graphicsWin->abandonDrawing();
    currentTool = toolNum;
    graphicsWin->setTool(toolNum);
}

void MultiNtrodeDisplayWidget::deleteAllClusters() {
    //Delete all clusters from all nTrodes
    lockSpikeSortMutex();
    for (int i = 0;i<nTrodeData.length();i++) {
        nTrodeData[i]->deleteAllClusterData();
    }
    unlockSpikeSortMutex();
    nTrodeData[currentNTrode]->setMipMapData();
    updateMultiPlotWindow();
    triggerScope->scopeWindows->reDrawWaveforms();

    for (int cl=1;cl<clusterSelectButtons->buttons().length()+1;cl++) {

            toggleClusterOn(cl,false);

    }

}


void MultiNtrodeDisplayWidget::showClusterContextMenu(const QPoint& pos, int nTrodeIndex, int clusterIndex) {

    graphicsWin->abandonDrawing();

    QPoint globalPos = pos;
    QMenu channelMenu;

    QString deleteOption = "Delete entire cluster";
    QString psthOption = "Create PSTH...";
    QString showOption = "Enable Cluster";
    QString hideOption = "Disable Cluster";
    channelMenu.addAction(deleteOption);
    channelMenu.addAction(psthOption);
    channelMenu.addAction(showOption);
    channelMenu.addAction(hideOption);

    QAction* selectedItem = channelMenu.exec(globalPos);
    if (selectedItem) {
        // something was chosen, do stuff
        if (selectedItem->text() == psthOption) {
            //Open PSTH dialog
            openPSTHWindowByNTrode(clusterIndex, nTrodeIndex);

        }
        else if (selectedItem->text() == deleteOption) {
            //Delete the entire cluster and all associated polygons
            //graphicsWin uses a 1-based index for the clusters
            graphicsWin->deleteCluster(nTrodeIndex,clusterIndex);
        }

        else if (selectedItem->text() == hideOption){
            //Hides this rubber band's cluster
            //graphicsWin uses a 1-based index for the clusters
            graphicsWin->enableDisableClusters(false, clusterIndex, nTrodeIndex);
        }
        else if (selectedItem->text() == showOption){
            //Shows this rubber band's cluster
            //graphicsWin uses a 1-based index for the clusters
            graphicsWin->enableDisableClusters(true, clusterIndex, nTrodeIndex);
        }
    } else {
        // nothing was chosen
    }


}

void MultiNtrodeDisplayWidget::deleteClusterData(int nTrodeInd, int clusterInd) {
    //Delete data for an entire cluster

    if (nTrodeData.length() <= nTrodeInd) {
        return;
    }

    lockSpikeSortMutex();
    nTrodeData[nTrodeInd]->deleteClusterData(clusterInd);
    unlockSpikeSortMutex();
    nTrodeData[currentNTrode]->setMipMapData();
    updateMultiPlotWindow();
    triggerScope->scopeWindows->reDrawWaveforms();

    toggleClusterOn(clusterInd,false);

}

void MultiNtrodeDisplayWidget::deletePolygonData(int clusterInd, int bitInd) {


    lockSpikeSortMutex();
    nTrodeData[currentNTrode]->deletePolygonData(clusterInd,bitInd);
    unlockSpikeSortMutex();
    nTrodeData[currentNTrode]->setMipMapData();

    //Replot the mipmaps
    //graphicsWin->setData(nTrodeData[currentNTrode]->scatterData.constData(), nTrodeData[currentNTrode]->pointClusterOwnership);

    updateMultiPlotWindow();
    triggerScope->scopeWindows->reDrawWaveforms();

    if (!nTrodeData[currentNTrode]->clustersDefined[clusterInd]) {
        toggleClusterOn(clusterInd,false);
    }
}

void MultiNtrodeDisplayWidget::calculatePointsInsideAllPolygons(QVector<QList<ClusterPolygonSet> > polylist) {
    lockSpikeSortMutex();

    for (int nt=0;nt<polylist.length();nt++) {
        for (int polyInd=0;polyInd<polylist[nt].length();polyInd++) {
            nTrodeData[nt]->addPolygonData(polylist[nt][polyInd]);
            nTrodeData[nt]->calculatePointsInsidePolygon(polylist[nt][polyInd].clusterInd, polylist[nt][polyInd].bitInd);
        }

        for (int cl=1;cl<clusterSelectButtons->buttons().length()+1;cl++) {
            if (nTrodeData[nt]->clustersDefined[cl]) {              
                nTrodeData[nt]->calculateCluster(cl);
            }
        }

        updatePSTHByNTrode(nt);

    }

    unlockSpikeSortMutex();
    setShownNtrode(currentNTrode);
}

void MultiNtrodeDisplayWidget::calculatePointsInsidePolygon(int clusterInd, ClusterPolygonSet p, int bitInd) {
//    qDebug() << "Calculating points inside cluster" << clusterInd << bitInd ;

    if (!nTrodeData[currentNTrode]->clustersDefined[clusterInd] && (clusterInd > 0)) {
            toggleClusterOn(clusterInd,true);
    }
    lockSpikeSortMutex();
    nTrodeData[currentNTrode]->addPolygonData(p);
    nTrodeData[currentNTrode]->calculatePointsInsidePolygon(clusterInd,bitInd);
    //nTrodeData[currentNTrode]->calculateCluster(clusterInd);
    for (int cl=1;cl<clusterSelectButtons->buttons().length()+1;cl++) {
        if (nTrodeData[currentNTrode]->clustersDefined[cl]) {
            nTrodeData[currentNTrode]->calculateCluster(cl);
        }
    }
    unlockSpikeSortMutex();

    writeToMipmapMutex.lock();
    nTrodeData[currentNTrode]->setMipMapData();
    triggerScope->scopeWindows->reDrawWaveforms();
    writeToMipmapMutex.unlock();

    updatePSTHByNTrode(currentNTrode);
}



void MultiNtrodeDisplayWidget::toggleClusterOn(int clusterInd, bool on) {
    //clustersDefined[clusterInd] = on;
    nTrodeData[currentNTrode]->clustersDefined[clusterInd] = on;


    if (on) {
        clusterSelectButtons->button(clusterInd-1)->setText("-"+ QString("%1").arg(clusterInd) + "-");
    } else {
        clusterSelectButtons->button(clusterInd-1)->setText(QString("%1").arg(clusterInd));

    }
}

void MultiNtrodeDisplayWidget::zoom(int changeAmount) {

    //int trodeNum = streamConf->trodeIndexLookupByHWChan[dispHWChannels[selection]];
    //int trodeChannel = streamConf->trodeChannelLookupByHWChan[dispHWChannels[selection]];
    int currentMaxDisp = spikeConf->ntrodes[currentNTrode]->maxDisp[0];
    int newMaxDisp = currentMaxDisp+changeAmount;

    if (newMaxDisp >= 50) {
        spikeConf->setMaxDisp(currentNTrode,newMaxDisp);
        emit sig_newMaxDisp(newMaxDisp);
    }
}

void MultiNtrodeDisplayWidget::setMaxDisplay(int nTrode, int newMaxDisplay) {

    graphicsWin->abandonDrawing();
    nTrodeData[nTrode]->setZeroToMax(newMaxDisplay);
    emit sig_newMaxDisp(newMaxDisplay);

    if (nTrode == currentNTrode) {

        graphicsWin->setDisplayWindow(nTrodeData[nTrode]->minAmplitude,nTrodeData[nTrode]->maxAmplitude);
        graphicsWin->updateBackgroundPlot();
        graphicsWin->scene->update();


        triggerScope->setMaxDisplay(newMaxDisplay);
        /*triggerScope->setDisplayedNTrode(currentNTrode,
                                         nTrodeData[currentNTrode]->waveImage);*/
    }

}

void MultiNtrodeDisplayWidget::lockSpikeSortMutex() {
    spikeSortMutex.lock();
}

void MultiNtrodeDisplayWidget::unlockSpikeSortMutex() {
    spikeSortMutex.unlock();
}

void MultiNtrodeDisplayWidget::setSingleView() {

    if (multiViewMode) {
        multiViewMode = false;

        selectSingleView->setDown(true);
        selectMultiView->setDown(false);

        graphicsWin->setMultipleProjections(false);
        channelViewCombo->setEnabled(true);
        updateScatterChannels(channelViewCombo->currentIndex());
        graphicsWin->setDisplayWindow(nTrodeData[currentNTrode]->minAmplitude,nTrodeData[currentNTrode]->maxAmplitude);

    }
}

void MultiNtrodeDisplayWidget::setMultiView() {

    if (!multiViewMode) {
        multiViewMode = true;

        selectSingleView->setDown(false);
        selectMultiView->setDown(true);

        graphicsWin->setMultipleProjections(true);
        channelViewCombo->setEnabled(false);
        graphicsWin->setDisplayWindow(nTrodeData[currentNTrode]->minAmplitude,nTrodeData[currentNTrode]->maxAmplitude);

    }

}


void MultiNtrodeDisplayWidget::closeEvent(QCloseEvent* event) {
//    qDebug() << "Close nTrode " << nTrodeNumber;
    emit windowClosed();
    //event->accept();
    event->ignore();
}

void MultiNtrodeDisplayWidget::moveEvent(QMoveEvent *event) {

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("spikeWindow"));
    settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();

}

void MultiNtrodeDisplayWidget::showEvent(QShowEvent *) {

    //Remembered settings...
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    //Place the window where it was last
    settings.beginGroup(QLatin1String("spikeWindow"));
    QRect tempPosition = settings.value(QLatin1String("position")).toRect();
    if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
    }
    settings.endGroup();


}

void MultiNtrodeDisplayWidget::resizeEvent(QResizeEvent *event) {

    graphicsWin->abandonDrawing();

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("spikeWindow"));
    settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();

    //QSize newSize = event->size();
    //emit newWindowSize(newSize);
}

void MultiNtrodeDisplayWidget::refreshGraphicsWin() {
    graphicsWin->refresh();
}

void MultiNtrodeDisplayWidget::setStreamManagerPtr(StreamProcessorManager *mgr) {
    streamManager = mgr;
}

void MultiNtrodeDisplayWidget::openPSTHWindow(int clusterInd){
    openPSTHWindowByNTrode(clusterInd, currentNTrode);
}

void MultiNtrodeDisplayWidget::openPSTHWindowByNTrode(int clusterInd, int ntrode) {

    if (!streamManager->getPSTHTriggerID().isEmpty()) {
        PSTHDialog *newDialog = new PSTHDialog();
//        newDialog->setAttribute(Qt::WA_DeleteOnClose, true); //deletes the object when the window is closed

        newDialog->setWindowTitle(streamManager->getPSTHTriggerID() + " vs nTrode " + QString().number(spikeConf->ntrodes[ntrode]->nTrodeId) + ", unit " + QString().number(clusterInd) );
        newDialog->show();

        connect(this, SIGNAL(binsChanged(int)), newDialog, SLOT(setNumBins(int)));
        connect(this, SIGNAL(rangeChanged(int)), newDialog, SLOT(setRange(int)));
        connect(newDialog, SIGNAL(binsChanged(int)), this, SIGNAL(binsChanged(int)));
        connect(newDialog, SIGNAL(rangeChanged(int)), this, SIGNAL(rangeChanged(int)));
        connect(this, SIGNAL(windowClosed()), newDialog, SLOT(close()));

        uint32_t startOfRange = nTrodeData[ntrode]->getStartOfLogTime();
        QVector<uint32_t> dEventTimes = streamManager->getDigitalEventTimes();
        //Remove any trigger events that occured before the range of the saved spikes
        while (!dEventTimes.isEmpty() && dEventTimes.front() < startOfRange) {
            dEventTimes.pop_front();
        }

        if (streamManager != NULL) {
            newDialog->plot(dEventTimes,nTrodeData[ntrode]->getSpikeTimesForCluster(clusterInd));
        }
    } else {
         QMessageBox::warning(this, "No PSTH channel selected", "Please enable and set a PSTH trigger on a channel in the Aux tab. ", QMessageBox::Ok);
    }

}

void MultiNtrodeDisplayWidget::newSplitterPos(int index, int newPos) {
    emit splitterPosChanged(panelSplitter->sizes());
}

void MultiNtrodeDisplayWidget::setSplitterPos(QList<int> panelSizes) {
    panelSplitter->setSizes(panelSizes);
}

void MultiNtrodeDisplayWidget::scatterPlotTimerExpired() {
    if (!exportMode && currentNTrode > -1 && !realTimeMode) {

        //graphicsWin->update();
        //scatterDisplay->update();


        updateScatter();
        refreshGraphicsWin();
        //graphicsWin->updateBackgroundPlot();

        graphicsWin->scene->update();

        updateAllPSTHs();

    }

}

void MultiNtrodeDisplayWidget::scopePlotTimerExpired() {
    //This triggers plotting of the most recent spike waveform.

    if (scopePlotMinimumIntervalTimer.elapsed() < 10) {
        //We don't allow updating faster than this minimum update interval.
        return;
    }

    if (!exportMode && currentNTrode > -1 && nTrodeData[currentNTrode]->currentBufferIndex > -1 && !realTimeMode) {
        //triggerScope->scopeWindows->plotData(waveForms.data());
//        int ind = nTrodeData[currentNTrode]->currentBufferIndex;
        triggerScope->scopeWindows->plotData(/*nTrodeData[currentNTrode]->waveForms[ind].data(), ind*/);
        scopePlotMinimumIntervalTimer.restart();
        /*
        int numChannels = triggerScope->scopeWindows.length();
        for (int i = 0; i < numChannels; i++) {

            triggerScope->scopeWindows[i]->plotData(waveForms.data(), i);
        }*/
    }

}

void MultiNtrodeDisplayWidget::waveformPlotTimerExpired(){
    if (!exportMode && currentNTrode > -1 && triggerScope->scopeWindows->showAll) {        
        triggerScope->scopeWindows->reDrawWaveforms();
    }
}

void MultiNtrodeDisplayWidget::clearAllButtonPressed() {
    graphicsWin->abandonDrawing();

    //Clears scatterplots for all nTrodes
    for (int i=0; i < nTrodeData.length(); i++) {
        nTrodeData[i]->clearButtonPressed();
    }

    graphicsWin->clearData();
    //graphicsWin->setChannels(currentXdisplay, currentYdisplay, scatterData.constData());
    //scatterDisplay->setChannels(currentXdisplay, currentYdisplay, scatterData.constData());
    updateMultiPlotWindow();
    triggerScope->scopeWindows->reDrawWaveforms();
}

void MultiNtrodeDisplayWidget::clearButtonPressed() {
    graphicsWin->abandonDrawing();

    //Clears scatterplot for the current nTrode
    nTrodeData[currentNTrode]->clearButtonPressed();

    graphicsWin->clearData();
    //graphicsWin->setChannels(currentXdisplay, currentYdisplay, scatterData.constData());
    //scatterDisplay->setChannels(currentXdisplay, currentYdisplay, scatterData.constData());
    updateMultiPlotWindow();
    triggerScope->scopeWindows->reDrawWaveforms();
}

void MultiNtrodeDisplayWidget::updateScatterChannels(int channelCombo) {

    graphicsWin->abandonDrawing();

    if (!changingDisplayedNtrode) {
        currentXdisplay = nTrodeData[currentNTrode]->channelViewXList[channelCombo];
        currentYdisplay = nTrodeData[currentNTrode]->channelViewYList[channelCombo];
        nTrodeData[currentNTrode]->currentXdisplay = currentXdisplay;
        nTrodeData[currentNTrode]->currentYdisplay = currentYdisplay;
        nTrodeData[currentNTrode]->currentChannelListIndex = channelCombo;
        graphicsWin->setChannels(currentXdisplay, currentYdisplay, nTrodeData[currentNTrode]->scatterData.constData(),nTrodeData[currentNTrode]->pointClusterOwnership);
    }
    //scatterDisplay->setChannels(currentXdisplay, currentYdisplay, scatterData.constData());

}

void MultiNtrodeDisplayWidget::scatterPairSelected(int pairIndex) {
    setSingleView();
    channelViewCombo->setCurrentIndex(pairIndex);
    graphicsWin->updateBackgroundPlot();
    graphicsWin->scene->update();
}

void MultiNtrodeDisplayWidget::setClusterShowing(bool showing, quint8 cluster){
    nTrodeData[currentNTrode]->waveImage->setClusterShowing(showing, cluster);
    triggerScope->scopeWindows->reDrawWaveforms();
}

void MultiNtrodeDisplayWidget::loadClusterFile(QString filename) {
    graphicsWin->abandonDrawing();
    bool ok;
    ok = loadClusters(filename);
}

bool MultiNtrodeDisplayWidget::loadClusters(QString fileName) {
    qDebug() << "Loading cluster file";
    QDomDocument doc("SpikeSortingInfo");
    QFile file;

    file.setFileName(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "[MultiNtrodeDisplayWidget] Error:" << QString("File %1 not found").arg(fileName);
        return false;
    }
    qDebug() << "[MultiNtrodeDisplayWidget] Loading clusters from file " << fileName;

    if (!doc.setContent(&file)) {
        file.close();
        qDebug("[MultiNtrodeDisplayWidget] Error: XML didn't read properly.");
        return false;
    }

    file.close();
    QDomElement root = doc.documentElement();
    if (root.tagName() != "SpikeSortingInfo") {
        qDebug("[MultiNtrodeDisplayWidget] SpikeSortingInfo not root node.");
        return false;
    }

    QDomNodeList polygonClusterList = root.elementsByTagName("PolygonClusters");
    QDomNode timePointNode;
    QDomElement timePointElements;

    if (polygonClusterList.length() == 0) {
        qDebug() << "Cluster file parsing error: no PolygonClusters entry found in file.";
        return false;
    }

    int timePointInd = 0;
    if (polygonClusterList.length() > 0) {
        //if there are multiple timepoints, determine which one should be used based on the current timestamp
        for (int i=0; i<polygonClusterList.length();i++) {
            timePointNode = polygonClusterList.item(i);
            timePointElements = timePointNode.toElement();
            uint32_t tempTimePoint = timePointElements.attribute("Timepoint", "0").toULong();
            if (tempTimePoint < currentTimeStamp) {
                timePointInd = i;
            }
        }
    }

    QVector<QList<ClusterPolygonSet> > polylist;
    polylist.resize(spikeConf->ntrodes.length());

    QDomNode nTrodeNode = polygonClusterList.item(timePointInd).firstChild();
    QDomElement nTrodeElem;

    while (!nTrodeNode.isNull()) {
        nTrodeElem = nTrodeNode.toElement();
        bool ok;
        if (!nTrodeElem.isNull()) {
            int nTrodeIndex =  nTrodeElem.attribute("nTrodeIndex","").toInt(&ok);
            if (!ok) {
                qDebug() << "Cluster file parsing error: no ntrode index found for entry.";
                return false;
            }
            if ((nTrodeIndex < 0) || (nTrodeIndex > spikeConf->ntrodes.length())) {
                qDebug() << "Cluster file parsing error: index for nTrode not within correct range";
                return false;
            }
            QDomNode clusterNode = nTrodeNode.firstChild();
            QDomElement clusterElem;
            while (!clusterNode.isNull()) {
                clusterElem = clusterNode.toElement();
                if (!clusterElem.isNull()) {
                    int clusterIndex = clusterElem.attribute("clusterIndex","").toInt(&ok);
                    if (!ok) {
                        qDebug() << "Cluster file parsing error: no cluster index found for entry.";
                        return false;
                    }
                    if ((clusterIndex < 0) || (clusterIndex > NUMCLUSTERS)) {
                        qDebug() << "Cluster file parsing error: index for cluster not within correct range";
                    }
                    QDomNode polyNode = clusterNode.firstChild();
                    QDomElement polyElem;
                    while (!polyNode.isNull()) {
                        polyElem = polyNode.toElement();
                        if (!polyElem.isNull()) {
                            int xAxis = polyElem.attribute("xAxis","").toInt(&ok);
                            if (!ok) {
                                qDebug() << "Cluster file parsing error: no xAxis index found for polygon entry.";
                                return false;
                            }
                            if ((xAxis < 0) || ((xAxis > 1)&&(xAxis > spikeConf->ntrodes[nTrodeIndex]->hw_chan.length()))) { //Right now, only peak feature calculated, so we can use number of channels
                                qDebug() << "Cluster file parsing error: xAxis index for polygon not within correct range.";
                                return false;
                            }
                            int yAxis = polyElem.attribute("yAxis","").toInt(&ok);
                            if (!ok) {
                                qDebug() << "Cluster file parsing error: no yAxis index found for polygon entry.";
                                return false;
                            }
                            if ((yAxis < 0) || ((yAxis > 1)&&(yAxis > spikeConf->ntrodes[nTrodeIndex]->hw_chan.length()))) { //Right now, only peak feature calculated, so we can use number of channels
                                qDebug() << "Cluster file parsing error: yAxis index for polygon not within correct range.";
                                return false;
                            }

                            ClusterPolygonSet tempPoly;
                            tempPoly.clusterInd = clusterIndex;
                            tempPoly.xAxis = xAxis;
                            tempPoly.yAxis = yAxis;

                            QDomNode vertexNode = polyNode.firstChild();
                            QDomElement vertexElem;
                            while (!vertexNode.isNull()) {
                                vertexElem = vertexNode.toElement();
                                if (!vertexElem.isNull()) {
                                    int xVert = vertexElem.attribute("x","").toInt(&ok);
                                    if (!ok) {
                                        qDebug() << "Cluster file parsing error: x value for point in polygon not found.";
                                        return false;
                                    }
                                    int yVert = vertexElem.attribute("y","").toInt(&ok);
                                    if (!ok) {
                                        qDebug() << "Cluster file parsing error: y value for point in polygon not found.";
                                        return false;
                                    }
                                    tempPoly.points.push_back(QPoint(xVert,yVert));

                                }
                                vertexNode = vertexNode.nextSibling();
                            }
                            //Add the polygon to the list
                            polylist[nTrodeIndex].push_back(tempPoly);
                        }
                        polyNode = polyNode.nextSibling();
                    }

                }
                clusterNode = clusterNode.nextSibling();
            }
        }
        nTrodeNode = nTrodeNode.nextSibling();
    }

    file.close();

    graphicsWin->deleteAllClusters();
    graphicsWin->addPolygons(polylist);

    //nTrodeData[nt]->addPolygonData();

    return true;
}

void MultiNtrodeDisplayWidget::saveCurrentClusters(QString filename) {

    //Saves all of the polygons defining individual clusters in an xml file

    QDomDocument doc;
    QDomElement root = doc.createElement("SpikeSortingInfo");

    doc.appendChild(root);


    QDomElement clusterSection = doc.createElement("PolygonClusters");

    root.appendChild(clusterSection);
    clusterSection.setAttribute("Timepoint", currentTimeStamp); //Polygons may change at different timepoints, so we need to log the time
    clusterSection.setAttribute("Method", "Manually drawn polygons");

    for (int nt = 0; nt < nTrodeData.length(); nt++) {
        //One "nTrode" section for each ntrode
        QDomElement nTrodeElem = doc.createElement("nTrode");
        nTrodeElem.setAttribute("nTrodeIndex", nt); //The 0-based order that the ntrodes appears in workspace
        nTrodeElem.setAttribute("nTrodeID", spikeConf->ntrodes[nt]->nTrodeId); //The user-defined id number
        if (spikeConf->ntrodes[nt]->hw_chan.length() == 1) {
            nTrodeElem.setAttribute("Features", "Max channel 1, Min channel 1");
        } else {
            QString feat;
            for (int f=0;f<spikeConf->ntrodes[nt]->hw_chan.length();f++) {
                if (f > 0) {
                    feat.append(", ");
                }
                feat.append(QString("Max channel %1").arg(f+1));
            }
            nTrodeElem.setAttribute("Features", feat);
        }

        //We next create one "cluster" section for each cluster
        for (int cluster = 0; cluster < NUMCLUSTERS; cluster++) {
            if (!nTrodeData[nt]->clustersDefined[cluster]) {
                continue;
            }
            QDomElement clusterElem = doc.createElement("Cluster");
            clusterElem.setAttribute("clusterIndex",cluster);
            QList<ClusterPolygonSet> polylist = nTrodeData[nt]->getPolygonsForCluster(cluster);

            //One "Polygon" section for each polygon
            for (int poly = 0; poly < polylist.length(); poly++) {
                QDomElement polyElem = doc.createElement("Polygon");
                polyElem.setAttribute("xAxis",polylist[poly].xAxis);
                polyElem.setAttribute("yAxis",polylist[poly].yAxis);

                for (int v = 0; v < polylist[poly].points.length(); v++) {
                    QDomElement vElem = doc.createElement("Vertex");

                    vElem.setAttribute("x",polylist[poly].points[v].x());
                    vElem.setAttribute("y",polylist[poly].points[v].y());
                    polyElem.appendChild(vElem);
                }

                clusterElem.appendChild(polyElem);
            }

            nTrodeElem.appendChild(clusterElem);
        }

        clusterSection.appendChild(nTrodeElem);
    }

    QFile file(filename);
    //QFile file(QString("outConfig.xml"));
    //use QIODevice::Truncate to overwrite any previously existing file and QIODevice::Text to write newline characters using the local8bit encoding (cross platform diz)
    if (file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)) {
        QTextStream TextStream(&file);
        QString xmlString = doc.toString();
        QString vers = "<?xml version=\"1.0\"?>";
        TextStream << vers << Qt::endl << xmlString;
        file.close();

    }
    else {
        qDebug() << "Error: could not create file (MultiNtrodeDisplayWidget::saveCurrentClusters)";
    }





}

void MultiNtrodeDisplayWidget::orientationButtonClicked() {
    //The orientation button was clicked to toggle horizontal vs vertical orientation

    int neworientation = 0;
    if (orientationButton->text().compare(QChar(0xBB,0x21))==0) {
        neworientation = 1;
    } else {
        neworientation = 0;
        setOrientation(0);
    }

    setOrientation(neworientation);

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("spikeWindow"));
    settings.setValue(QLatin1String("orientation"), neworientation);
    settings.endGroup();
}

void MultiNtrodeDisplayWidget::setOrientation(int orientation) {
    if (orientation == 0) {
        //horizontal
        //orientationButton->setText("Horizontal");
        QChar arrowchar(0xBB,0x21);
        orientationButton->setText(arrowchar);
        panelSplitter->setOrientation(Qt::Horizontal);
        triggerScope->setOrientation(0);

    } else if (orientation == 1) {
        //vertical

        //orientationButton->setText("Vertical");
        QChar arrowchar(0xBA,0x21);
        orientationButton->setText(arrowchar);
        panelSplitter->setOrientation(Qt::Vertical);
        triggerScope->setOrientation(1);



    }

}


void MultiNtrodeDisplayWidget::setUnclusteredShowing(bool showing){
    nTrodeData[currentNTrode]->waveImage->setClusterShowing(showing, 0);
    triggerScope->scopeWindows->reDrawWaveforms();
    clusterColors[0] = showing ? QColor(255,255,255) : QColor(0,0,0);
    for(QList<ScatterPlotMipmap*>::iterator mipmap = nTrodeData[currentNTrode]->mipMaps.begin(); mipmap != nTrodeData[currentNTrode]->mipMaps.end(); ++mipmap){
        ScatterPlotMipmap* map = *mipmap;
        map->setColorTable(clusterColors);
    }
    nTrodeData[currentNTrode]->setMipMapData();

}

void MultiNtrodeDisplayWidget::setOnlySelectedClusterShowing(bool on) {

    showOnlySelectedClusterMode = on;
    showOnlySelectedCluster->setDown(on);

    if (on) {
        for (int i=0; i<NUMCLUSTERS; i++) {
            if (i==selectedClusterInd) {
                //nTrodeData[currentNTrode]->waveImage->setClusterShowing(true, i);
                graphicsWin->enableDisableClusters(true, i, currentNTrode);
            } else {
                //nTrodeData[currentNTrode]->waveImage->setClusterShowing(false, i);
                graphicsWin->enableDisableClusters(false, i, currentNTrode);
            }
        }
    } else {
        for (int i=0; i<NUMCLUSTERS; i++) {
            //nTrodeData[currentNTrode]->waveImage->setClusterShowing(true, i);
            graphicsWin->enableDisableClusters(true, i, currentNTrode);
        }
    }


}

void MultiNtrodeDisplayWidget::receiveNewSpikes(QVector<SpikeWaveform> spikes)
{

    bool windowNeedsUpdate = false;

    for (int spikeIndex = 0; spikeIndex < spikes.length(); spikeIndex++) {

        int nTrodeInd = spikes.at(spikeIndex).ntrodeIndex;

        if (nTrodeInd == currentNTrode) {

            spikeSortMutex.lock();
            nTrodeData[nTrodeInd]->receiveNewSpike(spikes[spikeIndex]);
            if (!changingDisplayedNtrode) {
                writeToMipmapMutex.lock();
                graphicsWin->setDataPoint(nTrodeData[currentNTrode]->scatterData.constData(),
                                          nTrodeData[currentNTrode]->currentBufferIndex,
                                          nTrodeData[currentNTrode]->pointClusterOwnership[nTrodeData[currentNTrode]->currentBufferIndex]);

                writeToMipmapMutex.unlock();
            }
            spikeSortMutex.unlock();
            windowNeedsUpdate = true;

        } else {
            nTrodeData[nTrodeInd]->receiveNewSpike(spikes[spikeIndex]);
        }

    }

    if (windowNeedsUpdate) {
        emit spikeUpdateNeeded();
    }

}

void MultiNtrodeDisplayWidget::receiveNewEvent(int nTrodeInd, const QVector<int2d>* waveForm, const int* peaks, uint32_t time) {
//qDebug() << "spike received";
    if (nTrodeInd == currentNTrode) {
        spikeSortMutex.lock();
        nTrodeData[nTrodeInd]->receiveNewEvent(waveForm,peaks,time);
        if (!changingDisplayedNtrode) {
            writeToMipmapMutex.lock();
            graphicsWin->setDataPoint(nTrodeData[currentNTrode]->scatterData.constData(),
                                      nTrodeData[currentNTrode]->currentBufferIndex,
                                      nTrodeData[currentNTrode]->pointClusterOwnership[nTrodeData[currentNTrode]->currentBufferIndex]);
            writeToMipmapMutex.unlock();
        }
        spikeSortMutex.unlock();
    } else {
        nTrodeData[nTrodeInd]->receiveNewEvent(waveForm,peaks,time);
    }

}

void MultiNtrodeDisplayWidget::changeAudioChannel(int hardwareChannel) {

    int channelFound = -1;
    if (triggerScope->nTrodeNumber == streamConf->trodeIndexLookupByHWChan.at(hardwareChannel))
      channelFound = streamConf->trodeChannelLookupByHWChan.at(hardwareChannel);

    triggerScope->updateAudioHighlight(channelFound);

}

void MultiNtrodeDisplayWidget::updateScatter(){
    nTrodeData[currentNTrode]->setMipMapData();
}

void MultiNtrodeDisplayWidget::openMultiHistogramWindow(){
    openMultiHistogramWindow(currentNTrode);
}

void MultiNtrodeDisplayWidget::openMultiHistogramWindow(int ntrode){
    graphicsWin->abandonDrawing();
    if (streamManager && !streamManager->getPSTHTriggerID().isEmpty()) {
        MultiPlotDialog *newwindow = new MultiPlotDialog(0, spikeConf->ntrodes[ntrode]->color);
        newwindow->show();
        connect(this, SIGNAL(updateMultiPlot(int,QVector<uint32_t>,QVector<uint32_t>,QVector<QVector<uint32_t> >)),
                newwindow, SLOT(update(int,QVector<uint32_t>,QVector<uint32_t>,QVector<QVector<uint32_t> >)));
//        connect(newwindow, SIGNAL(requestUpdate(int)), this, SLOT(updatePSTHByNTrode(int)));
        connect(newwindow, SIGNAL(requestPSTH(int, int)), this, SLOT(openPSTHWindowByNTrode(int,int)));
        connect(newwindow, SIGNAL(binsChanged(int)), this, SIGNAL(binsChanged(int)));
        connect(newwindow, SIGNAL(rangeChanged(int)), this, SIGNAL(rangeChanged(int)));
        connect(this, SIGNAL(binsChanged(int)), newwindow, SLOT(setNumBins(int)));
        connect(this, SIGNAL(rangeChanged(int)), newwindow, SLOT(setRange(int)));
        connect(this, SIGNAL(windowClosed()), newwindow, SLOT(close()));
        pethWindows.insert(ntrode, newwindow);
//        updateMultiPlotWindow();
        updatePSTHByNTrode(ntrode);
    }
    else{
        QMessageBox::warning(this, "No PSTH channel selected", "Please enable and set a PSTH trigger on a channel in the Aux tab. ", QMessageBox::Ok);
    }

}

void MultiNtrodeDisplayWidget::openAllNTrodePETHs(){

    if (!streamManager->getPSTHTriggerID().isEmpty()) {
        QMessageBox::StandardButton answer = QMessageBox::question(0, "Warning", "Opening too many windows may slow down Trodes. Proceed?",  QMessageBox::Yes|QMessageBox::No);
        if(answer != QMessageBox::Yes)
            return;

        for(int i = 0; i < spikeConf->ntrodes.length(); i++){
            openMultiHistogramWindow(i);
        }
    }
    else{
        QMessageBox::warning(this, "No PSTH channel selected", "Please enable and set a PSTH trigger on a channel in the Aux tab. ", QMessageBox::Ok);
    }
}

void MultiNtrodeDisplayWidget::updateMultiPlotWindow(){
    updatePSTHByNTrode(currentNTrode);
}

void MultiNtrodeDisplayWidget::startDedicatedSpikePub() {
    std::string address = trodes::get_network_address(networkConf->trodesHost);
    int port = trodes::get_network_port(networkConf->trodesPort);

    spikeTrodesPub = std::move(
        std::unique_ptr<trodes::network::SourcePublisher<trodes::network::SpikePacket>>(
            new trodes::network::SourcePublisher<trodes::network::SpikePacket>(address, port, "source.spikes")
        )
    );

    waveformTrodesPub = std::move(
        std::unique_ptr<trodes::network::SourcePublisher<trodes::network::TrodesSpikeWaveformData>>(
            new trodes::network::SourcePublisher<trodes::network::TrodesSpikeWaveformData>(address, port, "source.waveforms")
        )
    );

    qDebug() << "[MultiNtrodeDisplayWidget::startDedicatedSpikePub] Set up publisher to \"source.spikes\" to publish at address"
             << QString::fromStdString(address)  << "port" << port;

    int maxChan = 0;
    for(int i = 0; i < spikeConf->ntrodes.length(); i++){
        // TODO: get rid of qMax
        maxChan = qMax(maxChan, spikeConf->ntrodes[i]->unconverted_hw_chan.length());
    }
    size_t totalBlockSize = sizeof(int)*2 + sizeof(uint32_t) + sizeof(int) + (POINTSINWAVEFORM*sizeof(int2d))*maxChan;
    spikeDataBlock = new byte[(int)totalBlockSize];
}

void MultiNtrodeDisplayWidget::publishSpike(int nTrodeID, int cluster, uint32_t timestamp, int numChan, const QVector<QVector<int2d> > waveform) {
    //mark: nrRefactor

    //data format is as follows: [32 bit nTrode ID, 32 bit cluster ID, 32 unsigned bit timestamp, numChan * 40 32 bit waveform points (2 16 bit pts each) ]
    //encode data to bytes;
    byte *data = spikeDataBlock;
    size_t offset = 0;
    size_t curSize;

    curSize = sizeof(int);
    memcpy(data+offset, &nTrodeID, curSize);
    offset += curSize;

    curSize = sizeof(int);
    memcpy(data+offset, &cluster, curSize);
    offset += curSize;

    curSize = sizeof(uint32_t);
    memcpy(data+offset, &timestamp, curSize);
    offset += curSize;

    curSize = sizeof(int);
    *(int*)(data+offset) = numChan*POINTSINWAVEFORM;
    offset += curSize;

    for (int j = 0; j < numChan; j++) {
        const QVector<int2d> curWaveform = waveform[j];
        memcpy(data+offset, curWaveform.constData(),curWaveform.size()*sizeof(int2d));
        offset += curWaveform.size()*sizeof(int2d);
    }

    // There's something weird going on with this waveform
    // There are 40 points in a waveform per channel sample
    // but each point is an int2d containing a template and
    // an actual sample value.
    // So we only need the y values from the int2d struct.
    std::vector<std::vector<int16_t>> spikeDataBuffer;
    for (auto c = 0; c < numChan; c++) {
        const QVector<int2d> curWaveform = waveform[c];
        std::vector<int16_t> spikeDataBufferWaveform;
        for (auto sample : curWaveform) {
            spikeDataBufferWaveform.push_back(sample.y);
        }
        spikeDataBuffer.push_back(spikeDataBufferWaveform);
    }

    int64_t msg_timestamp = trodes::network::util::get_timestamp();
    trodes::network::SpikePacket spikeData = {timestamp, (int32_t)nTrodeID,cluster, msg_timestamp};
    spikeTrodesPub->publish(spikeData);

    trodes::network::TrodesSpikeWaveformData waveformData = {timestamp,(uint32_t)nTrodeID,spikeDataBuffer, msg_timestamp};
    waveformTrodesPub->publish(waveformData);
}

void MultiNtrodeDisplayWidget::updatePSTHByNTrode(int ntrode){
    if(streamManager && pethWindows.contains(ntrode)){
//        QElapsedTimer t; t.start();
        uint32_t startOfRange = nTrodeData[ntrode]->getStartOfLogTime();
        QVector<uint32_t> dEventTimes = streamManager->getDigitalEventTimes();
        //Remove any trigger events that occured before the range of the saved spikes
        while (!dEventTimes.isEmpty() && dEventTimes.front() < startOfRange) {
            dEventTimes.pop_front();
        }
        QVector<uint32_t> sptmp = nTrodeData[ntrode]->getAllSpikeTimes();
        QVector<QVector<uint32_t> > tmp = nTrodeData[ntrode]->getAllClusterSpikeTimes();
//        emit updateMultiPlot(ntrode, dEventTimes, sptmp, tmp);
        pethWindows.value(ntrode)->update(ntrode, dEventTimes, sptmp, tmp);
    }
}

void MultiNtrodeDisplayWidget::updateAllPSTHs(){
    if(!streamManager || pethWindows.empty())
        return;

    QMultiMap<int, MultiPlotDialog*>::const_iterator i = pethWindows.constBegin();
    while (i != pethWindows.constEnd()) {
        if(!i.value()->isVisible()){
            ++i;
            continue;
        }
        uint32_t startOfRange = nTrodeData[i.key()]->getStartOfLogTime();
        QVector<uint32_t> dEventTimes = streamManager->getDigitalEventTimes();

        //Remove any trigger events that occured before the range of the saved spikes
        while (!dEventTimes.isEmpty() && dEventTimes.front() < startOfRange) {
            dEventTimes.pop_front();
        }
        QVector<uint32_t> sptmp = nTrodeData[i.key()]->getAllSpikeTimes();
        QVector<QVector<uint32_t> > tmp = nTrodeData[i.key()]->getAllClusterSpikeTimes();

        i.value()->update(i.key(), dEventTimes, sptmp, tmp);
        ++i;
    }
}

//TriggerScopeDisplayWidget displays all ScopeWindows for one nTrode.
TriggerScopeDisplayWidget::TriggerScopeDisplayWidget(QWidget *parent, int bsize, bool vd) :
  QFrame(parent)
{

  emitMaxDispSignal = true;
  emitThreshSignal = true;
  validDriver = vd;

  //nTrodeNumber = nTrodeNum;
  //numberOfChannels = numChannels;
  QGridLayout *mainLayout = new QGridLayout();
  QGridLayout *controlLayout = new QGridLayout();
  setStyleSheet("QFrame {border: 2px solid lightgray;"
                        "border-radius: 4px;"
                        "padding: 2px;}");


  controlFrame = new QFrame(this);

  controlFrame->setStyleSheet("QFrame {border: 2px solid lightgray;"
                              "border-radius: 4px;"
                              "padding: 2px;"
                              "background-color: white}");


  scopeLayout = new QGridLayout();
  scopeLayout->setHorizontalSpacing(10);


  //if true, all channels in the nTrode change together
  linkMaxDisplaySettings = true;
  linkThreshSettings = true;


  QFont labelFont;
  labelFont.setPixelSize(10);
  labelFont.setFamily("Arial");
  maxLabel = new QLabel(this);
  maxLabel->setStyleSheet("QFrame {border: 0px solid white;"
                              "border-radius: 4px;"
                              "padding: 0px;}");
  maxLabel->setFont(labelFont);
  maxLabel->setAlignment(Qt::AlignRight);
  maxLabel->setFixedWidth(25);
  zeroLabel = new QLabel(this);
  zeroLabel->setStyleSheet("QFrame {border: 0px solid white;"
                              "border-radius: 4px;"
                              "padding: 0px;}");
  zeroLabel->setFont(labelFont);
  zeroLabel->setAlignment(Qt::AlignRight);
  zeroLabel->setFixedWidth(25);
  minLabel = new QLabel(this);
  minLabel->setStyleSheet("QFrame {border: 0px solid white;"
                              "border-radius: 4px;"
                              "padding: 0px;}");
  minLabel->setFont(labelFont);
  minLabel->setAlignment(Qt::AlignRight);
  minLabel->setFixedWidth(25);
  maxLabel->setText(QString("%1").arg(-100));
  minLabel->setText(QString("%1").arg(100));
  zeroLabel->setText(QString("%1").arg(0));

  maxDispControl = new QSpinBox(this);
  maxDispControl->setFrame(false);
  maxDispControl->setStyleSheet("QSpinBox { background-color: white; }");
  maxDispControl->setMinimum(50);
  maxDispControl->setMaximum(4000);
  maxDispControl->setSingleStep(25);
  maxDispControl->setFixedSize(50,22);
  maxDispControl->setAlignment(Qt::AlignRight);
  maxDispControl->setFocusPolicy(Qt::NoFocus);
  maxDispControl->setToolTip(tr("Display range (+/- uV)"));
  QFont font(maxDispControl->font());

#ifdef Q_WS_WIN
 font.setPointSize(8);
#else
  font.setPointSize(8);
#endif

  font.setFamily("Arial");
  maxDispControl->setFont(font);
  //maxDispControl->setValue(spikeConf->ntrodes[nTrodeNumber]->maxDisp[0]);
  connect(maxDispControl,SIGNAL(valueChanged(int)),this,SLOT(updateMaxDisplay()));

  QLabel *maxDispLabel = new QLabel(this);
  maxDispLabel->setStyleSheet("QFrame {border: 0px solid white;"
                              "border-radius: 4px;"
                              "padding: 0px;"
                              "background-color: white}");
  maxDispLabel->setContentsMargins(0,0,0,0);
  maxDispLabel->setFrameStyle(QFrame::NoFrame);
  maxDispLabel->setFont(font);
  //maxDispLabel->setFixedSize(50,22);
  maxDispLabel->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
  //maxDispLabel->setFixedWidth(75);
  maxDispLabel->setText(QString("Zoom range (μV):"));
  //maxDispLabel->setText(QString::fromUtf8("Zoom range (μV):"));

  threshControl = new QSpinBox(this);
  threshControl->setFrame(false);
  //threshControl->setPalette(pal);
  threshControl->setStyleSheet("QSpinBox { background-color: white; }");
  //threshControl->setButtonSymbols(QAbstractSpinBox::NoButtons);
  threshControl->setMinimum(10);
  threshControl->setMaximum(4000);
  threshControl->setSingleStep(10);
  threshControl->setFixedSize(50,22);
  threshControl->setFont(font);
  threshControl->setAlignment(Qt::AlignRight);
  threshControl->setFocusPolicy(Qt::NoFocus);
  threshControl->setToolTip(tr("Trigger threshold (μV)"));
  //threshControl->setValue(spikeConf->ntrodes[nTrodeNumber]->thresh[0]);
  connect(threshControl,SIGNAL(valueChanged(int)),this,SLOT(updateThreshhold()));

  QLabel *threshLabel = new QLabel(this);
  threshLabel->setStyleSheet("QFrame {border: 0px solid white;"
                              "border-radius: 4px;"
                              "padding: 0px;"
                              "background-color: white}");
  threshLabel->setContentsMargins(0,0,0,0);

  threshLabel->setFont(font);
  threshLabel->setAlignment(Qt::AlignRight|Qt::AlignVCenter);
  //threshLabel->setFixedWidth(75);
  threshLabel->setText(QString("Threshold (μV):"));

  controlLayout->addWidget(maxDispLabel,0,1,Qt::AlignRight);
  controlLayout->addWidget(threshLabel,1,1,Qt::AlignRight);
  controlLayout->addWidget(maxDispControl,0,2,Qt::AlignLeft);
  controlLayout->addWidget(threshControl,1,2,Qt::AlignLeft);
  //controlLayout->setColumnStretch(0,1);
  controlLayout->setVerticalSpacing(2);
  controlLayout->setHorizontalSpacing(0);
  controlLayout->setContentsMargins(0,0,0,0);
  //mainLayout->addLayout(controlLayout,0,0,Qt::AlignCenter);
  controlFrame->setLayout(controlLayout);
  controlFrame->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
  controlFrame->setLineWidth(3);
  controlFrame->setContentsMargins(7,6,4,6);

  //controlFrame->setVisible(false);


  QCheckBox *allwaveformsCheckbox = new QCheckBox("Show all waveforms");
  allwaveformsCheckbox->setFont(labelFont);
  //QCheckBox *singlewaveformCheckbox = new QCheckBox("Show one waveform");
  //singlewaveformCheckbox->setVisible(false);
  //mainLayout->addWidget(singlewaveformCheckbox, 1, 0, Qt::AlignLeft);
  mainLayout->addWidget(allwaveformsCheckbox, 0, 0, Qt::AlignLeft);


  mainLayout->addWidget(controlFrame,0,1,Qt::AlignRight);


  triggerButtonLayout = new QGridLayout();

  /*
  for (int i=0;i<numChannels; i++) {

    triggerSwitches.push_back(new QPushButton(this));
    triggerSwitches[i]->setFlat(true);

    triggerSwitches[i]->setStyleSheet("QPushButton {border: 2px solid lightgray;"
                          "border-radius: 4px;"
                          "padding: 2px;}");

    triggerSwitches[i]->setFont(labelFont);
    //triggerSwitches[i]->setFixedWidth(25);
    triggerSwitches[i]->setToolTip(tr("Trigger on/off"));
    triggerSwitches[i]->setAutoExclusive(false);
    triggerSwitches[i]->setCheckable(true);
    triggerSwitches[i]->setChecked(spikeConf->ntrodes[nTrodeNumber]->triggerOn[i]);

    triggerSwitchMapper->setMapping(triggerSwitches[i], i);
    connect(triggerSwitches[i], SIGNAL(clicked()), triggerSwitchMapper, SLOT(map()));
    if (triggerSwitches[i]->isChecked()) {
        triggerSwitches[i]->setText("On");
    } else {
        triggerSwitches[i]->setText("Off");
    }

    triggerButtonLayout->addWidget(triggerSwitches[i],0,i,Qt::AlignCenter);

    threshholds.push_back(0);
    maxDisplays.push_back(0);

  }
  */

  scopeLayout->addLayout(triggerButtonLayout,0,1);



  //Create a layout for the scope window y axis labels
  QGridLayout* rangeLayout = new QGridLayout();
  rangeLayout->setContentsMargins(0,0,0,0);
  rangeLayout->addWidget(maxLabel,0,0);
  rangeLayout->addWidget(minLabel,4,0);
  rangeLayout->addWidget(zeroLabel,2,0);
  rangeLayout->setRowStretch(1,1);
  rangeLayout->setRowStretch(3,1);
  scopeLayout->addLayout(rangeLayout,1,0);


  scopeWindows = new ScopeWindow(this, bsize, validDriver);
  //scopeWindows->nTrodeNum = nTrodeNum;
  connect(scopeWindows,SIGNAL(clicked(int)),this,SLOT(updateAudio(int)));
  connect(scopeWindows,SIGNAL(zoom(int)),this,SIGNAL(zoom(int)));
  scopeWindows->setMinimumWidth(50);
  //scopeWinLayout->addWidget(scopeWindows,0,1,Qt::AlignCenter);
  //scopeWinLayout->setColumnStretch(1,1);
  scopeLayout->addWidget(scopeWindows,1,1);

  QLabel *trigLabel = new QLabel(this);
  trigLabel->setStyleSheet("QFrame {border: 0px solid white;"
                              "border-radius: 4px;"
                              "padding: 0px;}");
  trigLabel->setFont(labelFont);
  trigLabel->setAlignment(Qt::AlignRight);
  trigLabel->setFixedWidth(25);
  trigLabel->setFixedHeight(25);
  trigLabel->setText("Trig:");
  scopeLayout->addWidget(trigLabel,0,0,Qt::AlignVCenter);


  connect(allwaveformsCheckbox, SIGNAL(toggled(bool)), scopeWindows, SLOT(toggleAll(bool)));
  //connect(singlewaveformCheckbox, SIGNAL(toggled(bool)), scopeWindows, SLOT(toggleSingle(bool)));
  //singlewaveformCheckbox->setChecked(true);
  allwaveformsCheckbox->setChecked(true);
  allwaveformsCheckbox->setChecked(false);


  mainLayout->addLayout(scopeLayout,2,0, 1, 2);
 /*
  for (int i=0;i<numChannels; i++) {
      setMaxDisplay_display(i,spikeConf->ntrodes[nTrodeNumber]->maxDisp[i]);
      setThreshhold_display(i,spikeConf->ntrodes[nTrodeNumber]->thresh[i]);
  }
*/

  //connect(this, SIGNAL(threshEditChanged(int)), this, SLOT(updateThreshhold(int)));
  //connect(this, SIGNAL(maxDispEditChanged(int)), this, SLOT(updateMaxDisplay(int)));
  //connect(this, SIGNAL(audioChanged(int)), this, SLOT(updateAudio(int)));
  //connect(spikeConf,SIGNAL(newMaxDisplay(int,int)),this,SLOT(setMaxDisplay(int,int)));
  //connect(spikeConf,SIGNAL(newThreshold(int,int)),this,SLOT(setThreshold(int,int)));


  setLayout(mainLayout);

  /*
  if (nTrodeNum == 0) {
      //audioButton[0]->setChecked(true);
      updateAudio(0);
      audioChanged(0);
  }*/


}

void TriggerScopeDisplayWidget::setOrientation(int orientation) {
    if (orientation == 0) {
        //horizontal
        controlFrame->setVisible(true);
    } else if (orientation == 1) {
        //vertical
        controlFrame->setVisible(false); //to save vertical space
    }
}

void TriggerScopeDisplayWidget::toggleButton(bool b){
    scopeWindows->setVisible(!b);
}

void TriggerScopeDisplayWidget::setDisplayedNTrode(int nTrodeInd, WaveFormPlotImage* img/*, QVector<QVector<vertex2d> > *waves*/) {
    while (triggerSwitches.length() > 0) {
        //triggerButtonLayout->removeItem()
        delete triggerSwitches.takeLast();
    }

    nTrodeNumber = nTrodeInd;
    numberOfChannels = spikeConf->ntrodes[nTrodeNumber]->hw_chan.length();
    scopeWindows->setNTrode(nTrodeNumber, img/*, waves*/);
    setMaxDisplay(spikeConf->ntrodes[nTrodeNumber]->maxDisp[0]);
    setThreshold(spikeConf->ntrodes[nTrodeNumber]->thresh[0]);
    setTraceColor(spikeConf->ntrodes[nTrodeNumber]->color);

    QList<QPushButton*> tmp = triggerSwitches;
    for (int i=0;i<numberOfChannels; i++) {

      triggerSwitches.push_back(new TrodesButton());
      triggerSwitches[i]->setFlat(true);


      triggerSwitches[i]->setToolTip(tr("Trigger on/off"));
      triggerSwitches[i]->setAutoExclusive(false);
      triggerSwitches[i]->setCheckable(true);
      triggerSwitches[i]->setChecked(spikeConf->ntrodes[nTrodeNumber]->triggerOn[i]);
      triggerSwitches[i]->setProperty("channel",QVariant(i));

      //triggerSwitchMapper->setMapping(triggerSwitches[i], i);
      connect(triggerSwitches[i], SIGNAL(clicked()), this, SLOT(triggerButtonToggled()));
      if (triggerSwitches[i]->isChecked()) {
          triggerSwitches[i]->setText("On");
      } else {
          triggerSwitches[i]->setText("Off");
      }

      triggerButtonLayout->addWidget(triggerSwitches[i],0,i,Qt::AlignCenter);
      //triggerSwitches[i]->setVisible(false);

    }
    qDeleteAll(tmp);

}

void TriggerScopeDisplayWidget::triggerButtonToggled() {
    int channel = sender()->property("channel").toInt();
    if (triggerSwitches[channel]->isChecked()) {
        triggerSwitches[channel]->setText("On");
    } else {
        triggerSwitches[channel]->setText("Off");
    }

    spikeConf->setTriggerMode(nTrodeNumber,channel,triggerSwitches[channel]->isChecked());

}

void TriggerScopeDisplayWidget::turnOffAudio() {

    updateAudio(-1);

}

void TriggerScopeDisplayWidget::setTraceColor(QColor color) {

    scopeWindows->traceColor = color;

}

void TriggerScopeDisplayWidget::setMaxDisplay(int newMax) {
    maxDisplay = newMax;
    emitMaxDispSignal = false;
    maxDispControl->setValue(newMax);
    emitMaxDispSignal = true;
    scopeWindows->setMaxDisplay(maxDisplay);
    maxLabel->setText(QString("%1").arg(-newMax));
    minLabel->setText(QString("%1").arg(newMax));

}

void TriggerScopeDisplayWidget::setThreshold(int newThreshold) {
    threshold = newThreshold;
    emitThreshSignal = false;
    threshControl->setValue(newThreshold);
    emitThreshSignal = true;
    scopeWindows->setThresh(threshold);

}

void TriggerScopeDisplayWidget::updateAudio(int channel)
{
  if (channel > -1) {
    //emit(hasAudio());

    int hw_chan = spikeConf->ntrodes[nTrodeNumber]->hw_chan[channel];
    emit setAudioChannel(hw_chan);
    emit updateAudioSettings();
    if (numberOfChannels > 1) {
        scopeWindows->setHighlight(channel);
    } else {
        scopeWindows->setNoHighlight();
    }

  } else {
    scopeWindows->setNoHighlight();
  }
}

void TriggerScopeDisplayWidget::updateAudioHighlight(int channel)
{
  if (channel > -1) {
    //int hw_chan = spikeConf->ntrodes[nTrodeNumber]->hw_chan[channel];
    //emit setAudioChannel(hw_chan);
    if (numberOfChannels > 1) {
        scopeWindows->setHighlight(channel);
    } else {
        scopeWindows->setNoHighlight();
    }

  } else {
    scopeWindows->setNoHighlight();
  }
}

void TriggerScopeDisplayWidget::updateThreshhold() {
    //updates threshold for all channels in the nTrode
    int newThresh = threshControl->value();
    threshold = newThresh;

    if (emitThreshSignal) {
        emit newThreshSignal(newThresh);
        if (spikeConf->ntrodes[nTrodeNumber]->thresh[0] != threshold) {
            for (int i=0; i<spikeConf->ntrodes[nTrodeNumber]->thresh.length(); i++) {
                spikeConf->setThresh(nTrodeNumber,newThresh);
            }
        }
        scopeWindows->setThresh(newThresh);
    }

}

void TriggerScopeDisplayWidget::updateMaxDisplay() {
    //updates max display for all channels in the nTrode
    int newMaxDisplay = maxDispControl->value();
    maxDisplay = newMaxDisplay;

    if (emitMaxDispSignal) {
        if (newMaxDisplay < 0) {
            newMaxDisplay = 0;
        }
        maxLabel->setText(QString("%1").arg(-newMaxDisplay));
        minLabel->setText(QString("%1").arg(newMaxDisplay));

        if (spikeConf->ntrodes[nTrodeNumber]->maxDisp[0] != maxDisplay) {
            for (int i=0; i<spikeConf->ntrodes[nTrodeNumber]->maxDisp.length(); i++) {
                spikeConf->setMaxDisp(nTrodeNumber,maxDisplay);
            }
        }

        scopeWindows->setMaxDisplay(newMaxDisplay);

    }

}

void TriggerScopeDisplayWidget::setCurrentDisplayPair(int ch1, int ch2) {
    pairChannel1 = ch1;
    pairChannel2 = ch2;

    for (int i=0; i < triggerSwitches.length(); i++) {
        if (i==pairChannel1 || i==pairChannel2) {
            triggerSwitches[i]->setStyleSheet("QPushButton {border: 2px solid red;"
                                  "border-radius: 4px;"
                                  "padding: 2px;}");
        } else {
            triggerSwitches[i]->setStyleSheet("QPushButton {border: 2px solid lightgrey;"
                                  "border-radius: 4px;"
                                  "padding: 2px;}");
        }
    }

}

/*
void TriggerScopeDisplayWidget::setThreshhold_display(int channel, int newThresh) {
    threshholds[channel] = newThresh;
    //threshSpin[channel]->setValue(newThresh);
    scopeWindows->setThresh(newThresh);
}

void TriggerScopeDisplayWidget::setMaxDisplay_display(int channel, int newMaxDisplay) {
    maxDisplays[channel] = newMaxDisplay;
    scopeWindows->setMaxDisplay(newMaxDisplay);
    maxLabel->setText(QString("%1").arg(newMaxDisplay));
    minLabel->setText(QString("%1").arg(-newMaxDisplay));

}*/


//ScopeWindow plots the spike waveform for one ntrode
//ScopeWindow::ScopeWindow(QWidget *parent, int bsize, int vd):QWidget(parent){
//ScopeWindow::ScopeWindow(QWidget *parent, int bsize, int vd):QOpenGLWidget (parent) {
ScopeWindow::ScopeWindow(QWidget *parent, int bsize, int vd):QOpenGLWidget (parent) {
  setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

//  setAutoFillBackground(false);

  pairChannel1 = -1;
  pairChannel2 = -1;

  traceColor = QColor(255,255,255);
  nTrodeNum = -1;
  maxAmplitude = (400*65536)/AD_CONVERSION_FACTOR; //default is for Intan-based systems
  thresh = 0;
  beforeTriggerLength = 10;
  afterTriggerLength = 30;
  numChannels = 0;
  highlight = false;
  showAll = false;
  showOne = true;
  setMinimumHeight(80);
  bufferSize = bsize;
  currImage = QImage();
  initialized = false;
  renderer = 0;
  resizeTimer.start();
  setAttribute(Qt::WA_NoSystemBackground);
  //setFixedWidth(50);
  //setMinimumWidth(40);
  validDriver = vd;
  renderer = new RenderWorker;
  if(validDriver){
    renderThread = new QThread();
    renderThread->setObjectName("RenderThread");
    qRegisterMetaType<ClusterWaveformSet*>("ClusterWaveformSet*");
    connect(this, SIGNAL(startRender()), renderer, SLOT(initialize()));
    connect(this, SIGNAL(drawNewFbo(int, ClusterWaveformSet*, int, bool, int)), renderer, SLOT(renderNext(int, ClusterWaveformSet*, int, bool, int)));
    connect(this, SIGNAL(requestImage()), renderer, SLOT(imageRequested()));
    connect(renderThread, SIGNAL(started()), renderer, SLOT(initialize()));
    connect(renderThread, SIGNAL(finished()), renderer, SLOT(deleteLater()));
    connect(renderer, SIGNAL(imageReady(QImage)), this, SLOT(newImageReceived(QImage)));
  }
}


ScopeWindow::~ScopeWindow()
{
    if(validDriver){
        renderThread->quit();
        renderThread->wait(500);
    }
}

void ScopeWindow::setNTrode(int nTrodeInd, WaveFormPlotImage* img) {
    nTrodeNum = nTrodeInd;
    numChannels = spikeConf->ntrodes[nTrodeNum]->hw_chan.length();

    waveFormGLData.resize(numChannels*POINTSINWAVEFORM);
    for (int i = 0; i < numChannels; i++) {
        for (int j = 0; j < POINTSINWAVEFORM; j++) {
            waveFormGLData[i*POINTSINWAVEFORM+j].x = (GLfloat)(i*POINTSINWAVEFORM+j)*2/waveFormGLData.size() - 1.0;
            waveFormGLData[i*POINTSINWAVEFORM+j].y = 0;
        }
    }

    //maxAmplitude = (spikeConf->ntrodes[nTrodeNum]->maxDisp[0]*65536)/AD_CONVERSION_FACTOR;
    maxAmplitude = spikeConf->ntrodes[nTrodeNum]->maxDisp[0]*(1/spikeConf->ntrodes[nTrodeNum]->spike_scaling_to_uV);

    thresh = spikeConf->ntrodes[nTrodeNum]->thresh[0];
    currentBufferIndex = -1;

    waveImage = img;
    waveImage->setDataShowing(true);

    //Has to initialize here because when constructor is called, other threads are not done initializing
    //When setntrode is called for the first time, everything is finished and here we pass in the necessary variables
    if(validDriver && !initialized){
        initialized = true;
        renderer->clusterColors = clusterColors;
        renderer->bufferSize = bufferSize;
        renderer->m_size = QSize(750,750);
        renderer->moveToThread(renderThread);
        renderThread->start();
        return;
    }

    //Redrawwaveforms will call update()
    reDrawWaveforms();
//    update();
}

void ScopeWindow::setCurrentDisplayPair(int ch1, int ch2) {
    pairChannel1 = ch1;
    pairChannel2 = ch2;
}

void ScopeWindow::plotData() {

    memcpy(waveFormGLData.data(), waveImage->latestWaveform(), sizeof(int2d)*POINTSINWAVEFORM*numChannels);
    emit requestImage();
    if(showOne){
        //Just update otherwise if not showing all waveforms
        update();
    }
}

void ScopeWindow::plotAllData(){
//    if(showAll){
//        //Renderobject gets signal and returns an image that calls update()
//        emit drawNewFbo(nTrodeNum, waveImage->allWaveforms(), maxAmplitude, true, numChannels);
//        emit requestImage();
//    }
    if(showOne){
        //Just update otherwise if not showing all waveforms
        update();
    }
}

void ScopeWindow::toggleAll(bool b){
    if(!validDriver){
        QMessageBox::warning(this, "Warning", "Your graphics driver is not supported. You must install a driver for your graphics card.", QMessageBox::Ok);
        return;
    }
    showAll = b;
    reDrawWaveforms();
//    update();
}

void ScopeWindow::toggleSingle(bool b){
    showOne = b;
    reDrawWaveforms();
//    update();
}


void ScopeWindow::mousePressEvent(QMouseEvent *mouseEvent) {
    qreal xpos = mouseEvent->localPos().x();
    int selection = floor((numChannels * (xpos/width())));

    emit clicked(selection);
}

void ScopeWindow::wheelEvent(QWheelEvent *wheelEvent) {
    double changeAmount;
    double scrollDivisor = 5.0;

    changeAmount = (float)wheelEvent->angleDelta().y()/scrollDivisor;
    if ((changeAmount > 0.0) && (changeAmount < 1.0)) {
        changeAmount = 1.0;
    } else if ((changeAmount < 0.0) && (changeAmount > -1.0)) {
        changeAmount = -1.0;
    }

    emit zoom(-changeAmount);


}

void ScopeWindow::reDrawWaveforms(){
    //Called to get renderworker to draw a new image
    //The signal renderworker emits triggers newImageReceived()
    if(!validDriver){
        return;
    }
    if(!initialized){
        return;
    }
    if(!renderer){
        return;
    }
    if(!renderer->m_renderFbo){
        return;
    }

    ClusterWaveformSet *allwaves = waveImage->allWaveforms();
    emit drawNewFbo(nTrodeNum, allwaves, maxAmplitude, true, numChannels);
    emit requestImage();
}


void ScopeWindow::setHighlight(int channel) {
    highlight = true;
    highlightChannel = channel;
    update();
}

void ScopeWindow::setNoHighlight() {
    highlight = false;
    update();
}

void ScopeWindow::setMaxDisplay(int maxDisplayVal){
  //maxAmplitude = (maxDisplayVal*65536)/AD_CONVERSION_FACTOR;
  maxAmplitude = maxDisplayVal*(1/spikeConf->ntrodes[nTrodeNum]->spike_scaling_to_uV);
  update();
}

void ScopeWindow::setThresh(int threshVal){
  //thresh = (threshVal*65536)/AD_CONVERSION_FACTOR;
  thresh = threshVal*(1/spikeConf->ntrodes[nTrodeNum]->spike_scaling_to_uV);
  update();
}

//void ScopeWindow::paintGL(){
void ScopeWindow::paintEvent(QPaintEvent *){

    //Set window properties
    QColor bg = streamConf->backgroundColor;
    QImage chartLines = QImage(width(), height(),QImage::Format_ARGB32_Premultiplied);
    chartLines.fill(QColor(0,0,0,0));
    int xPointScale = 1000/(numChannels*(beforeTriggerLength + afterTriggerLength));
    int singlePanelWidth = xPointScale*(beforeTriggerLength + afterTriggerLength);
    int windowWidth = numChannels*singlePanelWidth;
    QRect box = QRect(0, -maxAmplitude, windowWidth, 2*maxAmplitude);

    QPainter painter;
    QPen pen;
    pen.setColor(QColor(210,210,210));
    //This makes sure horizontal lines do not go to zero when scaling with height bc of integers rounding to 0 sometimes for the width
    pen.setWidth((qreal)height()/this->geometry().height());
    painter.begin(&chartLines);
    painter.setViewport(0,0,width(), height());
    painter.setPen(pen);
    //Draw Chart lines, saved on image to be displayed at the end, on top of everything else
    if (highlight) {
        painter.fillRect(width()/numChannels*highlightChannel,0, width()/numChannels,height(),QBrush(QColor(150,100,100, 150)));
    }
    for(int i = 0; i < numChannels; i++)
        painter.drawRect(i*width()/numChannels,0, width()/numChannels, height());
    painter.drawLine(0,height()/2, width(), height()/2);
    int threshline = height() - (qreal)thresh/(maxAmplitude)*(qreal)height()/2-height()/2;
    painter.drawLine(0,threshline, width(), threshline);
    painter.end();

    //Begin drawing on window
    painter.begin(this);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    pen.setColor(spikeConf->ntrodes[nTrodeNum]->color);

    pen.setJoinStyle(Qt::MiterJoin);
    pen.setCosmetic(true);
    //pen.setWidth(1);

    painter.setPen(pen);
    painter.setViewport(0,0,width(), height());
    painter.setWindow(box);
    painter.fillRect(painter.window(), bg);

    //Draw all waveforms
    if(validDriver && showAll && !currImage.isNull()){
        painter.drawImage(painter.window(), currImage);
    }

    //Draw single waveform
    if(showOne){
        for(int i = 0; i < numChannels; i++){
            QPolygonF line;
            for(int j = 0; j < POINTSINWAVEFORM; j++){
                line.append(QPoint(i*singlePanelWidth + j*xPointScale, -waveFormGLData[i*POINTSINWAVEFORM+j].y));
            }
            painter.drawPolyline(line);
        }
    }

    //Draw chart lines over waveforms
    painter.drawImage(box, chartLines);

    painter.end();
}

void ScopeWindow::newImageReceived(const QImage & img){
    //Receives an image from renderworker when it finishes painting
//    makeCurrent();
    currImage = img;
    update();
}

RenderWorker::RenderWorker()
{
//    surface = new QOffscreenSurface;
//    m_context = new QOpenGLContext;
//    m_renderFbo = new QOpenGLFramebufferObject(QSize(1000,1000));
//    m_device = new QOpenGLPaintDevice;
    initialized = false;
    surface = new QOffscreenSurface;
//    surface->setFormat(m_context->format());
    surface->create();
}


void RenderWorker::initialize(){
    //Select max channels out of all ntrodes, potentially allow for variable ntrodes
    numChannels = 0;
    for(int i = 0; i < spikeConf->ntrodes.length(); i++)
        if(spikeConf->ntrodes[i]->hw_chan.length() > numChannels)
            numChannels = spikeConf->ntrodes[i]->hw_chan.length();

    m_context = new QOpenGLContext;
    m_context->create();
    m_context->makeCurrent(surface);
    m_context->functions()->initializeOpenGLFunctions();
    m_renderFbo = new QOpenGLFramebufferObject(m_size);
    m_device = new QOpenGLPaintDevice(m_size);

    //Shaders
    m_program = new QOpenGLShaderProgram;
    m_program->addShaderFromSourceCode(QOpenGLShader::Vertex,
        "attribute vec2 vertex;\n"
        "uniform mat4 projMatrix;\n"
        "uniform mat4 mvMatrix;\n"
        "void main(void)\n"
        "{\n"
        "   gl_Position = projMatrix * mvMatrix * vec4(vertex, 0.0, 1.0);\n"
        "}");
    m_program->addShaderFromSourceCode(QOpenGLShader::Fragment,
        "uniform mediump vec4 color;\n"
        "void main(void)\n"
        "{\n"
        "   gl_FragColor = color;\n"
        "}");
    m_program->bindAttributeLocation("vertex", 0);
    m_program->link();
    m_program->bind();
    m_projMatrixLoc = m_program->uniformLocation("projMatrix");
    m_mvMatrixLoc = m_program->uniformLocation("mvMatrix");
    m_colorLoc = m_program->uniformLocation("color");
    m_program->release();

    //Initialize buffers
    QOpenGLFunctions *f = m_context->functions();

    f->glBindBuffer(GL_ARRAY_BUFFER, 0);

    //AllWaveforms buffer
    f->glGenBuffers(1, &m_allwaveformsBuf);
    f->glBindBuffer(GL_ARRAY_BUFFER, m_allwaveformsBuf);
    f->glBufferData(GL_ARRAY_BUFFER, sizeof(int2d)*POINTSINWAVEFORM*numChannels*bufferSize, 0, GL_DYNAMIC_DRAW);

    f->glBindBuffer(GL_ARRAY_BUFFER, 0);

    m_context->doneCurrent();
    initialized = true;

}
void RenderWorker::renderNext(int ntrode, ClusterWaveformSet *waves, int maxAmp, bool redraw, int numCh){
    if(!initialized)
        return;

    m_context->makeCurrent(surface);

    QPainter painter;
    painter.begin(m_device);

    painter.beginNativePainting();
    painter.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);

    m_program->bind();

    m_proj.setToIdentity();
    //m_proj.scale(1.0, -(qreal)1/(qreal)maxAmp);
    m_proj.scale((qreal)2.0/(qreal)(POINTSINWAVEFORM*numCh), (qreal)1/(qreal)maxAmp);
    m_proj.translate(-1.0*POINTSINWAVEFORM*numCh/2.0, (qreal)0.0);

    m_program->setUniformValue(m_projMatrixLoc, m_proj);
    m_modelView.setToIdentity();
    m_program->setUniformValue(m_mvMatrixLoc, m_modelView);
    m_program->setUniformValue(m_colorLoc, spikeConf->ntrodes[ntrode]->color);

    QOpenGLFunctions *f = m_context->functions();
//    f->initializeOpenGLFunctions();

    //Draw Background
    QColor b =streamConf->backgroundColor;
    if(!m_renderFbo)
        return;
//    if(!m_renderFbo->isValid())
//        return;
    m_renderFbo->bind();
    if(redraw){
        f->glClearColor(0.0,0.0,0.0,0.0);
        f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        f->glClearColor(b.redF(), b.greenF(), b.blueF(), b.alphaF());
        f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
    f->glLineWidth(1);
    //Draw passed waveforms
    f->glBindBuffer(GL_ARRAY_BUFFER, m_allwaveformsBuf);
    f->glBufferData(GL_ARRAY_BUFFER, waves->points.size()*sizeof(int2d),
                    waves->points.constData(), GL_DYNAMIC_DRAW);
    f->glEnableVertexAttribArray(0);
    f->glVertexAttribPointer(0, 2, GL_SHORT, GL_FALSE, 0,0);

    for(int i = 0; i < waves->points.size()/POINTSINWAVEFORM/numCh; i++){
        int ii = waves->indices.at(i);
        if(waves->hidden.contains(waves->clusters.at(ii)))
            continue;
        m_program->setUniformValue(m_colorLoc, clusterColors[waves->clusters.at(ii)]);
        for(int j = 0; j < numCh; j++){
            f->glDrawArrays(GL_LINE_STRIP, (ii *numCh+j)*POINTSINWAVEFORM, POINTSINWAVEFORM);
        }
    }

    currimg = m_renderFbo->toImage();
    //Cleanup
    f->glDisableVertexAttribArray(0);
    f->glBindBuffer(GL_ARRAY_BUFFER, 0);
    m_renderFbo->bindDefault();
    m_program->release();

//    emit imageReady(m_renderFbo->toImage());
    painter.endNativePainting();
    painter.end();
    delete waves;
}

void RenderWorker::imageRequested(){
    emit imageReady(currimg);
}

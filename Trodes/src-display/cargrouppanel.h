#ifndef CARGROUPPANEL_H
#define CARGROUPPANEL_H

#include <QtWidgets>

#define MAXCARGROUPS 32
#define MAXCHANPERGROUP 16

class SpikeConfiguration;

struct CARChan{
    int ntrodeid;
    int chan;
    int hw_chan;
};

struct CARGroup{
    int useCount;
    QString description;
    QVector<CARChan> chans;
    CARGroup():useCount(0){}
};

class CARGroupPanel : public QWidget {
    Q_OBJECT
public:
    CARGroupPanel(QWidget *parent = nullptr);
    ~CARGroupPanel();
    void loadGroups(SpikeConfiguration* spikeConf);
    QVector<CARGroup> getGroups();
//    int getGroupSelected() const;
//    void resetButtons();
//    void selectGroup(int group);
protected:
//    void showEvent(QShowEvent *event);
//    void closeEvent(QCloseEvent *event);
private:
//    QButtonGroup btngroup;
    QListView *listview;
    QStringListModel *model;
    QStringList list;

    QVector<CARGroup> cargroups;
    int currentGroup;
    QLabel *grouplabel;
    QLineEdit *groupdescription;
    QTableWidget *grouptable;
    QPushButton *editgroupbutton;
    QPushButton *applychangesbtn;
    QByteArray geo;
    QPushButton* addbutton;
    QPushButton* rmvbutton;

//    bool isStreaming;


signals:
//    void groupchanged(int id);
//    void updateDisplay(int group);
//    void updateDescription(int group, const QString &text);
//    void cargroupedited(int groupid, QList<int>ntrodes, QList<int>chans, QString description);
//    void cargroupadded();
//    void cargroupremoved(int groupid);
public slots:
    void hwchansReceived(QList<int> hwchans);
//    void streamingStarted();
//    void streamingStopped();
private slots:
    void addbuttonclicked();
    void rmvbuttonclicked();
    void groupselected(int id);
    void editgrouptable();
    void applychanges();
//    void groupcellchanged(int row, int col);
};

#endif // CARGROUPPANEL_H

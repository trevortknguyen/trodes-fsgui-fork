import trodes.FSData.fsDataMain as fsDataMain
import logging
import logging.config
import cProfile
import sys
import getopt
from mpi4py import MPI

def main(argv):
    # setup logging
    logging.config.dictConfig({
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'simple': {
                'format': '%(asctime)s.%(msecs)03d [%(levelname)s] %(name)s: %(message)s',
                'datefmt': '%H:%M:%S',
            },
        },
        'handlers': {
            'console': {
                'level': 'INFO',
                'class': 'logging.StreamHandler',
                'formatter': 'simple',
            },
            'debug_file_handler': {
                'class': 'logging.handlers.RotatingFileHandler',
                'level': 'DEBUG',
                'formatter': 'simple',
                'filename': 'debug.log',
                'maxBytes': 10485760,
                'backupCount': 20,
                'encoding': 'utf8',
            }
        },
        'loggers': {
            '': {
                'handlers': ['console', 'debug_file_handler'],
                'level': 'NOTSET',
                'propagate': True,
            }
        }
    })

    # parse the command line arguments
    port = 0
    hostName = ''

    try:
        opts, args = getopt.getopt(argv, "", ["port=", "hostName="])
    except getopt.GetoptError:
        logging.error('Usage: fsdata.py --port <fsGUIServerPort> --hostName <fsGUIServerHost>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '--port':
            port = int(arg)
        elif opt == "--hostName":
            hostName = arg

    if (port == 0) or (hostName == ''):
        logging.error('Usage: fsdatapy.py --port <fsGUIServerPort> --hostName <fsGUIServerHost>')
        sys.exit(2)

    # setup MPI
    comm = MPI.COMM_WORLD           # type: MPI.Comm
    size = comm.Get_size()
    rank = comm.Get_rank()
    name = MPI.Get_processor_name()

    if size == 1:
        # MPI is not running or is running on a single node.  Single processor mode
        pass

    fsdata_super = fsDataMain.FSDataSupervisor(hostName, port)
    fsdata_super.main_loop()


cProfile.runctx('main(sys.argv[1:])', globals(), locals(), 'fsdatapy_profile')

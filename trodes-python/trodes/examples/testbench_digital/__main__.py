'''
Purpose of this module is to give a real-time readout of the analog
channels from trodes network.
'''

import sys
import PyQt6.QtCore as QtCore
import PyQt6.QtWidgets as QtWidgets

import json

import threading

from trodes.examples.general_networked_application import networked_main
import trodesnetwork.trodes as trodes
import trodes.examples.testbench_digital.config as config

class DigitalTestbench(QtWidgets.QDialog):
    def __init__(self, server_address, parent=None):
        super().__init__(parent=parent)
        self.text = ""
        self.digital_subscriber = trodes.DigitalSubscriber(
            server_address=server_address,
            channel_map=config.channel_map,
            channel_name='ECU_Din8',
            callback=self.print_data)

        self.setWindowTitle("Digital Testbench")

        layout = QtWidgets.QVBoxLayout()
        self.setLayout(layout)

        
        self.label = QtWidgets.QLabel()
        layout.addWidget(self.label)

        # we have to update on a timer because updating QtWidgets too fast
        # won't work
        self.label_timer = QtCore.QTimer(parent=self)
        self.label_timer.timeout.connect(self.update_label)
        self.label_timer.start(20)

    def print_data(self, data):
        self.text = str(data)

    def update_label(self):
        self.label.setText(self.text)

if __name__ == '__main__':
    print("Digital Testbench")
    networked_main(DigitalTestbench)

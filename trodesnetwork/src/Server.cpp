#include <TrodesNetwork/Generated/Handshake.h>
#include <TrodesNetwork/Generated/ServerRequestSimple.h>
#include <TrodesNetwork/Resources/RawServiceProvider.h>
#include <TrodesNetwork/Resources/RawSourcePublisher.h>
#include <TrodesNetwork/Server.h>
#include <TrodesNetwork/ServerState.h>
#include <TrodesNetwork/Util.h>

#include <atomic>
#include <chrono>
#include <functional>
#include <iostream>
#include <memory>
#include <thread>

namespace trodes {
namespace network {

    Server::Server(std::string address, int port)
        : address(address)
        , port(port)
        , running(new std::atomic<bool>(true))
    {
        // unique_ptr to service provider
        // get endpoint before moving into service thread
        std::unique_ptr<RawServiceProvider<ServerRequestSimple, std::string>> service(
                    new RawServiceProvider<ServerRequestSimple, std::string>(
                        util::get_wildcard_string(address)));
        std::string service_endpoint = service->last_endpoint();

        std::shared_ptr<RawSourcePublisher<std::string>> logger(
                    new RawSourcePublisher<std::string>(
                        util::get_wildcard_string(address)));
        std::string logger_endpoint = logger->last_endpoint();

        std::shared_ptr<ServerState> state(new ServerState());

        // thread owns the service
        // thread has shared access to state and logger
        service_thread = std::thread([ service = std::move(service), state, logger ]() {
            while (true) {
                service->handle([ state, logger ](ServerRequestSimple input) -> std::string {
                    if (input.tag == "add") {
                        state->add_endpoint(input.name, input.endpoint);
                        logger->publish("Server adding endpoint for " + input.name + " (" + input.endpoint + ")");
                        return "success";
                    }
                    if (input.tag == "get") {
                        auto endpoint = state->get_endpoint(input.name);
                        logger->publish("Searching for an endpoint: " + input.name);
                        return endpoint.value_or("");
                    }

                    logger->publish("Error.");
                    return "error";
                });
            }
        });
        service_thread.detach();

        handshake_thread = std::thread([address, port, service_endpoint, logger_endpoint, logger, running = running]() {
            const std::string endpoint = util::get_connection_string(address, port);
            RawSourcePublisher<Handshake> pub{endpoint};

            Handshake greeting{service_endpoint, logger_endpoint, "Welcome to Trodes server!"};

            while (running->load()) {
                pub.publish(greeting);
                logger->publish("published on greeting");
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
            }
        });
    }

    Server::~Server() {
        running->store(false);
        handshake_thread.join();
    }
}
}

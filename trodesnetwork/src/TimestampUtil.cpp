#include <Trodes/TimestampUtil.h>
#include <chrono>

namespace trodes {
namespace util {

    uint64_t system_timestamp_ns(){
        return (uint64_t) std::chrono::duration_cast<std::chrono::nanoseconds>(
                    std::chrono::system_clock::now().time_since_epoch()).count();
    }

}
}

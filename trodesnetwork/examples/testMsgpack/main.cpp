
#include <msgpack.hpp>
#include <TrodesNetwork/Generated/HardwareRequest.h>

#include <chrono>
#include <iostream>
#include <sstream>
#include <thread>
#include <vector>

#include <variant>
#include <type_traits>

template<class> inline constexpr bool always_false_v = false;

int main(int argc, char** argv) {
    if (argc) {}
    if (argv) {}

    trodes::network::HardwareRequest req = trodes::network::HRSetGS();


    std::visit([](auto&& arg){
            using T = std::decay_t<decltype(arg)>;

            if constexpr (std::is_same_v<T, trodes::network::HRStartStopCommand>)
                std::cout << "sscommand." << std::endl;
            else if constexpr (std::is_same_v<T, trodes::network::HRSet>)
                std::cout << "set." << std::endl;
            else if constexpr (std::is_same_v<T, trodes::network::HRClear>)
                std::cout << "clear." << std::endl;
            else if constexpr (std::is_same_v<T, trodes::network::HRSettle>)
                std::cout << "settle." << std::endl;
            else if constexpr (std::is_same_v<T, trodes::network::HRSetGS>)
                std::cout << "set gs." << std::endl;
            else if constexpr (std::is_same_v<T, trodes::network::HRSetGC>)
                std::cout << "set gc." << std::endl;
            else if constexpr (std::is_same_v<T, trodes::network::HRSCTrig>)
                std::cout << "sc trig." << std::endl;
            else 
                static_assert(always_false_v<T>, "non-exhaustive visitor!");

        },
        req
    );

    // serialize
    trodes::network::HardwareRequest data = req;
    std::stringstream buffer;
    msgpack::pack(buffer, data);
    std::string serialized = buffer.str();

    std::cout << serialized << std::endl;

    // deserialize with serialized
    msgpack::object_handle oh = msgpack::unpack(serialized.data(), serialized.size());
    msgpack::object deserialized = oh.get();

    trodes::network::HardwareRequest request = deserialized.as<trodes::network::HardwareRequest>();

    std::visit([](auto&& arg){
            using T = std::decay_t<decltype(arg)>;

            if constexpr (std::is_same_v<T, trodes::network::HRStartStopCommand>)
                std::cout << "sscommand." << std::endl;
            else if constexpr (std::is_same_v<T, trodes::network::HRSet>)
                std::cout << "set." << std::endl;
            else if constexpr (std::is_same_v<T, trodes::network::HRClear>)
                std::cout << "clear." << std::endl;
            else if constexpr (std::is_same_v<T, trodes::network::HRSettle>)
                std::cout << "settle." << std::endl;
            else if constexpr (std::is_same_v<T, trodes::network::HRSetGS>)
                std::cout << "set gs." << std::endl;
            else if constexpr (std::is_same_v<T, trodes::network::HRSetGC>)
                std::cout << "set gc." << std::endl;
            else if constexpr (std::is_same_v<T, trodes::network::HRSCTrig>)
                std::cout << "sc trig." << std::endl;
            else 
                static_assert(always_false_v<T>, "non-exhaustive visitor!");

        },
        request
    );

    return 0;
}

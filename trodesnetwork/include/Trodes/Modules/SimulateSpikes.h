#pragma once

#include <Trodes/Modules/SpikeGenerator.h>
#include <Trodes/Modules/TimestampedSample.h>


#include <TrodesNetwork/Generated/SpikePacket.h>
#include <TrodesNetwork/Resources/SourcePublisher.h>


#include <Trodes/TimestampUtil.h>

#include <string>

namespace trodes {

template<typename T>
class SimulateSpikes {
public:
    SimulateSpikes(std::string name, T sampleSource)
        : name(name),
          sampleSource(sampleSource),
          spikes(name)
    {
    }

    ~SimulateSpikes() {

    }

    void run() {
        uint64_t start_time_ns = util::system_timestamp_ns();
        sampleSource.start();
        while (true) {
            TimestampedSample sample = sampleSource.getSample();
            uint32_t local_sample_time = (uint32_t) (sample.systemTimestamp - start_time_ns);
            int16_t sample_amplitude = sample.sample;

            network::SpikePacket packet{0, 0, local_sample_time, sample_amplitude, sample_amplitude};
            spikes.publish(packet);
        }
    }

private:
    std::string name;
    T sampleSource;
    network::SourcePublisher<network::SpikePacket> spikes;
};

}

#pragma once

namespace trodes {

struct TimestampedSample {
    uint64_t systemTimestamp;
    int16_t sample;
};

}
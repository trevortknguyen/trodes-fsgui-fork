#pragma once

#include <TrodesNetwork/Util.h>

#include <thread>
#include <string>
#include <memory>
#include <atomic>

namespace trodes {
namespace network {

class Server {
public:
    Server(std::string address, int port);
    ~Server();

    const std::string address;
    const int port;
private:
    std::shared_ptr<std::atomic<bool>> running;
    std::thread service_thread;
    std::thread handshake_thread;
};

}
}

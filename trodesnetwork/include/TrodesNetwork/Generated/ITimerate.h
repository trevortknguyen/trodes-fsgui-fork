#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor ITimerate
struct ITimerate {
    // field impl timerate
    int32_t timerate;
    MSGPACK_DEFINE_MAP(timerate);
};


}  // network
}  // trodes

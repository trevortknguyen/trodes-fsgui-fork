#pragma once

#include <msgpack.hpp>
#include <string>
#include <zmq.hpp>

namespace trodes {
namespace network {
namespace util {

std::string get_connection_string(std::string address, int port);
std::string get_wildcard_string(std::string address);

int64_t get_timestamp();

std::string sock_endpoint(zmq::socket_t* socket);

std::string get_endpoint_retry_forever(std::string address, int port, std::string name);

template<typename T>
std::string pack(T data) {
    std::stringstream buffer;
    msgpack::pack(buffer, data);
    return buffer.str();
}

template<typename T>
T unpack(std::string data) {
    msgpack::object_handle oh = msgpack::unpack(data.data(), data.size());
    msgpack::object deserialized = oh.get();
    return deserialized.as<T>();
}

}
}
}

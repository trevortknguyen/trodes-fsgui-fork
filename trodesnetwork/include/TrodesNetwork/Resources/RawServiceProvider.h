#pragma once

#include <TrodesNetwork/Util.h>

#include <functional>
#include <msgpack.hpp>
#include <iostream>
#include <sstream>
#include <string>
#include <zmq.hpp>

namespace trodes {
namespace network {

template<typename A, typename B>
class RawServiceProvider {
public:
    RawServiceProvider(std::string endpoint)
    : ctx_(1),
      socket_(ctx_, zmq::socket_type::rep)
    {
        socket_.bind(endpoint.c_str());
    }

    ~RawServiceProvider() {
    }

    void handle(std::function<B(A)> f) {
        zmq::message_t req_msg;
        auto rv = socket_.recv(req_msg);
        if (!rv.has_value()) {
            std::cerr << "Failed to receive in service provider." << std::endl;
        }
        A req = util::unpack<A>(req_msg.to_string());

        B response = f(req);

        zmq::message_t res_msg(util::pack<B>(response));
        socket_.send(res_msg, zmq::send_flags::dontwait);
    }

    std::string last_endpoint() {
        return util::sock_endpoint(&socket_);
    }

private:
    zmq::context_t ctx_;
    zmq::socket_t socket_;
};


}
}
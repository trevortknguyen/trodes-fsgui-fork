#pragma once

#include <TrodesNetwork/Util.h>

#include <msgpack.hpp>
#include <iostream>
#include <sstream>
#include <string>
#include <zmq.hpp>

namespace trodes {
namespace network {

template<typename A, typename B>
class RawServiceConsumer {
public:
    RawServiceConsumer(std::string endpoint)
    : ctx_(1),
      socket_(ctx_, zmq::socket_type::req)
    {
        socket_.connect(endpoint.c_str());
    }

    ~RawServiceConsumer() {
    }

    B request(A request) {
        // send request
        zmq::message_t req_msg(util::pack<A>(request));
        socket_.send(req_msg, zmq::send_flags::none);

        // receive response
        zmq::message_t res_msg;
        auto rv = socket_.recv(res_msg);
        if (!rv.has_value()) {
            // failed to receive
        }

        return util::unpack<B>(res_msg.to_string());
    }

private:
    zmq::context_t ctx_;
    zmq::socket_t socket_;
};


}
}
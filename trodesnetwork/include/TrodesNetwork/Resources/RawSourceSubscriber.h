#pragma once

#include <TrodesNetwork/Util.h>

#include <msgpack.hpp>
#include <iostream>
#include <sstream>
#include <string>
#include <zmq.hpp>

namespace trodes {
namespace network {

template<typename T>
class RawSourceSubscriber {
public:
    RawSourceSubscriber(std::string endpoint)
    : ctx_(1),
      socket_(ctx_, zmq::socket_type::sub)
    {
        socket_.connect(endpoint.c_str());
        std::string filter = "";
        socket_.setsockopt(ZMQ_SUBSCRIBE, filter.c_str(), filter.length());
    }

    ~RawSourceSubscriber() {
    }

    T receive() {
        zmq::message_t message;
        auto rv = socket_.recv(message);
        if (!rv.has_value()) {
            // failed to receive
        }
        return util::unpack<T>(message.to_string());
    }

private:
    zmq::context_t ctx_;
    zmq::socket_t socket_;
};


}
}
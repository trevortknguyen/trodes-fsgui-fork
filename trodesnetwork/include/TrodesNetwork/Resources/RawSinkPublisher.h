#pragma once

#include <msgpack.hpp>
#include <iostream>
#include <sstream>
#include <string>
#include <zmq.hpp>

namespace trodes {
namespace network {

template<typename T>
class RawSinkPublisher {
public:
    RawSinkPublisher(std::string endpoint)
    : ctx_(1),
      socket_(ctx_, zmq::socket_type::push)
    {
        socket_.connect(endpoint.c_str());
    }

    ~RawSinkPublisher() {
    }

    void publish(T data) {
        zmq::message_t message(util::pack<T>(data));
        socket_.send(message, zmq::send_flags::dontwait);
    }

private:
    zmq::context_t ctx_;
    zmq::socket_t socket_;
};


}
}
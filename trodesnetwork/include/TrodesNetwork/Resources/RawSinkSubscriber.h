#pragma once

#include <TrodesNetwork/Util.h>

#include <msgpack.hpp>
#include <mutex>
#include <sstream>
#include <string>
#include <zmq.hpp>

namespace trodes {
namespace network {

template<typename T>
class RawSinkSubscriber {
public:
    RawSinkSubscriber(std::string endpoint)
    : ctx_(1),
      socket_(ctx_, zmq::socket_type::pull)
    {
        socket_.bind(endpoint.c_str());
    }

    ~RawSinkSubscriber() {
    }

    T receive() {
        // just in case this is a shared_ptr
        const std::lock_guard<std::mutex> lock(mutex_);

        zmq::message_t message;
        auto rv = socket_.recv(message);
        if (!rv.has_value()) {
            // failed to receive
        }
        return util::unpack<T>(message.to_string());
    }

    std::string last_endpoint() {
        return util::sock_endpoint(&socket_);
    }

private:
    mutable std::mutex mutex_;
    zmq::context_t ctx_;
    zmq::socket_t socket_;
};


}
}
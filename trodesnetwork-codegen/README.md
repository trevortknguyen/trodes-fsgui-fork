# TrodesNetwork Code Generation

This repository generates serializable network types in C++ using Haskell. Each
network type is a Haskell datatype and can be used to generated C++ header
files that specify C++ types. These types can be serialized using [MsgPack](https://msgpack.org/) and sent
over the [ZeroMQ](https://zeromq.org/)-based TrodesNetwork infrastructure.

## Using to generate C++ code

This project requires Haskell Stack. It is only tested on Linux.

```
stack build
stack exec trodesnetwork-codegen
```

The `.h` files are in the `generated` directory. Generated files should not be edited.

## Example generated file

Here is the Haskell file.

```
module Trodes.Network.Messages.AcquisitionCommand where

import Data.Text (Text)
import Data.Word
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data AcquisitionCommand = AcquisitionCommand
    { command :: Text
    , timestamp :: Word32
    } deriving (Generic, Cpp)
```

Here is the generated C++ class.

```
#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor AcquisitionCommand
struct AcquisitionCommand {
    // field impl command
    std::string command;
    // field impl timestamp
    uint32_t timestamp;
    MSGPACK_DEFINE_MAP(command, timestamp);
};


}  // network
}  // trodes
```

## Creating new network types

In order to create a new network type, create a new Haskell type. Look under `Trodes.Network.Messages` for examples.

For more complex types that involve sum types, see `Trodes.Network.Messages.HardwareRequest` for an example. It
has other types specified in `Trodes.Network.Types`.

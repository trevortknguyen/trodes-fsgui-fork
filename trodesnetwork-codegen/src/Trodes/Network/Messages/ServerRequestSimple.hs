module Trodes.Network.Messages.ServerRequestSimple where

import Data.Text (Text)
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data ServerRequestSimple = ServerRequestSimple
    { tag :: Text
    , name :: Text
    , endpoint :: Text
    } deriving (Generic, Cpp)

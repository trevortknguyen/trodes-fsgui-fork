module Trodes.Network.Messages.HardwareRequest where

import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

import qualified Trodes.Network.Types.HWStartStop as HW
import qualified Trodes.Network.Types.HWStimulationCommand as HW
import qualified Trodes.Network.Types.HWClear as HW
import qualified Trodes.Network.Types.HWGlobalStimulationSettings as HW
import qualified Trodes.Network.Types.HWGlobalStimulationCommand as HW
import qualified Trodes.Network.Types.HWTrigger as HW

data HardwareRequest
    = HRStartStopCommand HW.HWStartStop -- start/stop, slot/group, number
    | HRSet HW.HWStimulationCommand-- StimulationCommand
    | HRClear HW.HWClear-- number
    | HRSettle
    | HRSetGS HW.HWGlobalStimulationSettings -- GlobalStimulationSettings
    | HRSetGC HW.HWGlobalStimulationCommand -- GlobalStimulationCommand
    | HRSCTrig HW.HWTrigger -- fn number
    deriving (Generic, Cpp)

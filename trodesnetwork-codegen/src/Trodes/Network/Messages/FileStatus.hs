module Trodes.Network.Messages.FileStatus where

import Data.Text (Text)
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data FileStatus= FileStatus
    { message :: Text
    , filename :: Text
    } deriving (Generic, Cpp)

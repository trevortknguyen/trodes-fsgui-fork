module Trodes.Network.Messages.TrodesAnalogData where

import Data.Int
import Data.Word
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data TrodesAnalogData = TrodesAnalogData
    { localTimestamp :: Word32
    , analogData :: [[Word8]]
    , systemTimestamp :: Int64
    } deriving (Generic, Cpp)

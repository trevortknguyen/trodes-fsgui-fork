module Trodes.Network.Messages.FileCommand where

import Data.Text (Text)
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data FileCommand = FileCommand
    { command :: Text
    , filename :: Text
    } deriving (Generic, Cpp)

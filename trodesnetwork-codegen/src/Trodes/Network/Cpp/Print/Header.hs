{-# LANGUAGE OverloadedStrings #-}
module Trodes.Network.Cpp.Print.Header where

import Data.Text (Text)
import qualified Data.Text as T
import Trodes.Network.Cpp.Ast
import Trodes.Network.Cpp.Print.Types

prettyShowHeader :: CppDefinition -> Text
prettyShowHeader def = T.unlines
    [ "// This code is automatically generated. It is unwise to edit it manually."
    , "#pragma once"
    , ""
    , "#include <string>"
    , ""
    , "namespace trodes {"
    , "namespace network {"
    , ""
    , "class " <> classname <> " {"
    , "public:"
    , "// something of an implementation"
    , T.unlines $ map printField variables
    , "private:"
    , ""
    , "};"
    , ""
    , "}  // namespace network"
    , "}  // namespace trodes"
    ]
    where
        classname = cppName def

        variables = foldl (\b a -> (cppConstructorVariables a) <> b) [] (cppConstructors def)

        printField :: CppRecordField -> Text
        printField field = (printFieldType field) <> " " <> (printFieldName field) <> ";"

        printFieldType :: CppRecordField -> Text
        printFieldType field = docToText $ cppTypeRefDoc $ cppRecordFieldType field

        printFieldName :: CppRecordField -> Text
        printFieldName field = cppRecordFieldName field

        docToText = T.pack . show
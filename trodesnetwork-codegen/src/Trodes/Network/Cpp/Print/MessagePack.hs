module Trodes.Network.Cpp.Print.MessagePack where

import Data.Text (Text)
import qualified Data.Text as T
import Trodes.Network.Cpp.Ast
import Trodes.Network.Cpp.Print.Types
import Data.Text.Prettyprint.Doc as P

import Data.List (nub)

prettyShowType :: CppDefinition -> Text
prettyShowType def = T.intercalate "\n" $
    [ "#pragma once"
    , ""
    , "#include <string>"
    , "#include <msgpack.hpp>"
    , ""
    , imports
    , ""
    , "namespace trodes {"
    , "namespace network {"
    , ""
    , structDef
    , ""
    , "}  // network"
    , "}  // trodes"
    ] ++
    additional

    where
        structDef = if length (cppConstructors def) == 1
                        then prettyShowConstructor $ head $ cppConstructors def
                        else prettyShowSumType def
        imports = prettyShowCustomImports def
        additional = if length (cppConstructors def) == 1
                        then []
                        else [prettyShowMsgpack def]

prettyShowMsgpack :: CppDefinition -> Text
prettyShowMsgpack def = customMsgpack
    where
        customMsgpack = T.intercalate "\n" $
            [ "namespace msgpack {"
            , "MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS) {"
            , "namespace adaptor {"
            ] ++
            prettyShowMsgpackConvert def  ++
            prettyShowMsgpackPack def ++
            [ ""
            , "} // namespace adaptor"
            , "} // MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS)"
            , "} // namespace msgpack"
            ]
prettyShowMsgpackConvert :: CppDefinition -> [Text]
prettyShowMsgpackConvert def =
    [ "// Place class template specialization here"
    , "template<>"
    , "struct convert<" <> typename <> "> {"
    , "    msgpack::object const& operator()(msgpack::object const& o, " <> typename <> "& v) const {"
    , "        if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();"
    , "        std::string tag_id = o.via.array.ptr[0].as<std::string>();"
    , "        if (tag_id != \"tag\") throw msgpack::type_error();"
    , "        std::string tag = o.via.array.ptr[1].as<std::string>();"
    , ""
    , check_tag
    , ""
    , "        return o;"
    , "    }"
    , "};"
    ]
  where
    typename = "trodes::network::" <> (cppName def)
    check_tag = T.intercalate "\n" $
        [ "// here is some code"
        ] ++
        ifBlock actual_code (cppConstructors def) ++
        [
        ]
    actual_code :: CppConstructor -> [Text]
    actual_code c =
        [ "// handle this case"
        ] ++
        -- assumes only one thing
        concatMap (unpackStruct c) (zip [2..] (cppConstructorVariables c)) ++
        [ "v = " <> "trodes::network::" <> (cppConstructorName c) <> "{" <> params c <> "};"
        ]

    unpackStruct :: CppConstructor -> (Integer, CppRecordField) -> [Text]
    unpackStruct _ (i, r) =
        [ "" <> displayType (cppRecordFieldType r) <> " " <> (cppRecordFieldName r) <> " = o.via.array.ptr[" <> T.pack (show i) <> "].as<" <> displayType (cppRecordFieldType r) <> ">();"
        ]

    displayType :: TypeRef -> Text
    displayType t = case t of
        RefPrim _ -> undefined
        RefCustom x -> "trodes::network::" <> untypeName x

    params c = T.intercalate ", " $ map (cppRecordFieldName) $ cppConstructorVariables c


ifBlock :: (CppConstructor -> [Text]) -> [CppConstructor] -> [Text]
ifBlock f cs = case cs of
    [] ->
        [ "// no constructors, this shouldn't happen"
        , "throw msgpack::type_error();"
        ]
    x:xs ->
        [ "if (tag == \"" <> (cppConstructorName x) <> "\") {"
        ] ++
        (f x) ++
        (concatMap elseif xs) ++
        [ "} else {"
        , "    throw msgpack::type_error();"
        , "}"
        ]
  where
    elseif :: CppConstructor -> [Text]
    elseif c =
        [ "} else if (tag == \"" <> (cppConstructorName c) <> "\") {"
        ] ++
        (f c)

prettyShowMsgpackPack :: CppDefinition -> [Text]
prettyShowMsgpackPack def =
    [ "template<>"
    , "struct pack<" <> typename <> "> {"
    , "    template <typename Stream>"
    , "    packer<Stream>& operator()(msgpack::packer<Stream>& o, " <> typename <> " const& v) const {"
    , "        // packing member variables as an array."
    ] ++
    elems_assign ++
    [ "        o.pack_array(elems);"
    , "        o.pack(\"tag\");"
    ] ++
    visit_tag ++
    [ "        o.pack(tag);"
    ] ++
    pack_extra ++
    [ "        return o;"
    , "    }"
    , "};"
    ]
  where
    elems_assign =
        [ "auto elems = std::visit([](auto&& arg) {"
        , "    using T = std::decay_t<decltype(arg)>;"
        ] ++ 
        visitBlock elem_assign_code (cppConstructors def) ++
        [ "    },"
        , "    v"
        , ");"
        ]
    elem_assign_code :: CppConstructor -> [Text]
    elem_assign_code c =
        [ "    return " <> (T.pack $ show $ 2 + (length $ cppConstructorVariables c)) <> ";"
        ]

    typename = "trodes::network::" <> (cppName def)
    visit_tag =
        [ "std::string tag = std::visit([](auto&& arg) {"
        , "    using T = std::decay_t<decltype(arg)>;"
        ] ++ 
        visitBlock actual_code (cppConstructors def) ++
        [ "    },"
        , "    v"
        , ");"
        ]
    actual_code :: CppConstructor -> [Text]
    actual_code c =
        [ "    return \"" <> (cppConstructorName c) <> "\";"
        ]

    pack_extra :: [Text]
    pack_extra =
        [ "// this is sad"
        ] ++
        ifBlock packStuff (cppConstructors def)

    packStuff :: CppConstructor -> [Text]
    packStuff c =
        [ "// pack"
        ] ++
        concatMap (packStruct c) (cppConstructorVariables c)

    packStruct :: CppConstructor -> CppRecordField -> [Text]
    packStruct c r =
        [ "o.pack(std::get<" <> ("trodes::network::" <> cppConstructorName c) <> ">(v)." <> (cppRecordFieldName r) <> ");"
        ]

visitBlock :: (CppConstructor -> [Text]) -> [CppConstructor] -> [Text]
visitBlock f cs = case cs of
    [] ->
        [ "// no constructors, this shouldn't happen"
        , "throw msgpack::type_error();"
        ]
    x:xs ->
        [ "if constexpr (std::is_same_v<T, " <> (named x) <> ">) {"
        ] ++
        (f x) ++
        (concatMap elseif xs) ++
        [ "} else {"
        , "    throw msgpack::type_error();"
        , "}"
        ]
  where
    named :: CppConstructor -> Text
    named c = "trodes::network::" <> (cppConstructorName c)

    elseif :: CppConstructor -> [Text]
    elseif c =
        [ "} else if constexpr (std::is_same_v<T, " <> (named c) <> ">) {"
        ] ++
        (f c)


showNamespaceConsName :: CppConstructor -> Text
showNamespaceConsName c = "trodes::network::" <> (cppConstructorName c)

prettyShowSumType :: CppDefinition -> Text
prettyShowSumType def = if length (cppConstructors def) == 1
                        then "// not a sum type, shouldn't use variants"
                        else T.intercalate "\n"
                            [ "// sum types are still a work in progress"
                            , constructorStructs
                            , ""
                            , called
                            ]
    where
        called :: Text
        called = template typeName constructorNames

        template :: Text -> [Text] -> Text
        template parent cs = "using " <> parent <> " = std::variant<" <>
            T.intercalate ", " cs <> ">;"
        -- for each one
        constructorNames :: [Text]
        constructorNames = map (cppConstructorName) $ cppConstructors def
        typeName :: Text
        typeName = cppName def

        constructorStructs = T.intercalate "\n\n" $ map makeStruct $ cppConstructors def

        makeStruct :: CppConstructor -> Text
        makeStruct c = T.intercalate "\n"
            [ "struct " <> cppConstructorName c <> "{"
            , singleStructVar c
            , "};"
            , ""
            ]
        
        singleStructVar c = if length (cppConstructorVariables c) == 1
                            then "// hah\n" <> (prettyShowField $ head $ cppConstructorVariables c)
                            else "// error, was expecting only one unnamed variable"
       


prettyShowCustomImports :: CppDefinition -> Text
prettyShowCustomImports def = imports
    where
        imports = if length customTypes == 0
                        then "// (no additional imports)" 
                        else T.intercalate "\n" $ "#include <variant>" : (map printCustomImport customTypes)
        -- search through all constructors looking for imports
        customTypes = nub $ concatMap getCustomsConstructor $ cppConstructors def
        getCustomsConstructor :: CppConstructor -> [Text]
        getCustomsConstructor c = concatMap getCustomsField $ cppConstructorVariables c
        getCustomsField :: CppRecordField -> [Text]
        getCustomsField f = case cppRecordFieldType f of
            RefPrim _ -> []
            RefCustom name -> [untypeName name]
        printCustomImport :: Text -> Text
        printCustomImport name = "#include \"" <> name <> ".h\""


prettyShowConstructor :: CppConstructor -> Text
prettyShowConstructor constructor = T.unlines
    [ "// constructor " <> cppConstructorName constructor
    , "struct " <> cppConstructorName constructor <> " {"
    , fieldFinal
    , "};"
    ]
    where
        fields = map prettyShowField $ cppConstructorVariables constructor
        fieldNames = T.intercalate ", " $ map cppRecordFieldName $ cppConstructorVariables constructor
        msgpack = "MSGPACK_DEFINE_MAP(" <> fieldNames <> ");"
        fields' = indent 4 $ vsep $ (map pretty fields) ++ [pretty msgpack]
        fieldFinal = T.pack $ show fields'
        -- fieldFinal = T.unlines fields

prettyShowField :: CppRecordField -> Text
prettyShowField field = T.intercalate "\n"
    [ "// field impl " <> cppRecordFieldName field
    , printField field
    ]
    where
        printField :: CppRecordField -> Text
        printField f = (printFieldType f) <> " " <> (printFieldName f) <> ";"

        printFieldType :: CppRecordField -> Text
        printFieldType f = docToText $ cppTypeRefDoc $ cppRecordFieldType f

        printFieldName :: CppRecordField -> Text
        printFieldName f = cppRecordFieldName f

        docToText = T.pack . show

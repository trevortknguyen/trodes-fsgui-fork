#QT += core
#QT -= gui

CONFIG += c++11

TARGET = FakeEthernetSource
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    SyntheticDataGenerator.cpp

HEADERS += \
    SyntheticDataGenerator.h

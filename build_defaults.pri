#-------------------------------------------------
#
# Project include file for all Trodes projects:
#   Trodes app, modules and export scripts
#
#-------------------------------------------------
#########################################
#GENERAL
#########################################
#testline

# Require minimum Qt versions for Windows and Mac/Linux
MAJ_REQ = 5
MIN_REQ = 9


lessThan(QT_MAJOR_VERSION, $$MAJ_REQ) | \
if(equals(QT_MAJOR_VERSION, $$MAJ_REQ):lessThan(QT_MINOR_VERSION, $$MIN_REQ)) {
    error("Building Trodes on $${QMAKE_HOST.os} requires Qt $${MAJ_REQ}.$${MIN_REQ} or greater, but Qt $$[QT_VERSION] was detected.")
}

#QMAKE_MAC_SDK = macosx10.14
QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.14
# save the top-level trodes repository path (same as location of this .pro file)
TRODES_REPO_DIR = $$PWD
REPO_LIBRARY_DIR = $$PWD/Libraries
#########################################
#GIT Info
#########################################
# Store information about the which git commit the current instantiation is compiled from
# and store result in a preprocessor directive.
GIT_COMMAND = git --git-dir $$system_quote($$system_path($$TRODES_REPO_DIR/.git)) \
    --work-tree $$system_quote($$system_path($$TRODES_REPO_DIR/)) describe --always --all \
    --tags --dirty --long

#must not contain spaces
GIT_COMMIT = $$system($$GIT_COMMAND)
DEFINES += GIT_COMMIT=\\\"$$GIT_COMMIT\\\"

# Get date/time on which Qmake was run (usually compile time)
COMPILE_INFO = (Compiled using Qt version: $$QT_VERSION ($$[QT_INSTALL_PREFIX] on hostname: \'$$QMAKE_HOST.name\').)

INCLUDEPATH += $$TRODES_REPO_DIR
#########################################
#Build info/params
#########################################
#Destdir is build/ for release, build_d for debug
CONFIG(debug, debug|release) {
    DESTDIR = $$_PRO_FILE_PWD_/build_d
} else {
    DESTDIR = $$_PRO_FILE_PWD_/build
}
## keep each project's build files in folders (useful when not doing shadow builds)
UI_DIR = $$DESTDIR/ui
MOC_DIR = $$DESTDIR/moc
OBJECTS_DIR = $$DESTDIR/obj
RCC_DIR = $$DESTDIR/rcc

CONFIG += warn_on


!win32 {
    # For perf profiling
#    QMAKE_CFLAGS += -fno-omit-frame-pointer
#    QMAKE_CXXFLAGS += -fno-omit-frame-pointer

    # These flags block a bunch of warning messages but aren't present for msvc
    QMAKE_CFLAGS_WARN_ON += -Wno-unused-parameter -Wno-unused-variable
    QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter -Wno-unused-variable
} else {
    # MSVC equivalent
    QMAKE_CFLAGS_WARN_ON -= -w34100
    QMAKE_CXXFLAGS_WARN_ON -= -w34100
    QMAKE_CFLAGS += -wd4100 # Unreferenced formal parameter
    QMAKE_CXXFLAGS += -wd4100
}

macx{
    CONFIG(app_bundle) {
        QMAKE_CLEAN += -r $$DESTDIR/$${TARGET}.app*
    } else {
        QMAKE_CLEAN += -r $$DESTDIR/$${TARGET}*
    }

}

unix:!macx{
    QMAKE_LFLAGS += -no-pie

    # Debug build on Linux with gcc
    # to trigger this:
    #   `qmake -set GDB_DEBUG debug`
    # to untrigger this:
    #   `qmake -unset GDB_DEBUG`
    $$[GDB_DEBUG] {
        QMAKE_LFLAGS += -pg
        QMAKE_CXXFLAGS += -pg -Og
    }
}
#########################################
#INSTALLATION
#########################################

macx:       INSTALL_DIR = $$TRODES_REPO_DIR/bin/macOS
#win32:      INSTALL_DIR = $$TRODES_REPO_DIR/bin/win32
win32:      INSTALL_DIR = $$TRODES_REPO_DIR/bin/win64
unix:!macx: INSTALL_DIR = $$TRODES_REPO_DIR/bin/linux

target.path = $$INSTALL_DIR
macx{
    CONFIG(app_bundle) {
        target.files = $$DESTDIR/$${TARGET}.app
        message(Mac bundle $$DESTDIR/$${TARGET}.app)
    } else {
        message(No Mac bundle $$DESTDIR/$${TARGET})
    }
}
INSTALLS += target


#########################################
#INSTALLATION: LINUX
#########################################
unix:!macx{

    #########################################
    #Project-specific libraries installation and rpath
    #Subprojects should use libraries.files += ... to add lib files
    #########################################
    LINUX_LIB_REL_DIR = Libraries
    LINUX_LIB_ABS_DIR = $$INSTALL_DIR/$$LINUX_LIB_REL_DIR
    libraries.path = $$LINUX_LIB_ABS_DIR

    QMAKE_LFLAGS += '-Wl,-rpath,\'\$$ORIGIN/$${LINUX_LIB_REL_DIR}\''


    #########################################
    #Qt library installation
    #########################################
    LINUX_QTLIBS_REL_DIR = Qt/lib
    LINUX_QTLIBS_ABS_DIR = $$INSTALL_DIR/$$LINUX_QTLIBS_REL_DIR

    LINUX_QTPLUGINS_REL_DIR = Qt/plugins
    LINUX_QTPLUGINS_ABS_DIR = $$INSTALL_DIR/$$LINUX_QTPLUGINS_REL_DIR

    QMAKE_LFLAGS += '-Wl,-rpath,\'\$$ORIGIN/$${LINUX_QTLIBS_REL_DIR}\''
    QMAKE_LFLAGS += '-Wl,-rpath-link,$$[QT_INSTALL_LIBS]'
    QMAKE_RPATH=# Unset rpath entries

    #Finding qt.conf file to identify where plugins dir is
    RESOURCES += $$TRODES_REPO_DIR/Resources/qtconf.qrc

    #Copy over qt libraries
    QtDeploy.path = /
    QtDeploy.commands += mkdir -p $$LINUX_QTLIBS_ABS_DIR ;
    QtDeploy.commands += rsync -a --info=NAME $$[QT_INSTALL_LIBS]/*.so* $$LINUX_QTLIBS_ABS_DIR ;
#The command below does not properly work. the goal was to only deploy the necessary
#    QtDeploy.commands += rsync -a --info=NAME `ldd /home/kevin/spikegadgets/trodes/Trodes/build/Trodes | awk \'/.*=>.*Qt.*/ {f = \$\$1; print \"$$[QT_INSTALL_LIBS]/\"f\"*\"}\' ORS=\' \'` $$LINUX_QTLIBS_ABS_DIR;
#    QtDeploy.commands += echo \"`ldd /home/kevin/spikegadgets/trodes/Trodes/build/Trodes | awk \'/.*=>.*Qt.*/ {f = \$\$1; print \"$$[QT_INSTALL_LIBS]/\"f\"*\"}\'`\";

    #Copy over qt plugins
    QtDeploy.commands += mkdir -p $$LINUX_QTPLUGINS_ABS_DIR ;
    QtDeploy.commands += rsync -a --info=NAME $$[QT_INSTALL_PLUGINS]/* $$LINUX_QTPLUGINS_ABS_DIR ;

    #Remove rpaths from 3rd party libraries
    QtDeploy.commands += "command -v chrpath >/dev/null 2>&1 || { echo \"---> ---> \'chrpath\' command not found, install with \'apt-get install chrpath\'\" ; exit 1 ; } ;"
    QtDeploy.commands += "for lib in $${TRODES_LIB_INSTALL_DIR}/*.so*; do [ -f \$$lib ] || continue ; chrpath -d \$$lib; done ;"

    INSTALLS += QtDeploy

}

#########################################
#INSTALLATION: WINDOWS
#########################################
win32{
    #########################################
    #Project-specific libraries installation and rpath
    #Subprojects should use libraries.files += ... to add lib files
    #########################################
    WIN_LIB_ABS_DIR = $$INSTALL_DIR
    libraries.path = $$WIN_LIB_ABS_DIR

    #########################################
    #Qt library installation
    #########################################
    QtDeploy.path = $$INSTALL_DIR
    CONFIG(debug, debug|release) {
        QtDeploy.commands += $$shell_path($$[QT_INSTALL_BINS]/windeployqt.exe) --debug $$shell_path($$INSTALL_DIR/$${TARGET}.exe)
    } else {
        QtDeploy.commands += $$shell_path($$[QT_INSTALL_BINS]/windeployqt.exe) --release $$shell_path($$INSTALL_DIR/$${TARGET}.exe)
    }

    INSTALLS += QtDeploy
}

#########################################
#INSTALLATION: MACOS
#########################################
macx{
#    #########################################
#    #Project-specific libraries installation and rpath
#    #Subprojects should use libraries.files += ... to add lib files
#    #########################################
    MAC_LIB_ABS_DIR = $$INSTALL_DIR/$${TARGET}.app/Contents/Frameworks
    libraries.path = $$MAC_LIB_ABS_DIR

#    #########################################
#    #Qt library installation
#    #########################################
    CONFIG(app_bundle){
        QtDeploy.path = /
        CONFIG(debug, debug|release) {
            #debug
            QtDeploy.commands += $$shell_path($$[QT_INSTALL_BINS]/macdeployqt) --debug $$shell_path($$INSTALL_DIR/$${TARGET}.app) -always-overwrite ;
        } else {
            #release/profile
            QtDeploy.commands += $$shell_path($$[QT_INSTALL_BINS]/macdeployqt) $$shell_path($$INSTALL_DIR/$${TARGET}.app) -libpath=$$PWD/Libraries/MacOS/lib/ -always-overwrite ;
            message($$shell_path($$[QT_INSTALL_BINS]/macdeployqt) $$shell_path($$INSTALL_DIR/$${TARGET}.app))
        }


        INSTALLS += QtDeploy
    }
}
#use c++14
CONFIG += c++1z

###CZMQ/ZMQ
#win32{
#    INCLUDEPATH += $$PWD/Libraries/Windows/zmq/include
#    INCLUDEPATH += $$PWD/Libraries/Windows/zmq/libs

#    DEPENDPATH += $$PWD/Libraries/Windows/zmq/libs
#    #    LIBS += -llibzmq-v120-mt-4_0_4

#    CONFIG(release, debug|release): LIBS += -L$$PWD/Libraries/Windows/zmq/libs -llibczmq
#    else:CONFIG(debug, debug|release): LIBS += -L$$PWD/Libraries/Windows/zmq/libs -llibczmqd

#    CONFIG(release, debug|release): LIBS += -L$$PWD/Libraries/Windows/zmq/libs -llibmlm
#    else:CONFIG(debug, debug|release): LIBS += -L$$PWD/Libraries/Windows/zmq/libs -llibmlmd

#    CONFIG(release, debug|release): LIBS += -L$$PWD/Libraries/Windows/zmq/libs -llibzmq
#    else:CONFIG(debug, debug|release): LIBS += -L$$PWD/Libraries/Windows/zmq/libs -llibzmqd

#}
unix:!macx{
  #Include headers for zmq
  INCLUDEPATH += $$PWD/Libraries/Linux/zmq/include
}

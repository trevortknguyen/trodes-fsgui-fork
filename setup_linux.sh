#!/bin/bash

echo "Setting up Trodes"
REPO=$PWD

# setup
git submodule init
git submodule update

# install libzmq
cd $REPO/extern/libzmq;
cmake -B build -DCMAKE_INSTALL_PREFIX=$REPO/Libraries/Linux/libzmq -DWITH_PERF_TOOL=OFF -DZMQ_BUILD_TESTS=OFF -DENABLE_CPACK=OFF -DCMAKE_BUILD_TYPE=Release;
cmake --build build
cmake --install build


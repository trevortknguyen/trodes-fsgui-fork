#include "abstractCamera.h"

AbstractCamera::AbstractCamera() {
    fmt = Fmt_Invalid;
}

void AbstractCamera::sendFrameSignals(QImage *img, uint32_t frameCount, uint64_t hwTimestamp, bool flip, qint64 fileTimestamp) {

    if (frameCount == 0) {
        numFramesRecieved++;
    } else {
        numFramesRecieved = frameCount;
    }
    //Ordering of these signals is important. Should actually just rename the second one.
    //newframe with parameters -> calls functions to render frame
    //newframe without parameters -> calls a request to get timestamp
    emit newFrame(img,numFramesRecieved, hwTimestamp, flip, -1);
    emit newFrame_timerequest();

}

void AbstractCamera::setFormat(AbstractCamera::videoFmt format) {
    fmt = format;
    emit formatSet(fmt);
}

AbstractCamera::videoFmt AbstractCamera::getFormat() {
    return fmt;
}

void AbstractCamera::blinkAcquire() {

}

void AbstractCamera::resetCurrentCamera() {

}


/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIDEODISPLAY_H
#define VIDEODISPLAY_H

#ifdef ARAVIS
#include "araviswrapper.h"
#endif

#ifdef AVT_GIGE //AVT GIGE
#include "avtWrapper.h"
#endif //

#include "webcamWrapper.h"
#include "CameraModuleClient.h"
#include "geometry.h"

#include <QtGui>
#include <QtWidgets>
#include <QtMultimedia>
//#include <QGLWidget>
#include <QOpenGLWidget>
#include <QInputDialog>
#include <QEvent>
#include <QCamera>
//#include <QCameraInfo>
#include "trodesSocket.h"
#include "trodesdatastructures.h"
//#include "../Trodes/src-main/trodesSocket.h"
#include "videoDecoder.h"
#include "videoEncoder.h"
#include "abstractCamera.h"
#include "dialogs.h"
#include <qmath.h>
#include "spkg/serializer.h"
#include "trodesglobaltypes.h"
//#include "videoWriter.h"


#include "highfreqclasses.h"
#include <memory> // for unique_ptr
#include <TrodesNetwork/Resources/SourcePublisher.h>
#include <TrodesNetwork/Generated/VideoPacket.h>



//#include <QCameraViewfinder>

//#include <QtCore/QRect>
//#include <QtGui/QImage>
//#include <QtMultimedia/QAbstractVideoSurface>
//#include <QtMultimedia/QVideoFrame>
#define DEBUG_MODE 0 //set this to 1 to have cameraModule print out usefull debugging information (tracks function calls)
//NOTE: because we utilize multithreading, often times the function crashing the program may not be the last one to print itself.
#define TIMESLIDERSTEPS 400000
#define LINEAR_JUMP_TIMEOUT 60

#undef PixelFormat
Q_DECLARE_METATYPE(QVector<bool>);
Q_DECLARE_METATYPE(TrackingSettings);

//typedef QVector<bool> PixIncludeArray;

class VideoDisplayWindow;
class VideoImageProcessor;

enum OptionFlag { O_NULL, O_ALL, O_ZONE, O_EXCLUDE, O_INCLUDE, O_INCEXCL, O_LINEAR, O_RANGE }; //optionFlags for save/load function

//void new_frame_callback(ArvStream*, void*); 

/*
class VideoDisplaySurface : public QAbstractVideoSurface {

Q_OBJECT

public:
     VideoDisplaySurface(VideoDisplayWindow *displayWidget, QObject *parent = 0);

     QList<QVideoFrame::PixelFormat> supportedPixelFormats(
             QAbstractVideoBuffer::HandleType handleType = QAbstractVideoBuffer::NoHandle) const;
     bool isFormatSupported(const QVideoSurfaceFormat &format, QVideoSurfaceFormat *similar) const;

     bool start(const QVideoSurfaceFormat &format);
     void stop();

     bool present(const QVideoFrame &frame);

     QRect videoRect() const { return targetRect; }
     void updateVideoRect();

     void paint(QPainter *painter);
     //void sendImage(TrodesClient *tcpClient);
     int thresh;

 private:
     VideoDisplayWindow *widget;
     QImage::Format imageFormat;
     QRect targetRect;
     QSize imageSize;
     QRect sourceRect;
     QVideoFrame currentFrame;

signals:

     void newFrame(QImage *img, bool flip);
     void newFrame();
};

*/

class RubberBandPolygonNode : public QObject, public QGraphicsRectItem {
    Q_OBJECT

public:

    RubberBandPolygonNode(int nodeNum, QGraphicsItem *parent = 0);

private:
    int nodeNum;
    bool dragging;
    bool fill;


protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

signals:
    void nodeMoved(int nodeNum);
    void nodeMoveFinished();
    void nodeRightClicked(int nodeInd, QPointF location);


};

//enum POLYGON_TYPE {P_NULL, P_ZONE, P_INCL, P_EXCL};

class RubberBandPolygon : public QObject, public QGraphicsPolygonItem {

Q_OBJECT

public:
//    Q_DISABLE_COPY(RubberBandPolygon)

    RubberBandPolygon(QGraphicsItem *parent = 0);
    ~RubberBandPolygon();
    void addPoint(QPointF);
    void moveLastPoint(QPointF);
    void removeLastPoint();
    bool isIncludeType();
    bool isExcludeType();
    bool isZoneType();
    void setIncludeType();
    void setExcludeType();
    void setZoneType();
    void addText(QString text);
    void setColor(const QBrush &br, double opac = -1);
    void setLineColor(const QColor &c);
    void calculateIncludedPoints(bool* inside, int imageWidth, int imageHeight);

    inline int getType(void) {return(type);}
    inline void setId(int Id) {id = Id;}
    inline int getId(void) {return(id);}
    inline void addEvent(QString ev) {events.append(ev); int timer = 0; eventTimers.append(timer);}
    inline QString getEvent(int index) {if (index > -1 && index < events.length()) {return(events.at(index));} else {return("");}}
    inline int getEventTimer(int evIndex) {if (evIndex > -1 && evIndex < eventTimers.length()) {return(eventTimers.at(evIndex));} else {return(-1);}}
    inline void setEventTimer(int evIndex, int val) {if (evIndex > -1 && evIndex < eventTimers.length()) {  eventTimers.replace(evIndex, val); }}
    inline int getEventsLength(void) {return(events.length());}
    inline void tickEventTimers(void) {for(int i = 0; i < eventTimers.length(); i++) { if(eventTimers.at(i) > 0) { eventTimers.replace(i,(eventTimers.at(i)-1)); } } }
    inline bool isSomethingInside(void) {return(inside);}
    inline void setInside(bool isInside) {inside = isInside;}

    inline QString getPrevEvent(void) {return(prevEvent);}
    inline void setPrevEvent(QString ev) {prevEvent = ev;}

    inline bool polyContainsPoint(const QPointF &pt) {return(curPoly.containsPoint(pt,Qt::OddEvenFill));}

    inline QVector<RubberBandPolygonNode*> getNodes(void) {return(nodes);}
    inline QVector<QPointF> getRPoints(void) {return(relativePoints);}
    //inline void setType

private:
    QVector<RubberBandPolygonNode*> nodes;
    QVector<QPointF> points;
    QVector<QPointF> relativePoints; // points are mapped between 0 and 1 in x,y coordinates
    QVector<QString> events;
    QString prevEvent;
    QVector<int> eventTimers;
    QPolygonF curPoly;
    QBrush brush;
    bool hasText;
    QGraphicsSimpleTextItem *textPtr;

    int type;
    int id;
    double opacity;
    bool dragging;
    bool inside;
    QColor lineColor;
    QGraphicsItem *parentItem;

    //QGraphicsPolygonItem* poly;

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);

private slots:
    void childNodeMoved(int nodeNum);
    void childNodeMoved();

public slots:
    void updateSize();
    void highlight();
    void removeHighlight();


signals:
    void hasHighlight();
    void shapeChanged();
};

class RubberBandNodeShape : public QObject, public QGraphicsPathItem {

Q_OBJECT

public:
    RubberBandNodeShape(QGraphicsItem *parent = 0);
    ~RubberBandNodeShape();
    void addPoint(QPointF);
    void addBranch(int nodeIndex);
    void connectLastLineToNode(int nodeIndex);
    void moveLastPoint(QPointF);
    void removeLastPoint();
    int clickedNearNode(const QPointF &point);
    int clickedNearLine(const QPointF &point);
    void setLineHighlight(int lineIndex);
    QVector<QPointF> getNodes();
    QVector<LineNodeIndex> getLines();
    void setShape(const QVector<QPointF> &nodes, QVector<LineNodeIndex> &lines);
    void setLineZone(int lineInd, int zone);
    void setLastMouseClickLoc(const QPointF &point);
    void dragTo(const QPointF &point);
    void addText(QString text);
    bool contains(const QPointF &point) const;
    inline RubberBandPolygonNode* accessNode(int index) { return(nodes[index]); }
    //QPainterPath shape() const;
    //QPointF getPoint(int pointIndex);

private:
    QVector<RubberBandPolygonNode*> nodes;
    QPointF lastMouseClickLoc;
    QVector<QPointF> pointsRelativeToLastMouseCLick;
    //QVector<QGraphicsLineItem*> lines;
    QVector<LineNodeIndex> lines;
    QVector<QPointF> points;
    QVector<QPointF> relativePoints; // points are mapped between 0 and 1 in x,y coordinates
    QPointF currentLocationMarker;
    int type;
    bool dragging;
    int currentLineInd;
    int lineHighlight;
    bool hasText;
    QGraphicsSimpleTextItem *textPtr;
    void setPainterPath();
    qreal distToLine(const QPointF &p, const QLineF &line) const;

    //QGraphicsPolygonItem* poly;

protected:

    void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);


private slots:
    void childNodeMoved(int nodeNum);
    void childNodeMoved();

public slots:
    void updateSize();
    void highlight();
    void removeHighlight();
    void setCurrentLocation(QPointF loc);



signals:
    void hasHighlight();
    void shapeChanged();
    void nodeRightClicked(int nodeInd, QPointF location);
};

//MARK: Event
enum toolFlag {T_RANGE, T_LINE, T_EXCL, T_INC };

class VideoDisplayWindow : public QWidget  {

Q_OBJECT


public:
    VideoDisplayWindow(QWidget *parent);
    //QAbstractVideoSurface *videoSurface() const { return surface; }
    //VideoDisplaySurface *surface;

    //QSize sizeHint() const;
    ~VideoDisplayWindow();
    QSize getResolution();

protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);
    void enterEvent(QEvent *event);
    void leaveEvent(QEvent *event);
    bool event(QEvent *) Q_DECL_OVERRIDE;

public slots:
    //void newImage(const QImage &image);
    //void newImage(QImage *image);
    void newImage(QImage);
    inline void setPixelScale(double value) { pixelScale = value; }
    //MARK: event
    void setBool(bool check, toolFlag flg);

private:
    double          pixelScale;
    bool            foundRangeLine;
    QImage             currentImage;
    QSize              currentSize;
    //QCameraViewfinder *viewfinder;

signals:
    void resolutionChanged();
    void newHeightNeeded(int newHeight);
    //MARK: event
    void isObjAt(const QPoint pos);
    void sig_newImage(void);

};

class CustomScene : public QGraphicsScene {

    Q_OBJECT

signals:
    void emptySpaceClicked();
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event)
    {
        //check to see if the item pressed was a polygon or the background (a widget showing video)
        if(itemAt(event->scenePos(),QTransform())) {
            QGraphicsItem *clickedItem = itemAt(event->scenePos(),QTransform());
            if (!clickedItem->isWidget()) {
                //a polygon was clicked
                QGraphicsScene::mousePressEvent((event));
                return;
            }

        }   

        emit emptySpaceClicked();
    }
};

class GraphicsWindow : public QGraphicsView {

    Q_OBJECT

public:
    GraphicsWindow(QWidget *parent);
    VideoDisplayWindow *dispWin; //shows the video frames
    void initializeEvents(void);
    void addIncludePolygon();
    void addExcludePolygon();
    void addZonePolygon();
    void addLineGeometry();
    void addTrackGeometry();
    void passKeyEvent(QKeyEvent *event);
    void deleteLineShape(int index);

protected:
    void resizeEvent(QResizeEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);

private:
    CustomScene *scene;
    QVector<QGraphicsEllipseItem*> plottedPoints;
    QVector<QRectF> plottedPointsR;
    QVector<RubberBandPolygon*> polygons;
    QVector<RubberBandNodeShape*> lineShapes;
    QGraphicsEllipseItem *medianLocMarker;
    QGraphicsEllipseItem *LED1Marker;
    QGraphicsEllipseItem *LED2Marker;
    QGraphicsEllipseItem *ringMarker;
    QGraphicsLineItem *directionMarker;
    QGraphicsPathItem *directionArrow;

    bool isTrackingOn;
    bool showTwoLeds;

    TrackingSettings trackSettings;
    QSizeF ringSize;
    QPointF medianLoc;
    QPointF LED1Loc;
    QPointF LED2Loc;
    QPointF curTrackedLoc; //animal's current tracked location

    int currentIncludePolygon;
    int currentLinearizationShape;
    int linearizationShapeAnchorNode;
    int rangeLineShape;
    int rangeLineNodeNum;
    int rangeLineAnchorNode;
    //QVector<int> linearizationShapeLineZones;

    bool logging;
    bool recording;
    bool linShapeIsDragging;
    bool currentlyDrawing;
    bool currentlyDrawingLineShape;
    int currentlySelectedPolygon;
    int currentlySelectedLineShape;
    int currentTool;

    double userRealDistanceInput;
    double pixlScale;

    int polygonIdCounter;

    bool doesZoneGeoExist(void);
    bool doesInclusionGeoExist(void);
    bool doesExclusionGeoExist(void);
    bool doesIncExclGeoExist(void);

    QString getLineContaining(QString str, QTextStream *fullText, bool startAtBegin = true);
    void printTextStream(QTextStream *stream, int streamPos = 0);


public slots:
    void polygonHighlighted();
    void lineShapeHighlighted();
    void spaceClicked();
    void setTool(int toolNum);
    void setNoHighlight();
    void newLocation(QPoint p);
    void newLocation(QPoint p1, QPoint p2, QPoint midPoint);
    void newLinearLocation(QPoint p);
    void locMarkerOn(bool on);
    void newSettings(TrackingSettings s);

    void loadGeometry(QString filename, OptionFlag flg);
    void loadLinearGeometry(QString filename, QFile *inputFile);
    void loadRangeLineGeometry(QString filename, QFile *inputFile);
    void loadZoneGeometry(QString filename, QFile *inputFile);
    void loadInclusionGeometry(QString filename, QFile *inputFile);
    void loadExclusionGeometry(QString filename, QFile *inputFile);

    void saveAllGeometry(QString fileName);

    void saveGeometry(QString filename, OptionFlag flg);
    void saveCurrentLinearGeometry(QString filename, QFile *savefile);
    void saveCurrentRangeGeometry(QString filename, QFile *savefile);
    void saveCurrentZoneGeometry(QString filename, QFile *savefile);
    void saveCurrentInclusionGeometry(QString filename, QFile *savefile);
    void saveCurrentExclusionGeometry(QString filename, QFile *savefile);
    void setPolygonIDs(void);

    void showLinearGeometryNodeContextMenu(int nodeInd, QPointF position);
    void showLinearGeometryLineContextMenu(int lineInd, QPointF position);

    //MARK: event
    void checkPosForObj(const QPoint pos);
    void calculatePixelScale(bool getInput = false);
    void checkPtPolyOverlap(QPointF p);
    void tickPolygonEventTimers(void);
    void plotCurTrackedLoc(void);
    void plotPoint(QPointF pt);
    void sendZoneData(qint8 zoneID);

    //MARK: export
    void recordingStarted(void);
    void recordingStopped(void);
    void setLogging(bool isLogging);

private slots:
    void calculateConsideredPixels();
    void linearGeometryShapeChanged();
    void changeWindowHeight(int newHeight);

signals:
    void newIncludeCalculation(QVector<bool>);
    void userInput1(QPoint loc);
    void userInput2(QPoint loc);
    void linearGeometryExists(bool);
    void rangeGeometryExists(bool);
    void zoneGeometryExists(bool);
    void incExclGeometryExists(bool);
    void newLinearGeometry(QVector<QPointF> nodes, QVector<LineNodeIndex> lines);
    void linearGeometryAnchorNodeSet(int nodeInd);
    void linearGeometryLineZoneSet(int lineInd, int zone);
    void pixelScaleChanged(double changeVal);

    void sendNewDataPacket(dataPacket dataToSend);

    //MARK: event
    void isObjAt(bool check, toolFlag flg);
    void broadcastEvent(TrodesEventMessage);
    void broadcastNewEventReq(QString event);
    void broadcastRemoveEventReq(QString event);

    //MARK: refactor
    void sig_broadcastMsg(QString subject, TrodesMsg msg);

    //void newIncludeCalculation(PixIncludeArray);


};

#ifdef ARAVIS
class GigEVideoDisplayController : public QObject {

Q_OBJECT

public:
    GigEVideoDisplayController(QObject *parent, VideoDisplayWindow *displayWidget, CameraModuleClient *networkClient, VideoEncoder *encoder);
	~GigEVideoDisplayController();
	void new_frame_callback(ArvStream*, void*); 

private:
    CameraModuleClient *networkClient;
    TrodesClient     *tcpClient;
    VideoImageProcessor *imageProcessor;
    QThread             *imageProcessorThread;

    void _init_aravis_gige();
    AravisWrapper *_arv;
	GigECameraInterface *_cur_camera;

	
public slots:
    //void setThresh(int thresh);
    //void endProcessor();
    //void startRecording();
    //void stopRecording();


signals:
	//void newFrame(const QImage& frame);
	void newFrame(QImage* frame);
	void dummy();
    //void videoStreamStart(int yResolution, int xResolution);
    //void signal_startRecording();
    //void signal_stopRecording();
//
};
#endif

class VideoDisplayController : public QObject {

Q_OBJECT

public:
    VideoDisplayController(QObject *parent, VideoDisplayWindow *displayWidget, CameraModuleClient *networkClient, const TrodesConfiguration& workspace);
    VideoDisplayController(QObject *parent, VideoDisplayWindow *displayWidget, TrodesClient *tcpClient);
    ~VideoDisplayController();
    //VideoDisplayWindow *videoWindow;
    //QAbstractVideoSurface *videoSurface() const { return surface; }
    //VideoDisplaySurface *surface;
    VideoImageProcessor *imageProcessor;
    QStringList availableCameras();
    void setSlaveFilePlayback(bool val);


    void setPTPCheck(bool ptpcheck);

    void setLinearJumpDistanceMultiplier(qreal value);

    void overrideSamplingRate(int samplingrate);

    QVector<quint32> playbackTimeStamps;
private:
    NetworkConfiguration::NetworkType networkType;
    QString zmqAddress;
    quint16 zmqPort;
    QList<AbstractCamera*> cameraControllers;
    int currentCameraType;
    int currentCameraNum;
    bool liveCameraMode;
    qint32 sliderStart;
    qint32 sliderEnd;

    bool fastPlayback;
    bool slaveFilePlayback;
    bool outOfRange;

    int estFrameRate;
    int samplingRate;
    QVector<int> cameraType; //unique for each driver type
    QVector<int> cameraNum; //for each driver, count the number of cameras available
    QStringList cameraNames;

    bool playing;
    bool stopUponTrackingError;

    void closeCurrentCamera();

    /*QCamera *cameraInput;
#ifdef AVT_GIGE
    AvtCameraController *avtCamera;
#endif
    */

    H264_Decoder *decoder;
    CameraModuleClient *networkClient;

    TrodesClient     *tcpClient;
    QThread             *imageProcessorThread;
    //void _init_qcamera(QCameraInfo*);
    QTimer *frameReadTimer;
    qint32 currentFrameNum;

    QTimer displayFirstFrameTimer;


    bool ptpCheck;

    qreal linearjumpmult;
    bool linearjumpmultset;
private slots:
    void readNextFrameFromFile();
    void displayFirstFrameFromFile();
    void resetCamera();



public slots:
    //void setThresh(int thresh, bool trackDark);
    void endProcessor();
    void startRecording();
    void stopRecording();
    void newCameraSelected(int cameraID);
    bool inputFileSelected(QString fileName);
    void startPlayback();
    void startFastPlayback();
    void pausePlayback();
    void closePlaybackFile();
    void seekFrame(qint32);
    void stepForward();
    void stepBackward();
    void seekRelativeFrame(int position); //slider position input
    void playbackSeekToTime(quint32 t);
    void seekToTime(quint32 t); //seek to the frame closest to the given time
    void newSliderRange(int start, int end);
    void newTimeRange(quint32 start, quint32 end);
    void startFirstCameraFound();
    void startController();
    void closeDown();
    void startFirstFrameTimer();
    void setLiveCameraMode(bool);
    void pauseForBadLoc();
    void setStopPlaybackUponTrackingError(bool stop);

    void userInput1(QPoint loc);
    void userInput2(QPoint loc);


    void linearGeometryExists(bool);
    void newLinearGeometry(QVector<QPointF> nodes, QVector<LineNodeIndex> lines);

    inline void checkSignal() {qDebug() << "signal fired"; };
    inline void checkSignal2(const dataPacket &check) {qDebug() << "signal fired **"; check.printAtr(); };

    void checkNextTimeFrame(quint32 timestamp);


    void checkPTPValues();
signals:
    void videoStreamStart(int yResolution, int xResolution);
    void signal_startRecording();
    void signal_stopRecording();
    void toggleLogging(bool);
    void signal_createFile(QString filename);
    void signal_createPlaybackLogFile(QString);
    void signal_closePlaybackLogFile();
    void signal_closeFile();
    void signal_nextFileNeeded();
    void signal_newSettings(TrackingSettings t);
    void signal_logging(bool logging);

    void signal_linearGeometryExists(bool);
    void signal_newLinearGeometry(QVector<QPointF> nodes, QVector<LineNodeIndex> lines);

    void signal_endProcessor();

    void signal_newImage(QImage*, quint32, quint64, bool, qint64);
    void signal_seekPositionFile(quint32);
    void signal_seekLinearPositionFile(quint32);

    void newFrame(QImage *img);
    //void newFrame(const QImage &img);
    void newAnimalPos(quint32 time);

    void sendDataPacket(dataPacket dataToSend);
    //void sendDataPacket(int check);

    void frameOutOfRange(bool);
    void master_timerequest();

    void pausedForBadLoc();
    void filePlaybackReady();
    void filePlaybackClosed();
    void filePlaybackStopped();
    void videoFileTimeRate(quint32);
    void videoFileTimeRange(quint32, quint32);
    void newVideoTimeStamp(quint32 timeStamp);
    void finished();
    void sig_seekFrame(qint32);
    void signal_startFirstFrameTimer();
    void processorCreated();
    void getUserTrackingInput();
    void sig_UserInput1(QPoint loc);
    void sig_UserInput2(QPoint loc);
    void sig_newSliderRange(quint32 start, quint32 end);
    void sig_newSource();

    void sig_pixelScaleReceived(double pixScale);

    void sig_saveAllGeometry(QString fileName);

    void sig_ptp_fullyInitialized();
    void sig_ptp_tryingToInitialize();
    void sig_ptp_calibrating();
    void sig_ptp_notProperlySetup();
    void sig_ptp_wrongconfiguration();
    void sig_ptp_warningmessagebox(QString);
};


class FrameBundle {

public:
    FrameBundle();
    void setImage(QImage *imagePtr);
    void setTime(quint32 timestamp);
    void setLocation(qint16 x1, qint16 y1, qint16 x2, qint16 y2);

    void deleteFrame();
    bool bothFieldsFilled();

    QImage      *imagePtr;
    quint32     timestamp;
    quint32     hwFrameCount;
    quint64     hwTimestamp;
    qint16      xloc;
    qint16      yloc;
    qint16      xloc2;
    qint16      yloc2;

private:
    bool        videoFieldFilled;
    bool        timeFieldFilled;
    bool        locationFieldFilled;

};

class VideoImageBuffer : public QObject {

    Q_OBJECT

public:

    explicit VideoImageBuffer(QObject *parent);
    ~VideoImageBuffer();
    void addImage(QImage *imagePtr, uint32_t hwFrameCount, uint64_t hwTimestamp);
    void addImage(QImage *imagePtr,uint32_t hwFrameCount, uint64_t hwTimestamp, QPoint loc1,QPoint loc2);
    void addTimestamp(quint32 timestamp);
    FrameBundle getNextFrame();
    void removeLastFrame();

private:

    int bufferSize;
    FrameBundle *frameBundleBuffer;
    int imageWriteHead;
    int timestampWriteHead;
    int frameReadHead;
    int lastFrameReadHead;

};

class VideoImageProcessor : public QObject {

Q_OBJECT

public:
    VideoImageProcessor(QObject *parent);
    //VideoImageProcessor(QObject *parent,VideoWriter *writer);
    ~VideoImageProcessor();
    //VideoEncoder    *encoder;
    //VideoWriter     *writer;
    quint32 clockRate;
    bool isFileCreated();
    //void createFileAfterCameraLoaded(QString filename);
    int currentTime;
    qint32 currentFrameTimestamp;
    void setPlaybackTimestamps(QVector<quint32> playbackTimeStamps_in);

    /*
     * Called by the VideoDisplayController if the system is using a
     * zmq-based networking system.
     *
     * It creates the trodesnetwork publisher.
     *
     * Returns true if there's an error.
     *
     */
    bool initializeTrodesNetworkPublisher(const QString& address, const quint16& port);

    void setLinearJumpDistanceMultiplier(const qreal &value);

    void setTrackingFileInfo(const QFileInfo* fi);

private:
    QVector<quint32>    playbackTimeStamps;
    bool                scaleDownResolution;
    unsigned int        scaledDownVideoHeight;

    bool                framesStillComing;
    bool                streamActive;
    bool                videoInfoCalculated;
    bool                recording;

    int                 frameHeight;
    int                 frameWidth;
    float               frameRate;
    qint64              numFramesProcessed;

    QVector<uint16_t>   xcounts1;
    QVector<uint16_t>   ycounts1;
    QVector<uint16_t>   xcounts2;
    QVector<uint16_t>   ycounts2;

    int firstIndPosFile;
    int firstIndLinearPosFile;
    QVector<uint32_t> posFileTimestamps;
    QVector<uint32_t> linearPosFileTimestamps;
    QVector<uint16_t> segment;
    QVector<double> normalizedPos;
    QVector<uint16_t> xlocs;
    QVector<uint16_t> ylocs;
    QVector<uint16_t> xlocs2;
    QVector<uint16_t> ylocs2;

    AbstractCamera::videoFmt    currentVideoFormat;
    X264VideoEncoder*   videoEncoder;

    bool                fileCreated;
    bool                createFileAfterCameraLoad;
    QString             baseFileName;
    QString             nextFileName;

    VideoImageBuffer    *frameBuffer;
    QImage              *lastFrame;
    uint32_t            lastHWFrameCount;

    // For tracking
    TrackingSettings    currentSettings;
    bool                trackingOn;
    QVector<bool>       includedPixels;
    bool                skipPosCalc;

    qint16              xMedian;
    qint16              yMedian;
    QPoint              LED1Loc;
    QPoint              LED2Loc;
    QPoint              midPointLoc;
    //int                 brightPixThreshold;
    //bool                trackDark;
    //int                 ringSize;
    //int                 ringOn;
    bool trackLinearPosition;
    qreal currentTrackedLinPosition;
    QVector<QVector<qreal> > shortestDistanceMatrix; //used to keep track of the shortest distance between all nodes
    QVector<int> endNodes; //all of the linear geometry nodes where track arms end
    QVector<qreal> distToEndNodes;
    int linearGeometryAnchorNodeInd;
    int lastLocationSegment;
    int changeSegTimer;
    QVector<QPointF> linearTrackNodes;
    QVector<LineNodeIndex> linearTrackLines;
    qreal linearJumpDistanceMultiplier;

    QFile *linearPositionFile;
    QString linearpositionFileName;
    bool linearPositionFileOpen;
    qint64 linearFileStartOfdata;

    QFile *positionFile;
    QString positionFileName;
    bool positionFileOpen;
    qint64 positionfileStartOfData;
    bool logging;  //for offline position logging

    bool fastPlayback;

    QElapsedTimer *fpsTimer;

    qint32 currFileTimestamp;

    bool createLinearPositionFile(QString filename);
    void closeLinearPositionFile(void);
    void writeLinearPosition(qint32 timestamp, int16_t lineSegment, double linPosition);
    void setVideoResolutionReduction(bool on, unsigned int height);

    bool createPositionFile(QString filename);
    void closePositionFile();
    void writePosition(const FrameBundle &fb);

    void processImage(QImage &image, qint64 fileTimestamp, bool updateTimestamp);
    void processImage_2LED_REDGREEN(QImage &image, qint64 fileTimestamp, bool updateTimestamp);
    void findLinearPosition(QPoint currentLoc);
    qreal distToLine(const QPointF &p, const QLineF &line, qreal *lineProjection);

    qint64 findHeader(QFile* file, QString tag);
    void loadPositionData(QFile* file, qint64 pos);
    void loadLinearPositionData(QFile* file, qint64 pos);

    void updatePositionData(QPoint& p);
    void updateLinearPositionData(int& closestSegment);

    QTimer *watchdogTimer;

    bool linearTrackExists;

    double pixelScale;


    // this should be a pointer because it shouldn't exist if TrodesNetwork isn't enabled
    std::unique_ptr<trodes::network::SourcePublisher<trodes::network::VideoPacket>> positionTrodesPub;

//    std::vector<uint8_t> positionbuffer;
    SpkG::Serializer<uint32_t,int,double,int16_t,int16_t> positionData;

public slots:

    inline void setPixelScale(double pixScale) {pixelScale = pixScale;};
    void newImage(QImage *img, quint32 hardwareFrameCount, quint64 hardwareTimestamp, bool flip, qint64 fileTimestamp);
    void replotLastFrame();
    void newTimestamp(quint32 t);
    void endProcessing();
    void initialize();
    void createFile(QString filename);
    void createPlaybackLogFile(QString filename);
    void setResReduce(int maxHeight);

    void seekLinearPositionFile(quint32 t);
    void seekPositionFile(quint32 t);

    void closePlaybackLogFile();
    void toggleLogging(bool on);
    void startRecording();
    void stopRecording();
    void closeFile();
    void setFastPlayback(bool on);

    void linearGeometryExists(bool);
    void newLinearGeometry(QVector<QPointF> nodes, QVector<LineNodeIndex> lines);
    void linearGeometryAnchorNodeSet(int nodeInd);
    void linearGeometryLineZoneSet(int lineInd, int zone);


    void newTrackingSettings(TrackingSettings t);
    //void newThreshold(int value, bool trackDark);
    //void newRing(int ringSize, bool ringOn);
    void setTracking(bool);
    void setIncludedPixels(QVector<bool> includedPixels);
    void setVideoFormat(AbstractCamera::videoFmt);

    void userInput1(QPoint loc);
    void userInput2(QPoint loc);
    void resetSourceInfo();

    void watchdogTimout();



    //void debugSlot(void) { qDebug() << "DEBUG SLOT"; }

    //void setIncludedPixels(PixIncludeArray includedPixels);


signals:
    void nextImage();
    void finished();
    void streamStarted(int yResolution, int xResolution);
    void fileOpened();
    void recordingStarted();
    void recordingStopped();
    //void newImage_signal(QImage *img);
    void newImage_signal(QImage img);
    void newAnimalLocation(quint32 time);

    void encodeImage(const QImage &);
    void writeTimeStamp(quint32 t, quint32 hwFrameCount, quint64 hwTimestamp);
    void newLocation(QPoint p);
    void newLocation(QPoint p1, QPoint p2, QPoint midPoint);
    void newLinearLocation(QPoint p);
    void badLocation();

    void watchdogAlarm();
    void testSig();
    void sendNewDataPacket(const dataPacket &dataToSend);

    void sig_saveAllGeometry(QString filename);
    void sig_logging(bool logging);
    void sig_broadcastMsg(QString subject, TrodesMsg msg);
    //void sendNewDataPacket(int check);

};


#endif // VIDEODISPLAY_H

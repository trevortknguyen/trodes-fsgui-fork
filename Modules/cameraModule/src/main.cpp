/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"
#include "qtmoduledebug.h"
#include <QApplication>
#ifdef PYTHONEMBEDDED
#include "PythonQt.h"
#include "PythonQt_QtAll.h"
#endif

bool usePython = false;

int main(int argc, char *argv[])
{
    qInstallMessageHandler(moduleMessageOutput);
    setModuleName("cameraModule");
    //qSetMessagePattern("[cameraModule] %{message}");

    QApplication a(argc, argv);
    QCoreApplication::addLibraryPath(QCoreApplication::applicationDirPath());
    a.setStyle(new Style_tweaks);
    // turn on the DPI support**


    qRegisterMetaType<QVector<bool> >("QVector<bool>");
    qRegisterMetaType<TrackingSettings>("TrackingSettings");
    qRegisterMetaType<TrackingSettings>("AbstractCamera::videoFmt");
    qRegisterMetaType<QVector<QPointF> >("QVector<QPointF>");
    qRegisterMetaType<QVector<LineNodeIndex> >("QVector<LineNodeIndex>");
    qRegisterMetaType<dataPacket>("dataSend");
    qRegisterMetaType<dataPacket>("dataPacket");
    qRegisterMetaType<TrodesEvent>("TrodesEvent");
    qRegisterMetaType<TrodesEventMessage>("TrodesEventMessage");
    qRegisterMetaType<QVector<TrodesEvent> >("QVector<TrodesEvent>");
    qRegisterMetaType<uint32_t>("uint32_t");
    qRegisterMetaType<uint8_t>("uint8_t");
    qRegisterMetaType<TrodesMsg>("TrodesMsg");

    qDebug().noquote() << "CameraModule Version Info:\n" << GlobalConfiguration::getVersionInfo(false); //print version info to debug log

#ifdef PYTHONEMBEDDED
    //Ititialize PythonQt
    //wchar_t t[200];
    QVector<wchar_t> pathVect;
    //wchar_t b[200];
    //QString pythonBin = QCoreApplication::applicationDirPath() + "/";

    QString pythonPath;
    //QString pythonPath = QCoreApplication::applicationDirPath() + "/python/python3.5/";

    //QString pythonPath = "/usr/local/lib/python3.5/";
    //QString pythonPath = "/home/maxim/Downloads/Python-3.5.2/Lib";

    QFile inputFile("pythonPath.txt");
    QFileInfo pathFileInfo("pythonPath.txt");
    //MARK: CHANGE before push
    //QFile inputFile("/Users/maris/Programming/Work/Trodes/trodes/Modules/cameraModule/src/pythonPath.txt");
    //QFileInfo pathFileInfo("/Users/maris/Programming/Work/Trodes/trodes/Modules/cameraModule/src/pythonPath.txt");


    if (pathFileInfo.exists()) {
        qDebug() << "pythonPath.txt found";

        if (inputFile.open(QIODevice::ReadOnly))
        {
           QTextStream in(&inputFile);
           while (!in.atEnd())
           {
              QString line = in.readLine();
              if (line.startsWith("PYTHONPATH")) {
                  QStringList tempList = line.split(QRegularExpression("\\s*=\\s*"));
                  //QStringList tempList = line.split(QRegExp("\\s")); //++
                  if (tempList.length() > 1) {

                      pythonPath = tempList.at(1);
                      pathVect.resize(pythonPath.length()+1);
                      pathVect.fill(NULL);
                      QDir pathFileDir(pythonPath);
                      if (pathFileDir.exists()) {
                          qDebug() << "Setting python path to " << tempList.at(1);
                      } else {
                          qDebug() << "Error: python path does not exist: " << pythonPath;
                          pythonPath = "";
                      }
                  }
              }
              //mainContext.evalScript("sys.path.append('" + pathFileDir.absoluteFilePath(line) + "')\n");
           }
           inputFile.close();
        }

    } else {
        qDebug() << "pythonPath.txt not found";
    }



    if (!pythonPath.isEmpty()) {
        usePython = true;

        //pythonPath.toWCharArray(t);
        pythonPath.toWCharArray(pathVect.data());

        //Py_SetPath(t);
        setPyPath(pathVect.data());


        //qDebug() << QString().fromWCharArray(Py_GetPath()) << " == " << QString().fromWCharArray(pathVect.data());

       // pythonBin.toWCharArray(b);
        //pythonPath.toWCharArray(t);
        //Py_SetPath(t);

        //Py_SetPythonHome(b);

        //PythonQt::init(PythonQt::IgnoreSiteModule | PythonQt::RedirectStdOut);
        PythonQt::init(PythonQt::RedirectStdOut);
        PythonQt_QtAll::init();

    }


#endif

    // this is for non-display mode
    //a.setQuitOnLastWindowClosed(false);

    MainWindow w(a.arguments()); //pass the input arguments to the main window
    QCoreApplication::addLibraryPath(QCoreApplication::applicationDirPath());

    w.show();

    w.setFocusPolicy(Qt::StrongFocus);
    return a.exec();
}

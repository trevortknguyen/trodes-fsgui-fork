﻿/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"


#include <TrodesNetwork/Resources/SourceSubscriber.h>
#include <TrodesNetwork/Generated/AcquisitionCommand.h>



#include <memory>

extern bool usePython;

MainWindow::MainWindow(QStringList arguments, QWidget *parent)
    : QMainWindow(parent)
    , loadedWorkspace() {
    //GLOBAL CONFIGURATION POINTERS-- THESE ARE FILLED IF A WORKSPACE FILE IS PASSED IN THE ARGUMENTS
    //globalConf = NULL;
    //hardwareConf = NULL;
//    nTrodeTable = NULL;
    //streamConf = NULL;
    //spikeConf = NULL;
    //headerConf = NULL;
    //moduleConf = NULL;
    //networkConf = NULL;

    bool displayDebugOut = false; //set to true to display verbose output of window setup progress

    linearGeometryExists = false;
    numOfSentPackets = 0;
    programIsClosing = false;
    sourceDialogOpen = false;
    toolsDialogOpen = false;

    trackingOn = false;
    loggingOn = false;
    currentTool = 1;  //fix tool
    justSeekedFlag = false;
    lockEnterButtonLinkToPlay = false;

    moduleInstance = 1;
    ptpChecksEnabled = false;

    resReduceSetting = -1;  //-1 is no resolution reduction

    qDebug() << "Camera module starting.";

    //Place the window in the upper right hand corner.
    resize(400, 300);
    QScreen *screenInfo = QGuiApplication::primaryScreen();
    QRect ScreenRect = screenInfo->availableGeometry();
    int x;
    int y;

    //For some reason, window positioning is not the same across platforms.
#ifdef WIN32
    x = ScreenRect.right()-420;
    y = ScreenRect.top();
#else
    x = ScreenRect.right()-400;
    y = ScreenRect.top()-300;
#endif

    move(x,y);

    //menu setup
    //-------------------------------------------
    menuFile = new QMenu;
    menuFile->setTitle(tr("&File"));


    QMenu *menuSettings = new QMenu;
    menuSettings->setTitle(tr("&Settings"));
    menuBar()->addAction(menuFile->menuAction());
    menuBar()->addAction(menuSettings->menuAction());

    menuResReduce = new QMenu;
    menuResReduce->setTitle("Video resolution reduction");
    menuResReduceNone = new QAction;
    menuResReduceNone->setText("None");
    menuResReduceNone->setCheckable(true);
    menuResReduceNone->setChecked(true);
    menuResReduce480 = new QAction;
    menuResReduce480->setText("480p");
    menuResReduce480->setCheckable(true);
    menuResReduce480->setChecked(false);
    menuResReduce720 = new QAction;
    menuResReduce720->setText("720p");
    menuResReduce720->setCheckable(true);
    menuResReduce720->setChecked(false);
    menuResReduce1080 = new QAction;
    menuResReduce1080->setText("1080p");
    menuResReduce1080->setCheckable(true);
    menuResReduce1080->setChecked(false);
    menuResReduce->addAction(menuResReduceNone);
    menuResReduce->addAction(menuResReduce480);
    menuResReduce->addAction(menuResReduce720);
    menuResReduce->addAction(menuResReduce1080);
    menuSettings->addAction(menuResReduce->menuAction());
    connect(menuResReduceNone, SIGNAL(triggered()), this, SLOT(setResReduceNone()));
    connect(menuResReduce480, SIGNAL(triggered()), this, SLOT(setResReduce480()));
    connect(menuResReduce720, SIGNAL(triggered()), this, SLOT(setResReduce720()));
    connect(menuResReduce1080, SIGNAL(triggered()), this, SLOT(setResReduce1080()));


    settingsMenuPlayback = new QMenu;
    settingsMenuPlayback->setTitle("Playback");
    actionSetStopWhenTrackingError = new QAction;
    actionSetStopWhenTrackingError->setText("Stop upon tracking error");
    actionSetStopWhenTrackingError->setCheckable(true);
    actionSetStopWhenTrackingError->setChecked(true);
    connect(actionSetStopWhenTrackingError, SIGNAL(toggled(bool)),this,SLOT(setStopPlaybackUponTrackingError(bool)));
    settingsMenuPlayback->addAction(actionSetStopWhenTrackingError);
    menuSettings->addAction(settingsMenuPlayback->menuAction());



    //define subMenu items
    menuGeometry = new QMenu;
    menuLoadGeo = new QMenu;
    menuSaveGeo = new QMenu;
    menuLoadGeoOptions = new QMenu;
    menuSaveGeoOptions = new QMenu;
    menuGeometry->setTitle("Geometry");
    menuLoadGeo->setTitle("Load geometry...");
    menuSaveGeo->setTitle("Save geometry...");
    menuLoadGeoOptions->setTitle("Load Specific");
    menuSaveGeoOptions->setTitle("Save Specific");
    //Create Menu Structures
    menuFile->addAction(menuGeometry->menuAction());
    menuGeometry->addAction(menuLoadGeo->menuAction());
    menuGeometry->addAction(menuSaveGeo->menuAction());



    //define Actions
    //load options
    actionLoadAllGeoTrack = new MyAction(this);
    actionLoadAllGeoTrack->setText("Load All");
    actionLoadAllGeoTrack->setOption(O_ALL);
    actionLoadAllGeoTrack->setEnabled(true);
    menuLoadGeo->addAction(actionLoadAllGeoTrack);
    //save options
    actionSaveAllGeoTrack = new MyAction(this);
    actionSaveAllGeoTrack->setText("Save All");
    actionSaveAllGeoTrack->setOption(O_ALL);
    actionSaveAllGeoTrack->setEnabled(false);
    menuSaveGeo->addAction(actionSaveAllGeoTrack);

    menuLoadGeo->addAction(menuLoadGeoOptions->menuAction());
    menuSaveGeo->addAction(menuSaveGeoOptions->menuAction());

    //sub menu options
    //sub menu load options
    actionLoadLinearGeoTrack = new MyAction(this);
    actionLoadRangeGeoTrack = new MyAction(this);
    actionLoadZoneGeo = new MyAction(this);
    actionLoadInclusionExclusionGeo = new MyAction(this);
    actionLoadLinearGeoTrack->setText("Load Linear Geometry");
    actionLoadRangeGeoTrack->setText("Load Range Geometry");
    actionLoadZoneGeo->setText("Load Zone Geometry");
    actionLoadInclusionExclusionGeo->setText("Load Inclusion/Exclusion Geometry");
    actionLoadLinearGeoTrack->setOption(O_LINEAR);
    actionLoadRangeGeoTrack->setOption(O_RANGE);
    actionLoadZoneGeo->setOption(O_ZONE);
    actionLoadInclusionExclusionGeo->setOption(O_INCEXCL);
    actionLoadLinearGeoTrack->setEnabled(true);
    actionLoadRangeGeoTrack->setEnabled(true);
    actionLoadZoneGeo->setEnabled(true);
    actionLoadInclusionExclusionGeo->setEnabled(true);
    menuLoadGeoOptions->addAction(actionLoadLinearGeoTrack);
    menuLoadGeoOptions->addAction(actionLoadRangeGeoTrack);
    menuLoadGeoOptions->addAction(actionLoadZoneGeo);
    menuLoadGeoOptions->addAction(actionLoadInclusionExclusionGeo);

    //sub menu save options
    actionSaveLinearGeoTrack = new MyAction(this);
    actionSaveRangeGeoTrack = new MyAction(this);
    actionSaveZoneGeo = new MyAction(this);
    actionSaveInclusionExclusionGeo = new MyAction(this);
    actionSaveLinearGeoTrack->setText("Save Linear Geometry");
    actionSaveRangeGeoTrack->setText("Save Range Geometry");
    actionSaveZoneGeo->setText("Save Zone Geometry");
    actionSaveInclusionExclusionGeo->setText("Save Inclusion/Exclusion Geometry");

    actionSaveLinearGeoTrack->setOption(O_LINEAR);
    actionSaveRangeGeoTrack->setOption(O_RANGE);
    actionSaveZoneGeo->setOption(O_ZONE);
    actionSaveInclusionExclusionGeo->setOption(O_INCEXCL);
    actionSaveLinearGeoTrack->setEnabled(false);
    actionSaveRangeGeoTrack->setEnabled(false);
    actionSaveZoneGeo->setEnabled(false);
    actionSaveInclusionExclusionGeo->setEnabled(false);
    menuSaveGeoOptions->addAction(actionSaveLinearGeoTrack);
    menuSaveGeoOptions->addAction(actionSaveRangeGeoTrack);
    menuSaveGeoOptions->addAction(actionSaveZoneGeo);
    menuSaveGeoOptions->addAction(actionSaveInclusionExclusionGeo);

    //load action connections
    connect(actionLoadAllGeoTrack, SIGNAL(triggered()), actionLoadAllGeoTrack, SLOT(sendOption()));
    connect(actionLoadAllGeoTrack, SIGNAL(optionSig(OptionFlag)), this, SLOT(loadGeometry(OptionFlag)));
    connect(actionLoadLinearGeoTrack, SIGNAL(triggered()), actionLoadLinearGeoTrack, SLOT(sendOption()));
    connect(actionLoadLinearGeoTrack, SIGNAL(optionSig(OptionFlag)), this, SLOT(loadGeometry(OptionFlag)));
    connect(actionLoadRangeGeoTrack, SIGNAL(triggered()), actionLoadRangeGeoTrack, SLOT(sendOption()));
    connect(actionLoadRangeGeoTrack, SIGNAL(optionSig(OptionFlag)), this, SLOT(loadGeometry(OptionFlag)));
    connect(actionLoadZoneGeo, SIGNAL(triggered()), actionLoadZoneGeo, SLOT(sendOption()));
    connect(actionLoadZoneGeo, SIGNAL(optionSig(OptionFlag)), this, SLOT(loadGeometry(OptionFlag)));
    connect(actionLoadInclusionExclusionGeo, SIGNAL(triggered()), actionLoadInclusionExclusionGeo, SLOT(sendOption()));
    connect(actionLoadInclusionExclusionGeo, SIGNAL(optionSig(OptionFlag)), this, SLOT(loadGeometry(OptionFlag)));

    //save action connections
    connect(actionSaveAllGeoTrack, SIGNAL(triggered()), actionSaveAllGeoTrack, SLOT(sendOption()));
    connect(actionSaveAllGeoTrack, SIGNAL(optionSig(OptionFlag)), this, SLOT(saveGeometry(OptionFlag)));
    connect(actionSaveLinearGeoTrack, SIGNAL(triggered()), actionSaveLinearGeoTrack, SLOT(sendOption()));
    connect(actionSaveLinearGeoTrack, SIGNAL(optionSig(OptionFlag)), this, SLOT(saveGeometry(OptionFlag)));
    connect(actionSaveRangeGeoTrack, SIGNAL(triggered()), actionSaveRangeGeoTrack, SLOT(sendOption()));
    connect(actionSaveRangeGeoTrack, SIGNAL(optionSig(OptionFlag)), this, SLOT(saveGeometry(OptionFlag)));
    connect(actionSaveZoneGeo, SIGNAL(triggered()), actionSaveZoneGeo, SLOT(sendOption()));
    connect(actionSaveZoneGeo, SIGNAL(optionSig(OptionFlag)), this, SLOT(saveGeometry(OptionFlag)));
    connect(actionSaveInclusionExclusionGeo, SIGNAL(triggered()), actionSaveInclusionExclusionGeo, SLOT(sendOption()));
    connect(actionSaveInclusionExclusionGeo, SIGNAL(optionSig(OptionFlag)), this, SLOT(saveGeometry(OptionFlag)));

    menuGeometry->setEnabled(false);

    menuEdit = new QMenu(this);
    menuEdit->setTitle(tr("&Edit"));
    actionSetPythonDir = new QAction(this);
    actionSetPythonDir->setText("Set Python script folder...");
    actionSetPythonDir->setMenuRole(QAction::ApplicationSpecificRole);
    menuEdit->addAction(actionSetPythonDir);
    menuBar()->addAction(menuEdit->menuAction());
    connect(actionSetPythonDir, SIGNAL(triggered()), this, SLOT(setPythonDir()));

    menuHelp = new QMenu(this);
    menuHelp->setTitle(tr("&Help"));
    actionAbout = new QAction(this);
    actionAbout->setText("About");
    actionAbout->setMenuRole(QAction::AboutRole);
    menuHelp->addAction(actionAbout);
    menuBar()->addAction(menuHelp->menuAction());
    connect(actionAbout, SIGNAL(triggered()), this, SLOT(about()));





    QString lastCameraOpen;

    //Used the saved system settings from the last session as the default start settings
    //Any command-line inputs will override this below
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
    settings.beginGroup(QLatin1String("processor"));
    int tempThresh = settings.value(QLatin1String("thresh")).toInt();
    if (tempThresh < 0) {
        tempThresh = 0;
    }
    bool tmpTrackDark = settings.value(QLatin1String("trackDarkPixels")).toBool();
    trackSettings.currentThresh = tempThresh;
    trackSettings.trackDark = tmpTrackDark;


    int tempRing = settings.value(QLatin1String("ringSize")).toInt();
    if (tempRing < 0) {
        tempRing = 0;
    }
    bool tmpRingOn = settings.value(QLatin1String("ringOn")).toBool();
    trackSettings.currentRingSize = tempRing;
    trackSettings.ringOn = tmpRingOn;


    bool ok;
    int tempLEDColor = settings.value(QLatin1String("ledColorPair")).toInt(&ok);
    if (ok) {
        trackSettings.LEDColorPair = tempLEDColor;
    } else {
        trackSettings.LEDColorPair = LED_COLOR_WHITE_WHITE;
    }

    bool tmpTwoLEDs = settings.value(QLatin1String("twoLEDs")).toBool();
    trackSettings.twoLEDs = tmpTwoLEDs;

    settings.endGroup();

    clockRate = 30000;

    //Place the window where it was the last session
    settings.beginGroup(QLatin1String("position"));
    QRect tempPosition = settings.value(QLatin1String("position")).toRect();
    if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
    }
    settings.endGroup();

    //Remember the python folder
    settings.beginGroup(QLatin1String("pythonDir"));
    QString pDir = settings.value(QLatin1String("pythonDir")).toString();
    if (!pDir.isEmpty()) {
        pythonDir = pDir;
    }


    settings.endGroup();




    //Create a NULL networkConf variable (it's a global variable)
    //networkConf = nullptr;




    //Make the layout
    mainLayout = new QGridLayout();
    mainLayout->setVerticalSpacing(2);


    //isTcpClientConnected = false;
    //tcpClient = NULL;
    moduleID = -10;
    videoStreaming = false;
    fileInitiated = false;
    fileOpen = false;
    recording = false;

    cameraNum = 0;

    inputFileOpen = false;
    playbackMode = false;
    inputFileName = "";
    fileStartTime = 0;
    fileEndTime = 0;
    videoWasPlayingBeforeSliderPress = false;
    isVideoFilePlaying = false;
    isVideoSliderPressed = false;

    //Parse the command line options
    QString resValueX;
    QString resValueY;
    QString trodesConfigFile;
    //QString serverAddress;
    //QString serverPortValue;
    QString threshValue;
    QString cameraNumValue;
//    QString argFileName;
    qreal linearjumpdistmult;
    bool linearjumpset = false;
    int optionInd = 1;
    while (optionInd < arguments.length()) {
        if ((arguments.at(optionInd).compare("-resolutionx",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            resValueX = arguments.at(optionInd+1);
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-resolutiony",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            resValueY = arguments.at(optionInd+1);
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-trodesConfig",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            trodesConfigFile = arguments.at(optionInd+1);
            // parse the config file
            //nsParseTrodesConfig(trodesConfigFile);
            QString errorString = loadedWorkspace.readTrodesConfig(trodesConfigFile);
            if (!errorString.isEmpty()) {
                qDebug() << errorString;
            }
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-serverAddress",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            serverAddress = arguments.at(optionInd+1);
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-serverPort",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            serverPortValue= arguments.at(optionInd+1);
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-threshold",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            threshValue = arguments.at(optionInd+1);
            optionInd++;
        } else if ((arguments.at(optionInd).compare("-cameraNum",Qt::CaseInsensitive)==0) && (arguments.length() > optionInd+1)) {
            cameraNumValue = arguments.at(optionInd+1);
            optionInd++;
        } else if((arguments.at(optionInd).compare("-playback", Qt::CaseInsensitive)==0)){
            inputFileName = arguments.at(optionInd+1);
            inputFileOpen = true;
            optionInd++;
        } else if((arguments.at(optionInd).compare("-ptpEnabled", Qt::CaseInsensitive)==0)){
            ptpChecksEnabled = true;
        }
        else if((arguments.at(optionInd).compare("-linearJumpDistMult", Qt::CaseInsensitive)==0)&& (arguments.length() > optionInd+1)){
            linearjumpdistmult = arguments.at(optionInd+1).toDouble();
            linearjumpset = true;
            optionInd++;
        } else if( (arguments.at(optionInd).compare("-useFileTracking", Qt::CaseInsensitive) ==0) && (arguments.length() > optionInd+1) ) {
            trackSettings.usePositionFile = arguments.at(optionInd+1).toInt();
            trackSettings.useLinearPositionFile = arguments.at(optionInd+1).toInt();
            optionInd++;
        }
        optionInd++;
    }

    if ((!resValueY.isEmpty()) && (!resValueX.isEmpty()))   {
        bool ok1;
        bool ok2;
        resX = resValueX.toInt(&ok1);
        resY = resValueX.toInt(&ok2);
        if (!ok1 || !ok2) {
            //Conversion to int didn't work
            resX = -1;
            resY = -1;
        }
    } else {
        resX = -1;
        resY = -1;
    }

    if (!threshValue.isEmpty()) {
        bool ok1;
        int tmpCurrentThresh = threshValue.toInt(&ok1);
        if (ok1) {
            trackSettings.currentThresh = tmpCurrentThresh;
        }
    }

    if (!cameraNumValue.isEmpty()) {
        bool ok1;
        int tmpCameraNum = cameraNumValue.toInt(&ok1);
        if (ok1) {
            cameraNum = tmpCameraNum;
        }
    }


    //------------------------------------------



    bool connectionSuccess = false;

    if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {
        /*
        Module connection protocol:
        1) Trodes (or other master GUI) launches module X, or module X is started directly by the user
        2) Module X defines the provided and needed datatypes
        3) Module X creates a client and connects to Trodes (or the master GUI)
        4) When trodes responds with a module ID, a module server is automatically started if the module has any data available
        5) Module X gives Trodes it’s DataTypesAvailable structure, and it’s data server address
        6) Trodes fills its DataAvailable table entries with the given data, and checks that there are no repeats with other modules
        7) Trodes sends the current DataAvailable list to all currently connected modules.
        ...............
        8) Each module starts one client per needed data type and connects to the proper server
        9) When the module is ready to go (in this case, the camera is streaming), it calls sendModuleIsReady()
        */

        // Create the  moduleNet structure. This is required for all modules
        moduleNet = new TrodesModuleNetwork();
        // Define the data type that this module provides
        moduleNet->dataProvided.dataType = TRODESDATATYPE_POSITION;
        if ((!serverAddress.isEmpty()) &&  (!serverPortValue.isEmpty())) {
            //The address of the server was specified in the command line,
            //so we use that
            connectionSuccess = moduleNet->trodesClientConnect(serverAddress,serverPortValue.toUInt(), true);

        } else {
            //Try to find an existing Trodes server or look in config file.
            connectionSuccess = moduleNet->trodesClientConnect(true);

        }
    } else if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::zmq_based) {

        //We are using the newer ZMQ-based network
        if ((!serverAddress.isEmpty()) &&  (!serverPortValue.isEmpty())) {
            //The address of the server was specified in the command line,

            connectionSuccess = startTrodesNetworkClient(serverAddress, serverPortValue.toInt());

        } else {
            //TODO: default settings?

        }

    }

    //connectionSuccess = true;

    if (!connectionSuccess) {
        qDebug() << "Starting in standalone mode";
        currentOperationMode = CAMERAMODULE_STANDALONEMODE | CAMERAMODULE_FILEPLAYBACKMODE;
    }
    else if(inputFileOpen){
        qDebug() << "Starting in slave + file playback mode";
        currentOperationMode = CAMERAMODULE_SLAVEMODE | CAMERAMODULE_FILEPLAYBACKMODE;
    }
    else {
        qDebug() << "Starting in slave + streaming mode";
        //For now, we always start in camera streaming mode.  However, file playback is in the plans...
        currentOperationMode = CAMERAMODULE_SLAVEMODE | CAMERAMODULE_CAMERASTREAMINGMODE;
    }


//--------------------------------------------------------------------------------

    //Top control panel setup----------------------------------
    int numRightHandButtons = 6;
    int numLeftHandButtons = 2;
    int numButtons = numRightHandButtons + numLeftHandButtons;
    QHBoxLayout* headerLayout = new QHBoxLayout(); //contains the buttons and clock at the top of the screen
    headerLayout->setContentsMargins(QMargins(0,5,0,0));
//    headerLayout->setHorizontalSpacing(3);

    //Time display
    QTime mainClock(0,0,0,0);
    QFont labelFont;
    labelFont.setPixelSize(20);
    labelFont.setFamily("Arial");
    //timeLabel  = new QLabel;
    timeLabel = new QLineEdit;
    timeLabel->setFrame(false);
    //timeLabel->setStyleSheet("QLineEdit { border: none }");
    timeLabel->setStyleSheet("* { background-color: rgba(0,0,0,0); }");
    timeLabel->setText(mainClock.toString("hh:mm:ss"));
    timeLabel->setFont(labelFont);
    timeLabel->setReadOnly(true);
    connect(timeLabel,SIGNAL(editingFinished()),this,SLOT(newTimeEntered()));


    QFontMetrics fm(labelFont);
    int pixelsWide = fm.horizontalAdvance(mainClock.toString("hh:mm:ss"))+10;
    int pixelsHigh = fm.height();

    timeLabel->setFixedSize(pixelsWide, pixelsHigh);

    //timeLabel->setMinimumWidth(100);
    timeLabel->setAlignment(Qt::AlignLeft);
    //pullTimer = new QTimer(this);
    //connect(pullTimer, SIGNAL(timeout()), this, SLOT(updateTime()));
    //pullTimer->start(100); //update timer every 100 ms

    settingsButton = new TrodesButton();
    settingsButton->setCheckable(false);
    settingsButton->setFocusPolicy(Qt::NoFocus);
    settingsButton->setText("Settings");
    connect(settingsButton,SIGNAL(clicked()),this,SLOT(settingsButtonPressed()));
    //threshButton->setFixedSize(70,20);

    eventButton = new TrodesButton();
    eventButton->setText("Events");
    eventButton->setCheckable(false);
    eventButton->setFocusPolicy(Qt::NoFocus);
    connect(eventButton,SIGNAL(clicked()),this,SLOT(eventButtonPressed()));

    sourceButton = new TrodesButton();
    sourceButton->setCheckable(false);
    sourceButton->setFocusPolicy(Qt::NoFocus);

    if (currentOperationMode & CAMERAMODULE_CAMERASTREAMINGMODE) {
        sourceButton->setText("Source");

    } else {
        sourceButton->setText(" File ");

    }
    connect(sourceButton,SIGNAL(clicked()),this,SLOT(sourceButtonPressed()));
    //sourceButton->setFixedSize(70,20);

    toolsButton = new TrodesButton();
    toolsButton->setCheckable(false);
    toolsButton->setFocusPolicy(Qt::NoFocus);
    connect(toolsButton,SIGNAL(clicked()),this,SLOT(toolsButtonPressed()));
    //connect(toolsButton,SIGNAL(released()),this,SLOT(toolsButtonReleased()));

    toolsButton->setText("Tools");


    trackButton = new TrackingButton();
    trackButton->setCheckable(true);
    trackButton->setFocusPolicy(Qt::NoFocus);

    if (currentOperationMode & CAMERAMODULE_CAMERASTREAMINGMODE || currentOperationMode == (CAMERAMODULE_SLAVEMODE|CAMERAMODULE_FILEPLAYBACKMODE)) {
        trackButton->setText("Track");
    } else {
        trackButton->setText("Log");
        trackButton->setRedDown(true);
        trackButton->setEnabled(false);
    }
    connect(trackButton,SIGNAL(clicked()),this,SLOT(trackingButtonPressed()));
    //connect(trackButton,SIGNAL(released()),this,SLOT(trackingButtonReleased()));

    //Record timer
    recordTimer = new QElapsedTimer;
    msecRecorded = 0;

    //pause and play buttons

    pauseButton = new TrodesButton();
    playButton = new TrackingButton();


    QPixmap playPixmap(":/buttons/playImage.png");
    QPixmap pausePixmap(":/buttons/pauseImage.png");

    QIcon pauseButtonIcon(pausePixmap);
    QIcon playButtonIcon(playPixmap);

    pauseButton->setIcon(pauseButtonIcon);
    playButton->setIcon(playButtonIcon);

    pauseButton->setFocusPolicy(Qt::NoFocus);
    playButton->setFocusPolicy(Qt::NoFocus);

    pauseButton->setIconSize(QSize(10,10));
    playButton->setIconSize(QSize(15,15));

    pauseButton->setFixedHeight(20);
    playButton->setFixedHeight(20);
    //pauseButton->setFixedSize(50,20);
    //playButton->setFixedSize(50,20);
    pauseButton->setObjectName("Pause button");
    playButton->setObjectName("Play button");
    pauseButton->setToolTip(tr("Pause"));
    playButton->setToolTip(tr("Play file"));

    pauseButton->setVisible(false);
    playButton->setVisible(false);
    playButton->setCheckable(true);


    frameInRange = new QLabel(this);
    frameInRange->setObjectName("Frameinrange");
    frameInRange->setStyleSheet("QLabel{color: red;}");
    frameInRange->setVisible(false);
    frameInRange->setEnabled(false);



//    headerLayout->setColumnStretch(numLeftHandButtons,1);

    headerLayout->addWidget(playButton, 0, Qt::AlignTop);
    headerLayout->addWidget(pauseButton, 0, Qt::AlignTop);
    headerLayout->addWidget(frameInRange, 0, Qt::AlignTop);
    headerLayout->addWidget(new QWidget, 1);
    headerLayout->addWidget(eventButton,0, Qt::AlignTop);
    headerLayout->addWidget(trackButton, 0, Qt::AlignTop);
    headerLayout->addWidget(toolsButton,0, Qt::AlignTop);
    headerLayout->addWidget(sourceButton,0, Qt::AlignTop);
    headerLayout->addWidget(settingsButton,0, Qt::AlignTop);
    headerLayout->addWidget(timeLabel,0, Qt::AlignTop);
    mainLayout->addLayout(headerLayout,0,0);

    if (displayDebugOut) {
        qDebug() << "Buttons added to layout";
    }

    QGridLayout* fileControlLayout = new QGridLayout(); //contains the buttons and clock at the top of the screen
    fileControlLayout->setContentsMargins(QMargins(0,0,0,0));
    fileControlLayout->setHorizontalSpacing(5);
    labelFont.setPixelSize(10);
    fileStartTimeLabel = new QLineEdit();
    fileStartTimeLabel->setFocusPolicy(Qt::NoFocus);

    //fileStartTimeLabel->setE
    fileStartTimeLabel->setStyleSheet("* { background-color: rgba(0,0,0,0); "
                                      "    border: none}");
    fileStartTimeLabel->setText("00:00:00");
    fileStartTimeLabel->setFont(labelFont);
    fileStartTimeLabel->setFixedHeight(10);
    fileStartTimeLabel->setFixedWidth(50);
    connect(fileStartTimeLabel,SIGNAL(editingFinished()),this,SLOT(newStartTimeEntered()));


    fileControlLayout->addWidget(fileStartTimeLabel,0,0);

    filePositionSlider = new MySlider();
    filePositionSlider->setFocusPolicy(Qt::NoFocus);
    filePositionSlider->setOrientation(Qt::Horizontal);
    filePositionSlider->setFixedHeight(20);
    filePositionSlider->setMinimum(0);
    filePositionSlider->setMaximum(TIMESLIDERSTEPS);
    filePositionSlider->setSingleStep(1);
    //filePositionSlider->setStyle(new MyStyle(filePositionSlider->style()));

    connect(filePositionSlider,SIGNAL(sliderMoved(int)),this,SLOT(playbackSliderPositionChanged(int)));
    connect(filePositionSlider,SIGNAL(sliderPressed()),this,SLOT(playbackSliderPressed()));
    connect(filePositionSlider,SIGNAL(sliderReleased()),this,SLOT(playbackSliderReleased()));
    fileControlLayout->addWidget(filePositionSlider,0,1,Qt::AlignTop);
    fileControlLayout->setColumnStretch(1, 1);


    fileEndTimeLabel = new QLineEdit();
    fileEndTimeLabel->setFocusPolicy(Qt::NoFocus);
    fileEndTimeLabel->setStyleSheet("* { background-color:rgba(0,0,0,0); "
                                      "    border: none}");
    fileEndTimeLabel->setText("00:00:00");
    fileEndTimeLabel->setFont(labelFont);
    fileEndTimeLabel->setFixedHeight(10);
    fileEndTimeLabel->setFixedWidth(50);
    connect(fileEndTimeLabel,SIGNAL(editingFinished()),this,SLOT(newEndTimeEntered()));
    fileControlLayout->addWidget(fileEndTimeLabel,0,2);

    //File controls start off disabled
    fileStartTimeLabel->setVisible(false);
    filePositionSlider->setVisible(false);
    fileEndTimeLabel->setVisible(false);

    mainLayout->addLayout(fileControlLayout,1,0);

    TrodesFont dispFont;
    //File information layout (filename opened, and if recording, time recorded)
    QGridLayout *fileinfoLayout = new QGridLayout;
    fileInfoLabel = new QLabel();
    fileInfoLabel->setFont(dispFont);
    fileinfoLayout->addWidget(fileInfoLabel, 0, 0);
    fileInfoLabel->setVisible(false);
    fileInfoLabel->setEnabled(false);

    fileStatusColorIndicator = new QLabel();
    fileStatusColorIndicator->setStyleSheet("QLabel { background-color : yellow; color : black; border-radius: 5px}");
    fileStatusColorIndicator->setFont(dispFont);
    fileinfoLayout->addWidget(fileStatusColorIndicator, 0, 1, Qt::AlignTop);
    fileStatusColorIndicator->setVisible(false);
    fileStatusColorIndicator->setEnabled(false);
    fileStatusColorIndicator->setAlignment(Qt::AlignCenter);

    mainLayout->addLayout(fileinfoLayout, 2, 0);

    if (displayDebugOut) {
        qDebug() << "File info added to layout";
    }

    //Statusbar setup---------------------------
    QString tmpStatus = "";
    //if (!trodesConfigValue.isEmpty()) tmpStatus.append("Trodesconfig: " + trodesConfigValue + " ");
    //if (!resValueY.isEmpty()) tmpStatus.append("Resolution: " + resValueY + " by "+ resValueX + " ");

    statusbar = new QStatusBar(this);
    statusbar->setFont(dispFont);
    setStatusBar(statusbar);
    statusbar->showMessage(tmpStatus);

    if (displayDebugOut) {
        qDebug() << "Status bar added to layout";
    }


    //Playback controls
    if(currentOperationMode & CAMERAMODULE_SLAVEMODE  && currentOperationMode & CAMERAMODULE_FILEPLAYBACKMODE){
        if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {
            connect(this,SIGNAL(signal_sendPlaybackCommand(uint8_t,uint32_t)),moduleNet->trodesClient,SLOT(sendPlaybackCommand(uint8_t,uint32_t)));
            connect(moduleNet->trodesClient, SIGNAL(playbackCommandReceived(qint8,qint32)), this, SLOT(processPlaybackCommand(qint8, qint32)));
        }

        connect(this, &MainWindow::signal_sendPlaybackCommand, this, &MainWindow::sendAcquisitionCommand);

        connect(pauseButton,SIGNAL(clicked()),this,SLOT(pausePlaybackSignal()));
        connect(playButton,SIGNAL(clicked()),this,SLOT(playPlaybackSignal()));
        statusbar->showMessage("File playback synced with Trodes");
    }
    else{
        connect(pauseButton,SIGNAL(pressed()),this,SLOT(pauseButtonPressed()));
        connect(playButton,SIGNAL(pressed()),this,SLOT(playButtonPressed()));
        connect(pauseButton,SIGNAL(released()),this,SLOT(pauseButtonReleased()));
        connect(playButton,SIGNAL(released()),this,SLOT(playButtonReleased()));
    }



    panelSplitter = new QSplitter();
    panelSplitter->setOrientation(Qt::Vertical);

    graphicsWindow = new GraphicsWindow(this);

    //MARK: refactor
    if (connectionSuccess && networkClient){
        connect(graphicsWindow, &GraphicsWindow::sig_broadcastMsg, this, &MainWindow::signal_broadcastMsg);
//        qDebug() << "---network initialized and connected to graphicswin";
    }
    else{
//        qDebug() << "---error network not initialized";
    }

    graphicsWindow->setFocusPolicy(Qt::NoFocus);
    panelSplitter->addWidget(graphicsWindow);
    //QGraphicsView *view = new QGraphicsView(graphicsWindow);

    //displayWindow = new VideoDisplayWindow(NULL);

#ifdef ARAVIS
    GigEVideoDisplayController* gige_video_cntl = new GigEVideoDisplayController(this, displayWindow, networkClient.get(), encoder);
#else

    if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {
        videoController = new VideoDisplayController(this,displayWindow,moduleNet->trodesClient);
    } else if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::zmq_based) {
        videoController = new VideoDisplayController(
                    this, displayWindow, networkClient.get(),
                    loadedWorkspace);
    }

    if (displayDebugOut) {
        qDebug() << "Video controller added";
    }

    if (currentOperationMode&CAMERAMODULE_CAMERASTREAMINGMODE) {

        menuResReduce->setEnabled(true);
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
        settings.beginGroup(QLatin1String("processor"));
        resReduceSetting = settings.value(QLatin1String("res_reduce"),-1).toInt();
        if (resReduceSetting == 480) {
            setResReduce480();
        } else if (resReduceSetting == 720) {
            setResReduce720();
        } else if (resReduceSetting == 1080) {
            setResReduce1080();
        } else {
            setResReduceNone();
        }
        settings.endGroup();
    } else {
        menuResReduce->setEnabled(false);
        resReduceSetting = -1;
    }

    videoController->setPTPCheck(ptpChecksEnabled);
    if(linearjumpset)
        videoController->setLinearJumpDistanceMultiplier(linearjumpdistmult);

    connect(videoController,SIGNAL(videoStreamStart(int,int)),this,SLOT(videoStarted(int,int)));
    connect(videoController,SIGNAL(processorCreated()),this,SLOT(connectProcessor()));

    connect(videoController,SIGNAL(newAnimalPos(quint32)),this,SLOT(sendNewAnimalPosition(quint32)));
    connect(videoController,SIGNAL(filePlaybackReady()),this,SLOT(activatePlaybackControls()));
    connect(videoController,SIGNAL(filePlaybackReady()),this,SLOT(pausePlaybackSignal())); //When a playback file is opened, tell Trodes to pause if already playing
    connect(videoController,SIGNAL(filePlaybackClosed()),this,SLOT(turnOffPlayback()));
    connect(videoController,SIGNAL(videoFileTimeRate(quint32)),this,SLOT(setTimeRate(quint32)));
    connect(videoController,SIGNAL(newVideoTimeStamp(quint32)),this,SLOT(setTime(quint32)));
    connect(videoController,SIGNAL(videoFileTimeRange(quint32,quint32)),this,SLOT(setVideoTimeRange(quint32,quint32)));
    connect(videoController,SIGNAL(filePlaybackStopped()),this,SLOT(setFilePlaybackStopped()));
    connect(videoController,SIGNAL(getUserTrackingInput()),this,SLOT(getUserInput()));
    connect(videoController,SIGNAL(sig_newSliderRange(quint32,quint32)),this,SLOT(setSliderTimeRange(quint32,quint32)));
    connect(videoController,SIGNAL(signal_nextFileNeeded()),this,SLOT(nextFileRequested()));
    connect(videoController,SIGNAL(pausedForBadLoc()),this,SLOT(enableTimeEdit()));
    connect(videoController,SIGNAL(sig_saveAllGeometry(QString)), graphicsWindow,SLOT(saveAllGeometry(QString)));
    connect(videoController, &VideoDisplayController::sig_ptp_fullyInitialized, this, &MainWindow::ptpFullyInitialized);
    connect(videoController, &VideoDisplayController::sig_ptp_tryingToInitialize, this, &MainWindow::ptpTryingToInitialize);
    connect(videoController, &VideoDisplayController::sig_ptp_calibrating, this, &MainWindow::ptpCalibrating);
    connect(videoController, &VideoDisplayController::sig_ptp_notProperlySetup, this, &MainWindow::ptpNotProperlySetup);
    connect(videoController, &VideoDisplayController::sig_ptp_wrongconfiguration, this, &MainWindow::ptpWrongConfiguration);
    connect(videoController, &VideoDisplayController::sig_ptp_warningmessagebox, this, &MainWindow::ptpWarningMessageBox);
    connect(this, &MainWindow::signal_playbackSeekToTime, videoController, &VideoDisplayController::playbackSeekToTime);
    connect(this, &MainWindow::signal_setStopUponTackingError, videoController, &VideoDisplayController::setStopPlaybackUponTrackingError);



    connect(filePositionSlider,SIGNAL(newRange(int,int)),videoController,SLOT(newSliderRange(int,int)));
    connect(graphicsWindow,SIGNAL(userInput1(QPoint)),videoController,SLOT(userInput1(QPoint)));
    connect(graphicsWindow,SIGNAL(userInput2(QPoint)),videoController,SLOT(userInput2(QPoint)));
    connect(graphicsWindow,SIGNAL(linearGeometryExists(bool)),this,SLOT(setEnableLinearGeometrySave(bool)));
    connect(graphicsWindow,SIGNAL(rangeGeometryExists(bool)),this,SLOT(setEnableRangeGeometrySave(bool)));
    connect(graphicsWindow,SIGNAL(zoneGeometryExists(bool)),this,SLOT(setEnableZoneGeometrySave(bool)));
    connect(graphicsWindow,SIGNAL(incExclGeometryExists(bool)),this,SLOT(setEnableIncExclGeometrySave(bool)));
    connect(graphicsWindow,SIGNAL(linearGeometryExists(bool)),videoController,SLOT(linearGeometryExists(bool)));
    //MARK: add specific graphicsWindow signals here to detect whether or not specific geometry exists.
    connect(graphicsWindow,SIGNAL(newLinearGeometry(QVector<QPointF>,QVector<LineNodeIndex>)),videoController,SLOT(newLinearGeometry(QVector<QPointF>,QVector<LineNodeIndex>)));
    //data packet transmission connections, only do this if connections are made
    connect(graphicsWindow,SIGNAL(sendNewDataPacket(dataPacket)),this,SLOT(addDataPacket(dataPacket)));
    connect(videoController,SIGNAL(sendDataPacket(dataPacket)),this,SLOT(addDataPacket(dataPacket)));
    //send the calculated pixel scale from the graphicsWindow to the VideoController
    connect(graphicsWindow, SIGNAL(pixelScaleChanged(double)), videoController, SIGNAL(sig_pixelScaleReceived(double)));
    connect(videoController,SIGNAL(signal_logging(bool)),graphicsWindow,SLOT(setLogging(bool)));

    //MARK: event system
    connect(graphicsWindow,SIGNAL(broadcastEvent(TrodesEventMessage)), this, SLOT(broadcastEvent(TrodesEventMessage)));
    connect(graphicsWindow,SIGNAL(broadcastNewEventReq(QString)), this, SLOT(broadcastNewEventReq(QString)));
    connect(graphicsWindow,SIGNAL(broadcastRemoveEventReq(QString)), this, SLOT(broadcastRemoveEventReq(QString)));


        //connect(videoController->imageProcessor,SIGNAL(sendDataPacket(dataPacket)),this,SLOT(addDataPacket(dataPacket)));
    //connect(videoController->imageProcessor,SIGNAL(fileOpened()),this,SLOT(setFileOpen()));
    connect(this,SIGNAL(signal_startRecording()),videoController,SLOT(startRecording()));
    connect(this,SIGNAL(signal_stopRecording()),videoController,SLOT(stopRecording()));
    connect(this,SIGNAL(signal_createFile(QString)),videoController,SIGNAL(signal_createFile(QString)));
    connect(this,SIGNAL(signal_closeFile()),videoController,SIGNAL(signal_closeFile()));
    connect(this, SIGNAL(signal_seekRelativeFrame(int)), videoController, SLOT(seekRelativeFrame(int)));
    connect(this, SIGNAL(signal_newTimeRange(quint32, quint32)), videoController, SLOT(newTimeRange(quint32,quint32)));
    connect(this, SIGNAL(signal_seekToTime(quint32)), videoController, SLOT(seekToTime(quint32)));
//    if(!(currentOperationMode&CAMERAMODULE_FILEPLAYBACKMODE && currentOperationMode&CAMERAMODULE_SLAVEMODE)){

    connect(this,SIGNAL(signal_startPlayback()),videoController,SLOT(startPlayback()));
    connect(this,SIGNAL(signal_startFastPlayback()),videoController,SLOT(startFastPlayback()));
    connect(this,SIGNAL(signal_pausePlayback()),videoController,SLOT(pausePlayback()));
    connect(this, SIGNAL(signal_stepFrameForward()),videoController,SLOT(stepForward()));
    connect(this, SIGNAL(signal_stepFrameBackward()),videoController,SLOT(stepBackward()));
//    }
    connect(this,SIGNAL(signal_createPlaybackLogFile(QString)),videoController,SIGNAL(signal_createPlaybackLogFile(QString)));
    connect(this,SIGNAL(signal_closePlaybackLogFile()),videoController,SIGNAL(signal_closePlaybackLogFile()));

    connect(this,SIGNAL(signal_newSettings(TrackingSettings)),videoController,SIGNAL(signal_newSettings(TrackingSettings)));


    //The video controller is placed in a separate thread to make sure that it is not slowed down by the GUI
    QThread *videoControllerThread = new QThread;
    videoControllerThread->setObjectName("VideoController");
    connect(videoControllerThread,SIGNAL(started()),videoController,SLOT(startController()));

    if (connectionSuccess && currentOperationMode&CAMERAMODULE_CAMERASTREAMINGMODE) {

        //If we are connected to Trodes, start the first camera found automatically.
        //TODO: if Trodes is in playback mode, we want to open a file instead
        videoController->setLiveCameraMode(true);


    }

    connect(videoController, SIGNAL(finished()), videoControllerThread, SLOT(quit()));
    connect(videoController, SIGNAL(finished()), videoController, SLOT(deleteLater()));
    connect(videoControllerThread, SIGNAL(finished()), videoControllerThread, SLOT(deleteLater()));
    videoController->moveToThread(videoControllerThread);
    videoControllerThread->start(QThread::HighestPriority);

    if (displayDebugOut) {
        qDebug() << "Video Controller thread started";
    }


#endif

#ifdef PYTHONEMBEDDED

    if (usePython) {
    mainContext = PythonQt::self()->getMainModule();
    pyConsole = new PythonQtScriptingConsole(this, mainContext);
    posData_toPython.setContext(&mainContext);
    //mainLayout->addWidget(pyConsole,3,0);
    panelSplitter->addWidget(pyConsole);

    //PythonQt::self()->addSysPath("D/Windows/Program Files/trodes/bin/python/Python27/Lib");

    // add our custom QObject to the namespace of the main python context
    //mainContext.addObject("cameraModule", &pyInterface);
    mainContext.evalScript("import sys\n");
    //MARK: change before push
    QFile inputFile("pythonPath.txt");
    QFileInfo pathFileInfo("pythonPath.txt");
    //QFile inputFile("/Users/maris/Programming/Work/Trodes/trodes/Modules/cameraModule/src/pythonPath.txt"); //++
    //QFileInfo pathFileInfo("/Users/maris/Programming/Work/Trodes/trodes/Modules/cameraModule/src/pythonPath.txt"); //++
    if (pathFileInfo.exists()) {

        QDir pathFileDir(pathFileInfo.absoluteDir());
        QString tempPath;
        if (inputFile.open(QIODevice::ReadOnly))
        {
           QTextStream in(&inputFile);
           while (!in.atEnd())
           {
              QString line = in.readLine();
              if ((!line.startsWith("PYTHONPATH")) && (!line.startsWith("#")) )  {
                qDebug() << "Adding to python path: " << pathFileDir.absoluteFilePath(line);
                mainContext.evalScript("sys.path.append('" + pathFileDir.absoluteFilePath(line) + "')\n");
              }
           }
           inputFile.close();
        }

    } else {
        qDebug() << "pythonPath.txt not found";
    }
    mainContext.evalScript("import os\n");

    mainContext.evalScript("sys.path.append(os.getcwd())\n");

    if (!pythonDir.isEmpty()) {
        mainContext.evalScript("sys.path.append('"+pythonDir+"')\n");
    }


    mainContext.addObject("position", posData_toPython.dataBuffer);

    // evaluate a python file embedded in executable as resource:
    //mainContext.evalFile("pyCameraModule.py");
    // create an object, hold onto its reference
    //PythonQtObjectPtr tag = mainContext.evalScript("PosHandler()\n", Py_eval_input);
    //Q_ASSERT(!tag.isNull());
    //tag.call("setPosVar", QVariantList() << "position");
    //QVariant fn = tag.call("fileName", QVariantList());

    //pyConsole->hide();



    //console.show();
    }
#endif

    if (connectionSuccess) {
        if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {


            connect(moduleNet->trodesClient,SIGNAL(openFileEventReceived(QString)),this,SLOT(setupFile(QString)));
            connect(moduleNet->trodesClient,SIGNAL(timeRateReceived(quint32)),this,SLOT(setTimeRate(quint32)));
            connect(moduleNet->trodesClient,SIGNAL(startAquisitionEventReceived()),this,SLOT(startRecording()));
            connect(moduleNet->trodesClient,SIGNAL(stopAquisitionEventReceived()),this,SLOT(stopRecording()));
            connect(moduleNet->trodesClient,SIGNAL(closeFileEventReceived()),this,SLOT(closeFile()));
            connect(moduleNet->trodesClient,SIGNAL(quitCommandReceived()),this,SLOT(close()));
            connect(moduleNet->trodesClient,SIGNAL(instanceReveivedFromServer(int)),this,SLOT(setModuleInstance(int)));
            connect(moduleNet->trodesClient, SIGNAL(currentTimeReceived(quint32)), this, SLOT(setTime(quint32)));
            moduleNet->trodesClient->sendTimeRateRequest(); //We need to know the time base from the master process (Trodes or StateScript GUI)


        } else if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::zmq_based) {
            qDebug() << "trodesnetwork: started cameramodule acquisition subscriber thread";
            // any time there is a new event, process it

            connect(networkClient.get(), &CameraModuleClient::sig_cmd_openFile, this, &MainWindow::setupFile);
            connect(networkClient.get(), &CameraModuleClient::sig_cmd_timerate, this, &MainWindow::setTimeRate);
            connect(networkClient.get(), &CameraModuleClient::sig_cmd_acquisition, this, &MainWindow::aquisitionCommandReceived);
            connect(networkClient.get(), &CameraModuleClient::sig_cmd_closeFile, this, &MainWindow::closeFile);
            connect(networkClient.get(), &CameraModuleClient::sig_cmd_quit, this, &MainWindow::close);
            connect(networkClient.get(), &CameraModuleClient::sig_cmd_time, this, &MainWindow::setTime);
            connect(this, &MainWindow::signal_sendAquisitionCommand, networkClient.get(), &CameraModuleClient::qtSendPlaybackCommand);
            //        connect(this, &MainWindow::signal_broadcastMsg, networkClient.get(), &CameraModuleClient::qtSendStream);
            //networkClient->sendTimeRateRequest();

            //CameraModule client provides these events
            networkClient->qtProvideEvent(CAMERA_2DPOS);
            networkClient->qtProvideEvent(CAMERA_LIN);
            networkClient->qtProvideEvent(CAMERA_LIN_TRACK);
            networkClient->qtProvideEvent(CAMERA_VELOCITY);
            networkClient->qtProvideEvent(CAMERA_ZONE);
            networkClient->qtProvideEvent(CAMERA_PIXELPERCM);

            connect(this, &MainWindow::signal_broadcastMsg, networkClient.get(), &CameraModuleClient::qtSendEvent);

            //Parse network id to see what instance this cameramodule is
            QString idStr = QString::fromStdString(networkClient->getID());
            QStringList idStrL = idStr.split(".");
            if (idStrL.length() <= 1)
                setModuleInstance(1);
            else {
                idStr = idStrL.last();
                setModuleInstance(idStr.toInt());
            }
        }

    }

    if (displayDebugOut) {
        qDebug() << "Network connections complete";
    }

    //mainLayout->addWidget(graphicsWindow,2,0);
    mainLayout->addWidget(panelSplitter,4,0);
    mainLayout->setRowStretch(3,1);
    mainLayout->setRowStretch(5,1);

    //graphicsWindow->show();

    QWidget *window = new QWidget();
    window->setLayout(mainLayout);
    setCentralWidget(window);


    if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {
        moduleNet->sendModuleName("Camera");
    }

    qDebug() << "Camera module started";



    graphicsWindow->initializeEvents();

    iniActionList();
    eventHandler = new EventHandler(actionList, eventButton);
    //MARK: event
    //MARK: refactor TODO incorporate into event system
    eventHandler->setUpConnections(networkClient.get());
    connect(eventHandler,SIGNAL(sig_executeAction(int,TrodesEvent)), this, SLOT(callMethod(int,TrodesEvent)));
    trackSettings.currentOperationMode = currentOperationMode;

    /*
    if (currentOperationMode & CAMERAMODULE_SLAVEMODE) {

        trackSettings.ringOn = false;
    }*/
    newSettings(trackSettings);
    setTracking(trackingOn);

    setCurrentTool(1); //User input tool
    graphicsWindow->setTool(1);

    if (currentOperationMode&CAMERAMODULE_FILEPLAYBACKMODE && currentOperationMode&CAMERAMODULE_STANDALONEMODE) {
//        qDebug() << "tracking and tools set";

        setTracking(true);
        trackButton->setDown(false);
    }

    if ((loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) && (connectionSuccess)) {
        //Ask the master module what state it should be in (saving, etc)
        moduleNet->sendCurrentStateRequest();
    }

    if ((loadedWorkspace.networkConf.networkType == NetworkConfiguration::zmq_based) && (connectionSuccess)) {
        networkClient->sendTimeRateRequest();
    }




    //setThresh(trackSettings.currentThresh,trackSettings.trackDark);
    //setRing(trackSettings.currentRingSize,trackSettings.ringOn);

    //QMetaObject::connectSlotsByName(this);
    //retranslateUi();
}

MainWindow::~MainWindow() {
    endTrodesNetworkClient();

}

void MainWindow::connectProcessor() {
    //Once the VideoController thread has started, the image processor is created and this slot is called
    //connect(videoController->imageProcessor,SIGNAL(newImage_signal(QImage*)),graphicsWindow->dispWin,SLOT(newImage(QImage*)));


    if (currentOperationMode&CAMERAMODULE_CAMERASTREAMINGMODE) {

        connect(this,SIGNAL(signal_newResReduceSetting(int)),videoController->imageProcessor,SLOT(setResReduce(int)));

        if (resReduceSetting == 480) {
            setResReduce480();
        } else if (resReduceSetting == 720) {
            setResReduce720();
        } else if (resReduceSetting == 1080) {
            setResReduce1080();
        } else {
            setResReduceNone();
        }

    }
    connect(videoController->imageProcessor,&VideoImageProcessor::newImage_signal,graphicsWindow->dispWin,&VideoDisplayWindow::newImage);

    connect(videoController->imageProcessor,&VideoImageProcessor::fileOpened,this,&MainWindow::setFileOpenedStatus);
    connect(videoController->imageProcessor,&VideoImageProcessor::recordingStarted,this,&MainWindow::setRecordStatusOn);
    connect(videoController->imageProcessor,&VideoImageProcessor::recordingStopped,this,&MainWindow::setRecordStatusOff);
    //mark: export connect file open/close signals
    connect(this,SIGNAL(signal_fileOpened()),graphicsWindow,SLOT(recordingStarted()));
    connect(this,SIGNAL(signal_closeFile()),graphicsWindow,SLOT(recordingStopped()));
    connect(videoController->imageProcessor,SIGNAL(newLocation(QPoint)),graphicsWindow,SLOT(newLocation(QPoint))); //For one-point tracking
    connect(videoController->imageProcessor,SIGNAL(newLocation(QPoint,QPoint,QPoint)),graphicsWindow,SLOT(newLocation(QPoint,QPoint,QPoint))); //For 2-led tracking
    connect(videoController->imageProcessor,SIGNAL(newLinearLocation(QPoint)),graphicsWindow,SLOT(newLinearLocation(QPoint)));
    connect(graphicsWindow,&GraphicsWindow::newIncludeCalculation,videoController->imageProcessor,&VideoImageProcessor::setIncludedPixels);
    connect(graphicsWindow,&GraphicsWindow::linearGeometryAnchorNodeSet,videoController->imageProcessor,&VideoImageProcessor::linearGeometryAnchorNodeSet);
    connect(graphicsWindow,&GraphicsWindow::linearGeometryLineZoneSet,videoController->imageProcessor,&VideoImageProcessor::linearGeometryLineZoneSet);
    //connect(videoController->imageProcessor,SIGNAL(sendDataPacket(dataPacket)),this,SLOT(addDataPacket(dataPacket)));

#ifdef PYTHONEMBEDDED
    if (usePython) {
        connect(videoController->imageProcessor,SIGNAL(newLocation(QPoint)),&posData_toPython,SLOT(writePoint(QPoint))); //For one-point tracking
        connect(videoController->imageProcessor,SIGNAL(newLocation(QPoint,QPoint,QPoint)),&posData_toPython,SLOT(writePoint(QPoint,QPoint,QPoint))); //For 2-led tracking
        connect(&posData_toPython,SIGNAL(sendVelocity(double)),this,SLOT(sendVelocity(double)));
    }
#endif
    //If we are in live stream mode, try to start the first camera found
    if (currentOperationMode&CAMERAMODULE_CAMERASTREAMINGMODE) {
        qDebug() << "Detecting cameras...";
        videoController->availableCameras();
        qDebug() << "Starting first camera found";
        QMetaObject::invokeMethod(videoController, "startFirstCameraFound");
    }

    //Look for playback file opened by Trodes, if provided
    if(currentOperationMode & CAMERAMODULE_FILEPLAYBACKMODE && currentOperationMode & CAMERAMODULE_SLAVEMODE && inputFileOpen){
        inputFileOpen = false; //Set to false, should be true through turnOnPlayback(), and proper connect()'s set in it

        //Open up playback file
        QFileInfo file(inputFileName);
        if(file.exists()){
            turnOnPlayback(file.absoluteFilePath()); //sets inputFileOpen
            trackButton->setEnabled(true);
            enableTimeEdit();
            filePositionSlider->hide();
        }
        if(!inputFileOpen)
            sourceButtonPressed();//calls turnOnPlayback()
    }

    //PTP
    if(currentOperationMode & CAMERAMODULE_FILEPLAYBACKMODE){

    }
    else{
        QTimer::singleShot(1, this, [this](){this->checkPTPFeatures();});
    }
}

void MainWindow::closeEvent(QCloseEvent* event) {
    if(recording){
        int ret = QMessageBox::warning(this, "Still recording",
                  "Warning! Closing will stop video recording. Close anyway? ",
                  "No", "Yes, stop recording");
        if(ret != 1){
            event->ignore();
            return;
        }
    }

    if(fileOpen && !programIsClosing){
        int ret = QMessageBox::warning(this, "File open",
                  "Warning! Video file has been opened for recording. Close anyway?",
                  "No", "Yes, close video file");
        if(ret != 1){
            event->ignore();
            return;
        }
    }

    if (!programIsClosing) {
        programIsClosing = true;
        recording = false;
        videoController->endProcessor(); //stops the image processor and camera input
        videoController->closeDown();
        connect(&closeTimer,SIGNAL(timeout()),this,SLOT(closeAfterDelay()));
        closeTimer.start(250);
        event->ignore();
    } else {
        if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {
            moduleNet->trodesClient->sendQuit();
            moduleNet->disconnectClient();
        }


        QThread::msleep(250);
        event->accept();

    }

    //event->accept();

}

void MainWindow::setModuleInstance(int instanceNum) {
    moduleInstance = instanceNum;
    qDebug() << "Camera Module instance " << moduleInstance;
    setWindowTitle(QString("Camera %1").arg(moduleInstance));
}


void MainWindow::addDataPacket(dataPacket packetToAdd) {
    //MARK: refactor publish these packets directly to the cameraModule stream
    if (packetToAdd.getType() != PPT_NULL) {
        positionData.append(packetToAdd);
    }
    else {
        qDebug() << "Error: tried to append invalid dataPacket (MainWindow::addDataPacket)";
    }
}

void MainWindow::sendVelocity(double velocity) {
    //qDebug() << "**Velocity recieved -" << velocity;
    dataPacket velocityPack(PPT_Velocity);
    dataSend vel(DT_qreal,velocity);
    velocityPack.insert(vel);
    addDataPacket(velocityPack);

    //MARK: refactor
    TrodesMsg msg("d", velocity);
    emit signal_broadcastMsg(CAMERA_VELOCITY, msg);
}

void MainWindow::setCurrentTool(int toolNum) {
    currentTool = toolNum;
}

void MainWindow::about() {
    QMessageBox::about(this, tr("About CameraModule"), tr(qPrintable(GlobalConfiguration::getVersionInfo())));
}

void MainWindow::closeAfterDelay() {
    close();
}

void MainWindow::setFilePlaybackStopped() {
    isVideoFilePlaying = false;
    pauseButtonPressed();
    pauseButtonReleased();
    if (!loggingOn) {
        filePositionSlider->setValue(0);
        emit signal_playbackSeekToTime(fileStartTime);
    }
}

void MainWindow::processPlaybackCommand(qint8 flg, qint32 timestamp){
    if((currentOperationMode & CAMERAMODULE_SLAVEMODE  && currentOperationMode & CAMERAMODULE_FILEPLAYBACKMODE)){
        switch(flg) {
        case PC_PAUSE: {
            pauseButtonPressed();
            pauseButtonReleased();
            break;
        }
        case PC_PLAY: {

            playButtonPressed();
            playButtonReleased();
            setTime(timestamp);
            break;
        }
        case PC_SEEK:{
            if(timestamp > fileEndTime){
                pauseButtonPressed();
                return;
            }
            if(timestamp < fileStartTime){
                pauseButtonPressed();
                return;
            }

            int v = TIMESLIDERSTEPS * (qreal)(timestamp-fileStartTime)/((qreal)fileEndTime-(qreal)fileStartTime);
            filePositionSlider->setValue(v);
            if(playButton->isDown()){
                playButtonPressed();
                setTime(timestamp);
            }
            emit signal_playbackSeekToTime(timestamp);
            break;
        }
        case PC_STOP: {
            setFilePlaybackStopped();
            break;
        }
        case PC_NULL: {
            //null command
            break;
        }
        default:
            qDebug() << "Error: Invalid playback command flag [" << flg << "] received. (MainWindow::processPlaybackCommand)";
            break;
        }
    }
    else if(currentOperationMode == (CAMERAMODULE_SLAVEMODE | CAMERAMODULE_CAMERASTREAMINGMODE)){
        switch (flg) {
        case PC_STOPRECORD:
            stopRecording();
            break;
        case PC_RECORD:
            startRecording();
            break;
        default:
            break;
        }
    }

}

void MainWindow::pausePlaybackSignal(){
    emit signal_sendPlaybackCommand(PC_PAUSE, currentTime);
}

void MainWindow::playPlaybackSignal(){
    emit signal_sendPlaybackCommand(PC_PLAY, currentTime);
}

void MainWindow::frameOutOfRangeStatus(bool s){
    if(s){
        frameInRange->setText("Reqested frame time not in the video range.");
        frameInRange->setVisible(true);
        frameInRange->setEnabled(true);
//        pauseButtonPressed();
//        pauseButtonReleased();
    }
    else{
        frameInRange->setVisible(false);
        frameInRange->setEnabled(false);
//        playButtonPressed();
//        playButtonReleased();
    }
}

void MainWindow::pauseButtonPressed() {
     playButton->setDown(false);
     emit signal_pausePlayback();
//     emit signal_sendPlaybackCommand(PC_PAUSE, currentTime);
     //videoController->pausePlayback();
     isVideoFilePlaying = false;
     enableTimeEdit();

}
void MainWindow::pauseButtonReleased() {
        pauseButton->setDown(true);
}

void MainWindow::playButtonPressed() {
    lockEnterButtonLinkToPlay = false; //Used to prevent playback start after an enter press in the time dialog

    pauseButton->setDown(false);
    playButton->setBlinkOff();

    if (loggingOn) {
        emit signal_startFastPlayback();
    } else {
        emit signal_startPlayback();
    }
    //videoController->startPlayback();
    isVideoFilePlaying = true;
    disableTimeEdit();
}
void MainWindow::playButtonReleased() {
    playButton->setDown(true);
    playButton->setChecked(false);

}

void MainWindow::getUserInput() {
    //The tracking algorithm is confused, so we need user input
    isVideoFilePlaying = false;
    playButton->setDown(false);
    setCurrentTool(1); //User input tool
    graphicsWindow->setTool(1);
}

void MainWindow::playbackSliderPositionChanged(int position) {
    //videoController->seekRelativeFrame(position);

    int currentSliderLoc = position*((double)(videoController->playbackTimeStamps.length())/(double)TIMESLIDERSTEPS);
    quint32 approxTimeStamp;
    if (videoController->playbackTimeStamps.length() > currentSliderLoc) {
        approxTimeStamp = videoController->playbackTimeStamps.at(currentSliderLoc);
    } else {
        approxTimeStamp = videoController->playbackTimeStamps.last();
    }
    /*int tmpSeekFrame = 0;
    for (int i=0; i< videoController->playbackTimeStamps.length(); i++) {
        if (videoController->playbackTimeStamps.at(i) >= t) {
            break;
        }
        tmpSeekFrame = i;
    }

    double currentSliderLoc = (double)(tmpSeekFrame)/(double)(videoController->playbackTimeStamps.length());*/


    quint32 totalVideoTime = fileEndTime-fileStartTime;
    //quint32 approxTimeStamp = fileStartTime + (quint32)((double) position * ((double)totalVideoTime/TIMESLIDERSTEPS));
    emit signal_sendPlaybackCommand(PC_SEEK, approxTimeStamp);
    setTime(approxTimeStamp);

    /*if(!(currentOperationMode & (CAMERAMODULE_SLAVEMODE | CAMERAMODULE_FILEPLAYBACKMODE))){
        setTime(approxTimeStamp);
    }*/
}

void MainWindow::playbackSliderPressed() {
    isVideoSliderPressed = true;
    if (isVideoFilePlaying) {
        //videoController->pausePlayback();
        emit signal_pausePlayback();
//        emit signal_sendPlaybackCommand(PC_PAUSE, currentTime);

        videoWasPlayingBeforeSliderPress = true;
        isVideoFilePlaying = false;
    } else {

        videoWasPlayingBeforeSliderPress = false;
    }
}

void MainWindow::playbackSliderReleased() {
    justSeekedFlag = true; //used to clear the log marks on the slider
//    videoController->seekRelativeFrame(filePositionSlider->value());
    emit signal_seekRelativeFrame(filePositionSlider->value());
    if (videoWasPlayingBeforeSliderPress) {
        //videoController->startPlayback();
        emit signal_startPlayback();
//        emit signal_sendPlaybackCommand(PC_PLAY, currentTime);

        isVideoFilePlaying = true;
    }
    isVideoSliderPressed = false;

}

void MainWindow::newStartTimeEntered() {
    //The start of the time range was entered with a string (00:32:45)
    quint32 newTime;
    QString tString = fileStartTimeLabel->text();
    if (convertTimeString(tString,newTime)) {
        if ((newTime >= fileStartTime) && (newTime <=endSliderTime)) {
            startSliderTime = newTime;
        }
    }

    setSliderTimeRange(startSliderTime,endSliderTime); //sets the labels
    int newSliderPos = (double)(startSliderTime-fileStartTime)/((double)(fileEndTime-fileStartTime)/TIMESLIDERSTEPS);
    filePositionSlider->setStart(newSliderPos); //Move the start slider

//    videoController->newTimeRange(startSliderTime,endSliderTime); //update the video controller (which controls video playback)
    emit signal_newTimeRange(startSliderTime, endSliderTime);
    fileStartTimeLabel->clearFocus();
}

void MainWindow::newEndTimeEntered() {
    //The end of the time range was entered with a string (01:32:45)
    quint32 newTime;
    QString tString = fileEndTimeLabel->text();
    if (convertTimeString(tString,newTime)) {
        if ((newTime <= fileEndTime) && (newTime >=startSliderTime)) {
            endSliderTime = newTime;
        }
    }

    setSliderTimeRange(startSliderTime,endSliderTime); //Update the start and end time strings
    int newSliderPos = (double)(endSliderTime-fileStartTime)/((double)(fileEndTime-fileStartTime)/TIMESLIDERSTEPS);
    filePositionSlider->setEnd(newSliderPos); //Move the end slider

//    videoController->newTimeRange(startSliderTime,endSliderTime); //update the video controller (which controls video playback)
    emit signal_newTimeRange(startSliderTime, endSliderTime);
    fileEndTimeLabel->clearFocus();
}

void MainWindow::enableTimeEdit() {
    timeLabel->setReadOnly(false);
}

void MainWindow::disableTimeEdit() {
    timeLabel->setReadOnly(true);
}

void MainWindow::newTimeEntered() {
    //The time was entered with a string (01:32:45)
    lockEnterButtonLinkToPlay = true;
    quint32 newTime;
    QString tString = timeLabel->text();
    if (convertTimeString(tString,newTime)) {
        if ((newTime <= endSliderTime) && (newTime >=startSliderTime)) {
            setTime(newTime);
            emit signal_seekToTime(newTime);
//            videoController->seekToTime(newTime);
        } else {
//            qDebug() << "Not within range";
            setTime(currentTime);
        }
    } else {
        qDebug() << "Time not understood";
        setTime(currentTime);
    }

    //videoController->seekRelativeFrame(filePositionSlider->value());


    //setSliderTimeRange(startSliderTime,endSliderTime); //Update the start and end time strings
    //int newSliderPos = (double)(endSliderTime-fileStartTime)/((double)(fileEndTime-fileStartTime)/TIMESLIDERSTEPS);
    //filePositionSlider->setEnd(newSliderPos); //Move the end slider

    //videoController->newTimeRange(startSliderTime,endSliderTime); //update the video controller (which controls video playback)
    //fileEndTimeLabel->clearFocus();
}

void MainWindow::setSourceOn(bool on) {
    //Used to toggle the availability of menus that should only be available when a video source is on

    if (on) {
        menuGeometry->setEnabled(true);
    } else {
        menuGeometry->setEnabled(false);
    }
}

bool MainWindow::convertTimeString(QString tstring, quint32 &t) {
    //Convert the time string to number of seconds
    QTime tm = QTime::fromString(tstring,"h:mm:ss");
    if (tm.isValid()) {
        t = abs(tm.secsTo(QTime(0,0,0,0)))*clockRate;
        return true;
    } else {
        return false;
    }

}

void MainWindow::setFullTimeRange() {
    //Resets the start and end range to the full file range

    startSliderTime = fileStartTime;
    endSliderTime = fileEndTime;

    setSliderTimeRange(startSliderTime,endSliderTime); //Update the start and end time strings
    int newSliderPos = (double)(endSliderTime-fileStartTime)/((double)(fileEndTime-fileStartTime)/TIMESLIDERSTEPS);
    filePositionSlider->setEnd(newSliderPos); //Move the end slider

    newSliderPos = (double)(startSliderTime-fileStartTime)/((double)(fileEndTime-fileStartTime)/TIMESLIDERSTEPS);
    filePositionSlider->setStart(newSliderPos); //Move the end slider

//    videoController->newTimeRange(startSliderTime,endSliderTime); //update the video controller (which controls video playback)
    emit signal_newTimeRange(startSliderTime, endSliderTime);
}

void MainWindow::rewindFileToStartRange() {
    //Rewinds the file to the start range marker

    double currentSliderLoc = (double)(startSliderTime-fileStartTime)/(double)(fileEndTime-fileStartTime);
    filePositionSlider->setValue((int)(TIMESLIDERSTEPS*currentSliderLoc));

//    videoController->seekToTime(startSliderTime);
    emit signal_seekToTime(startSliderTime);

}

void MainWindow::setVideoTimeRange(quint32 start, quint32 end) {
    //Sets the start and end time of the entire video
    fileStartTime = start;
    fileEndTime = end;
    setSliderTimeRange(start, end);
    filePositionSlider->setSliderPosition(0);
}

void MainWindow::setSliderTimeRange(quint32 start, quint32 end) {
    //This sets the text in the start and end time displays

    startSliderTime = start;
    endSliderTime = end;

    quint32 convertTime = start;


    if (clockRate == 0) {
        qDebug() << "Error: clock rate is zero. Unable to calculate slider position.";
        return;
    }

    //QTime currentTime;
    QString currentTimeString("");
    uint32_t tmpTimeStamp = convertTime;
    int hoursPassed = floor(tmpTimeStamp/(clockRate*60*60));
    tmpTimeStamp = tmpTimeStamp - (hoursPassed*60*60*clockRate);
    int minutesPassed = floor(tmpTimeStamp/(clockRate*60));
    tmpTimeStamp = tmpTimeStamp - (minutesPassed*60*clockRate);
    int secondsPassed = floor(tmpTimeStamp/(clockRate));
    tmpTimeStamp = tmpTimeStamp - (secondsPassed*clockRate);
    //int tenthsPassed = floor(((tmpTimeStamp*10)/sourceSamplingRate));
    //int32_t currentTimeStamp = rawData.timestamps[rawData.writeIdx];



    if (hoursPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(hoursPassed));
    currentTimeString.append(":");
    if (minutesPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(minutesPassed));
    currentTimeString.append(":");
    if (secondsPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(secondsPassed));

    //qDebug() << currentTimeString;
    fileStartTimeLabel->setText(currentTimeString);

    //Now do the end time
    convertTime = end;

    //QTime currentTime;
    currentTimeString = "";
    tmpTimeStamp = convertTime;
    hoursPassed = floor(tmpTimeStamp/(clockRate*60*60));
    tmpTimeStamp = tmpTimeStamp - (hoursPassed*60*60*clockRate);
    minutesPassed = floor(tmpTimeStamp/(clockRate*60));
    tmpTimeStamp = tmpTimeStamp - (minutesPassed*60*clockRate);
    secondsPassed = floor(tmpTimeStamp/(clockRate));
    tmpTimeStamp = tmpTimeStamp - (secondsPassed*clockRate);
    //int tenthsPassed = floor(((tmpTimeStamp*10)/sourceSamplingRate));
    //int32_t currentTimeStamp = rawData.timestamps[rawData.writeIdx];

    if (hoursPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(hoursPassed));
    currentTimeString.append(":");
    if (minutesPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(minutesPassed));
    currentTimeString.append(":");
    if (secondsPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(secondsPassed));

    fileEndTimeLabel->setText(currentTimeString);


}

void MainWindow::setStopPlaybackUponTrackingError(bool stop) {
    qDebug() << "Stop playback upon error set to" << stop;

    if (actionSetStopWhenTrackingError->isChecked() != stop) {
        actionSetStopWhenTrackingError->setChecked(stop);
    }
    emit signal_setStopUponTackingError(stop);
}

void MainWindow::setResReduceNone() {
    menuResReduceNone->setChecked(true);
    menuResReduce480->setChecked(false);
    menuResReduce720->setChecked(false);
    menuResReduce1080->setChecked(false);
    resReduceSetting = -1;
    emit signal_newResReduceSetting(resReduceSetting);
    saveResReduceSetting();
}

void MainWindow::setResReduce480() {
    menuResReduceNone->setChecked(false);
    menuResReduce480->setChecked(true);
    menuResReduce720->setChecked(false);
    menuResReduce1080->setChecked(false);
    resReduceSetting = 480;
    emit signal_newResReduceSetting(resReduceSetting);
    saveResReduceSetting();
}

void MainWindow::setResReduce720() {
    menuResReduceNone->setChecked(false);
    menuResReduce480->setChecked(false);
    menuResReduce720->setChecked(true);
    menuResReduce1080->setChecked(false);
    resReduceSetting = 720;
    emit signal_newResReduceSetting(resReduceSetting);
    saveResReduceSetting();
}

void MainWindow::setResReduce1080() {
    menuResReduceNone->setChecked(false);
    menuResReduce480->setChecked(false);
    menuResReduce720->setChecked(false);
    menuResReduce1080->setChecked(true);
    resReduceSetting = 1080;
    emit signal_newResReduceSetting(resReduceSetting);
    saveResReduceSetting();
}

void MainWindow::saveResReduceSetting() {
    //Save resolution reduction setting
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
    settings.beginGroup(QLatin1String("processor"));
    settings.setValue(QLatin1String("res_reduce"), resReduceSetting);
    settings.endGroup();
}

void MainWindow::trackingButtonPressed() {

    if (currentOperationMode & CAMERAMODULE_CAMERASTREAMINGMODE || (currentOperationMode & CAMERAMODULE_SLAVEMODE && currentOperationMode & CAMERAMODULE_FILEPLAYBACKMODE)) {
        //Toggle tracking mode for live camera feed

        trackingOn = !trackingOn;
        setTracking(trackingOn);
        setCurrentTool(1); //User input tool
        graphicsWindow->setTool(1);
    } else {
        //Open file for offline tracking
        emit signal_pausePlayback();

        if (!loggingOn) {

            QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
            settings.beginGroup(QLatin1String("paths"));
            QString tempPath = settings.value(QLatin1String("inputFilePath")).toString();
            QStringList splitFromExtension = tempPath.split(".h264");
            tempPath = splitFromExtension.at(0);

#if defined (__linux__)
            //QString fileName = QFileDialog::getSaveFileName(this, "Create log file", tempPath, " position tracking files (*.videoPositionTracking)",nullptr, QFileDialog::DontUseNativeDialog);
            QString fileName = QFileDialog::getSaveFileName(this, "Create log file", tempPath, " position tracking files (*.videoPositionTracking)");
#else
            QString fileName = QFileDialog::getSaveFileName(this, "Create log file", tempPath, " position tracking files (*.videoPositionTracking)");
#endif

//            QString linearFileName = QFileDialog::getSaveFileName(0, "Create log file", tempPath, " position tracking files (*.videoPositionTracking)");
            if (!fileName.isEmpty()) {

                //Save the folder in system setting for the next session
                QFileInfo fi(fileName);
                //QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
                //settings.beginGroup(QLatin1String("paths"));
                //settings.setValue(QLatin1String("inputFilePath"), fi.absoluteFilePath());
                //settings.endGroup();
                rewindFileToStartRange();
                playButton->setBlinkOn();
                qDebug() << "loggingOn is set to true";
                loggingOn = true;
                emit signal_createPlaybackLogFile(fileName);

            } else {
                trackButton->setDown(false);
                trackButton->setChecked(false);
            }


        } else {
            QMessageBox::StandardButton reply;
            reply = QMessageBox::question(this, "Close log file", "CLose log file?",
                                          QMessageBox::Close|QMessageBox::Cancel);
            if (reply == QMessageBox::Close) {
                loggingOn = false;
                filePositionSlider->clearLogMarks();
                trackButton->setDown(false);
                emit signal_closePlaybackLogFile();
            }

        }

    }

}

void MainWindow::trackingButtonReleased() {
    if (currentOperationMode & CAMERAMODULE_CAMERASTREAMINGMODE || (currentOperationMode & CAMERAMODULE_SLAVEMODE && currentOperationMode & CAMERAMODULE_FILEPLAYBACKMODE)) {
        if (trackingOn) {
            trackButton->setDown(true);
        } else {
            trackButton->setDown(false);
        }
    } else {
        if (loggingOn) {
            trackButton->setDown(true);
        } else {
            trackButton->setDown(false);
        }
    }
}

void MainWindow::setTracking(bool tOn) {
    trackButton->setDown(tOn);
    videoController->imageProcessor->setTracking(tOn);
    graphicsWindow->locMarkerOn(tOn);
}

void MainWindow::turnOffPlayback() {
    playButton->setVisible(false);
    pauseButton->setVisible(false);
    pauseButton->setDown(false);
    playButton->setDown(false);
    fileStartTimeLabel->setVisible(false);
    filePositionSlider->setVisible(false);
    fileEndTimeLabel->setVisible(false);
    inputFileOpen = false;
    playbackMode = false;
    inputFileName = "";
    fileStartTime = 0;
    fileEndTime = 0;
    isVideoFilePlaying = false;
}

void MainWindow::activatePlaybackControls() {
    playButton->setVisible(true);
    pauseButton->setVisible(true);
    pauseButton->setDown(true);
    playButton->setDown(false);

    fileStartTimeLabel->setVisible(true);
    filePositionSlider->setVisible(true);
    fileEndTimeLabel->setVisible(true);

    //The video thread takes time to start-- we give it 500 ms before sending settings.
    connect(&functionCallTimer, SIGNAL(timeout()), this, SLOT(newSettings()));
    functionCallTimer.start(500);
    //newSettings(trackSettings);
}


void MainWindow::sendNewAnimalPosition(quint32 time) {

//    //Here we send out the animal's position to all modules that have requested that data
//    //only utilize the old network if the new network is not currently initialized
    if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {
        if (moduleNet->dataServerStarted) {
            for (int i = 0; i < moduleNet->dataServer->messageHandlers.length(); i++) {
                //first check if streaming is turned on by the recieving module
                if (moduleNet->dataServer->messageHandlers[i]->isModuleDataStreamingOn()) {
                    //attach header packet with timestamp and camera number **possibly move this to videoDisplay.cpp for the sake of consistancy...
                    dataPacket header(PPT_Header);
                    dataSend time1(DT_uint32_t, time);
                    dataSend cameraN(DT_uint8_t, cameraNum);
                    dataSend counter(DT_int, numOfSentPackets);
                    header.insert(time1);
                    header.insert(cameraN);
                    header.insert(counter);
                    positionData.append(header);
                    //qint32 scale = 30;
                    //qDebug() << "***Sending animalPosData from time [" << time << "] at time [" << currentTime << "] -Latency[" << ((currentTime - time)/30) << "]";
                    moduleNet->dataServer->messageHandlers[i]->sendAnimalPosition(positionData);
                    numOfSentPackets++;
                    positionData.clear(); //reset the datapacket so we don't ever send the same data twice
                    //qDebug() << "camera module sending Position" << time << x << y;

                }
            }
        }

    }
}

void MainWindow::turnOnPlayback(QString fileName) {

    //Possible danger here-- this is a direct call to an object that lives in another thread.
    //So this function will actually be called in this thread, while subsequent frames are processed in the
    //videoController thread. Need to double check for race conditions.
    if (videoController->inputFileSelected(fileName)) {

        inputFileOpen = true;
        playbackMode = true;
        inputFileName = fileName;
        newSettings(trackSettings);

        setFullTimeRange(); //Reset the start and end range.
        rewindFileToStartRange(); //seek to beginning
        setSourceOn(true);

        //VideoController connections for fileplayback with Trodes
        if(currentOperationMode & CAMERAMODULE_FILEPLAYBACKMODE && currentOperationMode & CAMERAMODULE_SLAVEMODE){
            videoController->setSlaveFilePlayback(true);
            if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {
                connect(moduleNet->trodesClient,SIGNAL(currentTimeReceived(quint32)),videoController,SLOT(checkNextTimeFrame(quint32)));
            }
            //MARK: refactor
            connect(networkClient.get(), &CameraModuleClient::sig_cmd_time, videoController, &VideoDisplayController::checkNextTimeFrame);
            connect(videoController, SIGNAL(pausedForBadLoc()), this, SLOT(pausePlaybackSignal()));
            connect(videoController, SIGNAL(frameOutOfRange(bool)), this, SLOT(frameOutOfRangeStatus(bool)));
        }

    } else {
        //TODO: open error dialog
    }

}

void MainWindow::toolsButtonPressed() {
    toolsButton->setDown(false);
    if (toolsDialogOpen) {
        emit closeDialogs();
        return;
    }

//    if (fileOpen) {
//        qDebug() << "Open Tools with linear tools disabled.";
//    }
//    else
//        qDebug() << "Open tools with no tools disabled";

    ToolsDialog *newDialog = new ToolsDialog(currentTool, this);
    //newDialog->setWindowFlags(Qt::Popup);
    //MARK: CUR
    Qt::WindowFlags flags = Qt::Popup;
    flags |= Qt::FramelessWindowHint;
    newDialog->setWindowFlags(flags);
    //newDialog->setAttribute(Qt::WA_TranslucentBackground,true);

    //newDialog->setWindowFlags(Qt::Popup| Qt::FramelessWindowHint|Qt::WA_TranslucentBackground);

    //For some reason, windows and Mac give different geometry info for the sound button. Will probably need to
    //add something for Linux here too.

    newDialog->setGeometry(QRect(this->geometry().x()+toolsButton->x(),this->geometry().y()+toolsButton->y()+toolsButton->height(),40,100));

    newDialog->setFixedWidth(TOOL_BUTTON_SIZE*TOOL_COLUMNS+TOOL_PANEL_PAD_W);
    newDialog->setFixedHeight(TOOL_BUTTON_SIZE*TOOL_ROWS+TOOL_PANEL_PAD_H);

    newDialog->setGeometryToolsEnabled(!fileOpen);
    if (currentOperationMode & CAMERAMODULE_STANDALONEMODE) {
        newDialog->setGeometryToolsEnabled(!loggingOn);
    }
    else
        newDialog->setGeometryToolsEnabled(!fileOpen);

    toolsDialogOpen = true;
    connect(this, SIGNAL(closeDialogs()),newDialog,SLOT(close()));
    connect(newDialog,SIGNAL(windowClosed()), this, SLOT(setToolsMenuClosed()));
    //specific signal/slot connections here
    connect(newDialog,SIGNAL(toolActivated(int)),this,SLOT(setCurrentTool(int)));
    connect(newDialog,SIGNAL(toolActivated(int)),graphicsWindow,SLOT(setTool(int)));
    //mark: export
    connect(this,SIGNAL(signal_fileOpened()),newDialog,SLOT(setCurrentToolToPoint()));

    newDialog->show();
}

void MainWindow::sourceButtonPressed() {


    if (sourceDialogOpen) {
        emit closeDialogs();
        return;
    }

    if (currentOperationMode & CAMERAMODULE_CAMERASTREAMINGMODE) {
        SourceDialog *newDialog = new SourceDialog(videoController->availableCameras(), this);
        //newDialog->setWindowFlags(Qt::Popup);
        Qt::WindowFlags flags = Qt::Popup;
        flags |= Qt::FramelessWindowHint;
        newDialog->setWindowFlags(flags);
        //newDialog->setAttribute(Qt::WA_TranslucentBackground,true);

        //newDialog->setWindowFlags(Qt::Popup| Qt::FramelessWindowHint|Qt::WA_TranslucentBackground);


        //newDialog->setGeometry(QRect(this->x()+sourceButton->x()+8,this->y()+sourceButton->y()+48,40,100));

        newDialog->setGeometry(QRect(this->geometry().x()+sourceButton->x(),this->geometry().y()+sourceButton->y()+sourceButton->height(),40,200));

        newDialog->setFixedWidth(150);
        newDialog->setFixedHeight(25*videoController->availableCameras().length());


        sourceDialogOpen = true;
        connect(this, SIGNAL(closeDialogs()),newDialog,SLOT(close()));
        connect(newDialog,SIGNAL(cameraSelected(int)),videoController,SLOT(newCameraSelected(int)));
        connect(newDialog, SIGNAL(inputFileSelected(QString)),this,SLOT(turnOnPlayback(QString)));
        //connect(newDialog,SIGNAL(newThresh(int,bool)),this,SLOT(setThresh(int,bool)));
        connect(newDialog,SIGNAL(windowClosed()), this, SLOT(setSourceMenuClosed()));
        newDialog->show();


    } else { //Open a dialog to select a playback file
        //Used the saved system settings from the last session as the default folder
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
        settings.beginGroup(QLatin1String("paths"));
        QString tempPath = settings.value(QLatin1String("inputFilePath")).toString();

        settings.endGroup();

#if defined (__linux__)
         //QString fileName = QFileDialog::getOpenFileName(this, QString("Select file to open"),tempPath,"h264 files (*.h264 *.mpg)",nullptr, QFileDialog::DontUseNativeDialog);
         QString fileName = QFileDialog::getOpenFileName(this, QString("Select file to open"),tempPath,"h264 files (*.h264 *.mpg)");
#else
         QString fileName = QFileDialog::getOpenFileName(this, QString("Select file to open"),tempPath,"h264 files (*.h264 *.mpg)");
#endif


        if (!fileName.isEmpty()) {

            //Save the folder in system setting for the next session
            QFileInfo fi(fileName);
            QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
            settings.beginGroup(QLatin1String("paths"));
            settings.setValue(QLatin1String("inputFilePath"), fi.absoluteFilePath());
            settings.endGroup();

            //Load the config file
            turnOnPlayback(fi.absoluteFilePath());
            trackButton->setEnabled(true);

            enableTimeEdit();


            //emit inputFileSelected(fi.absoluteFilePath());
            //close();
            if(currentOperationMode & CAMERAMODULE_SLAVEMODE && currentOperationMode & CAMERAMODULE_FILEPLAYBACKMODE)
                filePositionSlider->hide();
        }
    }
    sourceButton->setDown(false);
}

void MainWindow::setSourceMenuClosed() {
    sourceDialogOpen = false;
}
void MainWindow::setToolsMenuClosed() {
    toolsDialogOpen = false;
}

void MainWindow::settingsButtonPressed() {
    settingsButton->setDown(false);

    SettingsDialog *newDialog = new SettingsDialog(trackSettings, this);


    //newDialog->setWindowFlags(Qt::Popup);
    Qt::WindowFlags flags = Qt::Popup;
    flags |= Qt::FramelessWindowHint;
    newDialog->setWindowFlags(flags);
    //newDialog->setAttribute(Qt::WA_TranslucentBackground,true);

    //For some reason, windows and Mac give different geometry info for the sound button. Will probably need to
    //add something for Linux here too.


    newDialog->setGeometry(QRect(this->geometry().x()+settingsButton->x(),this->geometry().y()+settingsButton->y()+settingsButton->height(),40,100));
    newDialog->setFixedWidth(125);
    newDialog->setFixedHeight(250);



    connect(this, SIGNAL(closeDialogs()),newDialog,SLOT(close()));
    connect(newDialog,SIGNAL(settingsChanged(TrackingSettings)),this,SLOT(newSettings(TrackingSettings)));
    //connect(newDialog,SIGNAL(newThresh(int,bool)),this,SLOT(setThresh(int,bool)));
    //connect(newDialog,SIGNAL(newRing(int,bool)),this,SLOT(setRing(int,bool)));
    //connect(newDialog,SIGNAL(windowClosed()), SLOT(deleteLater()));


    newDialog->show();
}

void MainWindow::videoStarted(int resolutionY, int resolutionX) {
    if ((resolutionX != resX) || (resolutionY != resY)) {
        //For now, if the actual resolution does not match the command line input,
        //we change it.  TODO: implement image downsampling.
        resY = resolutionY;
        resX = resolutionX;
    }
    if ((!videoStreaming) && (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based))  {
        moduleNet->sendModuleIsReady(); //we send ready signal once video has started for the first time
    }
    videoStreaming = true;
    setSourceOn(true);



    //Here we deal with the situation where the master module has already created a log file, and perhaps even started recording before the camera module is started.
    //The camera module may have received these signals but had to delay them until video had started.
    if (fileInitiated && !fileOpen) {
        qDebug() << "Sending create file signal for pre-initiated file.";
        emit signal_createFile(fileName);
        //mark: maybe put "emit sig_saveAllGeometry(fileName)" here instead...

        if (recording) {

            emit signal_startRecording();
            recordTimer->restart();

        }

    }




}

//MARK: refactor
void MainWindow::aquisitionCommandReceived(QString command, uint32_t time) {
    if (command == acq_PLAY) {
        processPlaybackCommand(PC_PLAY, time);
//        startRecording();
    }
    else if (command == acq_RECORD) {
        processPlaybackCommand(PC_RECORD, time);
    }
    else if (command == acq_STOPRECORD) {
        processPlaybackCommand(PC_STOPRECORD, time);
    }
    else if (command == acq_PAUSE) {
        processPlaybackCommand(PC_PAUSE, time);
    }
    else if (command == acq_STOP) {
        processPlaybackCommand(PC_STOP, time);
    }
    else if (command == acq_SEEK) {
        processPlaybackCommand(PC_SEEK, time);
    }
    else
        qDebug() << "ERROR: aquisition command not recognized. (MainWindow::aquisitionCommandReceived)";
}

void MainWindow::sendAcquisitionCommand(qint8 cmd, quint32 time) {
//    qDebug() << "CAMERA Sending Acqusition command! [" << cmd << "]";
    QString strCmd;
    switch (cmd) {
    case PC_PLAY:
        strCmd = acq_PLAY;
        break;
    case PC_PAUSE:
        strCmd = acq_PAUSE;
        break;
    case PC_STOP:
        strCmd = acq_STOP;
        break;
    case PC_SEEK:
        strCmd = acq_SEEK;
        break;
    default:
        qDebug() << "Error: Cannot send acquisition command of type " << cmd;
        break;
    }
    emit signal_sendAquisitionCommand(strCmd, time);
}

void MainWindow::setRecordStatusOn() {
    statusbar->showMessage(QString("Status: recording   %1").arg(fileName));
    fileStatusColorIndicator->setStyleSheet("QLabel { background-color : lightgreen; color : black; border-radius: 5px}");
    fileStatusColorIndicator->setText("Recording active");
    fileStatusColorIndicator->setVisible(true);
}

void MainWindow::setRecordStatusOff() {
    statusbar->showMessage("Status: recording paused");
    fileStatusColorIndicator->setStyleSheet("QLabel { background-color : yellow; color : black; border-radius: 5px}");
    fileStatusColorIndicator->setText("Recording paused");
    fileStatusColorIndicator->setVisible(true);
}

void MainWindow::setFileOpenedStatus() {
    //Mark: Export put file open sig herek
    statusbar->showMessage(QString("Status: file created   %1").arg(fileName));
    fileInfoLabel->setVisible(true);
    fileInfoLabel->setEnabled(true);
    fileInfoLabel->setText(
        QString("%1   |   %2m %3sec recorded").arg(baseFileName)
        .arg((msecRecorded+recordTimer->elapsed()*recording) / 1000 / 60)
        .arg((msecRecorded+recordTimer->elapsed()*recording) / 1000 % 60));

    fileStatusColorIndicator->setStyleSheet("QLabel { background-color : yellow; color : black; border-radius: 5px}");
    fileStatusColorIndicator->setText("Recording paused");
    fileStatusColorIndicator->setVisible(true);
    fileOpen = true;
    sourceButton->setEnabled(false);
    emit signal_fileOpened();

    //The the record signal was recieved before the open file signal, we start recording now.
    if (videoStreaming && recording) {
        emit signal_startRecording();
        recordTimer->restart();
    }

    if(ptpChecksEnabled && ptp_status != 2){
        QMessageBox::warning(this, "PTP not ready",
                             "The camera's PTP functionality is not fully calibrated or initialized. "
                             "Timestamps will not be accurate until it is. Calibrating may take anywhere "
                             "from 1 min to 15 min.");
    }

    //Once a file is open, we do not allow a change in resolution
    menuResReduce->setEnabled(false);
}

void MainWindow::setFileClosedStatus() {
    statusbar->showMessage("Status: file closed");
    fileInfoLabel->setVisible(false);
    fileInfoLabel->setEnabled(false);

    fileStatusColorIndicator->setStyleSheet("QLabel { background-color : yellow; color : black; border-radius: 5px}");
    fileStatusColorIndicator->setVisible(false);
    fileStatusColorIndicator->setText("Recording paused");
    fileInitiated = false;
    fileOpen = false;
    sourceButton->setEnabled(true);
    menuResReduce->setEnabled(true);
}


void MainWindow::startRecording() {
    /*
    if (fileOpen) {
        statusbar->showMessage("Status: recording");
    }
    recording = true;
    */
    //sourceButton->setEnabled(false);
    qDebug() << "Got record signal from master module";
    recording = true;
    if (videoStreaming && fileInitiated) {
        emit signal_startRecording();
        recordTimer->restart();
    }
    if(ptpChecksEnabled && ptp_status != 2){
        QMessageBox::warning(this, "PTP not ready",
                             "The camera's PTP functionality is not fully calibrated or initialized. "
                             "Timestamps will not be accurate until it is. Calibrating may take anywhere "
                             "from 1 min to 15 min.");
    }
}

void MainWindow::stopRecording() {
    statusbar->showMessage("Status: not recording");
    fileStatusColorIndicator->setStyleSheet("QLabel { background-color : yellow; color : black; border-radius: 5px}");
    fileStatusColorIndicator->setText("Recording paused");
    fileStatusColorIndicator->setVisible(true);
    recording = false;
    //sourceButton->setEnabled(true);
    emit signal_stopRecording();
    msecRecorded += recordTimer->elapsed();
    recordTimer->restart();
}

void MainWindow::nextFileRequested() {

    qDebug() << "Closing and creating new file...";

    QFileInfo fi(baseFileName);

    QString fileBaseName = fi.completeBaseName();
    fileName = fi.absolutePath()+QString(QDir::separator())+fileBaseName;


    QFileInfo fileCheck(fileName + ".1.h264");
    int numChecks = 1;

    while (fileCheck.exists()) {

        numChecks++;
        fileCheck.setFile(fileName + QString(".%1").arg(numChecks) + ".h264");
    }
    if (numChecks > 0) {
        fileName = fileName + QString(".%1").arg(numChecks);
    }
    fileInitiated = true;
    emit signal_createFile(fileName);
    //closeFile();
    //setupFile(baseFileName);
}

void MainWindow::setupFile(QString filename) {
    //Create live recording file

    qDebug() << "File setup signal received.";

    if (!fileInitiated) {


        QFileInfo fi(filename);

        QString fileBaseName = fi.completeBaseName();
        fileName = fi.absolutePath()+QString(QDir::separator())+fileBaseName;



        int numChecks = moduleInstance;
        QFileInfo fileCheck(fileName + QString(".%1.h264").arg(moduleInstance));


        while (fileCheck.exists()) {

            numChecks++;
            fileCheck.setFile(fileName + QString(".%1").arg(numChecks) + ".h264");
        }
        if (numChecks > 0) {
            fileName = fileName + QString(".%1").arg(numChecks);
        }
        baseFileName = QFileInfo(fileName+".h264").fileName();

        QDir dir = QFileInfo(fileName).absoluteDir();
        if(!dir.exists()){
            //Directory of Trodes doesn't exist on this computer
            //Make a folder for the recording
            QString dirpath = QDir::currentPath() + QDir::separator() + dir.dirName();
            fileName = dirpath + QDir::separator() + QFileInfo(fileName).fileName();
            if(dir.mkpath(dirpath)){
                QMessageBox *msgbox = new QMessageBox(this);
                msgbox->setAttribute(Qt::WA_DeleteOnClose);
                msgbox->setStandardButtons(QMessageBox::Ok);
                msgbox->setWindowTitle("Directory not found");
                msgbox->setText("The directory " + dir.absolutePath() + " was not found on this computer. \n"
                                "The file has been created in " + dirpath + QDir::separator());
                msgbox->setModal(false);
                msgbox->open();
            }
        }

        fileInitiated = true;

        //TODO: This should be moved to a slot triggered when the file is actually opened
        //statusbar->showMessage("Status: file created");
        //fileOpen = true;
        if (videoStreaming) {
            //createFile();

            emit signal_createFile(fileName);
        }

    }
}

void MainWindow::setFileOpen() {
    statusbar->showMessage(QString("Status: file %1 created").arg(fileName));
    fileOpen = true;
}

/*
void MainWindow::createFile() {

    if (fileInitiated && videoStreaming) {
        int width=resX;
        int height=resY;
        //int bitrate=1000000;
        int bitrate=1000000;
        int gop = 40;
        int fps = 25;

        encoder->createFile(fileName+".mpg",width,height,bitrate,gop,fps);
        encoder->createTimestampFile(fileName+".tstamps");
        fileOpen = true;

    }
}*/

void MainWindow::closeFileLater() {
    qDebug() << "Emmiting close signal";
    emit signal_closeFile();
    setFileClosedStatus();
    msecRecorded = 0;
    fileInfoLabel->setText("");
}

void MainWindow::closeFile() {
    //Close live recording file

    if (fileOpen) {
        stopRecording();


        QTimer::singleShot(200,this,&MainWindow::closeFileLater);
        //emit signal_closeFile();

    }


}

void MainWindow::setPythonDir() {
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));

    QString pathName = QFileDialog::getExistingDirectory(this, tr("Set script location"),pythonDir);
    if (!pathName.isEmpty()) {
        qDebug() << "Setting python script location to" << pathName;
        pythonDir = pathName;

        //Remember the directory for the next session
        settings.beginGroup(QLatin1String("pythonDir"));
        settings.setValue(QLatin1String("pythonDir"), pythonDir);
        settings.endGroup();

#ifdef PYTHONEMBEDDED
        if (usePython) {
        mainContext.evalScript("sys.path.append('"+pythonDir+"')\n");
       }
#endif

    }
}


void MainWindow::saveGeometry(OptionFlag flg) {

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
    QString dataDir;
    QString fileName;

    //Remember the last folder used to save geometry
    settings.beginGroup(QLatin1String("geometryDir"));
    QString defaultDir = settings.value(QLatin1String("geometryDir")).toString();
    if (!defaultDir.isEmpty()) {
        dataDir = defaultDir;
    }
    settings.endGroup();
#if defined (__linux__)
    //fileName = QFileDialog::getSaveFileName(this, tr("Save geometry"),dataDir,tr("*.trackgeometry"),nullptr, QFileDialog::DontUseNativeDialog);
    fileName = QFileDialog::getSaveFileName(this, tr("Save geometry"),dataDir,tr("*.trackgeometry"));
#else
    fileName = QFileDialog::getSaveFileName(this, tr("Save geometry"),dataDir,tr("*.trackgeometry"));
#endif


    if (!fileName.isEmpty()) {
//        qDebug() << fileName;
        if (!fileName.contains(".trackgeometry"))
            fileName = QString("%1.trackgeometry").arg(fileName);
        QFileInfo fi(fileName);


        //Remember the directory for the next session
        settings.beginGroup(QLatin1String("geometryDir"));
        settings.setValue(QLatin1String("geometryDir"), fi.absolutePath());
        settings.endGroup();

        graphicsWindow->saveGeometry(fileName, flg);
        //graphicsWindow->saveCurrentLinearGeometry(fileName);

    }
}

void MainWindow::loadGeometry(OptionFlag flg) {
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
    QString dataDir;
    QString fileName;

    //Remember the last folder used to save track geometry
    settings.beginGroup(QLatin1String("geometryDir"));
    QString defaultDir = settings.value(QLatin1String("geometryDir")).toString();
    if (!defaultDir.isEmpty()) {
        dataDir = defaultDir;
    }
    settings.endGroup();
#if defined (__linux__)
    //fileName = QFileDialog::getOpenFileName(this, tr("Load linear geometry"),dataDir,tr("*.trackgeometry"),nullptr, QFileDialog::DontUseNativeDialog);
    fileName = QFileDialog::getOpenFileName(this, tr("Load linear geometry"),dataDir,tr("*.trackgeometry"));
#else
    fileName = QFileDialog::getOpenFileName(this, tr("Load linear geometry"),dataDir,tr("*.trackgeometry"));

#endif




    if (!fileName.isEmpty()) {
        QFileInfo fi(fileName);


        //Remember the directory for the next session
        settings.beginGroup(QLatin1String("geometryDir"));
        settings.setValue(QLatin1String("geometryDir"), fi.absolutePath());
        settings.endGroup();

        graphicsWindow->loadGeometry(fileName, flg);
    }
}

void MainWindow::setEnableAllGeometrySave(void) {
    //if any of the save tracks are enabled, enable the save all button
    if (actionSaveLinearGeoTrack->isEnabled() || actionSaveRangeGeoTrack->isEnabled() || actionSaveZoneGeo->isEnabled() || actionSaveInclusionExclusionGeo->isEnabled()) {
        actionSaveAllGeoTrack->setEnabled(true);

    }
    else { //nothing enabled, disable save all
        actionSaveAllGeoTrack->setEnabled(false);

    }
}

void MainWindow::setEnableLinearGeometrySave(bool on) {
    linearGeometryExists = on;
    actionSaveLinearGeoTrack->setEnabled(on);
    setEnableAllGeometrySave();
}

void MainWindow::setEnableRangeGeometrySave(bool on) {
    actionSaveRangeGeoTrack->setEnabled(on);
    setEnableAllGeometrySave();
}

void MainWindow::setEnableZoneGeometrySave(bool on) {
    actionSaveZoneGeo->setEnabled(on);
    setEnableAllGeometrySave();
}

void MainWindow::setEnableIncExclGeometrySave(bool on) {
    actionSaveInclusionExclusionGeo->setEnabled(on);
    setEnableAllGeometrySave();
}


void MainWindow::newSettings() {
    functionCallTimer.stop();
    newSettings(trackSettings);
}

void MainWindow::newSettings(TrackingSettings t) {
    trackSettings = t;
    //videoController->imageProcessor->newTrackingSettings(trackSettings);
    graphicsWindow->newSettings(trackSettings);
    emit signal_newSettings(t);

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));

    settings.beginGroup(QLatin1String("processor"));
    settings.setValue(QLatin1String("thresh"), trackSettings.currentThresh);
    settings.setValue(QLatin1String("trackDarkPixels"), trackSettings.trackDark);
    settings.setValue(QLatin1String("ringSize"), trackSettings.currentRingSize);
    settings.setValue(QLatin1String("ringOn"), trackSettings.ringOn);
    settings.setValue(QLatin1String("ledColorPair"), trackSettings.LEDColorPair);
    settings.setValue(QLatin1String("twoLEDs"), trackSettings.twoLEDs);

    settings.endGroup();
}


void MainWindow::setTimeRate(quint32 inTimeRate) {
    qDebug() << "Camera module received time rate: " << inTimeRate;
    //if(loadedWorkspace.isValid()) inTimeRate = loadedWorkspace.hardwareConf.sourceSamplingRate;
    clockRate = inTimeRate;
    videoController->imageProcessor->clockRate = inTimeRate;
    videoController->overrideSamplingRate(inTimeRate);

}

void MainWindow::setTime(quint32 t) {


    if (!(inputFileOpen || videoStreaming)) {
        return;
    }

    //bool backwardsJump = (t < currentTime); // UNUSED
    currentTime = t;

    //QTime currentTime;
    QString currentTimeString("");
    uint32_t tmpTimeStamp = currentTime;
    int hoursPassed = floor(tmpTimeStamp/(clockRate*60*60));
    tmpTimeStamp = tmpTimeStamp - (hoursPassed*60*60*clockRate);
    int minutesPassed = floor(tmpTimeStamp/(clockRate*60));
    tmpTimeStamp = tmpTimeStamp - (minutesPassed*60*clockRate);
    int secondsPassed = floor(tmpTimeStamp/(clockRate));
    tmpTimeStamp = tmpTimeStamp - (secondsPassed*clockRate);
    //int tenthsPassed = floor(((tmpTimeStamp*10)/sourceSamplingRate));
    //int32_t currentTimeStamp = rawData.timestamps[rawData.writeIdx];

    if (hoursPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(hoursPassed));
    currentTimeString.append(":");
    if (minutesPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(minutesPassed));
    currentTimeString.append(":");
    if (secondsPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(secondsPassed));
    //currentTimeString.append(".");
    //currentTimeString.append(QString::number(tenthsPassed));
    timeLabel->setText(currentTimeString);
    //qDebug() << "Current time: " << currentTime;
    if(currentOperationMode & CAMERAMODULE_CAMERASTREAMINGMODE){
        fileInfoLabel->setText(
            QString("%1   |   %2m %3sec recorded").arg(baseFileName)
            .arg((msecRecorded+recordTimer->elapsed()*recording) / 1000 / 60)
            .arg((msecRecorded+recordTimer->elapsed()*recording) / 1000 % 60));
    }

    if (inputFileOpen && (fileEndTime-fileStartTime > 0) && !isVideoSliderPressed) {

        int tmpSeekFrame = 0;
        for (int i=0; i< videoController->playbackTimeStamps.length(); i++) {
            if (videoController->playbackTimeStamps.at(i) >= t) {
                break;
            }
            tmpSeekFrame = i;
        }

        double currentSliderLoc = (double)(tmpSeekFrame)/(double)(videoController->playbackTimeStamps.length());
        //double currentSliderLoc = (double)(t-fileStartTime)/(double)(fileEndTime-fileStartTime);

        filePositionSlider->setValue((int)(TIMESLIDERSTEPS*currentSliderLoc));
        if (loggingOn) {

            filePositionSlider->markCurrentLocation();

            if (justSeekedFlag) {
                //filePositionSlider->clearLogMarksAfterCurrentPos();
                justSeekedFlag = false;
            }

        }
    }
}

void MainWindow::resizeEvent(QResizeEvent *) {
    emit closeDialogs();

    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();
}

void MainWindow::moveEvent(QMoveEvent *) {
    emit closeDialogs();

    //Remember the new position for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("cameraModule"));
    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    //A keyboard key was pressed.

    if (event->key() == Qt::Key_Escape) {
        //Escape key presses the pause button

        if (inputFileOpen && playbackMode && isVideoFilePlaying) {
            pauseButtonPressed();
            pauseButton->setDown(true);
        }

    } else if ((event->key() == Qt::Key_Enter) || (event->key() == Qt::Key_Return)) {
        //The enter button toggles play/pause

        if (inputFileOpen && playbackMode) {
            if (!lockEnterButtonLinkToPlay) {
                playButtonPressed();
                playButton->setDown(true);
            }
        }

        /*if (inputFileOpen && playbackMode && !isVideoFilePlaying) {
            if (!lockEnterButtonLinkToPlay) {
                playButtonPressed();
                playButton->setDown(true);
            }
        }  else if (inputFileOpen && playbackMode && isVideoFilePlaying) {
            pauseButtonPressed();
            pauseButton->setDown(true);
        }*/

        lockEnterButtonLinkToPlay = false;


    } else if ((event->key() == Qt::Key_Left) || (event->text() == "a")) {
        if (inputFileOpen && playbackMode && !isVideoFilePlaying) {
            //Step back one frame
            emit signal_stepFrameBackward();
        }

    } else if ((event->key() == Qt::Key_Right) || (event->text() == "d")){
        if (inputFileOpen && playbackMode && !isVideoFilePlaying) {
            //Step forward one frame
            emit signal_stepFrameForward();
        }
    }

    graphicsWindow->passKeyEvent(event);
}

bool MainWindow::event(QEvent *event) {
    if (event->type() == QEvent::ToolTip) {
        //add tooltips for the main window here!
        return(true);
    }
    return(QWidget::event(event));
}

void MainWindow::testSlot(OptionFlag flg) {
    qDebug() << "FLG: " << flg;
}

/*bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{

    if (event->type() == QEvent::KeyPress) {
      qDebug() << "keypress event";
      QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
      if ( keyEvent->key() == Qt::Key_Backspace ) {
        qDebug() << "BACKSPace";
      }
    }


}*/

MyAction::MyAction(QWidget *parent):QAction(parent) {
    flg = O_NULL;

}

void MyAction::sendOption(void) {
    emit optionSig(flg);
}


//-----------------------------------------------------------------------------
MySlider::MySlider(QWidget *parent):QSlider(parent) {
    draggingStart = false;
    draggingEnd = false;
    draggingKnob = false;
    startBorder = 0;
    endBorder = TIMESLIDERSTEPS;
    logMarker.resize(TIMESLIDERSTEPS);
    for (int i=0; i<TIMESLIDERSTEPS; i++) {
        logMarker[i] = false;
    }
}

void MySlider::markCurrentLocation() {
    logMarker[value()] = true;
}

void MySlider::clearLogMarks() {
    for (int i=0; i<TIMESLIDERSTEPS; i++) {
        logMarker[i] = false;
    }
    update();
}

void MySlider::clearLogMarksAfterCurrentPos() {
    for (int i=value()+1; i<TIMESLIDERSTEPS; i++) {
        logMarker[i] = false;
    }
    update();
}


void MySlider::mousePressEvent ( QMouseEvent * event ) {
    QStyleOptionSlider opt;
      initStyleOption(&opt);
      //QRect sr = style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, this); // UNUSED
      int clickVal = minimum() + ((maximum()-minimum()) * (event->x()-5)) / (width()-10);

      /*
      if (event->button() == Qt::LeftButton && sr.contains(event->pos()) == true) {
          if ((clickVal >= startBorder) && (clickVal <= endBorder)) {
            QSlider::mousePressEvent(event);
          }
      }*/

      int allowedClickError = TIMESLIDERSTEPS/50;
      if (event->button() == Qt::LeftButton) {

        //If the user clicked the main knob, and the click was closer to the main knob then the others...
        if ((abs(clickVal-value()) < allowedClickError) && (abs(clickVal-value()) < abs(clickVal-startBorder)) && (abs(clickVal-value()) < abs(clickVal-endBorder)) ){
              setValue(clickVal);
              draggingKnob = true;
              update();
              emit sliderPressed();
              event->accept();
        //If the user slicked the start knob, and the main knob was not nearby...
        } else if ((abs(clickVal-startBorder) < allowedClickError) && (abs(value()-startBorder)>(allowedClickError/4)) ){
            startBorder = clickVal;
            draggingStart = true;
            update();
            event->accept();
        //If the user slicked the end knob, and the main knob was not nearby...
        } else if ((abs(clickVal-endBorder) < allowedClickError) && (abs(value()-endBorder)>(allowedClickError/4)) ){
            endBorder = clickVal;
            draggingEnd = true;
            update();
            event->accept();
        //Otherwise, put the main knob where the user clicked.
        } else if ((clickVal >= startBorder) && (clickVal <= endBorder)) {
            setValue(clickVal);
            draggingKnob = true;
            update();
            emit sliderMoved(clickVal);
            emit sliderPressed();

            event->accept();

            //event->accept();
            //QSlider::mousePressEvent(event);
        }

      }

}

void MySlider::mouseReleaseEvent(QMouseEvent *event) {
    if (draggingStart && startBorder > value()) {
        setValue(startBorder);
        emit sliderReleased();
    } else if (draggingEnd && endBorder < value()) {
        setValue(endBorder);
        emit sliderReleased();
    } else if (draggingKnob){
        emit sliderReleased();
        //QSlider::mouseReleaseEvent(event);
    }

    draggingStart = false;
    draggingEnd = false;
    draggingKnob = false;
}

void MySlider::mouseMoveEvent(QMouseEvent *event) {
    int clickVal = minimum() + ((maximum()-minimum()) * (event->x()-5)) / (width()-10);
    if (draggingStart) {
        //Dragging the start border slider
        if ((clickVal >= 0) && (clickVal <= endBorder)) {
            startBorder = clickVal;
            emit newRange(startBorder,endBorder);
            update();
        }
    } else if (draggingEnd) {
        //Dragging the end border slider
        if ((clickVal >= startBorder) && (clickVal <= TIMESLIDERSTEPS)) {
            endBorder = clickVal;
            emit newRange(startBorder,endBorder);
            update();
        }
    } else if (draggingKnob) {
        //Dragging the main position slider
        if ((clickVal >= startBorder) && (clickVal <= endBorder)) {
            setValue(clickVal);
            emit sliderMoved(clickVal);
            update();
        }
    }
    /*
    else {
        if ((clickVal >= startBorder) && (clickVal <= endBorder)) {
          QSlider::mouseMoveEvent(event);
        }

    }*/

}

void MySlider::paintEvent(QPaintEvent *event) {
    QStylePainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    double pixPerUnit;
    if ((maximum()-minimum()) > 0) {
        pixPerUnit = (width()-10)/(double)(maximum()-minimum()) ;
    } else {
        pixPerUnit = 0;
    }

    double startSliderPos = (startBorder*pixPerUnit)+5;
    double endSliderPos = (endBorder*pixPerUnit)+5;
    double knobPos = (value()*pixPerUnit)+5;

    double pixPerLogMarkerStep = (double)(width()-10)/TIMESLIDERSTEPS;

    QPen linePen;

    linePen.setWidth(2);
    linePen.setBrush(Qt::red);
    painter.setPen(linePen);
    for (int i=0; i < TIMESLIDERSTEPS; i++) {
        if (logMarker[i]) {
            painter.drawPoint((i*pixPerLogMarkerStep)+5,2);
        }
    }


    linePen.setWidth(2);
    linePen.setBrush(Qt::lightGray);
    painter.setPen(linePen);

    //Draw the groove line
    QPainterPath rangePath;
    rangePath.moveTo(5, height()/2);
    rangePath.lineTo(width()-5, height()/2);
    painter.drawPath(rangePath);


    //Draw the range line
    linePen.setWidth(3);
    linePen.setBrush(Qt::gray);
    painter.setPen(linePen);
    QPainterPath groovePath;
    groovePath.moveTo(startSliderPos, height()/2);
    groovePath.lineTo(endSliderPos, height()/2);
    painter.drawPath(groovePath);


    //Draw the start range slider knob
    //qDebug() << value() << width() << pixPerUnit;

    QPainterPath startSlider;


    startSlider.addRoundedRect(startSliderPos-2.5,height()/4,5.0,height()/2,5,5.0);
    painter.fillPath(startSlider,QBrush(Qt::gray));

    linePen.setBrush(Qt::black);
    linePen.setWidth(1);
    painter.setPen(linePen);
    painter.drawPath(startSlider);


    //Draw the start range slider knob
    QPainterPath endSlider;
    endSlider.addRoundedRect(endSliderPos-2.5,height()/4,5.0,height()/2,5,5.0);
    painter.fillPath(endSlider,QBrush(Qt::gray));
    painter.drawPath(endSlider);


    //Draw the main slider knob
    QPainterPath mainSliderKnob;
    //mainSliderKnob.addRoundedRect(knobPos-3,0,6.0,height(),5,5.0);

    mainSliderKnob.addRoundedRect(knobPos-4,(height()/2)-4.0,8.0,8.0,5.0,5.0);
    painter.fillPath(mainSliderKnob,QBrush(QColor(150,150,200)));
    //painter.fillPath(mainSliderKnob,QBrush(Qt::gray));
    painter.drawPath(mainSliderKnob);

    painter.end();
}

void MySlider::setStart(int newStartBorder) {

    if ((newStartBorder >= 0) && (newStartBorder <= endBorder)) {
        startBorder = newStartBorder;
        //emit newRange(startBorder,endBorder);
        update();
    } else if (newStartBorder > endBorder) {
        //The start value is greater than the end value, set use the end value
        startBorder = endBorder;
        //emit newRange(startBorder,endBorder);
        update();
    } else if (newStartBorder < 0) {
        startBorder = 0;
        //emit newRange(startBorder,endBorder);
        update();
    }

    if (startBorder > value()) {
        setValue(startBorder);
        emit sliderReleased();
    } else if (endBorder < value()) {
        setValue(endBorder);
        emit sliderReleased();
    }

}

void MySlider::setEnd(int newEndBorder) {
    if ((newEndBorder <= TIMESLIDERSTEPS) && (newEndBorder >= startBorder)) {
        endBorder = newEndBorder;
        //emit newRange(startBorder,endBorder);
        update();
    } else if (newEndBorder < startBorder) {
        //The end value is less than the start value, set use the start value
        endBorder = startBorder;
        //emit newRange(startBorder,endBorder);
        update();
    } else if (newEndBorder > TIMESLIDERSTEPS) {
        endBorder = TIMESLIDERSTEPS;
        //emit newRange(startBorder,endBorder);
        update();
    }


    if (startBorder > value()) {
        setValue(startBorder);
        //emit sliderReleased();
    } else if (endBorder < value()) {
        setValue(endBorder);
        //emit sliderReleased();
    }
}

//MARK: event system
void MainWindow::broadcastEvent(TrodesEventMessage event) {
    //This is how the module sends an event out on the network for other modules to use
    //qDebug() << "Sending Event: " << event;
    if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {
        moduleNet->sendEvent(currentTime, event);
    } else if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::zmq_based) {
        //qDebug() << "ZMQ Event:" << event.getTime() << event.getEventMessage();

        //The message payload is the timestamp
        uint32_t t = event.getTime();
        TrodesMsg eventMsg("4", t);

        networkClient->qtSendEvent(event.getEventMessage(), eventMsg);
    }
}

void MainWindow::broadcastNewEventReq(QString newEventReq) {
    //This is how the module advertizes all of the events that it will provide
    //qDebug() << "Sending new event Req";
    if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {
        moduleNet->sendNewEventNameRequest(newEventReq);
    } else if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::zmq_based) {
        networkClient->qtProvideEvent(newEventReq);

    }
}

void MainWindow::broadcastRemoveEventReq(QString event) {
    //This is how the module removes an event that it was providing
    //qDebug() << "Sending remove event request";
    if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::qsocket_based) {
        moduleNet->sendEventRemoveRequest(event);
    } else if (loadedWorkspace.networkConf.networkType == NetworkConfiguration::zmq_based) {
        networkClient->qtUnprovideEvent(event);
    }
}

void MainWindow::printEventList(QVector<TrodesEvent> evList) {
    qDebug() << "Event list (size " << evList.length() << ") received, printing:";

    for (int i = 0; i < evList.length(); i++)
        evList.at(i).printEventInfo();


}

void MainWindow::eventButtonPressed() {
    eventHandler->promptEventConnectionMenu();
}

bool MainWindow::startTrodesNetworkClient(QString serveraddress, int serverport) {
    networkClient = std::move(std::unique_ptr<CameraModuleClient>(new CameraModuleClient(serveraddress, serverport)));
    return true;
}

void MainWindow::endTrodesNetworkClient() {
    networkClient.reset();
}

void MainWindow::iniActionList() {
    //append all methods called in 'callMethod' to actionList
    actionList.append("Plot point on Animal location"); //0
}

void MainWindow::callMethod(int actionIndex, TrodesEvent ev) {
    if (actionIndex > -1 && actionIndex < actionList.length())  {
        switch (actionIndex) {
        case 0:
        {
            addPoint();
            break;
        }
        default:
        {
            break;
        }
        }
    }
}

//MainWindow::addPoint() -- "Plot point on Animal Location"
void MainWindow::addPoint() {
    graphicsWindow->plotCurTrackedLoc();
}

void MainWindow::checkPTPFeatures(){
    if(!ptpChecksEnabled)
        return;

    bool sysclockfound = false;
    //Check for system clock in devices
    for(int i = 0; i < loadedWorkspace.hardwareConf.devices.length(); ++i){
        if(loadedWorkspace.hardwareConf.devices[i].name=="SysClock" && loadedWorkspace.hardwareConf.devices[i].available){
            sysclockfound = true;
            break;
        }
    }

    if(!sysclockfound){
        QMessageBox::warning(this, "System clock not configured",
                             "Your .trodesconf file is not configured to save system timestamps. "
                             "If you are recording with PTP enabled, it is recommended you close the "
                             "config, open it in the workspace editor, and add the device \"System Clock\".");
    }

    frameInRange->setVisible(true);
}

void MainWindow::ptpFullyInitialized(){
    frameInRange->setText("PTP: synchronized");
    frameInRange->setStyleSheet("QLabel{color: green;}");
    ptp_status = 2;
}

void MainWindow::ptpTryingToInitialize(){
    frameInRange->setText("PTP: listening for master clock...");
    frameInRange->setStyleSheet("QLabel{color: #847700;}");
    ptp_status = 1;
}

void MainWindow::ptpCalibrating(){
    frameInRange->setText("PTP: calibrating with computer...");
    frameInRange->setStyleSheet("QLabel{color: blue;}");
    ptp_status=1;
}

void MainWindow::ptpNotProperlySetup(){
    if(ptpChecksEnabled){
        frameInRange->setText("PTP: disabled or error occurred. Resetting...");
        frameInRange->setStyleSheet("QLabel{color: red;}");
        ptp_status = 0;
    }
    else{
        frameInRange->setText("PTP: Disabled");
        frameInRange->setStyleSheet("QLabel{color: black;}");
        ptp_status = 0;
    }
}

void MainWindow::ptpWrongConfiguration(){
    frameInRange->setText("PTP: Wrong configuration on camera");
    frameInRange->setStyleSheet("QLabel{color: red;}");
    ptp_status=0;
    if(ptpChecksEnabled){
        QMessageBox::warning(this, "Camera PTP in wrong configuration",
                         "This camera seems to have the wrong PTP configuration. "
                         "Please open VimbaViewer and double check that PtpMode is "
                         "set to \"Slave\". ");
    }
}

void MainWindow::ptpWarningMessageBox(QString type){
    //Not the best way, but see avtwrapper.cpp for the different types
    if(type == "NotPTPCapable"){
        QMessageBox::warning(this, "Camera not PTP capable",
                             "PTP synchronization was requested in the configuration "
                             "file (\"-ptpEnabled\" flag to cameraModule), but this "
                             "camera does not seem to be PTP capable. "
                             "Please double-check your camera's capabilities and "
                             "config file settings. Camera clock will not be "
                             "synchronized if you continue.");
    }
    else if(type == "BadTimestampCheck"){
        QMessageBox::warning(this, "Error checking timestamp",
                             "Although PTP is fully initialized on the camera, "
                             "there was an error while checking the synced "
                             "timestamp value.");
    }
    else if(type == "TimestampNotCorrect"){
        QMessageBox::warning(this, "Bad timestamp",
                             "Although PTP is fully initialized on the camera, "
                             "the timestamp retrieved was not within 10ms of "
                             "the system timestamps.");
    }
    else if(type == "BadFirmware"){
        QMessageBox::warning(this, "Camera firmware warning",
                             "The camera's firmware is old and may not properly "
                             "re-sync if PTP ever goes out of sync. Please update "
                             "firmware from Allied Vision's website. ");
    }
    else if(type == "UnknownError"){
        QMessageBox::warning(this, "Unknown PTP error",
                             "Something is wrong with the computer-camera PTP setup. "
                             "Please double check that the PTP daemon is running properly, "
                             "the camera is on \"Slave\" mode, and they are on the same "
                             "network. ");
    }
    else if(type == "PTPStillSyncing"){
        QMessageBox::warning(this, "PTP Synchronizing",
                             "PTP is correctly configured, however the camera has not yet "
                             "synchronized with the computer clock. If synchronization is "
                             "not achieved within a few minutes, ensure that the \"ptpd\" "
                             "daemon is running on the correct interface.");
    }
}

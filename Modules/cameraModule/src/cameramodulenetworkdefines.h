#ifndef CAMERAMODULENETWORKDEFINES_H
#define CAMERAMODULENETWORKDEFINES_H

//#define CAMERA_MODULE_NETWORK_ID "CameraModule" //camera module base ID
static const char CAMERA_MODULE_NETWORK_ID [] = "CameraModule";

static const char CAMERA_2DPOS [] = "2Dpos";
static const char CAMERA_LIN [] = "linPos";
static const char CAMERA_LIN_TRACK [] = "linearTrack";
static const char CAMERA_VELOCITY [] = "2DVelocity";
static const char CAMERA_ZONE [] = "newZone";
static const char CAMERA_PIXELPERCM [] = "pxpercm";

#endif // CAMERAMODULENETWORKDEFINES_H

#ifndef FTDI_WRAPPER_H
#define FTDI_WRAPPER_H

//wrapper to use libftdi instead of ftd2xx's propietary lib on linux
//currently just code to allow it to compile and run properly

#include "ftdi.h"
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

typedef int FT_STATUS;
typedef struct ftdi_context* FT_HANDLE;
#define FT_OK 0

//
// FT_OpenEx Flags
//
#define FT_OPEN_BY_SERIAL_NUMBER	1
#define FT_OPEN_BY_DESCRIPTION		2
#define FT_OPEN_BY_LOCATION			4
#define FT_OPEN_MASK (FT_OPEN_BY_SERIAL_NUMBER | \
                      FT_OPEN_BY_DESCRIPTION | \
                      FT_OPEN_BY_LOCATION)
FT_STATUS FT_OpenEx(const char* desc, int flags, FT_HANDLE *handle){
    if ((*handle = ftdi_new()) == 0){
        return 1;
    }

    char modified_desc[128];
    strcpy(modified_desc, desc);
    int len = strlen(modified_desc);
    enum ftdi_interface interface;
    if(strcmp(&modified_desc[len-2], " A") == 0){
        //DockingStation A -> "DockingStation"
        modified_desc[len-2] = '\0';
        modified_desc[len-1] = '\0';
        interface = INTERFACE_A;
    }
    if(strcmp(&modified_desc[len-2], " B") == 0){
        //DockingStation B -> "DockingStation"
        modified_desc[len-2] = '\0';
        modified_desc[len-1] = '\0';
        interface = INTERFACE_B;
    }

    // int ret = ftdi_usb_open_desc(*handle, 0x0403, 0x6010, NULL, NULL);
    // printf("2: %d\n", ret);
    struct ftdi_device_list *devlist, *curdev;
    int ret, i;
    char manufacturer[128], description[128], serial[128];
    if ((ret = ftdi_usb_find_all(*handle, &devlist, 0, 0)) < 0){
        return ret;
    }

    i = 0;
    for(curdev=devlist; curdev != NULL; ++i){
        if((ret = ftdi_usb_get_strings(*handle, curdev->dev, manufacturer, 128, description, 128, serial, 128)) < 0){
            //get_strings failed
            return ret;
        }
        if(strcmp(modified_desc, description) == 0){
            //this is the device, open it, or try to
            if((ret=ftdi_usb_open_dev(*handle, curdev->dev) != 0)){
                return ret;
            }
            ret = ftdi_set_interface(*handle, interface);
            return ret;
        }
        curdev = curdev->next;
    }

    return 2;
}

#define FT_FLOW_RTS_CTS SIO_RTS_CTS_HS

//
// Purge rx and tx buffers
//
#define FT_PURGE_RX			1
#define FT_PURGE_TX			2
FT_STATUS FT_Purge(FT_HANDLE handle, int flags){ 
    int ret=0;
    if(flags & FT_PURGE_RX){
        ret |= ftdi_tciflush(handle);
    }
    if(flags & FT_PURGE_TX){
        ret |= ftdi_tcoflush(handle);
    }
    return ret;
}

FT_STATUS FT_Write(FT_HANDLE handle, void* buffer, unsigned int size, unsigned int * bytesWritten){
    int ret = ftdi_write_data(handle, buffer, size);
    if(ret >= 0){
        *bytesWritten = ret;
        ret = FT_OK;
    }
    else{
        *bytesWritten = 0;
    }


    if(*bytesWritten == size){
        return 0;
    }
    return ret;
}

FT_STATUS FT_Read(FT_HANDLE handle, void* buffer, unsigned int size, unsigned int * bytesReceived){
    //implementation that blocks until timeout is reached
    struct timeval starttime;
    gettimeofday(&starttime, NULL);

    int ret = ftdi_read_data(handle, buffer, size);
    *bytesReceived = ret;
    
    if(ret < 0){
        //error reading
        *bytesReceived = 0;
        return ret;
    }
    else if(ret == size){
        //read all bytes
        return FT_OK;
    }
    else{
        //read incomplete
        int mscur, msstart = starttime.tv_sec*1000 + starttime.tv_usec/1000;
        struct timeval current;
        do{
            usleep(1000);//sleep for 10ms at a time until timeout is reached
            ret = ftdi_read_data(handle, (unsigned char*)buffer + (*bytesReceived), size);
            if(ret < 0){
                *bytesReceived = 0;
                return ret;
            }
            else{
                *bytesReceived += ret;
            }

            gettimeofday(&current, NULL);
            mscur = current.tv_sec*1000 + current.tv_usec/1000;
        }while(*bytesReceived < size && (mscur-msstart) < handle->usb_read_timeout);

        return FT_OK;
    }
    // int ret = ftdi_read_data(handle, buffer, size);
    // // printf("Read return: %d\n", ret);
    // if(ret >= 0){
    //     *bytesReceived = ret;
    //     ret = FT_OK;
    // }
    // else{
    //     *bytesReceived = 0;
    // }

    // if(*bytesReceived == size){
    //     return 0;
    // }

    // return ret;
}

FT_STATUS FT_Close(FT_HANDLE handle){
    int ret = ftdi_usb_close(handle);
    ftdi_free(handle);
    return ret;
}

FT_STATUS FT_SetBitMode(FT_HANDLE ftHandle, unsigned char ucMask, unsigned char ucEnable){
    return ftdi_set_bitmode(ftHandle, ucMask, ucEnable);
}

FT_STATUS FT_SetUSBParameters(FT_HANDLE ftHandle, unsigned int ulInTransferSize, unsigned int ulOutTransferSize){
    int ret = 0;
    ret |= ftdi_read_data_set_chunksize(ftHandle, ulInTransferSize);
    ret |= ftdi_write_data_set_chunksize(ftHandle, ulOutTransferSize);
    return ret;
}

FT_STATUS FT_SetLatencyTimer(FT_HANDLE ftHandle, unsigned char ucLatency){
    return ftdi_set_latency_timer(ftHandle, ucLatency);
}

FT_STATUS FT_SetFlowControl(FT_HANDLE ftHandle, unsigned short FlowControl, unsigned char XonChar, unsigned char XoffChar){
    return ftdi_setflowctrl_xonxoff(ftHandle, XonChar, XoffChar);
}

FT_STATUS FT_SetChars(FT_HANDLE ftHandle, unsigned char EventChar, unsigned char EventCharEnabled, unsigned char ErrorChar, unsigned char ErrorCharEnabled){
    int ret = ftdi_set_event_char(ftHandle, EventChar,EventCharEnabled);
    ret |= ftdi_set_error_char(ftHandle, ErrorChar, ErrorCharEnabled);
    return ret;
}

FT_STATUS FT_SetTimeouts(FT_HANDLE ftHandle,unsigned int ReadTimeout, unsigned int WriteTimeout){
    ftHandle->usb_read_timeout = ReadTimeout;
    ftHandle->usb_write_timeout = WriteTimeout;
    return FT_OK;
}

FT_STATUS FT_GetQueueStatus(FT_HANDLE ftHandle, LPDWORD lpdwAmountInRxQueue){
    *lpdwAmountInRxQueue = ftHandle->readbuffer_remaining;
    return FT_OK;
}
#endif //FTDI_WRAPPER_H

#include "commandengine.h"

/*FT_STATUS ftStatus;
FT_HANDLE ftHandle;
DWORD BytesWritten, BytesReceived;
uint8_t TxBuffer[1024], RxBuffer[131072];*/


CommandEngine::CommandEngine()
{
    currentDeviceName = nullptr;
}

int CommandEngine::timeval_subtract (struct timeval *result,struct timeval *x,struct timeval *y)

{
    /* Perform the carry for the later subtraction by updating y. */
    if (x->tv_usec < y->tv_usec) {
        int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
        y->tv_usec -= 1000000 * nsec;
        y->tv_sec += nsec;
    }
    if (x->tv_usec - y->tv_usec > 1000000) {
        int nsec = (x->tv_usec - y->tv_usec) / 1000000;
        y->tv_usec += 1000000 * nsec;
        y->tv_sec -= nsec;
    }

    result->tv_sec = x->tv_sec - y->tv_sec;
    result->tv_usec = x->tv_usec - y->tv_usec;

    /* Return 1 if result is negative. */
    return x->tv_sec < y->tv_sec;
}

uint8_t CommandEngine::packet_read (
            uint8_t *buff,			/* Pointer to the data buffer to store read data */
            DWORD start,    /* First data sector */
            ULONGLONG packet,
            DWORD psize,
            WORD count,
            uint8_t readcmd) {

    DWORD bytes;
    DWORD startSector, sectors;
    int offset;
    uint8_t res;

    offset = (packet * psize)%512;
    sectors = (((count * psize) + offset)/512)+1;
    startSector = ((packet * psize)/512) + start;
    bytes = sectors * 512;

    if (bytes > 131072) {
        // too many sectors
        res = 4;
    } else {
        TxBuffer[0] = readcmd;
        TxBuffer[1] = startSector & 0xff;
        TxBuffer[2] = (startSector>>8) & 0xff;
        TxBuffer[3] = (startSector>>16) & 0xff;
        TxBuffer[4] = (startSector>>24) & 0xff;
        TxBuffer[5] = sectors & 0xff;
        TxBuffer[6] = (sectors>>8) & 0xff;
        TxBuffer[7] = (sectors>>16) & 0xff;
        TxBuffer[8] = (sectors>>24) & 0xff;
        ftStatus = FT_Write(ftHandle, TxBuffer, 9, &BytesWritten);
        if (ftStatus != FT_OK) {
            printf("Error: USB - Write (FT_Write) failed!\n");
            res = 1;
        } else {
            ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
            if (ftStatus != FT_OK) {
                printf("Error: USB - Read (FT_Read) failed!\n");
                res = 1;
            } else {
                if (BytesReceived != bytes) {
                    // did not get all bytes bytes -> timeout!
                    printf("Error: USB - Timeout! bytes received vs expected: %d (%d)\n", (int)BytesReceived, (int)bytes);
                    res = 1;
                } else {
                    memcpy(buff, RxBuffer+offset, psize*count);
                    res = 0;
                }
            }
        }
    }
    return res;
}

int CommandEngine::strendswith(const char *str, const char *suffix)
{
    if (!str || !suffix)
        return 0;
    size_t lenstr = strlen(str);
    size_t lensuffix = strlen(suffix);
    if (lensuffix >  lenstr)
        return 0;
    return strncmp(str + lenstr - lensuffix, suffix, lensuffix) == 0;
}

void CommandEngine::setCurrentDeviceName(char* dev) {
    currentDeviceName = dev;
}

// int main ( int argc, char *argv[] ) {
int CommandEngine::readDockingstation(DockingStation_t *d, int detect, int readconfig, int pcheck, int extract, const char* filename, int trodesparse){
    //readDockingStation does things in order: detect, readconfig, pcheck, extract
    //if a later step is toggled, all steps before should also be toggled.
    //if trodesparse, they are all toggled.
    detect = detect || readconfig || pcheck || extract || trodesparse;
    readconfig = readconfig || pcheck || extract || trodesparse;
    pcheck = pcheck || extract || trodesparse;
    char* tmpDevice = d->deviceName;
    memset(d, 0, sizeof(*d));
    d->deviceName = tmpDevice;
    uint8_t Mask = 0xff;
    uint8_t Mode;
    DWORD bytes;
    uint64_t blocks;
    uint8_t res;
    uint16_t w;
    uint64_t dw;
    uint64_t start;
    uint32_t psize;
    uint32_t auxsize;
    // uint32_t sample_size, sample_rate;
    uint32_t channels;
    uint64_t maxPackets, lastPacket, packets_read, total_packets;
    uint64_t droppedPackets;
    uint32_t progress, lastProgress;
    int i, j;
    uint32_t shift;
    FILE *fp;
    uint8_t Buff[131072];
    int p1, p2;
    int bytesWritten;
    //struct timeval t_start, t_end, t_diff;
    uint32_t total_sectors, packets_to_read;
    uint64_t total_bytes;
    uint32_t excess_bytes, bytes_to_write;
    unsigned char id;

    setvbuf(stdout, 0, _IONBF, 0);

    start = 1;
    psize = 0;
    // sample_size = 0;
    // sample_rate = 30;
    auxsize = 0;
    id = 0;
    d->dock_USB_communication_ok = 0;
    d->sd_card_enabled_for_record = -1;

    printf("Attempting to open %s\n", d->deviceName);
    //ftStatus = FT_OpenEx((void*)desc,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    ftStatus = FT_OpenEx((void*)d->deviceName,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    if (ftStatus != FT_OK) {
        printf("Device not found.\n");
        //FT_STATUS ftStatus;
        FT_HANDLE ftHandleTemp;
        DWORD numDevs;
        DWORD Flags;
        DWORD ID;
        DWORD Type;
        DWORD LocId;
        char SerialNumber[16];
        char Description[64];
        // create the device information list
        /*ftStatus = FT_CreateDeviceInfoList(&numDevs);
        if (ftStatus == FT_OK) {
               printf("Number of available USB devices is %d\n",numDevs);
        } else {
            printf("Error: FT_CreateDeviceInfoList(%d)\n", (int)ftStatus);
        }
        if (numDevs > 0) {
               // get information for device 0
               for (int i=0; i<numDevs;i++) {
                    ftStatus = FT_GetDeviceInfoDetail(i, &Flags, &Type, &ID, &LocId, SerialNumber,Description,&ftHandleTemp);
                    if (ftStatus == FT_OK) {

                        printf("Device %d Serial Number - %s Desciption - %s\n", i, SerialNumber,Description);
                    }
               }
        }*/

        return 1;
    }

    if(!detect){
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 0;
    }

    Mode = 0x40; //Sync FIFO mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set sync mode! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    /*res = FT_ResetDevice(ftdi);
    res |= FT_SetUSBParameters(ftdi, 65536, 65536);	//Set USB request transfer size
    res |= FT_SetFlowControl(ftdi,FT_FLOW_RTS_CTS,0,0);
    res |= FT_SetChars(ftdi, false, 0, false, 0);	 //Disable event and error characters
    res |= FT_SetBitMode(ftdi, 0xff, 0x40);
    res |= FT_Purge(ftdi, FT_PURGE_RX | FT_PURGE_TX);
    res |= FT_SetLatencyTimer(ftdi, 64);
    res |= FT_SetTimeouts(ftdi, 1000, 1000);*/

    //ftStatus = FT_SetUSBParameters(ftHandle, 65536, 65536);	//Set USB request transfer size
    ftStatus = FT_SetUSBParameters(ftHandle, 64000, 64000);	//Set USB request transfer size
    ftStatus |= FT_SetFlowControl(ftHandle,FT_FLOW_RTS_CTS,0,0);
    ftStatus |= FT_SetChars(ftHandle, false, 0, false, 0);	 //Disable event and error characters
    ftStatus |= FT_SetTimeouts(ftHandle, 1000, 0);		//Sets the read and write timeouts in milliseconds for the FT2232H
    ftStatus |= FT_SetLatencyTimer(ftHandle, 2);		//Set the latency timer
    //ftStatus |= FT_SetLatencyTimer(ftHandle, 255);		//Set the latency timer
    ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX); // Clear buffers
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set parameters! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    // get docking station status
    // printf("Reading dock status... \n");
    TxBuffer[0] = SD_GET_STATUS_CMD;
    ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    bytes = 4; // expect 4 bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        RxBuffer[0] = 0;

        /*printf("  Timeout! bytes received: %d \n", (int)BytesReceived);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;*/
    }

    d->dock_USB_communication_ok = 1;

    if ((RxBuffer[0] & STATUS_MOUNTED) == STATUS_MOUNTED){
        // printf("  SD-card detected\n");
        d->sd_detected = 1;
    }

    TxBuffer[0] = HS_GET_STATUS_CMD;
    ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    bytes = 4; // expect 4 bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        //printf("  Timeout when requesting headstage data. Bytes received: %d \n", (int)BytesReceived);
        RxBuffer[0] = 0;
        /*Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;*/
    }

    /*for (int op=0;op<bytes;op++) {
        printf("Byte %d",RxBuffer[op]);
    }*/

    if ((RxBuffer[0] & STATUS_MOUNTED) == STATUS_MOUNTED){
        //printf("  Headstage detected\n");
        d->hs_detected = 1;
    }

    uint8_t getsizecmd = 0, readsectorscmd = 0;
    if(d->hs_detected){
        getsizecmd = HS_GET_SIZE_CMD;
        readsectorscmd = HS_READ_SECTORS_CMD;
    }
    else if(d->sd_detected){
        getsizecmd = SD_GET_SIZE_CMD;
        readsectorscmd = SD_READ_SECTORS_CMD;
    }
    else{
        //nothing detected
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);

        if (extract) {
            printf("Error: No SD card or headstage detected on the device.\n");
            //return 1;
            return 1;
        } else {
            return 0;
        }
    }

    // get disk size
    // printf("Reading disk size... \n");
    TxBuffer[0] = getsizecmd;
    ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    bytes = 4; // expect 4 bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        printf("Error: USB - Timeout! bytes received: %d \n", (int)BytesReceived);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    // printf("GET_SIZE: %02X %02X %02X %02X\n", RxBuffer[0], RxBuffer[1], RxBuffer[2], RxBuffer[3]);
    blocks = (RxBuffer[3] << 24) | (RxBuffer[2] << 16) | (RxBuffer[1] << 8) | RxBuffer[0];
    // printf("  Disk size: %d MB\n", blocks/2);
    d->disk_size_MB = blocks/2;


    if(!readconfig){
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 0;
    }


    // read first 5 sectors to get config info and first packet
    TxBuffer[0] = readsectorscmd;
    TxBuffer[1] = 0x00; // start sector
    TxBuffer[2] = 0x00;
    TxBuffer[3] = 0x00;
    TxBuffer[4] = 0x00;
    TxBuffer[5] = 0x05; // sector count
    TxBuffer[6] = 0x00;
    TxBuffer[7] = 0x00;
    TxBuffer[8] = 0x00;
    ftStatus = FT_Write(ftHandle, TxBuffer, 9, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    bytes = 2560; // expect 5 * 512 bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        printf("Error: USB - Timeout! bytes received: %d \n", (int)BytesReceived);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    // for(int i = 0; i < 512; ++i) printf("%02X ", RxBuffer[i]); printf("\n");
    // // Is the disk used for spike recordings?
    // // see if there is a partition table
    // for (i = 0x1be; i < 0x1ff; i++) {
    //     if (RxBuffer[i] != 0) {
    //     // looks like there is!
    //     printf("\n************************************************************\n");
    //     printf("WARNING: This does not look like a disk used for recordings.\n");
    //     printf("Are you sure this is the correct disk?\n");
    //     printf("************************************************************\n\n");
    //     printf("Continue? (yes or no) ");
    //     match = scanf("%s", answer);
    //     if (match == 0 || strcmp(answer,"yes")) {
    //         printf("Aborting...\n");
    //         Mode = 0x00; //reset mode
    //         ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    //         ftStatus = FT_Close(ftHandle);
    //         return 0;
    //     }
    //     break;
    //     }
    // }

    // Process the data in the config sector
    // printf("Reading configuration data:\n");
    channels = 0;
    for (i = 0; i < 32; i++) {
        if (RxBuffer[i]) {
            for (j = 0; j < 8; j++) {
                if ((RxBuffer[i] >> j) & 0x01) {
                channels++;
                }
            }
        }
    }
    d->channels = channels;

    if (RxBuffer[32] & 0x01)
        d->waitforstart_override = 1;
    else
        d->waitforstart_override = 0;

    if (RxBuffer[32] & 0x02)
        d->cardenablecheck_override = 1;
    else
        d->cardenablecheck_override = 0;

    if (RxBuffer[32] & 0x04)
        d->sample_rate_khz = 20;
    else
        d->sample_rate_khz = 30;

    if (RxBuffer[32] & 0x08)
        d->mag_enabled = 0;
    else
        d->mag_enabled = 1;

    if (RxBuffer[32] & 0x10)
        d->acc_enabled = 0;
    else
        d->acc_enabled = 1;

    if (RxBuffer[32] & 0x20)
        d->gyr_enabled = 0;
    else
        d->gyr_enabled = 1;

    if (RxBuffer[32] & 0x40)
        d->smartref_enabled = 1;
    else
        d->smartref_enabled = 0;

    if (RxBuffer[32] & 0x80)
        d->sample_size_bits = 12;
    else
        d->sample_size_bits = 16;

    if (RxBuffer[33] > 0) {
        d->rf_channel = (int)RxBuffer[33];
    }
    else
        d->rf_channel = 2;

    if (RxBuffer[34] > 0 || RxBuffer[35] > 0) {
    //   printf("\nAutofs threshold set to %d (%.2f mV) \n",
            //  (int)(RxBuffer[35]<<8 | RxBuffer[34]), (float)((int)((RxBuffer[35]<<8 | RxBuffer[34])))/5128);
        d->autofs_threshold =  (int)(RxBuffer[35]<<8 | RxBuffer[34]);
        d->autofs_threshold_mV = (float)((int)((RxBuffer[35]<<8 | RxBuffer[34])))/5128;
    }
    else{
        d->autofs_threshold = -1;
        d->autofs_threshold_mV = -1;
    }

    if (RxBuffer[36] > 0 || RxBuffer[37] > 0) {
    //   printf("Autofs channels set to %d\n", (int)(RxBuffer[37]<<8 | RxBuffer[36]));
        d->autofs_channels = (int)(RxBuffer[37]<<8 | RxBuffer[36]);
    }
    else{
        d->autofs_channels = -1;
    }
    if (RxBuffer[38] > 0 || RxBuffer[39] > 0) {
    //   printf("Autofs timeout set to %d samples\n", (int)(RxBuffer[39]<<8 | RxBuffer[38]));
        d->autofs_timeout_samples = (int)(RxBuffer[39]<<8 | RxBuffer[38]);
    }
    else{
        d->autofs_timeout_samples = -1;
    }


    // printf("  %d channels enabled, sample size = %d bits, sample rate = %d kHz\n", channels, sample_size, sample_rate);

    if ((RxBuffer[512] == 0xaa) &&
            (RxBuffer[513] == 0x55) &&
            (RxBuffer[514] == 0xc3) &&
            (RxBuffer[515] == 0x3c)) {
        //printf("Card is enabled for recording.\n");
        d->sd_card_enabled_for_record = 1;

    } else {
        //printf("Card is NOT enabled for recording.\n");
        d->sd_card_enabled_for_record = 0;
    }

    // See if we have a hw info sector
    if (RxBuffer[512] == 0xc3) {
        // hwinfo
        time_t epoch;
        struct tm ts;
        char tbuf[80];
        unsigned int hwinfo_version;
        unsigned int serial;
        unsigned int model;
        unsigned int hwminor;
        unsigned int hwmajor;
        unsigned int hwpatch;
        uint16_t batterysize;
        uint64_t chipid;
        uint8_t sensorversion;
        uint32_t serialsensor;
        uint16_t intanchannels;

        // printf("Reading headstage info:\n");
        epoch = RxBuffer[513]<<24 | RxBuffer[514]<<16 | RxBuffer[515]<<8 | RxBuffer[516];
        serial = RxBuffer[518]<<8 | RxBuffer[517];
        model = RxBuffer[520]<<8 | RxBuffer[519];
        batterysize = RxBuffer[522]<<8 | RxBuffer[521];
        hwminor = RxBuffer[523];
        hwmajor = RxBuffer[524];
        chipid =  (uint64_t)RxBuffer[532]<<56 | (uint64_t)RxBuffer[531]<<48 | (uint64_t)RxBuffer[530]<<40 | (uint64_t)RxBuffer[529]<<32 |
                (uint64_t)RxBuffer[528]<<24 | (uint64_t)RxBuffer[527]<<16 | (uint64_t)RxBuffer[526]<<8 | (uint64_t)RxBuffer[525];
        sensorversion = RxBuffer[533];
        serialsensor = RxBuffer[537]<<24 | RxBuffer[536]<<16 | RxBuffer[535]<<8 | RxBuffer[534];
        intanchannels = RxBuffer[539]<<8 | RxBuffer[538];
        hwinfo_version = RxBuffer[540];
        if (hwinfo_version == 0) {
            start = 2;
            hwpatch = 0;
            // printf("  Headstage serial number: %u\n", serial);
            // printf("  Headstage model number: %u\n", model);
            // printf("  Headstage hw version: %u.%u\n", hwmajor, hwminor);
        } else {
            hwpatch = RxBuffer[541];
            start = RxBuffer[545]<<24 | RxBuffer[544]<<16 | RxBuffer[543]<<8 | RxBuffer[542];
            auxsize = RxBuffer[546];
            id = RxBuffer[549];
            if (RxBuffer[547] & 0x01)
                d->acc_enabled = 0;
            else
                d->acc_enabled = 1;

            if (RxBuffer[547] & 0x02)
                d->gyr_enabled = 0;
            else
                d->gyr_enabled = 1;

            if (RxBuffer[547] & 0x04)
                d->mag_enabled = 0;
            else
                d->mag_enabled = 1;

            if (RxBuffer[548] > 0)
                d->rf_channel = (int)RxBuffer[548];
            else
                d->rf_channel = 2;
            if (RxBuffer[551] == 0) {
                // neuropixels probe
                auxsize = (((auxsize - 10) * 10) / 16) + 10;
                d->sample_rate_khz = 30;
                d->sample_size_bits = 10;
                d->smartref_enabled = 0;
            } else if (RxBuffer[551] == 1) {
                // intan probe
                if (RxBuffer[550] & 0x01)
                    d->sample_size_bits = 12;
                else
                    d->sample_size_bits = 16;

                if (RxBuffer[550] & 0x02)
                    d->sample_rate_khz = 20;
                else
                    d->sample_rate_khz = 30;
                if (RxBuffer[550] & 0x04)
                    d->smartref_enabled = 1;
                else
                    d->smartref_enabled = 0;
            }
            // printf("  Headstage serial number: %u\n", serial);
            // printf("  Headstage model number: %u\n", model);
            // printf("  Headstage hw version: %u.%u.%u\n", hwmajor, hwminor, hwpatch);
        }

        d->hs_serial_number = serial;
        d->hs_model_number = model;
        d->batterysize_mAh = batterysize;
        d->hs_major_version = hwmajor;
        d->hs_minor_version = hwminor;
        d->hs_patch_version = hwpatch;
        d->chipid = chipid;
        d->sensor_version = sensorversion;
        d->serial_sensor = serialsensor;
        d->intan_channels = intanchannels;
        if (epoch > 0) {
            ts = *localtime(&epoch);
            strftime(tbuf, sizeof(tbuf), "%a %Y-%m-%d %H:%M:%S %Z", &ts);
            // printf("Recording date: %s\n", tbuf);
            strcpy(d->recording_datetime, tbuf);
        }
    } else {
        start = 1;
        d->hs_serial_number = -1;
        d->hs_model_number = -1;
        d->hs_major_version = 0;
        d->hs_minor_version = 0;
        d->hs_patch_version = 0;
    }

    d->configvalid = 1;
    d->start = start;

    if(!pcheck){
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 0;
    }

    //printf("Running pcheck\n");
    // read first 8 sectors
    TxBuffer[0] = readsectorscmd;
    TxBuffer[1] = start & 0xff;
    TxBuffer[2] = (start>>8) & 0xff;
    TxBuffer[3] = (start>>16) & 0xff;
    TxBuffer[4] = (start>>24) & 0xff;
    TxBuffer[5] = 0x08; // sector count
    TxBuffer[6] = 0x00;
    TxBuffer[7] = 0x00;
    TxBuffer[8] = 0x00;
    ftStatus = FT_Write(ftHandle, TxBuffer, 9, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    bytes = 4096; // expect 8 * 512 bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        printf("Error: USB - Timeout! bytes received: %d \n", (int)BytesReceived);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    // Look for packet start
    if (RxBuffer[0] != 0x55) {

        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        if (extract) {
            printf("Error: No data found on the SD card or headstage. Extraction aborted.\n");
            return 1;
        } else {
            return 0;
        }
    }


    // Look for packet start
    if (auxsize > 100000) {
        printf("Error: Aux size calculation too large!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    if (id == 0)
        id = RxBuffer[1];
    // Figure out the packet size

    // search for the next packet header
    // first assume no sensor data
    //printf("Finding packet size\n");
    i =  1;

    if (auxsize > 0) {

        while (((RxBuffer[i] != 0x55) || (RxBuffer[i+1] != id) || (RxBuffer[i+auxsize+2] != 0x01) ||
                (RxBuffer[i+auxsize+3] != 0x00) || (RxBuffer[i+auxsize+4] != 0x00) ||
                (RxBuffer[i+auxsize+5] != 0x00)) && (i++ < 4095));


        if (i == 4096) {
            // printf("Error: Recording - Can't find the second packet start!\n");
            Mode = 0x00; //reset mode
            ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
            ftStatus = FT_Close(ftHandle);
            return 2;
        }
    } else {

        while (((RxBuffer[i] != 0x55) || (RxBuffer[i+1] != id) || (RxBuffer[i+2] != 0x01) ||
                (RxBuffer[i+3] != 0x00) || (RxBuffer[i+4] != 0x00) ||
                (RxBuffer[i+5] != 0x00)) && (i++ < 4095));
        if (i == 4096) {
            // next try with 8 byte sensor data
            i = 1;
            auxsize = 8;
            while (((RxBuffer[i] != 0x55) || (RxBuffer[i+1] != id) || (RxBuffer[i+10] != 0x01) ||
                    (RxBuffer[i+11] != 0x00) || (RxBuffer[i+12] != 0x00) ||
                    (RxBuffer[i+13] != 0x00)) && (i++ < 4095));
            if (i == 4096) {
            // printf("Error: Recording - Can't find the second packet start!\n");
            Mode = 0x00; //reset mode
            ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
            ftStatus = FT_Close(ftHandle);
            return 2;
            }
        }
    }
    psize = i;

    // printf("\nPacket size: %u bytes/packet\n", psize);
    d->packet_size_bytes = psize;

    //   if (sample_size == 12) {
    //     auxsize = (psize % 24) - 6;
    //   }
    //   else {
    //     auxsize = (psize % 32) - 6;
    //   }

    // see if we need to double the channel count for Jan Z special
    // if (((d->sample_size_bits == 12) && (psize-auxsize-6 == channels*3)) ||
    //     ((d->sample_size_bits == 16) && (psize-auxsize-6 == channels*4))) {
    //         channels *= 2;
    //        d->channels = channels;
    // }

    // figure out the recorded channel count based on the packet size
    channels = ((psize - auxsize - 6) * 8) / d->sample_size_bits;
    d->channels = channels;

    // if (auxsize)
    //   printf("Aux data size: %u bytes\n", auxsize);

    d->aux_size_bytes = auxsize;
    d->trodes_packet_size = 6 + auxsize + channels*2;

    // verify the packet size
    if ((RxBuffer[psize] != 0x55) || (RxBuffer[psize+auxsize+2] != 0x01) ||
        (RxBuffer[psize+auxsize+3] != 0x00) || (RxBuffer[psize+auxsize+4] != 0x00) ||
        (RxBuffer[psize+auxsize+5] != 0x00)) {
        printf("Error: Recording - Packet size mismatch\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 2;
    }

    w = 512;
    dw = blocks * 1024;
    maxPackets = ((((ULONGLONG)dw - start) * w)/psize);
    // printf("Maximum packets on the disk = %lu\n", maxPackets);
    d->max_packets = maxPackets;

    lastPacket = 1;
    shift = 0;
    while (lastPacket < maxPackets) {
        shift++;
        lastPacket = lastPacket<<1;
    }
    lastPacket = 0;

    //Starting from a value where the most significant bit resulted in a hit, scan towards the end for sync bytes to determine the exact location of the last packet.
    for (i=shift; i>=0; i--) {
        lastPacket |= (1<<i);
        if (lastPacket >= maxPackets) {
            lastPacket &= ~(1<<i);
            continue;
        }
        res = packet_read(Buff, start, lastPacket, psize, 1, readsectorscmd);
        if (res) {
            printf("Error: SD-card packet_read failed, rc=%d\n", (WORD)res);
            break;
        } else if ((Buff[0] != 0x55) || (Buff[1] != id)) {
            //We are likely past the end of the recording. But there is a chance that this was a failed write command, which can result in up to 1024 sectors of 0's. We test this by reading a packet a 1024 sectors later.
            if (lastPacket < (maxPackets-((1024*512)/psize)-1)) {
                res = packet_read(Buff, start, lastPacket+((1024*512)/psize)+1, psize, 1, readsectorscmd);
                //If no sync byte is found there either, then we can be fairly confident that we are past the end
                if ((Buff[0] != 0x55) || (Buff[1] != id)) {
                    //Flip the bit back to zero
                    lastPacket &= ~(1<<i);
                }
            } else {
                //Flip the bit back to zero
                lastPacket &= ~(1<<i);
            }
        }
    }
    if (lastPacket < (maxPackets-1))
        // Disk not full, don't use the last recorded packet since it might not be complete
        lastPacket--;

    // printf("Packets recorded on the disk = %lu\n", lastPacket+1);
    d->recorded_packets = lastPacket+1;

    res = packet_read(Buff, start, lastPacket, psize, 1, readsectorscmd);
    if (res) {
        printf("Error: SD-card packet_read failed, rc=%d\n", (WORD)res);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 2;
    }
    droppedPackets = (Buff[auxsize+5]<<24 | Buff[auxsize+4]<<16 | Buff[auxsize+3]<<8 | Buff[auxsize+2]) - lastPacket;
    d->dropped_packets = droppedPackets;
    d->packetsvalid = 1;

    // if (droppedPackets)
    //   printf("Dropped packets = %lu (%.2f ms)\n", droppedPackets, (float)droppedPackets/sample_rate);
    // else
    //   printf("No dropped packets\n");


    //This part of the function deals with extracting all of the data saved on the SD card.

    if(extract){
        if (strlen(filename)) {
            if(strendswith(filename, ".rec")){
                //append data after trodes config (TODO: verify validity of trodesconf)
                fp = fopen(filename, "ab");
            }
            else{
                fp = fopen(filename, "wb");
            }
        } else {
            fp = fopen("data.dat", "wb");
        }
        if (!fp) {
            printf("Error: Can't open file %s for writing\n", filename);
            return 2;
        }
    }
    else{
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 0;
    }
    printf("Extracting data\n");

    packets_read = 0;
    progress = 0;
    lastProgress = 0;
    total_packets = lastPacket+1;

    // calculate the total number of sectors to read
    total_sectors = (psize * total_packets)/512 + 1;
    total_bytes = total_sectors * 512;

    // calculate number of excess bytes on the last sector
    excess_bytes = total_bytes - (psize * total_packets);

    //------------------------------
    uint64_t numberOfSectorsToReadPerCommand = 1000; //This is the number of sectors we will ask for per transfer cycle.
    uint64_t currentSector = start;
    uint64_t sectors_read = 0;
    //uint64_t cleared_sector_marker = start;
    uint64_t total_bytes_cleared = 0;
    int bytesToFF = 0;

    int bytesLeftToProcess = 0;
    //int bytesLeftFromLastCycle = 0;
    bool recoveryNeeded = false;
    bool badDataSectionEncountered = false;
    int numConsecRecoveriesTried = 0;


    //Main transfer cycle loop
    while (sectors_read < total_sectors) {

        if (numberOfSectorsToReadPerCommand > (total_sectors-sectors_read)) {
            numberOfSectorsToReadPerCommand = total_sectors-sectors_read;
        }

        // Send a command to the dock (via USB) instructing it to stream the requested
        //sectors via USB
        TxBuffer[0] = readsectorscmd;
        TxBuffer[1] = currentSector & 0xff;
        TxBuffer[2] = (currentSector>>8) & 0xff;
        TxBuffer[3] = (currentSector>>16) & 0xff;
        TxBuffer[4] = (currentSector>>24) & 0xff;
        TxBuffer[5] = numberOfSectorsToReadPerCommand & 0xff;
        TxBuffer[6] = (numberOfSectorsToReadPerCommand>>8) & 0xff;
        TxBuffer[7] = (numberOfSectorsToReadPerCommand>>16) & 0xff;
        TxBuffer[8] = (numberOfSectorsToReadPerCommand>>24) & 0xff;
        ftStatus = FT_Write(ftHandle, TxBuffer, 9, &BytesWritten);

        //Did the write command send properly? If not we need to abort.
        if (ftStatus != FT_OK) {
            printf("Error: USB - Write (FT_Write) failed!\n");
            Mode = 0x00; //reset mode
            ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
            ftStatus = FT_Close(ftHandle);
            fclose(fp);
            return 1;

        }

        int packets_cleared_in_cycle = 0;
        int leftOverByteFromLastTransfer = bytesLeftToProcess;

        if (recoveryNeeded) {
            if (numConsecRecoveriesTried > 5) {
                //We have tried to recover too many times, so we need to give up and scan past this section
                badDataSectionEncountered = true;

                printf("Error: a corrupted section of the recording was encountered. Trying to skip past it.\n");

                //Reset the dock
                /*TxBuffer[0] = SD_RESET_CMD;
                ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);

                //Close the connection
                Mode = 0x00; //reset mode
                ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
                ftStatus = FT_Close(ftHandle);
                fclose(fp);
                return 1;*/

            }

            //A recovery from a transfer error occured, so we need to fast-foreward to the current byte in the sector
            ftStatus = FT_Read(ftHandle, RxBuffer, bytesToFF, &BytesReceived);

            //Did the read operation work?
            if (ftStatus != FT_OK) {
                printf("Error: USB - Read (FT_Read) failed after attenpted recovery.\n");
                Mode = 0x00; //reset mode
                ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
                ftStatus = FT_Close(ftHandle);
                fclose(fp);
                return 1;
            }
            //Did we get the expected amount of data back?
            else if (BytesReceived != bytesToFF) {
                // did not get all bytes bytes -> timeout!
                printf("Error: USB - Timeout! bytes received: %d Bytes expected: %d \n", (int)BytesReceived, (int) bytesToFF);

                Mode = 0x00; //reset mode
                ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
                ftStatus = FT_Close(ftHandle);
                fclose(fp);
                return 1;

            } else {
                //Recovery process completed.              
                bytesLeftToProcess = (numberOfSectorsToReadPerCommand*512)-bytesToFF;
                recoveryNeeded = false;
                numConsecRecoveriesTried++;
            }



        } else {
            //No recovery needed, so we add all of the trasfered sectors to the pile
            bytesLeftToProcess += numberOfSectorsToReadPerCommand*512;
            numConsecRecoveriesTried = 0;
        }

        int packets_left_in_buffer = bytesLeftToProcess/psize;


        // Calculate the maximum number of packets to read in each FT_Read()
        // FT_Read() max request is 65536 bytes
        if (packets_left_in_buffer > (64000/psize)) {
            packets_to_read = (64000/psize);
        } else {
            packets_to_read = packets_left_in_buffer;
        }

        while ((!recoveryNeeded) && (packets_to_read > 0)) {
            ftStatus = FT_Read(ftHandle, RxBuffer, packets_to_read * psize, &BytesReceived);

            //Did the read operation work?
            if (ftStatus != FT_OK) {
                printf("Error: USB - Read (FT_Read) failed!\n");

                recoveryNeeded = true;
            }

            //Did we get the expected amount of data back?
            if (BytesReceived != packets_to_read * psize) {
                // did not get all bytes bytes -> timeout!
                //printf("Error: USB - Timeout! bytes received: %d Bytes expected: %d Total bytes processed: %d Packets left: %d)\n", (int)BytesReceived, packets_to_read*psize, packets_cleared_in_cycle*psize, packets_left_in_buffer);

                recoveryNeeded = true;
            }

            //Is the data ok?
            bool foundGoodSync = false;
            for (i = 0; i < packets_to_read; i++) {
                if ((RxBuffer[i*psize] != 0x55) && (!badDataSectionEncountered)) {
                    //printf("\nError: Malformed data! Missing packet header\n");

                    recoveryNeeded = true;
                    break;
                }
                if (RxBuffer[i*psize] == 0x55) {
                    foundGoodSync = true;
                }
            }
            if ((foundGoodSync) && (badDataSectionEncountered)) {
                //We have scanned past a bad section of data
                badDataSectionEncountered = false;
                printf("\nBad data section skipped.\n");
            }


            if (recoveryNeeded) {
                //Something went wrong in this last transfer cycle. We need to try it again.

                //Reset the dock
                TxBuffer[0] = SD_RESET_CMD;
                ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
                if (ftStatus != FT_OK) {
                    printf("Error: USB - Write (FT_Write) failed.\n");

                    return 1;
                }

                //Completely reset the USB connection
                Mode = 0x00; //reset mode
                ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
                ftStatus = FT_Close(ftHandle);

                ftStatus = FT_OpenEx((void*)d->deviceName,FT_OPEN_BY_DESCRIPTION,&ftHandle);
                //ftStatus = FT_OpenEx((void*)desc,FT_OPEN_BY_DESCRIPTION,&ftHandle);
                if (ftStatus != FT_OK) {
                    printf("Can't re-init FT2232H device! \n");
                    return 1;
                }

                Mode = 0x40; //Sync FIFO mode
                ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
                if (ftStatus != FT_OK) {
                    printf("Error: USB - Can't set sync mode! \n");
                    Mode = 0x00; //reset mode
                    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
                    ftStatus = FT_Close(ftHandle);
                    return 1;
                }

                ftStatus = FT_SetUSBParameters(ftHandle, 64000, 64000);	//Set USB request transfer size
                ftStatus |= FT_SetFlowControl(ftHandle,FT_FLOW_RTS_CTS,0,0);
                ftStatus |= FT_SetChars(ftHandle, false, 0, false, 0);	 //Disable event and error characters
                ftStatus |= FT_SetTimeouts(ftHandle, 1000, 0);		//Sets the read and write timeouts in milliseconds for the FT2232H
                ftStatus |= FT_SetLatencyTimer(ftHandle, 2);		//Set the latency timer
                //ftStatus |= FT_SetLatencyTimer(ftHandle, 255);		//Set the latency timer
                ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX); // Clear buffers
                if (ftStatus != FT_OK) {
                    printf("Error: USB - Can't set parameters! \n");
                    Mode = 0x00; //reset mode
                    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
                    ftStatus = FT_Close(ftHandle);
                    return 1;
                }

                int bytesLeftFromLastTransfer = bytesLeftToProcess - ((int)numberOfSectorsToReadPerCommand*512);
                int bytesProcessedFromThisTransfer;


                if (bytesLeftFromLastTransfer < 0) {
                    //The issue occured after whatever was left from the previous transfer cycle,
                    //so we don't need to rewind to that.
                    bytesLeftFromLastTransfer = 0;
                    currentSector = (total_bytes_cleared/512);
                    bytesToFF = total_bytes_cleared-(currentSector*512);
                    bytesProcessedFromThisTransfer =  ((int)numberOfSectorsToReadPerCommand*512) - bytesLeftToProcess;

                } else {
                    bytesProcessedFromThisTransfer = 0;
                    currentSector = (total_bytes_cleared/512);
                    bytesToFF = total_bytes_cleared-(currentSector*512);

                }

                if (numConsecRecoveriesTried > 3) {
                    //We have tried multiple times to recover, so now we start printing out the details before
                    //we give up completely.
                    printf("Attempting recovery.\n");
                    printf("Sectors tranferred before current transfer: %d \n"
                           "Bytes that initially remained from that transfer %d \n"
                           "Bytes left to process from that transfer %d \n"
                           "Bytes processed successfully from the current transfer %d \n"
                           "Packet size: %d \n"
                           "Recovering at sector %d \n"
                           "Fast-forward num bytes: %d \n", (int)sectors_read, leftOverByteFromLastTransfer, bytesLeftFromLastTransfer,bytesProcessedFromThisTransfer,psize,(int)currentSector,(int)bytesToFF);
                }
                bytesLeftToProcess = 0;
                numberOfSectorsToReadPerCommand = 100; //Reduce read size just for the recovery
                //sectors_read += (bytesProcessedFromThisTransfer/512); //we will skip the sectors that were succesfully processed
                sectors_read = currentSector;
                currentSector += start;

            } else {

                //Everything checks out, so now we process the data and write to file.

                if (d->sample_size_bits == 10) {
                    //10-bit recording. We need to convert to 16-bit.

                    p1 = 0;
                    p2 = 0;
                    for (j = 0; j < packets_to_read; j++) {
                        // copy the first part of the packet header
                        memcpy(&Buff[p2], &RxBuffer[p1], 12);
                        p1 += 12;
                        p2 += 12;
                        // unpack the 10-bit lfp data to Buff2
                        for (i = 0; i < (auxsize-10)*8/10; i+=4) {
                            Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                            Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                            Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                            Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                            Buff[p2+4] = (RxBuffer[p1+2] << 2) & 0xc0;
                            Buff[p2+5] = (RxBuffer[p1+2] >> 6) | (RxBuffer[p1+3] << 2);
                            Buff[p2+6] = (RxBuffer[p1+3] << 0) & 0xc0;
                            Buff[p2+7] = (RxBuffer[p1+3] >> 8) | (RxBuffer[p1+4] << 0);
                            p2 += 8;
                            p1 += 5;
                        }
                        // copy the timestamp
                        memcpy(&Buff[p2], &RxBuffer[p1], 4);
                        p1 += 4;
                        p2 += 4;
                        // unpack the 10-bit ap data to Buff
                        for (i = 0; i < channels-3; i+=4) {
                            Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                            Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                            Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                            Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                            Buff[p2+4] = (RxBuffer[p1+2] << 2) & 0xc0;
                            Buff[p2+5] = (RxBuffer[p1+2] >> 6) | (RxBuffer[p1+3] << 2);
                            Buff[p2+6] = (RxBuffer[p1+3] << 0) & 0xc0;
                            Buff[p2+7] = (RxBuffer[p1+3] >> 8) | (RxBuffer[p1+4] << 0);
                            p2 += 8;
                            p1 += 5;
                        }
                        // must take care of runt data in case channels is not a multiple of 4
                        switch (channels % 4) {
                        case 3:
                            Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                            Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                            Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                            Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                            Buff[p2+4] = (RxBuffer[p1+2] << 2) & 0xc0;
                            Buff[p2+5] = (RxBuffer[p1+2] >> 6) | (RxBuffer[p1+3] << 2);
                            p2 += 6;
                            p1 += 4;
                            break;
                        case 2:
                            Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                            Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                            Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                            Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                            p2 += 4;
                            p1 += 3;
                            break;
                        case 1:
                            Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                            Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                            p2 += 2;
                            p1 += 2;
                            break;
                        }
                    }
                    bytes_to_write = p2;
                    bytesWritten = fwrite(Buff, 1, bytes_to_write, fp);
                } else if (d->sample_size_bits == 12) {
                    // 12-bit data: expand packet data from 12 to 16 bits
                    p1 = 0;
                    p2 = 0;
                    for (j = 0; j < packets_to_read; j++) {
                        // copy the packet header
                        memcpy(&Buff[p2], &RxBuffer[p1], auxsize + 6);
                        p1 += auxsize + 6;
                        p2 += auxsize + 6;
                        // unpack the 12-bit packet data from RxBuffer to Buff
                        for (i = 0; i < channels; i+=2) {
                            Buff[p2]   = RxBuffer[p1] << 4;
                            Buff[p2+1] = (RxBuffer[p1] >> 4) | (RxBuffer[p1+1] << 4);
                            Buff[p2+2] = RxBuffer[p1+1] & 0xf0;
                            Buff[p2+3] = RxBuffer[p1+2];
                            p2 += 4;
                            p1 += 3;
                        }
                    }
                    bytes_to_write = p2;
                    bytesWritten = fwrite(Buff, 1, bytes_to_write, fp);
                } else {
                    // 16-bit data: no data processing needed
                    bytes_to_write = packets_to_read * psize;
                    bytesWritten = fwrite(RxBuffer, 1, bytes_to_write, fp);
                }

                if (bytesWritten != bytes_to_write) {
                    printf("\nError: Disk write failed! Make sure there is enough free disk space.\n");
                    Mode = 0x00; //reset mode
                    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
                    ftStatus = FT_Close(ftHandle);
                    fclose(fp);
                    return 3;
                }



                //Update byte markers to prepare for next buffer read cycle

                total_bytes_cleared += packets_to_read*psize;
                packets_read += packets_to_read;
                packets_left_in_buffer -= packets_to_read;
                packets_cleared_in_cycle += packets_to_read;
                bytesLeftToProcess -= packets_to_read*psize;
                // Check if packets_to_read needs to be updated
                // This would be the case for the last FT_Read()
                if (packets_to_read > packets_left_in_buffer) {
                    packets_to_read = packets_left_in_buffer;
                }

            }

        }

        if (!recoveryNeeded) {
            //Update sector markers to prepare for next USB transfer
            sectors_read += numberOfSectorsToReadPerCommand;
            currentSector += numberOfSectorsToReadPerCommand;

            // Update progress
            progress = (packets_read*100)/total_packets;
            if (progress > lastProgress) {
                printf("\r%u%%",(unsigned)progress);
                lastProgress = progress;
            }

            numberOfSectorsToReadPerCommand = 1000; //It seems we may need to keep this to be below the USB incoming buffer size

        }




    }
    //-----------------------





    /*
    // read all the sectors in one command
    TxBuffer[0] = readsectorscmd;
    TxBuffer[1] = start & 0xff;
    TxBuffer[2] = (start>>8) & 0xff;
    TxBuffer[3] = (start>>16) & 0xff;
    TxBuffer[4] = (start>>24) & 0xff;
    TxBuffer[5] = total_sectors & 0xff;
    TxBuffer[6] = (total_sectors>>8) & 0xff;
    TxBuffer[7] = (total_sectors>>16) & 0xff;
    TxBuffer[8] = (total_sectors>>24) & 0xff;
    ftStatus = FT_Write(ftHandle, TxBuffer, 9, &BytesWritten);

    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        fclose(fp);
        return 1;
    }

    // Calculate the maximum number of packets to read in each FT_Read()
    // FT_Read() max request is 65536 bytes
    if (total_packets > (65536/psize))
        //packets_to_read = (65536/psize);
        packets_to_read = 1;
    else
        packets_to_read = total_packets;




    // start timer
    //gettimeofday(&t_start, NULL);

    while (packets_to_read > 0) {
        ftStatus = FT_Read(ftHandle, RxBuffer, packets_to_read * psize, &BytesReceived);
        if (ftStatus != FT_OK) {
            printf("Error: USB - Read (FT_Read) failed!\n");
            Mode = 0x00; //reset mode
            ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
            ftStatus = FT_Close(ftHandle);
            fclose(fp);
            return 1;
        }

        if (BytesReceived != packets_to_read * psize) {
            // did not get all bytes bytes -> timeout!
            printf("Error: USB - Timeout! bytes received: %d (%d)\n", (int)BytesReceived, packets_to_read*psize);
            Mode = 0x00; //reset mode
            ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
            ftStatus = FT_Close(ftHandle);
            fclose(fp);
            return 1;
        }
        if (d->sample_size_bits == 10) {
            // make sure all packets have packet header
            bool recoveryNeeded = false;
            for (i = 0; i < packets_to_read; i++) {
                if (RxBuffer[i*psize] != 0x55) {
                    printf("\nError: Malformed data! Missing packet header\n");                    
                    recoveryNeeded = true;



                    fclose(fp);
                    Mode = 0x00; //reset mode
                    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
                    ftStatus = FT_Close(ftHandle);
                    return 4;
                }
            }
            if (recoveryNeeded) {
                int recoveryScan = 0;
                ftStatus = FT_Read(ftHandle, RxBuffer, 1, &BytesReceived);
                recoveryScan++;
                while(RxBuffer[0] != 0x55) {
                    ftStatus = FT_Read(ftHandle, RxBuffer, 1, &BytesReceived);
                    recoveryScan++;
                }
                printf("Found potential sync after dumping %d bytes. Packet size: %d.\n", recoveryScan, psize);
                ftStatus = FT_Read(ftHandle, RxBuffer, psize-1, &BytesReceived);
                continue;
            }
            p1 = 0;
            p2 = 0;
            for (j = 0; j < packets_to_read; j++) {
                // copy the first part of the packet header
                memcpy(&Buff[p2], &RxBuffer[p1], 12);
                p1 += 12;
                p2 += 12;
                // unpack the 10-bit lfp data to Buff2
                for (i = 0; i < (auxsize-10)*8/10; i+=4) {
                    Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                    Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                    Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                    Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                    Buff[p2+4] = (RxBuffer[p1+2] << 2) & 0xc0;
                    Buff[p2+5] = (RxBuffer[p1+2] >> 6) | (RxBuffer[p1+3] << 2);
                    Buff[p2+6] = (RxBuffer[p1+3] << 0) & 0xc0;
                    Buff[p2+7] = (RxBuffer[p1+3] >> 8) | (RxBuffer[p1+4] << 0);
                    p2 += 8;
                    p1 += 5;
                }
                // copy the timestamp
                memcpy(&Buff[p2], &RxBuffer[p1], 4);
                p1 += 4;
                p2 += 4;
                // unpack the 10-bit ap data to Buff
                for (i = 0; i < channels-3; i+=4) {
                    Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                    Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                    Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                    Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                    Buff[p2+4] = (RxBuffer[p1+2] << 2) & 0xc0;
                    Buff[p2+5] = (RxBuffer[p1+2] >> 6) | (RxBuffer[p1+3] << 2);
                    Buff[p2+6] = (RxBuffer[p1+3] << 0) & 0xc0;
                    Buff[p2+7] = (RxBuffer[p1+3] >> 8) | (RxBuffer[p1+4] << 0);
                    p2 += 8;
                    p1 += 5;
                }
                // must take care of runt data in case channels is not a multiple of 4
                switch (channels % 4) {
                    case 3:
                        Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                        Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                        Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                        Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                        Buff[p2+4] = (RxBuffer[p1+2] << 2) & 0xc0;
                        Buff[p2+5] = (RxBuffer[p1+2] >> 6) | (RxBuffer[p1+3] << 2);
                        p2 += 6;
                        p1 += 4;
                        break;
                    case 2:
                        Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                        Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                        Buff[p2+2] = (RxBuffer[p1+1] << 4) & 0xc0;
                        Buff[p2+3] = (RxBuffer[p1+1] >> 4) | (RxBuffer[p1+2] << 4);
                        p2 += 4;
                        p1 += 3;
                        break;
                    case 1:
                        Buff[p2]   = (RxBuffer[p1] << 6) & 0xc0;;
                        Buff[p2+1] = (RxBuffer[p1] >> 2) | (RxBuffer[p1+1] << 6);
                        p2 += 2;
                        p1 += 2;
                        break;
                }
            }
            bytes_to_write = p2;
            bytesWritten = fwrite(Buff, 1, bytes_to_write, fp);
        } else if (d->sample_size_bits == 12) {
            // 12-bit data: expand packet data from 12 to 16 bits
            p1 = 0;
            p2 = 0;
            for (j = 0; j < packets_to_read; j++) {
                // copy the packet header
                memcpy(&Buff[p2], &RxBuffer[p1], auxsize + 6);
                p1 += auxsize + 6;
                p2 += auxsize + 6;
                // unpack the 12-bit packet data from RxBuffer to Buff
                for (i = 0; i < channels; i+=2) {
                    Buff[p2]   = RxBuffer[p1] << 4;
                    Buff[p2+1] = (RxBuffer[p1] >> 4) | (RxBuffer[p1+1] << 4);
                    Buff[p2+2] = RxBuffer[p1+1] & 0xf0;
                    Buff[p2+3] = RxBuffer[p1+2];
                    p2 += 4;
                    p1 += 3;
                }
            }
            bytes_to_write = p2;
            if(Buff[0] != 0x55){
                printf("\nError: Malformed data! Missing packet header\n");
                fclose(fp);
                Mode = 0x00; //reset mode
                ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
                ftStatus = FT_Close(ftHandle);
                return 4;
            }
            bytesWritten = fwrite(Buff, 1, bytes_to_write, fp);
        } else {
            // 16-bit data: no data processing needed
            bytes_to_write = packets_to_read * psize;
            if(RxBuffer[0] != 0x55){
                printf("\nError: Malformed data! Missing packet header\n");
                fclose(fp);
                Mode = 0x00; //reset mode
                ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
                ftStatus = FT_Close(ftHandle);
                return 4;
            }
            bytesWritten = fwrite(RxBuffer, 1, bytes_to_write, fp);
        }

        if (bytesWritten != bytes_to_write) {
            printf("\nError: Disk write failed! Make sure there is enough free disk space.\n");
            Mode = 0x00; //reset mode
            ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
            ftStatus = FT_Close(ftHandle);
            fclose(fp);
            return 3;
        }

        packets_read += packets_to_read;

        // Update progress
        progress = (packets_read*100)/total_packets;
        if (progress > lastProgress) {
            printf("\r%u%%",(unsigned)progress);
            lastProgress = progress;
        }

        // Check if packets_to_read needs to be updated
        // This would be the case for the last FT_Read()
        if (packets_to_read > total_packets - packets_read)
            packets_to_read = total_packets - packets_read;
    }

    */
    printf("\r100%%\nDone\n");

    // flush out the remaining bytes left from the last sector
    if (excess_bytes)
        ftStatus = FT_Read(ftHandle, RxBuffer, excess_bytes, &BytesReceived);

    // stop timer
    //gettimeofday(&t_end, NULL);

    // get the time difference
    //timeval_subtract (&t_diff, &t_end, &t_start);
    // compute the time in microseconds
    //float sec = (float)t_diff.tv_sec + (float)t_diff.tv_usec/1000000;
    // compute the transmission rate (in MBytes/sec)
    //float rate = (float)(packets_read*psize)/sec/1000000;
    //printf("\nPackets received: %d\n", (int)packets_read);
    printf("Recorded time: %d min %d sec\n",
        (int)((lastPacket+1)/d->sample_rate_khz/1000/60),
        ((int)((lastPacket+1)/d->sample_rate_khz/1000)%60));
    //printf("Download time: %ld min %ld sec\n", t_diff.tv_sec/60, t_diff.tv_sec%60);
    //printf("Transfer rate: %f MBytes/sec\n", rate);

    fclose(fp);
    Mode = 0x00; //reset mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    ftStatus = FT_Close(ftHandle);
    return 0;
}


int CommandEngine::dockCardEnable(){
    UCHAR Mask = 0xff;
    UCHAR Mode;
    DWORD bytes;
    FT_STATUS ftStatus;

    char answer[100];
    int match;
    unsigned char id, hwinfo_version;

    setvbuf(stdout, 0, _IONBF, 0);

    FT_HANDLE ftHandle;
    if (currentDeviceName == nullptr) {
        return 1;
    }
    ftStatus = FT_OpenEx((void*)currentDeviceName,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    //ftStatus = FT_OpenEx((void*)desc,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    if (ftStatus != FT_OK) {
        printf("Can't open FT2232H device for enable! \n");
        // fclose(fp);
        return 1;
    }

    Mode = 0x40; //Sync FIFO mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    if (ftStatus != FT_OK) {

        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    ftStatus = FT_SetUSBParameters(ftHandle, 65536, 65536);	//Set USB request transfer size
    ftStatus |= FT_SetFlowControl(ftHandle,FT_FLOW_RTS_CTS,0,0);
    ftStatus |= FT_SetChars(ftHandle, 0, 0, 0, 0);	 //Disable event and error characters
    ftStatus |= FT_SetTimeouts(ftHandle, 5000, 5000);		//Sets the read and write timeouts in milliseconds for the FT2232H
    ftStatus |= FT_SetLatencyTimer(ftHandle, 128);		//Set the latency timer
    ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX); // Clear buffers
    if (ftStatus != FT_OK) {
        printf("Can't set parameters! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    int hs_detected = 0, sd_detected = 0;
    TxBuffer[0] = SD_GET_STATUS_CMD;
    ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    bytes = 4; // expect 4 bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        printf("  Timeout! bytes received: %d \n", (int)BytesReceived);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    if ((RxBuffer[0] & STATUS_MOUNTED) == STATUS_MOUNTED){
        printf("  SD-card detected\n");
        sd_detected = 1;
    }

    TxBuffer[0] = HS_GET_STATUS_CMD;
    ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    bytes = 4; // expect 4 bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        printf("  Timeout! bytes received: %d \n", (int)BytesReceived);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    if ((RxBuffer[0] & STATUS_MOUNTED) == STATUS_MOUNTED){
        printf("  Headstage detected\n");
        hs_detected = 1;
    }

    uint8_t readsectorscmd = 0, writesectorscmd = 0; //, erasecardcmd = 0;
    if(hs_detected){
        readsectorscmd = HS_READ_SECTORS_CMD;
        writesectorscmd = HS_WRITE_SECTORS_CMD;
//        erasecardcmd = HS_CARD_ERASE_CMD;
    }
    else if(sd_detected){
        readsectorscmd = SD_READ_SECTORS_CMD;
        writesectorscmd = SD_WRITE_SECTORS_CMD;
//        erasecardcmd = SD_CARD_ERASE_CMD;
    }

    else{
        //nothing detected
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return -2;
    }


    // read first 2 sectors to get config info and hw info
    TxBuffer[0] = readsectorscmd;
    TxBuffer[1] = 0x00; // start sector
    TxBuffer[2] = 0x00;
    TxBuffer[3] = 0x00;
    TxBuffer[4] = 0x00;
    TxBuffer[5] = 0x02; // sector count
    TxBuffer[6] = 0x00;
    TxBuffer[7] = 0x00;
    TxBuffer[8] = 0x00;
    ftStatus = FT_Write(ftHandle, TxBuffer, 9, &BytesWritten);
    if (ftStatus != FT_OK) {
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    //printf("\Bytes written to hardware %d.\n",BytesWritten);
    bytes = 1024; // expect 2 * 512 bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);

    if (ftStatus != FT_OK) {
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        printf("\Did not get the expected number of bytes back from hardware %d.\n",BytesReceived);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    // Is the disk used for spike recordings?
    // see if there is a partition table
    /*for (int i = 0x1be; i < 0x1ff; i++) {
        if (RxBuffer[i] != 0) {
            // looks like there is!
            printf("\n************************************************************\n");
            printf("WARNING: This does not look like a disk used for recordings.\n");
            printf("Are you sure this is the correct disk?\n");
            printf("************************************************************\n\n");
            printf("Continue? (yes or no) ");
            match = scanf("%s", answer);
            if (match == 0 || strcmp(answer,"yes")) {
                printf("Aborting...\n");
                Mode = 0x00; //reset mode
                ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
                ftStatus = FT_Close(ftHandle);
                return 0;
            }
            // todo: add code to wipe out the partition table
            break;
        }
    }*/

    id = 0;

    // See if we have a hw info sector
    if (RxBuffer[512] == 0xc3) {
      hwinfo_version = RxBuffer[540];
      if (hwinfo_version > 0) {
        id = RxBuffer[549] + 1;
      }
    }
    //Card enable
    printf("Writing over recorded sectors ...\n");
    TxBuffer[0] = writesectorscmd;
    TxBuffer[1] = 0x01; // start sector
    TxBuffer[2] = 0x00;
    TxBuffer[3] = 0x00;
    TxBuffer[4] = 0x00;
    TxBuffer[5] = 0x01; // sector count
    TxBuffer[6] = 0x00;
    TxBuffer[7] = 0x00;
    TxBuffer[8] = 0x00;
    TxBuffer[9] = 0xaa;
    TxBuffer[10] = 0x55;
    TxBuffer[11] = 0xc3;
    TxBuffer[12] = 0x3c;
    for (int i = 4; i < 512; i++) {
      TxBuffer[i+9] = id;
    }

    //Send over
    ftStatus = FT_Write(ftHandle, TxBuffer, 521, &BytesWritten);
    if (ftStatus != FT_OK) {
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    if(BytesWritten != 521){
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 2;
    }

    // uint16_t dockstatus;
    int writefinished=0;
    int writecounter=0;
    do{
        Sleep(100);
        bytes=1;
        RxBuffer[0] = 0; RxBuffer[1] = 0; RxBuffer[2] = 0; RxBuffer[3] = 0;
        ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
        if (ftStatus != FT_OK) {
            Mode = 0x00; //reset mode
            ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
            ftStatus = FT_Close(ftHandle);
            return 1;
        }

        if(BytesReceived == bytes){
            writefinished = 1;
        }
        else{
            writecounter++;
        }
        printf(".");
    } while(writefinished==0 && writecounter < 30);
    printf("\n");

    DWORD remaining;
    FT_GetQueueStatus(ftHandle, &remaining);
    // printf("1. remaining read buffer: %d\n", remaining);
    ftStatus = FT_Read(ftHandle, RxBuffer, remaining, &BytesReceived);
    if (ftStatus != FT_OK) {
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    if (BytesReceived != remaining) {
        // did not get all bytes bytes -> timeout!
        DWORD remaining;
        FT_GetQueueStatus(ftHandle, &remaining);
        // printf("bytes received: %d, bytes remaining in buffer: %d\n", BytesReceived, remaining);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 2;
    }
    // for(int i = 0; i < remaining; ++i) printf("%02X ", RxBuffer[i]); printf("\n");

    if(sd_detected){
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 0;
    }
/*
    //erase hs sd card
    printf("Erasing remaining sectors ... \n");
    TxBuffer[0] = erasecardcmd;
    ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
    if (ftStatus != FT_OK) {
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    if(BytesWritten != 1){
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 2;
    }
    Sleep(100);
    int finished = 0;
    int counter = 0;
    do{
        bytes = 1; //expect 1 byte back
        ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
        if (ftStatus != FT_OK){
            Mode = 0x00; //reset mode
            ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
            ftStatus = FT_Close(ftHandle);
            return 1;
        }
        if(BytesReceived == bytes){
            finished = 1;
        }
        else{
            counter++;
        }
        printf(".");
    }while(finished==0 && counter < 60); //5 sec timeout for each FT_Read, so 5*60 = 300sec erase time. Enough?
*/
    printf("\n");

    Mode = 0x00; //reset mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    ftStatus = FT_Close(ftHandle);

    return 0;
}

int CommandEngine::dockWriteConfig(const char *cfgfile){
    UCHAR Mask = 0xff;
    UCHAR Mode;
    DWORD bytes;
    FT_STATUS ftStatus;

    char answer[100];
    int match;

    setvbuf(stdout, 0, _IONBF, 0);

    FT_HANDLE ftHandle;
    if (currentDeviceName == nullptr) {
        return 1;
    }
    ftStatus = FT_OpenEx((void*)currentDeviceName,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    //ftStatus = FT_OpenEx((void*)desc,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    if (ftStatus != FT_OK) {
        //printf("Can't open FT2232H device! \n");
        // fclose(fp);
        return 1;
    }

    Mode = 0x40; //Sync FIFO mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    if (ftStatus != FT_OK) {
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    ftStatus = FT_SetUSBParameters(ftHandle, 65536, 65536);	//Set USB request transfer size
    ftStatus |= FT_SetFlowControl(ftHandle,FT_FLOW_RTS_CTS,0,0);
    ftStatus |= FT_SetChars(ftHandle, 0, 0, 0, 0);	 //Disable event and error characters
    ftStatus |= FT_SetTimeouts(ftHandle, 1000, 0);		//Sets the read and write timeouts in milliseconds for the FT2232H
    ftStatus |= FT_SetLatencyTimer(ftHandle, 16);		//Set the latency timer
    ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX); // Clear buffers
    if (ftStatus != FT_OK) {
        //printf("Can't set parameters! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    // get docking station status
    int hs_detected = 0, sd_detected = 0;
    TxBuffer[0] = SD_GET_STATUS_CMD;
    ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
    if (ftStatus != FT_OK) {
      printf("Error: USB - Write (FT_Write) failed!\n");
      Mode = 0x00; //reset mode
      ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
      ftStatus = FT_Close(ftHandle);
      return 1;
    }
    bytes = 4; // expect 4 bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
      printf("Error: USB - Read (FT_Read) failed!\n");
      Mode = 0x00; //reset mode
      ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
      ftStatus = FT_Close(ftHandle);
      return 1;
    }
    if (BytesReceived != bytes) {
      // did not get all bytes bytes -> timeout!
      printf("  Timeout! bytes received: %d \n", (int)BytesReceived);
      Mode = 0x00; //reset mode
      ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
      ftStatus = FT_Close(ftHandle);
      return 1;
    }

    if ((RxBuffer[0] & STATUS_MOUNTED) == STATUS_MOUNTED){
      // printf("  SD-card detected\n");
      sd_detected = 1;
    }

    TxBuffer[0] = HS_GET_STATUS_CMD;
    ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
    if (ftStatus != FT_OK) {
      printf("Error: USB - Write (FT_Write) failed!\n");
      Mode = 0x00; //reset mode
      ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
      ftStatus = FT_Close(ftHandle);
      return 1;
    }
    bytes = 4; // expect 4 bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
      printf("Error: USB - Read (FT_Read) failed!\n");
      Mode = 0x00; //reset mode
      ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
      ftStatus = FT_Close(ftHandle);
      return 1;
    }
    if (BytesReceived != bytes) {
      // did not get all bytes bytes -> timeout!
      printf("  Timeout! bytes received: %d \n", (int)BytesReceived);
      Mode = 0x00; //reset mode
      ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
      ftStatus = FT_Close(ftHandle);
      return 1;
    }

    if ((RxBuffer[0] & STATUS_MOUNTED) == STATUS_MOUNTED){
      // printf("  Headstage detected\n");
      hs_detected = 1;
    }

    uint8_t readsectorscmd = 0, writesectorscmd = 0;
    if(hs_detected){
      readsectorscmd = HS_READ_SECTORS_CMD;
      writesectorscmd = HS_WRITE_SECTORS_CMD;
    }
    else if(sd_detected){
      readsectorscmd = SD_READ_SECTORS_CMD;
      writesectorscmd = SD_WRITE_SECTORS_CMD;
    }
    else{
      //nothing detected
      Mode = 0x00; //reset mode
      ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
      ftStatus = FT_Close(ftHandle);
      return 0;
    }


    // read first 4 sectors to get config info and first packet
    TxBuffer[0] = readsectorscmd;
    TxBuffer[1] = 0x00; // start sector
    TxBuffer[2] = 0x00;
    TxBuffer[3] = 0x00;
    TxBuffer[4] = 0x00;
    TxBuffer[5] = 0x01; // sector count
    TxBuffer[6] = 0x00;
    TxBuffer[7] = 0x00;
    TxBuffer[8] = 0x00;
    ftStatus = FT_Write(ftHandle, TxBuffer, 9, &BytesWritten);
    if (ftStatus != FT_OK) {
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    bytes = 512; // expect 1 * 512 bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    // Is the disk used for spike recordings?
    // see if there is a partition table
    for (int i = 0x1be; i < 0x1ff; i++) {
        if (RxBuffer[i] != 0) {
            // looks like there is!
            printf("\n************************************************************\n");
            printf("WARNING: This does not look like a disk used for recordings.\n");
            printf("Are you sure this is the correct disk?\n");
            printf("************************************************************\n\n");
            printf("Continue? (yes or no) ");
            match = scanf("%s", answer);
            if (match == 0 || strcmp(answer,"yes")) {
                printf("Aborting...\n");
                Mode = 0x00; //reset mode
                ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
                ftStatus = FT_Close(ftHandle);
                return 0;
            }
            break;
        }
    }

    //Write Config
    unsigned char buff[512];
    char str[100];
    uint8_t mask;
    float autofs_threshold;
    int autofs_channels, autofs_timeout;
    int ch;
    FILE* fpConfigFile = fopen(cfgfile, "r");
    if ( NULL == fpConfigFile ) {
        printf("Error: Could not open config file %s: %s\n", cfgfile, strerror(errno) );
        return -7;
    }
    for (int i = 0; i < 512; i++) {
        buff[i] = 0;
    }
    for (int i = 0; i < NUM_CHANNELS_PER_MODULE; i++) {
        match = fscanf (fpConfigFile, "%s", str);
        mask = 0;
        if (strlen(str) == NUM_MODULES) {
            for (int j = 0; j < NUM_MODULES; j++) {
            mask = mask << 1;
            if (str[j] == '1')
                mask |= 0x01;
            }
        }
        buff[i] = mask;
    }

    match = fscanf (fpConfigFile, "%s", str);
    mask = 0;
    if (strlen(str) == 8) {
        for (int j = 0; j < 8; j++) {
            mask = mask << 1;
            if (str[j] == '1')
                mask |= 0x01;
        }
    }
    buff[32] = mask;

    // get the rf channel
    match = fscanf(fpConfigFile, "%d", &ch);
    if (match > 0) {
        if (ch < 128)
            buff[33] = ch;
        else {
            printf("\nWarning: RF channel out of range (must be less than 128 but is set to %d\n. Setting to default", ch);
            buff[33] = 0;
        }
    }
    // get the auto-fastsettle parameters
    match = fscanf(fpConfigFile, "%f", &autofs_threshold);
    if (match > 0) {
        if ((autofs_threshold * 5128) < 32768) {
            int threshold = autofs_threshold *5128;
            buff[34] = threshold & 0xff;
            buff[35] = threshold >> 8;
        } else {
            printf("\nWarning: Autofs threshold out of range (must be less than 6.39 mV but is set to %.2f)\n", autofs_threshold);
            buff[34] = 0xff;
            buff[35] = 0x7f;
        }
    }
    match = fscanf(fpConfigFile, "%d", &autofs_channels);
    if (match > 0) {
        if (autofs_channels < 257) {
            buff[36] = autofs_channels & 0xff;
            buff[37] = autofs_channels >> 8;
        } else {
            printf("\nWarning: Autofs channels out of range (must be 256 or less but is set to %d)\n", autofs_channels);
            buff[36] = 0x00;
            buff[37] = 0x01;
        }
    }
    match = fscanf(fpConfigFile, "%d", &autofs_timeout);
    if (match > 0) {
        if (autofs_timeout < 65536) {
            buff[38] = autofs_timeout & 0xff;
            buff[39] = autofs_timeout >> 8;
        } else {
            printf("\nWarning: Autofs timeout out of range (must be less than 65536 but is set to %d)\n", autofs_timeout);
            buff[38] = 0xff;
            buff[39] = 0xff;
        }
    }
  // for(int i = 0; i < 512; ++i) printf("%02X ", buff[i]); printf("\n");
    TxBuffer[0] = writesectorscmd;
    TxBuffer[1] = 0x00; // start sector
    TxBuffer[2] = 0x00;
    TxBuffer[3] = 0x00;
    TxBuffer[4] = 0x00;
    TxBuffer[5] = 0x01; // sector count
    TxBuffer[6] = 0x00;
    TxBuffer[7] = 0x00;
    TxBuffer[8] = 0x00;
    for (int i = 0; i < 512; i++) {
      TxBuffer[i+9] = buff[i];
    }

    //Send over
    ftStatus = FT_Write(ftHandle, TxBuffer, 521, &BytesWritten);
    if (ftStatus != FT_OK) {
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    if(BytesWritten != 521){
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    int writefinished=0;
    int writecounter=0;
    do{
        Sleep(100);
        bytes=1;
        RxBuffer[0] = 0; RxBuffer[1] = 0; RxBuffer[2] = 0; RxBuffer[3] = 0;
        ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
        if (ftStatus != FT_OK) {
            Mode = 0x00; //reset mode
            ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
            ftStatus = FT_Close(ftHandle);
            return 1;
        }

        if(BytesReceived == bytes){
            writefinished = 1;
        }
        else{
            writecounter++;
        }
        printf(".");
    } while(writefinished==0 && writecounter < 30);

    return 0;
}

int CommandEngine::dockWriteSerial(uint16_t model, uint16_t serial){
    UCHAR Mask = 0xff;
    UCHAR Mode;
    unsigned int bytes;

    FT_STATUS ftStatus;
    FT_HANDLE ftHandle;
    if (currentDeviceName == nullptr) {
        return 1;
    }
    ftStatus = FT_OpenEx((void*)currentDeviceName,FT_OPEN_BY_DESCRIPTION,&ftHandle);

    //ftStatus = FT_OpenEx((void*)desc,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    if (ftStatus != FT_OK) {
        //printf("Can't open FT2232H device! \n");
        // fclose(fp);
        return 1;
    }

    Mode = 0x40; //Sync FIFO mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set sync mode! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    ftStatus = FT_SetUSBParameters(ftHandle, 65536, 65536);	//Set USB request transfer size
    ftStatus |= FT_SetFlowControl(ftHandle,FT_FLOW_RTS_CTS,0,0);
    ftStatus |= FT_SetChars(ftHandle, 0, 0, 0, 0);	 //Disable event and error characters
    ftStatus |= FT_SetTimeouts(ftHandle, 1000, 0);		//Sets the read and write timeouts in milliseconds for the FT2232H
    ftStatus |= FT_SetLatencyTimer(ftHandle, 16);		//Set the latency timer
    ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX); // Clear buffers
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set parameters! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    // get docking station status
    // printf("Set serial... \n");
    TxBuffer[0] = DOCK_SETSERIAL_CMD;
    *(uint16_t*)(TxBuffer+1) = model;
    *(uint16_t*)(TxBuffer+3) = serial;

    ftStatus = FT_Write(ftHandle, TxBuffer, 5, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    //reply
    bytes = 1; // expect 1 bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 2;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        printf("Error: USB - Timeout! bytes received: %d \n", (int)BytesReceived);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 3;
    }

    // printf("----%02x %02x %02x %02x\n", TxBuffer[1], TxBuffer[2], TxBuffer[3], TxBuffer[4]);
    Mode = 0x00; //reset mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    ftStatus = FT_Close(ftHandle);
    return 0;
}

int CommandEngine::dockReadSerial(uint16_t* model, uint16_t* serial){
    UCHAR Mask = 0xff;
    UCHAR Mode;
    unsigned int bytes;

    FT_STATUS ftStatus;
    FT_HANDLE ftHandle;
    if (currentDeviceName == nullptr) {
        return 1;
    }
    ftStatus = FT_OpenEx((void*)currentDeviceName,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    //ftStatus = FT_OpenEx((void*)desc,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't open FT2232H device! \n");
        // fclose(fp);
        return 1;
    }

    Mode = 0x40; //Sync FIFO mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set sync mode! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    ftStatus = FT_SetUSBParameters(ftHandle, 65536, 65536);	//Set USB request transfer size
    ftStatus |= FT_SetFlowControl(ftHandle,FT_FLOW_RTS_CTS,0,0);
    ftStatus |= FT_SetChars(ftHandle, 0, 0, 0, 0);	 //Disable event and error characters
    ftStatus |= FT_SetTimeouts(ftHandle, 1000, 0);		//Sets the read and write timeouts in milliseconds for the FT2232H
    ftStatus |= FT_SetLatencyTimer(ftHandle, 16);		//Set the latency timer
    ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX); // Clear buffers
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set parameters! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    // get docking station status
    // printf("Get serial... \n");
    TxBuffer[0] = DOCK_GETSERIAL_CMD;
    ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
    if (ftStatus != FT_OK) {
        //printf("FT_Write failed!%u\n", ftStatus);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    bytes = 4; // expect 4 bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 2;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        printf("Error: USB - Timeout! bytes received: %d \n", (int)BytesReceived);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 3;
    }

    // printf("----%02x %02x %02x %02x\n", RxBuffer[0], RxBuffer[1], RxBuffer[2], RxBuffer[3]);
    // *model = *(uint16_t*)(RxBuffer);
    // *serial = *(uint16_t*)(RxBuffer+2);
    *model = RxBuffer[1]<<8 | RxBuffer[0];
    *serial = RxBuffer[3]<<8 | RxBuffer[2];

    Mode = 0x00; //reset mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    ftStatus = FT_Close(ftHandle);
    return 0;
}

int CommandEngine::dockRFStartStop(int start, int stop){
    UCHAR Mask = 0xff;
    UCHAR Mode;
    unsigned int bytes;

    FT_STATUS ftStatus;
    FT_HANDLE ftHandle;
    if (currentDeviceName == nullptr) {
        return 1;
    }
    ftStatus = FT_OpenEx((void*)currentDeviceName,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    //ftStatus = FT_OpenEx((void*)desc,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    if (ftStatus != FT_OK) {
        //printf("Can't open FT2232H device! \n");
        // fclose(fp);
        return 1;
    }

    Mode = 0x40; //Sync FIFO mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set sync mode! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    ftStatus = FT_SetUSBParameters(ftHandle, 65536, 65536);	//Set USB request transfer size
    ftStatus |= FT_SetFlowControl(ftHandle,FT_FLOW_RTS_CTS,0,0);
    ftStatus |= FT_SetChars(ftHandle, 0, 0, 0, 0);	 //Disable event and error characters
    ftStatus |= FT_SetTimeouts(ftHandle, 1000, 0);		//Sets the read and write timeouts in milliseconds for the FT2232H
    ftStatus |= FT_SetLatencyTimer(ftHandle, 16);		//Set the latency timer
    ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX); // Clear buffers
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set parameters! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    // rf start command
    if(start){
        TxBuffer[0] = DOCK_START_CMD;
        bytes = 9;
        printf("Sending start command\n");
    }
    else if(stop){
        TxBuffer[0] = DOCK_STOP_CMD;
        bytes = 1;
        printf("Sending stop command\n");
    }
    else{
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 0;
    }

    ftStatus = FT_Write(ftHandle, TxBuffer, bytes, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    bytes = 4; // expect bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 2;
    }
    // if (BytesReceived != bytes) {
    //     // did not get all bytes bytes -> timeout!
    //     printf("  Timeout! bytes received: %d \n", (int)BytesReceived);
    //     Mode = 0x00; //reset mode
    //     ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    //     ftStatus = FT_Close(ftHandle);
    //     return 3;
    // }
    printf("----received %lu bytes: ", BytesReceived);
    for(DWORD i =0; i < BytesReceived; ++i) printf("%02x ", RxBuffer[i]);
    printf("\n");
    Mode = 0x00; //reset mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    ftStatus = FT_Close(ftHandle);
    return 0;
}

int CommandEngine::dockLastPL(){
    UCHAR Mask = 0xff;
    UCHAR Mode;
    unsigned int bytes;

    FT_STATUS ftStatus;
    FT_HANDLE ftHandle;
    if (currentDeviceName == nullptr) {
        return 1;
    }
    ftStatus = FT_OpenEx((void*)currentDeviceName,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    //ftStatus = FT_OpenEx((void*)desc,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    if (ftStatus != FT_OK) {
        //printf("Can't open FT2232H device! \n");
        // fclose(fp);
        return 1;
    }

    Mode = 0x40; //Sync FIFO mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set sync mode! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    ftStatus = FT_SetUSBParameters(ftHandle, 65536, 65536);	//Set USB request transfer size
    ftStatus |= FT_SetFlowControl(ftHandle,FT_FLOW_RTS_CTS,0,0);
    ftStatus |= FT_SetChars(ftHandle, 0, 0, 0, 0);	 //Disable event and error characters
    ftStatus |= FT_SetTimeouts(ftHandle, 1000, 0);		//Sets the read and write timeouts in milliseconds for the FT2232H
    ftStatus |= FT_SetLatencyTimer(ftHandle, 16);		//Set the latency timer
    ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX); // Clear buffers
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set parameters! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    TxBuffer[0] = DOCK_LATEST_PAYLOAD;//DOCK_LATEST_PL

    ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    bytes = 1; // expect bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 2;
    }
    // if (BytesReceived != bytes) {
    //     // did not get all bytes bytes -> timeout!
    //     printf("  Timeout! bytes received: %lu \n", BytesReceived);
    //     Mode = 0x00; //reset mode
    //     ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    //     ftStatus = FT_Close(ftHandle);
    //     return 3;
    // }
    printf("----received %lu bytes: ", BytesReceived);
    for(DWORD i =0; i < BytesReceived; ++i) printf("%02x ", RxBuffer[i]);
    printf("\n");

    bytes = RxBuffer[0]; // expect bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 2;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        printf("  Timeout! bytes received: %lu \n", BytesReceived);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 3;
    }
    printf("----received %lu bytes: ", BytesReceived);
    for(DWORD i =0; i < BytesReceived; ++i) printf("%02x ", RxBuffer[i]);
    printf("\n");

    Mode = 0x00; //reset mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    ftStatus = FT_Close(ftHandle);
    return 0;
}

int CommandEngine::dockSetRFChan(uint8_t chan){
    UCHAR Mask = 0xff;
    UCHAR Mode;
    // unsigned int bytes;

    FT_STATUS ftStatus;
    FT_HANDLE ftHandle;
    if (currentDeviceName == nullptr) {
        return 1;
    }
    ftStatus = FT_OpenEx((void*)currentDeviceName,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    //ftStatus = FT_OpenEx((void*)desc,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    if (ftStatus != FT_OK) {
        //printf("Can't open FT2232H device! \n");
        // fclose(fp);
        return 1;
    }

    Mode = 0x40; //Sync FIFO mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set sync mode! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    ftStatus = FT_SetUSBParameters(ftHandle, 65536, 65536);	//Set USB request transfer size
    ftStatus |= FT_SetFlowControl(ftHandle,FT_FLOW_RTS_CTS,0,0);
    ftStatus |= FT_SetChars(ftHandle, 0, 0, 0, 0);	 //Disable event and error characters
    ftStatus |= FT_SetTimeouts(ftHandle, 1000, 0);		//Sets the read and write timeouts in milliseconds for the FT2232H
    ftStatus |= FT_SetLatencyTimer(ftHandle, 16);		//Set the latency timer
    ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX); // Clear buffers
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set parameters! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    // rf start command
    TxBuffer[0] = DOCK_SET_RF_CHANNEL;
    TxBuffer[1] = chan;

    ftStatus = FT_Write(ftHandle, TxBuffer, 2, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    unsigned int bytes = 1; // expect bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 2;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        printf("  Timeout! bytes received: %lu \n", BytesReceived);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 3;
    }
    for(DWORD i =0; i < BytesReceived; ++i)
      printf("%02x ", RxBuffer[i]);
    printf("\n");

    Mode = 0x00; //reset mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    ftStatus = FT_Close(ftHandle);
    return 0;
}

int CommandEngine::dockGetRFChan(){
    UCHAR Mask = 0xff;
    UCHAR Mode;
    // unsigned int bytes;

    FT_STATUS ftStatus;
    FT_HANDLE ftHandle;
    if (currentDeviceName == nullptr) {
        return 1;
    }
    ftStatus = FT_OpenEx((void*)currentDeviceName,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    //ftStatus = FT_OpenEx((void*)desc,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    if (ftStatus != FT_OK) {
        //printf("Can't open FT2232H device! \n");
        // fclose(fp);
        return 1;
    }

    Mode = 0x40; //Sync FIFO mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set sync mode! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    ftStatus = FT_SetUSBParameters(ftHandle, 65536, 65536);	//Set USB request transfer size
    ftStatus |= FT_SetFlowControl(ftHandle,FT_FLOW_RTS_CTS,0,0);
    ftStatus |= FT_SetChars(ftHandle, 0, 0, 0, 0);	 //Disable event and error characters
    ftStatus |= FT_SetTimeouts(ftHandle, 1000, 0);		//Sets the read and write timeouts in milliseconds for the FT2232H
    ftStatus |= FT_SetLatencyTimer(ftHandle, 16);		//Set the latency timer
    ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX); // Clear buffers
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set parameters! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    // rf start command
    TxBuffer[0] = DOCK_GET_RF_CHANNEL;

    ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    unsigned int bytes = 1; // expect bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 2;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        printf("  Timeout! bytes received: %lu \n", BytesReceived);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 3;
    }
    printf("%02x %02x\n", RxBuffer[0], RxBuffer[1]);

    Mode = 0x00; //reset mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    ftStatus = FT_Close(ftHandle);
    return 0;
}

int CommandEngine::dockEraseSD(){
    UCHAR Mask = 0xff;
    UCHAR Mode;
    // unsigned int bytes;

    FT_STATUS ftStatus;
    FT_HANDLE ftHandle;
    if (currentDeviceName == nullptr) {
        return 1;
    }
    ftStatus = FT_OpenEx((void*)currentDeviceName,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    //ftStatus = FT_OpenEx((void*)desc,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    if (ftStatus != FT_OK) {
        //printf("Can't open FT2232H device! \n");
        // fclose(fp);
        return 1;
    }

    Mode = 0x40; //Sync FIFO mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set sync mode! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    ftStatus = FT_SetUSBParameters(ftHandle, 65536, 65536);	//Set USB request transfer size
    ftStatus |= FT_SetFlowControl(ftHandle,FT_FLOW_RTS_CTS,0,0);
    ftStatus |= FT_SetChars(ftHandle, 0, 0, 0, 0);	 //Disable event and error characters
    ftStatus |= FT_SetTimeouts(ftHandle, 1000, 0);		//Sets the read and write timeouts in milliseconds for the FT2232H
    ftStatus |= FT_SetLatencyTimer(ftHandle, 16);		//Set the latency timer
    ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX); // Clear buffers
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set parameters! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    // rf start command
    TxBuffer[0] = SD_CARD_ERASE_CMD;
    printf("Erasing *SD* card\n");
    ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    unsigned int bytes = 1; // expect bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 2;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        printf("  Timeout! bytes received: %lu \n", BytesReceived);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 3;
    }
    printf("%02x %02x\n", RxBuffer[0], RxBuffer[1]);

    Mode = 0x00; //reset mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    ftStatus = FT_Close(ftHandle);
    return 0;
}

int CommandEngine::getDockSettings(){
    UCHAR Mask = 0xff;
    UCHAR Mode;
    // unsigned int bytes;

    FT_STATUS ftStatus;
    FT_HANDLE ftHandle;
    if (currentDeviceName == nullptr) {
        return 1;
    }
    ftStatus = FT_OpenEx((void*)currentDeviceName,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    //ftStatus = FT_OpenEx((void*)desc,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    if (ftStatus != FT_OK) {
        printf("Can't open FT2232H device! \n");
        // fclose(fp);
        return 1;
    }

    Mode = 0x40; //Sync FIFO mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set sync mode! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    ftStatus = FT_SetUSBParameters(ftHandle, 65536, 65536);	//Set USB request transfer size
    ftStatus |= FT_SetFlowControl(ftHandle,FT_FLOW_RTS_CTS,0,0);
    ftStatus |= FT_SetChars(ftHandle, 0, 0, 0, 0);	 //Disable event and error characters
    ftStatus |= FT_SetTimeouts(ftHandle, 1000, 0);		//Sets the read and write timeouts in milliseconds for the FT2232H
    ftStatus |= FT_SetLatencyTimer(ftHandle, 16);		//Set the latency timer
    ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX); // Clear buffers
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set parameters! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    TxBuffer[0] = DOCK_USERDATA_LEN;
    ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    unsigned int bytes = 4; // expect bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 2;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        printf("  Timeout! bytes received: %lu \n", BytesReceived);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 3;
    }

    int len = (RxBuffer[3] << 24) | (RxBuffer[2] << 16) | (RxBuffer[1] << 8) | RxBuffer[0];

    TxBuffer[0] = DOCK_GET_SETTINGS;
    ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    bytes = len; // expect bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 2;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        printf("  Timeout! bytes received: %lu \n", BytesReceived);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 3;
    }

    if (bytes < 1024) {
        memcpy(deviceSettingsBuffer,RxBuffer,bytes);
    } else {
        printf("Return message too large");
        return 3;
    }

    if (strncmp(currentDeviceName,"Spikegadgets MCU",16) == 0) {
        //We are connected to an MCU
        int dockrfchan = RxBuffer[2];
        //char dockdescription[64] = "---";
        //memcpy(dockdescription, RxBuffer+1, 63);
        //dockdescription[63] = '\0';
        int docksamplingrate = (RxBuffer[3] == 20) ? 20 : 30;
        //int RFMode = (RxBuffer[65] < 2) ? 1 : 2;
        int RFMode = ((RxBuffer[4] >> 2) & 0xf)+1;


        printf("MCU settings\n");
        //When printing settings, make sure to separate field title and value with a ':'
        //Necessary for the GUI to parse properly. The GUI will be able to handle as many settings as available
        //Also, print these in the order they are represented in flash memory!
        printf("RF Channel(2-124):                %d\n", dockrfchan);
        //printf("Description(63 chars):            %s\n", dockdescription);
        printf("Sampling Rate(20 or 30):          %d\n", docksamplingrate);
        printf("RF mode (1=legacy, 2=session ID): %d\n", RFMode);
    } else {
        //We are connected to a docking station.
        int dockrfchan = RxBuffer[0];
        char dockdescription[64];
        memcpy(dockdescription, RxBuffer+1, 63);
        dockdescription[63] = '\0';
        int docksamplingrate = (RxBuffer[64] == 20) ? 20 : 30;
        //int RFMode = (RxBuffer[65] < 2) ? 1 : 2;
        int RFMode = RxBuffer[65]+1;


        printf("Docking station settings\n");
        //When printing settings, make sure to separate field title and value with a ':'
        //Necessary for the GUI to parse properly. The GUI will be able to handle as many settings as available
        //Also, print these in the order they are represented in flash memory!
        printf("RF Channel(2-124):                %d\n", dockrfchan);
        printf("Description(63 chars):            %s\n", dockdescription);
        printf("Sampling Rate(20 or 30):          %d\n", docksamplingrate);
        printf("RF mode (1=legacy, 2=session ID): %d\n", RFMode);
    }

    Mode = 0x00; //reset mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    ftStatus = FT_Close(ftHandle);
    return 0;
}

int CommandEngine::setDockSettings(int argc, char **argv){
    UCHAR Mask = 0xff;
    UCHAR Mode;
    // unsigned int bytes;

    FT_STATUS ftStatus;
    FT_HANDLE ftHandle;
    if (currentDeviceName == nullptr) {
        printf("Error: no device name given. \n");
        return 1;
    }

    if (getDockSettings()) {
        printf("Could not retrieve settings before writing new ones.");
        return 1;
    }
    ftStatus = FT_OpenEx((void*)currentDeviceName,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    //ftStatus = FT_OpenEx((void*)desc,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    if (ftStatus != FT_OK) {
        //printf("Can't open FT2232H device! \n");
        // fclose(fp);
        return 1;
    }

    Mode = 0x40; //Sync FIFO mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set sync mode! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    ftStatus = FT_SetUSBParameters(ftHandle, 65536, 65536);	//Set USB request transfer size
    ftStatus |= FT_SetFlowControl(ftHandle,FT_FLOW_RTS_CTS,0,0);
    ftStatus |= FT_SetChars(ftHandle, 0, 0, 0, 0);	 //Disable event and error characters
    ftStatus |= FT_SetTimeouts(ftHandle, 1000, 0);		//Sets the read and write timeouts in milliseconds for the FT2232H
    ftStatus |= FT_SetLatencyTimer(ftHandle, 16);		//Set the latency timer
    ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX); // Clear buffers
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set parameters! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    TxBuffer[0] = DOCK_USERDATA_LEN;
    ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    unsigned int bytes = 4; // expect bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 2;
    }
    if (BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        printf("  Timeout! bytes received: %lu \n", BytesReceived);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 3;
    }

    int len = (RxBuffer[3] << 24) | (RxBuffer[2] << 16) | (RxBuffer[1] << 8) | RxBuffer[0];
    // printf("Entered values:\n%s\n%s\n%s\n", argv[0], argv[1], argv[2]);

    TxBuffer[0] = DOCK_SET_SETTINGS;
    ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    memset(TxBuffer, 0, len);


    if (strncmp(currentDeviceName,"Spikegadgets MCU",16) == 0) {
        //We are connecting to an MCU, and we need to write back the whole settings structure.
        //We modify the parts we give access to, and keep the rest unchanged.
        memcpy(TxBuffer,deviceSettingsBuffer,len);
        int chan = (atoi(argv[0]) <= 124 && atoi(argv[0])>= 2) ? atoi(argv[0]) : 2;
        TxBuffer[2] = chan;
        int srate = (atoi(argv[1]) == 20) ? 20 : 30;
        TxBuffer[3] = srate;
        int RFMode = (atoi(argv[2]) < 2) ? 0 : 1;
        if (RFMode) {
            TxBuffer[4] |= (1 << 2);
        } else {
            TxBuffer[4] &= ~(1 << 2);
        }


    } else {
        //we are connecting to a logger dock
        //rf chan (2-124)
        int chan = (atoi(argv[0]) <= 124 && atoi(argv[0])>= 2) ? atoi(argv[0]) : 2;
        TxBuffer[0] = chan;
        //description (63 chars)
        int slen = (63 < strlen(argv[1])) ? 63 : strlen(argv[1]);
        memcpy(TxBuffer+1, argv[1], slen);
        //samplingrate (20 or 30)
        int srate = (atoi(argv[2]) == 20) ? 20 : 30;
        TxBuffer[64] = srate;

        if (argc > 2) {
            int RFMode = (atoi(argv[3]) < 2) ? 0 : 1;
            TxBuffer[65] = RFMode;
        }
    }

    // for(int i = 0; i < len; ++i) printf("%02X ", TxBuffer[i]); printf("\n");

    ftStatus = FT_Write(ftHandle, TxBuffer, len, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    Mode = 0x00; //reset mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    ftStatus = FT_Close(ftHandle);
    return 0;
}

int CommandEngine::getDockFirmwareVersion(uint8_t *major, uint8_t *minor, uint8_t *patch){
    uint8_t Mask = 0xff;
    uint8_t Mode;
    DWORD bytes;
    if (currentDeviceName == nullptr) {
        return 1;
    }
    ftStatus = FT_OpenEx((void*)currentDeviceName,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    //ftStatus = FT_OpenEx((void*)desc,FT_OPEN_BY_DESCRIPTION,&ftHandle);
    if (ftStatus != FT_OK) {
        // printf("Can't open FT2232H device! \n");
        return 1;
    }

    Mode = 0x40; //Sync FIFO mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set sync mode! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    ftStatus = FT_SetUSBParameters(ftHandle, 65536, 65536);	//Set USB request transfer size
    ftStatus |= FT_SetFlowControl(ftHandle,FT_FLOW_RTS_CTS,0,0);
    ftStatus |= FT_SetChars(ftHandle, false, 0, false, 0);	 //Disable event and error characters
    ftStatus |= FT_SetTimeouts(ftHandle, 1000, 0);		//Sets the read and write timeouts in milliseconds for the FT2232H
    ftStatus |= FT_SetLatencyTimer(ftHandle, 2);		//Set the latency timer
    ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX); // Clear buffers
    if (ftStatus != FT_OK) {
        printf("Error: USB - Can't set parameters! \n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }

    // uint8_t major, minor, patch;
    TxBuffer[0] = DOCK_FIRMWARE_VERSION;
    ftStatus = FT_Write(ftHandle, TxBuffer, 1, &BytesWritten);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Write (FT_Write) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    bytes = 3; // expect 4 bytes back
    ftStatus = FT_Read(ftHandle, RxBuffer, bytes, &BytesReceived);
    if (ftStatus != FT_OK) {
        printf("Error: USB - Read (FT_Read) failed!\n");
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    if (BytesReceived != 0 && BytesReceived != bytes) {
        // did not get all bytes bytes -> timeout!
        printf("  Timeout! bytes received: %d \n", (int)BytesReceived);
        Mode = 0x00; //reset mode
        ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
        ftStatus = FT_Close(ftHandle);
        return 1;
    }
    *major = RxBuffer[0];
    *minor = RxBuffer[1];
    *patch = RxBuffer[2];
    // printf("Firmware version: %u.%u.%u\n", major, minor, patch);

    Mode = 0x00; //reset mode
    ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
    ftStatus = FT_Close(ftHandle);
    return 0;
}

void CommandEngine::printHelpmenu(const char* p){
    printf( "Usage: %s [options] [args]\n"
            "\n"
            "Options:\n"
            "  -h or --help                       Display this \n"
            "  -d or --detect                     Detect if any docking stations are connected and whether a device is mounted\n"
            "  -r or --readconfig                 Read and display the configuration of mounted device\n"
            "  -p or --packetcheck                Find and display recording and packet information. \n"
            "  -c or --cardenable                 Enable the mounted device for recording, wipes out existing recording data.\n"
            "  -e or --extract=<data.dat>         Extract the existing recording data to a file passed in by the user.\n"
            "                <data.rec>            - If a file with extension .rec was passed, data will be appended to it. Proper Trodes config is NOT CHECKED.\n"
            "  -w or --writeconfig=<config.cfg>   Overwrite the configuration of mounted device with the .cfg file provided\n"
            "  -t or --trodesparse                Used by Trodes. Displays relevant config numbers and exits.\n"
            "  -g or --getserial                  Print out the model and serial number of the docking station itself.\n"
            "  -m or --getdocksettings            Print DockingStation settings like rf_channel, description, or samplingrate\n"
            "  -n or --setdocksettings            Set DockingStation settings. Pass settings as arguments in the same order that 'getdocksettings' prints them\n"
            "                                      - Example: ./dockingstation -n 3 \"My Description Here\" 20 2\n"
            "  -v or --version                    Get the firmware version number.\n"
            "\n"
            "For use with the SpikeGadgets Docking Station and wireless logging headstages. Visit http://spikegadgets.com for more info\n"
            , p);
}

void CommandEngine::printconfig(DockingStation_t d){
    printf( "HS_serial:         %d\n"
                "HS_model#:         %d\n"
                "HS version:        %d.%d\n"
                "# channels:        %d\n"
                "Sample size:       %d bits\n"
                "Sample rate:       %d khz\n"
                "Disk size:         %d MB\n"
                "Mag enabled:       %s\n"
                "Acc enabled:       %s\n"
                "Gyr enabled:       %s\n"
                "Smart ref:         %s\n"
                "RF channel:        %d\n"
                "Battery size:      %d mAh\n"
                /*"Chip id:           %lu\n"
                "Sensor version     %u\n"
                "Sensor serial      %u\n"
                "Intan channels     %u\n"*/,
                d.hs_serial_number,
                d.hs_model_number,
                d.hs_major_version, d.hs_minor_version,
                d.channels,
                d.sample_size_bits,
                d.sample_rate_khz,
                d.disk_size_MB,
                (d.mag_enabled ? "yes":"no"),
                (d.acc_enabled ? "yes":"no"),
                (d.gyr_enabled ? "yes":"no"),
                (d.smartref_enabled ? "yes":"no"),
                d.rf_channel,
                d.batterysize_mAh /*,
                d.chipid,
                d.sensor_version,
                d.serial_sensor,
                d.intan_channels*/);
    if(d.waitforstart_override){
        printf( "Wait for start:    override\n");
    }
    if(d.cardenablecheck_override){
        printf( "Card enable check: override\n");
    }
    printf("\n");
}

void CommandEngine::printconfigfortrodes(DockingStation_t d){
    //!!!!!!!Don't change %llu for now, windows needs it to be llu for some reason ...
    printf( "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d "
#if defined(_WIN32) || defined(__APPLE_CC__)
            "%llu %llu %llu "
#else
            "%lu %lu %lu "
#endif
            "%s\n",
                d.hs_serial_number,
                d.hs_model_number,
                d.hs_major_version,
                d.hs_minor_version,
                d.channels,
                d.mag_enabled,
                d.acc_enabled,
                d.gyr_enabled,
                d.smartref_enabled,
                d.rf_channel,
                d.aux_size_bytes,
                d.sample_size_bits,
                d.sample_rate_khz,
                d.trodes_packet_size,
                d.waitforstart_override,
                d.cardenablecheck_override,
                d.max_packets,
                d.recorded_packets,
                d.dropped_packets,
                strlen(d.recording_datetime) ? d.recording_datetime : "");
}


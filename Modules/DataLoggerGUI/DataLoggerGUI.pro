#-------------------------------------------------
#
# Project created by QtCreator 2018-04-06T15:13:24
#
#-------------------------------------------------


include(../module_defaults.pri)

QT       += core gui xml network charts
CONFIG += c++14
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DataLoggerGUI
TEMPLATE = app

#CONFIG += console

INCLUDEPATH += $$TRODES_REPO_DIR/Trodes/src-config
INCLUDEPATH += $$TRODES_REPO_DIR/Trodes/src-display
INCLUDEPATH += $$TRODES_REPO_DIR/Trodes/src-main
INCLUDEPATH += $$TRODES_REPO_DIR/Trodes/src-threads
INCLUDEPATH += $$TRODES_REPO_DIR/Modules/workspaceGUI

SOURCES += \
    controllerrfchannelprocess.cpp \
    controllersamplingrateprocess.cpp \
    dockconfigwidget.cpp \
    dockprogrammingprocess.cpp \
    jtagprocess.cpp \
        main.cpp \
        mainwindow.cpp \
    disklistwidget.cpp \
    extractprocess.cpp \
    mergeprocess.cpp \
    cardenableprocess.cpp \
    pcheckprocess.cpp \
    abstractprocess.cpp \
    sdtorecprocess.cpp \
    writeconfigprocess.cpp \
    readconfigprocess.cpp \
    #../workspaceGUI/workspaceEditor.cpp \
    ../../Trodes/src-display/quicksetup.cpp \
    #../../Trodes/src-display/workspaceeditordialog.cpp \
    ../../Trodes/src-display/cargrouppanel.cpp \
    ../../Trodes/src-config/configuration.cpp \
    ../../Trodes/src-display/probelayoutpanel.cpp \
    dockingquickstart.cpp \
    #ntrodechannelmappingwindow.cpp \
    loggerconfigwidget.cpp \
    firmwareupdater.cpp \
    DockConsoleUtility/commandengine.cpp \

HEADERS += \
    controllerrfchannelprocess.h \
    controllersamplingrateprocess.h \
    dockconfigwidget.h \
    dockprogrammingprocess.h \
    jtagprocess.h \
        mainwindow.h \
    disklistwidget.h \
    extractprocess.h \
    mergeprocess.h \
    cardenableprocess.h \
    pcheckprocess.h \
    abstractprocess.h \
    sdtorecprocess.h \
    writeconfigprocess.h \
    readconfigprocess.h \
   # ../workspaceGUI/workspaceEditor.h \
    ../../Trodes/src-display/quicksetup.h \
   # ../../Trodes/src-display/workspaceeditordialog.h \
    ../../Trodes/src-display/cargrouppanel.h \
    ../../Trodes/src-config/hardwaresettings.h \
    ../../Trodes/src-config/configuration.h \
    ../../Trodes/src-display/probelayoutpanel.h \
    dockingquickstart.h \
    #ntrodechannelmappingwindow.h \
    loggerconfigwidget.h \
    firmwareupdater.h \
    DockConsoleUtility/commandengine.h \


win32{
    #INCLUDEPATH += $$PWD/../../Trodes/Libraries/Windows
    #HEADERS += ../../Trodes/Libraries/Windows/ftd2xx.h
    #LIBS += -L$${PWD}/../../Trodes/Libraries/Windows -lftd2xx
    INCLUDEPATH += $$PWD/../../Trodes/Libraries/Windows
    HEADERS += ../../Trodes/Libraries/Windows/ftd2xx.h
    LIBS   += $$quote($$PWD/../../Trodes/Libraries/Windows/FTDI/amd64/ftd2xx64.lib)



    INCLUDEPATH += $$PWD/DockConsoleUtility/Windows
    SOURCES += \
        DockConsoleUtility/Windows/getopt.c
    HEADERS += \
        DockConsoleUtility/Windows/getopt.h


    RC_ICONS = dlGUIIcon.ico
    #RC_FILE = DataLoggerGUI.rc
    utilities.path = $$INSTALL_DIR/windows_sd_util/
    utilities.files += ./windows_sd_util/*

    mcu_utils.path = $$INSTALL_DIR/mcu_utils/windows
    mcu_utils.files += ./mcu_utils/windows/*

    INSTALLS += utilities mcu_utils

}

unix:!macx{


    INCLUDEPATH += ../../Trodes/Libraries/Linux
    LIBS += -L../../Trodes/Libraries/Linux
    HEADERS    += ../../Trodes/Libraries/Linux/WinTypes.h \
                  ../../Trodes/Libraries/Linux/ftd2xx.h
    LIBS       += -lftd2xx

    utilities.path = $$INSTALL_DIR/linux_sd_util/
    utilities.files += ./linux_sd_util/*

    icon.path = $$INSTALL_DIR/Resources/Images
    icon.files += ./dlGUIIcon.png

    mcu_utils.path = $$INSTALL_DIR/mcu_utils/linux
    mcu_utils.files += ./mcu_utils/linux/*

    INSTALLS += utilities mcu_utils icon

}
macx{
    INCLUDEPATH += ../../Trodes/Libraries/Mac
    LIBS += -L../../Trodes/Libraries/Mac
    HEADERS    += ../../Trodes/Libraries/Mac/WinTypes.h \
                  ../../Trodes/Libraries/Mac/ftd2xx.h
    #LIBS       += -lftd2xx
    LIBS       += $$quote($$PWD/../../Trodes/Libraries/Mac/libftd2xx.a) -framework Cocoa

    utilities.path = $$INSTALL_DIR/macos_sd_util/
    utilities.files += ./macos_sd_util/*

    icon.path = $$INSTALL_DIR/Resources/Images
    icon.files += ./dlGUIIcon.png

    mcu_utils.path = $$INSTALL_DIR/mcu_utils/macos
    mcu_utils.files += ./mcu_utils/macos/*

    INSTALLS += utilities mcu_utils icon
}
RESOURCES += \
    ../../Resources/Images/buttons.qrc

workspaces.path = $$INSTALL_DIR/Resources/SampleWorkspaces
workspaces.files += $$TRODES_REPO_DIR/Resources/SampleWorkspaces/BlankWorkspace.trodesconf
INSTALLS += workspaces

DISTFILES += \
    DataloggerGUI


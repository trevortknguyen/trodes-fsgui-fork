#include "loggerconfigwidget.h"
#include "writeconfigprocess.h"
#include "controllerrfchannelprocess.h"
#include "controllersamplingrateprocess.h"

LoggerConfigWidget::LoggerConfigWidget(LoggerRecording rec, QString device, QWidget *parent) : QDialog(parent), device(device)
{
    setWindowTitle("Logger configuration");
    QLabel *numchans = new QLabel("Number of channels");
    numchanBox = new QSpinBox;
    numchanBox->setRange(32, 4096);

    QLabel *rfchan= new QLabel("RF channel");
    rfchanBox = new QSpinBox;
    rfchanBox->setRange(2, 80);

    waitforstartoverride = new QCheckBox("Advanced: Wait-for-start override");
    cardenableoverride = new QCheckBox("Advanced: CardEnable check override");
    samplingrate20khz = new QCheckBox("20khz sampling rate");
    magnetometerdisable = new QCheckBox("Disable magnetometer");
    accelerometerdisable = new QCheckBox("Disable accelerometer");
    gyroscopedisable = new QCheckBox("Disable gyroscope");
    smartrefenable = new QCheckBox("Advanced: Enable smart ref");
    samplesize12bits = new QCheckBox("12 bit sample size");

    QDialogButtonBox *buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(buttons, &QDialogButtonBox::accepted, this, &LoggerConfigWidget::donepressed);
    connect(buttons, &QDialogButtonBox::rejected, this, &QDialog::reject);

//    QPushButton *loadCfgFile = new QPushButton("Load .cfg");
//    connect(loadCfgFile, &QPushButton::pressed, this, &LoggerConfigWidget::loadfile);

    QHBoxLayout *nchans = new QHBoxLayout;
    QHBoxLayout *rfchans = new QHBoxLayout;
    nchans->addWidget(numchanBox);
    nchans->addWidget(numchans);
    rfchans->addWidget(rfchanBox);
    rfchans->addWidget(rfchan);

    QVBoxLayout *layout = new QVBoxLayout;

    layout->addWidget(new QLabel("Apply new logger configuration settings"));
//    layout->addWidget(loadCfgFile, 0, Qt::AlignLeft);
    QFrame *line = new QFrame;
    line->setFrameShadow(QFrame::Sunken);
    line->setFrameShape(QFrame::HLine);
    layout->addWidget(line);


    layout->addLayout(nchans);
    layout->addLayout(rfchans);

    QFrame *line4 = new QFrame;
    line4->setFrameShadow(QFrame::Sunken);
    line4->setFrameShape(QFrame::HLine);
    layout->addWidget(line4);

    layout->addWidget(magnetometerdisable);
    layout->addWidget(accelerometerdisable);
    layout->addWidget(gyroscopedisable);

    QFrame *line2 = new QFrame;
    line2->setFrameShadow(QFrame::Sunken);
    line2->setFrameShape(QFrame::HLine);
    layout->addWidget(line2);

    layout->addWidget(samplingrate20khz);
    layout->addWidget(samplesize12bits);

    QFrame *line3 = new QFrame;
    line3->setFrameShadow(QFrame::Sunken);
    line3->setFrameShape(QFrame::HLine);
    layout->addWidget(line3);

    layout->addWidget(smartrefenable);
    layout->addWidget(waitforstartoverride);
    layout->addWidget(cardenableoverride);

    layout->addWidget(buttons);
    setLayout(layout);


    if(!rec.settings.valid){
        numchanBox->setEnabled(false);
        rfchanBox->setEnabled(false);
        samplingrate20khz->setEnabled(false);
        magnetometerdisable->setEnabled(false);
        accelerometerdisable->setEnabled(false);
        gyroscopedisable->setEnabled(false);
        smartrefenable->setEnabled(false);
        samplesize12bits->setEnabled(false);
        waitforstartoverride->setEnabled(false);
        cardenableoverride->setEnabled(false);
        buttons->button(QDialogButtonBox::Ok)->setEnabled(false);
        return;
    }
    samplingrate20khz->setChecked(rec.settings.sample20khzOn);
    magnetometerdisable->setChecked(!rec.settings.magSensorOn);
    accelerometerdisable->setChecked(!rec.settings.accelSensorOn);
    gyroscopedisable->setChecked(!rec.settings.gyroSensorOn);
    smartrefenable->setChecked(rec.settings.smartRefOn);
    samplesize12bits->setChecked(rec.settings.sample12bitOn);
    numchanBox->setValue(rec.settings.numberOfChannels);
    rfchanBox->setValue(rec.settings.rfChannel);
    waitforstartoverride->setChecked(rec.settings.waitForStartOverride);
    cardenableoverride->setChecked(rec.settings.cardEnableCheckOverride);

    rfChan = rec.settings.rfChannel;
    sample20khzOn = rec.settings.sample20khzOn;
}

void LoggerConfigWidget::donepressed()
{
    int ret = QMessageBox::warning(this, "Confirm", "Changing logger configuration. Continue?", QMessageBox::Yes|QMessageBox::No);
    if(ret != QMessageBox::Yes){
        return;
    }
    //create temp cfg file
    QTemporaryFile cfg("./dlgui_tmpcfgXXXXXX.cfg");
//    QFile cfg("test.cfg");

    if(cfg.open()){
//    if(cfg.open(QFile::ReadWrite)){
        QTextStream stream(&cfg);
        int onesperline = numchanBox->value()/32;
        if(numchanBox->value() > 256)
            onesperline = onesperline/2;
        for(int i = 0; i < 32; ++i){
            for(int j = 0; j < 8; ++j){
                stream << (j < 8-onesperline ? '0' : '1');
            }
            stream << '\n';
        }

        stream << (samplesize12bits->isChecked() ? '1' : '0'); //12 bit enable
        stream << (smartrefenable->isChecked() ? '1' : '0');    //smartref enable
        stream << (gyroscopedisable->isChecked() ? '1' : '0');  //gyro sensor disable
        stream << (accelerometerdisable->isChecked() ? '1' : '0'); //accel sensor disable
        stream << (magnetometerdisable->isChecked() ? '1' : '0');   //mag sensor disable
        stream << (samplingrate20khz->isChecked() ? '1' : '0'); //20khz enable
        stream << (cardenableoverride->isChecked() ? '1' : '0'); // card-enable check override
        stream << (waitforstartoverride->isChecked() ? '1' : '0'); // wait-for-start override
        stream << '\n';
        stream << rfchanBox->value();
        stream << '\n';
        cfg.close();

        WriteConfigProcess *process = new WriteConfigProcess(device, cfg.fileName());
        process->start();

        QMessageBox *waitbox = new QMessageBox();
        waitbox->setWindowTitle("Please wait");
        waitbox->setText("Writing new logger config. Please wait ... ");
        waitbox->setIcon(QMessageBox::Information);
        waitbox->setStandardButtons(QMessageBox::NoButton);
        connect(process, &AbstractProcess::processfinished, waitbox, &QMessageBox::accept);
        waitbox->show();

        if(process->waitForFinished(30000) && process->getReturnCode()==0){
            if(samplingrate20khz->isChecked() != sample20khzOn && rfchanBox->value() != rfChan){
                //both were changed. ask to change both
                auto response = QMessageBox::question(this, "Change RF Channel and sampling rate on MCU/Dock",
                                      "You changed the RF channel on your logger to " + QString::number(rfchanBox->value()) +
                                      " and sampling rate to " +  QString::number(samplingrate20khz->isChecked()?20:30) +
                                      ". Would you like to also change them on your MCU/Dock? (Make sure it is connected)");
                if(response == QMessageBox::Yes){
                    ControllerSamplingRateProcess *spprocess = new ControllerSamplingRateProcess(device, samplingrate20khz->isChecked()?20:30);
                    spprocess->disableErrorWarning();
                    spprocess->start();
                    ControllerRFChannelProcess *rfprocess = new ControllerRFChannelProcess(device, rfchanBox->value());
                    rfprocess->disableErrorWarning();
                    rfprocess->start();
                }
                else{

                }
            }
            else if(rfchanBox->value() != rfChan){
                //rfchan was changed. ask to change mcu/dock rf chan as well
                auto response = QMessageBox::question(this, "Change RF Channel on MCU/Dock",
                                      "You changed the RF channel on your logger to " + QString::number(rfchanBox->value()) +
                                      ". Would you like to also change it on your MCU/Dock? (Make sure it is connected)");
                if(response == QMessageBox::Yes){
                    ControllerRFChannelProcess *rfprocess = new ControllerRFChannelProcess(device, rfchanBox->value());
                    rfprocess->disableErrorWarning();
                    rfprocess->start();
                }
                else{

                }
            }
            else if(samplingrate20khz->isChecked() != sample20khzOn){
                //sampling rate was changed. ask to change mcu/dock sampling rate as well
                auto response = QMessageBox::question(this, "Change sampling rate on MCU/Dock",
                                      "You changed the sampling rate on your logger to " + QString::number(samplingrate20khz->isChecked()?20:30) +
                                      "khz. Would you like to also change it on your MCU/Dock? (Make sure it is connected)");
                if(response == QMessageBox::Yes){
                    ControllerSamplingRateProcess *spprocess = new ControllerSamplingRateProcess(device, samplingrate20khz->isChecked()?20:30);
                    spprocess->disableErrorWarning();
                    spprocess->start();
                }
                else{

                }
            }
            else{
                //rfchan and samplerate were not changed
                QMessageBox::information(this, "Config written", "The new configuration settings have been written.");
                accept();
            }
        }
        else{
            qDebug() << process->getOutput() << process->getReturnCode();
            printf("Error writing config. %s Return code: %d\n", process->getOutput().toLocal8Bit().data(), process->getReturnCode());
            QMessageBox::warning(this, "Error writing config", "There has been an error writing the new config. Please try again or contact support.");
        }
    }
    else{
        qDebug() << "Error creating temporary .cfg file!";
        reject();
    }
    //QMessageBox::information(this, "Config written", "The new configuration settings have been written.");
    accept();
}

void LoggerConfigWidget::loadfile()
{
//    QFileDialog::getOpenFileName();
}

#include "abstractprocess.h"

AbstractProcess::AbstractProcess(QString title, QWidget *parent)
    : QWidget(parent), title(title), process(NULL), console(NULL), disableWarning(false)
{
    process = new QProcess;
    process->setProcessChannelMode(QProcess::MergedChannels);
    console = new QTextEdit(this);
    console->setWindowFlag(Qt::Window);
    console->setReadOnly(true);
    console->setWindowTitle(title + " Process");
    console->append(title+"\n");
    const QFont fixedFont = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    console->setFont(fixedFont);
    console->append("=============================\n");
    console->resize(400,300);
//    console->show();
//    console->raise();
    console->setAttribute(Qt::WA_DeleteOnClose);
    connect(console, &QTextEdit::destroyed, this, &AbstractProcess::deleteLater);

    connect(process, &QProcess::readyReadStandardOutput, this, &AbstractProcess::readOutput);
    connect(process, &QProcess::readyReadStandardError, this, &AbstractProcess::readErrorOutput);
    connect(process, &QProcess::started, this, &AbstractProcess::processstarted);
    connect(process, &QProcess::started, console, &QTextEdit::raise);
    connect(process, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            [=](int exitCode, QProcess::ExitStatus exitStatus){ processFinished(exitCode, exitStatus);});
}

void AbstractProcess::killProcess() {
    if (process) {
        process->kill();
        process->waitForFinished();
    }
}

void AbstractProcess::processFinished(int code, QProcess::ExitStatus status){
    //qDebug() << "Process" << title << "finished with code" << code;
    if(code){
        qDebug() << "Error running" << title << "Code" << code;
        if(!disableWarning && !errorlist.isEmpty()) {
            QString errors = errorlist.join("\n");
            QMessageBox::warning(this, "Error running " + title,
                                 title + " returned with the following error messages: \n" +
                                 errors);
        }
    }
    console->raise();
    emit processfinished(code);
//    delete process;
}

void AbstractProcess::disableErrorWarning(){
    disableWarning = true;
}

void AbstractProcess::readOutput(){
    QString output = QString(process->readAllStandardOutput());
    console->append(output);
    QStringList lines = output.split("\n");
    for(auto const l : lines){

        if(l.startsWith("Error", Qt::CaseInsensitive)){
            errorlist.append(l);
        }
    }
    alloutput.append(output);
    customReadOutput(output);
    emit newOutputLine(output.trimmed());
}

void AbstractProcess::readErrorOutput(){
    /*QString output = QString(process->readAllStandardError());
    console->append(output);
    QStringList lines = output.split("\n");
    for(auto const l : lines){

        if(l.startsWith("Error", Qt::CaseInsensitive)){
            errorlist.append(l);
        }
    }
    alloutput.append(output);
    customReadOutput(output);
    emit newOutputLine(output.trimmed());*/
}

void AbstractProcess::customReadOutput(const QString &line){

}

void AbstractProcess::hideConsole(){
    console->hide();
}

bool AbstractProcess::waitForFinished(int msecs){
    return process->waitForFinished(msecs);
}

QString AbstractProcess::getOutput(){
    return alloutput;
}

int AbstractProcess::getReturnCode()
{
    return process->exitCode();
}

#include "dockconfigwidget.h"
#include "abstractprocess.h"
DockConfigWidget::DockConfigWidget(QString deviceName, QWidget *parent) : QDialog (parent)
{
    device = deviceName;
    QGridLayout *layout = new QGridLayout;

    QProcess getsettings;
    getsettings.start(DOCKINGPATH, {"-m","-x",device});
    getsettings.waitForFinished();
    QString output = getsettings.readAllStandardOutput();
    QTextStream stream(&output);

    QDialogButtonBox *buttons = new QDialogButtonBox(QDialogButtonBox::Apply | QDialogButtonBox::Cancel);
    connect(buttons->button(QDialogButtonBox::Apply), &QPushButton::clicked, this, &QDialog::accept);
    connect(buttons, &QDialogButtonBox::rejected, this, &QDialog::reject);

    int row = 1;
    QString firstline = stream.readLine();
    printf("\n%s\n",firstline.toLocal8Bit().data());
    if(firstline.contains("settings", Qt::CaseInsensitive)){
        layout->addWidget(new QLabel(firstline), 0, 0, 1, 2);
        while(!stream.atEnd()){
            QString line = stream.readLine();
            printf("%s\n",line.toLocal8Bit().data());
            int splitind = line.indexOf(':'); //first index of ':'
            if(splitind == -1){
                continue;
            }
            QString left = line.left(splitind+1);
            QString right = line.mid(splitind+1).trimmed();

            QLabel *label = new QLabel(left);
            layout->addWidget(label, row, 0);
            labels.append(label);

            QLineEdit *text = new QLineEdit(right);
            layout->addWidget(text, row, 1);
            textfields.append(text);

            row++;
        }
    }
    else if(firstline.contains("not supported", Qt::CaseInsensitive)){
        buttons->button(QDialogButtonBox::Apply)->setEnabled(false);
        QLabel *label = new QLabel("Not supported in this firmware version. Check for firmware updates");
        layout->addWidget(label, 0, 0, 1, 2);
    }

    layout->addWidget(buttons);
    setLayout(layout);
}

void DockConfigWidget::accept(){
    if(QMessageBox::Yes != QMessageBox::warning(this, "Confirm changes", "Changing dock settings. Continue?", QMessageBox::Yes|QMessageBox::No)){
        return;
    }
    printf("\nWriting settings for dock:\n");

    CommandEngine dockCommandEngine;


    QByteArray devArray = device.toLocal8Bit();
    dockCommandEngine.setCurrentDeviceName(devArray.data());


    //char **settingStrings;
    char **settingStrings = (char**) calloc(labels.length(), sizeof(char*));

    /*for ( i = 0; i < num_elements; i++ )
    {
        arr[i] = (char*) calloc(num_elements_sub, sizeof(char));
    }*/

    //(char**)settingStrings.data()


    QProcess setsettings;
    QStringList args("-n");

    args << "-x" << device;
    args << "-n";

    for(int i = 0; i < labels.length(); ++i){
        auto label = labels[i];
        auto lineEdit = textfields[i];
        settingStrings[i] = (char*) calloc(lineEdit->text().length()+1, sizeof(char));

        strncpy(settingStrings[i],lineEdit->text().toLocal8Bit().data(),lineEdit->text().length());
        printf("%s ........ %s\n",label->text().toLocal8Bit().data(),lineEdit->text().toLocal8Bit().data());
        args << lineEdit->text();
    }

    int retVal = dockCommandEngine.setDockSettings(labels.length(),settingStrings);
    for(int i = 0; i < labels.length(); ++i){
        free(settingStrings[i]);
    }
    free(settingStrings);

    if (retVal) {
        QMessageBox::warning(this, "Error writing settings",
                             QString("Error when writing settings to dock. Returned error (code %1)").arg(retVal));

    } else {
        //Sucessful, show message
        QMessageBox::information(this, "Successful settings write", "Settings successfully written to Dock");
        QDialog::accept();
    }

    /*setsettings.start(DOCKINGPATH, args);
    if(setsettings.waitForFinished()){
        //Sucessful, show message
        QMessageBox::information(this, "Successful settings write", "Settings successfully written to Dock");
        QDialog::accept();
    }
    else{
        //Something wrong, pop up message
        QMessageBox::warning(this, "Error writing settings",
                             QString("Error when writing settings to dock. Returned error (code %1): %2").
                                    arg(setsettings.exitCode()).
                                    arg(QString(setsettings.readAllStandardOutput())));
    }*/
}

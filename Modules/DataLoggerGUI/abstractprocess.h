#ifndef ABSTRACTPROCESS_H
#define ABSTRACTPROCESS_H

#include <QtWidgets>
#include <QProcess>
#include <QMap>
#include <QMutex>
#include "DockConsoleUtility/commandengine.h"


extern QString DOCKINGPATH;
extern QString MCUUTILPATH;

class AbstractProcess : public QWidget
{
    Q_OBJECT
public:
    explicit AbstractProcess(QString title, QWidget *parent = nullptr);
    void hideConsole();
    bool waitForFinished(int msecs);
    QString getOutput();
    int getReturnCode();
    void disableErrorWarning();

    virtual void start() = 0;

    //locks for devices. Can't run multiple processes on the same device.
    static QMap<QString, QMutex> devicelocks;
signals:
    void processstarted();
    void processfinished(int code);
    void newOutputLine(const QString &);

public slots:
    void processFinished(int code, QProcess::ExitStatus status);
    void killProcess();

protected:
    CommandEngine dockCommandEngine;
    QString title;
    QProcess *process;
    QTextEdit *console;
    QString alloutput;
    bool disableWarning;
    QStringList errorlist;
    virtual void customReadOutput(const QString &line);

private slots:
    void readOutput();
    void readErrorOutput();
};

#endif // ABSTRACTPROCESS_H

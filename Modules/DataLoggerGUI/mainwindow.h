#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>
#include <QAction>
#include <QtNetwork>

#include "filebrowser.h"
#include "disklistwidget.h"
#include "extractprocess.h"
#include "mergeprocess.h"
#include "cardenableprocess.h"
#include "pcheckprocess.h"
#include "sdtorecprocess.h"
#include "writeconfigprocess.h"
#include "readconfigprocess.h"
#include "dockingquickstart.h"
#include "firmwareupdater.h"
#include "jtagprocess.h"
#include "dockprogrammingprocess.h"
#include "configuration.h"

#define DLGUI_VERSION "1.6.0"
#define DLGUI_INTVERSION 160

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void closeEvent(QCloseEvent *event);
    void setEnvironmentalRecordingPath(QString path);

private:
    void generateMenus();
    void generateLayouts();
    void createConnects();
    void readSettings();
    void saveSettings();

    QMenu       *menuHelp;
    QAction         *actionAbout;

    //
    QSplitter           *TopHalf;
    QVBoxLayout *bb;

    //Extract SD card
    QVBoxLayout         *SDLayout;
    DiskListWidget      *deviceslist;
    DockingSettings     *dockingsettings;
    QPushButton         *runSDExtractButton;

    //Merge files
    QGroupBox           *extrarecGroupBox;
    QRadioButton        *autogenWorkspace;
    QRadioButton        *browseWorkspace;
    QPushButton         *CreateRecButton;

    QRadioButton *chanmaplabel;
    QRadioButton *manualmaplabel;
    QRadioButton *autofilllabel;
    QComboBox *chooseChansPerNtrode;
    QPushButton         *browseRecButton;
    QLineEdit           *trodesRecEntry;
    QPushButton         *browseConfButton;
    QLineEdit           *trodesConfEntry;

    QLineEdit *finalFileLineEdit;

    QTextEdit *processconsole;
    QProgressBar *progressBar;
    QLabel *progressLabel;
    QLabel *progressStatus;

    QPushButton  *closeButton;


    QString             trodesRecFilePath;
    QString             trodesConfInputPath;
    QString             mergeOutputFilePath;
    int                 numchannels;

    QString             cardenableDevice;

    //QSettings variables
    QString             defaultCfgFile;
    QString             defaultRecDir;
    QString             defaultTrodesConfigDir;
    QString             defaultMergeOutputDir;

    ExtractProcess      *extractprocess;
    int getNumChannelsFromFile(const QString &filename);

    FirmwareUpdater *firmwareupdater;

    QList<int>          latestManualMappings;
public slots:
//    void browseSDOutputPath();
//    void browseDatPath();
    void browseRecPath();
    void browseConfPath();
    void browseFinalRecOutputPath();
    void browseChannelMapPath();
    void configureChannelsManually();

//    void setSDOutputPath(QString selectedpath);
    void setTrodesConfPath(QString selectedpath);
    void setOutputPath(QString selectedpath);

    void runPcheck();
    void runCardEnable();
    void runReadConfig();
    void runWriteConfig();
//    void runExtractSDCard();
//    void runSdToRec();
//    void runMergeFiles();
    void runFirmwareUpdate(QString device, QString file);

    void setupEntireExtract();
    void runEntireExtract(QString datfile, bool mergerec, QString finalrec, QString finalworkspace, int numchannels, QString mergefile);

    void pcheckStarted();
    void cardenableStarted();
    void readconfigStarted();
    void writeconfigStarted();
    void extractStarted();
    void sdtorecStarted();
    void mergeStarted();

    void pcheckFinished(int code);
    void cardenableFinished(int code);
    void readconfigFinished(int code);
    void writeconfigFinished(int code);
    void extractFinished(int code);
    void sdtorecFinished(int code);
    void mergeFinished(int code);

    void newProcessOutputLine(const QString & line);
    void deviceSelected();
    void checkValidMergeInputs();

    void entireExtractCompleted(int code, bool autocomplete, QString finalworkspace);

    void openAboutDialog();

    void checkForUpdate();
signals:
    void SDOutputFileSelected(QString);
    void SDDatFileProcessed(QString);
    void checkinputs();
    void killAllProcesses();
};

#endif // MAINWINDOW_H

#ifndef READCONFIGPROCESS_H
#define READCONFIGPROCESS_H

#include "abstractprocess.h"

class ReadConfigProcess : public AbstractProcess
{
    Q_OBJECT
public:
    ReadConfigProcess(QString device, QWidget *parent=nullptr);
    void start();

private:
    QString device;
    void sd_readconfig();
    void docking_readconfig();

protected:
    void customReadOutput(const QString &line);
};

#endif // READCONFIGPROCESS_H

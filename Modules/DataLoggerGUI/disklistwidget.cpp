#include "disklistwidget.h"
#include <iostream>
#include <sstream>
#include <QtDebug>
#ifdef _WIN32
#include "winuser.h"
#endif
#include "readconfigprocess.h"
#include "pcheckprocess.h"
#include "loggerconfigwidget.h"
#include "dockconfigwidget.h"

DiskListWidget::DiskListWidget(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *layout = new QVBoxLayout;
    layout->setSpacing(2);
    setLayout(layout);

    QLabel *titleLabel = new QLabel("Detected Storage Devices");
    layout->addWidget(titleLabel);

    QHBoxLayout *buttonslayout = new QHBoxLayout;
//    QWidget *dummy = new QWidget;
//    dummy->setLayout(buttonslayout);
    layout->addLayout(buttonslayout);

    refreshbutton = new QPushButton("Refresh list");
//    refreshbutton->setMaximumWidth(100);
    buttonslayout->addWidget(refreshbutton, 0, Qt::AlignLeft);
    connect(refreshbutton, &QPushButton::pressed, this, &DiskListWidget::updateDevices);

    QPushButton *pcheckButton = new QPushButton("Data Check");
    pcheckButton->setToolTip("Check packets on this SD card and output results");
//    buttonslayout->addWidget(pcheckButton, 0, Qt::AlignLeft);
    connect(pcheckButton, &QPushButton::pressed, this, &DiskListWidget::pcheck);




    QPushButton *readconfigButton = new QPushButton("Read Config");
    readconfigButton->setToolTip("Read and display the configuration of this SD card");
//    buttonslayout->addWidget(readconfigButton, 0, Qt::AlignLeft);
    connect(readconfigButton, &QPushButton::clicked, this, &DiskListWidget::readconfig);

//    QPushButton *advancedbutton = new QPushButton("Advanced>>");
//    buttonslayout->addWidget(advancedbutton, 0, Qt::AlignLeft);
//    advancedbutton->setCheckable(true);

    enableButton = new QPushButton("Enable for recording");
    char tTip[] = "<html><head/><body><p>" \
                  "Enable this logger/SD card for recording or settings changes. A locked card prevents accidental data erasure." \
                  "</p></body></html>";
    enableButton->setToolTip(tTip);
//    cardenableButton->setMaximumWidth(100);
    buttonslayout->addWidget(enableButton, 0, Qt::AlignLeft);
    connect(enableButton, &QPushButton::clicked, this, &DiskListWidget::cardenable);

    loggerSettingsButton = new QPushButton("Edit logger config");
    loggerSettingsButton->setToolTip("Edit the configuration for this logger/SD");
    buttonslayout->addWidget(loggerSettingsButton, 0, Qt::AlignLeft);
    connect(loggerSettingsButton, &QPushButton::clicked, this, &DiskListWidget::writeconfig);

    dockSettingsButton = new QPushButton("Edit device settings");
    dockSettingsButton->setToolTip("Edit settings like rf channel, samplingrate, description, etc.");
    buttonslayout->addWidget(dockSettingsButton, 0, Qt::AlignLeft);
    connect(dockSettingsButton, &QPushButton::clicked, this, &DiskListWidget::dockconfig);
//    connect(advancedbutton, &QPushButton::toggled, writeconfigButton, &QPushButton::setVisible);
//    connect(advancedbutton, &QPushButton::toggled, cardenableButton, &QPushButton::setVisible);
//    advancedbutton->toggle();advancedbutton->toggle();

    //buttonslayout->addStretch();
    deviceslist = new QTreeView;
    model = new QStandardItemModel();
    deviceslist->setModel(model);
    deviceslist->setMinimumHeight(40);
    deviceslist->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    connect(deviceslist, &QTreeView::clicked, this, &DiskListWidget::deviceSelected);
    QHBoxLayout *existing = new QHBoxLayout;
    QLabel *haveexisting = new QLabel("Already have an extracted .dat file?");
    QPushButton *existingdat = new QPushButton("Browse existing .dat ...");
    existingDatSelected = new QLabel();
    existing->addWidget(haveexisting, 0, Qt::AlignLeft);
    existing->addWidget(existingdat, 0, Qt::AlignLeft);
    connect(existingdat, &QPushButton::clicked, this, &DiskListWidget::selectDatFile);

    layout->addWidget(deviceslist, 2);
    layout->addLayout(existing);
    layout->addWidget(existingDatSelected);
    layout->setSpacing(2);
    //updateDevices();

    connect(&updateTimer, &QTimer::timeout,this,&DiskListWidget::updateDevices);
    //updateTimer.start(5000);


}

void DiskListWidget::editStatus(const QString &device, const QString &status){
    for(int i = 0; i < model->rowCount(); i++){
        if(model->item(i)->text() == device){
            model->item(i, 3)->setText(status);
            break;
        }
    }
}

void DiskListWidget::cardenable(){
    if(!getSelectedDevicePath().isEmpty()){
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Card Enable",
                                      "Enable this device for recording?",QMessageBox::Yes|QMessageBox::No);
        if(reply == QMessageBox::Yes){
            emit cardEnableCalled();
        }
        else{
            return;
        }
    }
}

void DiskListWidget::pcheck(){
    if(!getSelectedDevicePath().isEmpty()){
        emit pcheckCalled();
    }
}

void DiskListWidget::readconfig(){
    if(!getSelectedDevicePath().isEmpty()){
        emit readconfigCalled();
    }
}

void DiskListWidget::writeconfig(){
    if(!getSelectedDevicePath().isEmpty()){
//        emit writeconfigCalled();
        LoggerConfigWidget* w = new LoggerConfigWidget(getSelectedDeviceSettings(), getSelectedDevicePath());
        if(w->exec() == QDialog::Accepted){
            updateDevices();
        }
    }
}

void DiskListWidget::dockconfig(){


    DockConfigWidget *w = new DockConfigWidget(getSelectedDevicePath());
    if(w->exec() == QDialog::Accepted){
        //settings changed after accept was pressed.
        //don't do anything here, a message pop up created
        // in the widget's accept() implementation whether success or failure
    }

}

void DiskListWidget::updateDevices(){
//    qDebug() << "Updating device info";
    //disable refresh for 1 sec
    refreshbutton->setEnabled(false);
    QTimer::singleShot(1000, refreshbutton, [this](){
        refreshbutton->setEnabled(true);
    });

    model->clear();
    model->setColumnCount(3);
    model->setHorizontalHeaderItem(0, new QStandardItem("Device"));
    model->setHorizontalHeaderItem(1, new QStandardItem("Size"));
    model->setHorizontalHeaderItem(2, new QStandardItem("Type"));
    model->setHorizontalHeaderItem(3, new QStandardItem("Status"));


    QStringList supportedDevList;
    supportedDevList << "DockingStation A" << "Spikegadgets MCU A";

    for (int devInd=0;devInd<supportedDevList.length();devInd++) {
        CommandEngine dockCommandEngine;
        CommandEngine::DockingStation_t d;

        QByteArray devArray = supportedDevList.at(devInd).toLocal8Bit();
        d.deviceName = devArray.data();
        int ret = dockCommandEngine.readDockingstation(&d, 1, 1, 0, 0, 0, 0);

        LoggerRecording::hwsetup setup;
        QString devName;
        if(ret == 0){


            if(d.hs_detected){
                setup = LoggerRecording::HS_DOCK;
                devName = "Headstage";
            }
            else if(d.sd_detected){
                setup = LoggerRecording::SD_DOCK;
                devName = "SD Card";
            }
            else{
                setup = LoggerRecording::NONE;
                devName = "None mounted";
            }


            QString disksize = QString::number((double)d.disk_size_MB/1024.0, 'g', 4) + " GB";

            QList<QStandardItem*> row;
            row.append(new QStandardItem(d.deviceName));
            row.append(new QStandardItem(disksize.trimmed()));
            row.append(new QStandardItem(devName));
            if (d.sd_card_enabled_for_record == 1) {
                row.append(new QStandardItem("Enabled for recording"));
            } else if (d.sd_card_enabled_for_record == 0) {
                row.append(new QStandardItem("NOT enabled for recording"));
            } else {
                row.append(new QStandardItem(""));
            }
            model->appendRow(row);

            deviceslist->setCurrentIndex(model->index(model->rowCount()-1, 0));
            fetchDeviceSettings(getSelectedDevicePath(), setup);

            emit rowSelected();

        }
        else{


            if (d.dock_USB_communication_ok == 0) {

                /*QMessageBox::warning(this, "Connection error",
                                    "No USB communication established with dock.");*/
            }

            else{
                //warning if plugged in, and something went wrong.
                QMessageBox::warning(this, "Error reading data from SD card or headstage.",
                                     "An error occurred when accessing the dock. Please try the following: \n"
                                     "1. Click refresh. The dock may still be initializing.\n"
                                     "2. If still problematic, restart your dock and re-mount your headstage.");
            }
        }
    }

    emit listUpdated();


}

void DiskListWidget::windows_parse_listDevices(const std::string &output){
    if(output.find("PHYSICALDRIVE") != output.npos){
        std::istringstream iss(output);
        std::string deviceid, size;
        QList<QStandardItem*> items;

        iss >> deviceid; // device
        iss >> size;
        items.append(new QStandardItem(deviceid.c_str()));
        items.last()->setFlags(items.last()->flags() & ~Qt::ItemIsEditable);

        //size
        double sz = QString::fromStdString(size).toDouble();
        sz = (((sz/1024)/1024)/1024);
        items.append(new QStandardItem(QString::number(sz) + " GB"));
        items.last()->setFlags(items.last()->flags() & ~Qt::ItemIsEditable);

        items.append(new QStandardItem("Removable"));
        items.last()->setFlags(items.last()->flags() & ~Qt::ItemIsEditable);

        items.append(new QStandardItem());
        model->appendRow(items);
    }
}

void DiskListWidget::linux_parse_listDevices(const std::string &output){
//    qDebug() << QString::fromStdString(output);
    std::istringstream iss(output);
    std::string s;
    QList<QStandardItem*> items;

    iss >> s;//RM?
    if(s=="0"){
        //Not removable
        return;
    }
    QString removable = (s=="1") ? "Removable" : "Not Removable";

    iss >> s; //NAME
    items.append(new QStandardItem(s.c_str()));
    items.last()->setFlags(items.last()->flags() & ~Qt::ItemIsEditable);

    iss >> s; //TYPE
    iss >> s; //SIZE in bytes
    double size = QString(s.c_str()).toDouble()/1024.0/1024.0/1024.0; //convert from Bytes to GB
    items.append(new QStandardItem(QString::number(size, 'g', 4) + " GB"));
    items.last()->setFlags(items.last()->flags() & ~Qt::ItemIsEditable);

    items.append(new QStandardItem(removable));
    items.last()->setFlags(items.last()->flags() & ~Qt::ItemIsEditable);

    items.append(new QStandardItem());
    items.last()->setFlags(items.last()->flags() & ~Qt::ItemIsEditable);
    model->appendRow(items);
}

void DiskListWidget::macos_parse_listDevices(const std::string &out)
{
//    qDebug() << QString::fromStdString(output);
    /*QString device = QString::fromStdString(out).trimmed();
    QProcess p;
    p.start("bash", {"-c", QString("diskutil info %1").arg(device)});
    if(!p.waitForFinished()){
        return;
    }

    QStringList output = QString(p.readAllStandardOutput().trimmed()).split('\n');
    QRegularExpression rex(".*Removable Media:.*");
    QRegularExpressionMatch m = rex.match(output[0]);
    int rmInd;
    if (!m.hasMatch()) {
        return;

    }
    rmInd = m.captured()


    QString removable = output[rmInd].remove("Removable Media:").trimmed();
    if(removable != "Removable"){
        return;
    }

    int sizeInd = output.indexOf(QRegExp(".*Disk Size:.*"));
    if(sizeInd == -1){
        return;
    }

    QRegularExpression sizerx("Disk Size:\\s+(.*GB)\\s+\\(([0-9]* Bytes)\\) .*");
    QRegularExpressionMatch m2 = sizerx.match(output);
    if (m2.hasMatch() && m2.lastCapturedIndex() > 1) {
        double size = m2.captured(2).remove("Bytes").toDouble()/1024.0/1024.0/1024.0;
        QList<QStandardItem*> items;
        items.append(new QStandardItem(device));
        items.last()->setFlags(items.last()->flags() & ~Qt::ItemIsEditable);
        items.append(new QStandardItem(QString::number(size, 'g', 4) + " GB"));
        items.last()->setFlags(items.last()->flags() & ~Qt::ItemIsEditable);
        items.append(new QStandardItem(removable));
        items.last()->setFlags(items.last()->flags() & ~Qt::ItemIsEditable);
        items.append(new QStandardItem());
        items.last()->setFlags(items.last()->flags() & ~Qt::ItemIsEditable);
        model->appendRow(items);

    }*/

}

void DiskListWidget::deviceSelected(const QModelIndex &index){
//    int row = model->itemFromIndex(index)->row();
//    selectedDevicePath = model->item(row, 0)->text();
//    selectedRow = row;
    emit rowSelected();
    selectedDatFile = "";
    existingDatSelected->setText(selectedDatFile);
}

void DiskListWidget::allowLoggerSettingsChange(bool allow) {
    if (allow) {
        loggerSettingsButton->setEnabled(true);
    } else {
        loggerSettingsButton->setEnabled(false);
    }
}

void DiskListWidget::allowDockSettingsChange(bool allow) {
    if (allow) {
        dockSettingsButton->setEnabled(true);
    } else {
        dockSettingsButton->setEnabled(false);
    }
}

void DiskListWidget::allowEnable(bool allow) {
    if (allow) {
        enableButton->setEnabled(true);
    } else {
        enableButton->setEnabled(false);
    }
}

QString DiskListWidget::getSelectedDevicePath() const{
    const QModelIndex cur = deviceslist->currentIndex();
    QString ret = "";
    if(specificDatFileSelected()){
        ret = "";
    }
    else if(!cur.isValid())
        ret = "";
    else if(model->itemFromIndex(cur)){
        int row = model->itemFromIndex(cur)->row();
        ret = model->item(row, 0)->text();
    }
    return ret;
//    return selectedDevicePath;
}

QByteArray DiskListWidget::saveColumnGeometry() const{
    QByteArray d;
//    QDataStream stream(&d, QIODevice::WriteOnly);

//    stream << deviceslist->columnWidth(0);
//    stream << deviceslist->columnWidth(1);
    return d;
}

void DiskListWidget::loadColumnGeometry(const QByteArray &d){

}

LoggerRecording DiskListWidget::getSelectedDeviceSettings(){
    return devicesettings.value(getSelectedDevicePath());
}

void DiskListWidget::fetchDeviceSettings(QString device, LoggerRecording::hwsetup setup){
    if(device.isEmpty()){
        return;
    }

    if(!device.isEmpty()){

        CommandEngine dockCommandEngine;
        CommandEngine::DockingStation_t d;
        QByteArray devArray = device.toLocal8Bit();
        d.deviceName = devArray.data();
        int returnCode = dockCommandEngine.readDockingstation(&d, 1, 1, 1, 0, 0, 0);


        HeadstageSettings s;
        LoggerRecording rec;
        s.hsSerialNumber = d.hs_serial_number;
        s.hsTypeCode = d.hs_model_number;
        s.majorVersion = d.hs_major_version;
        s.minorVersion = d.hs_minor_version;
        s.numberOfChannels = d.channels;

        s.magSensorOn = d.mag_enabled;
        s.magSensorAvailable = true; //this need to be fixed
        s.accelSensorOn = d.acc_enabled;
        s.accelSensorAvailable = true; //;this needs to be fixed
        s.gyroSensorOn = d.gyr_enabled;
        s.gyroSensorAvailable = true; //this needs to be fixed
        s.smartRefOn = d.smartref_enabled;
        s.smartRefAvailable = true; //this needs to be fixed
        s.rfChannel = d.rf_channel;
        s.rfAvailable = true; //this needs to be fixed
        s.auxbytes = d.aux_size_bytes;
        s.sample12bitOn = (d.sample_size_bits==12);
        s.sample12bitAvailable = true; //this needs to be fixed
        s.samplingRate = d.sample_rate_khz*1000;
        s.sample20khzOn = (d.sample_rate_khz==20);
        s.sample20khzAvailable = true; //this needs to be fixed
        s.packetSize = d.packet_size_bytes;
        s.waitForStartOverride = d.waitforstart_override;
        s.cardEnableCheckOverride = d.cardenablecheck_override;
        s.valid = true;

        rec.maxPackets = d.max_packets;
        rec.recordedPackets = d.recorded_packets;
        rec.droppedPackets = d.dropped_packets;
//                stream >> rec.recordingDateTime;
        rec.recordingDateTime.fromUtf8(d.recording_datetime,80);
        //rec.recordingDateTime = stream.readLine(80);
        rec.settings = s;
        rec.enabledForRecording = d.sd_card_enabled_for_record;
        rec.setup = setup;
        devicesettings.insert(device, rec);
        if(getSelectedDevicePath() == device){
            emit rowSelected();
        }






    }
    else{
        ReadConfigProcess *p = new ReadConfigProcess(device);
        p->hideConsole();
        p->start();
        bool ret = p->waitForFinished(1000);


        if(ret ){
            PcheckProcess *pp = new PcheckProcess(device);
            pp->hideConsole();
            pp->start();
            pp->waitForFinished(1000);

            HeadstageSettings s;
            LoggerRecording rec;
            s.magSensorOn = true; s.magSensorAvailable = true;
            s.accelSensorOn = true; s.accelSensorAvailable = true;
            s.gyroSensorOn = true; s.gyroSensorAvailable = true;
            s.smartRefOn = false; s.smartRefAvailable = true;
            s.rfAvailable = true;
            s.sample12bitOn = false; s.sample12bitAvailable = true;
            s.samplingRate = 30000; s.sample20khzOn = false; s.sample20khzAvailable = true;

            QString output = p->getOutput();
            QTextStream stream(&output);
            QString line;
            while(stream.readLineInto(&line)){
                if(line.contains(" channels enabled", Qt::CaseInsensitive)){
                    s.numberOfChannels = line.remove(" channels enabled", Qt::CaseInsensitive).toInt();
                } else if(line.contains("20 kHz sample", Qt::CaseInsensitive)){
                    s.sample20khzOn = true;
                    s.samplingRate = 20000;
                } else if(line.contains("Magnatometer disable", Qt::CaseInsensitive)){
                    s.magSensorOn = false;
                } else if(line.contains("Accelerometer disable", Qt::CaseInsensitive)){
                    s.accelSensorOn = false;
                } else if(line.contains("Gyro disable", Qt::CaseInsensitive)){
                    s.gyroSensorOn = false;
                } else if(line.contains("12-bit sample", Qt::CaseInsensitive)){
                    s.sample12bitOn = true;
                } else if(line.contains("RF channel set to", Qt::CaseInsensitive)){
                    s.rfChannel = line.remove("RF channel set to ").toInt();
                } else if(line.contains("Smart reference", Qt::CaseInsensitive)){
                    s.smartRefOn = true;
                } else if(line.contains("Wait-for-start", Qt::CaseInsensitive)){
                    s.waitForStartOverride = true;
                } else if(line.contains("Card-enable-check", Qt::CaseInsensitive)){
                    s.cardEnableCheckOverride = true;
                }
            }

            s.packetSize = s.numberOfChannels*2 + 6;
            output = pp->getOutput();
            stream.setString(&output);
            while(stream.readLineInto(&line)){
                if(line.contains("Aux data size:", Qt::CaseInsensitive)){
                    int auxsize = line.remove("Aux data size:").remove("bytes").toInt();
                    s.packetSize += auxsize;
                } else if(line.contains("Maximum packets", Qt::CaseInsensitive)){
                    rec.maxPackets = line.remove("Maximum packets on the disk = ").toInt();
                } else if(line.contains("Packets recorded", Qt::CaseInsensitive)){
                    rec.recordedPackets = line.remove("Packets recorded on the disk = ").toInt();
                } else if(line.contains("Dropped packets", Qt::CaseInsensitive)){
                    QString temp = line.remove("Dropped packets = ");
                    temp.truncate(line.indexOf('('));
                    rec.droppedPackets = temp.toInt();
                } else if(line.contains("Recording date:", Qt::CaseInsensitive)){
                    rec.recordingDateTime = line.remove("Recording date: ");
                }
            }
            s.valid = true;
            rec.settings = s;
            rec.setup = setup;
            devicesettings.insert(device, rec);
            if(getSelectedDevicePath() == device){
                emit rowSelected();
            }
        }
    }
}


void DiskListWidget::selectDatFile(){

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("DataLoggerGUI"));
    settings.beginGroup(QLatin1String("Paths"));
    QString datDir = settings.value(QLatin1String("defaultDatDir")).toString();
    settings.endGroup();

#if defined (__linux__)
     //QString datfile = QFileDialog::getOpenFileName(this, "Open existing .dat file", datDir, "Logger data files (*.dat)",nullptr, QFileDialog::DontUseNativeDialog);
     QString datfile = QFileDialog::getOpenFileName(this, "Open existing .dat file", datDir, "Logger data files (*.dat)");
#else
     QString datfile = QFileDialog::getOpenFileName(this, "Open existing .dat file", datDir, "Logger data files (*.dat)");
#endif

    if(!datfile.isNull()){
        //save selected datfile
        selectedDatFile = datfile;
        existingDatSelected->setText(selectedDatFile);

        //deselect devices list
        deviceslist->clearSelection();
    }
    emit rowSelected();
}

/*void DiskListWidget::dockDetectFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    QProcess *dockingstation = qobject_cast<QProcess*>(sender());
    int ret = exitCode;
    if(ret == 0){
        dockingstation->readLine();
        QString device(dockingstation->readLine());
        device.remove(0, 8);//Remove "Device: "
        LoggerRecording::hwsetup setup;
        if(device.contains("Headstage")){
            setup = LoggerRecording::HS_DOCK;
        }
        else if(device.contains("SD Card")){
            setup = LoggerRecording::SD_DOCK;
        }
        else{
            setup = LoggerRecording::NONE;
        }

        QString disksize(dockingstation->readLine());
        disksize.remove(0, 6);//Remove "Size: "
        disksize.remove("MB");
        double size = disksize.toDouble()/1024.0;
        disksize = QString::number(size, 'g', 4) + " GB";

        QList<QStandardItem*> row;
        row.append(new QStandardItem("Docking Station"));
        row.append(new QStandardItem(disksize.trimmed()));
        row.append(new QStandardItem(device.trimmed()));
        row.append(new QStandardItem(""));
        model->appendRow(row);
        if(setup != LoggerRecording::NONE){
            deviceslist->setCurrentIndex(model->index(model->rowCount()-1, 0));
            if(getSelectedDevicePath() == "Docking Station")
                fetchDeviceSettings("Docking Station", setup);
            emit rowSelected();
        }
    }
    else{
        if(dockingstation->readLine().contains("not detected")){ //no warning needed if it is just not plugged in
        }
        else{
            //warning if plugged in, and something went wrong.
            QMessageBox::warning(this, "Error reading dock",
                                    "An error occurred when accessing the dock. Please try the following: \n"
                                    "1. Click refresh. The dock may still be initializing.\n"
                                    "2. If still problematic, restart your dock and re-mount your headstage.");
        }
    }
}

void DiskListWidget::diskDetectFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    QProcess *listDevices = qobject_cast<QProcess*>(sender());
    std::string output;
    while((output=listDevices->readLine().toStdString()).length()){
    #if defined(Q_OS_WIN)
        windows_parse_listDevices(output);
    #elif defined(Q_OS_MAC)
        macos_parse_listDevices(output);
    #elif defined(Q_OS_LINUX)
        linux_parse_listDevices(output);
    #else
    #error "OS not supported!"
    #endif
    }
    deviceslist->setCurrentIndex(model->index(model->rowCount()-1, 0));
    if(getSelectedDevicePath() != "Docking Station"){
        fetchDeviceSettings(getSelectedDevicePath(), LoggerRecording::SD_PC_DIRECT);
    }
    emit rowSelected();
}*/

#include "pcheckprocess.h"
PcheckProcess::PcheckProcess(QString device, QWidget *parent)
    : AbstractProcess("Packet Check", parent), device(device)
{
}

void PcheckProcess::start(){
    if(device == "Docking Station"){
        docking_pcheck();
    }
    else{
    #if defined(Q_OS_WIN)
        win_start_pcheck();
    #elif defined(Q_OS_MAC)
        mac_start_pcheck();
    #elif defined(Q_OS_LINUX)
        lin_start_pcheck();
    #else
    #error "OS not supported!"
    #endif
    }
}

void PcheckProcess::win_start_pcheck(){

    QRegularExpression rex("(.*)([0-9]{1,3})"); //Takes the last digits of device string (PhysicalDrive1) -> 1
    QRegularExpressionMatch m = rex.match(device);
    if (m.hasMatch() && m.lastCapturedIndex() > 1) {
        QStringList args;
        args << m.captured(2);

        process->start(".\\windows_sd_util\\pcheck.exe", args);
    }

}

void PcheckProcess::lin_start_pcheck(){
    process->start("./linux_sd_util/pcheck", {device});
}

void PcheckProcess::mac_start_pcheck()
{
    process->start("./macos_sd_util/pcheck", {device});
}

void PcheckProcess::docking_pcheck(){
    process->start(DOCKINGPATH, {"-p","-x",device});
}

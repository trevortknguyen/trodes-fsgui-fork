#include "mainwindow.h"
#include <QDomNode>
#include "quicksetup.h"
//#include "workspacecreatordialog.h"
//#include "workspaceeditordialog.h"
//#include "ntrodechannelmappingwindow.h"
#include "loggerconfigwidget.h"
#include "firmwareupdater.h"
QString DOCKINGPATH;
QString MCUUTILPATH;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent){
#if defined(Q_OS_WIN)
    //DOCKINGPATH = "./windows_sd_util/dockingstation.exe";
    DOCKINGPATH = "./dockconsoleutility.exe";
    MCUUTILPATH = "./mcu_utils/windows";
#elif defined(Q_OS_MAC)
    //DOCKINGPATH = QCoreApplication::applicationDirPath() + "/../../../macos_sd_util/dockingstation";
    DOCKINGPATH = QCoreApplication::applicationDirPath() + "/../../../dockconsoleutility";
    MCUUTILPATH = QCoreApplication::applicationDirPath() + "/../../../mcu_utils/macos";
#elif defined(Q_OS_LINUX)
    //DOCKINGPATH = "./linux_sd_util/dockingstation";
    DOCKINGPATH = "./dockconsoleutility";
    MCUUTILPATH = "./mcu_utils/linux";
#else
#error "OS not supported!"
#endif
    //resize(600,200);
    //setMaximumHeight(100);
    generateMenus();
    generateLayouts();
    createConnects();
    readSettings();
    checkForUpdate();
    numchannels = 0;
//    emit checkinputs();
    extractprocess = nullptr;
//    dockingsettings = new DockingSettings(LoggerRecording());
}

MainWindow::~MainWindow()
{

}

void MainWindow::closeEvent(QCloseEvent *event){
    saveSettings();
    emit killAllProcesses();
    QMainWindow::closeEvent(event);
}

void MainWindow::generateMenus(){
    //File menu
//    menuFile = new QMenu;
//    menuFile->setTitle(tr("&File"));
//    menuBar()->addAction(menuFile->menuAction());

//    actionFindSDCard = new QAction;
//    actionFindSDCard->setText("Find SD Card");
//    menuFile->addAction(actionFindSDCard);

//    actionFindMergeRec = new QAction;
//    actionFindMergeRec->setText("Find .rec");
//    menuFile->addAction(actionFindMergeRec);

//    actionFindTrodesConfig = new QAction;
//    actionFindTrodesConfig->setText("Find Trodes Config");
//    menuFile->addAction(actionFindTrodesConfig);

//    QAction *actionGenQuickStart = new QAction("Quickstart");
//    menuFile->addAction(actionGenQuickStart);
//    connect(actionGenQuickStart, &QAction::triggered, this, [this](){
//        LoggerRecording rec = deviceslist->getSelectedDeviceSettings();
//        DockingQuickstart *q = new DockingQuickstart(rec);
//        connect(q, &DockingQuickstart::okPressed, this, [this](QString tempRootName){
//            QString tempworkspace = tempRootName + ".trodesconf";
//            QString sdoutput = tempRootName + ".dat";
//            QString recoutput = tempRootName + ".rec";
//            setSDOutputPath(sdoutput);
//            runExtractSDCard();
//            connect(extractprocess, &ExtractProcess::processfinished, this, [this, tempworkspace, recoutput](int code){
//                setTrodesConfPath(tempworkspace);
//                setOutputPath(recoutput);
//                runSdToRec();
//            });
//        });
//    });

    //Run menu
//    menuRun = new QMenu;
//    menuRun->setTitle(tr("&Run"));
//    menuBar()->addAction(menuRun->menuAction());

//    actionRunExtract = new QAction;
//    actionRunExtract->setText("Run SD extract");
//    menuRun->addAction(actionRunExtract);

//    actionRunSdToRec = new QAction;
//    actionRunSdToRec->setText("Run SD to Rec");
//    menuRun->addAction(actionRunSdToRec);

//    actionRunMerge = new QAction;
//    actionRunMerge->setText("Run Merge to .rec");
//    menuRun->addAction(actionRunMerge);

//    actionRunAll = new QAction;
//    actionRunAll->setText("Run All");
//    menuRun->addAction(actionRunAll);

    //Help menu
    menuHelp = new QMenu;
    menuHelp->setTitle(tr("&Help"));
    menuBar()->addAction(menuHelp->menuAction());

    actionAbout = new QAction;
    actionAbout->setText("About");
    menuHelp->addAction(actionAbout);
}

void MainWindow::generateLayouts()
{
    //Central widget, a splitter
//    QSplitter *splitter = new QSplitter;
//    splitter->setOrientation(Qt::Vertical);
//    splitter->setChildrenCollapsible(false);
//    QWidget *d = new QWidget();
//    QVBoxLayout *splitter = new QVBoxLayout();
//    d->setLayout(splitter);
//    setCentralWidget(d);
//    splitter->setOrientation(Qt::Vertical);
//    setLayout(splitter);

//    setCentralWidget(splitter);

    //Top half layout
    TopHalf = new QSplitter;
    TopHalf->setOrientation(Qt::Horizontal);
    TopHalf->setHandleWidth(10);
    TopHalf->setChildrenCollapsible(false);
    setCentralWidget(TopHalf);

//    splitter->addWidget(TopHalf);
//    QWidget *du = new QWidget;
//    TopHalf = new QHBoxLayout;
//    splitter->addWidget(du, 7);
//    du->setLayout(TopHalf);


    //--SD Extraction section
    QGroupBox *SDExtractBox = new QGroupBox(" Logger information");
    SDExtractBox->setStyleSheet("font-size: 12px");
    TopHalf->addWidget(SDExtractBox);
    SDLayout = new QVBoxLayout;
    SDExtractBox->setLayout(SDLayout);
    SDLayout->setSpacing(2);
    //SDLayout->setMargin(0);


    deviceslist = new DiskListWidget;

    //deviceslist->setMinimumWidth(500);
    //deviceslist->setMaximumHeight(250);
    dockingsettings = new DockingSettings(deviceslist->getSelectedDeviceSettings());
    SDLayout->addWidget(deviceslist);

    //QFrame *line = new QFrame;
    //line->setFrameShadow(QFrame::Sunken);
    //line->setFrameShape(QFrame::HLine);
    //SDLayout->addWidget(line);


    //--Merge Data section
    QGroupBox *MergeBox = new QGroupBox(" Extract and create recording");
    MergeBox->setStyleSheet("font-size: 12px");
    QVBoxLayout *MergeLayout = new QVBoxLayout;
    MergeBox->setLayout(MergeLayout);
    MergeLayout->setSpacing(2);

    QTabWidget *tabs = new QTabWidget;
    tabs->addTab(MergeBox, "Extract");
//    TopHalf->addWidget(tabs);
    SDLayout->addWidget(tabs);

//    QGroupBox *advancedBox = new QGroupBox(" Advanced operations on your logger (Coming soon ...)");
//    advancedBox->setStyleSheet("font-size: 16px");
//    QVBoxLayout *AdvancedLayout = new QVBoxLayout;
//    advancedBox->setLayout(AdvancedLayout);
//    tabs->addTab(advancedBox, "Advanced");
//    QPushButton *writeConfigButton = new QPushButton("WriteConfig");
//    AdvancedLayout->addWidget(new QLabel("View and modify your device's existing configuration"), 0, Qt::AlignLeft | Qt::AlignTop);
//    AdvancedLayout->addWidget(writeConfigButton, 0, Qt::AlignLeft | Qt::AlignTop);
//    AdvancedLayout->addWidget(new QWidget(), 1);
//    connect(writeConfigButton, &QPushButton::pressed, this, [this](){
//        LoggerConfigWidget* w = new LoggerConfigWidget(deviceslist->getSelectedDeviceSettings(), deviceslist->getSelectedDevicePath());
//        if(w->exec() == QDialog::Accepted){
//            deviceslist->updateDevices();
//        }
//    });

    QGroupBox *firmwareBox = new QGroupBox(" (Coming soon ...) Check or update Docking Station firmware");
    firmwareBox->setStyleSheet("font-size: 12px");
    //tabs->addTab(firmwareBox, "Firmware"); Hide firmware tab until it is cleaned up.
    QVBoxLayout *firmwareboxlayout = new QVBoxLayout;
    firmwareboxlayout->setSpacing(2);
    firmwareupdater = new FirmwareUpdater(HeadstageSettings());
    firmwareboxlayout->addWidget(firmwareupdater);
    firmwareBox->setLayout(firmwareboxlayout);
    connect(firmwareupdater, &FirmwareUpdater::startFirmwareUpload, this, &MainWindow::runFirmwareUpdate);

//    QHBoxLayout *radiolayout = new QHBoxLayout;
    QGridLayout         *wsSelectionlayout = new QGridLayout;
    browseWorkspace = new QRadioButton("Browse and select a workspace");
    autogenWorkspace = new QRadioButton("Automatically generate workspace");
    //Hide option to automatically generate workspace until we figure out how that should work
    //wsSelectionlayout->addWidget(browseWorkspace, 0, 0, 1, 2);
    //wsSelectionlayout->addWidget(autogenWorkspace, 0, 2, 1, 2);
    browseWorkspace->setChecked(true);


    browseConfButton = new QPushButton("Browse...");
    browseConfButton->setMaximumWidth(80);
    wsSelectionlayout->addWidget(browseConfButton, 1, 0, 1, 1);
    connect(browseConfButton, &QPushButton::clicked, this, &MainWindow::browseConfPath);

    trodesConfEntry = new QLineEdit("relevant_config.trodesconf");
    trodesConfEntry->setReadOnly(true);
    wsSelectionlayout->addWidget(trodesConfEntry, 1, 1, 1, 3);

    connect(browseWorkspace, &QRadioButton::toggled, browseConfButton, &QPushButton::setEnabled);
    connect(browseWorkspace, &QRadioButton::toggled, trodesConfEntry, &QLineEdit::setEnabled);

    QVBoxLayout *finalwslayout = new QVBoxLayout;
    finalwslayout->setSpacing(2);
    //finalwslayout->setContentsMargins(5,5,5,5);
//    finalwslayout->addLayout(radiolayout);
    QWidget *dummy = new QWidget;
    dummy->setLayout(wsSelectionlayout);
    finalwslayout->addWidget(dummy);
    QGroupBox *wsGroupBox = new QGroupBox(" Final Trodes workspace");
    wsGroupBox->setLayout(finalwslayout);



    QFrame *line2 = new QFrame;
    line2->setFrameShadow(QFrame::Sunken);
    line2->setFrameShape(QFrame::HLine);
//    line2->setFixedHeight(40);

    QHBoxLayout *extrareclayout = new QHBoxLayout;
    extrarecGroupBox = new QGroupBox(" Merge logger data with environmental data");
    extrarecGroupBox->setLayout(extrareclayout);
    extrarecGroupBox->setCheckable(true);
    extrarecGroupBox->setChecked(true);
//    trodesrecLabel = new QCheckBox("Merge logger data with MCU/ECU data");
//    trodesrecLabel->setChecked(false);

    browseRecButton = new QPushButton("Browse...");
    browseRecButton->setMaximumWidth(80);
    extrareclayout->addWidget(browseRecButton);
    connect(browseRecButton, &QPushButton::clicked, this, &MainWindow::browseRecPath);
//    browseRecButton->setEnabled(false);

    trodesRecEntry = new QLineEdit("");
    trodesRecEntry->setReadOnly(true);
    extrareclayout->addWidget(trodesRecEntry);
//    trodesRecEntry->setEnabled(false);

    QFrame *line3 = new QFrame;
    line3->setFrameShadow(QFrame::Sunken);
    line3->setFrameShape(QFrame::HLine);
//    line3->setFixedHeight(40);

    chanmaplabel = new QRadioButton(  "Coming soon ...\nApply from template\n (.trodesconf/.csv)");
    manualmaplabel = new QRadioButton("Configure mapping \nmanually");
    autofilllabel = new QRadioButton( "Autofill mapping. Choose \n# chans per ntrode");
    QPushButton *browseChannelMapping = new QPushButton("Browse...");
    browseChannelMapping->setMaximumWidth(100);
    connect(browseChannelMapping, &QPushButton::pressed, this, &MainWindow::browseChannelMapPath);
    QPushButton *configChannelMapping = new QPushButton("Configure...");
    configChannelMapping->setMaximumWidth(100);
    chooseChansPerNtrode = new QComboBox();
    chooseChansPerNtrode->addItems({"1", "2", "4", "8"});
    chooseChansPerNtrode->setMaximumWidth(100);
    QGridLayout *channelmaplayout = new QGridLayout;
    //channelmaplayout->setMargin(1);
    channelmaplayout->addWidget(autofilllabel, 0, 0);
    channelmaplayout->addWidget(manualmaplabel, 0, 1);
    channelmaplayout->addWidget(chanmaplabel, 0, 2);
    channelmaplayout->addWidget(chooseChansPerNtrode, 1, 0);
    channelmaplayout->addWidget(configChannelMapping, 1, 1);
    channelmaplayout->addWidget(browseChannelMapping, 1, 2);

    connect(configChannelMapping, &QPushButton::pressed, this, &MainWindow::configureChannelsManually);
    connect(chanmaplabel, &QRadioButton::toggled,
    [this, browseChannelMapping, configChannelMapping](bool toggled){
        browseChannelMapping->setEnabled(true);
        configChannelMapping->setEnabled(false);
        chooseChansPerNtrode->setEnabled(false);
    });
    connect(manualmaplabel, &QRadioButton::toggled,
    [this, browseChannelMapping, configChannelMapping](bool toggled){
        browseChannelMapping->setEnabled(false);
        configChannelMapping->setEnabled(true);
        chooseChansPerNtrode->setEnabled(false);
    });
    connect(autofilllabel, &QRadioButton::toggled,
    [this, browseChannelMapping, configChannelMapping](bool toggled){
        browseChannelMapping->setEnabled(false);
        configChannelMapping->setEnabled(false);
        chooseChansPerNtrode->setEnabled(true);
    });
    autofilllabel->setChecked(true);
    chanmaplabel->setEnabled(false);

    //Automatic channel mapping disabled until we figure out how it should work
    /*QGroupBox *chanmapGroupBox = new QGroupBox(" Channel Mapping");
    chanmapGroupBox->setLayout(channelmaplayout);
    finalwslayout->addWidget(chanmapGroupBox);
    finalwslayout->setSpacing(2);
    finalwslayout->setMargin(0);
    connect(autogenWorkspace, &QRadioButton::toggled, chanmapGroupBox, &QGroupBox::setEnabled);
    chanmapGroupBox->setEnabled(false);*/

    QPushButton *saveasButton = new QPushButton("Save As...");
    finalFileLineEdit = new QLineEdit(QString("temp_%1.rec").arg(QDateTime::currentDateTime().toString("MMddyyyy_hhmm")));
    QHBoxLayout *finalfilelayout = new QHBoxLayout;
    finalfilelayout->addWidget(saveasButton, 0, Qt::AlignLeft);
    finalfilelayout->addWidget(finalFileLineEdit, 1);
    connect(saveasButton, &QPushButton::clicked, this, &MainWindow::browseFinalRecOutputPath);

    QGroupBox *finalfileGroupBox = new QGroupBox(" Extracted file name");
    finalfileGroupBox->setLayout(finalfilelayout);
    finalfileGroupBox->setStyleSheet("QGroupBox{"
//                                     "background-color: #30000000; "
                                     "font-weight: bold;"
                                     "}");

    CreateRecButton = new QPushButton("START");
    /*CreateRecButton->setStyleSheet("font-weight: bold;"
                                   "margin-bottom: 2px;"
                                   "margin-top: 2px;"
                                   "padding: 2px;");*/
    connect(CreateRecButton, &QPushButton::pressed, this, &MainWindow::setupEntireExtract);

    MergeLayout->addWidget(extrarecGroupBox);
    MergeLayout->addWidget(line2);
    MergeLayout->addWidget(wsGroupBox);
    MergeLayout->addWidget(line3);
    MergeLayout->addWidget(finalfileGroupBox);
    MergeLayout->addWidget(CreateRecButton, 0, Qt::AlignCenter);


//    QTabWidget *consolewidget = new QTabWidget;
    QVBoxLayout *consolelayout = new QVBoxLayout;
    consolelayout->setSpacing(2);
//    consolewidget->setLayout(consolelayout);
    processconsole = new QTextEdit;
    processconsole->setReadOnly(true);

    QWidget *progresswidget = new QWidget;
    QVBoxLayout *progresslayout = new QVBoxLayout;
    progresslayout->setSpacing(2);
    progressLabel = new QLabel("Extract Progress");
    progressBar = new QProgressBar;
    progressStatus = new QLabel("");
//    progressBar->setRange()
    progressBar->setTextVisible(true);
    progresslayout->addWidget(progressLabel, Qt::AlignTop);
    progresslayout->addWidget(progressBar, Qt::AlignTop);
    progresslayout->addWidget(progressStatus, Qt::AlignTop);
    progresswidget->setLayout(progresslayout);

//    consolewidget->addTab(progresswidget, "Progress");
//    consolewidget->addTab(processconsole, "Console");
    consolelayout->addWidget(new QLabel("Console"));
    consolelayout->addWidget(processconsole);
    consolelayout->addWidget(progresswidget);
    MergeLayout->addLayout(consolelayout);
//    splitter->addWidget(consolewidget);
//    splitter->setStretchFactor(0,1);
//    QWidget *b = new QWidget;
    QGroupBox *b = new QGroupBox;
    bb = new QVBoxLayout;
    bb->setSpacing(2);
    b->setLayout(bb);
    bb->addWidget(dockingsettings, 1, Qt::AlignTop);

    QGridLayout* buttonLayout = new QGridLayout();
    closeButton = new QPushButton("Close");
    connect(closeButton, &QPushButton::released,this,&MainWindow::close);
    buttonLayout->addWidget(closeButton,0,1);
    buttonLayout->setColumnStretch(0,1);
    bb->addLayout(buttonLayout);

//    bb->addWidget(consolewidget);
    //bb->addLayout(consolelayout);
    TopHalf->addWidget(b);
}

void MainWindow::setEnvironmentalRecordingPath(QString path)
{
    //qDebug() << "Got a path:" << path;
    if (!path.isEmpty()) {
        trodesRecEntry->setText(path);
        trodesRecFilePath = path;
        defaultRecDir = QFileInfo(trodesRecFilePath).dir().absolutePath();
        defaultMergeOutputDir = defaultRecDir;
        QString suggestedMergeFileName = defaultMergeOutputDir + "/" + QFileInfo(trodesRecFilePath).baseName() + "_merged.rec";
        setOutputPath(suggestedMergeFileName);
        extrarecGroupBox->setChecked(true);
        emit checkinputs();
    }
}

void MainWindow::createConnects(){
//    connect(browseSDButton, &QPushButton::pressed, this, &MainWindow::browseSDOutputPath);
//    connect(browseDatButton, &QPushButton::pressed, this, &MainWindow::browseDatPath);
//    connect(browseRecButton, &QPushButton::pressed, this, &MainWindow::browseRecPath);
//    connect(browseConfButton, &QPushButton::pressed, this, &MainWindow::browseConfPath);
//    connect(browseMergeButton, &QPushButton::pressed, this, &MainWindow::browseMergeOutputPath);

    connect(deviceslist, &DiskListWidget::cardEnableCalled, this, &MainWindow::runCardEnable);
    connect(deviceslist, &DiskListWidget::pcheckCalled, this, &MainWindow::runPcheck);
    connect(deviceslist, &DiskListWidget::writeconfigCalled, this, &MainWindow::runWriteConfig);
    connect(deviceslist, &DiskListWidget::readconfigCalled, this, &MainWindow::runReadConfig);

//    connect(SDOutputEntry, &QLineEdit::textEdited, this, &MainWindow::checkinputs);
//    connect(mergeOutputEntry, &QLineEdit::textEdited, this, &MainWindow::checkinputs);
//    connect(trodesrecLabel, &QCheckBox::stateChanged, this, &MainWindow::checkinputs);
//    connect(trodesrecLabel, &QCheckBox::clicked, trodesRecEntry, &QLineEdit::setEnabled);
//    connect(trodesrecLabel, &QCheckBox::clicked, browseRecButton, &QPushButton::setEnabled);

    connect(this, &MainWindow::checkinputs, this, &MainWindow::deviceSelected);
    connect(this, &MainWindow::checkinputs, this, &MainWindow::checkValidMergeInputs);
    connect(deviceslist, &DiskListWidget::rowSelected, this, &MainWindow::deviceSelected);
    connect(deviceslist, &DiskListWidget::listUpdated, this, &MainWindow::deviceSelected);
    deviceslist->updateDevices();
//    connect(browseSDButton, &QPushButton::pressed, this, &MainWindow::checkValidSDInputs);
//    connect(datFileBrowser, &FileBrowser::pathSelected, this, &MainWindow::checkValidMergeInputs);
//    connect(browseDatButton, &QPushButton::pressed, this, &MainWindow::checkValidMergeInputs);
//    connect(browseRecButton, &QPushButton::pressed, this, &MainWindow::checkValidMergeInputs);
//    connect(browseConfButton, &QPushButton::pressed, this, &MainWindow::checkValidMergeInputs);
//    connect(browseMergeButton, &QPushButton::pressed, this, &MainWindow::checkValidMergeInputs);

//    connect(actionRunExtract, &QAction::triggered, this, &MainWindow::runExtractSDCard);
//    connect(actionRunSdToRec, &QAction::triggered, this, &MainWindow::runSdToRec);
//    connect(actionRunMerge, &QAction::triggered, this, &MainWindow::runMergeFiles);
    connect(actionAbout, &QAction::triggered, this, &MainWindow::openAboutDialog);

//    connect(runSDExtractButton, &QPushButton::pressed, actionRunExtract, &QAction::trigger);
//    connect(runSdToRecButton, &QPushButton::pressed, actionRunSdToRec, &QAction::trigger);
//    connect(runMergeButton, &QPushButton::pressed, actionRunMerge, &QAction::trigger);

//    connect(this, &MainWindow::SDDatFileProcessed, datFileBrowser, &FileBrowser::navigateToPath);
//    connect(this, &MainWindow::SDDatFileProcessed, datfileEntry, &QLineEdit::setText);
}

void MainWindow::saveSettings(){

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("DataLoggerGUI"));
    settings.beginGroup(QLatin1String("MainWindow"));
    settings.setValue(QLatin1String("geometry"), saveGeometry());
    settings.setValue(QLatin1String("windowState"), saveState());
    settings.endGroup();

    settings.beginGroup(QLatin1String("Paths"));
//    settings.setValue("defaultSDOutputDir", defaultSDOutputDir);
//    settings.setValue("defaultDatDir", defaultDatDir);
    settings.setValue(QLatin1String("defaultTrodesConfigDir"), defaultTrodesConfigDir);

    settings.setValue(QLatin1String("defaultMergeOutputDir"), defaultMergeOutputDir);


    QFileInfo fI(trodesRecFilePath);
    QDir defaultEnvironmentDir;
    //qDebug() << defaultRecDir;
    if (fI.isFile()) {

        defaultEnvironmentDir = fI.dir();
        defaultEnvironmentDir.cdUp();

        //defaultEnvironmentDir.setPath(fI.dir().absolutePath());
    } else {

        defaultEnvironmentDir = defaultRecDir;
    }
    //defaultEnvironmentDir.cdUp();
    //settings.setValue("defaultRecDir", defaultRecDir);
    settings.setValue(QLatin1String("defaultRecDir"), defaultEnvironmentDir.absolutePath());
    settings.setValue(QLatin1String("defaultCfgFile"), defaultCfgFile);
    settings.endGroup();

    settings.beginGroup(QLatin1String("Subwindowsizes"));
    settings.setValue(QLatin1String("devicesColSize"), deviceslist->saveColumnGeometry());
//    settings.setValue("datBrowserColSize", datFileBrowser->getColumnWidth());
//    settings.setValue("topsplitterpos", TopHalf->saveState());
    settings.endGroup();
    settings.sync();



}

void MainWindow::readSettings(){

    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("DataLoggerGUI"));
    settings.beginGroup(QLatin1String("MainWindow"));
    restoreGeometry(settings.value(QLatin1String("geometry")).toByteArray());
    restoreState(settings.value(QLatin1String("windowState")).toByteArray());
    settings.endGroup();

    settings.beginGroup(QLatin1String("Paths"));
//    defaultSDOutputDir = settings.value("defaultSDOutputDir").toString();
//    defaultDatDir = settings.value("defaultDatDir").toString();
    defaultTrodesConfigDir = settings.value(QLatin1String("defaultTrodesConfigDir")).toString();
    defaultMergeOutputDir = settings.value(QLatin1String("defaultMergeOutputDir")).toString();
    defaultRecDir = settings.value(QLatin1String("defaultRecDir")).toString();
    defaultCfgFile = settings.value(QLatin1String("defaultCfgFile")).toString();

    trodesRecEntry->setText(defaultRecDir + "/" + trodesRecEntry->text());
//    if(defaultSDOutputDir.isEmpty()){
//        defaultSDOutputDir = QDir::homePath() + "/";
//    }
//    else{
//        defaultSDOutputDir = QFileInfo(defaultSDOutputDir).dir().absolutePath()+"/";
//    }
//    sdOutputFilePath = defaultSDOutputDir;
//    SDOutputEntry->setText(sdOutputFilePath);

    if(defaultMergeOutputDir.isEmpty()){
        defaultMergeOutputDir = QDir::homePath();
    }
    mergeOutputFilePath = defaultMergeOutputDir + "/" + finalFileLineEdit->text();
    finalFileLineEdit->setText(mergeOutputFilePath);

    if(defaultTrodesConfigDir.isEmpty()){
        defaultTrodesConfigDir = "relevant_config.trodesconf";
    }
    trodesConfInputPath = defaultTrodesConfigDir;
    trodesConfEntry->setText(trodesConfInputPath);

//    if(defaultDatDir.isEmpty()){
//        defaultDatDir = QDir::homePath() + QDir::separator() + "untitled.dat";
//    }
//    datinputFilePath = defaultDatDir;
//    datfileEntry->setText(datinputFilePath);

    settings.endGroup();

    settings.beginGroup(QLatin1String("Subwindowsizes"));
    deviceslist->loadColumnGeometry(settings.value(QLatin1String("devicesColSize")).toByteArray());
//    int w;
//    w = settings.value("datBrowserColSize").toInt();
//    if(w){
//        datFileBrowser->setColumnWidth(w);
//    }
//    TopHalf->restoreState(settings.value("topsplitterpos").toByteArray());
    settings.endGroup();
}

void MainWindow::openAboutDialog(){
    QMessageBox::about(this, "About DataLoggerGUI", QString("DataLoggerGUI version: ")+DLGUI_VERSION);
}

//void MainWindow::browseSDOutputPath(){
//    QString selectedpath = QFileDialog::getSaveFileName(this, "Create file to save SD data",
//                                                        defaultSDOutputDir,
//                                                        "Data files (*.dat)");
//    setSDOutputPath(selectedpath);
//    emit checkinputs();
//}

//void MainWindow::setSDOutputPath(QString selectedpath){
////    if(!selectedpath.isNull()){
////        SDOutputEntry->setText(selectedpath);
////        sdOutputFilePath = selectedpath;
////        defaultSDOutputDir = QFileInfo(sdOutputFilePath).absoluteFilePath();
////        emit SDOutputFileSelected(sdOutputFilePath); //Set directories of the two filebrowsers to this filepath, if found
////    }
//}

//void MainWindow::browseDatPath(){
//    QString selectedpath = QFileDialog::getOpenFileName(this, "Select sd .dat file",
//                                                        defaultSDOutputDir,
//                                                        "SD dat files (*.dat)");
//    if(!selectedpath.isNull()){
////        datfileEntry->setText(selectedpath);
//        datinputFilePath = selectedpath;
//        defaultDatDir = QFileInfo(datinputFilePath).absoluteFilePath();
//        emit checkinputs();
//    }
//}

void MainWindow::browseRecPath(){

#if defined (__linux__)
    /*QString selectedpath = QFileDialog::getOpenFileName(this, "Select Trodes .rec to merge with",
                                                        defaultRecDir,
                                                        "Recording files (*.rec)",nullptr, QFileDialog::DontUseNativeDialog);*/

    QString selectedpath = QFileDialog::getOpenFileName(this, "Select Trodes .rec to merge with",
                                                        defaultRecDir,
                                                        "Recording files (*.rec)");

#else
    QString selectedpath = QFileDialog::getOpenFileName(this, "Select Trodes .rec to merge with",
                                                        defaultRecDir,
                                                        "Recording files (*.rec)");
#endif


    if(!selectedpath.isNull()){
        trodesRecEntry->setText(selectedpath);
        trodesRecFilePath = selectedpath;
        defaultRecDir = QFileInfo(trodesRecFilePath).dir().absolutePath();
        defaultMergeOutputDir = defaultRecDir;
        QString suggestedMergeFileName = defaultMergeOutputDir + "/" + QFileInfo(trodesRecFilePath).baseName() + "_merged.rec";
        setOutputPath(suggestedMergeFileName);

        emit checkinputs();
    }
}

void MainWindow::browseConfPath(){

#if defined (__linux__)
    /*QString selectedpath = QFileDialog::getOpenFileName(this, "Select Trodes config file for merge",
                                                        defaultTrodesConfigDir,
                                                        "Trodes config files (*.trodesconf *.xml)",nullptr, QFileDialog::DontUseNativeDialog);*/

    QString selectedpath = QFileDialog::getOpenFileName(this, "Select Trodes config file for merge",
                                                        defaultTrodesConfigDir,
                                                        "Trodes config files (*.trodesconf *.xml)");

#else
    QString selectedpath = QFileDialog::getOpenFileName(this, "Select Trodes config file for merge",
                                                        defaultTrodesConfigDir,
                                                        "Trodes config files (*.trodesconf *.xml)");
#endif


    setTrodesConfPath(selectedpath);
    if(!selectedpath.isNull()){
        emit checkinputs();
    }
}

void MainWindow::setTrodesConfPath(QString selectedpath){
    if(!selectedpath.isNull()){
        numchannels = getNumChannelsFromFile(selectedpath);
        if(numchannels<=0){
            qDebug() << "Num Channels attribute error!";
            QMessageBox::information(this,"Incompatible workspace","The merge workspace must have more than 0 channels configured. It should match the number of channels recorded on the logger.");
            return;
        }

        trodesConfEntry->setText(selectedpath);
        trodesConfInputPath = selectedpath;
        defaultTrodesConfigDir = QFileInfo(trodesConfInputPath).absoluteFilePath();
    }
}

int MainWindow::getNumChannelsFromFile(const QString &filename){
    QDomDocument doc("trodesconf");
    QFile f(filename);
    if(!f.open(QIODevice::ReadOnly)){
        qDebug() << "Could not open " << filename;
        return -1;
    }
    if(!doc.setContent(&f)){
        qDebug() << "Invalid .trodesconf!";
        return -2;
    }
    QDomNodeList hnodes = doc.elementsByTagName("HardwareConfiguration");
    if(hnodes.isEmpty()){
        qDebug() << "No HardwareConfiguration tag!";
        return -3;
    }
    QDomNode attrnode = hnodes.at(0).attributes().namedItem("numChannels");
    if(attrnode.isNull()){
        qDebug() << "No attribute numChannels!";
        return -4;
    }
    return attrnode.nodeValue().toInt();
}

void MainWindow::browseFinalRecOutputPath(){


#if defined (__linux__)
    /*QString selectedpath = QFileDialog::getSaveFileName(this, "Output file to save data",
                                                        defaultMergeOutputDir,
                                                        "Trodes recording (*.rec)",nullptr, QFileDialog::DontUseNativeDialog);*/

    QString selectedpath = QFileDialog::getSaveFileName(this, "Output file to save data",
                                                        defaultMergeOutputDir,
                                                        "Trodes recording (*.rec)");

#else
    QString selectedpath = QFileDialog::getSaveFileName(this, "Output file to save data",
                                                        defaultMergeOutputDir,
                                                        "Trodes recording (*.rec)");
#endif

    if (selectedpath.isEmpty()) {
        //Action canceled
        return;
    }
    if(!selectedpath.endsWith(".rec")){
        selectedpath = selectedpath + ".rec";
    }

    setOutputPath(selectedpath);
    if(!selectedpath.isNull()){
        emit checkinputs();
    }
}

void MainWindow::setOutputPath(QString selectedpath){
    if(!selectedpath.isNull()){
        finalFileLineEdit->setText(selectedpath);
        mergeOutputFilePath = selectedpath;
        defaultMergeOutputDir = QFileInfo(mergeOutputFilePath).absolutePath();
    }
}

void MainWindow::browseChannelMapPath(){
#if defined (__linux__)
    /*QString selectedpath = QFileDialog::getOpenFileName(this, "Select Trodes config file or channel mapping file (.trodesconf, .csv)",
                                                        defaultTrodesConfigDir,
                                                        "Channel mapping files (*.trodesconf *.csv)",nullptr, QFileDialog::DontUseNativeDialog);*/

    QString selectedpath = QFileDialog::getOpenFileName(this, "Select Trodes config file or channel mapping file (.trodesconf, .csv)",
                                                        defaultTrodesConfigDir,
                                                        "Channel mapping files (*.trodesconf *.csv)");
#else
    QString selectedpath = QFileDialog::getOpenFileName(this, "Select Trodes config file or channel mapping file (.trodesconf, .csv)",
                                                        defaultTrodesConfigDir,
                                                        "Channel mapping files (*.trodesconf *.csv)");
#endif


//    setTrodesConfPath(selectedpath);
//    if(!selectedpath.isNull())
    //        emit checkinputs();
}

void MainWindow::configureChannelsManually(){
    /*NTrodeChannelMappingWindow * win = new NTrodeChannelMappingWindow(deviceslist->getSelectedDeviceSettings().settings.numberOfChannels, this);
    if(win->exec()){
        latestManualMappings = win->getChannelMappings();
    }
    else{
        latestManualMappings.clear();
    }*/
}

void MainWindow::deviceSelected(){
//    if(deviceslist->getSelectedDevicePath().isEmpty()){
//        return;
//    }
//    dockingsettings->setLoggerRec(deviceslist->getSelectedDeviceSettings());
    bool listEmpty = false;
    if (deviceslist->getSelectedDevicePath().isEmpty()) {
        listEmpty = true;
        deviceslist->allowEnable(false);
        deviceslist->allowDockSettingsChange(false);
        deviceslist->allowLoggerSettingsChange(false);
    }

    LoggerRecording device = deviceslist->getSelectedDeviceSettings();

    if (!listEmpty) {


        if (device.setup == LoggerRecording::NONE) {
            deviceslist->allowEnable(false);
            deviceslist->allowLoggerSettingsChange(false);
        } else {
            deviceslist->allowEnable(true);
            deviceslist->allowLoggerSettingsChange(true);
        }

        if (device.setup == LoggerRecording::HS_DOCK && device.enabledForRecording) {
            deviceslist->allowLoggerSettingsChange(true);
        } else {
            deviceslist->allowLoggerSettingsChange(false);
        }

        if (deviceslist->getSelectedDevicePath() == "DockingStation A" || deviceslist->getSelectedDevicePath() == "Spikegadgets MCU A") {
            deviceslist->allowDockSettingsChange(true);
        } else {
            deviceslist->allowDockSettingsChange(false);
        }

    }

    DockingSettings *newsettings = new DockingSettings(device);
    dockingsettings->setVisible(false);
    dockingsettings->deleteLater();
    bb->replaceWidget(dockingsettings, newsettings);
    dockingsettings = newsettings;

    firmwareupdater->deviceSelected(device);
//    if(!deviceslist->getSelectedDevicePath().isEmpty() &&
//            QFileInfo(SDOutputEntry->text()).dir().exists() &&
//            !QDir(SDOutputEntry->text()).exists())
//    {
//        runSDExtractButton->setEnabled(true);
//    }
//    else{
//        runSDExtractButton->setEnabled(false);
//    }
}

void MainWindow::runPcheck(){
    qDebug() << "Running pcheck" << deviceslist->getSelectedDevicePath();
    PcheckProcess *p = new PcheckProcess(deviceslist->getSelectedDevicePath(), this);
    connect(this, &MainWindow::destroyed, p, &PcheckProcess::deleteLater);
    connect(p, &PcheckProcess::processstarted, this, &MainWindow::pcheckStarted);
    connect(p, &PcheckProcess::processfinished, this, &MainWindow::pcheckFinished);
    p->start();
}

void MainWindow::pcheckStarted(){
    deviceslist->editStatus(deviceslist->getSelectedDevicePath(), "Starting PCheck");
}

void MainWindow::pcheckFinished(int code){
    if(code == 0){
        qDebug() << "Pcheck finished successfully";
        deviceslist->editStatus(deviceslist->getSelectedDevicePath(), "Pcheck");
    }
    emit checkinputs();
}

void MainWindow::runCardEnable(){
    qDebug() << "Running cardEnable" << deviceslist->getSelectedDevicePath();
    cardenableDevice = deviceslist->getSelectedDevicePath();

    QMessageBox *waitbox = new QMessageBox();
    waitbox->setWindowTitle("Please wait");
    waitbox->setText("Enabling for recording and erasing existing data. Please wait ... ");
    waitbox->setIcon(QMessageBox::Information);
    waitbox->setStandardButtons(QMessageBox::NoButton);
    //connect(process, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished)
    //        , waitbox, &QMessageBox::accept);
    waitbox->show();
    //waitbox->exec();
    CommandEngine dockCommandEngine;


    QByteArray devArray = cardenableDevice.toLocal8Bit();
    dockCommandEngine.setCurrentDeviceName(devArray.data());
    int ret = dockCommandEngine.dockCardEnable();
    //waitbox->accept();
    waitbox->close();
    delete waitbox;


    if(ret == -2){
        printf("Error running card-enable: No device mounted on the docking station!\n");
        int msgret = QMessageBox::information(this, "Card Enable","Error running card-enable: No device mounted on the docking station!");


    }
    if(ret){
        printf("Error running card-enable: Communication error with USB. %d \n", ret);
        int msgret = QMessageBox::information(this, "Card Enable","Error running card-enable: Communication error with USB.");

    }
    else{
        printf("Card-enable successful. You may now use this device for recordings.\n");
        int msgret = QMessageBox::information(this, "Card Enable","Card-enable successful. You may now use this device for recordings.");
    }

    deviceslist->updateDevices();



   /*CardEnableProcess *p = new CardEnableProcess(cardenableDevice, this);
    connect(this, &MainWindow::destroyed, p, &CardEnableProcess::deleteLater);
    connect(p, &CardEnableProcess::processstarted, this, &MainWindow::cardenableStarted);
    connect(p, &CardEnableProcess::processfinished, this, &MainWindow::cardenableFinished);
    p->start();*/
}

void MainWindow::cardenableStarted(){
    deviceslist->editStatus(cardenableDevice, "Starting CardEnable");
}

void MainWindow::cardenableFinished(int code){


    //if(code == 0){
        //qDebug() << "Card enable finished successfully";
        //deviceslist->editStatus(cardenableDevice, "Card enabled");
        //QMessageBox::information(this, "CardEnable successful", "CardEnable successful. Your device is now enabled for recording.");
    //}
    deviceslist->updateDevices();
    emit checkinputs();
}

void MainWindow::runReadConfig(){
    qDebug() << "Running readConfig" << deviceslist->getSelectedDevicePath();
    ReadConfigProcess *p = new ReadConfigProcess(deviceslist->getSelectedDevicePath(), this);
    connect(this, &MainWindow::destroyed, p, &ReadConfigProcess::deleteLater);
    connect(p, &ReadConfigProcess::processstarted, this, &MainWindow::readconfigStarted);
    connect(p, &ReadConfigProcess::processfinished, this, &MainWindow::readconfigFinished);
    p->start();
}

void MainWindow::readconfigStarted(){
    deviceslist->editStatus(deviceslist->getSelectedDevicePath(), "Starting ReadConfig");
}

void MainWindow::readconfigFinished(int code){
    if(code == 0){
        qDebug() << "Read Config finished successfully";
        deviceslist->editStatus(deviceslist->getSelectedDevicePath(), "");
    }
}


void MainWindow::runWriteConfig(){

#if defined (__linux__)
    //QString cfgfile = QFileDialog::getOpenFileName(this, "Select .cfg file", defaultCfgFile, "SD Config *.cfg(*.cfg)",nullptr, QFileDialog::DontUseNativeDialog);
    QString cfgfile = QFileDialog::getOpenFileName(this, "Select .cfg file", defaultCfgFile, "SD Config *.cfg(*.cfg)");
#else
    QString cfgfile = QFileDialog::getOpenFileName(this, "Select .cfg file", defaultCfgFile, "SD Config *.cfg(*.cfg)");
#endif

    if(!cfgfile.isNull()){
        defaultCfgFile = cfgfile;
        qDebug() << "Running writeConfig" << deviceslist->getSelectedDevicePath() << cfgfile;
        WriteConfigProcess *p = new WriteConfigProcess(deviceslist->getSelectedDevicePath(), cfgfile, this);
        connect(this, &MainWindow::destroyed, p, &WriteConfigProcess::deleteLater);
        connect(p, &WriteConfigProcess::processfinished, this, &MainWindow::writeconfigFinished);
        connect(p, &WriteConfigProcess::processstarted, this, &MainWindow::writeconfigStarted);
        p->start();
    }
}

void MainWindow::writeconfigStarted(){
    deviceslist->editStatus(deviceslist->getSelectedDevicePath(), "Starting writeConfig");
}

void MainWindow::writeconfigFinished(int code){
    if(code == 0){
        qDebug() << "Write config finished successfully";
        deviceslist->editStatus(deviceslist->getSelectedDevicePath(), "Config written");
    }
    emit checkinputs();
}

//void MainWindow::runExtractSDCard(){
////    if(!SDOutputEntry->text().endsWith(".dat")){
////        SDOutputEntry->setText(SDOutputEntry->text().append(".dat"));
////    }
////    qDebug() << "Extracting" << deviceslist->getSelectedDevicePath() << SDOutputEntry->text();

////    ExtractProcess *p = new ExtractProcess(deviceslist->getSelectedDevicePath(), SDOutputEntry->text(), this);
////    connect(this, &MainWindow::destroyed, p, &ExtractProcess::deleteLater);
////    connect(p, &ExtractProcess::processstarted, this, &MainWindow::extractStarted);
////    connect(p, &ExtractProcess::processfinished, this, &MainWindow::extractFinished);

////    p->start();
////    extractprocess = p;
//}

//void MainWindow::runSdToRec(){
////    if(!mergeOutputEntry->text().endsWith(".rec")){
////        mergeOutputEntry->setText(mergeOutputEntry->text().append(".rec"));
////    }
////    qDebug() << "SD to Rec (no merge)" <<
//////                datFileBrowser->getSelectedPath();
////                datfileEntry->text();
////    SdtorecProcess *p = new SdtorecProcess(
//////                datFileBrowser->getSelectedPath(),
////                datfileEntry->text(),
////                trodesConfEntry->text(), mergeOutputEntry->text(), numchannels);

////    connect(this, &MainWindow::destroyed, p, &SdtorecProcess::deleteLater);
////    connect(p, &SdtorecProcess::processstarted, this, &MainWindow::sdtorecStarted);
////    connect(p, &SdtorecProcess::processfinished, this, &MainWindow::sdtorecFinished);

////    p->start();
//}

void MainWindow::checkValidMergeInputs(){
//    if(
//        QFileInfo(datfileEntry->text()).isFile() && datfileEntry->text().endsWith(".dat") &&
//        QFileInfo(trodesConfEntry->text()).isFile() &&
//        (numchannels=getNumChannelsFromFile(trodesConfEntry->text())) > 0 &&
//        !mergeOutputEntry->text().isEmpty() &&
//        QFileInfo(mergeOutputEntry->text()).dir().exists() &&
//        !QDir(mergeOutputEntry->text()).exists())
//    {
//        runSdToRecButton->setEnabled(true);
//        if(trodesrecLabel->isChecked() && QFileInfo(trodesRecEntry->text()).isFile()){
//            runMergeButton->setEnabled(true);
//        }
//        else{
//            runMergeButton->setEnabled(false);
//        }
//    }
//    else{
//        runSdToRecButton->setEnabled(false);
//        runMergeButton->setEnabled(false);
//    }
}
//void MainWindow::runMergeFiles(){
//    if(!mergeOutputEntry->text().endsWith(".rec")){
//        mergeOutputEntry->setText(mergeOutputEntry->text().append(".rec"));
//    }
//    qDebug() << "Merging SD and .rec." <<
////                datFileBrowser->getSelectedPath() <<
//                datfileEntry->text() <<
//                trodesRecEntry->text();
//    MergeProcess *p = new MergeProcess(
////                datFileBrowser->getSelectedPath(),
//                datfileEntry->text(),
//                trodesRecEntry->text(),
//                trodesConfEntry->text(), mergeOutputEntry->text(), numchannels);
//    connect(this, &MainWindow::destroyed, p, &MergeProcess::deleteLater);
//    connect(p, &MergeProcess::processstarted, this, &MainWindow::mergeStarted);
//    connect(p, &MergeProcess::processfinished, this, &MainWindow::mergeFinished);

    //    p->start();
//}

void MainWindow::runFirmwareUpdate(QString device, QString file){
    if(deviceslist->getSelectedDevicePath() != "Docking Station"){
        QMessageBox::warning(this, "No Docking Station", "Can only program firmware on or through the docking station. Please select the docking station");
        return;
    }
    AbstractProcess *p;
    if(device.contains("Dock", Qt::CaseInsensitive)){
        p = new DockProgrammingProcess(file);
    }
    else if(device.contains("Headstage", Qt::CaseInsensitive)){
        p = new JTAGProcess(file);
    }
    else{
        return;
    }
    connect(this, &MainWindow::destroyed, p, &JTAGProcess::deleteLater);
    connect(p, &AbstractProcess::newOutputLine, this, &MainWindow::newProcessOutputLine);
    connect(p, &AbstractProcess::processfinished, this, [this, device](int code){
        if(code){
            QMessageBox::warning(this, "Firmware programming error", QString("There was an error (error code %1) when programming your device. "
                                                                     "Please consult the output on the right for more information.").arg(code));
        }
        else{
            QMessageBox::information(this, "Firmware programming finished", "Finished programming firmware for " + device);
        }
        firmwareupdater->firmwareUploadFinished();
    });
    p->start();
}

void MainWindow::extractStarted(){
//    sdParamsText->setText(      "Parameters:\n"
//                                " - Device:         " + deviceslist->getSelectedDevicePath() + "\n"
//                                " - Output file:    " + sdOutputFilePath+ "\n") ;
//    sdStatusLabel->setText("Status: Started");
//    sdStatusLabel->setStyleSheet("color: black");
//    runSDExtractButton->setEnabled(false);
//    deviceslist->editStatus(deviceslist->getSelectedDevicePath(), "Extracting...");
}

void MainWindow::extractFinished(int code){
//    if(code == 0){
//        qDebug() << "Extract finished successfully";
//        sdStatusLabel->setText("Status: Completed successfully");
//        sdStatusLabel->setStyleSheet("color: green");
//        emit SDDatFileProcessed(sdOutputFilePath); //Send new file over to step two to be auto-selected
//        deviceslist->editStatus(deviceslist->getSelectedDevicePath(), "Data extracted");
//    }
//    else{
//        sdStatusLabel->setText("Status: Error code " + QString::number(code));
//        sdStatusLabel->setStyleSheet("color: red");
//    }
//    runSDExtractButton->setEnabled(true);
//    emit checkinputs();
}

void MainWindow::sdtorecStarted(){
    progressLabel->setText("Conversion Progress");
    progressBar->setValue(0);
//    mergeParamsText->setText(   "Parameters:\n"
////                                " - .dat file:      " + datFileBrowser->getSelectedPath() + "\n"
//                                " - .dat file:      " + datfileEntry->text() + "\n"
//                                " - .rec file:      " + "None required" + "\n"
//                                " - .trodesconf:    " + trodesConfEntry->text() + "\n"
//                                " - # Channels:     " + QString::number(numchannels) + "\n"
//                                " - Output file:    " + mergeOutputEntry->text());
//    mergeStatusLabel->setText("Status: Started");
//    mergeStatusLabel->setStyleSheet("color: black");
//    runSdToRecButton->setEnabled(false);
}

void MainWindow::sdtorecFinished(int code){
//    if(code == 0){
//        qDebug() << "SD to Rec finished successfully";
//        mergeStatusLabel->setText("Status: Completed successfully");
//        mergeStatusLabel->setStyleSheet("color: green");
//    }
//    else{
//        mergeStatusLabel->setText("Status: Error code " + QString::number(code));
//        mergeStatusLabel->setStyleSheet("color: red");
//    }
//    runSdToRecButton->setEnabled(true);
//    emit checkinputs();
}

void MainWindow::mergeStarted(){
    progressLabel->setText("Merge Progress");
    progressBar->setValue(0);
//    mergeParamsText->setText(   "Parameters:\n"
////                                " - .dat file:      " + datFileBrowser->getSelectedPath() + "\n"
//                                " - .dat file:      " + datfileEntry->text() + "\n"
//                                " - .rec file:      " + trodesRecEntry->text() + "\n"
//                                " - .trodesconf:    " + trodesConfEntry->text() + "\n"
//                                " - # Channels:     " + QString::number(numchannels) + "\n"
//                                " - Output file:    " + mergeOutputEntry->text());
//    mergeStatusLabel->setText("Status: Started");
//    mergeStatusLabel->setStyleSheet("color: black");
//    runMergeButton->setEnabled(false);
}

void MainWindow::mergeFinished(int code){
//    if(code == 0){
//        qDebug() << "Merge finished successfully";
//        mergeStatusLabel->setText("Status: Completed successfully");
//        mergeStatusLabel->setStyleSheet("color: green");
//    }
//    else{
//        mergeStatusLabel->setText("Status: Error code " + QString::number(code));
//        mergeStatusLabel->setStyleSheet("color: red");
//    }
//    runMergeButton->setEnabled(true);
    //    emit checkinputs();
}

void MainWindow::newProcessOutputLine(const QString &line)
{
    processconsole->append(line);
    if(line.endsWith("%")){
        QString l = line;
        l.chop(1);
        progressBar->setValue(l.toInt());
    }

}

void MainWindow::checkForUpdate(){
    /*


    //Check for latest version. If no internet access or network reply, dont continue.
    //https://forum.qt.io/topic/50200/solved-qnetworkaccessmanager-crash-related-to-ssl/7
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("DataLoggerGUI"));

    settings.beginGroup(QLatin1String("MainWindow"));


    //Set update check success to be false first, so that if trodes crashes then it knows next time to skip
    settings.setValue(QLatin1String("updatecheck"), 0);

    //Check for update
    QLoggingCategory::setFilterRules("qt.network.ssl.warning=false");
    QNetworkAccessManager manager;
    if(manager.networkAccessible()==1){
        QNetworkRequest request = QNetworkRequest(QUrl("http://www.spikegadgets.com/software/latest_trodes_version.xml"));
        request.setRawHeader( "User-Agent" , "Mozilla Firefox" );
        QNetworkReply *response = manager.get(request);
        QEventLoop event;
        QTimer timeout;
        timeout.start(1000);
        connect(response,SIGNAL(finished()),&event,SLOT(quit()));
        connect(&timeout, SIGNAL(timeout()), &event, SLOT(quit()));
        event.exec();
        if(manager.networkAccessible()==1 && response->error() == QNetworkReply::NoError){
            QString str = QString(response->readAll());
            QDomDocument xml;
            xml.setContent(str);
            QString version = xml.elementsByTagName("DataLoggerGUIVersionInfo").at(0).toElement().attribute("version");
            QString versioncpy(version);
            //If version is empty, something went wrong with getting the info. No warning will be made to the user
            bool isLatestVersion;
            if(version.replace(".", "").toInt() > DLGUI_INTVERSION && !version.isEmpty()){
                isLatestVersion = false;
            }
            else{
                isLatestVersion = true;
            }
            if(!isLatestVersion && !version.isEmpty()){
                QMessageBox::warning(this, "Newer version available", "There is a newer version of the DataLoggerGUI/SD utils online, " +
                                     versioncpy + ". Your current version is "+DLGUI_VERSION);
            }
        }
        delete response;
    }
    //Set value back to 1
    settings.setValue(QLatin1String("updatecheck"), 1);
    settings.endGroup();*/
}

void MainWindow::setupEntireExtract(){
    deviceslist->updateTimer.stop();
    bool mergerec = extrarecGroupBox->isChecked();
    bool autogenws = autogenWorkspace->isChecked();
    bool autofillmap = autofilllabel->isChecked();
    bool manualmap = manualmaplabel->isChecked();
    bool sysclockused = false;
    int numchannels = 0;
    QString finalrec = finalFileLineEdit->text();
    bool mergeecu = false;
    QString mergefile;
    int packetsize = 0;
    if(mergerec){
        mergefile = trodesRecEntry->text();
        //verify rec file, check sysclock
        TrodesConfiguration conf;
        conf.readTrodesConfig(mergefile);
        for(auto const &device : conf.hardwareConf.devices){
            if(device.name == "ECU"){
                mergeecu = true;
            }
            if(device.name == "SysClock"){
                sysclockused = true;
            }
        }
    }

    QString finalworkspace;
    LoggerRecording rec = deviceslist->getSelectedDeviceSettings();
    if(autogenws){
        /*if(deviceslist->specificDatFileSelected()){
            QMessageBox::warning(this, "Existing dat selected", "You cannot automatically generate a workspace from an existing .dat file, as there is no headstage/logger configuration available. Please browse and select an already existing .trodesconf");
            deviceslist->updateTimer.start(1000);
            return;
        }
        numchannels = rec.settings.numberOfChannels;
        WorkspaceEditorDialog *wsdialog = new WorkspaceEditorDialog();
        QString tempws = finalrec + ".trodesconf";
        int chanpernt = 4;
        packetsize = rec.settings.packetSize;
        if(autofillmap){
            chanpernt = chooseChansPerNtrode->currentText().toInt();
        }
        if(mergeecu){
            packetsize += 32;
        }
        wsdialog->fillInWorkspace(rec.settings, mergerec, false, chanpernt, packetsize, sysclockused);

        if(manualmap){
            if(latestManualMappings.length() != rec.settings.numberOfChannels){
                QMessageBox::warning(this, "Error with manual channel mappings!", "Please double check your channel mappings and re-enter them.");
                deviceslist->updateTimer.start(1000);
                return;
            }
            for(auto c : latestManualMappings){
                if(c < 0){
                    QMessageBox::warning(this, "Error with manual channel mappings!", "Please double check your channel mappings and re-enter them.");
                    deviceslist->updateTimer.start(1000);
                    return;
                }
            }
            wsdialog->workspaceGui->setChannelMap(latestManualMappings);
        }

        //Add in hardware config
        wsdialog->workspaceGui->loadedWorkspace.globalConf.applyHeadstageSettings(rec.settings);

//        bool ret = wsdialog->workspaceGui->saveToXML(tempws);
        wsdialog->workspaceGui->prepConfigsForSave();
        bool ret = wsdialog->workspaceGui->loadedWorkspace.writeRecConfig(tempws, 0);
        if(!ret){
            deviceslist->updateTimer.start(1000);
            return;
        }
        finalworkspace = tempws;*/
    }
    else{
        finalworkspace = trodesConfEntry->text();
        TrodesConfiguration conf;
        QString error = conf.readTrodesConfig(finalworkspace);
        if(!error.isEmpty()){
            QMessageBox::warning(this, "Error with trodesconf file!", error);
            //deviceslist->updateTimer.start(1000);
            return;
        }
        numchannels = conf.hardwareConf.NCHAN;

        //Can't check anything if a specific dat file is selected. the hardware is unknown
        if(!deviceslist->specificDatFileSelected()){
            //check for matching number of channels
            /*if(numchannels != rec.settings.numberOfChannels){
                int ret = QMessageBox::warning(this, "Mismatch: number of channels", QString("Mismatch between %1 and connected hardware for number of channels in recording. Created recording may not work.\nProceed?").arg(finalworkspace),
                                               QMessageBox::Yes|QMessageBox::Cancel, QMessageBox::Cancel);
                if(ret != QMessageBox::Yes){
                    return;
                }
            }*/
            //check for aux bytes
            if(rec.settings.auxbytes){
                bool foundauxbytes = false;
                for(auto const &device : conf.hardwareConf.devices){
                    if((device.name == "headstageSensor")||(device.name.contains("multiplexed",Qt::CaseInsensitive))){
                        foundauxbytes = true;
                        break;
                    }
                }
                if(!foundauxbytes){
                    int ret = QMessageBox::warning(this, "Mismatch: sensor/auxiliary bytes", "Mismatch between .trodesconf and connected hardware. Sensor/auxiliary bytes found in hardware, but not found in final .trodesconf. Created recording may not work.\nProceed?",
                                                   QMessageBox::Yes|QMessageBox::Cancel, QMessageBox::Cancel);
                    if(ret != QMessageBox::Yes){
                        //deviceslist->updateTimer.start(1000);
                        return;
                    }
                }
            }
            //if merge rec has ecu, check for ecu in final ws
            if(mergerec && mergeecu){
                bool foundecu = false;
                for(auto const &device : conf.hardwareConf.devices){
                    if(device.name == "ECU"){
                        foundecu = true;
                        break;
                    }
                }
                if(!foundecu){
                    int ret = QMessageBox::warning(this, "Mismatch: ECU", "Mismatch between .trodesconf and .rec to be merged. ECU found in .rec, not found in final .trodesconf. Created recording may not work.\nProceed?",
                                                   QMessageBox::Yes|QMessageBox::Cancel, QMessageBox::Cancel);
                    if(ret != QMessageBox::Yes){
                        //deviceslist->updateTimer.start(1000);
                        return;
                    }
                }
            }
            if(mergerec && sysclockused){
                bool foundsysclock = false;
                for(auto const &device : conf.hardwareConf.devices){
                    if(device.name == "SysClock"){
                        foundsysclock = true;
                        break;
                    }
                }
                if(!foundsysclock){
                    int ret = QMessageBox::warning(this, "Mismatch: SysClock", "Mismatch between .trodesconf and .rec to be merged. SysClock found in .rec, not found in final .trodesconf. Created recording may not work.\nProceed?",
                                                   QMessageBox::Yes|QMessageBox::Cancel, QMessageBox::Cancel);
                    if(ret != QMessageBox::Yes){
                        //deviceslist->updateTimer.start(1000);
                        return;
                    }
                }
            }

            //Add in hardware config
            conf.globalConf.applyHeadstageSettings(rec.settings);
            //QString tempws = finalrec + ".trodesconf";
            QString tempws = QFileInfo(finalrec).dir().absolutePath() + "/" + QFileInfo(finalrec).baseName()+".trodesconf";
            conf.writeRecConfig(tempws, 0);
            finalworkspace = tempws;
        }

        //calc packetsize
        for(auto const &device : conf.hardwareConf.devices){
            packetsize += device.numBytes;
        }
    }

    //check disk space
    QStorageInfo disk(finalrec);
    if (disk.isValid() && disk.isReady()) {
        size_t bytesneeded = rec.recordedPackets*packetsize + 2048; //data size + 2kb extra space
        size_t bytesavailable = disk.bytesAvailable();
        if(bytesavailable < bytesneeded){
            int ret = QMessageBox::warning(this, "Not enough disk space",
                                           QString("There may not be enough disk space available for the recording. %1 GB required, %2 GB available. \nProceed?").arg(bytesneeded/1024/1024/1024).arg(bytesavailable/1024/1024/1024),
                                           QMessageBox::Yes|QMessageBox::Cancel, QMessageBox::Cancel);
            if(ret != QMessageBox::Yes){
                //deviceslist->updateTimer.start(1000);
                return;
            }
        }
    }

    //extract
    //QString datfile = finalrec + ".dat";
    QString datfile = QFileInfo(finalrec).dir().absolutePath() + "/" +"logger_raw.dat";
    if(deviceslist->specificDatFileSelected()){
        datfile = deviceslist->getSelectedDatFile();
        int ret = QMessageBox::warning(this, "Using existing .dat file",
                                       "Since you are using an existing .dat file to convert into a .rec file, the .trodesconf cannot be checked with the hardware for mismatches. Continue?",
                                       QMessageBox::Yes|QMessageBox::Cancel, QMessageBox::Cancel);
        if(ret != QMessageBox::Yes){
            //deviceslist->updateTimer.start(1000);
            return;
        }
    }
    runEntireExtract(datfile, mergerec, finalrec, finalworkspace, numchannels, mergefile);
    //deviceslist->updateTimer.start(1000);
}

void MainWindow::runEntireExtract(QString datfile, bool mergerec, QString finalrec, QString finalworkspace, int numchannels, QString mergefile){
    bool autocomplete = autogenWorkspace->isChecked();
    bool existingdatfile = deviceslist->specificDatFileSelected();
    CreateRecButton->setEnabled(false);
    ExtractProcess *p = nullptr;
    if(existingdatfile && !mergerec){
        SdtorecProcess *p = new SdtorecProcess(datfile, finalworkspace, finalrec, numchannels, this);
        p->hideConsole();

        connect(p, &AbstractProcess::newOutputLine, this, &MainWindow::newProcessOutputLine);
        connect(this, &MainWindow::killAllProcesses, p, &AbstractProcess::killProcess);
        connect(this, &MainWindow::destroyed, p, &AbstractProcess::deleteLater);
        connect(p, &AbstractProcess::processstarted, this, &MainWindow::sdtorecStarted);
        connect(p, &AbstractProcess::processfinished, this, &MainWindow::sdtorecFinished);
        connect(p, &AbstractProcess::processfinished, this, [this, autocomplete, finalworkspace](int code){
            entireExtractCompleted(code, autocomplete, finalworkspace);
        });
        processconsole->append("<p>Creating .rec file from existing .dat file... </p>");
        p->start();
    }
    else if(existingdatfile && mergerec){
        MergeProcess *p = new MergeProcess(datfile, mergefile, finalworkspace, finalrec, numchannels, this);
        p->hideConsole();

        connect(p, &AbstractProcess::newOutputLine, this, &MainWindow::newProcessOutputLine);
        connect(this, &MainWindow::killAllProcesses, p, &AbstractProcess::killProcess);
        connect(this, &MainWindow::destroyed, p, &AbstractProcess::deleteLater);
        connect(p, &AbstractProcess::processstarted, this, &MainWindow::mergeStarted);
        connect(p, &AbstractProcess::processfinished, this, &MainWindow::mergeFinished);
        connect(p, &AbstractProcess::processfinished, this, [this, autocomplete, finalworkspace](int code){
            entireExtractCompleted(code, autocomplete, finalworkspace);
        });
        processconsole->append("<p>Merging .rec file with existing .dat file... </p>");
        p->start();
    }
    else if(!mergerec){
        if(deviceslist->getSelectedDevicePath() != "Docking Station"){
            p = new ExtractProcess(deviceslist->getSelectedDevicePath(), datfile, this);
        }
        else{
            //First copy finalworkspace data to finalrec
            QFile::copy(finalworkspace, finalrec);
            //Create extract process, passing in finalrec file.
            p = new ExtractProcess(deviceslist->getSelectedDevicePath(), finalrec, this);
        }
        p->hideConsole();
        connect(p, &AbstractProcess::newOutputLine, this, &MainWindow::newProcessOutputLine);
        connect(this, &MainWindow::killAllProcesses, p, &AbstractProcess::killProcess);
        connect(this, &MainWindow::destroyed, p, &AbstractProcess::deleteLater);
        connect(p, &AbstractProcess::processstarted, this, &MainWindow::extractStarted);
        if(deviceslist->getSelectedDevicePath() != "Docking Station"){
            connect(p, &AbstractProcess::processfinished, this, [datfile, finalworkspace, finalrec, numchannels, autocomplete, this](int code){
                if(code){
                    qDebug() << "Error with extract:" << code;
                    QMessageBox::warning(this, "Error extracting", "Error extracting. Not continuing with sdToRec");
                    return;
                }
                //compatibility for old sd card utilities that still need sdtorec
                SdtorecProcess *p = new SdtorecProcess(datfile, finalworkspace, finalrec, numchannels, this);
                p->hideConsole();

                connect(p, &AbstractProcess::newOutputLine, this, &MainWindow::newProcessOutputLine);
                connect(this, &MainWindow::killAllProcesses, p, &AbstractProcess::killProcess);
                connect(this, &MainWindow::destroyed, p, &AbstractProcess::deleteLater);
                connect(p, &AbstractProcess::processstarted, this, &MainWindow::sdtorecStarted);
                connect(p, &AbstractProcess::processfinished, this, &MainWindow::sdtorecFinished);
                connect(p, &AbstractProcess::processfinished, this, [this, autocomplete, finalworkspace](int code){
                    entireExtractCompleted(code, autocomplete, finalworkspace);
                });
                processconsole->append("<p>Starting sd to rec ... </p>");
                p->start();
            });
        }
        else{
            connect(p, &AbstractProcess::processfinished, this, [this, autocomplete, finalworkspace](int code){
                entireExtractCompleted(code, autocomplete, finalworkspace);
            });
        }
    }
    else{
        p = new ExtractProcess(deviceslist->getSelectedDevicePath(), datfile,this);
        p->hideConsole();
        connect(p, &AbstractProcess::newOutputLine, this, &MainWindow::newProcessOutputLine);
        connect(this, &MainWindow::killAllProcesses, p, &AbstractProcess::killProcess);
        connect(this, &MainWindow::destroyed, p, &AbstractProcess::deleteLater);
        connect(p, &AbstractProcess::processstarted, this, &MainWindow::extractStarted);
        connect(p, &AbstractProcess::processfinished, this,
        [datfile,mergerec,finalrec,finalworkspace,numchannels, mergefile, autocomplete, this](int code){
            //Lambda function code to be executed when extract process finished
            if(code){
                qDebug() << "Error with extract:" << code;
                QMessageBox::warning(this, "Error extracting", "Error extracting. Not continuing with merge");
                CreateRecButton->setEnabled(true);
                progressBar->setValue(0);

                return;
            }
            AbstractProcess *p;
            p = new MergeProcess(datfile, mergefile, finalworkspace, finalrec, numchannels, this);
            p->hideConsole();

            connect(p, &AbstractProcess::newOutputLine, this, &MainWindow::newProcessOutputLine);
            connect(this, &MainWindow::killAllProcesses, p, &AbstractProcess::killProcess);
            connect(this, &MainWindow::destroyed, p, &AbstractProcess::deleteLater);
            connect(p, &AbstractProcess::processstarted, this, &MainWindow::mergeStarted);
            connect(p, &AbstractProcess::processfinished, this, &MainWindow::mergeFinished);

            processconsole->append("<p>Starting merge sd and rec ... </p>");
            p->start();

            connect(p, &AbstractProcess::processfinished, this, [this, autocomplete, finalworkspace](int code){
                entireExtractCompleted(code, autocomplete, finalworkspace);
            });
        });
    }

    //Start extract
    if(p){
        processconsole->append("<p>Starting extract ... </p>");
        p->start();
    }
}

void MainWindow::entireExtractCompleted(int code, bool autocomplete, QString finalworkspace){
    CreateRecButton->setEnabled(true);
    //deviceslist->updateTimer.start(1000);
    if(code==0){
        processconsole->append("<p>Extract finished successfully!</p>");
        qDebug() << "Extract finished successfully:" << mergeOutputFilePath;

        progressBar->setValue(100);
        if(autocomplete){
            QMessageBox::information(this, "Generated workspace", QString(
                        "The workspace for the recording was automatically generated and saved at %1\n"
                        "This file can be used for future extractions and saving settings").arg(finalworkspace));
        }
    }
}

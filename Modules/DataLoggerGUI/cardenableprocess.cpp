#include "cardenableprocess.h"

CardEnableProcess::CardEnableProcess(QString device, QWidget *parent)
    : AbstractProcess("CardEnable", parent), device(device)
{
}

void CardEnableProcess::start(){
    if(device == "Docking Station"){
        docking_cardenable();
    }
    else{
    #if defined(Q_OS_WIN)
        win_start_cardenable();
    #elif defined(Q_OS_MAC)
        mac_start_cardenable();
    #elif defined(Q_OS_LINUX)
        lin_start_cardenable();
    #else
    #error "OS not supported!"
    #endif
    }
}

void CardEnableProcess::customReadOutput(const QString &line){
    if(line.contains("continue?", Qt::CaseInsensitive)){
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(nullptr, "Continue card enable?",
                                      "WARNING: It appears this card has never been used for spike recording. "
                                      "Enabling card for writing, all data will be lost. Continue?",QMessageBox::Yes|QMessageBox::No);
        if(reply == QMessageBox::Yes){
//            console->raise();
            console->insertPlainText("> Yes\n");
            process->write("yes\n");
        }
        else{
//            console->raise();
            console->insertPlainText("> No\n");
            process->write("no\n");
        }
    }
}


void CardEnableProcess::win_start_cardenable(){
    QRegularExpression rex("(.*)([0-9]{1,3})"); //Takes the last digits of device string (PhysicalDrive1) -> 1
    QRegularExpressionMatch m = rex.match(device);
    if (m.hasMatch() && m.lastCapturedIndex() > 1) {
        QStringList args;
        args << m.captured(2);

        process->start(".\\windows_sd_util\\cardEnable.exe", args);
    }

}

void CardEnableProcess::lin_start_cardenable(){
    process->start("./linux_sd_util/cardEnable",{device});
}

void CardEnableProcess::mac_start_cardenable()
{
    process->start("./macos_sd_util/cardEnable", {device});
}

void CardEnableProcess::docking_cardenable(){
    //process->start(DOCKINGPATH, {"-c"});
    QMessageBox *waitbox = new QMessageBox();
    waitbox->setWindowTitle("Please wait");
    waitbox->setText("Enabling for recording and erasing existing data. Please wait ... ");
    waitbox->setIcon(QMessageBox::Information);
    waitbox->setStandardButtons(QMessageBox::NoButton);
    //connect(process, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished)
    //        , waitbox, &QMessageBox::accept);
    waitbox->show();
    //waitbox->exec();

    int ret = dockCommandEngine.dockCardEnable();
    //waitbox->accept();
    waitbox->close();
    delete waitbox;


    if(ret == -2){
        printf("Error running card-enable: No device mounted on the docking station!\n");
        int msgret = QMessageBox::information(this, "Card Enable","Error running card-enable: No device mounted on the docking station!");


    }
    if(ret){
        printf("Error running card-enable: Communication error with USB. %d \n", ret);
        int msgret = QMessageBox::information(this, "Card Enable","Error running card-enable: Communication error with USB.");

    }
    else{
        printf("Card-enable successful. You may now use this device for recordings.\n");
        int msgret = QMessageBox::information(this, "Card Enable","Card-enable successful. You may now use this device for recordings.");
    }

    emit processfinished(ret);


}

#ifndef DISKLISTWIDGET_H
#define DISKLISTWIDGET_H

#include <QtWidgets>
#include <QStorageInfo>
#include <string>
#include "hardwaresettings.h"

struct LoggerRecording{
    HeadstageSettings settings;
    int packetSize;
    bool enabledForRecording;
    uint64_t maxPackets;
    uint64_t recordedPackets;
    uint64_t droppedPackets;
    bool MCUDataNeeded;
    bool ECUConnected;
    QString recordingDateTime;
    enum hwsetup{
        NONE,           //No hs detected
        SD_PC_DIRECT,   //SD or HS plugged directly into the computer
        HS_DOCK,        //HS plugged in thru a dockingstation
        SD_DOCK,        //SD plugged in thru a dockingstation
    };
    hwsetup setup;

    LoggerRecording() : packetSize(0), maxPackets(0), recordedPackets(0), droppedPackets(0), setup(NONE){}
};

class DiskListWidget : public QWidget
{
    Q_OBJECT
public:
    explicit DiskListWidget(QWidget *parent = nullptr);
    QString getSelectedDevicePath() const;
    void editStatus(const QString &device, const QString &status);
    QByteArray saveColumnGeometry() const;
    void loadColumnGeometry(const QByteArray &d);
    void allowLoggerSettingsChange(bool allow);
    void allowDockSettingsChange(bool allow);
    void allowEnable(bool allow);

    LoggerRecording getSelectedDeviceSettings();

    bool specificDatFileSelected() const {return !selectedDatFile.isEmpty();}
    QString getSelectedDatFile() const {return selectedDatFile;}
    QTimer updateTimer;
signals:
    void rowSelected();
    void listUpdated();
    void cardEnableCalled();
    void pcheckCalled();
    void readconfigCalled();
    void writeconfigCalled();
public slots:
    void updateDevices();
    void cardenable();
    void deviceSelected(const QModelIndex &index);
    void pcheck();
    void readconfig();
    void writeconfig();
    void dockconfig();
private:
    QPushButton *refreshbutton;
    QPushButton *enableButton;
    QPushButton *loggerSettingsButton;
    QPushButton *dockSettingsButton;

    QTreeView *deviceslist;
    QStandardItemModel *model;
    QString selectedDevicePath;

//    int selectedRow;

    //list sd storage devices connected directly to computer
    void windows_parse_listDevices(const std::string &output);
    void linux_parse_listDevices(const std::string &output);
    void macos_parse_listDevices(const std::string &output);

    //cache and store device settings when detected instead of looking them up again every time
    void fetchDeviceSettings(QString device, LoggerRecording::hwsetup setup);
    QMap<QString, LoggerRecording> devicesettings;

    void selectDatFile();
    QString selectedDatFile;
    QLabel *existingDatSelected;

   /* void dockDetectFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void diskDetectFinished(int exitCode, QProcess::ExitStatus exitStatus);*/
};

#endif // DISKLISTWIDGET_H

#ifndef WORKSPACEEDITOR_H
#define WORKSPACEEDITOR_H

#include <QObject>
#include <QDebug>
#include <QLabel>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QGroupBox>
#include <QCheckBox>
#include <QLineEdit>
#include <QComboBox>
#include <QListWidget>
#include <QMimeData>
#include <QDrag>
#include <QTreeWidget>
#include <QDropEvent>
#include <QDragMoveEvent>
#include <QDragEnterEvent>
#include <QMessageBox>
#include <QSettings>
#include <QDialog>
#include <QTableWidget>
#include <QColorDialog>
#include <QFileDialog>
#include <QDialogButtonBox>
#include <QColor>
#include <QSpinBox>
#include <QStyledItemDelegate>
#include <QPainter>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QCoreApplication>
#include <QHeaderView>
#include <QAction>
#include <QMenu>
#include <math.h>
#include <algorithm>
#include <QClipboard>
#include <QApplication>
#include "configuration.h"
#include "cargrouppanel.h"
#include "probelayoutpanel.h"

//#include "usbdaqThread.h"
//#include "dockusbthread.h"
//#include "ethernetSourceThread.h"
#include "sourceController.h"


//#include <QDragLeaveEvent>

/* *** Default Settings *** */
#define DEFAULT_SAMPLING_RATE 30000
#define DEFAULT_HWCHAN_MAXDISP 200
#define DEFAULT_HWCHAN_THRESH 30
#define DEFAULT_HWCHAN_THRESH_RANGECONVERT 400
#define DEFAULT_HWCHAN_STREAMINGCHANLOOKUP 400
#define DEFAULT_HWCHAN_TRIGGERON 1
#define DEFAULT_CURRENT_ITEM_COLOR QColor(0,0,255,50)
#define DEFAULT_AUX_DIG_MAXDISP 2
#define DEFAULT_AUX_DIG_COLOR "#aaaaaa"
#define DEFAULT_AUX_ANALOG_MAXDISP 2000
#define DEFAULT_AUX_ANALOG_COLOR "green"

/* *** GUI Settings *** */
#if defined (__WIN32__) //for some reason the light grey windows color doesn't show up on linux hence this ugly if preprocessor statement
    #define WE_TABLE_HEADER_COLOR "#e7e7e7"
    #define WE_RIGHT_ARROW_POINT 17
    #define WE_DOWN_ARROW_POINT 15
#elif defined (__APPLE__)
    #define WE_TABLE_HEADER_COLOR "#e7e7e7"
    #define WE_RIGHT_ARROW_POINT 18
    #define WE_DOWN_ARROW_POINT 10
#else
    #define WE_RIGHT_ARROW_POINT 15
    #define WE_DOWN_ARROW_POINT 10
    #define WE_TABLE_HEADER_COLOR "#C5C5C5"
#endif

/* *** Global Configuarion Values *** */

/*extern GlobalConfiguration *globalConf;
extern streamConfiguration *streamConf;
extern SpikeConfiguration *spikeConf;
extern headerDisplayConfiguration *headerConf;
extern ModuleConfiguration *moduleConf;
extern HardwareConfiguration *hardwareConf;*/
/**
struct HardwareChannelSettings {
    int ID;
    int thresh;
    int maxDisp;
    int streamingChannelLookup;
    bool triggerOn;
};**/

/* *** Enumerations *** */
enum FocusKey { FK_NULL, FK_LeftArrow, FK_RightArrow, FK_UpArrow, FK_DownArrow };
enum FocusTarget { FT_NULL, FT_nTrodeTree, FT_ChannelTree, FT_ConfiguredAux, FT_AvailAux };

/* *** Forward Declarations ** */
class TrodesTreeDelegate;

struct NTrodeSettings {
    QTreeWidgetItem *treeItem;
    SingleSpikeTrodeConf settings;
    //HardwareChannelSettings channelSettings;
};

class ColorGenerator {

public:
    ColorGenerator(void);
    //static QColor randColor(float saturation = 0.99, float value = 0.99 );
    static QColor naiveRandColor(void);
    QColor getSequenceColor(void);

private:
    int colorCounter;
    QList<QColor> sequenceColors;
};

class ClickableFrame : public QFrame {
    Q_OBJECT
public:
    explicit ClickableFrame(QWidget *parent = 0);

protected:
    void mousePressEvent(QMouseEvent *event);

signals:
    void sig_clicked(void);
};

class ClickableLabel : public QLabel {
    Q_OBJECT
public:
    explicit ClickableLabel(QString labelTxt = "", QWidget *parent = 0);

protected:
    void mousePressEvent(QMouseEvent *event);

signals:
    void sig_clicked(void);
};

class ClickableLineEdit : public QLineEdit {
    Q_OBJECT
public:
    explicit ClickableLineEdit(const QString& text = "", QWidget *parent = 0);

protected:
    void mousePressEvent(QMouseEvent *event);

signals:
    void sig_clicked(QString text);
};

class TagGroupingPanel : public QGroupBox {
    Q_OBJECT
public:
    explicit TagGroupingPanel(QWidget *parent = 0);

    QHash<QString,int> getNewTagHash(void);
    QList<QString> getTagList(void);

    QList<GroupingTag> getGTagList(void);
    QList<GroupingTag> getAddedTags(void); //All tags added by the panel
    QList<GroupingTag> getRemovedTags(void); //All tags removed by the panel
    void clearAddedTagList(void);
    void clearRemovedTagList(void);

private:
    CategoryDictionary cataDict;

    QVBoxLayout *mainLayout;
    QHBoxLayout *addTagLayout;
    QHBoxLayout *tagListLayout;

    QLabel      *labelCategory;
    QLabel      *labelTags;

    QComboBox   *categorySelectBox;
    QComboBox   *tagSelectBox;

    QPushButton *buttonAddTag;
    QPushButton *buttonRemoveTag;


    QTabWidget  *tagListView;
    QListWidget *tagAllList;
    QListWidget *tagSharedList;

    QHash<QString,int> availTags;
    QHash<QString,int> tempTags;

    QList<GroupingTag> addedTags;
    QList<GroupingTag> removedTags;

public slots:
    void updateDict(CategoryDictionary dict);
    void updateTags(QHash<QString,int> tags);
    void loadCurrentCategoryTags(void);

    void loadNTrodeTags(QHash<GroupingTag,int> loadedTags); //load one nTrode's tags
    void loadMultipleNTrodeTags(QList<SingleSpikeTrodeConf> nTrodes); //load multiple nTrode's tags
    void sortTagLists(void); //this function sorts the tagList widget by grouping tags of like categories together
    void sortTagList(QListWidget *list);

    inline void setCurrentTab(int tabIndex) {tagListView->setCurrentIndex(tabIndex);}

private slots:
    void processCategorySelectBox(int selectedIndex);
    void processTagSelectBox(int selectedIndex);
    void addCategory(void);
    void populateCategorySelectBox(void);

    void addTagToList(void);
    void removeTagFromList(void);

    void addTagToGUILists(QString tagStr);
    void removeTagFromGUILists(QString tagStr);

    void populateTagSelectBox(void);


    void setSharedTabEnabled(bool enabled);




signals:
    void sig_enableApplyButtons(void);
    void sig_categoryAdded(QString newCategory);
    void sig_newTagAdded(QString category, QString newTag); //this registers the tag with the global dictionary

    void sig_GTagAddedToNTrodes(GroupingTag newTag);
    void sig_GTagRemovedFromNTrodes(GroupingTag rTag);

    void sig_tabChanged(int newTabIndex);
};



#include <QThread>
//The TrodesTree class is a class derived from the QTreeWidget that implements several universal behaviors across all TreeWidgets used in Trodes, such as multi selection and shift-click range highlighting
class TrodesTree : public QTreeWidget
{
    Q_OBJECT

public:
    TrodesTree(QWidget *parent = 0);

    void setFocusPartner(FocusKey activatingKey, FocusTarget partner); //set the key that, when pressed, will pass the focus to the specified partner widget

    inline void setPrevSelectedItem(QTreeWidgetItem* prevItem) {prevSelectedItem = prevItem;};
    inline QTreeWidgetItem* getPrevSelectedItem(void) {return(prevSelectedItem);};

public slots: //TODO: move these to private slots
    void deselectAll(void);
    void processRangeHighlight(void);
    void setHighlightChannel(int HWChan);

protected:
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);


private:
    TrodesTreeDelegate *delegate;
    QTreeWidgetItem* prevSelectedItem;
    FocusKey focusKey;
    FocusTarget focusTarget;

signals:
    void sig_sendFocus(int focusTarget);
    void sig_enterKeyPressed(void);
    void hardwareChannelClicked(int HWChan);

};

//The TrodesTreeDelegate class subclasses QStyledItemDelegate to provide TrodesTree with expanded custom graphics -- ostensibly borders around QTreeWidgetItems
class TrodesTreeDelegate : public QStyledItemDelegate {
    Q_OBJECT
public:
    explicit TrodesTreeDelegate(QObject *parent, TrodesTree *thisTree);

protected:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

private:
    TrodesTree *tree;
};


class HWChannelTreeWidget : public TrodesTree
{
    Q_OBJECT

public:
    HWChannelTreeWidget(QWidget *parent = 0);
    QList<int> getChannelsInList();
    void setChannelsInList(QList<int> channels);

protected:
    void keyPressEvent(QKeyEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    //void startDrag(Qt::DropActions supportedActions) override;

private:
//    QTreeWidgetItem *prevSelectedItem;



public slots:
    void addHardwareChannel(QString name);
    void removeHardwareChannel(int channelId);
    QList<QTreeWidgetItem*> getSelectedChannels(void);
    QTreeWidgetItem* getFirstAvailChan(void);

private slots:
//    void deselectAll(void);
//    void processRangeHighlight(void);

signals:
    void sig_clickedOnChannel(int channelID);
//    void sig_sendFocus(int focusTarget);
    void sig_sendChannels(void);

};

class NTrodeChannelMappingTable : public QTableWidget
{
    Q_OBJECT
public:
    NTrodeChannelMappingTable(QWidget *parent = 0);
    void setEditProcessorLock(bool);
    bool isEditing();


public slots:
    void setNumHardwareChannels(int numHWChan);
    void assignNTrodeIDToChannel(int HWChan, int nTrodeID);
    void removeChannels(QList<int> hwChannels);
    QList<QList<double> > getChannelData();
    void assignDataToChannel(int HWChan, QList<double> channelData, int order = -1);
    QList<int> getChannelAssignments();
    QList<int> getChannelDisplayOrder();
    void setHighlightChannel(int HWChan);
    void rightClickMenuRequested(QPoint pos);
    void rightClickHeaderMenuRequested(QPoint pos);

protected:
    void keyPressEvent(QKeyEvent *event);

private:
    int totalHwChannels;
    bool editProcessorLocked;
    bool _isEditing;
    QStringList columnHeaders;
    QList<int> nTrodeHWAssignments;
    QModelIndexList lastSelectionList;
    QList<int> getChannelOrderForNTrodeID(int nTrodeID);
    void setChannelOrderForNTrodeID(int nTrodeID, int hwChan = -1, int orderVal = -1);


private slots:
    void processCellEdit(int row, int col);
    void processCellClicked(int row, int col);
    void processNewItemSelection();
    void copyToClipboard();
    void pasteFromClipboard();
    void pasteFromClipboard(int valueToAdd);
    void deleteSelected();

signals:
    void newChannelAssignment();
    void channelClicked(int HWChan);


};

class NTrodeTreeWidget : public TrodesTree
{
    Q_OBJECT

public:
    NTrodeTreeWidget(QWidget *parent = 0);


    void allClear(void);

    //NTrodeSettings getNTrode(int index);
    //void setNTrodeSettings(int index, SingleSpikeTrodeConf settings);
    //QTreeWidgetItem* addNewNTrode(QString name, SingleSpikeTrodeConf settings, bool loadingFromXML = false);
    //void loadNTrodeIntoTree(QString name, SingleSpikeTrodeConf settings);
    //void removeNTrode(QTreeWidgetItem *removedNTrode);
    //QTreeWidgetItem* addChildToNTrode(int index, int indexInNtrode, QString childToAdd);
    //void loadChildIntoTree(int index, QString childToAdd);
    //void removeChildFromNTrode(int nTrodeIndex, int childIndex);
    int findHWChannelsNtrode(int HWChannel);
    void updateMapping(QList<int> nTrodeMap);
    void updateMapping(QList<int> nTrodeMap, QList<int> displayOrder);
    void updateMapping(QList<SingleSpikeTrodeConf> *nTrodeConfigs);

    bool isTopLevelItem(QTreeWidgetItem *checkItem);



    QTreeWidgetItem* getFirstSelectedNTrode(void);

    //int getNTrodeIndex()
    //DEBUG
    //void printNTrodeList(void);

    inline void setLoading(bool isLoading) {loadingXML = isLoading;};

    //get properties
    inline bool isChanBeingDragged(void) {return(isDragging);};
    inline bool renderBorderUp(void) {return(borderUp);};
    inline QTreeWidgetItem* getHovNTrode(void) {return(overNTrode);};
    inline QTreeWidgetItem* getHovChan(void) {return(hwChan);};
    //inline QList<NTrodeSettings> getNTrodeList(void) {return(nTrodes);};

    inline int getTotalHwChannels(void) {return(totalHwChannels);};
    inline QList<int> getMap(void) {return(tableMap);};


protected:
    /*void dropEvent(QDropEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
    void dragLeaveEvent(QDragLeaveEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);*/
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);


private:
//    bool isTopLevelItem(QTreeWidgetItem *checkItem);
    bool loadingXML;
    bool processingSelection;
    QList<QList<int> > nTrodeHWAssignments;
    QList<int>         nTrodeIDs;
    QList<int>         tableMap;
    QList<int>         nTrodeDisplayOrder;


    //QList<NTrodeSettings> nTrodes;
    QList<SingleSpikeTrodeConf> *nTrodeConfigs;
    QList<int> availableIDs;
    int totalHwChannels;

    //these variables are for the delegate to use in the paint event
    bool isDragging;
    bool borderUp;

    ClickableLabel *buttonExpandAll;

    QTreeWidgetItem *overNTrode;
    QTreeWidgetItem *hwChan;
    QTreeWidgetItem *prevSelectedItem;

    void updateMapping();

public slots:
    //void correctDigitalReferences(int removedNTrodeID);
    //void correctDigitalReferences(int originNTrodeIndex, int removedChanIndex);
    //void correctLFPSettings(int indexInNtrode, NTrodeSettings *curNTrode);
    //void correctLFPSettings(int originNTrodeIndex, int removedChanIndex);
    void setTotalHwChannels(int hwChannels);
    void removeSelectedItems(void);
    void setItemsExpanded(bool expanded);


private slots:
//    void deselectAll(void);
    void expandAllButtonPushed(void);
    void processSelection();
//    void processRangeHighlight(void);

signals:
    void sig_returnChild(QString returnedChild);
    void sig_removeChildFromList(int childIdToRemove);
    void sig_addedNTrode(int ID, bool loadingFromXML);
    void sig_removedChannels(QList<int> HWchans);
    void sig_channelMoved(void);
    void sig_changed();
//    void sig_channelRemovedFromNTrode(int nTrodeID);
    void sig_reloadNTrodeSettings(int nTrodeIndex);
    void sig_selectedNTrode(int index);
    void sig_clickedOnChannel(int channelID);
//    void sig_channelMovedFromNTrode(int originNtrodeID, int channelID);
//    void sig_sendFocus(int focusTarget);
};

//custom delegate to control NTrodeTreeWidget's paint events
class NTrodeTreeWidgetDelegate : public TrodesTreeDelegate
{
    Q_OBJECT
public:
    explicit NTrodeTreeWidgetDelegate(QObject *parent, NTrodeTreeWidget *treeWidget);

protected:
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;

private:
    NTrodeTreeWidget *nTrodeTreeWidget;
};


class AuxChanTreeWidget : public TrodesTree
{
    Q_OBJECT
public:
    AuxChanTreeWidget(QWidget *parent = 0);

protected:
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);

signals:
    void sig_selectedAuxChannel(QTreeWidgetItem* selectedItem, QTreeWidgetItem* prevItem);
//    sig_removeConfiguredChannel(int deviceIndex, int channelIndex);
    void sig_deleteKeyPressed(void);

};

class AddTagDialog : public QDialog {
    Q_OBJECT
public:
    explicit AddTagDialog(QWidget *parent = 0);

    inline QString getText(void) {return(editTagName->text());};

    void setAddType(QString type); //allows you to change the labels asking what you're adding

private:
    QVBoxLayout *mainLayout;
    QHBoxLayout *editLineLayout;

    QLabel *labelMainText;
    QLabel *labelTagName;
    QLineEdit *editTagName;
    QDialogButtonBox *buttonBox;
};

class EditTagDialog : public QDialog {
    Q_OBJECT
public:
    explicit EditTagDialog(QList<QString> curTags, QHash<QString, int> availableTags, QWidget *parent = 0);

    inline QList<QString> getTags(void) {return(tags);};

private:
    //ui elements
    QVBoxLayout *mainLayout;

    QGroupBox   *addTagGroup;
    QHBoxLayout *addTagGroupLayout;
    QLabel      *labelAvailableTags;
    QComboBox   *tagSelectBox;
    QPushButton *buttonAddTag;

    QHBoxLayout *tagListLayout;
    QListWidget *tagList;
    QPushButton *buttonRemoveTag;

    QDialogButtonBox *buttonBox;

    //data storage
    QList<QString> tags;
    QHash<QString, int> availTags;

private slots:
    void processTagSelectBox(int selectedIndex);

    void addTagToList(void);
    void removeTagFromList(void);

    void acceptAndClose(void);
};

class AddModuleDialog : public QDialog {
    Q_OBJECT
public:
    explicit AddModuleDialog(QWidget *parent = 0);

    inline QString getText(void) {return(editModuleName->text());};
    inline void setMainText(QString text) {labelMainText->setText(text); setWindowTitle(text);};

private:
    QVBoxLayout *mainLayout;
    QHBoxLayout *editLineLayout;

    QLabel *labelMainText;
    QLabel *labelModuleName;
    QLineEdit *editModuleName;
    QDialogButtonBox *buttonBox;

};

class AddArgumentDialog : public QDialog {
    Q_OBJECT
public:
    explicit AddArgumentDialog(QWidget *parent = 0);

    inline QString getFlag(void) {return(QString("-%1").arg(editFlag->text()));};
    inline QString getValue(void) {return(editValue->text());};
    inline QString getArg(void) {return(QString("Argument -flag=\"%1\" -value=\"%2\"").arg(editFlag->text()).arg(editValue->text()));};
    inline void setMainText(QString text) {labelMainText->setText(text); setWindowTitle(text);};
    inline void populateEditFields(QString flag, QString value) {editFlag->setText(flag); editValue->setText(value);};

private:
    QVBoxLayout *mainLayout;
    QHBoxLayout *editLineLayout;

    QLabel *labelMainText;
    QLabel *labelFlag;
    QLabel *labelValue;
    QLineEdit *editFlag;
    QLineEdit *editValue;
    QDialogButtonBox *buttonBox;

};

enum Mode { M_NULL, M_STAND_ALONE, M_EMBEDDED };

class WorkspaceEditor : public QTabWidget
{
    Q_OBJECT
public:    
    WorkspaceEditor(Mode iniMode = M_STAND_ALONE, QWidget *parent = 0);
    enum HeadStageType {
        DEFAULT,
        NEUROPIXELS
    };

    void addECU();
    void addHSSensors();
    void addRF();
    void removeSysClock();
    void displayAllAux();
    void setNumChannels(int numchans);
    void configureDisplay(int columns, int pages);
    void setSamplingRate(int samplingrate);
    bool getWorkspaceIfValid(TrodesConfiguration &t);
    QList<int> getChannelMap();

    enum ButtonOption {BO_Cancel, BO_Preserve, BO_Overwrite };

    QString curSaveFile;
    QString curLoadedFile;

//    SingleSpikeTrodeConf defaultSettings;
    QList<DeviceInfo> availableDevices;
    int MCUIndex;
    int ECUIndex;
    int RFIndex;
    int HSIndex;

    /* ****** UI Members ****** */
    QWidget *tabOne;
    int auxTab;
    //int spikeTab;
    int channelMappingTab;
    int moduleTab;
    int guiHeight;
    int guiWidth;

    /* *** General Settings Tab *** */
    //Intro View
    QWidget     *introView;
    QVBoxLayout *introMainLayout;
    QHBoxLayout *buttonBar;
    QLabel      *labelWelcome;
    QLabel      *labelWelcome2;
    QPushButton *buttonLoad;
    QPushButton *buttonNew;
    QList<QPushButton*> createWorkspaceButtons;

    //General Settings View
    QWidget     *generalInfoView;
    QVBoxLayout *generalInfoMainLayout;
    QLabel      *labelGenInfoTitle;

        //Global Group Variables
    QVBoxLayout       *ggVertLayout;
    QGridLayout       *ggTopLayout;
    QHBoxLayout       *ggBottomLayout;
    QGroupBox         *globalGroup;
    //QCheckBox         *realTimeMode;
    QCheckBox         *saveDisplayedChanOnly;
    ClickableLineEdit *editFilePath;
    QLineEdit         *editFilePrefix;
    QLabel            *labelFilePath;
    QLabel            *labelFilePrefix;

    QButtonGroup      *networkModeCheckBoxes;

    QLineEdit         *editRecordSegmentMax;
    QCheckBox         *recordSegmentCheckbox;
    QLabel            *labelMBsize;

    QComboBox         *lfpRateBox;
    QLabel            *lfpRateLabel;

        //Hardware Group Variables
    QTabWidget        *hardwareTypeTabs;
    QGroupBox         *hardwareGroup;
    QGroupBox         *deviceGroup;
    QVBoxLayout       *deviceGroupMainLayout;
    QHBoxLayout       *deviceGroupAddLayout;
    QHBoxLayout       *deviceGroupListButtonLayout;
    QHBoxLayout       *hgLayout;
    QVBoxLayout       *hgChannelFreqLayout;
    QComboBox         *channelSelector;
    ClickableLineEdit *editSamplingRate;

    QComboBox         *numNPProbesSelector;
    bool              npProbeCountChanged;

    QListWidget       *deviceList;
    QComboBox         *deviceSelector;
    QPushButton       *buttonAddDevice;
    QPushButton       *buttonRemoveDevice;

    QLabel            *labelSamplingRate;
    QLabel            *labelDeviceList;

        //Stream Group Variables
    QGroupBox      *streamGroup;
    QHBoxLayout    *streamGroupMainLayout;
    QHBoxLayout    *streamGroupColumnsLayout;
    QHBoxLayout    *streamGroupColorButtonLayout;

    QLabel         *labelStreamColumns;
    QLabel         *labelStreamPages;
    QLabel         *labelStreamColorButton;

    QComboBox      *streamColumnComboBox;
    QComboBox      *streamPagesComboBox;
    ClickableFrame *streamColorButton;




    /* *** Auxiliary Configuration Tab *** */
    QWidget        *auxConfView;
    QVBoxLayout    *auxMainLayout;

        //  aux channel settings group
    QGroupBox      *auxChannelGroup;
    QVBoxLayout    *auxChannelGroupMainLayout;
    QHBoxLayout    *auxChannelGroupTopLayout;
    QHBoxLayout    *auxChannelGroupBottomLayout;
    QHBoxLayout    *auxChannelGroupMaxDispLayout;
    QHBoxLayout    *auxChannelGroupButtonLayout;


    QLabel         *labelAuxChannelID;
    QLabel         *labelAuxChannelDevice;
    QLabel         *labelAuxMaxDispSpin;
    QLabel         *labelColor;
    QLabel         *labelMismatchAuxSettingsWarning;

    QSpinBox       *auxMaxDispSpinBox;

    ClickableFrame *buttonColorBox;

    QPushButton    *buttonAuxCancel;
    QPushButton    *buttonAuxApply;
    QPushButton    *buttonAuxApplyToAll;

        //  aux tree list layouts
    QHBoxLayout       *auxListLayout;
    QVBoxLayout       *auxListButtonLayout;

    TrodesTree       *availableAuxChannelsTree;
    AuxChanTreeWidget *configuredAuxChannelsTree;

    QPushButton       *buttonAuxAddChannel;
    QPushButton       *buttonAuxRemoveChannel;
    QPushButton       *buttonAutoPopulate;

    /* *** Spike Configuration Tab *** */
    QWidget     *spikeConfView;
    QWidget     *channelMappingView;
    QVBoxLayout *scMainLayout;
    QPushButton *buttonLoadChannelMapping;

    /*QVBoxLayout *dgMainLayout;
    QHBoxLayout *dgSecondaryLayout;
    QGridLayout *dgPanelsLayout;

    QVBoxLayout *dgDigitalRefLayout;
    QVBoxLayout *dgDigitalRefGroupLayout;
    QHBoxLayout *dgDigitalRefGroupTopLayout;
    QHBoxLayout *dgDigitalRefGroupBottomLayout;
    QVBoxLayout *dgDigitalRefGroupNTrodeLayout;
    QVBoxLayout *dgDigitalRefGroupChannelLayout;
    QVBoxLayout *dgSpikeFilterLayout;
    QHBoxLayout *dgSpikeFilterGroupLayout;
    QVBoxLayout *dgSpikeFilterGroupLowLayout;
    QVBoxLayout *dgSpikeFilterGroupHighLayout;
    QVBoxLayout *dgLFPsettingsLayout;
    QHBoxLayout *dgLFPsettingsGroupLayout;
    QVBoxLayout *dgLFPsettingsGroupChannelLayout;
    QVBoxLayout *dgLFPsettingsGroupHighLayout;
    QVBoxLayout *dgHardwareChannelLayout;
    QHBoxLayout *dgDisplaySettingsGroupLayout;
    QVBoxLayout *dgDisplaySettingsMaxDispLayout;
    QVBoxLayout *dgDisplaySettingsGroupChannelColorLayout;

    ClickableFrame *buttonChannelColorBox;

    QCheckBox   *enableSpikeFilter;
    QCheckBox   *enableDigitalRef;
    QCheckBox   *enableLFPRef;

    QComboBox   *nTrodeBox;
    QComboBox   *channelBox;
    QComboBox   *lowFilterBox;
    QComboBox   *highFilterBox;
    QComboBox   *LFPChannelsBox;
    QComboBox   *LFPHighFilterBox;

    QSpinBox    *maxDispSpinBox;

    QLabel      *labelNTrode;
    QLabel      *labelChannel;
    QLabel      *labelRefOptions;
    QLabel      *labelLowFilter;
    QLabel      *labelHighFilterFilter;
    QLabel      *labelLFPChannelsFilter;
    QLabel      *labelLFPHighFilter;
    QLabel      *labelLFPsettings;
    QLabel      *labelMaxDispSpin;
    QLabel      *labelChannelColor;*/

    QHBoxLayout *addNTrodeLayout;
    QHBoxLayout *listLayout;
    QVBoxLayout *listButtonLayout;
    QGroupBox   *defaultGroup;
    QGroupBox   *dgDigitalRefGroup;
    QGroupBox   *dgSpikeFilterGroup;
    QGroupBox   *dgLFPsettingsGroup;
    QGroupBox   *dgDisplaySettingsGroup;

    QLineEdit   *editNumToAdd;
    QPushButton *buttonAddNTrode;

    QLabel      *labelSCHeader;


    NTrodeTreeWidget                      *nTrodeTree;
    NTrodeChannelMappingTable             *channelTable;
    NTrodeTreeWidgetDelegate              *nTrodeDelegate;
    HWChannelTreeWidget                   *availHardChanList;
    ProbeLayoutWidget                     *probeLayoutWidget;

        //Hardware Channel Settings Editor
    QSpacerItem *hardwareChannelSpacer;
    QVBoxLayout *scChannelSettingsMainLayout;

    QHBoxLayout *scChannelSettingsGroupLayout;


    QHBoxLayout *scChannelSettingsEnableTriggerLayout;
    QVBoxLayout *scChannelSettingsThreshLayout;


    QHBoxLayout *nTrodeSettingsButtonLayout;

    QWidget     *channelSettingsView;
    QGroupBox   *channelSettingsGroup;

    QLabel      *labelChannelID;
    QLabel      *labelThreshSpin;
    QLabel      *labelEnableTrigger;
    QLabel      *labelMismatchSettingsWarning;

    QCheckBox   *enableTrigger;

    QSpinBox    *threshSpinBox;

    //QPushButton *buttonNTrodeAdd;
    //QPushButton *buttonAddListItems;
    //QPushButton *buttonRemoveListItems;
    QPushButton *buttonNTrodeApplyToAll;
    QPushButton *buttonNTrodeApply;
    QPushButton *buttonNTrodeCancel;

        //Tag Settings Editor
    TagGroupingPanel *tagGroupPanel;

    QVBoxLayout *dgTagPanelLayout;

    QVBoxLayout *tagGroupLayout;

    QGroupBox   *tagGroup;

    QLabel      *labelTagPanel;
    QLabel      *labelTagSelection;

    QComboBox   *tagSelectionBox;

        //Filter Bar Elements
    QGroupBox   *filterGroup;
    QVBoxLayout *filterGroupLayout;
    QHBoxLayout *filterGroupTopLayout;

    QLabel      *labelFilterSelector;
    QComboBox   *filterSelector;
    QLabel      *labelFilterOperationSelector;
    QComboBox   *filterOperationSelector;
    QPushButton *buttonClearFilters;

    QLabel      *labelActiveFilters;

    /* *** Reference groups tab *** */
    CARGroupPanel *cargrouppanel;

    /* *** Module Configuration Tab *** */
    QWidget     *moduleConfView;
    QVBoxLayout *mcMainLayout;
    QLabel      *labelMCHeader;


        //Add Module Group Variables
    QGroupBox   *amGroup;
    QHBoxLayout *amGroupLayout;
    QComboBox   *modulePullDownMenu;
    QCheckBox   *enableSendNetworkInfo;
    QCheckBox   *enableSendTrodesConfig;
    QLabel      *labelSendNetworkInfo;
    QLabel      *labelSendTrodesConfig;
    QPushButton *buttonAddModule;
    bool         editingModule;

        //Module/Argument List layout
    QHBoxLayout *amLowerMainLayout;
    QVBoxLayout *amLowerButtonLayout;
    QTreeWidget *moduleArgumentTree;
    //QListWidget *moduleArgumentList;
    QPushButton *buttonAddArgument;
    QPushButton *buttonEdit;
    QPushButton *buttonDelete;

    /* *** Hardware diagnostics Tab *** */
    QWidget     *diagnosticsView;
    QGridLayout *diagnosticsLayout;
    QLabel      *diagnosticsHeader;
    QTextEdit   *diagnosticsConsole;


    //CONFIGURATION CONTAINERS
    //global Configuration container
    GlobalConfiguration *globalConfig;

    NetworkConfiguration *networkConfig;

    //Hardware Configuration container;
    HardwareConfiguration *hardwareConfig;

    //Auxilliary channel Configuration container
    headerDisplayConfiguration *headerConfig;
    QHash<QString, headerChannel> auxChanRefTable;

    //Stream Configuration container;
    streamConfiguration *streamConfig;

    //NTrode Containers;
    SpikeConfiguration             *spikeConfig;
    QList<SingleSpikeTrodeConf>    nTrodeConfigs;
    QList<HardwareChannelSettings> hardwareChanConfigs;
    QHash<QString, int>            availableTags;
    QHash<QString, int>            activeFilters;

    //deviceinfo Container
    QList<DeviceInfo>              deviceInfoConfigs;

    //ModuleConfiguration container
    ModuleConfiguration            *moduleConfig;
    QList<SingleModuleConf>        moduleConfigs;

    //Benchmark Configuration container
    BenchmarkConfig *benchmarkConfig;


    QLabel                         *labelPlaceholder;

    TrodesConfiguration loadedWorkspace;
protected:
    void resizeEvent(QResizeEvent *ev);

private:
    Mode mode;
    HeadStageType hsType;

    bool unsavedChanges; //this boolean tracks if any unsaved changes have been made to the current configuration
    bool loadingXML;
    ColorGenerator colorGenerator;
    QTreeWidgetItem *lastNTrode;

    enum GUI_Arrangment {A_Vertical, A_Horizontal };


    QHash<QString, int> getTopLevelHash(QTreeWidget *tree);
    bool parseDeviceProfiles(void);

    void updateHeadstageTypeFromWorkspace();

    //Initialize Tab Functions
    void iniIntroTab(void);
    void iniAuxConfigTab(void);
    //void iniSpikeConfigTab(void);
    void iniChannelMappingTab(void);
    void iniReferenceGroupsTab();
    void iniModuleConfigTab(void);
    void iniDiagnosticsTab();

    //Initialize Intro Tab Elements Functions
    void iniGlobalConfigGroup(QBoxLayout *masterLayout);
    void iniHardwareConfigGroup(QBoxLayout *masterLayout);
    void iniStreamConfigGroup(QBoxLayout *masterLayout);

    //Initialize Auxiliary Tab Elements Functions
    void iniAuxChannelSettingsPanel(QBoxLayout *masterLayout);
    void iniAuxListLayout(QBoxLayout *masterLayout);

    //Initialize Spike Tab Elements Functions
    //void iniSpikeConfigDefaultGroup(QBoxLayout *masterLayout, int arranged = A_Horizontal);
    void iniAddNTrodeGroup(QBoxLayout *masterLayout);
    void iniFilterBar(QBoxLayout *masterLayout); //DEPRECIATED: New grouping tag system makes this function obsolete
    void iniListLayout(QBoxLayout *masterLayout);
    void iniHardwareChannelPanel(QBoxLayout *masterLayout);
    void iniTagPannel(QBoxLayout *masterLayout);

    //Initialize Module Tab Elements Functions
    void iniAddModuleGroup(QBoxLayout *masterLayout);
    void iniLowerLayout(QBoxLayout *masterLayout);

    void connectTabElements(void);

    void replaceTab(int index, QWidget *page, QString name);

    //common GUI manipulation functions
    void setWidgetTextPaletteColor(QWidget *widget, QColor newColor);
    void setWidgetTextPaletteColor(QCheckBox *widget, QColor newColor);



public slots:
    void fileSizeCheckboxClicked(bool state);
    void fileSizeEditingFinished();

    static void setComboBoxFirstItemUnselectable(QComboBox *comboBox);

    QString askToSaveUnsavedChanges(void);
    bool saveToXML(QString filename = "", bool isTempSave = false);
    QString saveAsToXML(void);
    void loadFromXML(QString filename);
    void loadWorkspaceToDisplay();
    void loadDefaultUnconfigured();
    void clearGUI(void);

    void setForReconfigure(void);

    void buttonLoadPressed(void);
    void setTabOneToGeneralView(void);

    void insertAvailableTag(QString tag, int value = 1);
    void clearAvailableTags(void);

    void setHardwareChannelHighlight(int HWchan);
    void setChannelMap(QList<int> channelMap);
    void fillChannelSetttings(TrodesConfiguration *inputConf);

    void prepConfigsForSave(void);
private slots:
    //this function is called whenever the configuration is changed to track unsaved changes
    void configChanged(void);
    void samplingRateChanged(QString rate);
    void channelMapChangedInList();
    void channelMapChangedInTable();
    void headstageTypeChanged(int index);
    void numNeuropixelsProbesChanged(int index);

    void updateTitle();

    void runUSBStreamDiagnostics();
    void runEthStreamDiagnostics();
    void runDiagnostic(QByteArray& data, HeadstageSettings& hsSettings, HardwareControllerSettings& cnSettings);


    void displayDiagnosticOutput(QString msg);


    //prep config values for save

    //void addNTrodesToList(void); //move this fun down
    void initializeDefaults(void); //function to initialize all GUI defaults (to do: port all default initializations to here)

    //Global Config Slots
    void setGlobalConfigValues(void);
    void loadGlobalConfigIntoGUI(void);

    void clickedEditFilePath(QString lineEditText);
    void lfpRateValueChanged(int newIndex);

    //Hardware Config Slots
    void setHardwareConfigValues(void);
    void loadHardwareConfigIntoGUI(void);
    void channelNumberSelected(int index);
    void setHardwareChannels(int numChannels);
    void clickedEditSamplingRate(QString samplingRate);
    void addDevice(void);
    void removeDevice(void);

    //Stream Config Slots
    void setStreamConfigValues(void);
    void getBackgroundColorDialog(void);
    void loadStreamConfigIntoGUI(void);

    void setStreamDisplayColumns(void);
    void setStreamDisplayPages(void);

    //Auxiliary Config(tab) Slots
    void getColorDialog(void);
    void clearAuxGUI(void);
    void loadHeaderConfigIntoGUI(void);

    void selectedAuxChannel(QTreeWidgetItem* selectedItem, QTreeWidgetItem* prevItem);
    void clickedAuxChannel(QTreeWidgetItem* clickedItem, int clickedIndex); //depreciated by 'selectedAuxChannel'
    void loadAuxInfoChannelIntoPanel(QString deviceName, QString chanID, bool force = false);

    //The checkAuxChannelsForConflicts() function checks all selected aux channel conflicts by calling the checkSelectedAuxChannelForConflicts function
    bool checkAuxChannelsForConflicts(void);
    bool checkSelectedAuxChannelForConflicts(headerChannel selectedChan);
    void resetAuxPanelLabelColor(void);

    void addDeviceToAuxChannelList(DeviceInfo deviceToAdd);
    void removeDeviceFromAuxChannelList(QString deviceName);
    void addAvailableChannelToList(QString deviceName, QString id);
    void removeConfiguredChannel(int parentIndex, int itemIndex);
    void addAuxChannelToConfiguredTree(QString deviceName, QString id, bool loadFromXML = false);
    void setConfiguredChannelValues(QString chanID);
        //save bindings
    void auxSaveMaxDisplay(void);
    void auxSaveChannelColor(void);

    void auxSetApplyButtonsEnabled(bool enabled = true);
    void auxCancelButtonPressed(void);
    void auxApplyButtonPressed(void);
    void auxApplyToAllButtonPressed(void);

    void auxAddChannelButtonPressed(void);
    void auxRemoveChannelButtonPressed(void);
    void auxRemoveDevice(QString devicename);


    //Spike Config(Tab) Slots
    void setNTrodeConfigValues(void);
    void loadNTrodeConfigIntoGUI(void);
    //void convertNTrodeRefToIDs(void);
    void convertNTrodeIDsToRef(void);
    //void addedNTrode(int ID, bool loadedFromXML);
    //void removedNTrode(int index);

    void autopopulateButtonPressed();
    void buttonLoadChannelMapPressed();
    void buttonExportChannelMapPressed();

    void addListItemsButtonPressed(void);
    QStringList aquireSelectedChannels(void);
    QTreeWidgetItem* aquireDestinationNTrode(void);
    //void addChannelsToNTrode(QStringList channels, QTreeWidgetItem* destNTrode);
    void setFocusedWiget(int focusTarget);

    //void resetNTrodePanelLabelColor(void);
    //void loadNTrodeSettingsIntoPanel(int nTrodeIndex, bool force = false);
    /*void saveNTrodeSettings(int nTrodeIndex, QString *LFPerrorMessage = 0);
        //Save bindings
    void nTrodeSaveSpikeFilter(void);
    void nTrodeSaveLowFilter(void);
    void nTrodeSaveHighFilter(void);
    void nTrodeSaveDigitalRef(void);
    void nTrodeSaveLFPRef(void);
    void nTrodeSaveRefNTrodeID(void);
    void nTrodeSaveRefNTrodeChan(void);
    void nTrodeSaveLFPChannel(void);
    void nTrodeSaveLFPHighFilter(void);
    void nTrodeSaveLFPFilter(void);
    void nTrodeSaveThresh(void);
    void nTrodeSaveMaxDisp(void);
    void nTrodeSaveChanColor(void);
    void nTrodeSaveEnableTrigger(void);
    void nTrodeSaveTags(void);

    void loadLFPSettingsIntoPanel(int nTrodeIndex);

    void getChanColorDialog(void);

    //checkNTrodesForConflicts calls 'checkSelectedNtrodesForConflicts' and automatically checks all nTrodes
    bool checkNTrodesForConflicts(void);
    bool checkSelectedNTrodesForConflicts(SingleSpikeTrodeConf checkedNTrode);

    void checkDigitalRefs(int removedNTrode);
    void checkDigitalRefs(int originNTrodeID, int removedChanID);
    void setDigChannelBoxNum(int nTrodeIndex);
    void addCategoryToDict(QString newCategory);
    void addTagToDict(QString category, QString newTag);
    void processTagSelectionBox(int selectedIndex);*/

//    void processOperationSelector(void);
    /*void processFilterSelector(void); //DEPRECIATED: New grouping tag system makes this function obsolete
    void applyFilters(void); //DEPRECIATED: New grouping tag system makes this function obsolete
    void clearFilters(void); //DEPRECIATED: New grouping tag system makes this function obsolete
    void updateFilterSelector(void); //DEPRECIATED: New grouping tag system makes this function obsolete
*/
    /*
    void setHardwareEditViewToChannel(int chanIndex);

    void loadChannelSettingsIntoPanel(int chanIndex);
    void saveChannelPannelSettings(int chanIndex);
    */
    void nTrodeSetApplyToAllButtonEnabled(bool enabled = true);
    void nTrodeSetApplyButtonsEnabled(void);
    void nTrodeCancelButtonPressed(void);
    void nTrodeApplyButtonPressed(void);
    void nTrodeApplyToAllButtonPressed(void);
    void nTrodeAddbuttonPressed(void);

    //Reference group tab slots
    void loadRefGroupsIntoGUI();
    void setRefGroupConfigValues();

    //Module Config(Tab) Slots
    void setModuleConfigValues(void);
    void loadModuleConfigIntoGUI(void);
    void processModulePullDownMenuIndex(int index);
    void addEditModuleToConfig(void);
    QTreeWidgetItem* addModuleToTree(SingleModuleConf module);
    void addArgument(void);
    void addArgumentToTree(QTreeWidgetItem *parentItem, QString flag, QString value);
    void editModuleArgument(void);
    void removeModuleArgument(void);

    //workspace verification slots
    bool verifyCurrentWorkspace(QString *errorMsg = 0);
    bool verifyHardwareConfiguration(QString *errorMsg = 0);
    bool verifyNTrodeConfiguration(QString *errorMsg = 0);
    bool verifyRefGroupConfiguration(QString *errorMsg = 0);

    //debug
    void printModuleConfigs(void);
    void printDeviceInfoConfigs(void);
    void printNTrodeSettings(bool printHWChannels = true);
    void printAvailableTags(void);

signals:
    void hardwareChannelsInitialized(int numChannels);
    void updatedAvailTags(QHash<QString,int> newTagHash);
    void newTitle(QString title);


};



#endif // WORKSPACEEDITOR_H

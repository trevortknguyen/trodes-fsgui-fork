


TARGET = stateScript
include(../module_defaults.pri)

QT       += opengl widgets xml serialport network

INCLUDEPATH  += ../../Trodes/
INCLUDEPATH  += ../../Trodes/src-main
INCLUDEPATH  += ../../Trodes/src-config
INCLUDEPATH  += ../../Trodes/src-display
INCLUDEPATH  += ../../Trodes/src-network
INCLUDEPATH  += ../../Libraries

# trodesnetwork: includes, libs
include(../../trodesnetwork_defaults.pri)

#########################################
#Source files and libraries
#########################################
# Use Qt Resource System for images, etc
RESOURCES += \
    $$PWD/../../Resources/Images/buttons.qrc

HEADERS += mainwindow.h \
        console.h \
        scripteditor.h \
        highlighter.h \
        localcallbackwidget.h \
        dialogs.h \
        fileselectorwindow.h \
        ../../Trodes/src-main/trodesSocket.h \
        ../../Trodes/src-config/configuration.h \
        ../../Trodes/src-main/trodesdatastructures.h \
        ../../Trodes/src-main/eventHandler.h \
        ../../Trodes/src-display/sharedtrodesstyles.h \
        ../../Libraries/qtmoduleclient.h \
        ../../Libraries/qtmoduledebug.h \
        ../../Trodes/src-network/TrodesCentralServer.h

SOURCES += main.cpp\
        mainwindow.cpp \
        console.cpp \
        scripteditor.cpp \
        highlighter.cpp \
        localcallbackwidget.cpp \
        dialogs.cpp \
        fileselectorwindow.cpp \
        ../../Trodes/src-main/trodesSocket.cpp \
        ../../Trodes/src-config/configuration.cpp \
        ../../Trodes/src-main/trodesdatastructures.cpp \
        ../../Trodes/src-main/eventHandler.cpp \
        ../../Trodes/src-display/sharedtrodesstyles.cpp \
        ../../Libraries/qtmoduleclient.cpp \
        ../../Libraries/qtmoduledebug.cpp \
        ../../Trodes/src-network/TrodesCentralServer.cpp
 
unix:!macx {
    INCLUDEPATH += ../../Trodes/Libraries/Linux
    LIBS += -L../../Trodes/Libraries/Linux
    HEADERS    += ../../Trodes/Libraries/Linux/WinTypes.h \
                  ../../Trodes/Libraries/Linux/ftd2xx.h
    LIBS       += -lftd2xx
}

win32 {
    RC_ICONS += trodesWindowsIcon_ss.ico
    INCLUDEPATH += $$TRODES_REPO_DIR/Libraries/Windows64/include
    LIBS += -L$$TRODES_REPO_DIR/Libraries/Windows64/lib/ -lTrodesNetwork

    INCLUDEPATH += $$TRODES_REPO_DIR/Trodes/Libraries/Windows
    HEADERS     += $$TRODES_REPO_DIR/Trodes/Libraries/Windows/ftd2xx.h
    LIBS        += $$quote($$TRODES_REPO_DIR/Trodes/Libraries/Windows/FTDI/amd64/ftd2xx64.lib)
}

macx {
    ICON        = trodesMacIcon_ss.icns
    QMAKE_INFO_PLIST += Info.plist

    INCLUDEPATH += $$TRODES_REPO_DIR/Trodes/Libraries/Mac
    HEADERS    += $$TRODES_REPO_DIR/Trodes/Libraries/Mac/WinTypes.h \
                    $$TRODES_REPO_DIR/Trodes/Libraries/Mac/ftd2xx.h

    LIBS       += $$quote($$PWD/../../Trodes/Libraries/Mac/libftd2xx.a) -framework Cocoa


    INCLUDEPATH += ../../Libraries/MacOS/include
    LIBS += -L../../Libraries/MacOS/lib -lTrodesNetwork
    QMAKE_POST_LINK += "cp $$TRODES_REPO_DIR/Libraries/MacOS/lib/libTrodesNetwork.*.dylib $$DESTDIR/$${TARGET}.app/Contents/MacOS/"
#    LIBS += -L$$TRODES_REPO_DIR/Trodes/Libraries/Mac -lftd2xx.1.4.4
}


#########################################
#Install
#########################################
unix:!macx{
    #FTDXX library
    #libraries.path is set in build_defaults.pri
    libraries.files += $$TRODES_REPO_DIR/Trodes/Libraries/Linux/libftd2xx.so*
#    libraries.files += $$TRODES_REPO_DIR/Libraries/Linux/TrodesNetwork/libTrodesNetwork.so*
    QtDeploy.commands += cp -a $$TRODES_REPO_DIR/Libraries/Linux/TrodesNetwork/lib/libTrodesNetwork.so* $$INSTALL_DIR/Libraries/
}


INSTALLS += libraries
#########################################
#          OLD
##########################################
##Directories that need to be copied to the install dir
#MATLABSCRIPTDIR = $$PWD/matlabObserver/*
#MATLABDESTDIR = $$DESTDIR/MatlabStateScriptObserver
#win32:MATLABSCRIPTDIR ~= s,/,\\,g
#win32:MATLABDESTDIR ~= s,/,\\,g
##mscripts.commands = $(MKDIR) $$MATLABDESTDIR $$COMMAND_SEP
##mscripts.commands += $(COPY) $$MATLABSCRIPTDIR $$MATLABDESTDIR
##QMAKE_EXTRA_TARGETS += mscripts
##POST_TARGETDEPS += mscripts

#PYTHONSCRIPTDIR = $$PWD/pythonObserver/*
#PYTHONDESTDIR = $$DESTDIR/PythonStateScriptObserver
#win32:PYTHONSCRIPTDIR ~= s,/,\\,g
#win32:PYTHONDESTDIR ~= s,/,\\,g
##pscripts.commands = $(MKDIR) $$PYTHONDESTDIR $$COMMAND_SEP
##pscripts.commands += $(COPY) $$PYTHONSCRIPTDIR $$PYTHONDESTDIR
##QMAKE_EXTRA_TARGETS += pscripts
##POST_TARGETDEPS += pscripts




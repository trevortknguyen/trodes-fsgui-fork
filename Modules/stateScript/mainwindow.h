/*
   Trodes is a free, open-source neuroscience data collection and experimental control toolbox

   Copyright (C) 2012 Mattias Karlsson

   This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui>
#include <QtSerialPort/QSerialPort>
#include <QtWidgets>
#include <QtNetwork>
#include "console.h"
#include "scripteditor.h"
#include "localcallbackwidget.h"
#include "fileselectorwindow.h"
#include "dialogs.h"
#include "configuration.h"
#include "qtmoduleclient.h"
#include "TrodesCentralServer.h"
#include "trodesSocket.h"


#include "highfreqclasses.h"

#include <TrodesNetwork/Resources/ServiceProvider.h>



#ifdef WIN32
    #include <windows.h>
    #include "ftd2xx.h"
#endif
#ifdef __APPLE__
    #include "WinTypes.h"
    #include "ftd2xx.h"
#endif
#ifdef linux
    #include "WinTypes.h"
    #include "ftd2xx.h"
#endif

#define VENDOR 0x0403
#define DEVICE 0x6010
extern FT_STATUS res;
extern FT_HANDLE ftdi;

//A four bit value descibes the modules current operation mode:
#define STATESCRIPTMODULE_STANDALONEMODE 1 //StateScript module functions as master
#define STATESCRIPTMODULE_SLAVEMODE 2

#define STATESCRIPTMODULE_DIRECTCONNECTION 8 //communicate directly to harware via serial
#define STATESCRIPTMODULE_TRODESCONNECTION 16 //Trodes handles the communication
extern GlobalConfiguration *globalConf;
extern HardwareConfiguration *hardwareConf;
extern NTrodeTable *nTrodeTable;
extern streamConfiguration *streamConf;
extern SpikeConfiguration *spikeConf;
extern headerDisplayConfiguration *headerConf;
extern ModuleConfiguration *moduleConf;
extern NetworkConfiguration *networkConf;

class Style_tweaks : public QProxyStyle
{
    public:

        void drawPrimitive(PrimitiveElement element, const QStyleOption *option,
                           QPainter *painter, const QWidget *widget) const
        {
            /* do not draw focus rectangles - this permits modern styling */
            if (element == QStyle::PE_FrameFocusRect)
                return;

            QProxyStyle::drawPrimitive(element, option, painter, widget);
        }
};

class CommandInput : public QLineEdit
{
    Q_OBJECT

public:
    CommandInput(QWidget *parent = 0);
    void eraseAndStore();

protected:

    void keyPressEvent(QKeyEvent *event);
private:
    QStringList commandHistory;
    int currentCommandHistoryIndex;

};


struct NetworkStatescriptCommand {
    // field impl command
    std::string command;
    MSGPACK_DEFINE_MAP(command);
};

class StateScriptClient : public QtModuleClient{
    Q_OBJECT
public:
    StateScriptClient(QString serveraddress, int port)
        : QtModuleClient("StateScript", serveraddress.toStdString().c_str(), port){
            std::shared_ptr<StateScriptClient> client(this);
            auto script_subscriber_thread = std::thread([ address = serveraddress.toStdString(), port = port, client ]() {
                trodes::network::ServiceProvider<NetworkStatescriptCommand, std::string> service(address, port, "statescript.service");
                while(true) {
                    service.handle([ client ](NetworkStatescriptCommand input) -> std::string {
                        auto command = QString::fromStdString(input.command);
                        emit client->sig_statescriptCommand(command);
                        return "success";
                    });
                }
            });
            script_subscriber_thread.detach();
        }
    ~StateScriptClient(){}

    int processOtherMsg(const char *sender, const char *subject, TrodesMsg &msg){
        if(!strcmp(subject, "StatescriptCommand")){
            QString command = QString::fromStdString(msg.popstr());
            emit sig_statescriptCommand(command);
        }
        else if(!strcmp(subject, "PythonCommand") || !strcmp(subject, "MatlabCommand")){
            QString command = QString::fromStdString(msg.popstr());
            emit sig_cblanguageCommand(command);
        }

        return 0;
    }

signals:
    void sig_statescriptCommand(QString);
    void sig_cblanguageCommand(QString);
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QStringList options, QWidget *parent = 0);
    ~MainWindow();
    QMenu*      menuFile;
    //QMenu *menuScripts;
    QMenu*      menuLogFiles;
    QAction*    actionCreateLogFile;
    QAction*    actionCloseLogFile;

    QMenu*      menuFileFolders;
    QAction*    actionChangeStateScriptFolder;
    QAction*    actionChangeLocalScriptFolder;
    QAction*    actionMacros;


    QMenu*      menuEdit;
    QMenu*      menuHelp;
    QAction*    actionUndo;
    QAction*    actionRedo;
    QAction*    actionCut;
    QAction*    actionCopy;
    QAction*    actionPaste;
    QAction*    actionAbout;

    QAction*    actionSelectLocalLanguage;

    uint32_t currentTimeStamp;

    void retranslateUi()
    {
        //setWindowTitle(QApplication::translate("Main", "stateScript", 0));


        menuFile->setTitle(QApplication::translate("Main", "File", 0));
        //menuScripts->setTitle(QApplication::translate("Main", "Scripts", 0));
        menuLogFiles->setTitle(QApplication::translate("Main", "Log file", 0));
        actionCreateLogFile->setText(QApplication::translate("Main", "Create new...", 0));
        actionCloseLogFile->setText(QApplication::translate("Main", "Close", 0));
        menuFileFolders->setTitle(QApplication::translate("Main", "Script folders", 0));
        actionChangeStateScriptFolder->setText(QApplication::translate("Main", "Protocols", 0));
        actionChangeLocalScriptFolder->setText(QApplication::translate("Main", "Callbacks", 0));
        actionMacros->setText(QApplication::translate("Main", "Macros...", 0));


        menuEdit->setTitle(QApplication::translate("Main", "Edit", 0));
        actionUndo->setText(QApplication::translate("Main", "Undo", 0));
        actionRedo->setText(QApplication::translate("Main", "Redo", 0));
        actionCut->setText(QApplication::translate("Main", "Cut", 0));
        actionCopy->setText(QApplication::translate("Main", "Copy", 0));
        actionPaste->setText(QApplication::translate("Main", "Paste", 0));
        actionSelectLocalLanguage->setText(QApplication::translate("Main", "Observer language...", 0));
    }

private:


    TrodesConfiguration loadedWorkspace;

    //For writing to USB with d2xx driver
    bool                   FTDIInit();
    void                   closeFTDI();
    bool                   FTDIWrite(const QByteArray &data);
    bool FTDI_Initialized;
    bool FTDI_connected;
    QTimer FTDIReadTimer;
    TrodesModuleNetwork*   trodesNet; //

    //For sending data via UDP socket
    QUdpSocket*            udpSocket;
    QUdpSocket*            udpReturnSocket;
    bool                   udpSocketInit();
    void                   closeUdpSocket();
    bool                   udpSocketWrite(const QByteArray &data);
    bool udpSocketInitialized;

    //The time info from Trodes (for slave mode)
    quint32 trodesTimeRate;
    quint32 currentTrodesTime;
    QTimer getCurrentTime_timer;

    QString fileNameFromTrodes;

    void                   enableConsole();

    QTabWidget*            scriptTabs;
    QGridLayout*           mainLayout;
    QSplitter*             panelSplitter;
    QToolBar*              toolBar;

    QStatusBar*            statusbar;
    QPushButton*           tcpPortButton;
    TrodesButton*           controllerButton;
    TrodesButton*           cameraButton;
    TrodesButton*           sendScriptButton;
    TrodesButton*           clearAllButton;
    TrodesButton*           startLocalLanguageButton;
    QLabel*                timeLabel;
    QLabel*                fileLabel;
    TrodesModuleNetwork    *moduleNet;
    Console*               serialConsole;
    FileSelectorWindow*    fileSelector;
    //scriptEditor*          editorWindow;
    localCallbackWidget*   localCallbackController;
    QGroupBox*              commandBox;
    QGridLayout*            commandLayout;
    CommandInput*           commandInput;

    QString currentScriptFile;

    QString currentTcpAddress;
    quint16 currentTcpPort;

    bool unSavedData;
    bool scriptFileSelected;
    bool localFileSelected;
    //bool                   fileHasName;

    QSerialPort            *serial;
    QString currentSerialPort;
    bool isSerialConnected;
    TrodesClient*          tcpClient;
    bool isTcpClientConnected;
    QTimer compileCheckTimer;
    QTimer                *clockUpdateTimer;
    int numCompileChecks;
    bool gotCompileSignal;
    TrodesServer           *cameraServer;
    bool cameraModuleConnected;
    QProcess*              cameraProgram;

    QString stateScriptFolder;
    QString localScriptFolder;

    QString localCallbackLanguage;
    QString localCallbackLanguageLocation;
    bool localLanguageConnected;

    bool logFileOpen;
    QFile logFile;
    QString logFileFolder;
    bool isRecording;

    bool waitingForScriptSend;
    QVector<int> ignoreUpdatePorts;

    //Standalone mode? Use USB or go through MCU via Ethernet?
    unsigned char currentOperationMode;

    macroDialog     *macroDlog;
    TrodesButton    *macroButton[NUM_MACROS];
    QLabel          *macroLabel;
    QStringList     macroDescriptions,macroSnippets;

    QString serveraddress;
    int serverport;

    StateScriptClient *networkClient;
    bool startTrodesClient(QString serveraddress, int port);
    void endTrodesClient();

    CentralBroker *broker;
    TrodesCentralServer *server;
    HighFreqPub *timestampspub;
    QProcess* startSingleModule(SingleModuleConf s, QProcess *moduleProcess);
protected:
    void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent *);
    void moveEvent(QMoveEvent *);
    void keyPressEvent(QKeyEvent *event);

    //void paintEvent(QPaintEvent *);

private slots:

    void about();
//    void tcpPortButtonPressed();
    void controllerButtonPressed();
    void setTcpClientConnected();
    void cameraButtonPressed();
    void languageButtonPressed();
    void startCameraModule(QString path, QStringList arguments);
    void stopCameraModule();
    void cameraModuleStarted();
    void cameraModuleEnded();
    void setTcpClientDisconnected();
    void sendCompileCommand();
    void updateTime();
    void openStateScriptFolderSelector();
    void openLocalCallbackScriptFolderSelector();
    void openLanguageSelector();
    void getAcquisitionCmd(QString cmd, uint32_t time);
    void turnRecordingOn();
    void turnRecordingOff();
    void enableLanguageMenu();
    void setLocalCallbackLanguage(QString language, QString path);
    void scriptSendFinished();
    void autoSourceConnect(QString source);
    void setTimeRate(quint32);
    void setTimeFromTrodes(quint32);
    void askTimeFromTrodes();
    void setFileNameFromTrodes(QString);
    void setSSTextFromTrodes(QString);
    void openMacroDialog();
    void updateMacros();
    void executeMacro();
    void getSavedMacros();

public slots:
    void tcpConnect(QString addressIn, quint16 portIn);
    void tcpDisconnect();
    void displaySocketError(QString errMsg);
    void receiveMessageFromTcpServer(QString msg);
    void createLogFile();
    void createLogFile(QString);
    void insertFileText(QString filename, QString filepath);
    void closeLogFile();
    void openSerialPort(QString serialPortIn);
    void closeSerialPort();
    void sendScript();
    void sendScriptCmd(QString);
    void sendClearAll();
    void writeData(const QByteArray &data);
    void writeData(const QString &data);
    void readData();
    void FTDIRead();
    void udpSocketRead();
    void udpReturnSocketRead();
    void stateScriptFileSelected();
    void localScriptFileSelected();
    void sendCommand();

signals:

    void closeDialogs();
    void serialConnected();
    void newStateScriptFolder(QString);
    void newLocalCallbackFolder(QString);
    void logFileOpened(QString);
    void logFileClosed();
    void recordingTurnedOn();
    void recordingTurnedOff();
};
#endif // MAINWINDOW_H

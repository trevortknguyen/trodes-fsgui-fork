#include "customApplication.h"
#include <iostream>
#ifndef _WIN32
#include <unistd.h>
#endif
//This class inherits QApplication, and is used to process
//the command line arguments or OS events such as double clicking
//on a file associated with this program

customApplication::customApplication( int & argc, char **argv )
    : QApplication(argc, argv)
{
    win = new MainWindow();
    for (int i = 1; i < argc; i++) {
        win->openScriptFile(argv[i]);
    }

    //pNot = new QSocketNotifier(STDIN_FILENO, QSocketNotifier::Read, this);
    //connect(pNot, SIGNAL(activated(int)), this, SLOT(stdinEvent()));
    //pNot->setEnabled(true);

    win->show();
}
//
customApplication::~customApplication()
{
}
//

void customApplication::stdinEvent() {



        QTextStream stream(stdin, QIODevice::ReadOnly);
        bool endOfMessage = false;
        QString str;
        while(!endOfMessage) {
            //reading stdin has weird blocking behavior, so we read one character at a time
            QString tmpstr = stream.read(1);
            if (tmpstr == "\n") {
                endOfMessage = true;
            } else {
                str += tmpstr;
            }
        }

        loadFile(str);



}

void customApplication::loadFile(const QString &fileName)
{
    win->raise();
    win->openScriptFile(fileName);

}
bool customApplication::event(QEvent *event)
{
        switch (event->type()) {
        case QEvent::FileOpen:
            loadFile(static_cast<QFileOpenEvent *>(
                     event)->file());
            return true;
        default:
            return QApplication::event(event);
    }
}



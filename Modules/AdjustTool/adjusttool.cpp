#include "adjusttool.h"

AdjustTool::AdjustTool(QStringList arguments, QWidget* parent) : QObject(parent)
{
    moduleNet = new TrodesModuleNetwork();

    int optionInd = 1;
    trodes = false;
    configFileName = "";
    backendPath = "";

    while (optionInd < arguments.length()) {
        if ((arguments.at(optionInd).compare("-trodesConfig", Qt::CaseInsensitive) == 0) && (arguments.length() > optionInd + 1)) {
            nsParseTrodesConfig(arguments.at(++optionInd));
            qDebug() << "AdjustTool parsing trodesConfig file " << arguments.at(optionInd);
            trodes = 1;
        }
        else if ((arguments.at(optionInd).compare("-serverAddress", Qt::CaseInsensitive) == 0) && (arguments.length() > optionInd + 1)) {
            moduleNet->trodesServerHost = arguments.at(++optionInd);
            trodes = 1;
        }
        else if ((arguments.at(optionInd).compare("-serverPort", Qt::CaseInsensitive) == 0) && (arguments.length() > optionInd + 1)) {
            moduleNet->trodesServerPort = (quint16)(arguments.at(++optionInd).toInt());
        }
        else if ((arguments.at(optionInd).compare("-AdjustBackend", Qt::CaseInsensitive) == 0) && (arguments.length() > optionInd + 1)) {
            fsDataPath = arguments.at(++optionInd);
        }
        else if ((arguments.at(optionInd).compare("-config", Qt::CaseInsensitive) == 0) && (arguments.length() > optionInd + 1)) {
            configFileName = arguments.at(++optionInd);
        }
        optionInd++;
    }

    if ((moduleNet->trodesServerPort == 0) && (trodes == 1)) {
        // try setting this from the network config file
        moduleNet->trodesServerHost = networkConf->trodesHost;
        moduleNet->trodesServerPort = networkConf->trodesPort;
    }
    else if (trodes == 0) {
        qDebug() << "Error: FSGui must be run with trodes";
        exit(1);
    }

}


/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FILESOURCETHREAD_H
#define FILESOURCETHREAD_H



#include <QtGui>
#include <QGLWidget>
#include <QThread>
#include <QTimer>
#include <QElapsedTimer>
#include "configuration.h"
#include "sharedVariables.h"
#include "abstractTrodesSource.h"

class fileSourceRuntime: public AbstractSourceRuntime
{
    Q_OBJECT
public:
  fileSourceRuntime(QObject *parent);
  ~fileSourceRuntime();
  //void run();
  //bool quitNow;
  //bool aquiring;
  bool jumpToBeginning;
  bool waitForThreadsFlag;
  QFile *file;
  QVector<char> buffer;
  void calculateChannelInfo();
  bool loopFinished;

private:




  QElapsedTimer stopWatch;

  qint64 mSecElapsed;
  int currentSampleNum;

  int numChannelsInFile;
  //int filePacketSize;
  QVector<int> channelPacketLocations_input;
  QVector<int> channelPacketLocations_output;



  //void generateData();
  //int16_t  sourceData[5000];
  //int   waveRes;
  int32_t  currentTimestamp;
  //int waveAmpInternal;
  //float samplesPerCycle;
  //int currentCyclePosition;
  //float cyclePositionCarryOver;


public slots:
  void Run(void);
  //void endThread(void);

signals:

  void endOfFileReached(void);

private slots:
  //void pullTimerExpired();

};


class fileSourceInterface : public AbstractTrodesSource {
  Q_OBJECT

public:
  fileSourceInterface(QObject *parent);
  ~fileSourceInterface(void);
  int state;
  bool filePaused;

private:
  fileSourceRuntime* acquisitionThread;



public slots:

  void InitInterface(void);
  void StartAcquisition(void);
  void StopAcquisition(void);
  void CloseInterface(void);

  void PauseAcquisition(void);
  void waitForThreads(void);

private slots:


};


#endif // FILESOURCETHREAD_H

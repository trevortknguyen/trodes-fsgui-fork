/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef carrierTHREAD_H
#define carrierTHREAD_H

#include <QtGui>
#include <QGLWidget>
#include <QThread>
#include <QTimer>
#include <QElapsedTimer>
#include "configuration.h"
#include "../../Trodes/src-main/sharedVariables.h"
#include "abstractTrodesSource.h"


#ifdef WIN32
    #include <windows.h>
    #include "C:\Program Files (x86)\National Instruments\NI-DAQ\DAQmx ANSI C Dev\include\NIDAQmx.h"
#endif

class carrierRuntime: public AbstractSourceRuntime
{
    Q_OBJECT
public:
    explicit carrierRuntime(QObject *parent = 0);
    ~carrierRuntime();
    QList<double>  waveFrequency_1;
    QList<double>  waveFrequency_2;

    bool    loopFinished;

private:

#ifdef WIN32
    TaskHandle taskHandle;
#endif

    void    generateData();

public slots:

    void Run(void);

};


class carrierInterface : public AbstractTrodesSource {
  Q_OBJECT

public:
  carrierInterface(QObject *parent);
  ~carrierInterface(void);
  int state;

private:
  carrierRuntime* acquisitionThread;

public slots:

  void InitInterface(void);
  void StartAcquisition(void);
  void StopAcquisition(void);
  void CloseInterface(void);

private slots:

};

#endif // carrierTHREAD_H

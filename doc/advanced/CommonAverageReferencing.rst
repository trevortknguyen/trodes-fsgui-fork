Common Average Referencing
==========================

Common Average Referencing (CAR) is a referencing technique that can
produce better results than single channel referencing. As of version
1.8.0, Trodes supports creating CAR groups that any nTrode can reference
from. These groups can be composed of up to 16 channels from any
configuration of ntrodes/channels, and you can have up to 32 different
groups.

Opening the group panel
-----------------------

When Trodes is open to a workspace, click on the **nTrode** button to
open the **nTrode Settings** panel. On the top right, click on the
button for **CAR groups** and another panel should open up.

.. figure:: https://bitbucket.org/repo/d7xGjL/images/2260308075-cargroupsbutton1.png
   :alt: cargroupsbutton1.png

   cargroupsbutton1.png

Using the CAR groups panel
--------------------------

1. Select a group (left side)

   -  Select which group you would like this nTrode to reference. If you
      would like for it to continue to use single channel referencing,
      select the first group **None**. Settings update automatically and
      in real time when you select groups.

2. Edit a group (right side)

   -  You can edit what channels each group consists of by clicking on
      the **Edit** button on the right, making your changes, and
      clicking apply. You cannot change the group name (Group 1, Group
      2, etc), but you can add a description to each group in the text
      edit box.

3. Add a new group (bottom)

   -  Finally, to add a group, you can click the **Add group** button on
      the bottom.

.. figure:: https://bitbucket.org/repo/d7xGjL/images/1638396341-car_dialog.png
   :alt: car_dialog.png

   car_dialog.png

Applying to multiple nTrodes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As with the other settings, you can apply changes to multiple nTrodes at
once by CTRL+Click or SHIFT+Click the different nTrodes on the stream
display. Any changes you make will be applied to all of them.

4. Apply the reference

Back in the **nTrodes Settings Panel**, you still need to apply
references to nTrodes by checking the boxes **Spike**, **LFP**, or
**Raw**. These boxes turn on references before applying spike filters,
LFP filters, or for raw data.

.. figure:: https://bitbucket.org/repo/d7xGjL/images/416583209-cargroupsbutton2.png
   :alt: cargroupsbutton2.png

   cargroupsbutton2.png

Saving
------

When exiting Trodes, you will be asked if you would like to save these
changes. Until this feature is not in beta anymore, we recommend you
save the changes to a new file, with CAR in the name for clarity.

Exporting
---------

Exports related to spikes, LFP, or raw data will automatically check the
workspace of the .rec file for CAR flags and export with the group
referencing.

If you have a previous recording that you want to apply CAR to, follow
these steps:

1. Open the same workspace you created the recording with in Trodes
2. Edit and apply the groups in the panels
3. Save as a new workspace
4. When exporting, add an extra flag ``-reconfig <new workspace file>``.

   -  This tells the export utility to apply the newer workspace file
      when exporting, which contains the CAR data.

==========
Networking
==========

.. note::
   The Trodes Network API has changed significantly in Version 2.0.

Timestamping and Synchronization
================================

All timestamps sent out should be based on Unix time.
Within C++, use the ``int64_t trodes::network::util::get_timestamp()`` function to
ensure consistency.
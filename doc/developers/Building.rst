========
Building
========

Building with CMake on Linux
============================

CMake is a build system currently being used experimentally in development only.
For stable builds for deployment, please use the QMake build system.

.. note::
   The ``nproc`` command in Linux returns the number of processors available on your system.
   It is being used below to insert that number into the command.

::

   cmake -B build
   cmake --build build --parallel `nproc` --config Debug

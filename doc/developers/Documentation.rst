======================
Building Documentation
======================

The documentation for Trodes is in the ``doc`` directory. It can
be viewed online at https://docs.spikegadgets.com. The following instructions are
for building and viewing locally on a development machine.

Setup
=====

::

    pip install sphinx
    pip install sphinx_rtd_theme

Running
=======

This is only supported on Linux or Mac.

::

    cd doc
    make html

Now you can view the HTML files generated in ``doc/_build``.

Contributing
============

We welcome contributions to our documentation. Documentation is written in ReStructuredText (ReST).
Because ReST is flexible in specifying headers, we have standardized on this format.

:: 

    ==============
    Header level 1
    ==============

    Header level 2
    ==============

    Header level 3
    --------------

    Header level 4
    ~~~~~~~~~~~~~~

    Header level 5
    ^^^^^^^^^^^^^^
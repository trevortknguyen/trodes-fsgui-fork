======================================
Setting up the development environment
======================================


Qt Installation (All platforms)
===============================

The graphical user interface is using Qt. Building Trodes
requires installing Qt.

For all platforms, download the `Qt online installer <https://www.qt.io/download-qt-installer>`__.

Make sure to download the right installer for your platform.

It will prompt you login with a Qt account. You create one and verify your email.

After it finishes loading, it will prompt you to select which packages to install.

Select a custom installation.

Deselect the defaults and only select ``LTS``.

We want the latest ``5.x.x`` version. Under the latest ``5.x.x`` version,
select only:

1. Desktop gcc 64 bit
2. Sources
3. Qt Debug Information Files

.. note::
   For Windows users, it may display Desktop (MSVC) or something similar. Select that.

Leave any packages under "Development tools" selected. Those defaults are fine.

Agree to the licenses and install.


Repository setup
================
The git repository is hosted on Bitbucket. To set up a local clone, do the following steps.

1. ``sudo apt-get install git`` (if you do not already have git installed)

2. ``git clone https://bitbucket.org/mkarlsso/trodes.git``

3. ``git submodule init``

4. ``git submodule update``


Building
========

For the first build, it is best to use Qt Creator, and open up Trodes-all.pro. Make sure you are compiling in Release mode (the default is debug mode). Also, make sure that shadow build is turned off. Then, run qmake from Qt Creator. After that, you can build either from Qt Creator or from the command line:   

1. ``qmake``

2. ``make``

3. ``make install``


Platform-specific instructions
==============================

Linux (Ubuntu)
--------------

Code is tested on Ubuntu 20.04.


Install the following packages before building:

1. ``sudo apt update``

2. ``sudo apt install build-essential``

3. ``sudo apt-get install chrpath`` (used during the build +
   installation process)

4. ``sudo apt-get install mesa-common-dev libglu1-mesa-dev``. (Note that
   this may not actually be necessary; if you are using a graphics card
   and installed its driver, then OpenGL is probably set up already.
   Many manufacturers such as Nvidia have implemented OpenGL for their
   hardware, so installing the appropriate driver will pull in the
   necessary dependencies).

5. ``sudo apt-get install libpulse-dev`` (needed for audio)

6. ``sudo apt-get install libgstreamer-plugins-base0.10-0`` (optional,
   for camera module)

Mac
---

1. `Install Homebrew <https://brew.sh/>`__. Then execute
   ``brew install git``

2. `Go to Apple’s developer
   website <https://developer.apple.com/download>`__. Search for Xcode.
   Download and install that as well as Xcode Command Line Tools. Then
   from the top menu, Xcode->Preferences->Locations. Select the
   appropriate kit for the Command Line Tools.

Windows
-------

1. Trodes now depends on MSVC 2019, so you will need to install MSVC in order to compile. 

2. Qt on Windows uses jom, which is similar to make. We recommend building Trodes_all.pro from Qt Creator on windows.
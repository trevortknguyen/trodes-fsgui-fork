# Trodes Documentation

This directory contains all of the documentation for Trodes.
It is currently deployed on <https://docs.spikegadgets.com>.

## Setting up to build locally

```
pip install -r requirements.txt
```

## Building on Linux/Darwin

```
make html
```

## Building on Windows

Use the `.bat` file.
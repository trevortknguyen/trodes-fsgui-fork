Trodes Export Utility Functions
===============================

Export functions are utilities that will extract and save specific features from a raw recording 
file (``.rec``) into a binary file. All extracted binary files can be read into Matlab using a single
function: ``readTrodesExtractedDataFile.m`` (in the ``TrodesToMatlab`` toolbox), or into Python using 
a single function: ``readTrodesExtractedDataFile.py`` (in the ``TrodesToPython`` toolbox). 

**Note: In Trodes versions before 2.0.0, export functions were separated into different 
command line programs for each data type (exportLFP, exportSpikes, etc.). These functions 
are now deprecated and have been combined into a single export tool called "trodesexport". 
This program can either be run from the command line, or from Trodes when a recorded file is 
opened in playback mode. The following instructions are for use with trodesexport.** 

Trodes Export Utility can run either from within the Trodes interface or from the command 
line. When finished, it will save the resulting ``.dat`` files in a subfolder in the same location 
as the .rec file. The subfolder will be named after the processing type.

Running from Trodes
~~~~~~~~~~~~~~~~~~~
To run the export utility from within Trodes, you will need to open a previously recorded ``.rec`` file in 
playback mode. This can be done by clicking Open Playback File from the main menu of Trodes, then selecting 
Browse... to select the file you would like to play back. Once the file is open:

- Go to File->Extract... 
- This will open a separate window named "Extract data from file"
- On the left side of the window, check the processing mode(s) that you want to run
- Click the "Start" button
- The export process, along with percentage complete, will show in the embedded console window.


Altering Export Settings and Channels Using the Workspace
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The trodesexport application exports data using the display settings (channels, filters, references, 
LFP channel, spike detection thresholds, etc.) embedded in the recording file or in the Workspace file saved to the 
recording directory. This means that the channels exported and export settings applied can be changed by altering 
the workspace. For instance, if exporting only a subset of input/output channels is desired, this 
can be done by removing the unwanted channels from display in the Workspace settings. This will not alter the saved data, 
only determine which channels are visible in Trodes and what settings have been applied to the raw data.

**IMPORTANT NOTE:** When Trodes loads data in Playback Mode, or exports data using the command line, it does so using 
the Workspace file found in the data directory that shares the same name as the data file (e.g. myfile.rec & 
myfile.trodesconf). This Workspace file can be edited in Trodes using the Edit Existing File... option under the 
Create/Edit Workspace menu. If creating a new Workspace file is preferable, the original data file must be moved to 
another directory or deleted and the new Workspace file must be saved to the data directory and have the same filename 
as file being viewed/extracted. 


Command Line Usage
~~~~~~~~~~~~~~~~~~
The trodesexport application can be executed from the command line and doing so offers expanded extraction options 
that are not available when using the Trodes GUI, such as batch processing.

To do this, simply open the command line, set your current directory to the Trodes application folder, and call 
trodesexport with the relevant input arguments.


trodesexport Command Line Inputs
--------------------------------

**trodesexport** <-h -v> <-spikes -lfp -spikeband -raw -stim> -rec FILENAME <-optionflag1 optionvalue1> <optionflag2 optionvalue2> ... 

- If the *-h* flag is entered, a full usage menu will print to the screen
- If the *-v* flag is entered, the program's version number will print to the screen
- At least one of the processing flags must be provided (such as *-spikes*)
- A rec file must be provided after the *-rec* flag
- By default, the trodesexport program will use the settings defined when recording the data or the settings defined 
in an external workspace (see above). Options for overriding this using the command line are detailed below. 


Processing Modes (one required; multiples allowed)
--------------------------------------------------

- *-spikes*                         -- Spike waveform export (spike snippets only). 
- *-lfp*                            -- Continuous LFP band export. 
- *-spikeband*                      -- Continuous spike band export. 
- *-raw*                            -- Continuous raw band export. 
- *-stim*                           -- Continuous stimulation band export for stimulation capable recording channels. 
- *-dio*                            -- Digital IO channel state change export. 
- *-analogio*                       -- Continuous analog IO export. 
- *-mountainsort*                   -- One mountainsort file per ntrode (using raw band data) 
- *-kilosort*                       -- One kilosort file per ntrode (using raw band data). 


Optional Flags
--------------

- *-rec <filename>*                 -- Recording filename. Required. Muliple *-rec <filename>* entries can be used to append data in output.
- *-output <basename>*              -- The base name for the output files. If not specified, the base name of the first .rec file is used. 
- *-outputdirectory <directory>*    -- A root directory to extract output files to (default is directory of .rec file). 
- *-reconfig <filename>*            -- Use a different workspace than the one embedded in the recording file. 
- *-interp <integer>*               -- (Default is -1 for infinite) Maximum number of dropped packets to interpolate over (linear). A value of -1 is infinite. 0 is no interpolation. 
- *-sortingmode <0,1, or 2>*        -- Used by mountainsort and kilosort modes. Can be a value of 0, 1 (default), or 2. 0 combines channels into one file. Channels in the file will be in the same order as that in the workspace, but won't be divided into separate ntrodes. A value of 1 separates files by nTrodes. A value of 2 separates files by their designated sorting group.
- *-abortbaddata <1 or 0>*          -- Whether or not to abort export if data appears corrupted. 
- *-paddingbytes <integer>*         -- Used to add extra bytes to the expected packet size if an override is required


Datatype-Specific Options
-------------------------

 **LFP data band**

- *-lfplowpass <integer>*           -- Low pass filter value for the LFP band. Overrides settings in workspace for all nTrodes.
- *-lfpoutputrate <integer>*        -- (Default 1500) The LFP band output can have a sampling rate less than the full sampling rate.
- *-uselfprefs <1 or 0>*            -- Override what is in the workspace for lfp band reference on/off settings for all nTrodes.
- *-uselfpfilters <1 or 0>*         -- Override what is in the workspace for lfp filter on/off settings for all nTrodes. 


 **Spike data band**

- *-spikehighpass <integer>*        -- High pass filter value for the spike band. Overrides settings in workspace for all nTrodes. 
- *-spikelowpass <integer>*         -- Low pass filter value for the spike band. Overrides settings in workspace for all nTrodes.
- *-invert <1 or 0>*                -- (Default 1) Sets whether or not to invert spikes to go upward.
- *-usespikerefs <1 or 0>*          -- Override what is in the workspace for spike band reference on/off settings for all nTrodes.
- *-usespikefilters <1 or 0>*       -- Override what is in the workspace for spike filter on/off settings for all nTrodes. 


 **Spike event processing**

- *-thresh <integer>*               -- Overrides the thresholds for each nTrode stored in the workspace and applies the new threshold to all nTrodes.


 **Raw data band**

- *-userawrefs <1 or 0>*            -- Override what is in the workspace for raw band reference on/off settings for all nTrodes.



Other Export Utilities
~~~~~~~~~~~~~~~~~~~~~~

Some export formats are not included in the main *trodesexport* utility. These are provided as separate command line programs: 

**exportofflinesorter (**\ `Offline
Sorter <http://www.plexon.com/products/offline-sorter>`__\ **)**


Used to extract continous data from a raw rec file and save to binary file that can be imported with Offline Sorter. 
   *exportofflinesorter* -rec INPUTFILENAME OPTION1 VALUE1 OPTION2 VALUE2 ...  

Input argument defaults:

- *-outputrate* -1 (full) 
- *-usespikefilters* 1 (Filter the channels with the saved spike filters)
- *-interp* -1 (interpolation not supported, do not change) 
- *-combinechannels* 1 (combine neural channels into one file. Instead use one file per channel.) 
- *-appendauxdata* 0 (do not append DIO and analog data into the file)
- *-userefs* 1 (Use the digital reference channels designated in the file)

To import the binary file into Offline Sorter, from the main menu of Offline Sorter select File | Import | Binary 
File with Continuously Digitized Data. Choose the following settings:

- Number of channels: the total  number of channels in the exported file 
- Data offset: 0
- Digitization freq: 30000 or 20000 (depending on what you actually used)
- A/D Conversion resolution: 16
- Maximum Voltage (mV): 6.39
- Swap bytes: No
Untethered Recordings Using Datalogging Headstages
===================================================

Overview
--------

Many SpikeGadgets headstages have the ability to
record neural data directly to a SD card on the headstage. This
allows for completely untethered operation, which enables a wide range of
experiments that are not practical with tethers. However, this creates
an issue: if the neural signals are recorded on the headstage, how can
we now correlate those signals to events in the environment (lever
presses, beam breaks, video, lights, sounds)? 

During tethered recording,
we combine such signals with the neural recording and timestamp video
frames as data are collected. But during untethered recordings, the
environmental record is recorded separately and must be registered with
the neural data on the SD card after the recording has finished. Here we
cover the main things to consider and how to use the Trodes toolbox to
perform these critical operations. 


Synchronization
---------------

Synchronization between the environmental record recorded by Trodes and
the neural data recorded on the headstage relies on a radio frequency (RF)
synchronization signal that is sent to the headstage at regular intervals 
(most setups use a 10-second interval). 

This RF sync signal is sent from either the Spikegadgets Main Control Unit 
(MCU) or Logger Dock, which acquire the environmental data and connect 
to your computer. Trodes connects to this hardware to display and save the
environmental data during the recording session. Importantly, you will not 
have access to the neural data during recording. 

Any video captured using the camera module in Trodes is also saved to this 
computer, and the camera module is set up the same way it would be for a 
tethered recording. 

Once the recording session has ended, you will have 2 files: one containing 
neural data recorded to the headstage SD card, and one containing the 
environmental record recorded to your local computer. These files will be 
aligned using the RF synchronization signals, which have been saved to both 
data files.


Recording
---------
During an untethered session, where neural data and environmental data are recorded 
by different devices, the workspace used for recording must contain only channels for 
environmental data. This is done by creating a Workspace in Trodes with channel count 
set to zero. 

The headstage and Control Unit must also be set to the same RF channel and sampling 
rate, and session ID mode should be turned on for both. This will ensure that the headstage can 
be properly controlled by the MCU/Logger Dock; receiving Start/Stop commands and RF sync 
signals throughout the recording session.

If you are also using an Environmental Control Unit (ECU) when recording, this must be added to 
your hardware devices when creating your zero-channel Workspace in the Workspace Editor. 


Setting up an Untethered Data Logging Experiment:
-------------------------------------------------
These steps provide an overview of basic setup for untethered recording. For more detailed headstage-specific 
instructions, please see the user manual or Wiki entry for the specific headstage you are using.

1. Open Trodes and either create a new workspace “From Scratch” under the Create/Edit Workspace menu or open the workspace you have already created.

2. Connect your headstage to the MCU via HDMI, and link your MCU to Trodes using the "Connection" dropdown menu by selecting: Source > SpikeGadgets > USB OR Ethernet. 
  - Your SD card should not be inserted into the headstage yet.

3. Under the "Settings" dropdown menus, set the MCU and headstage RF channel and sampling rate to the same values and select the session ID mode checkbox for both:

  MCU: Settings > MCU Settings…

  Headstage: Settings > Headstage Settings…

4. Apply your settings above, then save your Workspace. This Workspace can be used later as the basis for the Workspace used to merge your neural and environmental data.


Initiating an Untethered Recording:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1. Disconnect your headstage from the MCU, connect the headstage to battery power, insert your enabled SD card into the headstage and power the headstage on.

  - For more information about enabling your SD card, please see the `SD Card Enable <https://docs.spikegadgets.com/en/latest/basic/DataLoggerGUI.html#sd-card-enable>`_ 
    subsection of the `DataLoggerGUI Guide <https://docs.spikegadgets.com/en/latest/basic/DataLoggerGUI.html#>`_. 

2. Open Trodes and create a new Workspace with neural channel count set to zero. This workspace will be used to acquire the environmental record.

3. If you are using an Environmental Control Unit (ECU), make sure this is connected to your MCU via HDMI. Your ECU must also be added to your 
  Workspace by selecting "ECU" from the Hardware Devices dropdown menu, then clicking "+Add Device."

4. Connect your RF transceiver to the Aux 1 port on the front of the MCU.

5. Open your workspace, connect to your MCU, and set your RF channel and sampling rate to match the settings you previously applied to your headstage 
   settings, also making sure Session ID mode is checked.

6. When you are ready to begin recording, select "Stream from source" from the Connections menu. This will trigger the headstage to begin recording.

7. To initiate recording with the MCU, select "New Recording..." from the File dropdown menu and select your recording directory.

8. Hit the red "record" button to begin recording your environmental record with the MCU.

9. To end your recording, hit the pause button, and disconnect your stream.

**IMPORTANT NOTE:** The headstage will start recording when you begin streaming with the MCU, but the MCU will NOT start 
recording until you create a new recording file and hit the record button in Trodes. The same holds true for ending 
your recording; the MCU recording can be ended or paused using the interface buttons, but the headstage recording is 
ended by disconnecting your stream.

When using the Logger Dock to record, the Logger Dock RF channel and Session ID mode settings can be set by opening 
the DataLoggerGUI, selecting the Logger Dock entry that will appear under the "Detected Storage Devices" menu, then 
clicking the "Edit dock settings" button to the upper right. You will find the RF channel and session ID settings 
in this pop-up menu.


Transferring Data to Computer and Merging Files
-----------------------------------------------
Once your untethered recording session has ended, the data recorded by both systems will need to be merged 
before further processing and analysis will be possible. Three files are required for merging the data: 

1. The neural data file recorded to SD 
2. The environmental data file recorded to your computer
3. A Workspace file to be appended to the merged data. 

The Workspace file required should contain all the settings that would have been used if the recording 
had been taken in tethered mode. This Workspace will include accurate channel count and nTrodes mapping 
from your headstage AND any settings relevant for recording the environmental record, such as the ECU 
being added to your Workspace Hardware Device list. Once you have these files, the data can be merged 
using the Data Logger GUI application that can be found in the Trodes directory. More information about 
the merging process and Data Logger GUI can be found `here <https://docs.spikegadgets.com/en/latest/basic/DataLoggerGUI.html>`_.
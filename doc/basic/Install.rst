Installing Trodes
=================

**Windows**

Step 1. Install the `FTDI D2XX
Driver <http://www.ftdichip.com/Drivers/D2XX.htm>`__. \* For easy
installation, use the “setup executable” listed in the comments for
Windows.

Step 2. Download the latest release of the Trodes software suite here:
https://bitbucket.org/mkarlsso/trodes/downloads/

Step 3. Uncompress the downloaded folder and copy to a desired location
on your machine.

Step 4. Run the included program vc_redist.x64.exe to install the required Microsoft plug-ins.

Step 5. Double-click on Trodes.exe to run.


**Linux (Ubuntu 20.04)**

Step 1. Install the `FTDI D2XX
Driver <http://www.ftdichip.com/Drivers/D2XX.htm>`__. \* Follow the
instructions in the first section of their
`README <http://www.ftdichip.com/Drivers/D2XX/Linux/ReadMe-linux.txt>`__
\* Also follow the Ubuntu USB setup below

Step 2. Trodes requires you graphics card driver to support open GL. To install 
the correct driver for your card, type 'sudo ubuntu-drivers autoinstall' in a terminal.

Step 3. Download the latest release of the Trodes software suite here:
https://bitbucket.org/mkarlsso/trodes/downloads/

Step 4. Uncompress the downloaded folder and copy to a desired location
on your machine.

Step 5. If you are using a version of Ubuntu other that 20.04, there may 
missing libraries that you will need to install using sudo apt-get commands. 
For this reason, we currently recommend using Ubuntu 20.04.

Step 6. Make sure Trodes program has correct permissions. Right-click on
Trodes and select ‘Properties’ and navigate to the ‘Permissions’ tab.
Make sure that ‘Allow executing file as program’ is checked.

Step 7. Double-click on Trodes to run, or run from a terminal by typing ./Trodes from the install folder.


**MacOS**

Step 1. Install the `FTDI D2XX
Driver <http://www.ftdichip.com/Drivers/D2XX.htm>`__. \* Run the
D2xxHelper program listed in the comments section to prevent OS X claiming the device as a serial port.

Step 2. Download the latest release of the Trodes software suite here:
https://bitbucket.org/mkarlsso/trodes/downloads/

Step 3. Uncompress the downloaded folder and copy to a desired location
on your machine.

Step 4. Because Trodes is not officially registered with Apple, you will need 
to let the OS know that the programs contained in the suite are safe to run. 
To do so, right-click on Trodes.app and select 'Open'. For MacOS 10.15 or later, 
a warning dialog will appear saying that the program is not registered and it 
can't be opened. Usually, the first time you do this, it does not even give you 
an option to open the program anyway. But the second time you try it, there 
should be a button that allows you to open it. When it opens, it will tell you 
that Trodes needs access to the folder where you installed the program, and you 
need to allow it. Once you do all this once, you do not need to do it again until 
you update Trodes. Note, for earlier versions of MacOS, you could simply go to your 
Settings, and change your security settings (“Allow apps downloaded from”) from “Mac 
App Store and identified developers” to “Anywhere”.

Step 5. Do the same procedure described in step 4 for the following apps: cameraModule.app, DataLoggerGUI.app, stateScript.app, stateSCriptEditor.app, and trodesexport.app.

Connecting to the MCU
---------------------

The MCU can be connected to your computer using either USB or ethernet. When using USB to connect between the MCU and computer, 
the data rate is typically limited to 28 - 30 MB/s. If recording bandwidth exceeds this range the number of dropped packets will increase.
For experiments requiring high channel count ethernet is recommended as it can handle > 100 MB/s when connected to a sufficiently powerful 
computer with a good ethernet card. 

For reference, the data throughput when recording 384 channels of data from 1 Neuropixels probe @ 30Khz is ~25 MB/s. This means USB is sufficient 
for recording from one probe, but ethernet is required for multiprobe Neuropixels recordings.


USB Setup
---------

To use USB connection, you must install the FTD2XX driver. Follow
directions in the ftdichip ‘comments’ section for your OS here:

http://www.ftdichip.com/Drivers/D2XX.htm

**Windows**

On Windows USB connectivity works without any extra setup. 


**Linux (Ubuntu 20.04)**

On Ubuntu Linux, to connect to the MCU and ECU via USB, you must tell
your OS how to handle SpikeGadgets hardware when it is plugged in. You
only need to do this once. Run the following command (all on one line)
to copy the udev rules file to your machine:

::

   sudo wget https://bitbucket.org/mkarlsso/trodes/downloads/spikegadgets.rules -O /etc/udev/rules.d/spikegadgets.rules

Rules will be applied the next time you connect the hardware. If already
connected, unplug and replug the USB cable. The .rules file is also
distributed with the precompiled binary, in Resources/SetupHelp, and in
the source repository, at Resources/Linux, so you can copy from there
instead of using ‘wget’ if you want.


**MacOS**

For MacOS, you may need to run the D2xxHelper program to prevent the OS from claiming the device as a serial port.


Ethernet Setup
---------------

If you plan to use ethernet (UDP) to connect to the MCU, you will need a
dedicated ethernet port on the computer for the MCU connection.
Configure this port for IPv4 connection with the following address and
subnet:

::

   Address: 192.168.0.2
   Subnet: 255.255.255.0 

You also need to set the maximum transmission unit (MTU) for the
connection to 9000 or more. This is generally done in the network settings panel of your operating system.

In order to prevent excessive packet drops, you must maximize ethernet 
performance by increasing the UDP buffers on the operating system. This process 
is different for each OS.


**Windows**

For Windows, adjusting the size of receive buffers is done using a
different procedure. To set the default size, edit (or create) the
following DWORD registry keys (using ‘regedit’ program from the start
menu run box) and restart Windows:

::

   [HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Afd\Parameters]
   DefaultReceiveWindow (REG_DWORD) = 8384512 (decimal)
   DefaultSendWindow (REG_DWORD) = 8384512 (decimal)


**Linux (Ubuntu 20.04)**

For Ubuntu Linux, edit /etc/sysctl.conf (as root) and add the following
lines:

::

   net.core.rmem_max=8388607
   net.core.rmem_default=8388607
   net.core.wmem_max=8388607
   net.core.wmem_default=8388607


**MacOS**

On MacOS, edit /etc/sysctl.conf (as root) and add the following
lines:

::

   net.inet.udp.recvspace = 8388607  
   net.inet.udp.sendspace = 8388607   


Firewall
~~~~~~~~

If your computer has a firewall set up, it may block all or portions of the 
traffic between the computer and the MCU when you are using an ethernet connection 
to the MCU. To prevent this, you can either turn off the firewall completely, 
or allow the specific traffic to go through. If you need to use a firewall, 
you will need to allow access (public network) between your computer and Trodes. 
You can either give full permissions to the Trodes program, or as a separate 
strategy you can allow traffic on specific ports (Trodes uses ports 8100 and 8200). 
For Windows, we recommend using the procedure outlined below. 

**Windows firewall reset strategy**

The Windows firewall can lead to lots of problems when trying to connect to the 
MCU with an ethernet connection. We recommend first completely resetting your firewall 
settings and then allowing Windows to automatically “Allow” Trodes access to the MCU 
when you try to connect for the first time. Follow these steps (you only need to do 
this once everytime you update Trodes):

Step 1.

First open windows "Firewall & network protection". Click on "Restore firewall to 
defaults".  A message message will pop up-- click on "Restore defaults". A warning 
will pop up. Click “Yes”. Then, run Trodes as Administrator, open a workspace, connect 
to ethernet and make sure Status is "Connected to source".  Is so, proceed to step 2. 

Step 2. 

Click on "Stream from source". Trodes will blink RED with the message "No data coming 
from source" and a message will pop up saying that Windows Defender is blocking the 
App. Make sure you check “public networks” (the ethernet adapter used for MCU will show 
up as a public network). Click "Allow access".  


Advanced
--------

**Enabling Core Dumps on Linux**

On Ubuntu, if the program crashes, it usually generates a file to be
used to debug if the settings are configured. This can’t be done after
the crash has already happened, so it is advised to enable it. In the
rare event a crash occurs, Spike Gadgets developers can debug the issue.

To enable:

Edit the file /etc/security/limits.conf (as root), to include the
following lines (NB: The line begins with an asterisk):

::

   # (Trodes) Enable core dumps
   * soft core unlimited

Edit /etc/sysctl.conf (as root) and add the following lines:

::

   # (Trodes) Include process id (PID) in core dump file names
   kernel.core_uses_pid=1

Log out and log back in for settings to take effect.

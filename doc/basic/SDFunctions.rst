SD Card Setup
=============

A Note on SD Card Headstage Configuration
-----------------------------------------
Before a SpikeGadgets data logging headstage can be used for untethered recording, it must first 
be configured, then enabled. Historically, the headstage was configured by saving configuration settings to 
the SD card used to record. This process has been streamlined via firmware update, and headstage 
configuration settings are now stored in the headstage's flash memory and can be updated via Trodes. This makes it easier to use 
the same SD card with multiple headstages and makes SD card configuration unnecessary in most situations.


SD Card Enable
--------------
Once a SD card has been used to record data, it must be re-enabled before it can be used to 
record again. This prevents data on the SD card from being accidentally overwritten. This must also 
be done when using a SD card for the first time, and can be done either using the DataLoggerGUI, 
or the MCU (if running firmware 3.19 or later). 

**DataLoggerGUI:** Mount the SD card to either the Logger Dock or MCU (if running firmware 3.19 
or later), select this relevant device from the device list in the DataLoggerGUI, then select the 
above "Enable for recording" button. 

**MCU:** Mount the SD card to the MCU, then press and hold the left button on the MCU. The left 
LED will rapidly flash red for a few seconds while the card is being enabled. Once the card has been enabled the LED 
will return to solid green and the button can be released.



SD Card Data Syncing and Merging
--------------------------------
When an untethered recording session is completed, the neural data in a ``.dat`` format on the SD card. 
This data must first be extracted from the SD card, then converted into the ``.rec`` format, which is 
usable by the Trodes application suite. This data is often then merged with environmental data recorded by Trodes. 
During the merge process, the neural data and environmental data are aligned using the radio frequency (RF) 
sync pulses sent from the control unit to the headstage that have been recorded by both system. The data merging 
and syncing process can either be done using the Data Logger GUI or from the command line. 


For more information about data syncing and merging using the Data Logger GUI, please see the 
`untethered syncing <https://docs.spikegadgets.com/en/latest/basic/UntetheredDataSync.html>`_ 
and `Data Logger GUI <https://docs.spikegadgets.com/en/latest/basic/DataLoggerGUI.html>`_ subsections of the wiki.


Merging Data Using the Command line
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
If merging neural and environmental data using the command line is preferable, this can be done using the 
``mergesdrecording`` command line utility found in the Trodes application folder. 

In addition to the SD and Trodes recording files, you will also need the "Final Trodes workspace" file. 
This workspace file will be appended to your recorded data, and will include the relevant header data for your 
neural recording. Importantly, this is not the workspace file used to record your environmental data, but rather a 
workspace containing all the settings that would have been used if the neural data was acquired using a tethered 
recording, such as channel count, nTrode assignment, etc. 

To use mergesdrecording, simply open the command line, set your current directory to the Trodes application folder, and call 
mergesdrecording with the relevant input arguments.


**Input Arguments:**

- *rec <filename>* - Recording filename from Trodes ending in `.rec`
- *sd <filename>* - Recording filename from SD ending in `.rec` 
- *numchan <integer>* - Number of neural recording channels in SD recording
- *mergeconf <filename>* - Workspace file used for merging ending in `.trodesconf` 

**Optional arguments:** 

- *output <filename>* - Filename assigned to the merged output file
- *outputdirectory <directory>* - Directory the output file is saved to

mergesdrecording is called using the following syntax:
::

   mergesdrecording -rec [ENVIRONMENTALRECORDING.rec] -sd [SDRECORDING] -numchan [NUMCHANNELS] -mergeconf [WORKSPACE.trodesconf]


Below is an example of how this would look when merging an untethered recording using the HH128 128-channel headstage with ECU data recorded in Trodes. 

::

   PATH/TO/TRODES/mergesdrecording -rec PATH/TO/ECU/RECORDING/myRecording.rec -sd PATH/TO/RECORDING/sdFile.dat -numchan 128 -mergeconf 128_Tetrodes_ECU_Sensors.trodesconf

"PATH/TO/TRODES/" can be omitted if your current directory is already set to the Trodes application folder.


If you do not need to merge the data with a simultaneously recorded environmental record from the 
MCU and ECU, `sdtorec` can be used to simply append the "Final Trodes Workspace" to your SD recording.
Syntax and input arguments are the same as above, with the omission of `-rec`.
::

   sdtorec -sd [SDRECORDING.dat] -numchan [NUMCHANNELS] -mergeconf [WORKSPACE.trodesconf]


Quickstart Guide
================

**Getting started with SpikeGadgets Main Control Unit (MCU) and  Trodes**
-------------------------------------------------------------------------

In this guide we will go over the basic information needed to take your first recording using the SpikeGadgets Main 
Control Unit (MCU) and Trodes. Throughout this guide you will find links to the broader Trodes Wiki, where you will 
find additional information about the topics covered herein, as well as information about the many other features 
Trodes has to offer!

This guide will largely focus on tethered recording, for information about untethered recording, please see our 
`guide on untethered recording <https://docs.spikegadgets.com/en/latest/basic/Untethered.html>`_.


Installing Trodes
-----------------

**Windows:**

1. Download and install `FTDI D2XX Driver <http://www.ftdichip.com/Drivers/D2XX.htm>`_.

2. Download the most recent release of the Trodes software suite here.

3. Uncompress the downloaded folder and copy it to the desired location on your machine.

4. Open the Trodes application.


**MacOS:**

1. Download and install `FTDI D2XX Driver <http://www.ftdichip.com/Drivers/D2XX.htm>`_. It is recommended to run the `D2xxHelper <https://www.ftdichip.com/old2020/Drivers/D2XX/MacOSX/D2xxHelper_v2.0.0.pkg>`_ program to prevent MacOS from claiming the device as a serial port.

2. Download the most recent release of the Trodes software suite here.

3. Uncompress the downloaded folder and copy it to the desired location on your machine.

4. Open the Trodes application.

    **NOTE:** When opening Trodes you will see a message saying that Trodes is not registered and 
    can’t be opened. When this occurs, select the option to open the program anyway. If this 
    option is not available, close the window and open Trodes again. You should now see the 
    option to Open the program anyway. When prompted, allow Trodes access to the application 
    directory. 

    You may need to do this individually for each of the Trodes apps: cameraModule, DataLogger, 
    stateScript, stateScriptEditor & trodesexport.

**Linux: (Ubuntu 20.04)**

`Ubuntu 20.04 <https://releases.ubuntu.com/20.04/>`_ is recommended for running Trodes. If you are running another version of Linux you may need to install missing libraries using sudo apt-get commands.

1. Download and install `FTDI D2XX Driver <http://www.ftdichip.com/Drivers/D2XX.htm>`_, following the instructions found in the first section of the `D2xx for Linux README <http://www.ftdichip.com/Drivers/D2XX/Linux/ReadMe-linux.txt>`_. In order to connect to your Main Control Unit (MCU) via USB, you will also need to run the following command as a single line before plugging your MCU in for the first time:

::

    sudo wget https://bitbucket.org/mkarlsso/trodes/downloads/spikegadgets.rules -O /etc/udev/rules.d/spikegadgets.rules

This will tell your OS how to handle the SpikeGadgets hardware when it is plugged in.

2. Download the most recent release of the Trodes software suite `here <https://bitbucket.org/mkarlsso/trodes/downloads/>`_.

3. Uncompress the downloaded folder and copy it to the desired location on your machine.

4. Open the Trodes application.

If connecting to your MCU via ethernet is preferable, setup instructions can be found `here <https://docs.spikegadgets.com/en/latest/basic/Install.html#ethernet-setup>`_.


Creating Your First Workspace in Trodes
---------------------------------------

In this section we will go over how to create your first workspace in Trodes using the 
Workspace Editor. Workspaces are where you will set up and store the hardware and software 
configurations for your experiment. Trodes makes it easy to save your workspace setting for 
subsequent experiments, or sharing with other users.

The Workspace Editor is a very powerful and flexible tool for setting up your experiments 
and contains configuration options for many different types of experiments. 

First we will go over how to set up a very basic tethered recording experiment using the 
Workspace Editor, then we will cover how to playback a recording using a previously recorded 
`sample dataset <https://bitbucket.org/mkarlsso/trodes/downloads/Example_Recording.zip>`_. 

This guide will just be scratching the surface, so feel free to explore the `Trodes Wiki <https://docs.spikegadgets.com/en/latest/>`_ for more details.  Each link headings below will take you to the relevant subsection of the wiki, where you will find more information about specific topics and functions!


Setting Up Your Workspace
-------------------------

1. Open Trodes and select “Create/Edit Workspace.”

2. Select “From Scratch” to open a new Workspace in the Workspace Editor. 

.. figure:: https://spikegadgets.com/wp-content/uploads/2022/09/Trodes-createWorkspaceFrom-Scratch.png
    :alt: Creating a new Workspace "From Scratch"
*figure 1: Creating a new Workspace "From Scratch"*

`General Settings tab <https://docs.spikegadgets.com/en/latest/basic/Workspace.html#general-settings-tab>`_:

3. Under “General” click the empty field next to “Set File Path.” This will open the file explorer, allowing you to set the default save directory. “Set File Prefix” allows you to enter a default prefix that will be added to your recording file(s). 

4. Next, under “Hardware Configuration,” select the number of channels your headstage has from the dropdown menu under “Headstage: Default.” If you are using Neuropixel probes, you can select the number of probes under “Headstage: Neuropixel.”

    **NOTE: For Untethered recordings, the channel count must be set to 0 and the sampling rate should be set to 20KHz.**

.. figure:: https://spikegadgets.com/wp-content/uploads/2022/09/TrodesWorkspaceChannelNumberSelect.png
    :alt: Selecting headstage channel count
*figure 2: Selecting headstage channel count*

`Channel Map tab <https://docs.spikegadgets.com/en/latest/basic/Workspace.html#channel-map-tab>`_:

5. The Channel Map tab is where you will assign individual electrode channels to groups called nTrodes. Each channel also includes additional fields, where additional information such as stimulation capability, channel location, sorting and display order can be specified. 

   This information can be entered directly for each channel in Trodes, but for most recording configurations we recommend using a 3rd party spreadsheet application such as MS Excel to help create your map.  

   Once you have created the channel map spreadsheet it can be imported using the “Import channel map” button. For more information about this see the `Channel Map <https://docs.spikegadgets.com/en/latest/basic/Workspace.html#channel-map-tab>`_ section of the wiki. If custom channel mapping is not required select “autopopulate.”

.. figure:: https://spikegadgets.com/wp-content/uploads/2022/09/TrodesWorkspaceChannelMap.png
    :alt: Channel Map tab
*figure 3: Channel Map tab in Workspace editor*

6. Once you have either selected “autopopulate,” or imported your channel map, Save and/or Open your workspace. 


Using Your First Workspace
--------------------------
**Connecting your MCU & headstage for tethered recording:**
If you have not done so already, connect your MCU to your computer using your preferred method 
(either USB or `ethernet <https://docs.spikegadgets.com/en/latest/basic/Install.html#ethernet-setup>`_), and power on the MCU. If you have not connected your headstage yet you 
will notice the right-most status LED flashing orange. This LED will change to solid green once your headstage has been connected. 

You will want to plug your headstage into the MCU using the HDMI tether regardless of if you plan 
to take a tethered or untethered recording. 

Now that your MCU and headstage are plugged in, you can link your MCU to Trodes by selecting your 
preferred connection method under the connection dropdown menu in your workspace: 

    *Connection > Source > SpikeGadgets >*

For tethered recordings, no additional steps are needed for connecting your headstage and you can feel 
free to skip the following section detailing untethered connections.


**Connecting your MCU & headstage for Untethered recording:**
When setting up an untethered recording, the MCU and headstage must first be set to the same Radio 
Frequency (RF) channel and sampling rate, with the Session ID mode checkbox checked for both the MCU and headstage. This ensures that the 
MCU can sync with the headstage. To do this, first connect the headstage to the MCU via HDMI.

Now that your MCU and headstage are plugged in, you can link your MCU to Trodes by selecting your 
preferred connection method under the connection dropdown menu in your workspace: 

    *Connection > Source > SpikeGadgets >*

Once the headstage has been connected via HDMI, the RF channel, Session ID mode and sampling rates can be set under 
the Settings dropdown menu:

- *MCU:* Settings > MCU Settings… 

- *Headstage:* Settings > Headstage > Headstage Settings

.. figure:: https://spikegadgets.com/wp-content/uploads/2022/09/MCU-RFsessionID.png
    :alt: Setting MCU RF channel and turning on Session ID
*figure 4: Setting MCU RF channel and turning on Session ID*

Once you have confirmed that the MCU and headstage sampling rate and RF channel settings match, a few 
additional steps are needed to finish setting up your first untethered recording:

1. Unplug the headstage from HDMI.
2. Plug the `RF transceiver <https://spikegadgets.com/products/rf-transceiver/>`_ into the Aux 1 port on the front of the MCU.
3. Connect the headstage battery (if your headstage does not use an integrated battery).
4. Insert the SD card into the headstage. Importantly, the SD card must be enabled for recording using the MCU or DataLogger GUI.

For additional information about untethered recordings, please see the `getting started with untethered recording <https://docs.spikegadgets.com/en/latest/basic/Untethered.html#getting-started-first-time-steps-to-set-up-data-logging>`_ and `Data Logger GUI <https://docs.spikegadgets.com/en/latest/basic/DataLoggerGUI.html>`_ sections of the wiki.


Taking Your First Tethered Recording
------------------------------------
1. Begin streaming by selecting “Stream from source” under the Connection dropdown menu. The workspace will now begin streaming data from the headstage. You will only see neural data streaming when taking a tethered recording. If you would also like to record video data, you can do this by clicking the “Video” button at the top of the workspace, which will open the video module.
2. Now, create a new recording by selecting “New recording…” under the File dropdown menu. You will be prompted to set the filename and save directory for your new recording.
3. With the save directory set, you will see the record and pause buttons in the upper left corner of your workspace. Pressing the record button will initiate your first recording. Importantly, untethered recordings cannot be paused, but tethered recordings can be paused and restarted at any time.
4. When you are ready to end your recording, press the pause button. This will pause the recording, but will not pause the data stream. To end the recording, disconnect the data stream by selecting the “disconnect” option under Connection dropdown menu.


Viewing Your Data
-----------------
Now that you have taken your first recording, you can view the data you recorded inside Trodes. 
If you have not yet taken a recording, you can also download a sample dataset `here <https://bitbucket.org/mkarlsso/trodes/downloads/Example_Recording.zip>`_.

To open your data file either select “Open Playback File” from the main menu on the landing page of Trodes, 
and click “Browse” to find your data file (.rec file), or select "Playback last recording" from the File 
dropdown menu in your workspace. From here you can playback your data by clicking the Play button in the upper 
left corner, which will playback your data in the same view it was acquired in. 

Video files captured while recording can also be viewed by clicking the Video button. If you would like to view a 
video file independent of your recording data, you can also do this by opening the Video module application directly 
from the Trodes folder.


Exporting Your Data
-------------------
Data export is also done using the “Open Playback File” option in Trodes. Once you have opened the desired video file in playback mode, you can use the following steps to export your data in the desired format. For more detailed information about data export, see `Exporting Data <https://docs.spikegadgets.com/en/latest/basic/Export.html#>`_ and `Export Utility Functions <https://docs.spikegadgets.com/en/latest/basic/ExportFunctions.html>`_.

1. Select “Extract…” under the File dropdown menu. 

2. A window will open allowing you to select how you would like to export the data. More than one option can be selected. 

   .. figure:: https://spikegadgets.com/wp-content/uploads/2022/09/ExtractDataFromFile.png
    :alt: Data extraction options
   *Figure 5: Trodes Export Utility extraction options*

3. The checkbox options allow you to export your data in the following ways:
 - *Spikes:* exports spike waveform snippets
 - *Lfp:* exports low-bandpass filtered local field potential (LFP) data 
 - *Spikeband:* exports bandpass filtered continuous data containing spikes
 - *Raw:* exports broadband unfiltered continuous data
 - *Stim:* exports current trace data for each channel when using `stimulation-capable headstages <https://spikegadgets.com/products/32-channel-stimulation-headstage/>`_ and `SHCU <https://spikegadgets.com/products/stim-capable-headstage-combiner-unit-shcu/>`_
 - *Dio:* exports digital input and output data
 - *Analogio:* exports analog input and output data
 - *Mountainsort:* exports broadband raw data whitened and referenced for import into Mountainsort
 - *Kilosort:* exports bandpass filtered spikeband data, segmented into user-defined sorting groups for import into Kilosort

4. Once you have selected your desired export formats, hit "Start" to begin data export.

    NOTE: The command line can be used for more `advanced export options <https://docs.spikegadgets.com/en/latest/basic/ExportFunctions.html#command-line-usage>`_, such as exporting data for further analysis with `Offline Sorter <http://www.plexon.com/products/offline-sorter>`_. More information about this and other command line export functions can be found under `Export Utility Functions <https://docs.spikegadgets.com/en/latest/basic/ExportFunctions.html>`_.


Importing Your Data For Further Analysis With Matlab, or Python
---------------------------------------------------------------

Once you have exported your data, if you’d like to import your data into Matlab or Python, you can find utility scripts for doing this packaged with Trodes. 
You can find these scripts in the following locations:

*Matlab:* Trodes folder > Resources > TrodesToMatlab 

*Python:* Trodes folder > Resources > PythonToMatlab 

For more information about these scripts as well as additional information about Trodes data 
structures, please see `Exporting Data <https://docs.spikegadgets.com/en/latest/basic/Export.html#>`_ and `Export Utility Functions <https://docs.spikegadgets.com/en/latest/basic/ExportFunctions.html>`_.















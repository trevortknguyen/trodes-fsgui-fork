Data Logger GUI
---------------

The Data Logger GUI was developed to provide a convenient way to download neural 
data recorded during an untethered experiment from SpikeGadgets' data loggers and 
merge that data with a simultaneously recorded environmental record. 

It can be used in conjunction with the SpikeGadgets' 
`Logger Dock <https://spikegadgets.com/products/logger-dock/>`_ or `Main Control Unit (MCU) <https://spikegadgets.com/products/main-control-unit/>`_. 
The Data Logger GUI can be opened directly, or from Trodes using the "Merge with logger data" 
menu item under "File" when an environmental record is opened in playback mode.


Mounting SD Card
~~~~~~~~~~~~~~~~

.. figure:: https://spikegadgets.com/wp-content/uploads/2022/11/DataLoggerGUI-SDinMCU.png
    :alt: Data Logger GUI with SD card mounted in MCU

*figure 1: Data Logger GUI with SD card mounted in MCU*


Once you have finished your recording, you need to download the data to
your computer. To start this process, insert the SD card into the Logger 
Dock or MCU. The Logger Dock or MCU should be turned on and connector to your computer 
via a USB connection. After opening the Data Logger GUI, The SD card will be listed 
with the control unit you are using under "Detected Storage Devices". If your control 
unit does not appear on this list, click the refresh list button, and it should appear.

**NOTE:** The MCU must be running firmware version 3.19 or later to be used with the DataLoggerGUI.

The SD card will appear in the device list under the "Type" heading of your control unit, 
with information about card size and status listed. If data has been recorded on the SD card, the 
right-hand Headstage Setting panel will populate with information about the recording, 
and the card status will indicate that the card is "Not enabled for recording". 
This is a safety feature to prevent accidental erasing of data before it had been 
downloaded to your computer. After you have downloaded the data, you will need to 
enable the card for recording before it can be used to record with a SpikeGadgets 
datalogger again. This is done by clicking the "Enable for recording" button. 


Updating Control Unit and Headstage Settings 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The DataLoggerGUI can also be used to update control unit and headstage settings such 
as radio frequency (RF), sampling rate and session ID for untethered recording. The MCU must be running firmware 
3.19 or later to update MCU settings or headstage settings, with the exception of the ML32, which can be plugged 
into the Logger Dock directly.

**Updating control unit settings:** Control unit settings can be updated by selecting the desired 
control unit (Logger Dock or MCU) from the Detected Storage Device list, then clicking the Edit device 
settings button above. 

**Updating data logger configuration:** For some data loggers, configuration settings can be updated by selecting the 
desired data logger from the Detected Storage Devices list, and clicking Edit logger config. To do this, your data logger 
must be connected to the MCU via HDMI tether or docked on the Logger Dock. 


Extracting Data and Merging With Environmental Record
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Most users will have two recordings that were simultaneously recorded during 
an untethered experiment: 1) the neural recording on the SD card, and 2) environmental 
recording (.rec file) taken by the Trodes software, which may be linked to other 
files such as video. 

These 2 files are aligned and merged into a single ``.rec`` file using the radio frequency (RF) sync 
pulses embedded in each file. These RF sync pulses are transmitted from the control unit with a 10-second 
interval (for most setups) and are recorded by both devices. More information about Untethered 
Data Synchronization can be found `here <https://docs.spikegadgets.com/en/latest/basic/UntetheredDataSync.html>`_.

To merge the neural data with the environmental data, make sure the checkbox 
next to "Merge logger data with environmental data" under the "Export" tab is checked. If you opened 
the DataLoggerGUI application from Trodes, the path to the environmental recording 
will already be populated. If you opened the application directly, you will need 
to browse to the relevant environmental ``.rec`` file.

Next, you will need to select the "Final Trodes workspace" file. This workspace file will be 
appended to your recorded data, and will include the relevant header data for your neural recording. 
Importantly, this is not the workspace file used to record your environmental data, but rather a workspace 
containing all the settings that would have been used if the neural data was acquired using a tethered recording, 
such as channel count, nTrode assignment, etc. 

Often the easiest way to generate this workspace is to simply save the workspace used when applying settings to 
your headstage while setting up your experiment. Just be sure to include any custom nTrode mapping when doing this.
Otherwise, simply create and save a new workspace with the relevant settings.

For more information about workspace creation, see the `Workspaces <https://docs.spikegadgets.com/en/latest/basic/Workspace.html>`_ subsection of the Trodes wiki.

Finally, you will need to specify a filename for the merged data file. The DataLoggerGUI will create a default 
filename, which can be seen under "Extracted file name," but this can be edited to meet your needs.

Click the "START" button to begin the extraction and merging process.

  **Notes:** 
  - If you are not merging with an environmental record, uncheck the corresponding checkbox at the 
  top of the Extract tab. The "Final workspace" will simply be appended to the extracted file without merging the data.

  - If you have previously extracted a file from an SD card and the resulting ``.dat`` file is already on your computer, 
  this data can be extracted and/or merged by selecting the "Browse existing .dat" button below your device list. When 
  using this approach, the extraction step will be skipped and the ``.dat`` file data will be used instead.


Process Output
~~~~~~~~~~~~~~

When the START button is pressed, the extraction process will begin. The "Console" 
window will display the output of the process, with percentage complete indications 
and potential errors or warnings displayed. For large files, this process may take 
several hours.

When the extraction process is complete, the extracted file will be saved to your 
computer in the same folder as the designated output file. This is to expedite any 
subsequent merge processes.

Next, the merge process will begin. This process is generally fairly fast. Percent 
complete will be shown in the console window, along with errors and warnings. If the 
process completes without errors, the merged ``.rec`` file will be saved to your computer 
automatically. Please review the "Console" output of the merge process to check for any errors 
or warnings that may need your attention:

- **Excessive drift** During the ongoing merge process, every time a synchronization 
  signal is detected the program will display how far apart the two files have drifted. 
  It is normal for the two files to drift a few dozen samples every ten seconds. However, 
  drift in the thousands of samples points to a larger problem that needs to be addressed.

- **Excessive dropped data** Every time data are dropped in either the environmental file 
  or the neural data file, a warning will be printed to the console displaying how may 
  packets were dropped. A few hundred packets here and there is generally not a problem, 
  but excessive drops will also need to be addressed, and likely indicates that the SD 
  card was not fast enough to record the number of channels used on the headstage.

All alignment corrections (due to drift or dropped packets) gives priority to the neural 
recording. Data are either filled in or taken out of the environmental recording to accomplish 
the alignment. When data are filled in, the last digital or analog values are repeated.

Once extraction and merging are complete, if you opened the DataLoggerGUI from Trodes, clicking 
the "Close" button at the bottom right of the window will trigger Trodes to automatically open 
the resulting merged ``.rec`` file in playback mode for inspection.


SD Card Enable
~~~~~~~~~~~~~~

Once a SD card has been used to record data, it must be re-enabled before it can be used to 
record again. This prevents data on the SD card from being accidentally overwritten. This 
must also be done when using a SD card for the first time. The SD card can be enabled either 
using the DataLoggerGUI, or the MCU (if running firmware 3.19 or later).

**DataLoggerGUI:** Mount the SD card to either the Logger Dock or MCU (if running firmware 3.19 
or later), select this relevant device from the device list in the DataLoggerGUI, then select the 
above "Enable for recording" button. 

**MCU:** Mount the SD card to the MCU, then press and hold the left button on the MCU. The left 
LED will rapidly flash red for a few seconds while the card is being enabled. Once the card has been enabled the LED 
will return to solid green and the button can be released.
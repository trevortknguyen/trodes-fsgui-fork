Importing data to Python
=========================


Once data has been `exported <https://docs.spikegadgets.com/en/latest/basic/Export.html>`__. \* 
from Trodes, it can be imported into Python for secondary processing and 
analysis using scripts provided in the Resources > TrodesToPython 
folder contained within the Trodes installation folder. The scripts contained within 
will enable you to import your recording binary data and camera module video timestamps into 
Python for further evaluation and/or analysis. 

**readTrodesExtractedDataFile3:**
- Reads in a single binary data (``.dat``) file exported from Trodes using the export utility. 
- Returns a Python dict containing the file metatdata as well as a numpy array containing the binary data stored in the ‘data’ field.
- Some Camera Module outputs such as position data also create a binary file which can be parsed using this function. 

**NOTE: numpy installation is required**

For Python 2 support, use readTrodesExtractedDataFile2 contained within the same folder.

Information about about the data and data type(s) contained within 'data' can be found in the 'fields' dict key entry. 
::

   fields: <time uint32><LineSegment uint16><RelativeLinearPos double>

In this example, 'fields' indicates that each position in the 'data' array contains time, line segment and relative linear position data as seen below. 
::
   >importedData['data']
   [(160157682, 0,  0.23706176), (160159750, 0,  0.26997685),
   (160161938, 0,  0.34283835), (160165086, 0,  0.42896333),
   ...
   (160168038, 0,  0.492976), (160170927, 0,  0.55913753)]

Some camera module outputs such as position data also create a binary file. These files can also be parsed with this function.

**readCameraModuleTimeStamps:**
- Reads in a single Camera Module timestamp (``.videoTimeStamps``) file exported from Trodes.
- Returns a numpy array containing the Camere Module video timestamps.

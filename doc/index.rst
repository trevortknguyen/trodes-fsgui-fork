.. Trodes documentation master file, created by
   sphinx-quickstart on Tue Mar  2 23:41:29 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Trodes Documentation
====================

This is the official online documentation for Trodes. The user manual will provide a
step-by-step guide to getting started with Trodes, setting up a
recording session, and more. The development docs will provide details
on system architecture, design details, messaging protocols, etc.

.. toctree::
   :maxdepth: 2
   :caption: Basic

   basic/QuickstartGuide.rst
   basic/Install.rst
   basic/BasicUsage.rst
   basic/Workspace.rst
   basic/Modules.rst
   basic/Export.rst
   basic/ExportFunctions.rst
   basic/NeuropixelsProbe.rst
   basic/Untethered.rst
   basic/UntetheredDataSynchronization.rst
   basic/DataLoggerGUI.rst
   basic/SDFunctions.rst
   basic/FAQ.rst

.. toctree::
   :maxdepth: 2
   :caption: Advanced

   advanced/PrecisionTimeProtocol.rst
   advanced/CommonAverageReferencing.rst
   advanced/CommandLineCalls.rst
   advanced/NetworkingAPI.rst
   advanced/Benchmarking.rst
   advanced/WorkspaceXmlFormat.rst

.. toctree::
   :maxdepth: 2
   :caption: Developers

   developers/index.rst
   developers/DevelopmentEnvironment.rst
   developers/Building.rst
   developers/Configuration.rst
   developers/SystemArchitecture.rst
   developers/Debugging.rst
   developers/Networking.rst
   developers/TrodesNetworkPython.rst
   developers/InstallingModules.rst
   developers/CameraModuleDev.rst
   developers/CodingStyle.rst
   developers/Documentation.rst

.. toctree::
   :maxdepth: 2
   :caption: Release notes

   ReleaseNotes.rst
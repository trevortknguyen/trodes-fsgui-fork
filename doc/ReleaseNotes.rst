Release Notes
=============

2.3.4
-----

**Critical Bug Fixes**

- Common Average Referencing was broken when the system was using AVX (the default since version 2.0.0). CAR is now fixed for AVX processing mode.

- mergesdrecording program had a channel number scanning bug causing it to think that some SD recordings had a different channel count than it should. Now fixed.

- Fixed bug where some channels were skipped during impedance testing.

- DataLoggerGUI fix so that error messages are actually displayed in the pop up window

- DataLoggerGUI fix to terminate ongoing processes upon main window close.


**Minor Bug Fixes**

- Fix to restart audio output if it goes idle during data streaming 


**Features**

- Added version output to mergeSdRecording

- Added time as an extraction option in the GUI during playback mode


2.3.3
-----

**Critical Bug Fixes**

- Adding and removing hardware devices in the workspace editor no longer results in wrong packet location identifiers for other Aux channels

- Spike processing now moved to a different thread than the main processors to prevent instability and slow downs

- USB webcam instability in Linux fixed for cameramodule

- Fixed camera module timestamp issue that occurred when using USB webcam and ZMQ network

**Minor Bug Fixes**

- Fixed display units for impedance measures

- Trodesexport fix to read in very long workspaces (more than 2000 channels)

- Fix the merge program for more robust channel number scanning and verification

- Cameramodule fix to playback speed during offline tracking

- DataLoggerGui fix to writing settings to ml32 attached to logger dock

**Features**

- Improved and expanded documentation

- You can now toggle whether or not cameramodule auto stops when confused about the animal's location

- Users can request simulated data from supporting hardware

 
2.3.2
-----

**Critical Bug Fixes**

- Trodesexport had serious segfault issue introduced in 2.3.0

- stateScriptEditor crashed when opening file in windows

**Minor Bug Fixes**

- trodesexport maximum interp window was off by 2 samples, now fixed

- Audio would go silent on Ubuntu after stopping and restarting stream. Now fixed.

**Features**

- Added support for setting and detecting radio modes

- interp flag now supported for export of neuropixels data
 


2.3.1
-----

**Critical Bug Fixes**

- Fixed LFP output stream bug on Trodesnetwork API that was introduced in 2.3.0

- Fixed crash conditions in opening workspaces with 0 channels introduced in 2.3.0

**Minor Bug Fixes**

- Fixed bug in spectrum analyzer introduced in 2.3.0

- Fixed some display issues in opening menu introduced in 2.3.0 



2.3.0
-----

**New dependencies**

- Building now requires Qt6

- Ubuntu version requires Ubuntu 20.04 (previously it was 18.04)

**Critical Bug Fixes**

- Fixed position tracking issues in camera module that were introduced in 2.2.3

- Fixed video source from USB failure introduced in 2.2.3

- Fixed issue where pressing play button twice in camera module led to crash

**Minor Bug Fixes**

- SD card data extraction now robust to blank sectors of data

- Fixed an issue with trodesexport where appending multiple files to a single output was not working properly under some conditions 

- Fixed issue where DataLoggerGUI would wait forever to enable SD card if the card has a partition table set up. 

- Fixed issue with DataLoggerGUI extraction from SD card to make it more robust to corrupted data sections

**Features**

- Trodes now monitors the status byte coming from the MCU, and triggers a visual error signal if the headstage is unresponsive.

- Enable/disable raw data publishing streams via trodesnetwork 

- Event system via trodesnetwork and python bindings

- Zone entry/exit trodesnetwork events 

- File IO via trodesnetwork

- Real-time mode to enable reduced closed-loop latencies using trodesnetwork 

- Added a timestampoffset.txt file for trodesexport to allow users to lookup the timestamps in output files where multiple rec files were appended.

- Impedance measurements using Intan-based headstages

- Human-readable linear track information in geometry export file

- Added PythonExampleScripts folder in the Resources folder for simple python demonstrations of various API features

- trodesexport now has a time export mode to export both hardware timestamps and system timestamps

- Increased buffers for spikes in the cluster displays

- Visual progress indicator when workspace is loading

- Aux IO channels are now visually clipped if larger than max display setting

- Improved support for high DPI monitors

- Spike display now updates faster when new spikes come in



2.2.3
-----

**Critical Bug Fixes**

- Fixed issue with camera module offline tracking mode where all tracking data gets erased when the file position marker returns to the first frame. Now, you can skip around without the tracking data being erased for both 2d and linear tracking files.

- Fixed issue where ZMQ network did not send out DIO data while using USB source.

**Minor Bug Fixes**

- Fixed issue with raw export mode for trodesexport. Sorting modes 0, 1, and 2 did not have proper channel count info embedded in metadata.

- Fixed issue with the Annotate tool where the history was not being displayed.

- mergeSDRecording utility has a fix in a rare merge condition error.

- Filter view mode selector behavior fixed

- .trodesconf extension now on workspaces saved from playback file.

**Features**

- Added ability to manually turn off use of AVX as a workspace setting.

- Added a single keypress option to the Annotate GUI



2.2.2
-----

**Critical Bug Fixes**

- Reference group on/off setting was not properly being saved when saving a new workspace. Now fixed.

- Stimulation band export was in the wrong floating point format. Now fixed.


**Minor Bug Fixes**

- Fixed headstage settings dialog to correctly set autosettle percent channels.

- trodesexport metadata for LFP now contains the nTrode channel that was used.

- Fixed issue where exportofflinesorter was inverting the spike band 

**Features**

- trodesexport now has a sortingmode flag to flexibly control how mountainsort, kilosort, and raw files divide the neural channels into separate files.

- splitRec utility updated to include a timesections argument to split the rec file into equal time sections

2.2.1
-----

**Critical Bug Fixes**

- Fixed an issue where a new source helper thread was not being properly shut down after USB use, causing streaming errors when users switch between USB and Ethernet source.


2.2.0
-----

**Critical Bug Fixes**

- Fixed an issue where the MCU IP address was being shifted when setting other MCU settings.

- Moved ZMQ raw data sending to separate thread to prevent the source thread from dropping packets.

- Fix to where the temp.trodesconf file gets saved and loaded from when creating workspace from scratch. 

- Fix to trodesexport, correcting how multiple files are appended.


**Minor Changes or Bug Fixes**

- Fixed stim command error statements

- Fix for when the remembered window location falls off screen.

2.1.1
-----

**Critical Bug Fixes**

- Neuropixels workspaces were not properly using the new spikeSortingGroups feature, causing trodesexport to hang. Now fixed so that each probe has its own sorting group.

**Minor Changes or Bug Fixes**

- Small changes to debug log in dropped packets reporting.
 

2.1.0
-----

**Critical Bug Fixes**

- The Camera Module and StateScript Modules now receive the correct sampling rate from Trodes when using the new ZMQ network

- Stimulation commands now have the correct number of pulses (it was doing one more than it should have

- Fixed a camera module crash condition when closing, then opening a video recording file  

- Camera module can save/load all geometry types again (has been disabled)

**Minor Bug Fixes**

- Reduced audio output latency 

- Export utility displays percent done more reliably

**Features**

- Sorting groups are now used by default when exporting to MountainSort or Kilosort formats. The sorting group for each channel can be modified in the channel map interface of the workspace editor.

- Better Red/Green tracking in the camera module makes it usable even if the background is white

- Workspace editor now has a feature to split recordings into set file sizes. Commands are sent to modules to also split files at the same time (i.e., video)

- Workspace editor now has an LFP subsampling rate interface that is used by the export program and the ZMQ network 

- Camera module can now reduce the resolution of incoming video frames in order to reduce CPU usage during tracking and video compression.



2.0.0
-----

**Critical Bug Fixes**

- DataLogger GUI has many fixes relating to data extraction and merging
- Stability improvement for when multiple nTrodes were selected and their settings were changed together.
- Stability improvement when lack of available audio hardware
- The merge utility has multiple additions to improve tolerance for less common conditions


**Features**

- A combined export utility (trodesexport) was developed that is used to export multiple types a data at once.

- Trodes and the export utilities now use AVX2 to process data if it is available on the machine. This includes filtering, referencing, and spike waveform processing

- An API is introduced that allows users to gain access to the data in real time using either c++ or python. This API is based on the ZMQ library.

- Neuropixels support, including an interface to select which channels are active 

- Stimulation control and pulse visualization



1.9.1
-----

**Critical Bug Fixes**
- nTrode menu was not updating correctly as the user clicked on a new nTrode. This issue was introduced in 1.9.0

- Audio was not updating properly with CAR referencing changes.

- exportLFP was scrambling the channels id's in the output. This issue was introduced in 1.9.0

- Fixed a crash condition in splitRec



**Features**

- Two new export functions were added: exportraw and exportspikeband. These are used to export continuous data from either the raw data band or the spike data band. 

- CAR groups can now be added from the main Trodes window.




**Minor fixes and features**

- Workspace editor was overwriting the nTrode colors. This is now fixed.

- Improved export program help menu



1.9.0
-----

**Major Features**

- Data Logger GUI: Updated with a better, more streamlined interface.
    Works with new Dock for faster data extractions, charging station, and streaming.

- RF connected device panel: Our new Dock streams DIO/analog like an MCU, and can also
    connect to a data logger wirelessly! When connected in Trodes, an RF panel shows up
    that displays details about the connected data logger, including # of channels, SD
    capacity, battery remaining, and more.

- RMS plot: Plots root-mean-sq of each hardware channel. Useful for noise debugging

- Power spectral density plot: Density plot of frequencies of a selected ntrode's signal. Useful for noise debugging.

- Allied Vision cameras: Using Bayer8 image format (can be set in their VimbaViewer software),
    your video frame rate can improve by as much as 2x, because CameraModule now supports Bayer8
    to normal RGB24 conversion.
- Thanks to Tom and Abhilasha from the Frank Lab!

- Updated internal streaming and processing code to be more efficient

1.8.1
-----

**Bugfixes**

- Moved create/edit/remove of CAR groups to workspace editor

- Renamed quickstart to auto-connect

- Version checking and online updater works and can be properly launched from Trodes

- Changed names of certain hardware devices to more intuitive names

- For wireless logger users: Added specific .trodesconf workspaces for datalogging to SampleWorkspaces/Logger_workspaces

- System clock bug fixes:
                - export programs can properly deal with sysclock
                - workspaces with sysclock display DIO data properly

- Improved tooltips in ntrode settings dialog

- Statescript does simple check if observer language executable is valid

- Group referencing is included in export code


1.8.0
-----

**Major Features**

- Auto-connect that can generate workspaces based on connected hardware

- Common Average Referencing

- PTP for AVT cameras

- System clock in recordings, saved in each packet

- Data Logger GUI

- Export GUI

- Installer and updater tools


1.7.2 (Mar 27, 2018)
--------------------



**Major Features**

   - NTrode selection and settings: Multiple ntrodes can now be selected, by using typical conventions with SHIFT or CTRL and clicking on multiple ntrodes. The ntrode button now creates an ntrode settings panel which applies the changes to all ntrodes selected. 

   - Categories and tags for ntrodes: Ntrodes can be assigned categories and tags in the settings panel. Categories can have any number of tags. Any ntrode can be labeled with a category-tag combination. They are stored in the config file. The categories and tags can also be used to filter and select ntrodes. 

   - Spike display: Unclustered points and waveforms can now be hidden. This is useful when clusters are created and the user wants to hide unclustered points. Both the scatterplot and waveforms plot support this. Individual clusters can also be shown or hidden at the menu from a right-click on a cluster. 

   - Stream display: The user can decide to view a spike stream, LFP stream, or raw unfiltered stream. The LFP stream only displays the designated channel. The filters are separate from the stream display, so the user can view the LFP stream while still enabling the spike filter. 

   - Hardware disconnects: if the hardware briefly disconnects, the system will automatically attempt to reconnect. There is also now an indicator for hardware status. 

   - Moved to Qt5.9: Qt has fixed several of their own bugs and improved features over their releases. We are moving from 5.5.1 to 5.9.1. 



**Bug fixes from 1.7.0-1.7.2**

   - Critical bug fix in export spikes.

   - Opening a file on cameraModule on a different machine will result in creating a folder for the recording in the cameraModule's directory

   - Switching cameras is now more stable and does not result in random crashes

   - Monitor switching should no longer cause crashes, was reintroduced by new Qt version 5.9

   - Fixed GUI on cameramodule that was ambiguous and did not prevent user error

   - LFP and unfiltered data polarity was mistakenly reversed. They are now normal. Spike polarity is kept as reversed.

   - Fixed disconnect fault tolerence with USB connection

   - Export functions bugs and settings fixes

   - Build script fixed on Linux


1.6.1 (July 6, 2017)
--------------------



**MINOR CHANGES**

   - We are now a registered Apple developer . This means Mac users will not receive messages about Trodes being from an "unverified developer"

   - Can now highlight and select multiple channels/ntrodes at a time in the workspace gui using shift click

   - Workspace GUI has a more intuitive interface 

   - CameraModule can now save and load inclusion/exclusion zone geometries

**MINOR BUGFIXES**

   - Workspace GUI highlighting and selecting bug fixes.

   - Available space in hard drive is back in status bar when recording

   - CameraModule in playback mode opening in the wrong mode or the wrong files is fixed

   - Changed video encoding so there isn't a missing frame at the end of a recorded video file

   - Merge SD-Rec bugs fixed

1.6.0 (June 7, 2017)
--------------------



**MAJOR FEATURES**

- Multi waveform plotting.  The spike waveform viewer can not display many spikes at once and color them according to sorted spikes.  

- Workspace editor interface.  Workspaces can now be generated and edited using a graphical interface.  This eliminates the need to know XML to create or edit workspaces.

- File playback improvements.  File playback mode now uses a slider bar to allow the user to skip to any part of the file.  The camera module will open the accompanying video (if it exists) and synchronize playback with neural data.

- Intro menu.  Upon startup, Trodes now displays a menu allowing the user to quickly access previously used workspaces or recordings.

- Headstage settings dialog.  Users can now change headstage settings such as auto settle, smart referencing, and motion sensor on/off states from Trodes.

- Improved recording status indicator. Previously, when a file was opened for recording, some users had the assumption that it meant recording had started, because there was no obvious indicator. Now, indicators have been added to make it clear to the user if recording is currently on or not. We added text that states the number of hours and minutes recorded so far, a long status bar that changes color and reads either "Recording Paused" or "Recording Active”, and on the camera module, the file opened and time recorded is displayed at the top of the window


**MINOR CHANGES**

- Freeze display.  The user can now freeze the continuous stream display by either pushing the Freeze Display button, using the menu, or pressing the space bar.

- Hardware serial number and version display. This information is also appended to recordings.

- StateScript GUI improvements.  Macro buttons can be configured to enter StateScript commands to toggle DIO pins, etc. Also, there is improved labeling of the different panels. 

- Version update lookup.  Trodes will automatically lookup if a newer version is available.  In the intro menu, the ‘Information’ box will alert the user if a new version is available for download.

- Camera module export additions.  The camera module will now export linear track geometries, zone geometries, and scale info (cm per pixel).  It will also export linearized position if a linear geometry was enabled during tracking.

- If settings are changed to any nTrodes after a recording file is opened, the program will prompt the user to save an updated workspace alongside the recording (since the header at the top of the recording only captures the initial state).

- Multiple PETH windows open.  The user can open separate PETH windows for multiple nTrodes, which will automatically update when new events occur.

- Spike waveform alignment.  Spikes are now automatically aligned to the peak. This applies both the Trodes and the exportspikes program.

- Audio Off command line argument to disable audio.  On some machine, auto driver incompatibilities can cause crashes, so this was implemented to diagnose audio issues.

- For two-diode tracking in camera module, users can set if the red diode or the green diode is in the front.

    
**CRITICAL BUG FIXES**

   None


**MINOR BUG FIXES**

- Python data extraction program fixed

- mergeSDRecording had incorrect error outputs that were removed

- Fixed the number of displayed columns to match what is in the workspace.

- Digital reference nTrodes are referenced by their in the workspace ID number rather than their index 

1.5.2
-----



**MINOR CHANGES**

   - Camera module now allows user to define which color LED is the ‘front’ using red-green position tracking.

**BUG FIXES**

   None

1.5.1 (February 28, 2017)
-------------------------



**MINOR CHANGES**

   - Camera module now does not allow switching cameras once a recording file has been created. File must be closed before the camera can be switched again.


**BUG FIXES**

   - Fixed minor memory leak from PETH windows

   - Fixed Camera module time rate being recorded as 0 instead of 30000 

   - Fixed bug causing StateScript GUI to crash when in standalone mode and opening the camera module

1.5.0 (February 15, 2017)
-------------------------



**MAJOR FEATURES**

- Added Multi-Plot PETH functionality in spikes window. Users now have the option of seeing a histogram of all spike data points as well as histograms of individual, user-defined cluster plots at the same time. Clicking on specific cluster plots will open a PSTH window containing more specific data bout that cluster. Further, switching to a different nTrode will update the histogram window automatically.

- Automated testing system, specifically with regard to command line testing. These tests currently exist: a workspaces test (opens all workspaces and closes them among a set), a file playback test (opens all files in directory and checks to make sure all threads are syncing properly), a hardware connection test (tries communicating with a hardware setup), and a hardware streaming test to make sure data can be streamed from the hardware.

- Benchmarking GUI. Basically a settings control panel available in the main Trodes program, the Benchmarking GUI allows the user to edit benchmarking settings at runtime. Along with turning specific benchmarking tests on/off, the user can edit the frequency with which benchmarking statements are being printed (this is to prevent console clogging). These benchmarking settings can now also be saved and loaded via workspace files.

- Version information (compile date/time, git commit compiled, Trodes version info) can now be displayed from all modules in their ‘About’ tabs. This information is also saved in recordings and exported using the export utilities.

- Linux build system now automatically packages all required files into final build folder. All modules are compiled alongside Trodes main in all OS’s using Trodes_all.pro.

- Event system.  Events can now be defined and recieved by other modules for user-defined behavior.  Current events include spikes from defined units and zone enter/exit events from the camera module.



**MINOR CHANGES**

   - Warnings are now thrown when opening Trodes if a separate instance of Trodes is detected to be running with an open workspace and Trodes will prevent itself from opening a workspace if this is the case to prevent networking issues.

   - Trodes debug .txt logs are now automatically created in a folder directly adjacent to the Trodes executable, for windows/linux this is the same location as the binary/exe, and for mac the .app folder.



**BUG FIXES**

   - Debug messages from modules now forwarded correctly to Trodes main on Windows

   - Camera module auto gain behavior removed for GigE cameras

   - Switching between two GigE cameras is now more robust to failed connections

   - Switching from either of the signal generator sources no longer causes the next source to crash the system

   - Rhythm source turned off by default to prevent crash when selected

   - Improved button behavior and appearance across OS’s

1.4.2
------



**MAJOR FEATURES**

- StateScript GUI now has a separate input widget to type commands to hardware. Users can use the up and down arrows to scroll through command history.

- Event System Added
- Implemented an event system in Trodes server-client architecture that allows modules to dynamically create and send event messages to all other modules.
- Added EventHandler class implementing a GUI menu allowing users to create connections between events and module actions. (e.g. when an event is received, all actions/methods linked to the event will be executed)
- This class is highly portable and can easily be added to any module. (see documentation on the Trodes Wiki)
 
- Position data streaming greatly expanded, the following data is now being streamed:
- Linear position data (continuous)
- Linear track data whenever it is created/edited/moved in a relative scale (0-1) format. (Non-Continuous)
- Velocity data received from embedded Python. (Continuous)
- See documentation on the Trodes wiki for specific format information
- A compatibility mode was added that will either enable, or disable the changes to  position data streaming (modules that rely on the previous position data format will not be broken by this update).  

- SimpleCommunicator is a heavily modified version of SimpleMonitor which should be used as an example for how to set up a module to request and receive data from other modules. (It should go hand-in-hand with the wiki for documentation purposes)
- SimpleCommunicator currently supports full communication with Trodes and CameraModule. (Notably, SimpleCommunicator will display CameraModule’s real time position data)
- SimpleCommunicator contains a fully implemented EventHandler.


**MINOR FEATURES**

   
- All executables can now be called directly. (I.e you can run Trodes, instead of Trodes.sh) The shell scripts have been removed.

- OfflineSorter exporter added to export programs
- Added event system support to Trodes main module (it can now send events to other modules)
- Added events to Trodes which track when spikes occur in user defied clusters in different NTrodes. These events are created when a new polygon is created in the ‘spike’ dialog menu.

- Several event definitions added at module startup (e.g. Zone created, linearization tract created) that can be seen by other modules.
- Events added that tell the user when specific zones have been entered and exited by tracked objects.
- Zones will now turn blue when entered by tracked objects and revert to clear when left.
- Added UI element in Camera Module that brings up an eventHandler menu.
- Added a plotLocation method action visible in the event connection menu that plots a point on the currently tracked position 


**BUG FIXES**

  
- Fixed an issue that would cause a crash if the user tried to create a linear track while tracking was enabled.
- Fixed an issue that would cause a crash when double clicking the first node while drawing with either the Linear Track tool or the Range Line tool.
- Fixed a bug in the load geometry code which would append a bad node onto the end of loaded linear geometry.

1.4.1
------


**MINOR FEATURES**

- Added debugging tool which prints all function calls as specific IDs in console output (set DEBUG_MODE define to 1 in videoDisplay.h)

**BUG FIXES**

- Export code can now keep going if an erroneous backwards timestamp is detected with -abortbaddata 0 flag

- Fixed an issue where cameraModule would try to track linear position on a rangeLine and cause a crash.

1.4.0
-----


**MAJOR CHANGES**

- Improved buffering of data to ensure that data are not lost even if the hardrive become inaccessible for over a second.  If the buffer is overrun, the system will now recover smoothly after the lost section.

- Embedded python in Camera Module.  Still a work in progress, but this will be used to allow custom processing of position information

- Optimized processing of video frames in Camera Module to reduce CPU load

- Added export programs, including ones for Phy Suite and Offline Sorter




**MINOR CHANGES**

- Improved graphics plotting in channel streaming window.  All done with Open GL.

- Camera Module linearization tool to project animal's position onto a track geometry. Bugs were fixed to allow saving and loading of geometry files.

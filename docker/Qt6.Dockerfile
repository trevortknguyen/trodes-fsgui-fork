# Every Docker image has a strategy. In this
# case, we want an image that only has the tools
# that we need and nothing else.

# We can add the source we want to compile by
# using an attached volume. That way it's
# running without ever really needing to be
# updated.

from ubuntu:20.04
from python:3.9

WORKDIR /Qt
RUN pip install aqtinstall
# need to install the charts too!
# RUN aqt install-qt linux desktop 6.2.2 -m qtcharts qtmultimedia
# install everything to rule it out
RUN aqt install-qt linux desktop 6.2.2 -m qt3d qt5compat qtcharts qtconnectivity qtdatavis3d qtimageformats qtlottie qtmultimedia qtnetworkauth qtpositioning qtquick3d qtquicktimeline qtremoteobjects qtscxml qtsensors qtserialbus qtserialport qtshadertools qtvirtualkeyboard qtwaylandcompositor qtwebchannel qtwebengine qtwebsockets qtwebview
# make sure qmake is on the path
ENV QT_PATH=/Qt/6.2.2/gcc_64/bin
ENV QT_PLUGIN_PATH=/Qt/6.2.2/gcc_64/plugins
ENV LD_LIBRARY_PATH=/Qt/6.2.2/gcc_64/lib

ENV PATH="${QT_PATH}:${PATH}"

# follow what the docs say, don't dance
RUN apt-get update
RUN apt-get install -y build-essential
RUN apt-get install -y chrpath
RUN apt-get install -y mesa-common-dev libglu1-mesa-dev
RUN apt-get install -y libpulse-dev
RUN apt-get install -y libgstreamer-plugins-base1.0-0

# ran into some link errors
RUN apt-get install -y libxkbcommon-x11-dev
RUN apt-get install -y libgstreamer-plugins-base1.0-dev

# help with make install
RUN apt-get install -y rsync

# adds messagepack headers
RUN apt-get install -y libmsgpack-dev
# adds zmq.hpp because there is no install

WORKDIR /cppzmq
RUN wget https://github.com/zeromq/cppzmq/archive/refs/tags/v4.8.1.tar.gz && tar -xvf v4.8.1.tar.gz
RUN cp cppzmq-4.8.1/zmq.hpp /usr/include

WORKDIR /code
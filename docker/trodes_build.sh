DOCKER_WORKDIR="/code"
DOCKERFILE="./Qt6.Dockerfile"
IMAGE_TAG="trodes-build-qt6"

build_docker_image () {
    docker build \
        --tag "$IMAGE_TAG" \
        --file "$DOCKERFILE" \
        .
}

run_interactive_docker_image () {
    local source="$1"
    docker run \
        --mount type=bind,source=$source,target="$DOCKER_WORKDIR" \
        --rm \
        --interactive \
        --tty \
        $IMAGE_TAG \
        /bin/bash
}

run_build_inside_docker_container () {
    local source="$1"
    docker run \
        --mount type=bind,source=$source,target="$DOCKER_WORKDIR" \
        --rm \
        $IMAGE_TAG \
        /bin/bash -c "qmake && make -j4 && make install"
}

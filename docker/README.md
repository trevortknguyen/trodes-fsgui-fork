# Building Trodes using a Docker container

## Using

```
source trodes_build.sh

# build the image
build_docker_image

# enter an interactive session
run_interactive_docker_image /path/to/trodes/source

# build trodes without intervention
run_build_inside_docker_container /path/to/trodes/source
```

Building the image should be done within the same directory as `Qt6.Dockerfile` because the path to the Dockerfile is relative. The other commands can be run in any directory.
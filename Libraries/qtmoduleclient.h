#ifndef QTMODULECLIENT_H
#define QTMODULECLIENT_H

//#include <configuration.h>
//#include <qtmoduledebug.h>
#include <QObject>
#include <QString>
#include <QDebug>
#include <QMap>
#include <QVector>
#include <QThread>

#include <TrodesNetwork/Generated/AcquisitionCommand.h>
#include <TrodesNetwork/Generated/InfoRequest.h>
#include <TrodesNetwork/Generated/InfoResponse.h>
#include <TrodesNetwork/Generated/TrodesEvent.h>
#include <TrodesNetwork/Resources/ServiceConsumer.h>
#include <TrodesNetwork/Resources/SinkPublisher.h>

#include "trodesmsg.h"

Q_DECLARE_METATYPE(TrodesMsg)
/*! \class QtModuleClient
 * This class is meant to serve as a base class for all Qt based applications
 * that want to connect to the Trodes network.  It provides specific signals
 * for all incomming message types as well as overloadable processing functions
 * for Trodes-defined high frequency data types.
 */
class QtModuleClient :  public QObject {
    Q_OBJECT
public:
    QtModuleClient(const char* id, const char *addr, int ePort);
    ~QtModuleClient(void);
    void configChanged();

    int initialize(); // from MlmWrap

    void sendTimeRequest(); // from AbstractModuleClient
    void sendTimeRateRequest(); // from AbstractModuleClient
    uint32_t latestTrodesTimestamp(); // from AbstractModuleClient
    uint32_t trodesTimeRate();
    uint32_t lastRequestedTimestamp();

public slots:
    void qtRequestEventList();
    void qtSubscribeToEvent(QString module, QString event);
    void qtUnsubscribeFromEvent(QString module, QString event);
    void qtProvideEvent(QString event);
    void qtUnprovideEvent(QString event);
    void qtSendTimeRequest(void);
    void qtSendPlaybackCommand(QString cmd, uint32_t time);
    void qtSendStream(QString subject, TrodesMsg msg);
    void qtSendEvent(QString event, TrodesMsg msg);

protected:
    //Trodes event system function handles
    void recv_event(std::string origin, std::string event, TrodesMsg &msg);

    //Trodes command function handles
    void recv_file_open(std::string filename);
    void recv_file_close();
    void recv_source(std::string source);
    void recv_acquisition(std::string command, uint32_t time);
    void recv_quit();
    void recv_time(uint32_t time);
    void recv_timerate(int timerate);

private:
    std::string address;
    int port;
    uint32_t lastRequestedTimeValue;
    // allow module to send playback commands
    trodes::network::ServiceConsumer<trodes::network::AcquisitionCommand, std::string> acquisitionConsumer;
    // allow module to request the last frame
    trodes::network::ServiceConsumer<trodes::network::InfoRequest, trodes::network::InfoResponse> infoConsumer;
    trodes::network::SinkPublisher<trodes::network::TrodesEvent> eventSender;

    bool timestampRequestLock;

signals:
    void sig_event_list_received(QVector<EventDataType>);
    //event system signals
    void sig_event_received(QString origin, QString event, TrodesMsg msg);

    //command signals
    void sig_cmd_openFile(QString filename);
    void sig_cmd_closeFile(void);
    void sig_cmd_source(QString source);
    void sig_cmd_acquisition(QString command, quint32 time);
    void sig_cmd_quit(void);
    void sig_cmd_time(quint32 time);
    void sig_cmd_timerate(int timerate);

};

#endif // QTMODULECLIENT_H

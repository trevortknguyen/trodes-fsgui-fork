#ifndef QTMODULEDEBUG_H
#define QTMODULEDEBUG_H
#include <QMutex>
#include <QString>
#include <stdio.h>
#include <QByteArray>



extern void moduleMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg);
extern void setModuleName(QString name);

#endif // QTMODULEDEBUG_H
